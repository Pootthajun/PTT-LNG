﻿Imports System.IO
Imports System.Drawing

Public Class UC_StationaryOffRoutineTemplate6
    Inherits System.Web.UI.UserControl
    Dim BL As New EIR_BL

    Public Property ImageButtonImage1 As ImageButton
        Get
            Return ImageButton1
        End Get
        Set(value As ImageButton)
            ImageButton1 = value
        End Set
    End Property

    Public Property ImageButtonImage2 As ImageButton
        Get
            Return ImageButton2
        End Get
        Set(value As ImageButton)
            ImageButton2 = value
        End Set
    End Property

    Public Property ImageButtonImage3 As ImageButton
        Get
            Return ImageButton3
        End Get
        Set(value As ImageButton)
            ImageButton3 = value
        End Set
    End Property

    Public Property ImageButtonImage4 As ImageButton
        Get
            Return ImageButton4
        End Get
        Set(value As ImageButton)
            ImageButton4 = value
        End Set
    End Property

    Public Property TextDetail1 As UC_TextEditor
        Get
            Return UC_TextEditor1
        End Get
        Set(value As UC_TextEditor)
            UC_TextEditor1 = value
        End Set
    End Property
    Public Property TextDetail2 As UC_TextEditor
        Get
            Return UC_TextEditor2
        End Get
        Set(value As UC_TextEditor)
            UC_TextEditor2 = value
        End Set
    End Property
    Public Property TextDetail3 As UC_TextEditor
        Get
            Return UC_TextEditor3
        End Get
        Set(value As UC_TextEditor)
            UC_TextEditor3 = value
        End Set
    End Property
    Public Property TextDetail4 As UC_TextEditor
        Get
            Return UC_TextEditor4
        End Get
        Set(value As UC_TextEditor)
            UC_TextEditor4 = value
        End Set
    End Property



End Class