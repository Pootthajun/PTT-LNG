﻿Imports System.IO
Public Class RenderImageEditor
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Form("Action") Is Nothing Then Exit Sub
        Try
            Dim fileContent As Byte() = Convert.FromBase64String(Request.Form("filecontent").Replace("data:image/png;base64,", ""))

            Dim TempPicturePath As String = BL.Picture_Path
            If TempPicturePath.StartsWith("\") = False Then
                TempPicturePath += "\"
            End If
            TempPicturePath += "Temp\" & Session("USER_Name") & "\"

            If IO.Directory.Exists(TempPicturePath) = False Then
                IO.Directory.CreateDirectory(TempPicturePath)
            End If

            'ImageEditor
            Dim FilePath As String = TempPicturePath & "DrawingImage.png"
            File.WriteAllBytes(FilePath, fileContent)

            If File.Exists(FilePath) = True Then
                Session("DrawingImagePath") = FilePath
                Response.Write(FilePath)
            Else
                Response.Write("")
            End If
        Catch ex As Exception
            Response.Write("")
        End Try

    End Sub

End Class