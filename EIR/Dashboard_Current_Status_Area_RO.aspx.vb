﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status_Area_RO
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)
        End Set
    End Property

    Public Property AREA_ID() As Integer
        Get
            If Not IsNothing(ViewState("AREA_ID")) AndAlso IsNumeric(ViewState("AREA_ID")) Then
                Return ViewState("AREA_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AREA_ID") = value
            '---------------- Set Relate Plant ID-----------
            Dim SQL As String = "SELECT PLANT_ID FROM MS_AREA WHERE AREA_ID=" & AREA_ID
            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count = 0 Then
                PLANT_ID = 0
            Else
                PLANT_ID = DT.Rows(0).Item("PLANT_ID")
            End If


            SQL = "SELECT Area_Name FROM MS_AREA WHERE AREA_ID=" & value
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count = 0 Then
                lblArea.Text = "..."
            Else
                lblArea.Text = DT.Rows(0).Item("Area_Name")
            End If
            lblArea.PostBackUrl = lblPlant.PostBackUrl


        End Set
    End Property

    Protected Sub lblBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblBack.Click
        Response.Redirect("Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & PLANT_ID)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("AREA_ID")) Then
                AREA_ID = Request.QueryString("AREA_ID")
            End If
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SQL As String = " DECLARE @AREA_ID INT=" & AREA_ID & vbNewLine
        SQL &= "   SELECT TAG_ID,TAG_CODE,TAG_NAME,TAG_TYPE_ID,TAG_TYPE_NAME,CUR_LEVEL," & vbNewLine
        SQL &= "   CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END Normal," & vbNewLine
        SQL &= "   CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END ClassC," & vbNewLine
        SQL &= "   CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END ClassB," & vbNewLine
        SQL &= "   CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END ClassA," & vbNewLine
        SQL &= "   RPT_Year,RPT_No" & vbNewLine
        SQL &= "   FROM" & vbNewLine
        SQL &= "   (" & vbNewLine
        SQL &= "   SELECT DISTINCT MS_RO_TAG.TAG_ID,MS_Area.AREA_CODE+'-'+MS_Process.PROC_CODE +'-' + MS_RO_TAG.TAG_NO TAG_CODE,MS_RO_TAG.TAG_NAME,MS_RO_TAG.TAG_TYPE_ID,TAG_TYPE_NAME," & vbNewLine
        SQL &= "   ISNULL(CUR_LEVEL,0) CUR_LEVEL,VW.RPT_Year,VW.RPT_No" & vbNewLine
        SQL &= "   FROM MS_Area" & vbNewLine
        SQL &= "   INNER JOIN MS_RO_TAG ON MS_RO_TAG.AREA_ID=MS_Area.AREA_ID AND MS_RO_TAG.Active_Status=1" & vbNewLine
        SQL &= "   INNER JOIN MS_RO_TAG_TYPE ON MS_RO_TAG.TAG_TYPE_ID=MS_RO_TAG_TYPE.TAG_TYPE_ID AND MS_RO_TAG_TYPE.Active_Status=1" & vbNewLine
        SQL &= "   LEFT JOIN MS_Process ON MS_RO_TAG.PROC_ID=MS_Process.PROC_ID AND MS_Process.Active_Status=1" & vbNewLine
        SQL &= "   LEFT JOIN (" & vbNewLine
        SQL &= "        SELECT TAG_ID,CUR_LEVEL,RPT_Year,RPT_No" & vbNewLine
        SQL &= "        FROM" & vbNewLine
        SQL &= "        (" & vbNewLine
        SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
        SQL &= "            FROM" & vbNewLine
        SQL &= "            (" & vbNewLine
        SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
        SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
        SQL &= "                FROM" & vbNewLine
        SQL &= "     		    (" & vbNewLine
        SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_RO_Detail.TAG_TYPE_ID,RPT_RO_Detail.INSP_ID," & vbNewLine
        SQL &= "     				CASE WHEN RPT_RO_Detail.INSP_ID = 12 THEN" & vbNewLine
        SQL &= "     					CASE ICLS_ID WHEN 3 THEN 3 WHEN 2 THEN 1 WHEN 1 THEN 0 WHEN 0 THEN 0 ELSE ICLS_ID END" & vbNewLine
        SQL &= "     				ELSE" & vbNewLine
        SQL &= "                        ICLS_ID" & vbNewLine
        SQL &= "     				END CUR_LEVEL" & vbNewLine
        SQL &= "                    FROM RPT_RO_Detail" & vbNewLine
        SQL &= "     		    ) TB1" & vbNewLine
        SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
        SQL &= "     	    ) TB2" & vbNewLine
        SQL &= "        ) TB3" & vbNewLine
        SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "   ) VW ON MS_RO_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
        SQL &= "   WHERE MS_Area.Active_Status = 1 AND MS_Area.AREA_ID=@AREA_ID" & vbNewLine
        SQL &= "   )" & vbNewLine
        SQL &= "   TAG" & vbNewLine
        SQL &= "   ORDER BY TAG_CODE" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("Dashboard_Current_Status_RO_Area_" & AREA_ID) = DT
        DisplayChart(ChartMain, New EventArgs)

        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        lblTag.Text = e.Item.DataItem("TAG_CODE")
        lblLevel.Text = BL.Get_Problem_Level_Name(e.Item.DataItem("CUR_LEVEL"))
        lblLevel.CssClass = BL.Get_Inspection_Css_Text_By_Level(e.Item.DataItem("CUR_LEVEL"))
        tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_RO.aspx?TAG_ID=" & e.Item.DataItem("TAG_ID") & "&RPT_YEAR=" & e.Item.DataItem("RPT_YEAR") & "&RPT_NO=" & e.Item.DataItem("RPT_NO") & "','Dialog_Tag_ST_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_ChartType.SelectedIndexChanged

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartMain.Series(0).ChartType
        Select Case ddl_ChartType.Items(ddl_ChartType.SelectedIndex).Value
            Case "Pie"
                ChartType = DataVisualization.Charting.SeriesChartType.Pie
            Case "Doughnut"
                ChartType = DataVisualization.Charting.SeriesChartType.Doughnut
            Case "Funnel"
                ChartType = DataVisualization.Charting.SeriesChartType.Funnel
            Case "Pyramid"
                ChartType = DataVisualization.Charting.SeriesChartType.Pyramid
        End Select
        ChartMain.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Current_Status_RO_Area_" & AREA_ID)
        Dim Normal As Double = DT.Compute("SUM(Normal)", "")
        Dim ClassC As Double = DT.Compute("SUM(ClassC)", "")
        Dim ClassB As Double = DT.Compute("SUM(ClassB)", "")
        Dim ClassA As Double = DT.Compute("SUM(ClassA)", "")

        Dim YValue As Double() = {Normal, ClassC, ClassB, ClassA}
        'Dim XValue As String() = {"Normal", "ClassC", "ClassB", "ClassA"}
        ChartMain.Series("Series1").Points.DataBindY(YValue)
        ChartMain.Series("Series1").Points(0).LegendText = "Normal"
        ChartMain.Series("Series1").Points(1).LegendText = "ClassC"
        ChartMain.Series("Series1").Points(2).LegendText = "ClassB"
        ChartMain.Series("Series1").Points(3).LegendText = "ClassA"

        ChartMain.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(1).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(2).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(3).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

        ChartMain.Series("Series1").Points(0).Color = Drawing.Color.Green
        ChartMain.Series("Series1").Points(1).Color = Drawing.Color.Yellow
        ChartMain.Series("Series1").Points(2).Color = Drawing.Color.Orange
        ChartMain.Series("Series1").Points(3).Color = Drawing.Color.Red

        ChartMain.Titles(1).Text = "Total " & FormatNumber(Normal + ClassC + ClassB + ClassA, 0) & " tag(s)"
    End Sub


End Class