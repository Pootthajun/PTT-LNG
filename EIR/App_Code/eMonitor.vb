﻿Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports System.Data
Public Class eMonitor

    Public Function Get_eMonitor_Data(ByVal PlantCode As String, ByVal Tag() As String) As DataTable

        Dim Tag_List As String = ""
        For i As Integer = 0 To Tag.Length - 1
            Tag_List &= "'" & Tag(i).Replace("'", "''").Replace(" ", "") & "',"
        Next
        If Tag_List = "" Then Return Nothing

        Dim PLSQL As String = "select " & vbLf
        PLSQL &= " STATION.HIERARCHY_ID,STATION.HIERARCHY_LABEL AS STATION,'' AS AREA," & vbLf
        PLSQL &= " TAG.HIERARCHY_LABEL AS TAG,LOCATION.ID AS LOCATION,LOCATION.POSITION AS POS,DIRECTION.DIRECTION," & vbLf
        PLSQL &= " LOCATION.CATEGORY,LOCATION.DESCRIPTION,MEASUREMENT_DEF.COLLECTION," & vbLf
        PLSQL &= " SINGLE_VALUE_DATA.TIME_STAMP AS TIME_STAMP_MAGNITUDE,SINGLE_VALUE_DATA.AMPLITUDE_MAX AS AMPLITUDE_MAX_MAGNITUDE," & vbLf
        PLSQL &= " MULTI_VALUE_DATA.TIME_STAMP AS TIME_STAMP_SPECTRUM,MULTI_VALUE_DATA.AMPLITUDE_MAX AS AMPLITUDE_MAX_SPECTRUM" & vbLf

        PLSQL &= " from HIERARCHY_LIST STATION" & vbLf
        PLSQL &= " left outer join HIERARCHY_LIST TAG on STATION.HIERARCHY_ID = TAG.PARENT_ID" & vbLf
        PLSQL &= " left outer join LOCATION on TAG.HIERARCHY_ID = LOCATION.HIERARCHY_ID" & vbLf
        PLSQL &= " left outer join DIRECTION on LOCATION.DIRECTION = DIRECTION.ID" & vbLf
        PLSQL &= " left outer join MEASUREMENT_DEF on LOCATION.LOCATION_ID = MEASUREMENT_DEF.LOCATION_ID" & vbLf
        PLSQL &= " left outer join SINGLE_VALUE_DATA on MEASUREMENT_DEF.MEASDEF_ID = SINGLE_VALUE_DATA.MEASDEF_ID" & vbLf
        PLSQL &= " left outer join MULTI_VALUE_DATA on MEASUREMENT_DEF.MEASDEF_ID = MULTI_VALUE_DATA.MEASDEF_ID" & vbLf

        PLSQL &= " WHERE TAG.PARENT_ID > 0" & vbLf
        PLSQL &= " AND replace(TAG.HIERARCHY_LABEL,' ') IN (" & Tag_List.Substring(0, Tag_List.Length - 1) & ")" & vbLf
        PLSQL &= " AND DIRECTION.DIRECTION IN ('Axial','Vertical','Horizontal')" & vbLf
        '--------------------- Filter Used Criteria -----------------
        PLSQL &= " order by STATION.HIERARCHY_LABEL,TAG.HIERARCHY_LABEL,LOCATION.POSITION " & vbLf

        Dim OracleConnStr As String = "Data Source=ENTEK815;User Id=" & PlantCode & ";Password=euser;"
        Dim DA As New OracleDataAdapter(PLSQL, OracleConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        'HIERARCHY_ID
        'STATION
        'AREA
        'Tag
        'LOCATION
        'POS
        'DIRECTION
        'CATEGORY
        'DESCRIPTION
        'COLLECTION
        'TIME_STAMP_MAGNITUDE
        'AMPLITUDE_MAX_MAGNITUDE
        'TIME_STAMP_SPECTRUM
        'AMPLITUDE_MAX_SPECTRUM

        Return DT

    End Function

End Class
