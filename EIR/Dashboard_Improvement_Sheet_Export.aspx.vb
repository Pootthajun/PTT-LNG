﻿Public Class Dashboard_Improvement_Sheet_Export
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Year As Integer = Request.QueryString("Year")
            Dim Equipment As Integer = Request.QueryString("Equipment")
            UC_Dashboard_Improvement_Sheet.BindData(Year, Equipment)
        End If
    End Sub

End Class