﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_Annual_Progress
    Inherits System.Web.UI.UserControl

    Dim Dashboard As New DashboardClass

    Friend Sub BindData(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type, ByVal Plant_ID As Integer, ByVal Plant_Name As String)
        'Equipment
        '1 = Stationary
        '2 = Rotating
        '3 = Stationary & Rotating
        '4 = Lube Oil
        'Plant_id = 0 คือ All Plant

        If Year > 2500 Then
            Year = Year - 543
        End If

        lblYear.Text = Year
        lblEquipment.Text = Equipment
        lblPlantID.Text = Plant_ID

        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        DT_Dashboard = Dashboard.GetDayOfYear(Year) 'ข้อมูลทุกวันในปีตั้งไว้รอ

        Dim DT_Actual As DataTable = DT_Dashboard.Copy
        DT_Actual.Columns.Remove("Date")
        DT_Actual.Columns.Remove("Months")
        DT_Actual.Columns.Remove("Plan")
        DT_Actual.Columns.Remove("Progess")
        DT_Actual.Columns.Add("TotalFinish", GetType(Integer))


        '--------------------------------
        'หาจำนวนงานที่เสร็จจริงทั้งหมดในปี(รายละเอียดทั้งหมด เพื่อมาคำนวณเป็นเปอร์เซน:ปี)
        DT = Dashboard.AnnualProgressDetail(Year, Equipment, Plant_ID) 'ข้อมูลดิบ
        Dim Total As Integer = DT.Rows.Count

        For i As Integer = 0 To DT_Actual.Rows.Count - 1
            Dim TotalFinish As Integer = DT.Compute("COUNT(RPT_ANL_DATE)", "RPT_ANL_DATE<='" & DT_Actual.Rows(i).Item("Days") & "'")
            DT_Actual.Rows(i).Item("TotalFinish") = TotalFinish
        Next

        DT_Actual.Columns.Add("Progress", GetType(Double), "TotalFinish*100/" & DT.Rows.Count)

        Dim _col() As String = {"RPT_Period_End"}
        Dim DT_Plan As DataTable = DT.DefaultView.ToTable(True, _col).Copy
        DT_Plan.Columns.Add("TotalFinish", GetType(Integer))
        For i As Integer = 0 To DT_Plan.Rows.Count - 1
            Dim TotalFinish As Integer = DT.Compute("COUNT(RPT_Period_End)", "RPT_Period_End<='" & DT_Plan.Rows(i).Item("RPT_Period_End") & "'")
            DT_Plan.Rows(i).Item("TotalFinish") = TotalFinish
        Next

        If DT_Plan.Rows.Count > 0 Then
            If Not DT_Plan.Rows(0).Item("RPT_Period_End") Like "*0101" Then
                Dim DR_Plan As DataRow = DT_Plan.NewRow
                DR_Plan("RPT_Period_End") = Year & "0101"
                DR_Plan("TotalFinish") = 0
                DT_Plan.Rows.InsertAt(DR_Plan, 0)
            End If
        End If


        DT_Plan.Columns.Add("Progress", GetType(Double), "TotalFinish*100/" & DT.Rows.Count)
        DisplayChart2(DT_Plan, DT_Actual, Year, Equipment, Plant_ID, Plant_Name)

        '************* Data Repeter ***************
        Dim DT_RPT As New DataTable
        Dim DR As DataRow
        DT_RPT.Columns.Add("Month")
        DT_RPT.Columns.Add("Month_Name")
        DT_RPT.Columns.Add("Plan")
        DT_RPT.Columns.Add("Progress")

        For i As Integer = 1 To 12
            Dim C As New Converter
            Dim Plan As Object = DT_Plan.Compute("MAX([Progress])", "RPT_Period_End <= '" & Year & i.ToString.PadLeft(2, "0") & "32" & "'")
            Dim Progess As Object = DT_Actual.Compute("MAX([Progress])", "Days <= '" & Year & i.ToString.PadLeft(2, "0") & "32" & "'")

            DR = DT_RPT.NewRow
            DR("Month") = i
            DR("Month_Name") = C.ToMonthNameEN(i).Substring(0, 3)
            If Not IsDBNull(Plan) Then
                DR("Plan") = Plan
            End If
            If Not IsDBNull(Progess) Then
                DR("Progress") = Progess
            End If
            DT_RPT.Rows.Add(DR)
        Next

        rptData.DataSource = DT_RPT
        rptData.DataBind()

    End Sub

    Protected Sub DisplayChart2(ByVal DT_Plan As DataTable, ByVal DT_Actual As DataTable, ByVal Year As Integer, ByVal Equipment As Integer, ByVal Plant_ID As Integer, ByVal Plant_Name As String)
        ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Clear()
        ChartMain.ChartAreas("ChartArea1").AxisX.Title = "Dec " & Year
        Dim culture As New System.Globalization.CultureInfo("en-US")
        Dim C As New Converter

        Dim Plant As String = ""
        If Plant_ID > 0 Then
            Plant = "  Plant  " & Plant_Name
        End If

        If Year < 2500 Then
            Year = Year + 543
        End If

        ChartMain.Titles("Title1").Text = "Annual Inspection  Progress for " & vbNewLine & vbNewLine & Dashboard.FindEquipmentName(Equipment) & " Equipment   Year  " & Year
        lblHeader.Text = ChartMain.Titles("Title1").Text

        '------------ เริ่ม plot Plan ต่อ ----------
        For i As Integer = 0 To DT_Plan.Rows.Count - 1
            Dim D As String = DT_Plan.Rows(i).Item("RPT_Period_End")
            DT_Actual.DefaultView.RowFilter = "Days='" & D & "'"
            If DT_Actual.DefaultView.Count = 0 Then Continue For
            Dim P As Integer = DT_Actual.Rows.IndexOf(DT_Actual.DefaultView(0).Row)
            ChartMain.Series("Series1").Points.AddXY(P + 1, DT_Plan.Rows(i).Item("Progress"))
        Next
        Dim MaxReport As String = "0"
        If DT_Plan.Rows.Count > 0 Then
            MaxReport = DT_Plan.Rows(DT_Plan.Rows.Count - 1).Item("TotalFinish").ToString
        End If

        '------------ เริ่ม plot จาก Actual ก่อนเพื่อตั้งวันให้ครบปี ----------
        For i As Integer = 0 To DT_Actual.Rows.Count - 1
            'Dim D As String 
            ChartMain.Series("Series2").Points.AddXY(i + 1, DT_Actual.Rows(i).Item("Progress"))
            ChartMain.Series("Series2").Points(i).ToolTip = "จำนวน Report ที่ตรวจเสร็จ " & DT_Actual.Rows(i).Item("TotalFinish") & "/" & MaxReport
        Next


    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Year As Integer, ByVal Equipment As Integer, ByVal Plant_ID As Integer, ByVal Plant_Name As String)

        Dim Plant As String = ""
        If Plant_ID > 0 Then
            Plant = "  Plant  " & Plant_Name
        End If

        If Year < 2500 Then
            Year = Year + 543
        End If

        ChartMain.Titles("Title1").Text = "Annual Inspection  Progress for " & vbNewLine & vbNewLine & Dashboard.FindEquipmentName(Equipment) & " Equipment   Year  " & Year

        '----------------------------------------------------
        Dim DT As DataTable = Session("Dashboard_Annual_Progress")
        If Year > 2500 Then
            Year = Year - 543
        End If

        ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Clear()
        ChartMain.ChartAreas("ChartArea1").AxisX.Title = "Dec " & Year
        Dim culture As New System.Globalization.CultureInfo("en-US")
        Dim C As New Converter
        'For i As Integer = 1 To 12
        '    Dim tmp As String = Year & "-" & i.ToString.PadLeft(2, "0")
        '    Dim StartM As DateTime = DT.Compute("MIN([Date])", "Days LIKE '" & tmp.Replace("-", "") & "%'")
        '    Dim EndM As DateTime = DT.Compute("MAX([Date])", "Days LIKE '" & tmp.Replace("-", "") & "%'")
        '    ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(StartM.ToOADate, EndM.ToOADate, C.ToMonthNameEN(i).ToString.Substring(0, 3).ToString)
        'Next

        Dim IsLastPoint As Boolean = False
        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series2").Points.Clear()

        Dim LastPlan As Double = 0
        Dim LastProgress As Double = 0

        For i As Integer = 0 To DT.Rows.Count - 1
            'ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("Months"), DT.Rows(i).Item("Plan"))
            'ChartMain.Series("Series2").Points.AddXY(DT.Rows(i).Item("Months"), DT.Rows(i).Item("Progess"))

            Dim D As DateTime = DT.Rows(i).Item("DATE")
            Dim L As String = ""
            If DatePart(DateInterval.Day, D) = DateTime.DaysInMonth(DatePart(DateInterval.Year, D), DatePart(DateInterval.Month, D)) Then
                L = C.ToMonthNameEN(DatePart(DateInterval.Month, D)).Substring(0, 3)
            End If

            If Not IsDBNull(DT.Rows(i).Item("Plan")) Then
                Dim Plan As Double = 0
                Plan = FormatNumber(DT.Rows(i).Item("Plan"), 2)
                ChartMain.Series("Series1").Points.AddXY(D, Plan)
                ChartMain.Series("Series1").Points(i).ToolTip = FormatNumber(Plan)
                LastPlan = Plan
            End If


            If Not IsDBNull(DT.Rows(i).Item("Progess")) Then
                Dim Progress As Double = 0
                Progress = FormatNumber(DT.Rows(i).Item("Progess"), 2)
                ChartMain.Series("Series2").Points.AddXY(D, Progress)
                ChartMain.Series("Series2").Points(i).ToolTip = FormatNumber(Progress)
                LastProgress = Progress
            End If

            If (IsDBNull(DT.Rows(i).Item("Plan")) Or IsDBNull(DT.Rows(i).Item("Progess"))) Then
                If Not IsLastPoint Then
                    ChartMain.Series("Series1").Points(ChartMain.Series("Series1").Points.Count - 1).Label = FormatNumber(LastPlan)
                    ChartMain.Series("Series1").Points(ChartMain.Series("Series1").Points.Count - 1).LabelBackColor = Drawing.Color.Green
                    ChartMain.Series("Series1").Points(ChartMain.Series("Series1").Points.Count - 1).LabelForeColor = Drawing.Color.White
                    ChartMain.Series("Series2").Points(ChartMain.Series("Series2").Points.Count - 1).Label = FormatNumber(LastProgress)
                    ChartMain.Series("Series2").Points(ChartMain.Series("Series2").Points.Count - 1).LabelBackColor = Drawing.Color.Blue
                    ChartMain.Series("Series2").Points(ChartMain.Series("Series2").Points.Count - 1).LabelForeColor = Drawing.Color.White
                    IsLastPoint = True
                End If

                ChartMain.Series("Series1").Points.AddXY(D, DBNull.Value)
                ChartMain.Series("Series2").Points.AddXY(D, DBNull.Value)
            End If

        Next
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblMonth As Label = e.Item.FindControl("lblMonth")
        Dim lblPlan As Label = e.Item.FindControl("lblPlan")
        Dim lblProgress As Label = e.Item.FindControl("lblProgress")

        Dim Plan As Double = 0.0
        Dim Progress As Double = 0.0
        lblMonth.Text = e.Item.DataItem("Month_Name")

        If Not IsDBNull(e.Item.DataItem("Plan")) AndAlso e.Item.DataItem("Plan") <> 0 Then
            Plan = e.Item.DataItem("Plan")
            lblPlan.Text = FormatNumber(Plan, 2)
        Else
            lblPlan.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("Progress")) AndAlso e.Item.DataItem("Progress") <> 0 Then
            Progress = e.Item.DataItem("Progress")
            lblProgress.Text = FormatNumber(Progress, 2)
        Else
            lblProgress.Text = "-"
        End If

    End Sub

End Class