﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_Template6.ascx.vb" Inherits="EIR.UC_ST_TA_Template6" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_TextEditor.ascx" tagname="UC_TextEditor" tagprefix="uc1" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<%--------------------------------------------------------------------------------------%>
<tr  style="text-align: center">
    <td style="width: 50%; background-color: black;text-align: center;">
       <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview1"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />

        <asp:Button ID="btnRefreshImage" runat="server" Text="Update/Refresh" Style="visibility: hidden; width: 0px;" /></td>

 <td style="width: 50%; background-color: black;text-align: center;" >
       <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview2"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />


</tr>


<tr id="trimage_1" runat="server" visible="false" style="text-align: center">
    <td style="width: 50%; background-color: black;">
        <div class="thumbnail">


            <a id="lnk_File_Dialog1" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File1" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>
    <td style="width: 50%; background-color: black;"  >
        <div class="thumbnail">


            <a id="lnk_File_Dialog2" runat="server" target="_blank" title="Figure No.2">
                <asp:Image ID="img_File2" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>
</tr>
<%--------------------------------------------------------------------------------------%>
<tr id="trbtn_1" runat="server" visible="false">
    <td style="width: 100%;" class="toolbar">
        <asp:Button ID="btnUpload1" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete1"></cc1:ConfirmButtonExtender>
    </td>  
    <td style="width: 100%;" class="toolbar" >
        <asp:Button ID="btnUpload2" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit2" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete2" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete2_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete2"></cc1:ConfirmButtonExtender>
    </td>
</tr>
<!-- End Table Images -->

<%--------------------------------------------------------------------------------------%>



<tr>
    <td >
        <uc1:UC_TextEditor ID="UC_TextEditor1" runat="server"   />
    </td>
    <td >
        <uc1:UC_TextEditor ID="UC_TextEditor2" runat="server"   />
    </td>
</tr>


<tr  style="text-align: center">
    <td style="width: 50%; background-color: black;text-align: center;">
       <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview3"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />


 <td style="width: 50%; background-color: black;text-align: center;" >
       <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview4"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />


</tr>
<tr>
    <td >
        <uc1:UC_TextEditor ID="UC_TextEditor3" runat="server"   />
    </td>
    <td >
        <uc1:UC_TextEditor ID="UC_TextEditor4" runat="server"   />
    </td>
</tr>
<tr id="trimage_2" runat="server" visible="false" style="text-align: center">
    <td style="width: 50%; background-color: black;">
        <div class="thumbnail">


            <a id="lnk_File_Dialog3" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File3" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>
    <td style="width: 50%; background-color: black;"  >
        <div class="thumbnail">


            <a id="lnk_File_Dialog4" runat="server" target="_blank" title="Figure No.2">
                <asp:Image ID="img_File4" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>
</tr>
<tr id="trbtn_2" runat="server" visible="false">
    <td style="width: 100%;" class="toolbar">
        <asp:Button ID="btnUpload3" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit3" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete3" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete3_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete3"></cc1:ConfirmButtonExtender>
    </td>  
    <td style="width: 100%;" class="toolbar" >
        <asp:Button ID="btnUpload4" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit4" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete4" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete4_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete4"></cc1:ConfirmButtonExtender>
    </td>
</tr>