﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing

Public Class EIR_PIPE
    Dim BL As New EIR_BL

    Public Enum Report_Step
        NA = 0
        Before_Remove = 1
        After_Remove = 2
        Thickness_Measurement = 3
        After_Repair = 4
        Coating_Inspection = 5
        Insulating_Inspection = 6
        Final = 7
    End Enum

    Public Function Get_REPORT_Step_Color(ByVal ReportStep As Report_Step) As Color
        Select Case ReportStep
            Case Report_Step.NA
                Return Color.Gray
            Case Report_Step.Before_Remove
                Return Color.SteelBlue
            Case Report_Step.After_Remove
                Return Color.Violet
            Case Report_Step.Thickness_Measurement
                Return Color.Red
            Case Report_Step.After_Repair
                Return Color.Orange
            Case Report_Step.Coating_Inspection
                Return Color.Brown
            Case Report_Step.Insulating_Inspection
                Return Color.MediumSeaGreen
            Case Report_Step.Final
                Return Color.Green
        End Select
    End Function

    Public Sub BindDDL_ReportOfficer(ByRef cmb As DropDownList,
                                   ByVal RPT_Year As Integer,
                                   ByVal RPT_No As Integer,
                                   ByVal RPT_Step As Report_Step,
                                   ByVal Role As EIR_BL.User_Level,
                                   Optional ByVal User_Full_Name As String = "")


        Dim HeaderTable As String = ""
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                HeaderTable = "RPT_PIPE_Before_Remove"
            Case Report_Step.After_Remove
                HeaderTable = "RPT_PIPE_After_Remove"
            Case Report_Step.Thickness_Measurement
                HeaderTable = "RPT_PIPE_TM_Header"
            Case Report_Step.After_Repair
                HeaderTable = "RPT_PIPE_After_Repair"
            Case Report_Step.Coating_Inspection
                HeaderTable = "RPT_PIPE_Coating"
            Case Report_Step.Insulating_Inspection
                HeaderTable = "RPT_PIPE_Insulating"
            Case Report_Step.Final
                HeaderTable = "RPT_PIPE_Final_Report"
        End Select
        '--------------------------------- PIPE
        Dim Sql As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbLf
        Sql &= " DECLARE @RPT_No AS INT =" & RPT_No & vbLf
        Dim RoleName As String = ""
        Select Case Role
            Case EIR_BL.User_Level.Collector
                Sql &= " SELECT 0 POS_ID, ISNULL(RPT_Outsource,'') User_Full_Name FROM " & HeaderTable & vbLf
                RoleName = "Creater"
            Case EIR_BL.User_Level.Inspector
                Sql &= " SELECT 0 POS_ID, ISNULL(RPT_Engineer,'') User_Full_Name FROM " & HeaderTable & vbLf
                RoleName = "Evaluator"
            Case EIR_BL.User_Level.Approver
                Sql &= " SELECT 0 POS_ID, ISNULL(RPT_Approver,'') User_Full_Name FROM " & HeaderTable & vbLf
                RoleName = "Approver"
        End Select
        Sql &= " WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No" & vbLf
        Dim DTPipe As New DataTable
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        DA.Fill(DTPipe)

        '--------------------------------- USER
        Sql = "SELECT P.POS_ID,ISNULL(U.User_Prefix,'') + ISNULL(U.USER_Name,'') + ' ' + ISNULL(U.User_Surname,'') User_Full_Name" & vbLf
        Sql &= " FROM MS_USER U " & vbLf
        Sql &= " INNER JOIN MS_User_Coverage C ON U.USER_ID=C.USER_ID" & vbLf
        Sql &= " INNER JOIN MS_User_Position P ON U.POS_ID=P.POS_ID" & vbLf
        Sql &= " INNER JOIN MS_User_Skill S ON U.USER_ID=S.USER_ID" & vbLf
        Sql &= " WHERE S.S_Stationary=1 " & vbLf
        Select Case Role
            Case EIR_BL.User_Level.Collector
                Sql &= " AND U.POS_ID=0"
                RoleName = "Creater"
            Case EIR_BL.User_Level.Inspector
                Sql &= " AND U.POS_ID IN (1,2,3)" & vbLf
            Case EIR_BL.User_Level.Approver
                Sql &= " AND U.POS_ID IN (2,3)" & vbLf
        End Select
        Dim DTUser As New DataTable
        DA = New SqlDataAdapter(Sql, BL.ConnStr)
        DA.Fill(DTUser)

        DTPipe.Merge(DTUser)

        Dim Col() As String = {"User_Full_Name"}
        Dim DT As DataTable = DTPipe.DefaultView.ToTable(True, "User_Full_Name")
        DTPipe.Dispose()
        DA.Dispose()

        cmb.Items.Clear()
        cmb.Items.Add(New ListItem("Choose " & RoleName & "...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("User_Full_Name").ToString.Trim = "" Then Continue For
            Dim Item As New ListItem(DT.Rows(i).Item("User_Full_Name"), DT.Rows(i).Item("User_Full_Name"))
            cmb.Items.Add(Item)
            If User_Full_Name = DT.Rows(i).Item("User_Full_Name") Then
                cmb.SelectedIndex = cmb.Items.Count - 1
            End If
        Next
    End Sub

    Public Sub BindDDl_Tag(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = -1, Optional ByVal AREA_ID As Integer = -1, Optional ByVal TAGE_ID As Integer = 0, Optional ByVal OnlyAvailable As Boolean = False)
        Dim SQL As String = "SELECT TAG_ID,TAG_Code " & vbLf
        SQL &= " FROM VW_PIPE_TAG" & vbLf
        Dim Filter As String = ""
        If PLANT_ID <> -1 Then
            Filter &= " PLANT_ID=" & PLANT_ID & " AND "
        End If
        If AREA_ID <> -1 Then
            Filter &= " AREA_ID=" & AREA_ID & " AND "
        End If
        If OnlyAvailable Then
            Filter &= " Active_Status=1 AND "
        End If
        If Filter <> "" Then SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        SQL &= " ORDER BY TAG_Code"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Tag...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_Code"), DT.Rows(i).Item("TAG_ID"))
            ddl.Items.Add(Item)
        Next
        If TAGE_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = TAGE_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Point(ByRef ddl As DropDownList, ByVal TAG_ID As Integer, Optional ByVal POINT_ID As Integer = -1, Optional ByVal AllowCreatePoint As Boolean = True)
        Dim SQL As String = "SELECT TAG_ID,POINT_ID,POINT_Name " & vbLf
        SQL &= " FROM MS_PIPE_POINT" & vbLf
        SQL &= " WHERE TAG_ID=" & TAG_ID & vbLf
        If POINT_ID <> -1 Then
            SQL &= " AND POINT_ID=" & POINT_ID & vbLf
        End If
        SQL &= " ORDER BY POINT_Name"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Point...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("POINT_Name"), DT.Rows(i).Item("POINT_ID"))
            ddl.Items.Add(Item)
        Next
        If POINT_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = POINT_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit Sub
                End If
            Next
            ddl.SelectedIndex = 0
        End If
    End Sub

    Public Sub BindDDl_Service(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Service WHERE active_status=1 ORDER BY SERVICE_Name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Service...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("SERVICE_Name"), DT.Rows(i).Item("SERVICE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Pressure_Code(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Pressure WHERE active_status=1 ORDER BY PRS_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Pressure Code...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PRS_Code"), DT.Rows(i).Item("PRS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Insulation_Code(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Insulation WHERE active_status=1 ORDER BY IN_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Insulation Code...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("IN_Code"), DT.Rows(i).Item("IN_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_TM_Instrument_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_TM_Instrument_Type ORDER BY INSM_Type_ID"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Instrument Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("INSM_Type_Name"), DT.Rows(i).Item("INSM_Type_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDl_Plant(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0)
        Dim SQL As String = "SELECT DISTINCT MS_PLANT.PLANT_ID,MS_PLANT.PLANT_Code" & vbLf
        SQL &= " FROM MS_PLANT INNER JOIN MS_PIPE_TAG On MS_PLANT.PLANT_ID=MS_PIPE_TAG.PLANT_ID" & vbLf
        SQL &= " WHERE MS_PLANT.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY PLANT_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Plant...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PLANT_CODE"), DT.Rows(i).Item("PLANT_ID"))
            ddl.Items.Add(Item)
        Next
        If PLANT_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = PLANT_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Area(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT DISTINCT MS_AREA.AREA_ID,MS_AREA.AREA_Code" & vbLf
        SQL &= " FROM MS_AREA INNER JOIN MS_PIPE_TAG On MS_AREA.AREA_ID=MS_PIPE_TAG.AREA_ID " & vbLf
        SQL &= " WHERE MS_AREA.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY AREA_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose an Area...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AREA_CODE"), DT.Rows(i).Item("AREA_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Area(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT DISTINCT MS_AREA.AREA_ID,MS_AREA.AREA_Code" & vbLf
        SQL &= " FROM MS_AREA INNER JOIN MS_PIPE_TAG On MS_AREA.AREA_ID=MS_PIPE_TAG.AREA_ID " & vbLf
        SQL &= " WHERE MS_AREA.PLANT_ID=" & PLANT_ID & " AND MS_AREA.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY AREA_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose an Area...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AREA_CODE"), DT.Rows(i).Item("AREA_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Process(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT MS_PROCESS.PROC_ID,PROC_Code" & vbLf
        SQL &= " FROM MS_PROCESS INNER JOIN MS_PIPE_TAG On MS_PROCESS.PROC_ID=MS_PIPE_TAG.PROC_ID" & vbLf
        SQL &= " WHERE MS_PROCESS.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY PROC_Code" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Process...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PROC_Code"), DT.Rows(i).Item("PROC_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDl_Material(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_Material WHERE active_status='Y' ORDER BY material_name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Material...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("material_name"), DT.Rows(i).Item("material_id"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Material_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_MATERIAL_TYPE WHERE active_status='Y' ORDER BY material_type_name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Material Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("material_type_name"), DT.Rows(i).Item("material_type_id"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Media(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Media WHERE active_status=1 ORDER BY MD_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Media...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Name As String = DT.Rows(i).Item("MD_Code")
            If Not IsDBNull(DT.Rows(i).Item("MD_Name")) AndAlso DT.Rows(i).Item("MD_Name") <> "" Then
                Name &= " : " & DT.Rows(i).Item("MD_Name")
            End If
            Dim Item As New ListItem(Name, DT.Rows(i).Item("MD_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Insulation(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Insulation WHERE active_status=1 ORDER BY IN_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Insulation...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Name As String = DT.Rows(i).Item("IN_Code")
            If Not IsDBNull(DT.Rows(i).Item("IN_Name")) AndAlso DT.Rows(i).Item("IN_Name") <> "" Then
                Name &= " : " & DT.Rows(i).Item("IN_Name")
            End If
            Dim Item As New ListItem(Name, DT.Rows(i).Item("IN_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Component_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Component_Type WHERE active_status=1 ORDER BY Component_Type_Name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Component Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Component_Type_Name"), DT.Rows(i).Item("Component_Type"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_TM_Instrument(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = 0)
        Dim SQL As String = "SELECT * FROM MS_TM_Instrument WHERE active_status=1 ORDER BY INSM_NAME"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Instrument...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("INSM_NAME"), DT.Rows(i).Item("INSM_ID"))
            ddl.Items.Add(Item)
        Next
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub Save_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal Attachment As FileAttachment)
        Dim SQL As String = "SELECT * FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID & " AND UNIQUE_ID='" & Attachment.UNIQUE_ID & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("TAG_ID") = TAG_ID
            DR("POINT_ID") = POINT_ID
            DR("UNIQUE_ID") = Attachment.UNIQUE_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("AFT_ID") = Attachment.DocType
        DR("File_Title") = Attachment.Title
        DR("File_Desc") = Attachment.Description
        DR("File_TAG") = Attachment.Tag
        '--------------- Get Dimension --------
        Dim C As New Converter
        Dim SVG As New SVG_API
        Dim IMG As Image = Nothing
        Dim SVGContent As Byte() = {}
        Select Case Attachment.Extension
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
                 FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF
                IMG = Image.FromStream(C.ByteToStream(Attachment.Content)) '---------------- Prepare Image Content---------------
                SVGContent = C.StringToByte(SVG.ImageToSVG(IMG))'---------------- Prepare SVG Content----------------------------
            Case FileAttachment.ExtensionType.SVG
                IMG = SVG.SVGToImage(C.ByteToString(Attachment.Content)) '---------------- Prepare Image Content---------------
                SVGContent = Attachment.Content '---------------- Prepare SVG Content------------------------------------------
            Case Else
                Exit Sub
        End Select
        DR("File_Width") = IMG.Width
        DR("File_Height") = IMG.Height
        DR("EXT_ID") = Attachment.Extension
        'DR("FILE_ORDER") = xxxxxxxxxxxxxxxxx
        DR("Update_By") = Attachment.LastEditBy
        DR("Update_Time") = Attachment.LastEditTime

        '------------------ Save Physical File--------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG\"
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image\"
        If Not Directory.Exists(SVGPath) Then Directory.CreateDirectory(SVGPath)
        If Not Directory.Exists(ImagePath) Then Directory.CreateDirectory(ImagePath)
        SVGPath &= Attachment.UNIQUE_ID
        ImagePath &= Attachment.UNIQUE_ID
        '---------------- Delete Destination -------------
        If File.Exists(SVGPath) Then
            Try
                DeleteFile(SVGPath)
            Catch : End Try
        End If
        If File.Exists(ImagePath) Then
            Try
                DeleteFile(ImagePath)
            Catch : End Try
        End If
        '---------------- Save SVG -------------------------
        Try
            IMG.Save(ImagePath)
            Dim F As FileStream = File.Open(SVGPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            F.Write(SVGContent, 0, SVGContent.Count)
            F.Close()
            F.Dispose()
        Catch ex As Exception
            Exit Sub
        End Try
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
    End Sub

    Public Sub Save_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal Attachments As List(Of FileAttachment), Optional ByVal RemoveOutboundList As Boolean = False)

        Dim SQL As String = "SELECT * FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim cmd As SqlCommandBuilder

        If RemoveOutboundList Then
            Dim MustRemove As Boolean = False
            For i As Integer = DT.Rows.Count - 1 To 0 Step -1
                Dim UNIQUE_ID As String = DT.Rows(i).Item("UNIQUE_ID")
                Dim Existed As Boolean = False
                For j As Integer = 0 To Attachments.Count - 1
                    If Attachments(j).UNIQUE_ID = UNIQUE_ID Then
                        Existed = True
                        Exit For
                    End If
                Next
                If Not Existed Then
                    Drop_FILE(TAG_ID, POINT_ID, UNIQUE_ID)
                    MustRemove = True
                    DT.Rows(i).Delete()
                End If
            Next
            If MustRemove Then
                cmd = New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
            End If
        End If
        '------------- Save File -----------
        For i As Integer = 0 To Attachments.Count - 1
            Save_FILE(TAG_ID, POINT_ID, Attachments(i))
        Next

    End Sub

    Public Function Get_ReportStep(ByVal ReportType As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_CUI_ERO_Report_Step" & vbLf
        Select Case ReportType
            Case EIR_BL.Report_Type.Pipe_CUI_Reports
                SQL &= "WHERE CUI=1" & vbLf
            Case EIR_BL.Report_Type.Pipe_ERO_Reports
                SQL &= "WHERE ERO=1" & vbLf
        End Select
        SQL &= "ORDER BY RPT_STEP"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_ReportStep_Name(ByVal RPT_Step As Report_Step) As String
        Dim SQL As String = "Select STEP_Name FROM MS_PIPE_CUI_ERO_Report_Step" & vbLf
        SQL &= "WHERE RPT_STEP=" & CInt(RPT_Step) & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Try
            Return DT.Rows(0).Item("STEP_Name")
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Sub BindDDl_ReportStep(ByRef ddl As DropDownList, ByVal ReportType As EIR_BL.Report_Type, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim DT As DataTable = Get_ReportStep(ReportType)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Step...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("STEP_Name"), DT.Rows(i).Item("RPT_STEP"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Function Get_Empty_Measurement_DataTable() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("Point", GetType(String))
        For i As Integer = 1 To 9
            DT.Columns.Add("Sub" & i, GetType(Single))
        Next
        Return DT
    End Function

    Public Function Get_Thickness_Mesaurement_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim DT As DataTable = Get_Empty_Measurement_DataTable()
        Dim DB As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_TM_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DB)

        For p As Integer = Asc("A") To Asc("H")
            Dim DR As DataRow = DT.NewRow
            DR("Point") = Chr(p)
            For i As Integer = 1 To 9
                DB.DefaultView.RowFilter = "Measure_Point='" & Chr(p) & i & "'"
                If DB.DefaultView.Count > 0 AndAlso IsNumeric(DB.DefaultView(0).Item("Thickness")) Then
                    DR("Sub" & i) = DB.DefaultView(0).Item("Thickness")
                End If
            Next
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Public Function Get_Previous_Thickness_Mesaurement_Detail(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal RPT_Date As Date) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_PIPE_TM_Report_Detail " & TAG_ID & "," & POINT_ID & ",'" & RPT_Date.Year & "-" & RPT_Date.Month & "-" & RPT_Date.Day & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Code(ByVal AREA_Code As String, ByVal PROC_Code As String, ByVal Line_No As String, ByVal Size As Double,
                    ByVal Manual_Class As String, ByVal MAT_Code As String, ByVal PRS_Code As String, ByVal Corrosion_Allowance As Double,
                    ByVal MD_Code As String, ByVal IN_Code As String, ByVal IN_Thickness As Integer) As String

        Dim DT As New DataTable
        Dim SQL As String = "Select dbo.UDF_PIPE_Tag_Code('" & AREA_Code.Replace("'", "''") & "','" & PROC_Code.Replace("'", "''") & "','" & Line_No.Replace("'", "''") & "',"
        If Size <= 0 Then
            SQL &= "Null,"
        Else
            SQL &= Size & ","
        End If
        SQL &= "'" & Manual_Class.Replace("'", "''") & "','" & MAT_Code.Replace("'", "''") & "','" & PRS_Code.Replace("'", "''") & "',"
        If Corrosion_Allowance <= 0 Then
            SQL &= "Null,"
        Else
            SQL &= Corrosion_Allowance & ","
        End If
        SQL &= "'" & MD_Code.Replace("'", "''") & "','" & IN_Code.Replace("'", "''") & "',"
        If IN_Thickness <= 0 Then
            SQL &= "Null)"
        Else
            SQL &= IN_Thickness & ")"
        End If

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT.Rows(0)(0)

    End Function

    Public Function Get_Class(ByVal MAT_Code As String, ByVal PRS_Code As String, ByVal Corrosion_Allowance As Double, ByVal MD_Code As String) As String

        Dim DT As New DataTable
        Dim SQL As String = "Select dbo.UDF_PIPE_Class('" & MAT_Code.Replace("'", "''") & "','" & PRS_Code.Replace("'", "''") & "',"
        If Corrosion_Allowance <= 0 Then
            SQL &= "Null,"
        Else
            SQL &= Corrosion_Allowance & ","
        End If
        SQL &= "'" & MD_Code.Replace("'", "''") & "')"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT.Rows(0)(0)

    End Function

    Public Function Get_Tag_Code_By_ID(ByVal TAG_ID As Integer) As String
        Dim DT As New DataTable
        Dim SQL As String = "Select TAG_Code FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("TAG_Code")) Then
            Return DT.Rows(0).Item("TAG_Code")
        Else
            Return ""
        End If
    End Function

    Public Function Get_New_Point_ID(ByVal TAG_ID As Integer) As String
        Dim DT As New DataTable
        Dim SQL As String = "Select ISNULL(MAX(POINT_ID),0)+1 P FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Public Function Get_Point_Param(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM VW_PIPE_POINT WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Point_Param(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM VW_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_FileAttachment_By_ID(ByVal UNIQUE_ID As String) As FileAttachment
        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM MS_PIPE_File WHERE UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Nothing

        '-------------- Check Path----------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG"
        If Not Directory.Exists(SVGPath) Then
            Directory.CreateDirectory(SVGPath)
        End If
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
        If Not Directory.Exists(ImagePath) Then
            Directory.CreateDirectory(ImagePath)
        End If

        Dim C As New Converter
        Dim SVG As New SVG_API
        Dim Attachment As New FileAttachment
        Dim DR As DataRow = DT.Rows(0)
        With Attachment
            .UNIQUE_ID = DR("UNIQUE_ID").ToString
            .Title = DR("File_Title").ToString
            .Description = DR("File_Desc").ToString
            .Tag = DR("File_TAG").ToString
            .Extension = DR("EXT_ID")
            .DocType = DR("AFT_ID")
            Dim Path As String = SVGPath & "\" & DR("UNIQUE_ID").ToString
            If File.Exists(Path) Then
                .Content = C.StringToByte(SVG.GetSVGContent(Path))
            ElseIf File.Exists(ImagePath & "\" & DR("UNIQUE_ID").ToString) Then
                Path = ImagePath & "\" & DR("UNIQUE_ID").ToString
                Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim IMG As Image = Image.FromStream(F)
                .Content = C.StringToByte(SVG.ImageToSVG(IMG))
                F.Dispose()
                IMG.Dispose()
            End If
            .LastEditTime = DR("Update_Time")
            .LastEditBy = DR("Update_By")
        End With
        Return Attachment
    End Function

    Public Function Get_Point_File(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_File WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Point_File(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_File WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Point_FileAttachments(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As List(Of FileAttachment)

        Dim DT As DataTable = Get_Point_File(TAG_ID, POINT_ID)
        Dim Attachments As New List(Of FileAttachment)
        Dim C As New Converter
        Dim SVG As New SVG_API

        '------------- Sort Attachment ----------
        DT = DT.DefaultView.ToTable.Copy
        DT.DefaultView.Sort = ""

        '-------------- Check Path----------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG"
        If Not Directory.Exists(SVGPath) Then
            Directory.CreateDirectory(SVGPath)
        End If
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
        If Not Directory.Exists(ImagePath) Then
            Directory.CreateDirectory(ImagePath)
        End If

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Attachment As New FileAttachment
            With Attachment
                .UNIQUE_ID = DT.Rows(i).Item("UNIQUE_ID").ToString
                .Title = DT.Rows(i).Item("File_Title").ToString
                .Description = DT.Rows(i).Item("File_Desc").ToString
                .Tag = DT.Rows(i).Item("File_TAG").ToString
                .Extension = DT.Rows(i).Item("EXT_ID")
                .DocType = DT.Rows(i).Item("AFT_ID")
                Dim Path As String = SVGPath & "\" & DT.Rows(i).Item("UNIQUE_ID").ToString
                If File.Exists(Path) Then
                    .Content = C.StringToByte(SVG.GetSVGContent(Path))
                ElseIf File.Exists(ImagePath & "\" & DT.Rows(i).Item("UNIQUE_ID").ToString) Then
                    Path = ImagePath & "\" & DT.Rows(i).Item("UNIQUE_ID").ToString
                    Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                    Dim IMG As Image = Image.FromStream(F)
                    .Content = C.StringToByte(SVG.ImageToSVG(IMG))
                    F.Dispose()
                    IMG.Dispose()
                End If
                .LastEditTime = DT.Rows(i).Item("Update_Time")
                .LastEditBy = DT.Rows(i).Item("Update_By")
            End With
            Attachments.Add(Attachment)
        Next
        Return Attachments

    End Function

    Public Function Get_All_Report_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = ""
        SQL &= " DECLARE @RPT_Year AS INT =" & RPT_Year & vbLf
        SQL &= " Declare @RPT_No As INT=" & RPT_No & vbLf
        SQL &= " " & vbLf
        SQL &= " Select RPT.RPT_STEP,STEP_Name,RPT_Date,ReportPage,StartPage,EndPage FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= " Select 1 RPT_STEP,RPT_Date,2 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Before_Remove WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 2 RPT_STEP,RPT_Date,2 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_After_Remove WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 3 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_TM_Header WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 4 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_After_Repair WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 5 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Coating WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 6 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Insulating WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 7 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Final_Report WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " " & vbLf
        SQL &= " ) RPT INNER JOIN MS_PIPE_CUI_ERO_Report_Step STEP ON RPT.RPT_STEP=STEP.RPT_STEP" & vbLf
        SQL &= " ORDER BY RPT.RPT_STEP"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        '------------ Calculate Page For Report-----------
        Dim cur As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            cur += 1
            DT.Rows(i).Item("StartPage") = cur
            For j As Integer = 1 To DT.Rows(i).Item("ReportPage") - 1
                cur += 1
            Next
            DT.Rows(i).Item("EndPage") = cur
        Next

        Return DT
    End Function

    Public Function Get_CUI_ReportHeader_By_Tag(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM RPT_PIPE_CUI_Header WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_ERO_ReportHeader_By_Tag(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM RPT_PIPE_ERO_Header WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub SAVE_RPT_FILE(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal ImageID As Integer, ByRef Attachment As FileAttachment)
        If IsNothing(Attachment) Then Exit Sub

        Dim TableName As String = ""
        Dim SQL As String = "Select TAG_ID,POINT_ID FROM " & vbLf
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                TableName = "RPT_PIPE_Before_Remove"
            Case Report_Step.After_Remove
                TableName = "RPT_PIPE_After_Remove"
            Case Report_Step.Thickness_Measurement
                TableName = "RPT_PIPE_TM_Header"
            Case Report_Step.After_Repair
                TableName = "RPT_PIPE_After_Repair"
            Case Report_Step.Coating_Inspection
                TableName = "RPT_PIPE_Coating"
            Case Report_Step.Insulating_Inspection
                TableName = "RPT_PIPE_Insulating"
            Case Report_Step.Final
                TableName = "RPT_PIPE_Final_Report"
        End Select
        SQL &= TableName & " TB " & vbLf
        SQL &= " INNER JOIN RPT_PIPE_CUI_Header Header On TB.RPT_Year=Header.RPT_Year And TB.RPT_No=Header.RPT_No" & vbLf
        SQL &= " WHERE TB.RPT_Year=" & RPT_Year & " And TB.RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        Dim TAG_ID As Integer = DT.Rows(0).Item("TAG_ID")
        Dim POINT_ID As Integer = DT.Rows(0).Item("POINT_ID")
        Save_FILE(TAG_ID, POINT_ID, Attachment)

        SQL = "UPDATE " & TableName & " Set IMG" & ImageID & "='" & Attachment.UNIQUE_ID.Replace("'", "''") & "'"
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim COM As New SqlCommand
        Dim CON As New SqlConnection(BL.ConnStr)
        CON.Open()
        With COM
            .Connection = CON
            .CommandType = CommandType.Text
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        CON.Close()
        CON.Dispose()
    End Sub



#Region "Drop PIPE POINT FILE"
    Public Sub Drop_TAG(ByVal TAG_ID As Integer)
        '---------- Drop CUI Report-------------------
        Drop_RPT_CUI_Header(TAG_ID)
        '---------- Drop ERO Report-------------------
        Drop_RPT_ERO_Header(TAG_ID)
        '---------- Drop Routine Report---------------
        '---------- Drop Off-Routine Report-----------
        '---------- Drop Measurement Point -----------
        Drop_POINT(TAG_ID)
        '---------- Drop Tag Info --------------------
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub DROP_POINT(ByVal TAG_ID As Integer)
        Drop_FILE(TAG_ID)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub DROP_POINT(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer)
        Drop_FILE(TAG_ID, POINT_ID)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " And POINT_ID=" & POINT_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub Drop_FILE_BY_ID(ByVal UNIQUE_ID As String)
        Dim SQL As String = "SELECT TAG_ID,POINT_ID FROM MS_PIPE_FILE WHERE UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DA.Dispose()
            DT.Dispose()
            Exit Sub
        End If
        Drop_FILE(DT.Rows(0).Item("TAG_ID"), DT.Rows(0).Item("POINT_ID"), UNIQUE_ID)
        DA.Dispose()
        DT.Dispose()
    End Sub

    Public Sub Drop_FILE(ByVal TAG_ID As Integer)
        Dim SQL As String = "SELECT POINT_ID,UNIQUE_ID FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            '-------- Delete File -------------
            Drop_FILE(TAG_ID, DT.Rows(i).Item("POINT_ID"), DT.Rows(i).Item("UNIQUE_ID"))
        Next
        DA.Dispose()
        DT.Dispose()
    End Sub

    Public Sub Drop_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer)
        Dim SQL As String = "SELECT UNIQUE_ID FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            '-------- Delete File -------------
            Drop_FILE(TAG_ID, POINT_ID, DT.Rows(i).Item("UNIQUE_ID").ToString)
        Next
        DA.Dispose()
        DT.Dispose()
    End Sub

    Public Sub Drop_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal UNIQUE_ID As String)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID & " And UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        '-------- Delete File -------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG\" & UNIQUE_ID
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image\" & UNIQUE_ID
        Try
            If File.Exists(SVGPath) Then DeleteFile(SVGPath)
        Catch : End Try
        Try
            If File.Exists(ImagePath) Then DeleteFile(ImagePath)
        Catch : End Try

    End Sub
#End Region

#Region "Drop Report"
    Public Sub Drop_RPT_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal ImageID As Integer)

        Dim SQL As String = "Select * From "
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                SQL &= "RPT_PIPE_Before_Remove" & vbLf
            Case Report_Step.After_Remove
                SQL &= "RPT_PIPE_After_Remove" & vbLf
            Case Report_Step.Thickness_Measurement
                SQL &= "RPT_PIPE_TM_Header" & vbLf
            Case Report_Step.After_Repair
                SQL &= "RPT_PIPE_After_Repair" & vbLf
            Case Report_Step.Coating_Inspection
                SQL &= "RPT_PIPE_Coating" & vbLf
            Case Report_Step.Insulating_Inspection
                SQL &= "RPT_PIPE_Insulating" & vbLf
            Case Report_Step.Final
                SQL &= "RPT_PIPE_Final_Report" & vbLf
        End Select
        SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        If IsDBNull(DT.Rows(0).Item("IMG" & ImageID)) Then Exit Sub
        Dim UNIQUE_ID As String = DT.Rows(0).Item("IMG" & ImageID)
        Try
            Drop_FILE_BY_ID(UNIQUE_ID)
        Catch : End Try
        DT.Rows(0).Item("IMG" & ImageID) = DBNull.Value
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        DT.Dispose()
        DA.Dispose()
    End Sub

    Public Sub Drop_RPT_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step)
        Dim TotalImage As Integer = 1
        Select Case RPT_Step
            Case Report_Step.Thickness_Measurement
                TotalImage = 2
            Case Else
                TotalImage = 4
        End Select
        For i As Integer = 1 To TotalImage
            Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step, i)
        Next
    End Sub

    Public Sub Drop_RPT_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Final)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Insulating_Inspection)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Coating_Inspection)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.After_Repair)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Thickness_Measurement)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.After_Remove)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Before_Remove)

        '---------- Delete Folder-----------
        Dim Path As String = BL.Picture_Path & "\" & RPT_Year & "\" & RPT_No
        If Directory.Exists(Path) Then
            Try
                My.Computer.FileSystem.DeleteDirectory(Path, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch : End Try
        End If
    End Sub

    Public Sub Drop_RPT_By_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step)
        '------------ Drop Image-------------
        Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)
        '------------ Drop From Database ----
        Dim Table As String() = {}
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                BL.PushString(Table, "RPT_PIPE_Before_Remove")
            Case Report_Step.After_Remove
                BL.PushString(Table, "RPT_PIPE_After_Remove")
            Case Report_Step.Thickness_Measurement
                BL.PushString(Table, "RPT_PIPE_TM_Header")
                BL.PushString(Table, "RPT_PIPE_TM_Detail")
            Case Report_Step.After_Repair
                BL.PushString(Table, "RPT_PIPE_After_Repair")
            Case Report_Step.Coating_Inspection
                BL.PushString(Table, "RPT_PIPE_Coating")
            Case Report_Step.Insulating_Inspection
                BL.PushString(Table, "RPT_PIPE_Insulating")
            Case Report_Step.Final
                BL.PushString(Table, "RPT_PIPE_Final_Report")
        End Select

        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = ""
            For i As Integer = 0 To Table.Length - 1
                .CommandText &= "DELETE FROM " & Table(i) & " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf
            Next
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        Table = Nothing
    End Sub

    Public Sub Drop_RPT_TM_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = ""
            .CommandText &= "DELETE FROM RPT_PIPE_TM_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub Drop_RPT_By_Tag(ByVal TAG_ID As Integer, ByVal RPT_Step As Report_Step)
        Dim DT As DataTable = Get_CUI_ReportHeader_By_Tag(TAG_ID)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_By_Step(DT.Rows(i).Item("RPT_Year"), DT.Rows(i).Item("RPT_No"), RPT_Step)
        Next
    End Sub


#Region "CUI_Header"
    Public Sub Drop_RPT_CUI_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Final)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Insulating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Coating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Repair)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Thickness_Measurement)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Remove)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Before_Remove)

        '---------Drop Posted Report File-----------
        Dim Path As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT Result_FileName FROM RPT_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("Result_FileName")) AndAlso DT.Rows(0).Item("Result_FileName") <> "" Then
            Path = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")
            If File.Exists(Path) Then
                Try : My.Computer.FileSystem.DeleteFile(Path) : Catch : End Try
            End If
        End If
        DT.Dispose()
        DA.Dispose()

        '--------- Drop Header------------
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM RPT_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_RPT_CUI_Header(ByVal TAG_ID As Integer)
        Dim DT As DataTable = Get_CUI_ReportHeader_By_Tag(TAG_ID)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_CUI_Header(DT.Rows(i).Item("RPT_Year"), DT.Rows(i).Item("RPT_No"))
        Next
    End Sub

    Public Sub Update_CUI_Header_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal USER_ID As Integer)
        Dim SQL As String = "UPDATE RPT_PIPE_CUI_Header SET" & vbLf
        SQL &= " RPT_Step=" & CInt(RPT_Step) & "," & vbLf
        SQL &= " Update_By=" & USER_ID & "," & vbLf
        SQL &= " Update_Time=GetDate()" & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

#Region "EROSION Header"
    Public Sub Drop_RPT_ERO_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Final)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Insulating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Coating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Repair)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Thickness_Measurement)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Remove)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Before_Remove)

        '---------Drop Posted Report File-----------
        Dim Path As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT Result_FileName FROM RPT_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("Result_FileName")) AndAlso DT.Rows(0).Item("Result_FileName") <> "" Then
            Path = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")
            If File.Exists(Path) Then
                Try : My.Computer.FileSystem.DeleteFile(Path) : Catch : End Try
            End If
        End If
        DT.Dispose()
        DA.Dispose()

        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM RPT_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_RPT_ERO_Header(ByVal TAG_ID As Integer)
        Dim DT As DataTable = Get_ERO_ReportHeader_By_Tag(TAG_ID)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_ERO_Header(DT.Rows(i).Item("RPT_Year"), DT.Rows(i).Item("RPT_No"))
        Next
    End Sub

    Public Sub Update_ERO_Header_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal USER_ID As Integer)
        Dim SQL As String = "UPDATE RPT_PIPE_CUI_Header SET" & vbLf
        SQL &= " RPT_Step=" & CInt(RPT_Step) & "," & vbLf
        SQL &= " Update_By=" & USER_ID & "," & vbLf
        SQL &= " Update_Time=GetDate()" & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

#End Region

End Class
