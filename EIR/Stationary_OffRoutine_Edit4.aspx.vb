﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Stationary_OffRoutine_Edit4
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Off_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Stationary_OffRoutine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_ST_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_ST_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_ST_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")

            BindTabData()

            '---------------Hide Dialog-------------------------
            GL_DialogUploadDocument.CloseDialog()
        End If

        '-------------- Implement Repeater Command--------
        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by otherbody"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Stationary_OffRoutine_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year

        '------------------ GET TAG ID------------------------------
        SQL = "SELECT TOP 1 TAG_ID FROM RPT_ST_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Stationary_OffRoutine_Summary.aspx")
            Exit Sub
        End If
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        lbl_TAG.Text = BL.Get_Tag_Code_Stationary(TAG_ID)
        '------------------------------Header -----------------------------------
        rpt_Doc.DataSource = BL.Get_Stationary_OffRoutine_Document(RPT_Year, RPT_No)
        rpt_Doc.DataBind()
    End Sub

    Protected Sub rpt_Doc_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_Doc.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Select Case e.CommandName
            Case "Edit"
                Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                GL_DialogUploadDocument.UNIQUE_POPUP_ID = UNIQUEKEY
                GL_DialogUploadDocument.ShowDialog(RPT_Year, RPT_No, BL.Get_Tag_Code_Stationary(TAG_ID), e.CommandArgument)
            Case "Delete"
                BL.Drop_Stationary_OffRoutine_Document(RPT_Year, RPT_No, e.CommandArgument)
                BindTabData()
        End Select
    End Sub

    Protected Sub rpt_Doc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_Doc.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim imgType As Image = e.Item.FindControl("imgType")
        Dim lblFileName As Label = e.Item.FindControl("lblFileName")
        Dim lblDetail As Label = e.Item.FindControl("lblDetail")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        'Select Case e.Item.DataItem("DOC_Type")
        '    Case "application/pdf"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_pdf.png"
        '    Case "application/vnd.ms-excel"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_xls.png"
        '    Case "text/plain"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_txt.png"
        '    Case "image/tiff"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_tif.png"
        '    Case "image/jpeg", "image/pjpeg"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_jpg.png"
        '    Case "image/png", "image/x-png"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_png.png"
        '    Case "application/msword"
        '        imgType.ImageUrl = "resources/images/icons/file_extension_doc.png"
        'End Select

        lblFileName.Text = "INSP-" & RPT_Year.ToString.Substring(2, 2) & "-" & RPT_No.ToString.PadLeft(4, "0") & "-" & e.Item.DataItem("DOC_ID").ToString.PadLeft(2, "0")
        'lblDetail.Text = e.Item.DataItem("DOC_Detail")

        btnEdit.CommandArgument = e.Item.DataItem("DOC_ID")
        btnDelete.CommandArgument = e.Item.DataItem("DOC_ID")

    End Sub

#Region "Navigator"

    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Next.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub
#End Region

    Protected Sub lnkUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUpload.Click
        Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
        GL_DialogUploadDocument.UNIQUE_POPUP_ID = UNIQUEKEY
        GL_DialogUploadDocument.ShowDialog(RPT_Year, RPT_No, BL.Get_Tag_Code_Stationary(TAG_ID))
    End Sub

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Try
            BL.Drop_Stationary_OffRoutine_Document(RPT_Year, RPT_No)
            BindTabData()
        Catch ex As Exception
            Exit Sub
        End Try
        BindTabData()
    End Sub

    Protected Sub GL_DialogUploadDocument_UpdateCompleted(ByRef sender As GL_DialogUploadDocument) Handles GL_DialogUploadDocument.UpdateCompleted
        BindTabData()
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

End Class