﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status_AllTag_LO
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("PLANT_ID")) Then
                PLANT_ID = Request.QueryString("PLANT_ID")
            End If
            lblBack.Text = "Back to see all current status for " & lblPlant.Text
            lblBack.PostBackUrl = "Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & PLANT_ID
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SQL As String = " DECLARE @PLANT_ID INT=" & PLANT_ID & vbNewLine
        SQL &= "SELECT TAG_ID,TAG_NO,TAN_RPT_Year,TAN_RPT_No,Ox_RPT_Year,Ox_RPT_No,VANISH_RPT_Year,VANISH_RPT_No,WATER_RPT_Year,WATER_RPT_No," & vbNewLine
        SQL &= "CASE WHEN TAN_Value IS NOT NULL THEN CASE WHEN TAN_Value >= 1.5 THEN 1 ELSE 0 END ELSE NULL END AS TAN_Value," & vbNewLine
        SQL &= "CASE WHEN Ox_Value IS NOT NULL THEN CASE WHEN (Oil_Cat=1 AND Ox_Value>=30) OR (Oil_Cat=2 AND Ox_Value<=30) THEN 1 ELSE 0 END ELSE NULL END AS Ox_Value," & vbNewLine
        SQL &= "CASE WHEN VANISH_Value IS NOT NULL THEN CASE WHEN VANISH_Value>30 THEN 1 ELSE 0 END ELSE NULL END AS VANISH_Value," & vbNewLine
        SQL &= "CASE WHEN WATER_Value IS NOT NULL THEN CASE WHEN (Oil_Cat=1 AND WATER_Value>=750) OR (Oil_Cat=2 AND WATER_Value>=1500) THEN 1 ELSE 0 END ELSE NULL END AS WATER_Value," & vbNewLine
        SQL &= "RPT_CODE,LAST_RPT_Year,LAST_RPT_No" & vbNewLine
        SQL &= "FROM" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG.LO_TAG_ID TAG_ID,LO_TAG_NO TAG_NO,Oil_TYPE_Name,TAG.PLANT_ID,Oil_Cat," & vbNewLine
        SQL &= "	TAN_Value,TAN_RPT_Year,TAN_RPT_No," & vbNewLine
        SQL &= "	OX_Value,OX_RPT_Year,OX_RPT_No," & vbNewLine
        SQL &= "	VANISH_Value,VANISH_RPT_Year,VANISH_RPT_No," & vbNewLine
        SQL &= "	WATER_Value,WATER_RPT_Year,WATER_RPT_No,RPT_CODE,LAST_RPT_Year,LAST_RPT_No" & vbNewLine
        SQL &= "	FROM MS_LO_TAG TAG" & vbNewLine
        SQL &= "	LEFT JOIN MS_LO_Oil_Type ON TAG.Oil_TYPE_ID = MS_LO_Oil_Type.Oil_TYPE_ID" & vbNewLine
        SQL &= "	LEFT JOIN" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG_ID,TAN_RPT_Year,TAN_RPT_No,TAN_Value " & vbNewLine
        SQL &= "		FROM" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY TAN_RPT_Year DESC,TAN_RPT_No DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT LO_TAG_ID TAG_ID,RPT_Year TAN_RPT_Year,RPT_No TAN_RPT_No,TAN_Value " & vbNewLine
        SQL &= "		FROM RPT_LO_Detail WHERE PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "		WHERE TAN_Value IS NOT NULL" & vbNewLine
        SQL &= "		) TB2" & vbNewLine
        SQL &= "		WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "	) TAN_Value" & vbNewLine
        SQL &= "	ON TAG.LO_TAG_ID = TAN_Value.TAG_ID" & vbNewLine
        SQL &= "	LEFT JOIN" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG_ID,OX_RPT_Year,OX_RPT_No,OX_Value " & vbNewLine
        SQL &= "		FROM" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY OX_RPT_Year DESC,OX_RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT LO_TAG_ID TAG_ID,RPT_Year OX_RPT_Year,RPT_No OX_RPT_No,OX_Value " & vbNewLine
        SQL &= "		FROM RPT_LO_Detail WHERE PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "		WHERE OX_Value IS NOT NULL" & vbNewLine
        SQL &= "		) TB2" & vbNewLine
        SQL &= "		WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "	) OX_Value" & vbNewLine
        SQL &= "	ON TAG.LO_TAG_ID = OX_Value.TAG_ID" & vbNewLine
        SQL &= "	LEFT JOIN" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG_ID,VANISH_RPT_Year,VANISH_RPT_No,VANISH_Value " & vbNewLine
        SQL &= "		FROM" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY VANISH_RPT_Year DESC,VANISH_RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT LO_TAG_ID TAG_ID,RPT_Year VANISH_RPT_Year,RPT_No VANISH_RPT_No,VANISH_Value " & vbNewLine
        SQL &= "		FROM RPT_LO_Detail WHERE PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "		WHERE VANISH_Value IS NOT NULL" & vbNewLine
        SQL &= "		) TB2" & vbNewLine
        SQL &= "		WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "	) VANISH_Value" & vbNewLine
        SQL &= "	ON TAG.LO_TAG_ID = VANISH_Value.TAG_ID" & vbNewLine
        SQL &= "	LEFT JOIN" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "	SELECT TAG_ID,WATER_RPT_Year,WATER_RPT_No,WATER_Value " & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY WATER_RPT_Year DESC,WATER_RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT LO_TAG_ID TAG_ID,RPT_Year WATER_RPT_Year,RPT_No WATER_RPT_No,WATER_Value " & vbNewLine
        SQL &= "		FROM RPT_LO_Detail WHERE PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "		WHERE WATER_Value IS NOT NULL" & vbNewLine
        SQL &= "		) TB2" & vbNewLine
        SQL &= "		WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "	) WATER_Value" & vbNewLine
        SQL &= "	ON TAG.LO_TAG_ID = WATER_Value.TAG_ID " & vbNewLine
        SQL &= "	LEFT JOIN" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG_ID,LAST_RPT_Year,LAST_RPT_No,dbo.UDF_RPT_Code(LAST_RPT_Year, LAST_RPT_No) RPT_CODE" & vbNewLine
        SQL &= "		FROM" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY LAST_RPT_Year DESC,LAST_RPT_No DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year LAST_RPT_Year,RPT_No LAST_RPT_No" & vbNewLine
        SQL &= "			FROM RPT_LO_Detail WHERE PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "			) TB1" & vbNewLine
        SQL &= "		) TB2" & vbNewLine
        SQL &= "		WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "	) LAST_RPT" & vbNewLine
        SQL &= "	ON TAG.LO_TAG_ID = LAST_RPT.TAG_ID " & vbNewLine
        SQL &= "	WHERE TAG.Active_Status = 1 AND PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= ") ALL_TAG" & vbNewLine
        SQL &= "WHERE PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "ORDER BY TAG_NO" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        rptData.DataSource = DT
        rptData.DataBind()

        Session("Dashboard_Current_Status_AllTag_LO") = DT
        DisplayChart()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblTAN As HtmlAnchor = e.Item.FindControl("lblTAN")
        Dim lblOx As HtmlAnchor = e.Item.FindControl("lblOx")
        Dim lblVarnish As HtmlAnchor = e.Item.FindControl("lblVarnish")
        Dim lblWater As HtmlAnchor = e.Item.FindControl("lblWater")
        Dim lblLastReport As HtmlAnchor = e.Item.FindControl("lblLastReport")

        lblTag.Text = e.Item.DataItem("TAG_NO").ToString
        If e.Item.DataItem("TAN_Value").ToString <> "" Then
            If e.Item.DataItem("TAN_Value") = 0 Then
                lblTAN.InnerHtml = "Normal"
                lblTAN.Style("color") = "Green"
                lblTAN.Style("cursor") = "pointer"
            Else
                lblTAN.InnerHtml = "Abnormal"
                lblTAN.Style("color") = "Red"
                lblTAN.Style("cursor") = "pointer"
            End If
            lblTAN.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("TAN_RPT_Year") & "&RPT_No=" & e.Item.DataItem("TAN_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Ox_Value").ToString <> "" Then
            If e.Item.DataItem("Ox_Value") = 0 Then
                lblOx.InnerHtml = "Normal"
                lblOx.Style("color") = "Green"
                lblOx.Style("cursor") = "pointer"
            Else
                lblOx.InnerHtml = "Abnormal"
                lblOx.Style("color") = "Red"
                lblOx.Style("cursor") = "pointer"
            End If
            lblOx.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("Ox_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Ox_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("VANISH_Value").ToString <> "" Then
            If e.Item.DataItem("VANISH_Value") = 0 Then
                lblVarnish.InnerHtml = "Normal"
                lblVarnish.Style("color") = "Green"
                lblVarnish.Style("cursor") = "pointer"
            Else
                lblVarnish.InnerHtml = "Abnormal"
                lblVarnish.Style("color") = "Red"
                lblVarnish.Style("cursor") = "pointer"
            End If
            lblVarnish.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("VANISH_RPT_Year") & "&RPT_No=" & e.Item.DataItem("VANISH_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")

        End If
        If e.Item.DataItem("WATER_Value").ToString <> "" Then
            If e.Item.DataItem("WATER_Value") = 0 Then
                lblWater.InnerHtml = "Normal"
                lblWater.Style("color") = "Green"
                lblWater.Style("cursor") = "pointer"
            Else
                lblWater.InnerHtml = "Abnormal"
                lblWater.Style("color") = "Red"
                lblWater.Style("cursor") = "pointer"
            End If
            lblWater.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("WATER_RPT_Year") & "&RPT_No=" & e.Item.DataItem("WATER_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If

        If e.Item.DataItem("RPT_CODE").ToString <> "" Then
            lblLastReport.InnerHtml = e.Item.DataItem("RPT_CODE")
            lblLastReport.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("Last_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Last_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            lblLastReport.Style("cursor") = "pointer"
        End If

    End Sub

    Protected Sub DisplayChart() Handles ddl_ChartType.SelectedIndexChanged

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartLO.Series(0).ChartType
        Select Case ddl_ChartType.Items(ddl_ChartType.SelectedIndex).Value
            Case "Pie"
                ChartType = DataVisualization.Charting.SeriesChartType.Pie
            Case "Doughnut"
                ChartType = DataVisualization.Charting.SeriesChartType.Doughnut
            Case "Funnel"
                ChartType = DataVisualization.Charting.SeriesChartType.Funnel
            Case "Pyramid"
                ChartType = DataVisualization.Charting.SeriesChartType.Pyramid
        End Select
        ChartLO.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Current_Status_AllTag_LO")
        Dim Normal As Double
        Dim Abnormal As Double
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("TAN_Value").ToString = "1" Or DT.Rows(i).Item("OX_Value").ToString = "1" Or DT.Rows(i).Item("VANISH_Value").ToString = "1" Or DT.Rows(i).Item("WATER_Value").ToString = "1" Then
                Abnormal += 1
            Else
                Normal += 1
            End If
        Next

        Dim YValue() As Double = {Normal, Abnormal}
        Dim XValue() As String = {"Normal", "Abnormal"}

        ChartLO.Series("Series1").Points.DataBindY(YValue)
        ChartLO.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
        ChartLO.Series("Series1").Points(1).ToolTip = "Abnormal : " & FormatNumber(Abnormal, 0) & " tag(s)"
        ChartLO.Titles("Title1").Text = ChartLO.Titles("Title1").Text.Replace("xxx", lblPlant.Text)
        ChartLO.Titles("Title2").Text = ChartLO.Titles("Title2").Text.Replace("xxx", DT.Rows.Count.ToString)

        If Normal > 0 Then
            ChartLO.Series("Series1").Points(0).Label = Normal
            ChartLO.Series("Series1").Points(0).LabelForeColor = Drawing.Color.White
        End If
        If Abnormal > 0 Then
            ChartLO.Series("Series1").Points(1).Label = Abnormal
            ChartLO.Series("Series1").Points(0).LabelForeColor = Drawing.Color.White
        End If

        ChartLO.Series("Series1").Points(0).Color = Drawing.Color.Green
        ChartLO.Series("Series1").Points(1).Color = Drawing.Color.Red
    End Sub

End Class