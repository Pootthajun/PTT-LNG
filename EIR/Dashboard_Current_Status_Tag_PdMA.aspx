﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Current_Status_Tag_PdMA.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Tag_PdMA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
    <style type="text/css">
    .ValueMapingTable
    {
    	width:100%;
    	font-size:10px;    	
    	}
    .ValueMapingTable tr td
    {
    	padding:3px;
    	text-align:center;
    	border:1px solid #ccc;
    	}
    
    </style>
</head>
<body style="background-image:none;">
    <form id="form1" runat="server">
<cc1:toolkitscriptmanager ID="ToolkitScriptManager1" runat="server" />
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

<div class="content-box">
				
				<div class="content-box-header">
					<h3 style="margin-top:0"><asp:Label ID="lblRptCode" runat="server"></asp:Label></h3>
				</div>
				
				<div class="content-box-content">
					<p style="font-weight:bold; font-size:16px;">
					<label class="column-left" style="width:120px; font-size:16px;" >Report for: </label>
					     <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
					<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader" ></asp:Label>
					    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
				    </p>
					
                     <asp:Panel ID="pnlData" runat="server">
			                    <label style="width:300px; height:10px; font-size:16px;" >Name Plate Motor : 
                                <asp:Label ID="lblTagNo" runat="server" Text="-" CssClass="EditReportHeader"></asp:Label>
                                </label>
                                 <label style="font-weight:normal; height:10px;"></label>   
				                 <table cellpadding="0" cellspacing="0" style="width:100%;"> 
					                <tr style="height:35px;">
					                    <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Brand
					                    </td>
					                    <td style="border:1px solid #CCCCCC; width:320px; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagBrand" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Voltage (V)
					                    </td>
					                    <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagVoltage" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="width:120px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Frequency (Hz)
					                    <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagFrequency" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td></td>
					                </tr>
					                <tr style="height:35px;">
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Type
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagType" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Current (A)
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagCurrent" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Power Factor
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagPowerFactor" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td></td>
					                </tr>
					                <tr style="height:35px;">
					                    <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        KW
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagKW" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        RPM
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagRPM" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700; padding-left:10px;">
					                        Insulation Class
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; padding-left:10px;">
					                        <asp:Label runat="server" ID="lblTagInsulationClass" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td></td>
					                </tr>
					             </table>
					             <label style="font-weight:normal; height:10px;"></label>
                                 <table cellpadding="0" cellspacing="0"> 
                                    <tr style="height:35px;">
                                        <td style="font-weight:bold; border:1px solid #CCCCCC; width:100px; padding-left:10px;">Mode</td>
                                        <td style="border:1px solid #CCCCCC; width:250px; padding-left:10px;">
                                            <asp:Label ID="lblMode" runat="server" MaxLength="100" Font-Bold="true"
                                                style="border-width:0px; text-align:left; background-color:Transparent;" 
                                                Width="95%"></asp:Label>
                                        </td>
                                        <td style="font-weight:bold; border:1px solid #CCCCCC; width:150px; padding-left:10px;">Inspected Date</td>
                                        <td style="border:1px solid #CCCCCC; width:150px; padding-left:10px;">
                                            <asp:Label ID="lblInspDate" runat="server" MaxLength="100" Font-Bold="true"
                                                style="border-width:0px; text-align:left; background-color:Transparent;" 
                                                Width="95%"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <label style="font-weight:normal; height:30px;"></label>
                                <asp:Panel ID="pnlOnline" runat="server" Width="1000px">
                                    <label style="height:10px; font-size:16px;">
                                    Current Signature Analysis</label>
                                    <label style="font-weight:normal; height:10px;">Evaluating the condition of an induction motor&#39;s squirrel cage rotor</label> 
                                                   <label style="font-weight:normal; height:10px;"></label>                 
                                       <table cellpadding="0" 
                                        cellspacing="0">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                            <td style="text-align:center; font-weight:700; border:1px solid #CCCCCC; width:200px; vertical-align:middle;">
                                                Condition</td>
                                            <td rowspan="11" 
                                                style="width:420px; text-align:center;">
                                                <div>
                                                    <asp:ImageButton ID="imgCurrent1" runat="server" Enabled="false" Height="200px" 
                                                        ImageUrl="resources/images/Sample_40.png" Width="400px" />
                                                    <br>
                                                    <asp:ImageButton ID="imgCurrent2" runat="server" Enabled="false" Height="200px" 
                                                        ImageUrl="resources/images/Sample_40.png" Width="400px" />
                                                    </br>
                                                </div>
                                            </td>
                                            <td rowspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; height:30px; font-weight:bold;">
                                                Fline-Fpole pass<br />
                                                (dB)</td>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; height:30px;">
                                                <asp:Label ID="lblFlineValue" runat="server" Text="-" Font-Bold="True" ForeColor="Black"></asp:Label>
                                            </td>
                                            <td ID="tdFline" runat="server" 
                                                style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblFline" runat="server" Font-Bold="True" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; height:30px; font-weight:bold;">
                                                Swirl Effect</td>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:117px;">
                                                <asp:Label ID="lblSwirlEffectValue" runat="server" Text="-" Font-Bold="True" ForeColor="Black"></asp:Label>
                                            </td>
                                            <td ID="tdSwirlEffect" runat="server" 
                                                style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblSwirlEffect" runat="server" Font-Bold="True" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="8" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:bold; vertical-align:top; padding-top:10px;">
                                                Ref. IEEE Std<br />
                                                43-2000</td>
                                            <td colspan="2" 
                                                style="font-weight:bold; text-align:center; border:1px solid #CCCCCC; height:30px; font-size:14px">
                                                Criteria for analyzing CS</td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                &gt; 60
                                            </td>
                                            <td bgcolor="#00b0f0" 
                                                style="color:Black; text-align:center; border:1px solid #CCCCCC;">
                                                Excellent
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                60 - 54
                                            </td>
                                            <td bgcolor="#00b050" 
                                                style="color:Black; text-align:center; border:1px solid #CCCCCC;">
                                                Good
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                54 - 48
                                            </td>
                                            <td bgcolor="#92d050" 
                                                style="color:Black; text-align:center; border:1px solid #CCCCCC;">
                                                Moderate
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                48 - 42
                                            </td>
                                            <td bgcolor="#ffff00" 
                                                style="color:Black; text-align:center; border:1px solid #CCCCCC;">
                                                Rotor bar crack may be developing or problem with high resistance joints
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                42 - 36
                                            </td>
                                            <td bgcolor="#ffc000" 
                                                style="color:Black; text-align:center; border:1px solid #CCCCCC;">
                                                Rotor bars likely cracked or broken or problems with high resistance joints
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                36 - 30
                                            </td>
                                            <td bgcolor="#ff0000" 
                                                style="color:Black; text-align:center; border:1px solid #CCCCCC;">
                                                Multiple cracked or broken bars at end rings indicated. Also slip ring and joint 
                                                problems.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" style="text-align:center; border:1px solid #CCCCCC;">
                                                &lt; 30
                                            </td>
                                            <td bgcolor="#c00000" 
                                                style="color:White; text-align:center; border:1px solid #CCCCCC;">
                                                Multiple cracked or broken bars at end rings indicated. Severe rotor problems.
                                            </td>
                                        </tr>
                                    </table>
                                    <p>
                                        <label style="font-weight:normal; height:10px;">
                                        </label>
                                        <label style="height:10px; font-size:16px;">
                                        Eccentricity</label>
                                    </p>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:bold; height:35px;">
                                                Peak 1 (∆dB)</td>
                                            <td ID="tdPeak1" runat="server" 
                                                style="border:1px solid #CCCCCC; width:120px; text-align:center;">
                                                <asp:Label ID="lblPeak1Value" runat="server" ForeColor="Black" Text="-" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td>
                                                 </td>
                                            <td rowspan="10" style="width:500px; padding-left:10px; vertical-align:top;">
                                                <div>
                                                    <asp:ImageButton ID="imgEccentricity" runat="server" Height="200px" 
                                                        ImageUrl="resources/images/Sample_40.png" Width="400px" />
                                                </div>
                                            </td>
                                            <td rowspan="10" style="border-left:none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:bold; height:35px;">
                                                Peak 2 (∆dB)</td>
                                            <td ID="tdPeak2" runat="server" 
                                                style="border:1px solid #CCCCCC; width:100px; text-align:center">
                                                <asp:Label ID="lblPeak2Value" runat="server" ForeColor="Black" Text="-" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="border-right:none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:bold; height:35px;">
                                                Peak 3 (∆dB)</td>
                                            <td ID="tdPeak3" runat="server" 
                                                style="border:1px solid #CCCCCC; width:100px; text-align:center;">
                                                <asp:Label ID="lblPeak3Value" runat="server" ForeColor="Black" Text="-" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="border-right:none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:bold; height:35px;">
                                                Peak 4 (∆dB)</td>
                                            <td ID="tdPeak4" runat="server" 
                                                style="border:1px solid #CCCCCC; width:100px; text-align:center;">
                                                <asp:Label ID="lblPeak4Value" runat="server" ForeColor="Black" Text="-" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:bold;">
                                                #Peaks above<br />
                                                noise level</td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblPeakCount" runat="server" style="text-align:center;" Font-Bold="True" ForeColor="Black"></asp:Label>
                                            </td>
                                            <td ID="tdAbove" runat="server" 
                                                style="border:1px solid #CCCCCC; width:100; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblPeaksAbove" runat="server" Font-Bold="True" ForeColor="Black" 
                                                    Text="Good"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="height:35px;">
                                        <td colspan="3" style="font-weight:700; text-align:center; border:1px solid #CCCCCC; vertical-align:middle;">Criteria for analyzing Eccentricity</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">Ref. IEEE Std<br />43-2000</td>
                                        <td style="text-align:center; border:1px solid #CCCCCC;" bgcolor="#DBDBDB">
                                            > 20
                                        </td>
                                        <td style="width:200px; height:30px; color:Black; text-align:center; border:1px solid #CCCCCC;" bgcolor="#ff0000">
                                            Severe
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC;" bgcolor="#DBDBDB">
                                            10 - 20
                                        </td>
                                        <td style="color:Black; height:30px; text-align:center; border:1px solid #CCCCCC;" bgcolor="#ffff00">
                                            Moderate
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC;" bgcolor="#DBDBDB">
                                            0 - 10
                                        </td>
                                        <td style="color:Black; height:30px; text-align:center; border:1px solid #CCCCCC;" bgcolor="#92d050">
                                            Good
                                        </td>
                                    </tr>
                                    </table>
                                    <p>
                                        <label style="font-weight:normal; height:10px;">
                                        </label>
                                        <label style="height:10px; font-size:16px;">
                                        Power Quality     
                                        <asp:TextBox ID="txtPowerQualityStatus" runat="server" 
                                            style="text-align:center;" Width="100px"></asp:TextBox>
                                        </label>
                                        <label style="font-weight:normal; height:5px;">
                                        </label>
                                        <asp:ImageButton ID="imgPowerQuality" runat="server" Enabled="false" 
                                            Height="800px" ImageUrl="resources/images/Sample_40.png" Width="800px" />
                                    </p>
                                </asp:Panel>
                                <asp:Panel ID="pnlOffline" runat="server" Width="1000px">
                                    <label style="height:10px; font-size:16px;">
                                    AC STANDARD TEST</label><label style="font-weight:normal; height:10px;"></label> 
                                      <table cellpadding="0" cellspacing="0">
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:250px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Temperature (°C)
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:150px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblTemperatureValue" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="center" style="text-align:center; border-left:1px solid #CCCCCC; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; font-weight:bold;">Condition</td>
                                            <td colspan="6" align="center" style="text-align:center; border:1px solid #CCCCCC; font-weight:bold;">Limit Value</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td colspan="2" 
                                                style="border:1px solid #CCCCCC; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                <asp:Label ID="lblNote" runat="server" Font-Bold="True" Font-Underline="True" 
                                                    Text="Note"></asp:Label>
                                                 : Stator
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Ohm Ph 1 to 2
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblOhm12Value" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Ohm Ph 1 to 3
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblOhm13Value" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Ohm Ph 2 to 3
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblOhm23Value" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                %Resistive Imbalance
                                            </td>
                                            <td style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblResistiveImbalancevalue" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td ID="tdResistiveImbalance" runat="server" 
                                                style="border:1px solid #CCCCCC; text-align:center; width:150px;">
                                                <asp:Label ID="lblResistiveImbalance" runat="server" Font-Bold="True" 
                                                    ForeColor="Black"></asp:Label>
                                            </td>
                                            <td colspan="6" rowspan="3" style="text-align:left; vertical-align:top; border-right:1px solid #CCCCCC;"">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="ValueMapingTable">
                                              <tr>
                                                <td colspan="6" style="text-align:center;"><b>% Resistive Imbalance</b></td>
                                              </tr>
                                              <tr>
                                                <td>Voltage &lt;= 600</td>
                                                <td>>= 4</td>
                                                <td class="PDMA_Eccentricity_Severe">Severe</td>
                                                <td>Voltage > 600</td>
                                                <td>>= 3</td>
                                                <td class="PDMA_Eccentricity_Severe">Severe</td>
                                              </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                                <td>>= 3</td>
                                                <td class="PDMA_PI_Caution">Coution</td>
                                                <td>&nbsp;</td>
                                                <td>>= 2</td>
                                                <td class="PDMA_PI_Caution">Coution</td>
                                              </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                                <td>< 3</td>
                                                <td class="PDMA_PI_Good">Good</td>
                                                <td>&nbsp;</td>
                                                <td>< 2</td>
                                                <td class="PDMA_PI_Good">Good</td>
                                              </tr>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                mH Ph 1 to 2
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblmH12Value" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                mH Ph 1 to 3
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblmH13Value" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                mH Ph 2 to 3
                                            </td>
                                            <td style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblmH23Value" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                            <td colspan="6" rowspan="5"  style="text-align:left; vertical-align:bottom; border-right:1px solid #CCCCCC;"">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="ValueMapingTable">
                                              <tr>
                                                <td colspan="6" align="center"><b>%Inductive Imbalance</b></td>
                                              </tr>
                                              <tr>
                                                <td>Voltage &lt;= 600</td>
                                                <td>&gt;= 12</td>
                                                <td class="PDMA_Eccentricity_Severe">Severe</td>
                                                <td>Voltage &gt; 600</td>
                                                <td>&gt;= 7</td>
                                                <td class="PDMA_Eccentricity_Severe">Severe</td>
                                              </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                                <td>&gt;= 8</td>
                                                <td class="PDMA_PI_Caution">Coution</td>
                                                <td>&nbsp;</td>
                                                <td>&gt;= 5</td>
                                                <td class="PDMA_PI_Caution">Coution</td>
                                              </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                                <td>&lt; 8</td>
                                                <td class="PDMA_PI_Good">Good</td>
                                                <td>&nbsp;</td>
                                                <td>&lt; 5</td>
                                                <td class="PDMA_PI_Good">Good</td>
                                              </tr>
                                              <tr>
                                                <td colspan="6" align="center"><b>Resistance to Ground (M&#937;)</b></td>
                                              </tr>
                                              <tr>
                                                <td>Voltage &lt;= 600</td>
                                                <td>&lt;= 5</td>
                                                <td class="PDMA_Eccentricity_Severe">Severe</td>
                                                <td>Voltage &gt; 600</td>
                                                <td>&lt;= 100</td>
                                                <td class="PDMA_Eccentricity_Severe">Severe</td>
                                              </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                                <td>&gt; 5</td>
                                                <td class="PDMA_PI_Good">Good</td>
                                                <td>&nbsp;</td>
                                                <td>&gt; 100</td>
                                                <td class="PDMA_PI_Good">Good</td>
                                              </tr>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Average Inductance
                                            </td>
                                            <td style="border:1px solid #CCCCCC; width:100px; text-align:center;">
                                                <asp:Label ID="lblAverageInductance" runat="server" style="text-align:center;" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                %Inductive Imbalance
                                            </td>
                                            <td style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblInductiveImbalanceValue" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td ID="tdInductiveImbalance" runat="server" 
                                                style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblInductiveImbalance" runat="server" Font-Bold="True" 
                                                    ForeColor="Black"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Capacitance to Ground (pF)
                                            </td>
                                            <td style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblCapacitancevalue" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td ID="tdCapacitance" runat="server" 
                                                style="border:1px solid #CCCCCC; text-align:center;">
                                                <asp:Label ID="lblCapacitance" runat="server" Font-Bold="True" 
                                                    ForeColor="Black" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="height:35px;">
                                            <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                                Resistance to Ground (MΩ)
                                            </td>
                                            <td style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblResistanceValue" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td ID="tdResistance" runat="server" 
                                                style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;">
                                                <asp:Label ID="lblResistance" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <p>
                                        <label style="font-weight:normal; height:10px;">
                                        </label>
                                        <label style="height:10px; font-size:16px;">
                                        POLARIZATION INDEX</label>
                                        <label style="font-weight:normal; height:10px;">
                                        Addressing the insulation fault zone</label>
                                    </p>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width:100px;">
                                            </td>
                                            <td style="width:100px;">
                                            </td>
                                            <td style="text-align:center; border:1px solid #CCCCCC; height:30px; width:120px; font-weight:bold;">
                                                Condition
                                            </td>
                                            <td rowspan="9">
                                                <div style="padding-left:40px;">
                                                    <asp:ImageButton ID="imgPolarization" runat="server" Height="200px" 
                                                        ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" 
                                                        ToolTip="Click to edit picture" Width="400px" />
                                                    <asp:Button ID="btnSaveImagePolarization" runat="server" Height="0px" 
                                                        style="visibility:hidden; position:absolute;" Width="0px" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-weight:bold; height:30px;">
                                                PI Value
                                            </td>
                                            <td style="text-align:center; border:1px solid #CCCCCC; text-align:center; ">
                                                <asp:Label ID="lblPI_ValueValue" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td ID="tdPI_Value" runat="server" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px;">
                                                <asp:Label ID="lblPI_Value" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="7" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; vertical-align:top;">
                                                Ref. IEEE Std 43-2000
                                            </td>
                                            <td colspan="2" 
                                                style="text-align:center; border:1px solid #CCCCCC; height:35px;">
                                                Criteria for analyzing PI
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; height:30px;">
                                                &gt; 7
                                            </td>
                                            <td bgcolor="#ff0000" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black;">
                                                *Severe
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; height:30px;">
                                                &gt; 5
                                            </td>
                                            <td bgcolor="#ffc000" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black;">
                                                *Caution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; height:30px;">
                                                &gt; 2 - 5
                                            </td>
                                            <td bgcolor="#92d050" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black;">
                                                Good
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; height:30px;">
                                                1.5 - 2.0
                                            </td>
                                            <td bgcolor="#ffff00" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black;">
                                                Observe
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; height:30px;">
                                                1.0 - 1.5
                                            </td>
                                            <td bgcolor="#ffc000" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black;">
                                                Caution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#DBDBDB" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:100px; height:30px;">
                                                &lt; 1
                                            </td>
                                            <td bgcolor="#ff0000" 
                                                style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black;">
                                                Severe
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" 
                                                style="border:1px solid #CCCCCC; padding:3px;">
                                                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Underline="True" 
                                                    Text="Note"></asp:Label>
                                                 : If the 1 min RTG is &gt; 5 Gohms, the
                                               
                                                calculated PI ratio may not be meaningful.<br />
                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" 
                                                    Text="* Interpreting value with a graph shape."></asp:Label>
                                            </td>
                                            <td>
                                                 </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                             </asp:Panel>
				</div>         
	              
</div>

</ContentTemplate>
</asp:UpdatePanel>
    </form>
</body>
</html>
