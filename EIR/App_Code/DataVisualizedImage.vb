﻿Imports System.Drawing
Imports System.Web.UI.DataVisualization
Imports System.Web.UI.WebControls
Imports System
Imports System.DateTime
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class DataVisualizedImage

    Public Function Get_LubeOil_RadarImage(ByVal Detail_ID As Integer) As Byte()
        Dim Radar As New Charting.Chart
        With Radar
            .Width = Unit.Pixel(300)
            .Height = Unit.Pixel(250)
            .Series.Add("Series1")
            .Series("Series1").ChartType = Charting.SeriesChartType.Radar
            .Series("Series1").YValuesPerPoint = 5


            .ChartAreas.Add("Area1")
            With .ChartAreas("Area1")
                .BackColor = Color.White

                .AxisY.Maximum = 100
                .AxisY.LineColor = Color.Silver
                .AxisY.MajorGrid.LineColor = Color.Silver
                .AxisY.MajorTickMark.Enabled = False
                .AxisY.LabelStyle.Enabled = False
                .AxisY.ScaleBreakStyle.LineColor = Color.Transparent
                '.AxisY.ScaleBreakStyle.LineWidth = 0

                .AxisX2.MajorTickMark.Enabled = False
                .AxisX2.LineColor = Color.Transparent
                '.AxisX2.LineWidth = 0


                .AxisY2.MajorGrid.LineColor = Color.Transparent
                '.AxisY2.MajorGrid.LineWidth = 0

            End With
        End With

        Dim BL As New EIR_BL
        Dim Sql As String = "EXEC dbo.sp_LO_RADAR_PARAM " & Detail_ID
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        With Radar.Series(0)
            .Points.Clear()
            For i As Integer = 0 To DT.Rows.Count - 1
                .Points.AddXY("", DT.Rows(i).Item("COND_RATIO"))
                .Points(.Points.Count - 1).Label = DT.Rows(i).Item("COND_NAME").ToString.Replace("_", " ")
                .Points(.Points.Count - 1).Color = Drawing.Color.LightSalmon
                .Points(.Points.Count - 1).BackSecondaryColor = Drawing.Color.Red
                .Points(.Points.Count - 1).BackGradientStyle = Charting.GradientStyle.LeftRight
                .Points(.Points.Count - 1).LabelForeColor = Drawing.Color.Gray
            Next
        End With

        Dim ServerMapPath As String = ConfigurationManager.AppSettings("ServerMapPath").ToString
        Dim ImagePath As String = ServerMapPath & "\Temp\" & Detail_ID & "_" & Now.ToOADate.ToString.Replace(".", "") & ".png"
        Radar.SaveImage(ImagePath)
        Dim F As IO.FileStream = File.Open(ImagePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)

        Dim C As New Converter
        Dim B As Byte() = C.StreamToByte(F)
        F.Close()
        DT.Dispose()
        '------------- Delete File ------------
        Try : My.Computer.FileSystem.DeleteFile(ImagePath) : Catch : End Try

        Return B
    End Function

End Class
