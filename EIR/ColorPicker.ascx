﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ColorPicker.ascx.vb" Inherits="EIR.ColorPicker" %>

<asp:Panel ID="pnl" runat="server" Width="150px" Height="20px" Style="overflow:hidden; border:1px solid #CECECE;">

<table style="width:100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="20" style="width:100%">
        <asp:Button ID="btnResult" runat="server" BorderStyle="None" BackColor="White" Text="" Height="20px" style="cursor:pointer" /></td>
  </tr>  
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnRed" runat="server" BorderStyle="None" BackColor="Red" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnGreen" runat="server" BorderStyle="None" BackColor="Green" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnBlue" runat="server" BorderStyle="None" BackColor="Blue" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnYellow" runat="server" BorderStyle="None" BackColor="Yellow" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnViolet" runat="server" BorderStyle="None" BackColor="Violet" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnWhite" runat="server" BorderStyle="None" BackColor="White" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
  <tr>
    <td height="20" style="width:100%"><asp:Button ID="btnBlack" runat="server" BorderStyle="None" BackColor="Black" Text="" Width="100%" Height="20px" style="cursor:pointer" /></td>
  </tr>
</table>

</asp:Panel>
