﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class UC_Dashboard_Total_Problem_Area_Report_Plant
    Inherits System.Web.UI.UserControl
    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL
    Dim CV As New Converter

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type)
        Dim DT_Dashboard As New DataTable

        If Year_F > 2500 Then
            Year_F = Year_F - 543
        End If
        If Year_T > 2500 Then
            Year_T = Year_T - 543
        End If

        lblMONTH_F.Text = Month_F
        lblMONTH_T.Text = Month_T
        lblYEAR_F.Text = Year_F
        lblYEAR_T.Text = Year_T
        lblEQUIPMENT.Text = Equipment
        lblPlantID.Text = Plant_ID

        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & lblPlantID.Text
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblPlantName.Text = DT.Rows(0).Item("PLANT_Name").ToString
        End If

        Dim DateFrom As Date = CV.StringToDate(Year_F.ToString & "-" & Month_F.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year_T.ToString & "-" & Month_T.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)

        If Equipment = EIR_BL.Report_Type.All Then
            DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.All, EIR_BL.InspectionLevel.All, True)
        Else
            DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate)
        End If

        Dim col() As String = {"RPT_TYPE_ID", "AREA_ID", "AREA_Name"}
        Dim DT_DATA As DataTable = DT_Dashboard.DefaultView.ToTable(True, col)
        DT_DATA.Columns.Add("ClassC")
        DT_DATA.Columns.Add("ClassB")
        DT_DATA.Columns.Add("ClassA")

        For i As Integer = 0 To DT_DATA.Rows.Count - 1
            DT_DATA.Rows(i).Item("ClassA") = DT_Dashboard.Compute("COUNT(TAG_ID)", "RPT_TYPE_ID = " & DT_DATA.Rows(i).Item("RPT_TYPE_ID").ToString & " AND AREA_ID = " & DT_DATA.Rows(i).Item("AREA_ID").ToString & " AND ICLS_ID=3")
            DT_DATA.Rows(i).Item("ClassB") = DT_Dashboard.Compute("COUNT(TAG_ID)", "RPT_TYPE_ID = " & DT_DATA.Rows(i).Item("RPT_TYPE_ID").ToString & " AND AREA_ID = " & DT_DATA.Rows(i).Item("AREA_ID").ToString & " AND ICLS_ID=2")
            DT_DATA.Rows(i).Item("ClassC") = DT_Dashboard.Compute("COUNT(TAG_ID)", "RPT_TYPE_ID = " & DT_DATA.Rows(i).Item("RPT_TYPE_ID").ToString & " AND AREA_ID = " & DT_DATA.Rows(i).Item("AREA_ID").ToString & " AND ICLS_ID=1")
        Next
        DT_DATA.DefaultView.Sort = "RPT_TYPE_ID,AREA_Name"
        DT_DATA = DT_DATA.DefaultView.ToTable
        Session("Dashboard_Total_Problem_Area_Report_Plant") = DT_DATA

        If DT_DATA.Rows.Count > 6 Then
            Dim NewWidth As Unit = Unit.Pixel(60 * DT_DATA.Rows.Count)
            ChartMain.Width = NewWidth
        End If

        DisplayChart(ChartMain, New EventArgs, Plant_ID, Month_F, Month_T, Year_F, Year_T, Equipment)

        rptData.DataSource = DT_DATA
        rptData.DataBind()

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type)
        Dim DisplayText As String = ""
        Dim DT As New DataTable

        If Year_F < 2500 Then
            Year_F = Year_F + 543
        End If
        If Year_T < 2500 Then
            Year_T = Year_T + 543
        End If

        If Year_F = Year_T Then
            If Month_F = Month_T Then
                DisplayText = "on  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F
            Else
                DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_F
            End If
        Else
            DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_T
        End If

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)

        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy

        'If Equipment = EIR_BL.Report_Type.All Then
        '    ChartMain.Titles("Title1").Text = "Total Problem by Area for Stationary & Rotating" & vbNewLine & vbNewLine & DisplayText & "  on  " & lblPlantName.Text
        'Else
        ChartMain.Titles("Title1").Text = "Total Problem by Area for  " & Dashboard.FindEquipmentName(Equipment) & vbNewLine & vbNewLine & DisplayText & "  on  " & lblPlantName.Text
        'End If

        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter

        ChartMain.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain.ChartAreas("ChartArea1").AxisY.Title = "Problem(s)"
        ChartMain.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver

        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series2").Points.Clear()
        ChartMain.Series("Series3").Points.Clear()
        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        ChartMain.Series("Series2").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        ChartMain.Series("Series3").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        DT = New DataTable
        DT = Session("Dashboard_Total_Problem_Area_Report_Plant")

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Url As String = ""
            Select Case Equipment
                Case EIR_BL.Report_Type.Stationary_Routine_Report
                    Url = "Dashboard_Total_Problem_Area_Report_ST.aspx?PLANT_ID=" & lblPlantID.Text & "&AREA_ID=" & DT.Rows(i).Item("AREA_ID").ToString & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text
                Case EIR_BL.Report_Type.Rotating_Routine_Report
                    Url = "Dashboard_Total_Problem_Area_Report_RO.aspx?PLANT_ID=" & lblPlantID.Text & "&AREA_ID=" & DT.Rows(i).Item("AREA_ID").ToString & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text
            End Select

            ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("AREA_Name").ToString, DT.Rows(i).Item("ClassC"))
            ChartMain.Series("Series2").Points.AddXY(DT.Rows(i).Item("AREA_Name").ToString, DT.Rows(i).Item("ClassB"))
            ChartMain.Series("Series3").Points.AddXY(DT.Rows(i).Item("AREA_Name").ToString, DT.Rows(i).Item("ClassA"))

            Dim Tooltip_ClassC As String = "ClassC : " & DT.Rows(i).Item("ClassC").ToString & " Tag(s)"
            Dim Tooltip_ClassB As String = "ClassB : " & DT.Rows(i).Item("ClassB").ToString & " Tag(s)"
            Dim Tooltip_ClassA As String = "ClassA : " & DT.Rows(i).Item("ClassA").ToString & " Tag(s)"

            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip_ClassC
            ChartMain.Series("Series2").Points(i).ToolTip = Tooltip_ClassB
            ChartMain.Series("Series3").Points(i).ToolTip = Tooltip_ClassA
            ChartMain.Series("Series1").Points(i).Url = Url
            ChartMain.Series("Series2").Points(i).Url = Url
            ChartMain.Series("Series3").Points(i).Url = Url
        Next

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblEquipment As Label = e.Item.FindControl("lblEquipment")
        Dim lblArea As Label = e.Item.FindControl("lblArea")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")

        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0

        lblEquipment.Text = Dashboard.FindEquipmentName(e.Item.DataItem("RPT_TYPE_ID"))
        lblArea.Text = e.Item.DataItem("AREA_Name")

        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If

        Select Case e.Item.DataItem("RPT_TYPE_ID")
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Total_Problem_Area_Report_ST.aspx?PLANT_ID=" & lblPlantID.Text & "&AREA_ID=" & e.Item.DataItem("AREA_ID") & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "';"
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Total_Problem_Area_Report_RO.aspx?PLANT_ID=" & lblPlantID.Text & "&AREA_ID=" & e.Item.DataItem("AREA_ID") & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "';"
        End Select

    End Sub

End Class