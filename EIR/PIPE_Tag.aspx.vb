﻿Option Strict Off
Imports System.Data.SqlClient
Imports EIR

Public Class PIPE_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE
    Dim C As New Converter

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property TAG_ID As Integer
        Get
            Try
                Return ViewState("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
            ImplementJavascriptControl()
        End If

        StoreJS()
    End Sub

    Private Sub StoreJS()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJSPageLoad1", "resizeTabPoint();", True)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJSPageLoad2", "$(document).ready(resizeTabPoint); window.onresize = resizeTabPoint;", True)
    End Sub

#Region "Validator"

    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
        pnlBindingPointError.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnBindingPointError_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingPointError.Click
        pnlBindingPointError.Visible = False
    End Sub


#End Region

    Private Sub ImplementJavascriptControl()
        ImplementJavaNumericText(txt_Search_Size, "Center")
        ImplementJavaNumericText(txt_Search_CA, "Center")
        ImplementJavaNumericText(txt_search_Insulation_Thickness, "Center")
    End Sub

    Private Sub BindTag()

        Dim SQL As String = ""
        SQL &= " Select TAG_ID,TAG_Code,PIPE_Class,PLANT_ID,PLANT_Code,AREA_ID,AREA_Code" & vbLf
        SQL &= " ,PROC_ID,PROC_Code,Line_No,Manual_Class,material_id,material_name,PRS_ID,PRS_Code" & vbLf
        SQL &= " ,Corrosion_Allowance,MD_ID,MD_Code,IN_ID,IN_Code,IN_Thickness,Active_Status" & vbLf
        SQL &= " ,Component_Type,Component_Type_Name,Size" & vbLf
        SQL &= " ,SERVICE_ID,SERVICE_Name,Initial_Year,Total_Point,Loop_No" & vbLf
        SQL &= " FROM VW_PIPE_TAG" & vbLf

        Dim WHERE As String = ""

        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Process.SelectedIndex > 0 Then
            WHERE &= " PROC_ID=" & ddl_Search_Process.Items(ddl_Search_Process.SelectedIndex).Value & " And "
        End If
        If txt_Search_LineNo.Text <> "" Then
            WHERE &= " Line_No Like '%" & txt_Search_LineNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_Search_Size.Text <> "" Then
            WHERE &= " Size = " & txt_Search_Size.Text.Replace(",", "") & " AND "
        End If
        If ddl_Search_Material.SelectedIndex > 0 Then
            WHERE &= " material_id=" & ddl_Search_Material.Items(ddl_Search_Material.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Pressure.SelectedIndex > 0 Then
            WHERE &= " PRS_ID=" & ddl_Search_Pressure.Items(ddl_Search_Pressure.SelectedIndex).Value & " And "
        End If
        If txt_Search_CA.Text <> "" Then
            WHERE &= " Corrosion_Allowance = " & txt_Search_CA.Text.Replace(",", "") & " AND "
        End If
        If ddl_Search_Service.SelectedIndex > 0 Then
            WHERE &= " SERVICE_ID=" & ddl_Search_Service.Items(ddl_Search_Service.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Insulation.SelectedIndex > 0 Then
            WHERE &= " IN_ID=" & ddl_Search_Insulation.Items(ddl_Search_Insulation.SelectedIndex).Value & " And "
        End If
        If txt_search_Insulation_Thickness.Text <> "" Then
            WHERE &= " IN_Thickness = " & txt_search_Insulation_Thickness.Text.Replace(",", "") & " AND "
        End If
        If txt_Search_Code.Text <> "" Then
            WHERE &= " (TAG_Code Like '%" & txt_Search_Code.Text.Replace("'", "''") & "%') AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "   ORDER BY TAG_Code" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("PIPE_Tag") = DT

        pnlSearch.Visible = True
        Navigation.SesssionSourceName = "PIPE_Tag"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Private Sub rptTag_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblMaterial As Label = e.Item.FindControl("lblMaterial")
        Dim lblPoint As Label = e.Item.FindControl("lblPoint")
        Dim lblService As Label = e.Item.FindControl("lblService")
        Dim lblLoopNo As Label = e.Item.FindControl("lblLoopNo")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblCode.Text = e.Item.DataItem("TAG_Code").ToString
        lblPlant.Text = e.Item.DataItem("PLANT_Code").ToString
        lblService.Text = e.Item.DataItem("Total_Point").ToString
        lblMaterial.Text = e.Item.DataItem("material_name").ToString
        lblService.Text = e.Item.DataItem("SERVICE_Name").ToString
        lblLoopNo.Text = e.Item.DataItem("Loop_No").ToString
        If e.Item.DataItem("Total_Point") = 0 Then
            lblPoint.Text = "-"
        Else
            lblPoint.Text = FormatNumber(e.Item.DataItem("Total_Point"), 0)
        End If


        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

    End Sub

    Private Sub rptTag_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        TAG_ID = btnEdit.Attributes("TAG_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                TAG_ID = btnEdit.Attributes("TAG_ID")

                '------------ Bind Tag Data-----------
                Pipe_Info.BindData(TAG_ID)
                If Pipe_Info.TAG_ID = 0 Then
                    lblBindingError.Text = "TAG Not found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If
                lblTagCode.Text = Pipe_Info.TAG_CODE
                '------------ Bind Tag File-----------
                'Pipe_File.UNIQUE_ID = GenerateNewUniqueID() '------------ Already Set at ClearPanelEdit------
                '-------- เก็บ List File ใส่ Session ---------
                Dim TagFiles As List(Of FileAttachment) = PIPE.Get_Point_FileAttachments(TAG_ID, 0)
                For i As Integer = 0 To TagFiles.Count - 1
                    Session(TagFiles(i).UNIQUE_ID) = TagFiles(i)
                Next
                Session(Pipe_File.UNIQUE_ID) = TagFiles
                Pipe_File.Attachments = TagFiles
                Pipe_File.BindData()

                '------------ Bind Point Data------------------
                Dim PT As DataTable = PIPE.Get_Point_Param(TAG_ID)
                PT.Columns.Add("UNIQUE_ID", GetType(String)) '----------- เก็บ Unique_ID ของ Point -----------
                PT.DefaultView.RowFilter = "POINT_ID<>0"
                PT = PT.DefaultView.ToTable
                PT.DefaultView.RowFilter = ""

                '------------ Generate New Point Signature And  Bind Files-----
                For i As Integer = 0 To PT.Rows.Count - 1
                    Dim UNIQUE_ID As String = GenerateNewUniqueID()
                    PT.Rows(i).Item("UNIQUE_ID") = UNIQUE_ID '----------- Named Point ---------
                    '-------- Get List Point Files-------------
                    Dim PointFiles As List(Of FileAttachment) = PIPE.Get_Point_FileAttachments(TAG_ID, PT.Rows(i).Item("POINT_ID"))
                    '-------- เก็บ List File ใส่ Session ---------
                    Session(UNIQUE_ID) = PointFiles '----------- Collect Point File Collection ---------
                    For j As Integer = 0 To PointFiles.Count - 1
                        Session(PointFiles(j).UNIQUE_ID) = PointFiles(j)
                    Next
                Next
                '------------ Bind All Point Information ------
                rptPoint.DataSource = PT
                rptPoint.DataBind()

                Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
                chkAvailable.Checked = btnToggle.ImageUrl = "resources/images/icons/tick.png"

                pnlEdit.Visible = True
                btnCreate.Visible = False
                pnlSearch.Visible = False
                lblUpdateMode.Text = "Update"

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE MS_PIPE_TAG Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

            Case "Delete"

                Dim TotalReport = PIPE.Get_CUI_ReportHeader_By_Tag(TAG_ID).Rows.Count
                TotalReport += PIPE.Get_ERO_ReportHeader_By_Tag(TAG_ID).Rows.Count
                If TotalReport > 0 Then
                    lblBindingError.Text = "This Tag is already existing in some inspection report. Unable to delete this tag"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Try
                    PIPE.Drop_TAG(TAG_ID)
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try


                Dim PageIndex As Integer = Navigation.CurrentPage
                BindTag()
                Navigation.CurrentPage = PageIndex

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True


        End Select
    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
    End Sub

    Private Sub ClearPanelEdit()

        '------------ Tag Info -------------
        Pipe_Info.BindData(0)
        '------------ Tag File -------------
        Pipe_File.UNIQUE_ID = GenerateNewUniqueID()
        Pipe_File.Attachments = New List(Of FileAttachment)
        Pipe_File.BindData()
        '------------ Point Info ------------
        Dim PointData As DataTable = BlankPointDataTable()
        rptPoint.DataSource = PointData
        rptPoint.DataBind()
        '------------ Change Tab -----------
        ChangeTab(MyTab.Tag)

        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True

        '-------------Point Data------------
        ClearPanelEditPoint()
    End Sub


    Private Function PointData() As DataTable
        Dim DT As DataTable = BlankPointDataTable()
        For Each item As RepeaterItem In rptPoint.Items
            If item.ItemType <> ListItemType.Item And item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim trPoint As HtmlTableRow = item.FindControl("trPoint")
            Dim DR As DataRow = DT.NewRow
            For i As Integer = 0 To DT.Columns.Count - 1
                Dim Param As String = DT.Columns(i).ColumnName
                Select Case DT.Columns(i).DataType
                    Case GetType(String)
                        DR(Param) = trPoint.Attributes(Param)
                    Case GetType(Integer)
                        If IsNothing(trPoint.Attributes(Param)) OrElse trPoint.Attributes(Param) = "" Then
                            DR(Param) = DBNull.Value
                        Else
                            DR(Param) = CInt(trPoint.Attributes(Param))
                        End If
                    Case GetType(Double)
                        If IsNothing(trPoint.Attributes(Param)) OrElse trPoint.Attributes(Param) = "" Then
                            DR(Param) = DBNull.Value
                        Else
                            DR(Param) = CDbl(trPoint.Attributes(Param))
                        End If
                End Select
            Next
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDlArea(ddl_Search_Area)
        BL.BindDDlProcess(ddl_Search_Process)
        txt_Search_LineNo.Text = ""
        txt_Search_Size.Text = ""
        PIPE.BindDDl_Material(ddl_Search_Material)
        PIPE.BindDDl_Pressure_Code(ddl_Search_Pressure)
        txt_Search_CA.Text = ""
        PIPE.BindDDl_Service(ddl_Search_Service)
        PIPE.BindDDl_Insulation_Code(ddl_Search_Insulation)
        txt_search_Insulation_Thickness.Text = ""

        txt_Search_Code.Text = ""
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Search_Changed(sender As Object, e As EventArgs) Handles ddl_Search_Area.SelectedIndexChanged, ddl_Search_Process.SelectedIndexChanged,
        ddl_Search_Pressure.SelectedIndexChanged, ddl_Search_Service.SelectedIndexChanged, ddl_Search_Material.SelectedIndexChanged, ddl_Search_Insulation.SelectedIndexChanged,
        txt_Search_CA.TextChanged, txt_search_Insulation_Thickness.TextChanged, txt_Search_LineNo.TextChanged, txt_Search_Size.TextChanged, txt_Search_Code.TextChanged
        BindTag()
    End Sub

    Private Sub ddl_Search_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        Dim PLANT_ID As Integer = ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value
        Dim AREA_ID As Integer = ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value
        If PLANT_ID = 0 Then
            BL.BindDDlArea(ddl_Search_Area, AREA_ID)
        Else
            BL.BindDDlArea(PLANT_ID, ddl_Search_Area, AREA_ID)
        End If
        BindTag()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        lblUpdateMode.Text = "Create"
        '-----------------------------------
        pnlSearch.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        '----------- Save TAG ------------
        If Pipe_Info.ValidateIncompleteMessage <> "" Then
            '---------- Change To Tab Tag------------
            HTabTag_Click(Nothing, Nothing)
            Exit Sub
        End If

        Try
            TAG_ID = Pipe_Info.SaveData()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        Dim SQL As String = "SELECT * FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.Rows(0)
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        '----------- Remove Unused Point---------------
        Dim PT As DataTable = PointData()
        PT.DefaultView.RowFilter = "POINT_ID>0" '---------- Except PointID IN -1(New Point), 0(Main) -------------
        PT = PT.DefaultView.ToTable.Copy
        Dim PointDB As DataTable = PIPE.Get_Point_Param(TAG_ID)
        PointDB.DefaultView.RowFilter = "POINT_ID>0" '---------- Except PointID IN 0(Main) -------------
        PointDB = PointDB.DefaultView.ToTable.Copy
        For i As Integer = 0 To PointDB.Rows.Count - 1
            PT.DefaultView.RowFilter = "POINT_ID=" & PointDB.Rows(i).Item("POINT_ID")
            If PT.DefaultView.Count = 0 Then
                PIPE.DROP_POINT(TAG_ID, PointDB.Rows(i).Item("POINT_ID"))
            End If
        Next
        PointDB.Dispose()
        '------------- Save Main Tag File Attachments--------------
        Dim Files As List(Of FileAttachment) = Session(Pipe_File.UNIQUE_ID)
        PIPE.Save_FILE(TAG_ID, 0, Files, True)
        '----------------- Clear Memory-----------
        For i As Integer = Files.Count - 1 To 0 Step -1
            Dim F As FileAttachment = Files(i)
            Files.Remove(F)
            F = Nothing
        Next
        Files = Nothing

        '-------------- Save Point --------------------------------
        PT = PointData()
        For i As Integer = 0 To PT.Rows.Count - 1
            SQL = "SELECT * FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & PT.Rows(i).Item("POINT_ID")
            DT = New DataTable
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT)
            Dim POINT_ID As Integer = PT.Rows(i).Item("POINT_ID")
            If DT.Rows.Count = 0 Then
                DR = DT.NewRow
                DT.Rows.Add(DR)
                POINT_ID = PIPE.Get_New_Point_ID(TAG_ID)
                DR("TAG_ID") = TAG_ID
                DR("POINT_ID") = POINT_ID
            Else
                DR = DT.Rows(0)
            End If
            DR("POINT_Name") = PT.Rows(i).Item("POINT_Name").ToString
            If Not IsDBNull(PT.Rows(i).Item("Component_Type")) AndAlso PT.Rows(i).Item("Component_Type") <> 0 Then
                DR("Component_Type") = PT.Rows(i).Item("Component_Type")
            Else
                DR("Component_Type") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("MD_ID")) AndAlso PT.Rows(i).Item("MD_ID") <> 0 Then
                DR("MD_ID") = PT.Rows(i).Item("MD_ID")
            Else
                DR("MD_ID") = DBNull.Value
            End If
            DR("Manual_Class") = PT.Rows(i).Item("Manual_Class").ToString
            If Not IsDBNull(PT.Rows(i).Item("Size")) Then
                DR("Size") = PT.Rows(i).Item("Size")
            Else
                DR("Size") = DBNull.Value
            End If
            DR("Location_From") = PT.Rows(i).Item("Location_From").ToString
            DR("Location_To") = PT.Rows(i).Item("Location_To").ToString
            If Not IsDBNull(PT.Rows(i).Item("material_id")) AndAlso PT.Rows(i).Item("material_id") <> 0 Then
                DR("material_id") = PT.Rows(i).Item("material_id")
            Else
                DR("material_id") = DBNull.Value
            End If
            DR("Schedule") = PT.Rows(i).Item("Schedule").ToString
            If Not IsDBNull(PT.Rows(i).Item("PRS_ID")) AndAlso PT.Rows(i).Item("PRS_ID") <> 0 Then
                DR("PRS_ID") = PT.Rows(i).Item("PRS_ID")
            Else
                DR("PRS_ID") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("IN_ID")) AndAlso PT.Rows(i).Item("IN_ID") <> 0 Then
                DR("IN_ID") = PT.Rows(i).Item("IN_ID")
            Else
                DR("IN_ID") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("IN_Thickness")) Then
                DR("IN_Thickness") = PT.Rows(i).Item("IN_Thickness")
            Else
                DR("IN_Thickness") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Initial_Year")) Then
                DR("Initial_Year") = PT.Rows(i).Item("Initial_Year")
            Else
                DR("Initial_Year") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Norminal_Thickness")) Then
                DR("Norminal_Thickness") = PT.Rows(i).Item("Norminal_Thickness")
            Else
                DR("Norminal_Thickness") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Corrosion_Allowance")) Then
                DR("Corrosion_Allowance") = PT.Rows(i).Item("Corrosion_Allowance")
            Else
                DR("Corrosion_Allowance") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Calculated_Thickness")) Then
                DR("Calculated_Thickness") = PT.Rows(i).Item("Calculated_Thickness")
            Else
                DR("Calculated_Thickness") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Pressure_Design")) Then
                DR("Pressure_Design") = PT.Rows(i).Item("Pressure_Design")
            Else
                DR("Pressure_Design") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Pressure_Operating")) Then
                DR("Pressure_Operating") = PT.Rows(i).Item("Pressure_Operating")
            Else
                DR("Pressure_Operating") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Temperature_Design")) Then
                DR("Temperature_Design") = PT.Rows(i).Item("Temperature_Design")
            Else
                DR("Temperature_Design") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Temperature_Operating")) Then
                DR("Temperature_Operating") = PT.Rows(i).Item("Temperature_Operating")
            Else
                DR("Temperature_Operating") = DBNull.Value
            End If
            DR("P_ID_No") = PT.Rows(i).Item("P_ID_No").ToString
            DR("Update_By") = Session("USER_ID")
            DR("Update_Time") = Now

            '----------- Update Point ----------
            cmd = New SqlCommandBuilder(DA)
            DA.Update(DT)

            '------------ Save File ------------
            Dim Attachments As List(Of FileAttachment) = Session(PT.Rows(i).Item("UNIQUE_ID"))
            PIPE.Save_FILE(TAG_ID, POINT_ID, Attachments, True)
        Next

        '----------------- Clear Memory-----------
        For i As Integer = 0 To PT.Rows.Count - 1
            Dim Attachments As List(Of FileAttachment) = Session(PT.Rows(i).Item("UNIQUE_ID"))
            For j As Integer = Attachments.Count - 1 To 0 Step -1
                Dim F As FileAttachment = Attachments(j)
                Attachments.Remove(F)
                F = Nothing
            Next
            Attachments = Nothing
        Next

        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        BindTag()

    End Sub

#Region "Tab Control"
    Private Sub ChangeTab(ByVal Tab As MyTab)
        Select Case Tab
            Case MyTab.Tag
                TabTag.Visible = True
                TabPoint.Visible = False

                HTabTag.CssClass = "default-tab current"
                HTabPoint.CssClass = ""

                pnlSaveTag.Visible = True
            Case MyTab.Point
                TabTag.Visible = False
                TabPoint.Visible = True

                HTabTag.CssClass = ""
                HTabPoint.CssClass = "default-tab current"

                pnlSaveTag.Visible = Not pnlEditPoint.Visible
        End Select
    End Sub

    Enum MyTab
        Tag = 0
        Point = 1
    End Enum

    Private Sub HTabTag_Click(sender As Object, e As EventArgs) Handles HTabTag.Click
        ChangeTab(MyTab.Tag)
    End Sub

    Private Sub HTabPoint_Click(sender As Object, e As EventArgs) Handles HTabPoint.Click
        ChangeTab(MyTab.Point)
    End Sub
#End Region

    Private Sub Pipe_Info_PropertyChangedByUser(ByRef Sender As UC_Pipe_Tag_Info, Prop As UC_Pipe_Tag_Info.PropertyItem) Handles Pipe_Info.PropertyChangedByUser
        Select Case Prop
            Case UC_Pipe_Tag_Info.PropertyItem.PLANT
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.AREA
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.LOOP_NO
            Case UC_Pipe_Tag_Info.PropertyItem.PROCESS
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.LINE_NO
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.SERVICE
            Case UC_Pipe_Tag_Info.PropertyItem.PIPE_SIZE
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.SCHEDULE
            Case UC_Pipe_Tag_Info.PropertyItem.INITIAL_YEAR
            Case UC_Pipe_Tag_Info.PropertyItem.LOCATION_FROM
            Case UC_Pipe_Tag_Info.PropertyItem.LOCATION_TO
            Case UC_Pipe_Tag_Info.PropertyItem.NORMINAL_THICKNESS
            Case UC_Pipe_Tag_Info.PropertyItem.CALCULATED_THICKNESS
            Case UC_Pipe_Tag_Info.PropertyItem.INSULATION_ID
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.INSULATION_THICKNESS
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.DESIGN_PRESSURE
            Case UC_Pipe_Tag_Info.PropertyItem.OPERATING_PRESSURE
            Case UC_Pipe_Tag_Info.PropertyItem.DESIGN_TEMPERATURE
            Case UC_Pipe_Tag_Info.PropertyItem.OPERATING_TEMPERATURE
            Case UC_Pipe_Tag_Info.PropertyItem.MATERIAL
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.PRESSURE_CODE
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.CORROSION_ALLOWANCE
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.SERVICE_MEDIA
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_Pipe_Tag_Info.PropertyItem.PIPE_CLASS
            Case UC_Pipe_Tag_Info.PropertyItem.P_ID_No
        End Select
    End Sub

#Region "TabPoint"

    Private Function BlankPointDataTable() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("POINT_ID", GetType(Integer))
        DT.Columns.Add("POINT_Name", GetType(String))
        DT.Columns.Add("Component_Type", GetType(Integer))
        DT.Columns.Add("Component_Type_Name", GetType(String))
        DT.Columns.Add("MD_ID", GetType(Integer))
        DT.Columns.Add("Manual_Class", GetType(String))
        DT.Columns.Add("Size", GetType(String))
        DT.Columns.Add("Location_From", GetType(String))
        DT.Columns.Add("Location_To", GetType(String))
        DT.Columns.Add("material_id", GetType(Integer))
        DT.Columns.Add("Schedule", GetType(String))
        DT.Columns.Add("PRS_ID", GetType(Integer))
        DT.Columns.Add("IN_ID", GetType(Integer))
        DT.Columns.Add("IN_Thickness", GetType(Integer))
        DT.Columns.Add("Initial_Year", GetType(Integer))
        DT.Columns.Add("Norminal_Thickness", GetType(Double))
        DT.Columns.Add("Corrosion_Allowance", GetType(Double))
        DT.Columns.Add("Calculated_Thickness", GetType(Double))
        DT.Columns.Add("Pressure_Design", GetType(Double))
        DT.Columns.Add("Pressure_Operating", GetType(Double))
        DT.Columns.Add("Temperature_Design", GetType(Double))
        DT.Columns.Add("Temperature_Operating", GetType(Double))
        DT.Columns.Add("P_ID_No", GetType(String))
        DT.Columns.Add("UNIQUE_ID", GetType(String)) '----------- เก็บ Unique_ID ของ UC_FileAlbum.Attachments -----------
        Return DT
    End Function

    Private Sub rptPoint_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPoint.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim trPoint As HtmlTableRow = e.Item.FindControl("trPoint")
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblComponent As Label = e.Item.FindControl("lblComponent")
        Dim lblFile As Label = e.Item.FindControl("lblFile")
        Dim lblLife As Label = e.Item.FindControl("lblLife")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnDelete_Confirm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("btnDelete_Confirm")

        lblNo.Text = e.Item.ItemIndex + 1
        lblName.Text = e.Item.DataItem("POINT_Name").ToString
        lblComponent.Text = e.Item.DataItem("Component_Type_Name").ToString
        Try
            Dim Attachements As List(Of FileAttachment) = Session(e.Item.DataItem("UNIQUE_ID").ToString)
            If Attachements.Count = 0 Then
                lblFile.Text = "-"
            Else
                lblFile.Text = Attachements.Count
            End If
        Catch ex As Exception
            lblFile.Text = "-"
        End Try
        lblLife.Text = "-" '------ Hardcode--------
        btnDelete_Confirm.ConfirmText = "Are you sure you want to delete " & lblName.Text & " ?"

        '--------------- Collect Parameter --------------
        Dim BT As DataTable = BlankPointDataTable()
        Dim DT As DataTable = rptPoint.DataSource
        For i As Integer = 0 To BT.Columns.Count - 1
            Dim Param As String = BT.Columns(i).ColumnName
            If DT.Columns.IndexOf(Param) > -1 Then
                trPoint.Attributes(Param) = e.Item.DataItem(Param).ToString
            End If
        Next
        'POINT_ID
        'POINT_Name
        'Component_Type
        'Component_Type_Name
        'MD_ID
        'Manual_Class
        'Size
        'Location_From
        'Location_To
        'material_id
        'Schedule
        'PRS_ID
        'IN_ID
        'IN_Thickness
        'Initial_Year
        'Norminal_Thickness
        'Corrosion_Allowance
        'Calculated_Thickness
        'Pressure_Design
        'Pressure_Operating
        'Temperature_Design
        'Temperature_Operating
        'P_ID_No
        'UNIQUE_ID
    End Sub

    Private Sub rptPoint_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptPoint.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEditPoint()
                Dim DR As DataRow = PointData().Rows(e.Item.ItemIndex)
                '------------ Bind Data To Point_Info --------------
                With Point_Info
                    .POINT_ID = DR("POINT_ID")
                    .POINT_NAME = DR("POINT_NAME")
                    If Not IsDBNull(DR("Component_Type")) Then .COMPONENT_TYPE_ID = DR("Component_Type")
                    If Not IsDBNull(DR("MD_ID")) Then .MEDIA_ID = DR("MD_ID")
                    .MANUAL_CLASS = DR("Manual_Class").ToString
                    If Not IsDBNull(DR("Size")) Then .PIPE_SIZE = DR("Size")
                    .LOCATION_FROM = DR("Location_From").ToString
                    .LOCATION_TO = DR("Location_To").ToString
                    If Not IsDBNull(DR("material_id")) Then .MATERIAL_ID = DR("material_id")
                    .SCHEDULE = DR("Schedule").ToString
                    If Not IsDBNull(DR("PRS_ID")) Then .PRESSURE_ID = DR("PRS_ID")
                    If Not IsDBNull(DR("IN_ID")) Then .INSULATION_ID = DR("IN_ID")
                    If Not IsDBNull(DR("IN_Thickness")) Then .INSULATION_THICKNESS = DR("IN_Thickness")
                    If Not IsDBNull(DR("Initial_Year")) Then .INITIAL_YEAR = DR("Initial_Year")
                    If Not IsDBNull(DR("Norminal_Thickness")) Then .NORMINAL_THICKNESS = DR("Norminal_Thickness")
                    If Not IsDBNull(DR("Corrosion_Allowance")) Then .CORROSION_ALLOWANCE = DR("Corrosion_Allowance")
                    If Not IsDBNull(DR("Calculated_Thickness")) Then .CALCULATED_THICKNESS = DR("Calculated_Thickness")
                    If Not IsDBNull(DR("Pressure_Design")) Then .PRESSURE_DESIGN = DR("Pressure_Design")
                    If Not IsDBNull(DR("Pressure_Operating")) Then .PRESSURE_OPERATING = DR("Pressure_Operating")
                    If Not IsDBNull(DR("Temperature_Design")) Then .TEMPERATURE_DESIGN = DR("Temperature_Design")
                    If Not IsDBNull(DR("Temperature_Operating")) Then .TEMPERATURE_OPERATING = DR("Temperature_Operating")
                    .P_ID_No = DR("P_ID_No").ToString
                    .UNIQUE_ID = DR("UNIQUE_ID")
                End With
                Point_File.UNIQUE_ID = DR("UNIQUE_ID")
                Point_File.BindData()

                '------------ Toggle Show/Hide Panel--
                pnlEditPoint.Visible = True
                pnlListPoint.Visible = False
                pnlSaveTag.Visible = False
                lblEditPoint.Text = "Edit point"
            Case "Delete"
                Dim DT As DataTable = PointData()
                '------------ Clear Point_File-----------
                Dim Attachments As List(Of FileAttachment) = Session(DT.Rows(e.Item.ItemIndex).Item("UNIQUE_ID"))
                While Attachments.Count > 0
                    Dim F As FileAttachment = Attachments(0)
                    Attachments.Remove(F)
                    F = Nothing
                End While
                '------------ Delete From Point_Data----------
                DT.Rows.RemoveAt(e.Item.ItemIndex)
                rptPoint.DataSource = DT
                rptPoint.DataBind()
        End Select
    End Sub

    Private Sub ClearPanelEditPoint()

        '------------ Point Info -------------
        Point_Info.UNIQUE_ID = GenerateNewUniqueID()
        Point_Info.BindData(TAG_ID, -1)
        Point_Info.RaisePropertyChangedByUser = False '------- Prevent Auto Postback ----------
        '------------ Point File -------------
        Point_File.UNIQUE_ID = Point_Info.UNIQUE_ID
        Point_File.Attachments = New List(Of FileAttachment)
        Point_File.BindData()

        '------------ Toggle Show/Hide Panel--
        pnlEditPoint.Visible = False
        pnlListPoint.Visible = True
        pnlSaveTag.Visible = True
        lblEditPoint.Text = "Add new point"

    End Sub

    Private Sub btnAddPoint_Click(sender As Object, e As EventArgs) Handles btnAddPoint.Click
        ClearPanelEditPoint()
        pnlEditPoint.Visible = True
        pnlListPoint.Visible = False
        pnlSaveTag.Visible = False

        '---------- Set Default Main point value----------
        With Point_Info
            .MEDIA_ID = Pipe_Info.MEDIA_ID
            .MATERIAL_ID = Pipe_Info.MATERIAL_ID
            .INSULATION_ID = Pipe_Info.INSULATION_ID
            .PRESSURE_ID = Pipe_Info.PRESSURE_ID
            .PIPE_SIZE = Pipe_Info.PIPE_SIZE
            .SCHEDULE = Pipe_Info.SCHEDULE
            .LOCATION_FROM = Pipe_Info.LOCATION_FROM
            .LOCATION_TO = Pipe_Info.LOCATION_TO
            .INITIAL_YEAR = Pipe_Info.INITIAL_YEAR
            .NORMINAL_THICKNESS = Pipe_Info.NORMINAL_THICKNESS
            .CALCULATED_THICKNESS = Pipe_Info.CALCULATED_THICKNESS
            .INSULATION_THICKNESS = Pipe_Info.INSULATION_THICKNESS
            .CORROSION_ALLOWANCE = Pipe_Info.CORROSION_ALLOWANCE
            .PRESSURE_DESIGN = Pipe_Info.PRESSURE_DESIGN
            .PRESSURE_OPERATING = Pipe_Info.PRESSURE_OPERATING
            .TEMPERATURE_DESIGN = Pipe_Info.TEMPERATURE_DESIGN
            .TEMPERATURE_OPERATING = Pipe_Info.TEMPERATURE_OPERATING
        End With
    End Sub

    Private Sub btnOKPoint_Click(sender As Object, e As EventArgs) Handles btnOKPoint.Click

        '------------- Copy Validate From Point Info.Save------------
        Dim Msg As String = Point_Info.ValidateIncompleteMessage()
        If Msg <> "" Then
            pnlBindingPointError.Visible = True
            lblBindingPointError.Text = Msg
            Exit Sub
        End If

        '--------------- Check Duplicate--------------
        Dim PT As DataTable = PointData()
        PT.DefaultView.RowFilter = "POINT_NAME='" & Point_Info.POINT_NAME.Replace("'", "''") & "' AND UNIQUE_ID<>'" & Point_Info.UNIQUE_ID.Replace("'", "''") & "'"
        If PT.DefaultView.Count > 0 Then
            pnlBindingPointError.Visible = True
            lblBindingPointError.Text = "This point name is already existed"
            Exit Sub
        End If
        PT.DefaultView.RowFilter = "POINT_NAME='" & Point_Info.POINT_NAME.Replace("'", "''") & "'"
        Dim PR As DataRow
        If PT.DefaultView.Count = 0 Then
            PR = PT.NewRow
            PT.Rows.Add(PR)
        Else
            PR = PT.DefaultView(0).Row
        End If
        PT.DefaultView.RowFilter = ""

        With Point_Info
            PR("POINT_ID") = .POINT_ID
            PR("POINT_Name") = .POINT_NAME
            If .COMPONENT_TYPE_ID <> 0 Then
                PR("Component_Type") = .COMPONENT_TYPE_ID
                PR("Component_Type_Name") = .COMPONENT_TYPE_NAME
            Else
                PR("Component_Type") = DBNull.Value
                PR("Component_Type_Name") = DBNull.Value
            End If
            If .MEDIA_ID <> 0 Then
                PR("MD_ID") = Point_Info.MEDIA_ID
            Else
                PR("MD_ID") = DBNull.Value
            End If
            If .ClassAssignMode = UC_Pipe_Point_Info.AssignedMode.Manual Then
                PR("Manual_Class") = .MANUAL_CLASS
            Else
                PR("Manual_Class") = ""
            End If
            If IsNumeric(.PIPE_SIZE) Then
                PR("Size") = .PIPE_SIZE
            Else
                PR("Size") = ""
            End If
            PR("Location_From") = .LOCATION_FROM
            PR("Location_To") = .LOCATION_TO
            If .MATERIAL_ID <> 0 Then
                PR("material_id") = .MATERIAL_ID
            Else
                PR("material_id") = DBNull.Value
            End If
            PR("Schedule") = .SCHEDULE
            If .PRESSURE_ID <> 0 Then
                PR("PRS_ID") = .PRESSURE_ID
            Else
                PR("PRS_ID") = DBNull.Value
            End If
            If .INSULATION_ID <> 0 Then
                PR("IN_ID") = .INSULATION_ID
            Else
                PR("IN_ID") = DBNull.Value
            End If
            If IsNumeric(.INSULATION_THICKNESS) Then
                PR("IN_Thickness") = .INSULATION_THICKNESS
            Else
                PR("IN_Thickness") = DBNull.Value
            End If
            If IsNumeric(.INITIAL_YEAR) Then
                PR("Initial_Year") = .INITIAL_YEAR
            Else
                PR("Initial_Year") = DBNull.Value
            End If
            If IsNumeric(.NORMINAL_THICKNESS) Then
                PR("Norminal_Thickness") = .NORMINAL_THICKNESS
            Else
                PR("Norminal_Thickness") = DBNull.Value
            End If
            If IsNumeric(.CORROSION_ALLOWANCE) Then
                PR("Corrosion_Allowance") = .CORROSION_ALLOWANCE
            Else
                PR("Corrosion_Allowance") = DBNull.Value
            End If
            If IsNumeric(.CALCULATED_THICKNESS) Then
                PR("Calculated_Thickness") = .CALCULATED_THICKNESS
            Else
                PR("Calculated_Thickness") = DBNull.Value
            End If
            If IsNumeric(.PRESSURE_DESIGN) Then
                PR("Pressure_Design") = .PRESSURE_DESIGN
            Else
                PR("Pressure_Design") = DBNull.Value
            End If
            If IsNumeric(.PRESSURE_OPERATING) Then
                PR("Pressure_Operating") = .PRESSURE_OPERATING
            Else
                PR("Pressure_Operating") = DBNull.Value
            End If
            If IsNumeric(.TEMPERATURE_DESIGN) Then
                PR("Temperature_Design") = .TEMPERATURE_DESIGN
            Else
                PR("Temperature_Design") = DBNull.Value
            End If
            If IsNumeric(.TEMPERATURE_OPERATING) Then
                PR("Temperature_Operating") = .TEMPERATURE_OPERATING
            Else
                PR("Temperature_Operating") = DBNull.Value
            End If
            PR("P_ID_No") = .P_ID_No
            PR("UNIQUE_ID") = .UNIQUE_ID
        End With
        '-------------  Keep Attachment File -------------
        '------------- Also Keep via Point_File-----------

        '------------- BindRepeater ----------------------
        rptPoint.DataSource = PT
        rptPoint.DataBind()

        pnlEditPoint.Visible = False
        pnlListPoint.Visible = True
        pnlSaveTag.Visible = True
    End Sub

    Private Sub btnCancelPoint_Click(sender As Object, e As EventArgs) Handles btnCancelPoint.Click
        pnlEditPoint.Visible = False
        pnlListPoint.Visible = True
        pnlSaveTag.Visible = True
    End Sub

#End Region


End Class