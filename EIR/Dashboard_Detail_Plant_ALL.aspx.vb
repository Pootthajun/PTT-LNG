﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Detail_Plant_ALL
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim SQL As String = ""
            Dim DT_PLANT As New DataTable
            Dim DA As New SqlDataAdapter
            SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & Request.QueryString("PLANT_ID")
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT_PLANT)
            If DT_PLANT.Rows.Count > 0 Then
                lblPlantName.Text = DT_PLANT.Rows(0).Item("PLANT_Name").ToString
            End If
            lblBack.Text = "Back to see on this plant " & lblPlantName.Text
            lblMonth.Text = Request.QueryString("MONTH")
            lblYear.Text = Request.QueryString("YEAR")
            lblReportName.Text = Request.QueryString("REPORT")
            Select Case Request.QueryString("REPORT")
                Case EIR_BL.ReportName_Problem.NEW_PROBLEM
                    lblBack.PostBackUrl = "Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM
                    lblHeader.Text = "New Problem Occurred"
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Thermography_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                Case EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                    lblBack.PostBackUrl = "Dashboard_Total_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                    lblHeader.Text = "Total Problem by Month"
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Thermography_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
            End Select
            BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH_F"), Request.QueryString("MONTH_T"), Request.QueryString("YEAR_F"), Request.QueryString("YEAR_T"), Request.QueryString("EQUIPMENT"), Request.QueryString("MONTH"), Request.QueryString("YEAR"))
        End If

    End Sub

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer, ByVal Month As Integer, ByVal Year As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        If Year > 2500 Then
            Year = Year - 543
        End If

        Dim DateFrom As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)
        Select Case lblReportName.Text
            Case EIR_BL.ReportName_Problem.NEW_PROBLEM
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)
            Case EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate)
                DT_Dashboard.DefaultView.RowFilter = "ICLS_ID > 0"
                DT_Dashboard = DT_Dashboard.DefaultView.ToTable
        End Select

        Dim Col() As String = {"RPT_TYPE_ID"}
        Dim DT_RPT As DataTable = DT_Dashboard.DefaultView.ToTable(True, Col)
        DT_RPT.Columns.Add("ClassA", GetType(Integer))
        DT_RPT.Columns.Add("ClassB", GetType(Integer))
        DT_RPT.Columns.Add("ClassC", GetType(Integer))

        For i As Integer = 0 To DT_RPT.Rows.Count - 1
            DT_RPT.Rows(i).Item("ClassA") = DT_Dashboard.Compute("COUNT(TAG_ID)", "RPT_TYPE_ID=" & DT_RPT.Rows(i).Item("RPT_TYPE_ID") & " AND ICLS_ID=3")
            DT_RPT.Rows(i).Item("ClassB") = DT_Dashboard.Compute("COUNT(TAG_ID)", "RPT_TYPE_ID=" & DT_RPT.Rows(i).Item("RPT_TYPE_ID") & " AND ICLS_ID=2")
            DT_RPT.Rows(i).Item("ClassC") = DT_Dashboard.Compute("COUNT(TAG_ID)", "RPT_TYPE_ID=" & DT_RPT.Rows(i).Item("RPT_TYPE_ID") & " AND ICLS_ID=1")
        Next

        For i As Integer = 0 To DT_RPT.Rows.Count - 1
            Dim DR As DataRow
            DR = DT_RPT.Rows(i)
            DisplayChart(i + 1, DR, Plant_ID, Month_F, Month_T, Year_F, Year_T, Month, Year)
        Next
    End Sub

    Private Sub DisplayChart(ByVal ChartNo As Integer, ByVal DR As DataRow, ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Month As Integer, ByVal Year As Integer)

        Dim ClassC As Double = DR.Item("ClassC")
        Dim ClassB As Double = DR.Item("ClassB")
        Dim ClassA As Double = DR.Item("ClassA")

        Dim Url As String = ""
        Select Case CInt(DR.Item("RPT_TYPE_ID"))
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                Url = "Dashboard_Detail_Plant_ST.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & EIR_BL.Report_Type.Stationary_Routine_Report & "&REPORT=" & lblReportName.Text & "&MONTH=" & Month & "&YEAR=" & Year & "&BACK_EQUIPMENT=" & EIR_BL.Report_Type.All
                Dim YValue() As Double = {ClassC, ClassB, ClassA}
                Dim XValue() As String = {"ClassC", "ClassB", "ClassA"}

                Chart1.Series("Series1").Points.DataBindY(YValue)
                Chart1.Titles("Title1").Text = Dashboard.FindEquipmentName(EIR_BL.Report_Type.Stationary_Routine_Report)
                Chart1.Series("Series1").Points(0).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
                Chart1.Series("Series1").Points(1).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
                Chart1.Series("Series1").Points(2).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("ClassC") > 0 Then
                    Chart1.Series("Series1").Points(0).Label = DR.Item("ClassC")
                End If
                If DR.Item("ClassB") > 0 Then
                    Chart1.Series("Series1").Points(1).Label = DR.Item("ClassB")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart1.Series("Series1").Points(2).Label = DR.Item("ClassA")
                End If

                Chart1.Series("Series1").Points(0).Color = Drawing.Color.Yellow
                Chart1.Series("Series1").Points(1).Color = Drawing.Color.Orange
                Chart1.Series("Series1").Points(2).Color = Drawing.Color.Red

                Chart1.Series("Series1").Url = Url

                lbl1_C.Text = DR.Item("ClassC").ToString
                lbl1_B.Text = DR.Item("ClassB").ToString
                lbl1_A.Text = DR.Item("ClassA").ToString

            Case EIR_BL.Report_Type.Rotating_Routine_Report
                Url = "Dashboard_Detail_Plant_RO.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & EIR_BL.Report_Type.Rotating_Routine_Report & "&REPORT=" & lblReportName.Text & "&MONTH=" & Month & "&YEAR=" & Year & "&BACK_EQUIPMENT=" & EIR_BL.Report_Type.All

                Dim YValue() As Double = {ClassC, ClassB, ClassA}
                Dim XValue() As String = {"ClassC", "ClassB", "ClassA"}

                Chart2.Series("Series1").Points.DataBindY(YValue)
                Chart2.Titles("Title1").Text = Dashboard.FindEquipmentName(EIR_BL.Report_Type.Rotating_Routine_Report)
                Chart2.Series("Series1").Points(0).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
                Chart2.Series("Series1").Points(1).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
                Chart2.Series("Series1").Points(2).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("ClassC") > 0 Then
                    Chart2.Series("Series1").Points(0).Label = DR.Item("ClassC")
                End If
                If DR.Item("ClassB") > 0 Then
                    Chart2.Series("Series1").Points(1).Label = DR.Item("ClassB")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart2.Series("Series1").Points(2).Label = DR.Item("ClassA")
                End If

                Chart2.Series("Series1").Points(0).Color = Drawing.Color.Yellow
                Chart2.Series("Series1").Points(1).Color = Drawing.Color.Orange
                Chart2.Series("Series1").Points(2).Color = Drawing.Color.Red

                Chart2.Series("Series1").Url = Url

                lbl2_C.Text = DR.Item("ClassC").ToString
                lbl2_B.Text = DR.Item("ClassB").ToString
                lbl2_A.Text = DR.Item("ClassA").ToString
            Case EIR_BL.Report_Type.Lube_Oil_Report
                Url = "Dashboard_Detail_Plant_LO.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & EIR_BL.Report_Type.Lube_Oil_Report & "&REPORT=" & lblReportName.Text & "&MONTH=" & Month & "&YEAR=" & Year & "&BACK_EQUIPMENT=" & EIR_BL.Report_Type.All

                Dim YValue() As Double = {ClassA}
                Dim XValue() As String = {"ClassA"}

                Chart3.Series("Series1").Points.DataBindY(YValue)
                Chart3.Titles("Title1").Text = Dashboard.FindEquipmentName(EIR_BL.Report_Type.Lube_Oil_Report)
                Chart3.Series("Series1").Points(0).ToolTip = "Abnormal : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("ClassA") > 0 Then
                    Chart3.Series("Series1").Points(0).Label = DR.Item("ClassA")
                End If

                Chart3.Series("Series1").Points(0).Color = Drawing.Color.Red

                Chart3.Series("Series1").Url = Url

                lbl3_A.Text = DR.Item("ClassA").ToString
            Case EIR_BL.Report_Type.PdMA_Report
                Url = "Dashboard_Detail_Plant_PDMA.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & EIR_BL.Report_Type.PdMA_Report & "&REPORT=" & lblReportName.Text & "&MONTH=" & Month & "&YEAR=" & Year & "&BACK_EQUIPMENT=" & EIR_BL.Report_Type.All

                Dim YValue() As Double = {ClassA}
                Dim XValue() As String = {"ClassA"}

                Chart4.Series("Series1").Points.DataBindY(YValue)
                Chart4.Titles("Title1").Text = Dashboard.FindEquipmentName(EIR_BL.Report_Type.PdMA_Report)
                Chart4.Series("Series1").Points(0).ToolTip = "Abnormal : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("ClassA") > 0 Then
                    Chart4.Series("Series1").Points(0).Label = DR.Item("ClassA")
                End If

                Chart4.Series("Series1").Points(0).Color = Drawing.Color.Red

                Chart4.Series("Series1").Url = Url

                lbl4_A.Text = DR.Item("ClassA").ToString
            Case EIR_BL.Report_Type.Thermography_Report
                Url = "Dashboard_Detail_Plant_THM.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & EIR_BL.Report_Type.Thermography_Report & "&REPORT=" & lblReportName.Text & "&MONTH=" & Month & "&YEAR=" & Year & "&BACK_EQUIPMENT=" & EIR_BL.Report_Type.All

                Dim YValue() As Double = {ClassA}
                Dim XValue() As String = {"ClassA"}

                Chart5.Series("Series1").Points.DataBindY(YValue)
                Chart5.Titles("Title1").Text = Dashboard.FindEquipmentName(EIR_BL.Report_Type.Thermography_Report)
                Chart5.Series("Series1").Points(0).ToolTip = "Abnormal : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("ClassA") > 0 Then
                    Chart5.Series("Series1").Points(0).Label = DR.Item("ClassA")
                End If

                Chart5.Series("Series1").Points(0).Color = Drawing.Color.Red

                Chart5.Series("Series1").Url = Url

                lbl5_A.Text = DR.Item("ClassA").ToString
        End Select

    End Sub

End Class