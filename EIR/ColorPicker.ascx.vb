﻿Imports System.Drawing
Public Class ColorPicker
    Inherits System.Web.UI.UserControl
    Public Property Width() As Integer
        Get
            Return pnl.Width.Value
        End Get
        Set(ByVal value As Integer)
            pnl.Width = Unit.Pixel(value)
            btnResult.Width = Unit.Pixel(value)
            btnRed.Width = Unit.Pixel(value)
            btnGreen.Width = Unit.Pixel(value)
            btnBlue.Width = Unit.Pixel(value)
            btnViolet.Width = Unit.Pixel(value)
            btnYellow.Width = Unit.Pixel(value)
            btnWhite.Width = Unit.Pixel(value)
            btnBlack.Width = Unit.Pixel(value)
        End Set
    End Property
    Public Property ResultColor() As Color
        Get
            Return btnResult.BackColor
        End Get
        Set(ByVal value As Color)
            btnResult.BackColor = value
        End Set
    End Property

    Protected Sub btnColor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBlack.Click, btnBlue.Click, btnGreen.Click, btnRed.Click, btnWhite.Click, btnYellow.Click, btnViolet.Click, btnGreen.Click
        ResultColor = Color.FromName(CType(sender, Button).ID.ToString.Replace("btn", ""))
        pnl.Height = Unit.Pixel(20)
    End Sub

    Protected Sub btnResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResult.Click
        Select Case pnl.Height.Value
            Case 20
                pnl.Height = Unit.Pixel(140)
            Case Else
                pnl.Height = Unit.Pixel(20)
        End Select
    End Sub
End Class