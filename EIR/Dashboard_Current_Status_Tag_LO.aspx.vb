﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Dashboard_Current_Status_Tag_LO
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Radar As New DataVisualizedImage
    Dim CV As New Converter
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Lube_Oil_Report

    Private Property Tag_ID() As Integer
        Get
            If IsNumeric(ViewState("Tag_ID")) Then
                Return ViewState("Tag_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Tag_ID") = value
        End Set
    End Property

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property RPT_Month() As Integer
        Get
            If IsNumeric(ViewState("RPT_Month")) Then
                Return ViewState("RPT_Month")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Month") = value
        End Set
    End Property

    Private Property RPT_Period_Type() As EIR_BL.LO_Period_Type
        Get
            If IsNumeric(ViewState("RPT_Period_Type")) Then
                Return ViewState("RPT_Period_Type")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As EIR_BL.LO_Period_Type)
            ViewState("RPT_Period_Type") = value
        End Set
    End Property

    Private Property LO_TAG_TYPE() As EIR_BL.LO_Tag_Type
        Get
            If IsNumeric(ViewState("LO_TAG_TYPE")) Then
                Return ViewState("LO_TAG_TYPE")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As EIR_BL.LO_Tag_Type)
            ViewState("LO_TAG_TYPE") = value
        End Set
    End Property

    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Tag_ID = Request.QueryString("Tag_ID")
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")

            Dim SQL As String = ""
            SQL &= "SELECT RPT_Month,DETAIL_ID FROM RPT_LO_Header LEFT JOIN RPT_LO_Detail " & vbNewLine
            SQL &= "ON RPT_LO_Header.RPT_Year = RPT_LO_Detail.RPT_Year" & vbNewLine
            SQL &= "AND RPT_LO_Header.RPT_NO = RPT_LO_Detail.RPT_No AND RPT_LO_Detail.LO_TAG_ID=" & Tag_ID & vbNewLine
            SQL &= "WHERE RPT_LO_Header.RPT_Year=" & RPT_Year & " AND RPT_LO_Header.RPT_No=" & RPT_No & " AND DETAIL_ID Is Not NULL"

            '--------------Check Initialize Report--------------
            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            RPT_Month = DT.Rows(0).Item("RPT_Month")
            DETAIL_ID = DT.Rows(0).Item("DETAIL_ID")

            SQL = ""
            SQL = "SELECT * FROM VW_REPORT_LO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)

            '------------------------------Header -----------------------------------
            lbl_Month.Text = MonthEng(RPT_Month)
            lbl_Year.Text = RPT_Year
            lbl_Period.Text = DT.Rows(0).Item("RPT_Period_Type_Name")
            RPT_Period_Type = DT.Rows(0).Item("RPT_Period_Type")
            BindTagData()

        End If

    End Sub

    Private Sub BindTagData()
        Dim SQL As String = "SELECT * FROM VW_REPORT_LO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        lblRptCode.Text = DT.Rows(0).Item("RPT_Code")
        lblEquipement.Text = DT.Rows(0).Item("LO_TAG_Name")
        lblTagNo.Text = DT.Rows(0).Item("LO_TAG_NO")
        lblArea.Text = DT.Rows(0).Item("PLANT_Name")
        lblOilType.Text = DT.Rows(0).Item("Oil_TYPE_Name")
        LO_TAG_TYPE = DT.Rows(0).Item("LO_TAG_TYPE")

        If Not IsDBNull(DT.Rows(0).Item("Recomment")) Then
            txt_Suggess.Text = DT.Rows(0).Item("Recomment")
        Else
            txt_Suggess.Text = ""
        End If

        '-------------- Add Autosafe Button------------------
        txt_Suggess.Attributes("onchange") = "document.getElementById('" & btnUpdateDetail.ClientID & "').click();"

        '--------------- Set Parameter Visibility----------------
        '---------------------- TAN Parameter--------------------
        If LO_TAG_TYPE = EIR_BL.LO_Tag_Type.Critical_Machine Then
            '-------------- Visibility ------------------
            tr_TAN.Visible = True
            '--------------- UI Script ------------------
            td_TAN_Value.Attributes("onclick") = "document.getElementById('" & lbl_TAN_Value.ClientID & "').focus();"
            td_TAN_Date.Attributes("onclick") = "document.getElementById('" & lbl_TAN_Date.ClientID & "').focus();"
            '-----------------Value Biding---------------
            If IsNumeric(DT.Rows(0).Item("TAN_Value")) Then
                lbl_TAN_Value.Text = FormatNumber(DT.Rows(0).Item("TAN_Value"))
                td_TAN.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_TAN_Css(DT.Rows(0).Item("TAN_Value"))
            Else
                td_TAN.Attributes("Class") = "tbEdit_td"
                lbl_TAN_Value.Text = ""
            End If
            If Not IsDBNull(DT.Rows(0).Item("TAN_Date")) Then
                lbl_TAN_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("TAN_Date"))
            Else
                lbl_TAN_Date.Text = ""
            End If
        Else
            tr_TAN.Visible = False
        End If

        '---------------------- Oxidation Parameter--------------------
        If LO_TAG_TYPE = EIR_BL.LO_Tag_Type.Critical_Machine Then
            '-------------- Visibility ------------------
            tr_OX.Visible = True
            '--------------- UI Script ------------------
            td_OX_Value.Attributes("onclick") = "document.getElementById('" & lbl_OX_Value.ClientID & "').focus();"
            td_OX_Date.Attributes("onclick") = "document.getElementById('" & lbl_OX_Date.ClientID & "').focus();"
            lbl_OX_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------

            If Not IsDBNull(DT.Rows(0).Item("Oil_Cat")) Then
                If DT.Rows(0).Item("Oil_Cat") = 2 Then
                    imgOxidation.Visible = False
                End If

                Select Case CInt(DT.Rows(0).Item("Oil_Cat"))
                    Case 1 '----------------Oxidation ---------------
                        lbl_OX_UNIT.Text = "%"
                        td_OX_Type.Attributes("Class") = "tbEdit_td"
                        td_OX_Type.InnerHtml = "Oxidation"
                        If IsNumeric(DT.Rows(0).Item("OX_Value")) Then
                            td_OX_Type.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_OX_Css(DT.Rows(0).Item("OX_Value"))
                        Else
                            td_OX_Type.Attributes("Class") = "tbEdit_td"
                        End If
                        imgWater1.Visible = True
                        imgWater2.Visible = False
                    Case 2 '-----------Anti Oxidation----------------
                        lbl_OX_UNIT.Text = "% remain"
                        td_OX_Type.InnerHtml = "Anti-Oxidation"
                        If IsNumeric(DT.Rows(0).Item("OX_Value")) Then
                            td_OX_Type.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_ANTI_OX_Css(DT.Rows(0).Item("OX_Value"))
                        Else
                            td_OX_Type.Attributes("Class") = "tbEdit_td"
                        End If
                        imgWater1.Visible = False
                        imgWater2.Visible = True
                    Case Else
                        lbl_OX_UNIT.Text = ""
                        td_OX_Type.Attributes("Class") = "tbEdit_td"
                End Select
            Else
                lbl_OX_UNIT.Text = ""
                td_OX_Type.Attributes("Class") = "tbEdit_td"
            End If

            If IsNumeric(DT.Rows(0).Item("OX_Value")) Then
                lbl_OX_Value.Text = FormatNumber(DT.Rows(0).Item("OX_Value"))
            Else
                lbl_OX_Value.Text = ""
            End If

            If Not IsDBNull(DT.Rows(0).Item("OX_Date")) Then
                lbl_OX_Date.Text = CV.DateToString(DT.Rows(0).Item("OX_Date"), "yyyy-MM-dd")
            Else
                lbl_OX_Date.Text = ""
            End If

        Else
            tr_OX.Visible = False
        End If

        '---------------------- Water Parameter--------------------
        If LO_TAG_TYPE = EIR_BL.LO_Tag_Type.Critical_Machine Then
            '-------------- Visibility ------------------
            tr_Water.Visible = True
            '--------------- UI Script ------------------
            td_Water_Value.Attributes("onclick") = "document.getElementById('" & lbl_Water_Value.ClientID & "').focus();"
            td_Water_Date.Attributes("onclick") = "document.getElementById('" & lbl_Water_Date.ClientID & "').focus();"
            lbl_Water_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------
            If IsNumeric(DT.Rows(0).Item("Water_Value")) Then
                lbl_Water_Value.Text = FormatNumber(DT.Rows(0).Item("Water_Value"))
            Else
                lbl_Water_Value.Text = ""
            End If
            If Not IsDBNull(DT.Rows(0).Item("Water_Date")) Then
                lbl_Water_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("Water_Date"))
            Else
                lbl_Water_Date.Text = ""
            End If

            If Not IsDBNull(DT.Rows(0).Item("Oil_Cat")) Then
                Select Case DT.Rows(0).Item("Oil_Cat")
                    Case 1
                        If IsNumeric(DT.Rows(0).Item("Water_Value")) Then
                            td_Water.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Water_Mineral_Css(DT.Rows(0).Item("Water_Value"))
                        Else
                            td_Water.Attributes("Class") = "tbEdit_td"
                        End If
                    Case 2
                        If IsNumeric(DT.Rows(0).Item("Water_Value")) Then
                            td_Water.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Water_Synthetic_Css(DT.Rows(0).Item("Water_Value"))
                        Else
                            td_Water.Attributes("Class") = "tbEdit_td"
                        End If
                End Select
            End If
        Else
            tr_Water.Visible = False
        End If

        '---------------------- Particles Count--------------------
        If RPT_Period_Type = EIR_BL.LO_Period_Type.Quaterly Or LO_TAG_TYPE <> EIR_BL.LO_Tag_Type.Balance_Of_Plant Then
            '-------------- Visibility ------------------
            tr_PART_COUNT.Visible = True
            '--------------- UI Script ------------------
            'td_PART_COUNT_Value.Attributes("onclick") = "document.getElementById('" & ddl_PART_COUNT_Value.ClientID & "').click();"
            'td_PART_COUNT_Date.Attributes("onclick") = "document.getElementById('" & txt_PART_COUNT_Date.ClientID & "').focus();"
            '-----------------Value Biding---------------
            'ddl_PART_COUNT_Value.SelectedIndex = 0
            'If Not IsDBNull(DT.Rows(0).Item("PART_COUNT_Value")) Then
            '    For i As Integer = 0 To ddl_PART_COUNT_Value.Items.Count - 1
            '        If ddl_PART_COUNT_Value.Items(i).Value = DT.Rows(0).Item("PART_COUNT_Value") Then
            '            ddl_PART_COUNT_Value.SelectedIndex = i
            '            Exit For
            '        End If
            '    Next
            'End If
            lblPART_COUNT_Value.Text = DT.Rows(0).Item("PART_COUNT_Display").ToString
            If Not IsDBNull(DT.Rows(0).Item("PART_COUNT_Date")) Then
                lbl_PART_COUNT_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("PART_COUNT_Date"))
            Else
                lbl_PART_COUNT_Date.Text = ""
            End If
        Else
            tr_PART_COUNT.Visible = False
        End If

        '---------------------- Varnish--------------------
        If RPT_Period_Type = EIR_BL.LO_Period_Type.Quaterly Then
            '-------------- Visibility ------------------
            tr_Varnish.Visible = True
            '--------------- UI Script ------------------
            td_Varnish_Value.Attributes("onclick") = "document.getElementById('" & lbl_Varnish_Value.ClientID & "').focus();"
            td_Varnish_Date.Attributes("onclick") = "document.getElementById('" & lbl_Varnish_Date.ClientID & "').focus();"
            lbl_Varnish_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------
            If IsNumeric(DT.Rows(0).Item("VANISH_Value")) Then
                lbl_Varnish_Value.Text = FormatNumber(DT.Rows(0).Item("VANISH_Value"), 0)
                td_Varnish.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_Varnish_Css(DT.Rows(0).Item("VANISH_Value"))
            Else
                lbl_Varnish_Value.Text = ""
                td_Varnish.Attributes("Class") = "tbEdit_td"
            End If

            If Not IsDBNull(DT.Rows(0).Item("VANISH_Date")) Then
                lbl_Varnish_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("VANISH_Date"))
            Else
                lbl_Varnish_Date.Text = ""
            End If
        Else
            tr_Varnish.Visible = False
        End If

        '---------------- Set Parameter Group Visibility ----------------------
        tb_Oil_Condition.Visible = tr_TAN.Visible Or tr_OX.Visible Or tr_Varnish.Visible
        tb_Contamination.Visible = tr_PART_COUNT.Visible Or tr_Water.Visible

        '---------------- Display Radar -----------------
        Dim B As Byte() = Radar.Get_LubeOil_RadarImage(DETAIL_ID)
        Dim UNIQUE_ID = Now.ToOADate.ToString.Replace(".", "")
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_1") = B
        imgRadar.ImageUrl = "RenderImage.aspx?UNIQUE_ID=" & UNIQUE_ID & "&Image=1&"

    End Sub

End Class