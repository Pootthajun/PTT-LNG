﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Dashboard_Current_Status_AllTag_THM
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("PLANT_ID")) Then
                PLANT_ID = Request.QueryString("PLANT_ID")
            End If
            lblBack.Text = "Back to see all current status for " & lblPlant.Text
            lblBack.PostBackUrl = "Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & PLANT_ID
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SQL As String = " DECLARE @PLANT_ID INT=" & PLANT_ID & vbNewLine
        SQL &= "SELECT TAG.TAG_ID,TAG.TAG_Code,THM_TYPE_Name,ROUTE_Name,RPT_Year,RPT_No,RPT_CODE" & vbNewLine
        SQL &= ",CASE TAG_STATUS WHEN 1 THEN 1 WHEN 0 THEN 0 ELSE NULL END CUR_LEVEL" & vbNewLine
        SQL &= "FROM MS_THM_Tag TAG" & vbNewLine
        SQL &= "LEFT JOIN MS_THM_Type " & vbNewLine
        SQL &= "ON TAG.THM_TYPE_ID = MS_THM_Type.THM_TYPE_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "    SELECT RPT_Year,RPT_No,TAG_ID,TAG_STATUS,dbo.UDF_RPT_Code(RPT_Year, RPT_No) RPT_CODE" & vbNewLine
        SQL &= "    FROM" & vbNewLine
        SQL &= "    (" & vbNewLine
        SQL &= " 	SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
        SQL &= "        FROM" & vbNewLine
        SQL &= "        (" & vbNewLine
        SQL &= "            SELECT TAG_ID,RPT_Year,RPT_No,TAG_STATUS," & vbNewLine
        SQL &= " 	    CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
        SQL &= "            FROM" & vbNewLine
        SQL &= " 		    (" & vbNewLine
        SQL &= " 			SELECT RPT_Year,RPT_No,RPT_THM_Detail.TAG_ID,TAG_STATUS" & vbNewLine
        SQL &= "			FROM RPT_THM_Detail " & vbNewLine
        SQL &= " 		    ) TB1" & vbNewLine
        SQL &= " 		    GROUP BY TAG_ID,RPT_Year,RPT_No,TAG_STATUS" & vbNewLine
        SQL &= " 	    ) TB2" & vbNewLine
        SQL &= "    ) TB3" & vbNewLine
        SQL &= "    WHERE ROW_NUM = 1" & vbNewLine
        SQL &= " ) TAG_STATUS" & vbNewLine
        SQL &= " ON TAG.TAG_ID = TAG_STATUS.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN MS_Plant " & vbNewLine
        SQL &= "ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= "LEFT JOIN MS_THM_Route" & vbNewLine
        SQL &= "ON TAG.ROUTE_ID = MS_THM_Route.ROUTE_ID" & vbNewLine
        SQL &= "WHERE TAG.PLANT_ID = @PLANT_ID AND TAG.Active_Status = 1" & vbNewLine
        SQL &= "ORDER BY THM_TYPE_Name,ROUTE_Name,TAG_Code" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        rptData.DataSource = DT
        rptData.DataBind()

        Session("Dashboard_Current_Status_AllTag_THM") = DT
        DisplayChart()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblEqm As Label = e.Item.FindControl("lblEqm")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        'Dim lblStatus As HtmlAnchor = e.Item.FindControl("lblStatus")
        'Dim lblLastReport As HtmlAnchor = e.Item.FindControl("lblLastReport")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        lblTag.Text = e.Item.DataItem("TAG_Code").ToString.Trim
        lblEqm.Text = e.Item.DataItem("THM_TYPE_Name").ToString
        lblRoute.Text = e.Item.DataItem("ROUTE_Name").ToString

        If e.Item.DataItem("CUR_LEVEL").ToString <> "" Then
            If e.Item.DataItem("CUR_LEVEL") = 0 Then
                lblStatus.Text = "Normal"
                lblStatus.Style("color") = "Green"
                'lblStatus.Style("cursor") = "pointer"
            Else
                lblStatus.Text = "Abnormal"
                lblStatus.Style("color") = "Red"
                'lblStatus.Style("cursor") = "pointer"
            End If
            'lblStatus.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_THM.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "','Dialog_Tag_THM_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        Else
            lblStatus.Text = "N/A"
        End If

        'If e.Item.DataItem("RPT_CODE").ToString <> "" Then
        'lblLastReport.InnerHtml = e.Item.DataItem("RPT_CODE")
        'lblLastReport.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_THM.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "','Dialog_Tag_THM_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        'lblLastReport.Style("cursor") = "pointer"
        'End If

    End Sub

    Protected Sub DisplayChart() Handles ddl_ChartType.SelectedIndexChanged

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartTHM.Series(0).ChartType
        Select Case ddl_ChartType.Items(ddl_ChartType.SelectedIndex).Value
            Case "Pie"
                ChartType = DataVisualization.Charting.SeriesChartType.Pie
            Case "Doughnut"
                ChartType = DataVisualization.Charting.SeriesChartType.Doughnut
            Case "Funnel"
                ChartType = DataVisualization.Charting.SeriesChartType.Funnel
            Case "Pyramid"
                ChartType = DataVisualization.Charting.SeriesChartType.Pyramid
        End Select
        ChartTHM.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Current_Status_AllTag_THM")
        Dim Normal As Double
        Dim Abnormal As Double
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("CUR_LEVEL").ToString = "1" Then
                Abnormal += 1
            Else
                Normal += 1
            End If
        Next

        Dim YValue() As Double = {Normal, Abnormal}
        Dim XValue() As String = {"Normal", "Abnormal"}

        ChartTHM.Series("Series1").Points.DataBindY(YValue)
        ChartTHM.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
        ChartTHM.Series("Series1").Points(1).ToolTip = "Abnormal : " & FormatNumber(Abnormal, 0) & " tag(s)"
        ChartTHM.Titles("Title1").Text = ChartTHM.Titles("Title1").Text.Replace("xxx", lblPlant.Text)
        ChartTHM.Titles("Title2").Text = ChartTHM.Titles("Title2").Text.Replace("xxx", DT.Rows.Count.ToString)

        If Normal > 0 Then
            ChartTHM.Series("Series1").Points(0).Label = Normal
            ChartTHM.Series("Series1").Points(0).LabelForeColor = Drawing.Color.White
        End If
        If Abnormal > 0 Then
            ChartTHM.Series("Series1").Points(1).Label = Abnormal
            ChartTHM.Series("Series1").Points(0).LabelForeColor = Drawing.Color.White
        End If

        ChartTHM.Series("Series1").Points(0).Color = Drawing.Color.Green
        ChartTHM.Series("Series1").Points(1).Color = Drawing.Color.Red

    End Sub

End Class