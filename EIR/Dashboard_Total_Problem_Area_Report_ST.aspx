﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Total_Problem_Area_Report_ST.aspx.vb" Inherits="EIR.Dashboard_Total_Problem_Area_Report_ST" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<%@ Register src="UC_Dashboard_Total_Problem_Area_Report_Plant.ascx" tagname="UC_Dashboard_Total_Problem_Area_Report_Plant" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDP1" runat="server">
    <ContentTemplate>
            <style type="text/css">
                
                
                
                .summary_table_tr{
                    border-bottom:solid 1px #efefef;
                    background-color:White;
                }
                
                .summary_table_tr td{
                    text-align:center; 
                    font-size:12px; 
                    font-family:Arial; 
                    border-top:#efefef 1px solid; 
                    padding-bottom:5px; 
                    padding-top:5px;
                }
                
                .summary_table_header_td
                {
                     text-align:center; 
                     background-color:#003366; 
                     color:White; 
                     font-size:12px; font-family:Arial; 
                     padding:5px,10px,5px,10px; 
                     border-top:1px solid #003366;"
                }
                
                .detail_table_td
                {
                    background-color:#fff;
                    cursor:pointer;
                    border-top:1px solid #eee;
                    border-right:1px solid #eee;
                }
                
                tr:hover .detail_table_td
                {
                    background-color:#DAE7FC;
                }
                
            </style>

			<!-- Page Head -->
			<h2>Stationary Tag Problem Status for <asp:LinkButton ID="lblPlant" runat="server"></asp:LinkButton>
				Area <asp:LinkButton ID="lblArea" runat="server"></asp:LinkButton></h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td colspan="3">        
                    <asp:LinkButton ID="lblBack" runat="server" Text="Back to see all plants"></asp:LinkButton>
                </td>
			  </tr>
			  <tr>
			    <td colspan="3">
			        <h3>
                        <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="From"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Month_F" runat="server" AutoPostBack="true"></asp:DropDownList>
                        &nbsp;<asp:DropDownList ID="ddl_Year_F" runat="server" AutoPostBack="true"></asp:DropDownList>
                        &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="#009933" Text="To"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Month_T" runat="server" AutoPostBack="true"></asp:DropDownList>
                        &nbsp;<asp:DropDownList ID="ddl_Year_T" runat="server" AutoPostBack="true"></asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;<asp:Label ID="Label1" runat="server" ForeColor="#009933" Text="Tag"></asp:Label>
                        &nbsp;<asp:TextBox ID="txtSearchTag" AutoPostBack="true" runat="server" Height="20" style="text-align:center;"></asp:TextBox>
                    </h3>            
			    </td>
			  </tr>
			  <tr>
				<td width="500" style="vertical-align:top; width: 0%;">
				<asp:Chart ID="ChartMain" runat="server" Width="500px" Height="400px" CssClass="ChartHighligh" >
                    <legends>
                        <asp:Legend Name="Legend1" DockedToChartArea="ChartArea1">
                        </asp:Legend>
                    </legends>
                    <titles>
                        <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" 
                            Text="All tag(s) in this area">
                        </asp:Title>
                        <asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" 
                            Font="Microsoft Sans Serif, 9.75pt">
                        </asp:Title>
                    </titles>
                    <Series>
                        <asp:Series Name="Series1" ChartType="Pie" Palette="Bright" ShadowColor="" 
                            Legend="Legend1" Font="Microsoft Sans Serif, 8.25pt, style=Bold">
                            <points>
                                <asp:DataPoint AxisLabel="" BackGradientStyle="None" BackSecondaryColor="" 
                                    Color="Green" Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Normal" 
                                    MapAreaAttributes="" ToolTip="" Url="" YValues="10" />
                                <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" Color="Yellow" 
                                    Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Class C" MapAreaAttributes="" 
                                    ToolTip="" Url="" YValues="20" />
                                <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" 
                                    Color="255, 128, 0" Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Class B" 
                                    MapAreaAttributes="" ToolTip="" Url="" YValues="30" />
                                <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" Color="Red" 
                                    Font="Tahoma, 8.25pt" Label="#VAL" LabelForeColor="Maroon" LegendText="Class A" 
                                    MapAreaAttributes="" ToolTip="" Url="" YValues="40" />
                            </points>
                            <emptypointstyle isvisibleinlegend="False" />
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                    </asp:Chart>
                </td>
                <td style="width:50px;"></td>
			      <td style="vertical-align:top;">
			          <h3 style="width:100%; text-align:center;">Total Problems Summary</h3>
                      <table border="0" cellpadding="0" cellspacing="0" style="width:420px; border:1px solid #efefef;">
                         <tr>
                              <td class="summary_table_header_td">
                                  Tag</td>
                              <td class="summary_table_header_td">
                                  ClassC</td>
                              <td class="summary_table_header_td">
                                  ClassB</td>
                              <td class="summary_table_header_td">
                                  ClassA</td>
                              <td class="summary_table_header_td">
                                  Total</td>
                           </tr>
                         <asp:Repeater ID="rptDataSummary" runat="server">
                                <ItemTemplate>
                                    <tr class="summary_table_tr">
                                      <td >
                                          <asp:Label ID="lblTag" runat="server"></asp:Label></td>
                                      <td>
                                          <asp:Label ID="lblClassC" runat="server" CssClass="TextClassC"></asp:Label></td>
                                      <td>
                                          <asp:Label ID="lblClassB" runat="server" CssClass="TextClassB"></asp:Label></td>
                                      <td>
                                          <asp:Label ID="lblClassA" runat="server" CssClass="TextClassA"></asp:Label></td>
                                      <td>
                                          <asp:Label ID="lblTotal" runat="server"></asp:Label></td>
                                    </tr>
                                </ItemTemplate>
                              </asp:Repeater>
                      </table>
                  </td>
		      </tr>
			</table>
			<div>
			    <h3 style="width:100%; text-align:center; background-color:#FFFFFF;">Detail</h3>
			    <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #efefef;">
                     <tr>
                          <td class="summary_table_header_td">
                              Date</td>
                          <td class="summary_table_header_td">
                              Tag</td>
                          <td class="summary_table_header_td">
                              Inspection / Status</td>
                          <td class="summary_table_header_td">
                              Detail</td>
                          <td class="summary_table_header_td">
                              Class</td>
                          <td class="summary_table_header_td">
                              Report No</td>
                       </tr>
                     <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="tbTag" runat="server" >
                                  <td id="tdDate" runat="server" class="detail_table_td">
                                      <asp:Label ID="lblDate" runat="server"></asp:Label></td>
                                  <td id="tdTag" runat="server" class="detail_table_td">
                                      <asp:Label ID="Label2" runat="server"></asp:Label></td>
                                  <td class="detail_table_td">
                                      <asp:Label ID="lblProblem" runat="server"></asp:Label></td>
                                  <td class="detail_table_td">
                                      <asp:Label ID="lblDetail" runat="server"></asp:Label></td>
                                  <td id="tdClass" runat="server" style="text-align:center;">
                                      <asp:Label ID="lblClass" runat="server"></asp:Label></td>
                                  <td class="detail_table_td">
                                      <asp:Label ID="lblReportNo" runat="server"></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                     </asp:Repeater>
                </table>
			</div>
            <div style="visibility: hidden">
                <asp:Label ID="lblPlantID" runat="server" Text="Label"></asp:Label>
            </div>
            
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
