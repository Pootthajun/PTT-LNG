﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_Inspection
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim Cons As New EIR_Consequence

#Region "Property"
    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_ID") = value
        End Set
    End Property

    'Dim _tag_class As String = "ST"
    Public Property TAG_CLASS() As EIR_BL.Tag_Class
        Get
            If ViewState("TagClass") = "RO" Then
                Return EIR_BL.Tag_Class.Rotating
            Else
                Return EIR_BL.Tag_Class.Stationary
            End If
        End Get
        Set(ByVal value As EIR_BL.Tag_Class)
            Select Case value
                Case EIR_BL.Tag_Class.Rotating
                    ViewState("TagClass") = "RO"
                Case Else
                    ViewState("TagClass") = "ST"
            End Select
        End Set
    End Property

    Public Property TAG_TYPE_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_TYPE_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_TYPE_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_TYPE_ID") = value
        End Set
    End Property

    Public Property INSP_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("INSP_ID")) Then
                Return 0
            Else
                Return Me.Attributes("INSP_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("INSP_ID") = value
        End Set
    End Property

    Public Property DETAIL_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("DETAIL_ID")) Then
                Return 0
            Else
                Return Me.Attributes("DETAIL_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("DETAIL_ID") = value
        End Set
    End Property

    Public Property INSP_NAME() As String
        Get
            Return lbl_Inspection.Text
        End Get
        Set(ByVal value As String)
            lbl_Inspection.Text = value
        End Set
    End Property

    Public Property PARTNO() As String
        Get
            Return txtPart.Text
        End Get
        Set(ByVal value As String)
            txtPart.Text = value
            lblPart.Text = value
        End Set
    End Property

    Public ReadOnly Property CURRENT_STATUS() As Integer
        Get
            If ddl_Status.SelectedIndex > 0 Then
                Return ddl_Status.Items(ddl_Status.SelectedIndex).Value
            Else
                Return -1
            End If
            Return ddl_Level.SelectedIndex - 1
        End Get
    End Property

    Public Property CURRENT_LEVEL() As EIR_BL.InspectionLevel
        Get
            Select Case INSP_ID
                Case 11
                    Return 0
                Case Else
                    Return ddl_Level.SelectedIndex - 1
            End Select
        End Get
        Set(ByVal value As EIR_BL.InspectionLevel)
            ddl_Level.SelectedIndex = value + 1
            'UpdateTrace()
        End Set
    End Property

    Public Property BIZ_LEVEL As EIR_Consequence.BusinessConsLevel
        Get
            Try
                Return ddl_Biz_Level.Items(ddl_Biz_Level.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As EIR_Consequence.BusinessConsLevel)
            Try
                ddl_Biz_Level.SelectedIndex = value
            Catch : End Try
        End Set
    End Property

    Public Property SAFETY_LEVEL As EIR_Consequence.SafetyConsLevel
        Get
            Try
                Return ddl_Safety_Level.Items(ddl_Safety_Level.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As EIR_Consequence.SafetyConsLevel)
            Try
                ddl_Safety_Level.SelectedIndex = value
            Catch : End Try
        End Set
    End Property

    Public Property USE_CONSEQUENCE As EIR_Consequence.Consequence_Category
        Get
            If rdoBiz.Checked Then
                Return EIR_Consequence.Consequence_Category.Business
            ElseIf rdoSafety.Checked Then
                Return EIR_Consequence.Consequence_Category.Safety
            Else
                Return EIR_Consequence.Consequence_Category.Unknow
            End If
        End Get
        Set(value As EIR_Consequence.Consequence_Category)
            rdoBiz.Checked = False
            rdoSafety.Checked = False
            Select Case value
                Case EIR_Consequence.Consequence_Category.Business
                    rdoBiz.Checked = True
                Case EIR_Consequence.Consequence_Category.Safety
                    rdoSafety.Checked = True
            End Select
        End Set
    End Property

    Public Property MA_Day As Integer
        Get
            Try
                Return ddl_MA_Day.Items(ddl_MA_Day.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ddl_MA_Day.SelectedIndex = 0
            For i As Integer = 0 To ddl_MA_Day.Items.Count - 1
                If ddl_MA_Day.Items(i).Value = value Then
                    ddl_MA_Day.SelectedIndex = i
                    Exit Property
                End If
            Next
        End Set
    End Property

    Public Property LAST_LEVEL() As EIR_BL.InspectionLevel
        Get
            Select Case INSP_ID
                Case 11
                    Return 0
                Case 12
                    Return BL.Get_Inspection_Zone_By_Text_Css(lbl_LastStatus.Attributes("class"))
                Case Else
                    Return BL.Get_Inspection_Level_By_Text_Css(lbl_LastStatus.Attributes("class"))
            End Select

        End Get
        Set(ByVal value As EIR_BL.InspectionLevel)
            Select Case INSP_ID
                Case 12
                    lbl_LastStatus.Attributes("class") = BL.Get_Inspection_Css_Text_By_Zone(value)
                Case Else
                    lbl_LastStatus.Attributes("class") = BL.Get_Inspection_Css_Text_By_Level(value)
            End Select
            lbl_LastStatus.Title = lbl_LastStatus.Attributes("class").Replace("Text", "")
            'UpdateTrace()
        End Set
    End Property

    Public Property LAST_STATUS_NAME() As String
        Get
            Return lbl_LastStatus.InnerHtml
        End Get
        Set(ByVal value As String)
            lbl_LastStatus.InnerHtml = value
        End Set
    End Property

    Public Property REF_INSP_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("REF_INSP_ID")) Then
                Return 0
            Else
                Return Me.Attributes("REF_INSP_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("REF_INSP_ID") = value
        End Set
    End Property

    Public Property REF_STATUS_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("REF_STATUS_ID")) Then
                Return -1
            Else
                Return Me.Attributes("REF_STATUS_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("REF_STATUS_ID") = value
        End Set
    End Property

    Public Property Fixed() As TriState
        Get
            Select Case ddl_Fixed.SelectedIndex
                Case 1
                    Return TriState.True
                Case 2
                    Return TriState.False
                Case Else
                    Return TriState.UseDefault
            End Select
        End Get
        Set(ByVal value As TriState)
            Select Case value
                Case TriState.UseDefault
                    ddl_Fixed.SelectedIndex = 0
                Case TriState.True
                    ddl_Fixed.SelectedIndex = 1
                    ddl_Fixed.ForeColor = Drawing.Color.Green
                Case TriState.False
                    ddl_Fixed.SelectedIndex = 2
                    ddl_Fixed.ForeColor = Drawing.Color.Red
            End Select
        End Set
    End Property

    Public Property PROB_DETAIL() As String
        Get
            Return lbl_Detail.Text
        End Get
        Set(ByVal value As String)
            lbl_Detail.Text = value
        End Set
    End Property

    Public Property WarningType() As EIR_BL.Warning
        Get
            If Not imgWarning.Visible Then Return EIR_BL.Warning.Normal
            Select Case imgWarning.ImageUrl
                Case "resources/images/icons/alert.gif"
                    Return EIR_BL.Warning.Alert
                Case "resources/images/icons/warning.gif"
                    Return EIR_BL.Warning.Warning
            End Select
            Return EIR_BL.Warning.Normal
        End Get
        Set(ByVal value As EIR_BL.Warning)
            Select Case value
                Case EIR_BL.Warning.Normal
                    imgWarning.Visible = False
                Case EIR_BL.Warning.Warning
                    imgWarning.Visible = True
                    imgWarning.ImageUrl = "resources/images/icons/warning.gif"
                Case EIR_BL.Warning.Alert
                    imgWarning.Visible = True
                    imgWarning.ImageUrl = "resources/images/icons/alert.gif"
            End Select
        End Set
    End Property

    Private Sub UpdateTrace()

        Dim l As Integer = LAST_LEVEL
        Dim c As Integer = CURRENT_LEVEL

        Select Case INSP_ID
            Case 11 '----------------- Machine Running ----------------

                ddl_Level.SelectedIndex = 0
                ddl_Level.Style.Item("visibility") = "hidden"
                lbl_Trace.Style.Item("visibility") = "hidden"
                lbl_Trace.Text = "No problem found"

                TDLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(0)

            Case 12 ' Vibration level

                ddl_Level.Style.Item("visibility") = "visible"
                lbl_Trace.Style.Item("visibility") = "visible"

                If ddl_Level.SelectedIndex = -1 Then
                    ddl_Level.CssClass = BL.Get_Inspection_Css_Box_By_Zone(LAST_LEVEL)
                    ddl_Status.CssClass = BL.Get_Inspection_Css_Text_By_Zone(LAST_LEVEL)
                Else
                    ddl_Level.CssClass = BL.Get_Inspection_Css_Text_By_Zone(CURRENT_LEVEL)
                    ddl_Status.CssClass = BL.Get_Inspection_Css_Text_By_Zone(CURRENT_LEVEL)
                End If

                If l <= 1 And c > 1 Then
                    lbl_Trace.Text = "New Problem"
                    lbl_Trace.ForeColor = Drawing.Color.Red
                ElseIf l > 1 And c > 1 Then
                    Select Case Fixed
                        Case TriState.True
                            lbl_Trace.Text = "Fixed incompletely"
                        Case Else
                            lbl_Trace.Text = "Still has problem"
                    End Select
                    lbl_Trace.ForeColor = Drawing.Color.Red
                ElseIf l > 1 And c <= 1 And c >= 0 Then
                    lbl_Trace.Text = "Problem fixed"
                    lbl_Trace.ForeColor = Drawing.Color.Green
                ElseIf l > 1 And c < 0 Then
                    lbl_Trace.Text = "Still has problem"
                    lbl_Trace.ForeColor = Drawing.Color.Red
                Else
                    lbl_Trace.Text = "No problem found"
                    lbl_Trace.ForeColor = Drawing.Color.Silver
                End If

                TDLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Zone(IIf(CURRENT_LEVEL >= 0, CURRENT_LEVEL, LAST_LEVEL))

            Case Else

                ddl_Level.Style.Item("visibility") = "visible"
                lbl_Trace.Style.Item("visibility") = "visible"

                If LAST_LEVEL > 0 And CURRENT_LEVEL = 0 Then
                    lbl_Trace.Text = "Problem fixed"
                    lbl_Trace.ForeColor = Drawing.Color.Green
                ElseIf LAST_LEVEL < 1 And CURRENT_LEVEL > 0 Then
                    lbl_Trace.Text = "New Problem"
                    lbl_Trace.ForeColor = Drawing.Color.Red
                ElseIf LAST_LEVEL > 0 And CURRENT_LEVEL > 0 Then
                    Select Case Fixed
                        Case TriState.True
                            lbl_Trace.Text = "Fixed incompletely"
                        Case Else
                            lbl_Trace.Text = "Still has problem"
                    End Select
                    lbl_Trace.ForeColor = Drawing.Color.Red
                Else
                    lbl_Trace.Text = "No problem found"
                    lbl_Trace.ForeColor = Drawing.Color.Silver
                End If

                If ddl_Level.SelectedIndex = -1 Then
                    ddl_Level.CssClass = BL.Get_Inspection_Css_Text_By_Level(LAST_LEVEL)
                    ddl_Status.CssClass = BL.Get_Inspection_Css_Text_By_Level(LAST_LEVEL)
                Else
                    ddl_Level.CssClass = BL.Get_Inspection_Css_Text_By_Level(CURRENT_LEVEL)
                    ddl_Status.CssClass = BL.Get_Inspection_Css_Text_By_Level(CURRENT_LEVEL)
                End If
                TDLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(IIf(CURRENT_LEVEL >= 0, CURRENT_LEVEL, LAST_LEVEL))
        End Select

        '------------ Set Color For BizLevel AND MA_Day
        ddl_Biz_Level.CssClass = Cons.Get_Biz_Css_Text_By_Level(BIZ_LEVEL)
        ddl_MA_Day.CssClass = Cons.Get_MA_Day_Css_Text_By_Day(MA_Day)

    End Sub

    Public Property LAST_DETAIL_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("LAST_DETAIL_ID")) Then
                Return 0
            Else
                Return Me.Attributes("LAST_DETAIL_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("LAST_DETAIL_ID") = value
            lblPart.Visible = value <> 0
            txtPart.Visible = value = 0
        End Set
    End Property

    Public Property Disabled() As Boolean
        Get
            Return lbl_Inspection.Font.Strikeout
        End Get
        Set(ByVal value As Boolean)
            lbl_Inspection.Font.Strikeout = value

            If value Then
                txtPart.Visible = False
                lblPart.Visible = False
            Else
                lblPart.Visible = LAST_DETAIL_ID <> 0
                txtPart.Visible = LAST_DETAIL_ID = 0
            End If


            If value And (CURRENT_LEVEL >= 0 Or CURRENT_STATUS > 0 Or PROB_DETAIL <> "") And WarningType <> EIR_BL.Warning.Alert Then
                imgWarning.ToolTip = "This inspection point has existing information" & vbNewLine & "This may make conflict dependent referring rule!!" & vbNewLine & vbNewLine & "We suggess to delete all information for this line.."
                WarningType = EIR_BL.Warning.Warning
            Else
                If (CURRENT_LEVEL >= 0 And CURRENT_STATUS = 0) Or (CURRENT_LEVEL < 0 And CURRENT_STATUS > 0) Then
                    imgWarning.ToolTip = "Unable to autosave" & vbNewLine & "Please completed all require detail !!"
                    WarningType = EIR_BL.Warning.Alert
                End If
            End If

            '----------- Add Hidding If don't need to display ----------
            'If value And WarningType = EIR_BL.Warning.Normal Then Me.Visible = False
        End Set
    End Property



#End Region

    Private Sub ClearDetail()
        INSP_NAME = ""
        LAST_LEVEL = -1
        LAST_STATUS_NAME = ""
        ddl_Level.Items.Clear()
        ddl_Status.Items.Clear()
        PROB_DETAIL = ""
        PARTNO = ""
        REF_INSP_ID = 0
        REF_STATUS_ID = 0
        LAST_DETAIL_ID = 0

        Select Case INSP_ID
            Case 12 '------------Vibration Level----------
                BL.BindDDlVibrationZone(ddl_Level)
            Case Else '------------ Condition Level-------
                BL.BindDDlLevel(ddl_Level)
        End Select

        Cons.BindDDl_BizLevel(ddl_Biz_Level)
        Cons.BindDDl_SafetyLevel(ddl_Safety_Level)
        Cons.BindDDl_MA_ActionDay(ddl_MA_Day)
        rdoBiz.Checked = False
        rdoSafety.Checked = False

        ddl_Level.Items(0).Text = ""
        Select Case TAG_CLASS
            Case EIR_BL.Tag_Class.Stationary
                BL.BindDDl_ST_INSPStatus(ddl_Status, TAG_TYPE_ID, INSP_ID)
            Case EIR_BL.Tag_Class.Rotating
                BL.BindDDl_RO_INSPStatus(ddl_Status, TAG_TYPE_ID, INSP_ID)
        End Select
        'UpdateTrace()
        WarningType = EIR_BL.Warning.Normal
    End Sub

    Public Sub BindData()

        ClearDetail()
        ''---------------- Bind Tag Detail ---------------
        Dim Sql As String = "SELECT * FROM VW_REPORT_" & ViewState("TagClass").ToString & "_DETAIL WHERE DETAIL_ID=" & DETAIL_ID

        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        DETAIL_ID = DT.DefaultView(0).Item("DETAIL_ID")
        INSP_ID = DT.DefaultView(0).Item("INSP_ID")
        INSP_NAME = DT.Rows(0).Item("INSP_NAME")

        If Not IsDBNull(DT.Rows(0).Item("CURRENT_STATUS_ID")) Then
            Select Case TAG_CLASS
                Case EIR_BL.Tag_Class.Stationary
                    BL.BindDDl_ST_INSPStatus(ddl_Status, TAG_TYPE_ID, INSP_ID, DT.DefaultView(0).Item("CURRENT_STATUS_ID"))
                Case EIR_BL.Tag_Class.Rotating
                    BL.BindDDl_RO_INSPStatus(ddl_Status, TAG_TYPE_ID, INSP_ID, DT.DefaultView(0).Item("CURRENT_STATUS_ID"))
            End Select
        Else
            Select Case TAG_CLASS
                Case EIR_BL.Tag_Class.Stationary
                    BL.BindDDl_ST_INSPStatus(ddl_Status, TAG_TYPE_ID, INSP_ID)
                Case EIR_BL.Tag_Class.Rotating
                    BL.BindDDl_RO_INSPStatus(ddl_Status, TAG_TYPE_ID, INSP_ID)
            End Select
        End If

        If Not IsDBNull(DT.Rows(0).Item("CURRENT_COMPONENT")) Then
            PARTNO = DT.Rows(0).Item("CURRENT_COMPONENT")
        Else
            PARTNO = ""
        End If

        Select Case INSP_ID
            Case 12
                If Not IsDBNull(DT.Rows(0).Item("CURRENT_LEVEL")) Then
                    BL.BindDDlVibrationZone(ddl_Level, DT.DefaultView(0).Item("CURRENT_LEVEL"), False)
                Else
                    BL.BindDDlVibrationZone(ddl_Level)
                End If
            Case Else
                If Not IsDBNull(DT.Rows(0).Item("CURRENT_LEVEL")) Then
                    BL.BindDDlLevel(ddl_Level, DT.DefaultView(0).Item("CURRENT_LEVEL"), False)
                Else
                    BL.BindDDlLevel(ddl_Level, , False)
                End If
        End Select

        If Not IsDBNull(DT.Rows(0).Item("CURRENT_PROB_DETAIL")) Then
            PROB_DETAIL = DT.Rows(0).Item("CURRENT_PROB_DETAIL")
        End If

        '----------------------------- Last Detail --------------------
        If Not IsDBNull(DT.Rows(0).Item("LAST_LEVEL")) Then
            LAST_LEVEL = DT.Rows(0).Item("LAST_LEVEL")
        End If
        If Not IsDBNull(DT.Rows(0).Item("LAST_STATUS_NAME")) Then
            LAST_STATUS_NAME = DT.Rows(0).Item("LAST_STATUS_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("LAST_DETAIL_ID")) Then
            LAST_DETAIL_ID = DT.Rows(0).Item("LAST_DETAIL_ID")
        End If


        ddl_Fixed.Visible = Trim(lbl_LastStatus.InnerHtml) <> ""
        If Not IsDBNull(DT.Rows(0).Item("CURRENT_Fixed")) Then
            If DT.Rows(0).Item("CURRENT_Fixed") Then
                Fixed = True
            Else
                Fixed = False
            End If
        Else
            Fixed = TriState.UseDefault
        End If

        If Not IsDBNull(DT.Rows(0).Item("CURRENT_BIZ_LEVEL")) Then
            BIZ_LEVEL = DT.Rows(0).Item("CURRENT_BIZ_LEVEL")
        End If
        If Not IsDBNull(DT.Rows(0).Item("CURRENT_SAFETY_LEVEL")) Then
            SAFETY_LEVEL = DT.Rows(0).Item("CURRENT_SAFETY_LEVEL")
        End If
        If Not IsDBNull(DT.Rows(0).Item("CUR_USE_CONS")) Then
            Select Case DT.Rows(0).Item("CUR_USE_CONS")
                Case EIR_Consequence.Consequence_Category.Business
                    rdoBiz.Checked = True
                Case EIR_Consequence.Consequence_Category.Safety
                    rdoSafety.Checked = True
            End Select
        End If
        If Not IsDBNull(DT.Rows(0).Item("CURRENT_MA_DAY")) Then
            MA_Day = DT.Rows(0).Item("CURRENT_MA_Day")
        End If

        If Not IsDBNull(DT.Rows(0).Item("REF_INSP_ID")) Then
            REF_INSP_ID = DT.Rows(0).Item("REF_INSP_ID")
        Else
            REF_INSP_ID = 0
        End If
        If Not IsDBNull(DT.Rows(0).Item("REF_STATUS_ID")) Then
            REF_STATUS_ID = DT.Rows(0).Item("REF_STATUS_ID")
        Else
            REF_STATUS_ID = 0
        End If

        Dim P1 As Object = DT.Rows(0).Item("CURRENT_PICTURE1")
        Dim P2 As Object = DT.Rows(0).Item("CURRENT_PICTURE2")
        P1 = Not IsDBNull(P1) AndAlso P1
        P2 = Not IsDBNull(P2) AndAlso P2

        '------------- Logic นี้ แก้ใน UDF_Calculate_Incomplete_Tag_Info_In_Report ด้วย------------
        If BL.IsInspectionRequirePicture(INSP_ID) Then
            '----------- Require Picture -----------
            If LAST_DETAIL_ID <> 0 Or
            CURRENT_LEVEL >= EIR_BL.InspectionLevel.Normal Or
            CURRENT_STATUS > 0 Or
            BIZ_LEVEL > 0 Or
            SAFETY_LEVEL > 0 Or
            USE_CONSEQUENCE > 0 Or
            MA_Day > 0 Or
            PROB_DETAIL <> "" Then
                '-------- ปัญหาเก่า และ ปัญหาใหม่ที่เริ่มมีการกรอก ----------
                If PROB_DETAIL = "" Or
                   CURRENT_LEVEL = -1 Or
                   CURRENT_STATUS = -1 Or
                   BIZ_LEVEL = 0 Or
                   SAFETY_LEVEL = 0 Or
                   USE_CONSEQUENCE = EIR_Consequence.Consequence_Category.Unknow Or
                   MA_Day = 0 Or
                   Not (P1 Or P2) Then '---------- ต้องมีอยา
                    '----------- พวกที่ต้อง Alert ----------
                    imgWarning.ToolTip = "Please upload image and more detail for this inspection point !!"
                    WarningType = EIR_BL.Warning.Alert
                ElseIf LAST_DETAIL_ID <> 0 And Fixed = TriState.UseDefault Then
                    '----------- แค่ Warning ----------
                    imgWarning.ToolTip = "Please select fixing status !!"
                    WarningType = EIR_BL.Warning.Warning
                End If
            End If

        Else '----------- Not Require Picture -----------
            '------------------ เชค logic ใหม่
            If INSP_ID = 11 Then '------------ Machine Running ----------------
                WarningType = EIR_BL.Warning.Normal
            ElseIf LAST_DETAIL_ID <> 0 Or
             CURRENT_LEVEL >= EIR_BL.InspectionLevel.Normal Or
             CURRENT_STATUS > 0 Or
             BIZ_LEVEL > 0 Or
             SAFETY_LEVEL > 0 Or
             USE_CONSEQUENCE > 0 Or
             MA_Day > 0 Or
             PROB_DETAIL <> "" Then
                '-------- ปัญหาเก่า และ ปัญหาใหม่ที่เริ่มมีการกรอก ทั้งสองกรณี----------
                If INSP_ID = 12 Then '-------------- Vibration Level--------------
                    If (LAST_DETAIL_ID <> 0 And (CURRENT_STATUS = -1 Or CURRENT_LEVEL = -1)) _
                        Or
                        (CURRENT_STATUS <> -1 And CURRENT_LEVEL < -1) _
                        Or
                        (CURRENT_STATUS = -1 And CURRENT_LEVEL >= -1) _
                        Or
                        BIZ_LEVEL = 0 Or SAFETY_LEVEL = 0 Or USE_CONSEQUENCE = EIR_Consequence.Consequence_Category.Unknow Or MA_Day = 0 _
                        Then
                        imgWarning.ToolTip = "Please insert more detail for this inspection point !!"
                        WarningType = EIR_BL.Warning.Alert
                    Else
                        WarningType = EIR_BL.Warning.Normal
                    End If
                ElseIf PROB_DETAIL = "" Or
                       CURRENT_LEVEL = -1 Or
                       BIZ_LEVEL = 0 Or
                       SAFETY_LEVEL = 0 Or
                       USE_CONSEQUENCE = EIR_Consequence.Consequence_Category.Unknow Or
                       MA_Day = 0 Or
                       CURRENT_STATUS = -1 Then
                    '----------- พวกที่ต้อง Alert ----------
                    imgWarning.ToolTip = "Please insert more detail for this inspection point !!"
                    WarningType = EIR_BL.Warning.Alert
                ElseIf LAST_DETAIL_ID <> 0 And Fixed = TriState.UseDefault Then
                    '----------- แค่ Warning ----------
                    imgWarning.ToolTip = "Please select fixing status !!"
                    WarningType = EIR_BL.Warning.Warning
                End If
            End If


        End If

        UpdateTrace()
        '---------------Hide Dialog-------------------------
        GL_DialogUploadImage1.CloseDialog()

    End Sub

    Private Sub btn_Autosave_Click(sender As Object, e As EventArgs) Handles btn_Autosave.Click
        SaveFromGrid()
    End Sub

    Private Sub SaveFromGrid()

        If CURRENT_LEVEL <= -1 Or CURRENT_STATUS <= 0 Then
            imgWarning.ToolTip = "Unable to autosave" & vbNewLine & "Please completed all require detail !!"
            WarningType = EIR_BL.Warning.Alert
            'Exit Sub
        End If

        Dim SQL As String
        '------------------ Check Detail_ID -------------
        If DETAIL_ID <> 0 Then
            SQL = "SELECT * FROM RPT_" & ViewState("TagClass").ToString & "_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Else
            SQL = "SELECT * FROM RPT_" & ViewState("TagClass").ToString & "_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID
        End If

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count > 0 Then
            DR = DT.Rows(0)
        Else
            DR = DT.NewRow
            Select Case TAG_CLASS
                Case EIR_BL.Tag_Class.Stationary
                    DR("DETAIL_ID") = BL.Get_New_ST_DetailID
                Case EIR_BL.Tag_Class.Rotating
                    DR("DETAIL_ID") = BL.Get_New_RO_DetailID
            End Select
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("TAG_TYPE_ID") = TAG_TYPE_ID
            DR("INSP_ID") = INSP_ID
            DR("LAST_DETAIL_ID") = LAST_DETAIL_ID
        End If
        If CURRENT_STATUS > -1 Then
            DR("STATUS_ID") = CURRENT_STATUS
        Else
            DR("STATUS_ID") = DBNull.Value
        End If
        If ddl_Level.SelectedIndex > 0 Then
            DR("ICLS_ID") = ddl_Level.Items(ddl_Level.SelectedIndex).Value
        Else
            DR("ICLS_ID") = DBNull.Value
        End If

        If ddl_Biz_Level.SelectedIndex > 0 Then
            DR("BIZ_LEVEL") = BIZ_LEVEL
        Else
            DR("BIZ_LEVEL") = DBNull.Value
        End If

        If ddl_Safety_Level.SelectedIndex > 0 Then
            DR("SAFETY_LEVEL") = SAFETY_LEVEL
        Else
            DR("SAFETY_LEVEL") = DBNull.Value
        End If

        If USE_CONSEQUENCE = EIR_Consequence.Consequence_Category.Unknow Then
            DR("USE_CONS") = DBNull.Value
        Else
            DR("USE_CONS") = USE_CONSEQUENCE
        End If


        If ddl_MA_Day.SelectedIndex > 0 Then
            DR("MA_Day") = ddl_MA_Day.Items(ddl_MA_Day.SelectedIndex).Value
        Else
            DR("MA_Day") = DBNull.Value
        End If

        DR("COMP_NO") = PARTNO

        If ddl_Fixed.Visible Then
            Select Case Fixed
                Case TriState.UseDefault
                    DR("Fixed") = DBNull.Value
                Case Else
                    DR("Fixed") = CBool(Math.Abs(Fixed))
            End Select
        Else
            DR("Fixed") = DBNull.Value
        End If

        DR("PROB_Detail") = PROB_DETAIL

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            '----------- If Error --------------
            imgWarning.ToolTip = "Unable to autosave" & vbNewLine & "Please completed all require detail !!"
            WarningType = EIR_BL.Warning.Alert
            Exit Sub
        End Try

        DETAIL_ID = DR("DETAIL_ID")

        Dim GLT As Object = Me.Parent.Parent.Parent.Parent.Parent.Parent
        Me.BindData()
        GLT.BindHeader()

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Select Case TAG_CLASS
            Case EIR_BL.Tag_Class.Stationary
                BL.Drop_RPT_ST_Detail_CheckFlag(DETAIL_ID)
            Case EIR_BL.Tag_Class.Rotating
                BL.Drop_RPT_RO_Detail_CheckFlag(DETAIL_ID)
        End Select
        Dim GLT As Object = Me.Parent.Parent.Parent.Parent.Parent.Parent
        GLT.BindHeader()
        GLT.IsExpand = GLT.IsExpand
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEdit.Click
        Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
        GL_DialogUploadImage1.UNIQUE_POPUP_ID = UNIQUEKEY
        GL_DialogUploadImage1.TAG_CLASS = TAG_CLASS

        Dim SQL As String = ""
        If DETAIL_ID <> 0 Then
            GL_DialogUploadImage1.ShowDialog(DETAIL_ID)
        ElseIf LAST_DETAIL_ID <> 0 Then
            SQL = "SELECT * FROM RPT_" & ViewState("TagClass").ToString & "_Detail WHERE DETAIL_ID=" & LAST_DETAIL_ID
            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count = 0 Then
                GL_DialogUploadImage1.ShowDialog(RPT_Year, RPT_No, TAG_ID)
            Else
                GL_DialogUploadImage1.ShowDialog(RPT_Year, RPT_No, TAG_ID)
                GL_DialogUploadImage1.LAST_DETAIL_ID = LAST_DETAIL_ID
            End If
        Else
            GL_DialogUploadImage1.ShowDialog(RPT_Year, RPT_No, TAG_ID)
        End If

    End Sub

    Protected Sub GL_DialogUploadImage1_UpdateCompleted(ByRef sender As GL_DialogUploadImage) Handles GL_DialogUploadImage1.UpdateCompleted
        Dim GLT As Object = Me.Parent.Parent.Parent.Parent.Parent.Parent
        GLT.BindHeader()
        GLT.IsExpand = GLT.IsExpand
    End Sub

    Protected Sub lbl_LastStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_LastStatus.Click
        GL_DialogUploadImage1.UNIQUE_POPUP_ID = UniqueID
        GL_DialogUploadImage1.TAG_CLASS = Me.TAG_CLASS
        GL_DialogUploadImage1.ShowDialog(LAST_DETAIL_ID)
        GL_DialogUploadImage1.Disabled = True
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lbl_LastStatus.Attributes("onclick") = "document.getElementById('" & btn_LastStatus.ClientID & "').click();"

        txtPart.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        ddl_Level.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        ddl_Biz_Level.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        ddl_MA_Day.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        ddl_Fixed.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        ddl_Status.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        'txt_Detail.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        ddl_Safety_Level.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        rdoBiz.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
        rdoSafety.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"
    End Sub


End Class