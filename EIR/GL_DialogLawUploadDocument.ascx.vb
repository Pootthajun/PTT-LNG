﻿Imports System.IO
Imports System.Data.SqlClient


Public Class GL_DialogLawUploadDocument
    Inherits System.Web.UI.UserControl

    Dim CL As New LawClass
    Public Event SaveCompleted(ByRef sender As GL_DialogLawUploadDocument)


#Region "Dynamic Property"
    Public ReadOnly Property FOLDER_ID As Integer
        Get
            Return lblFolderID.Text
        End Get
    End Property
    Public ReadOnly Property DOCUMENT_ID As Integer
        Get
            Return lblDocumentID.Text
        End Get
    End Property


#End Region




    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Public Sub CloseDialog()
        lblAction.Text = ""
        lblDocPath.Text = ""
        lblFolderID.Text = "0"
        lblDocumentID.Text = "0"
        txt_DocumentName.Text = ""
        txt_Description.Text = ""
        lblTempFilePath.Text = ""
        lblUploadFileName.Text = ""
        txt_Notice_Date.Text = ""
        txt_Critical_Date.Text = ""
        lblActualDate.Text = ""

        dialogLowDocument.Hide()
    End Sub

    Public Sub ShowDialogAddDocument(FolderID As Integer)
        'Disabled = False
        lblDocPath.Text = CL.GetFolderPath(FolderID)
        lblAction.Text = "New "
        lblFolderID.Text = FolderID

        Dim DT As New DataTable
        DT.Columns.Add("file_path")
        DT.Columns.Add("file_name")
        DT.Columns.Add("file_id")

        pnlValidation.Visible = False

        dialogLowDocument.Show()
    End Sub

    Public Sub ShowDialogEditDocument(DocumentID As Integer)
        'Disabled = False

        Dim dt As DataTable = CL.GetDocumentDetail(DocumentID)
        If dt.Rows.Count > 0 Then
            lblAction.Text = "Edit "
            lblDocumentID.Text = DocumentID
            lblFolderID.Text = dt.Rows(0)("folder_id")
            lblDocPath.Text = CL.GetFolderPath(lblFolderID.Text)
            txt_DocumentName.Text = dt.Rows(0)("document_name")
            If Convert.IsDBNull(dt.Rows(0)("document_description")) = False Then txt_Description.Text = dt.Rows(0)("document_description")

            If Convert.IsDBNull(dt.Rows(0)("file_path")) = False AndAlso File.Exists(dt.Rows(0)("file_path")) = True Then
                lblTempFilePath.Text = dt.Rows(0)("file_path")
                lblUploadFileName.Text = dt.Rows(0)("original_file_name")
            End If

            If Convert.IsDBNull(dt.Rows(0)("notice_date")) = False Then txt_Notice_Date.Text = Convert.ToDateTime(dt.Rows(0)("notice_date")).ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US"))
            If Convert.IsDBNull(dt.Rows(0)("critical_date")) = False Then txt_Critical_Date.Text = Convert.ToDateTime(dt.Rows(0)("critical_date")).ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US"))
            If Convert.IsDBNull(dt.Rows(0)("actual_date")) = False Then lblActualDate.Text = Convert.ToDateTime(dt.Rows(0)("actual_date")).ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US"))
        End If
        dt.Dispose()
        dialogLowDocument.Show()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False

        If IsPostBack = False Then
            Dim TempPath As String = CL.UploadTempPath() & Session("USER_Name") & "\"
            If Directory.Exists(TempPath) = True Then
                Directory.Delete(TempPath, True)
            End If
        End If
    End Sub

    'Private Sub rptFileList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptFileList.ItemDataBound
    '    If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

    '    Dim lblFileName As Label = e.Item.FindControl("lblFileName")
    '    Dim lblFilePath As Label = e.Item.FindControl("lblFilePath")
    '    Dim lblFileID As Label = e.Item.FindControl("lblFileID")
    '    Dim lblNo As Label = e.Item.FindControl("lblNo")
    '    Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

    '    lblNo.Text = e.Item.ItemIndex + 1
    '    If Not IsDBNull(e.Item.DataItem("file_path")) Then
    '        lblFilePath.Text = e.Item.DataItem("file_path").ToString
    '    End If
    '    If Not IsDBNull(e.Item.DataItem("file_name")) Then
    '        lblFileName.Text = e.Item.DataItem("file_name").ToString
    '    End If

    '    lblFileID.Text = e.Item.DataItem("file_id").ToString

    'End Sub

    'Private Sub rptFileList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptFileList.ItemCommand
    '    Select Case e.CommandName
    '        Case "Delete"
    '            Dim DT As DataTable = CurrentFileList()
    '            Try
    '                Dim FileID As Long = Convert.ToInt64(DT.Rows(e.Item.ItemIndex)("file_id"))
    '                If FileID > 0 Then
    '                    CL.DeleteFile(FileID)
    '                End If

    '                DT.Rows(e.Item.ItemIndex).Delete()
    '                DT.AcceptChanges()
    '            Catch : End Try
    '            rptFileList.DataSource = DT
    '            rptFileList.DataBind()
    '    End Select
    '    dialogLowDocument.Show()
    'End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        lblValidation.Text = ""
        pnlValidation.Visible = False
        dialogLowDocument.Show()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txt_DocumentName.Text.Trim = "" Then
            lblValidation.Text = "Please insert Document Name"
            pnlValidation.Visible = True
            dialogLowDocument.Show()
            Exit Sub
        End If

        Dim c As New Converter
        Dim NoticeDate As DateTime
        Dim CriticalDate As DateTime

        Dim cl As New LawClass
        If ful.HasFile = False Then
            If txt_Notice_Date.Text.Trim = "" Then
                lblValidation.Text = "Please select Notice Date"
                pnlValidation.Visible = True
                dialogLowDocument.Show()
                Exit Sub
            End If

            If txt_Critical_Date.Text.Trim = "" Then
                lblValidation.Text = "Please select Critical Date"
                pnlValidation.Visible = True
                dialogLowDocument.Show()
                Exit Sub
            End If

            NoticeDate = c.StringToDate(txt_Notice_Date.Text, "dd-MMM-yyyy")
            CriticalDate = c.StringToDate(txt_Critical_Date.Text, "dd-MMM-yyyy")
            If NoticeDate.Date >= CriticalDate.Date Then
                lblValidation.Text = "Notice Date is not before Critical Date"
                pnlValidation.Visible = True
                dialogLowDocument.Show()
                Exit Sub
            End If
        Else
            'ถ้ามีการ Upload File
            Dim upFileName As String = ful.FileName

            Dim TempPath As String = cl.UploadTempPath() & Session("USER_Name") & "\"
            If Directory.Exists(TempPath) = False Then
                Directory.CreateDirectory(TempPath)
            End If

            Dim FilePath As String = TempPath & upFileName
            If File.Exists(FilePath) = True Then
                'ถ้ามีไฟล์เดิมอยู่แล้ว ก็ลบออกก่อน
                File.Delete(FilePath)
            End If

            'บันทึกไฟล์ที่ทำการ Upload
            ful.SaveAs(FilePath)

            If File.Exists(FilePath) = True Then
                lblTempFilePath.Text = FilePath
                lblUploadFileName.Text = upFileName
            End If
        End If


        Dim LawDocumentPath As String = cl.LawDocumentPath()
        If Directory.Exists(LawDocumentPath) = False Then
            Directory.CreateDirectory(LawDocumentPath)
        End If

        Dim ret() As String = Split(cl.SaveLawDocument(lblDocumentID.Text, lblFolderID.Text, txt_DocumentName.Text, txt_Description.Text, lblUploadFileName.Text, NoticeDate, CriticalDate, Now, Session("USER_ID"), lblTempFilePath.Text), "|")
        If ret.Length = 2 Then
            If ret(0).ToLower = "true" Then
                lblDocumentID.Text = ret(1)
                RaiseEvent SaveCompleted(Me)
                CloseDialog()
            Else
                lblValidation.Text = ret(1)
                pnlValidation.Visible = True
                dialogLowDocument.Show()
            End If
        Else
            lblValidation.Text = ret(0)
            pnlValidation.Visible = True
            dialogLowDocument.Show()
        End If
    End Sub
End Class