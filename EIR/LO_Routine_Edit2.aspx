﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LO_Routine_Edit2.aspx.vb" Inherits="EIR.LO_Routine_Edit2" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>Edit Lube Oil Report</h2>
			
            <div class="clear"></div> <!-- End .clear -->		
		
			;<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" CssClass="default-tab current">Report 
                            Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:100px;" >Report for: </label>
								    <asp:Label ID="lbl_Month" runat="server" Text="Month" CssClass="EditReportHeader" ></asp:Label> 
								     <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								     (<asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ForeColor="Gray"></asp:Label>)
								</p>	
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear
							          </span>
							        </asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete current information permanently?">
                                      </cc1:ConfirmButtonExtender>
								  <%--<li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>	--%>							  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>								
                            <br><br>
                            </fieldset>
                            <asp:Panel ID="pnlGrid" runat="server">
                            
                            
                                 <table>
                                    <thead>
                                        <tr>
                                            <th style="text-align:center; ">
                                                Area
                                            </th>
                                            <th style="text-align:center; ">
                                                Tag No.
                                            </th>
                                            <%--<th align="center">
                                                Tag Name
                                            </th>--%>
                                            <th style="text-align:center; ">
                                                Type
                                            </th>
                                            <th style="text-align:center; ">
                                                Oil Type
                                            </th>
                                            <th style="text-align:center; ">
                                                TAN
                                            </th>
                                            <th style="text-align:center; ">
                                                Oxidation/Anti-Oxidation
                                            </th>
                                            <th style="text-align:center; " id="HeaderVanish" runat="server">
                                                Varnish
                                            </th>
                                            <th style="text-align:center; ">
                                                Particles Count
                                            </th>
                                            <th style="text-align:center; ">
                                                Water
                                            </th>
                                            <th style="text-align:center; ">
                                                &nbsp;
                                            </th>
                                            <th style="text-align:center; ">
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                         <tbody>
                                            <asp:Repeater ID="rptTAG" runat="server" >
                                                <ItemTemplate>
                                                        <tr>
                                                            <td id="td1" runat="server" style="text-align:center; ">
                                                                <asp:LinkButton ID="lblArea" Text="" runat="server" ForeColor="Teal"></asp:LinkButton>
                                                            </td>
                                                            <td id="td2" runat="server" style="border-left:1px solid #eeeeee;">
                                                                <asp:LinkButton ID="lblNo" Text="" runat="server" Font-Bold="True"></asp:LinkButton>
                                                            </td>
                                                           
                                                           <td id="td4" runat="server" style="text-align:center; ">
                                                                <asp:LinkButton ID="lblType" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td5" runat="server" style="text-align:center; ">
                                                                <asp:LinkButton ID="lblOil" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td6" runat="server" style="text-align:right; ">
                                                                <asp:LinkButton ID="lblTAN" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td7" runat="server" style="text-align:center; ">
                                                                <asp:LinkButton ID="lblOx" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td8" runat="server" style="text-align:center; ">
                                                                <asp:LinkButton ID="lblVarnish" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td9" runat="server" style="text-align:center; ">
                                                                <asp:LinkButton ID="lblPart" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td10" runat="server" style="text-align:right; ">
                                                                <asp:LinkButton ID="lblWater" Font-Size="10px" Text="" runat="server" ForeColor="#666666"></asp:LinkButton>
                                                            </td>
                                                            <td id="td3" runat="server" style="text-align:center; ">
                                                                <asp:ImageButton ID="btnInComplete" runat="server" ImageUrl="resources/images/icons/alert.gif" ToolTip="Incomplete"></asp:ImageButton>                                                               
                                                                
                                                            </td>
                                                            <td id="td11" runat="server" style="text-align:center; ">
                                                                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="resources/images/icons/pencil.png" BorderStyle="None" CommandName="Edit" />
                                                                <a ID="btnReport" runat="server" href="javascript:;"><img src="resources/images/icons/printer.png" border="0"></a>
                                                            </td>
                                                        </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            
                                     </tbody>
                                 </table>
                             </asp:Panel>
                             
                             <asp:Panel ID="pnlEdit" runat="server" >                              
                                    <table style="width:100%; border-top:1px solid #cccccc;">
                                        <tr>
                                            <td style="vertical-align:top;">
                                                <p style="font-weight:bold;">
								                    <label class="column-left" style="width:150px; color:Gray;">Equipement Name: </label>
								                    <asp:Label ID="lblEquipement" runat="server"></asp:Label>
								                </p>
								                <p style="font-weight:bold;">
								                    <label class="column-left" style="width:150px; color:Gray;" >Tag No: </label>
								                    <asp:Label ID="lblTagNo" runat="server"></asp:Label>
								                </p>
								                <p style="font-weight:bold;">
								                    <label class="column-left" style="width:150px; color:Gray;" >Area: </label>
								                    <asp:Label ID="Label1" runat="server"></asp:Label>
								                </p>
								                <p style="font-weight:bold;">
								                    <label class="column-left" style="width:150px; color:Gray;" >Oil Type: </label>
								                    <asp:Label ID="lblOilType" runat="server"></asp:Label>
								                </p>
								                <p>
									                <label class="column-left" style="width:150px;" >Suggession: </label>
									                 <asp:TextBox Width="100%" CssClass="text-input" ID="txt_Suggess" TextMode="MultiLine" Height="60px" MaxLength="1000" runat="server"></asp:TextBox>									
									                <asp:Button ID="btnUpdateDetail" runat="server" style="display:none;" />									                
								                </p>
                                            </td>
                                            <td style="text-align:right;">
                                              <asp:Image ID="imgRadar" runat="server" Width="300px" Height="250px" />
                                            </td>
                                        </tr>
                                        
                                    </table>
								    <style type="text/css">
								        .tbEdit{
								            border:1px solid #CCCCCC;
								            text-align:center;
								            vertical-align:middle;
								            background-color:White;
								        }
								        .tbEdit_td
								        {
								            border:1px solid #CCCCCC;
								            text-align:center;
								            vertical-align:middle;
								        }
								        .tbEdit_th
								        {
								            border:1px solid #CCCCCC;
								            text-align:center;
								            font-weight:bold;
								        }
								    </style>	    
								    <table class="tbEdit" id="tb_Oil_Condition" runat="server">
								    <thead>
                                        <tr>
                                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                                Oil Condition
                                            </th>
                                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                                Unit
                                            </th>
                                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                                Current Sample
                                            </th>
                                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                                Sample Date
                                            </th>
                                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                                Standard<br>Practice
                                            </th>
                                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                                Alarm Limit
                                            </th>                                            
                                        </tr>
                                    </thead>
                                        
                                            <tr id="tr_TAN" runat="server">
                                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_TAN" runat="server">TAN</td>
                                                <td style="width:100px;" class="tbEdit_td">mgKOH/g</td>
                                                <td id="td_TAN_Value" runat="server" class="tbEdit_td"><asp:TextBox ID="txt_TAN_Value" MaxLength="10" runat="server" style="border-width:0px; width:50px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox></td>
                                                <td id="td_TAN_Date" runat="server" class="tbEdit_td">
                                                    <asp:TextBox ID="txt_TAN_Date" runat="server" style="border-width:0px;  width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_TAN_Date_Calendar" runat="server" 
                                                    Format="yyyy-MM-dd" TargetControlID="txt_TAN_Date" PopupPosition="Right"></cc1:CalendarExtender>
                                                </td>
                                                <td style="width:100px;" class="tbEdit_td">ASTM E2412</td>
                                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/TAN_Table.png" alt="Rolls-Royce Gas generator" style="width:400px; cursor:pointer;" onclick="window.open(this.src);"/></td>
                                            </tr>
                                            <tr id="tr_OX" runat="server">
                                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_OX_Type" runat="server">                                                    
                                                </td>
                                                <td style="width:100px;"  class="tbEdit_td">
                                                    <asp:Label ID="lbl_OX_UNIT" runat="server"></asp:Label>
                                                </td>
                                                <td class="tbEdit_td" id="td_OX_Value" runat="server" >
                                                    <asp:TextBox ID="txt_OX_Value" MaxLength="10"  runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox>
                                                 </td>
                                                <td style="width:100px;" id="td_OX_Date" runat="server" class="tbEdit_td">
                                                    <asp:TextBox ID="txt_OX_Date" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_OX_Date_Calendar" runat="server" 
                                                    Format="yyyy-MM-dd" TargetControlID="txt_OX_Date" PopupPosition="Right"></cc1:CalendarExtender>
                                                </td>
                                                <td style="width:100px;" class="tbEdit_td">ASTM E2412</td> 
                                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/Oxidation_Table.png" id="imgOxidation" runat="server" alt="Oxidation for Mineral Oil" style="cursor:pointer;" onclick="window.open(this.src);"/></td>                                               
                                            </tr>
                                            <tr id="tr_Varnish" runat="server">
                                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_Varnish" runat="server">Varnish</td>
                                                <td style="width:100px;" class="tbEdit_td">-</td>
                                                <td id="td_Varnish_Value" runat="server" class="tbEdit_td">
                                                    <asp:TextBox ID="txt_Varnish_Value" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                                <td style="width:100px;" id="td_Varnish_Date" runat="server" class="tbEdit_td">
                                                    <asp:TextBox ID="txt_Varnish_Date" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_Varnish_Date_Calendar" runat="server" 
                                                    Format="yyyy-MM-dd" TargetControlID="txt_Varnish_Date" PopupPosition="Right"></cc1:CalendarExtender>
                                                </td>
                                                <td style="width:100px;" class="tbEdit_td">ASTM D7843</td>
                                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/Varnish_Table.png" alt="Varnish Criteria" style="width:400px; cursor:pointer;" onclick="window.open(this.src);"/></td>
                                            </tr>                                            
                                    </table>
                                    <table class="tbEdit" id="tb_Contamination" runat="server">
								    <thead>
                                        <tr>
                                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                                Contamination
                                            </th>
                                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                                Unit
                                            </th>
                                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                                Current Sample
                                            </th>
                                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                                Sample Date
                                            </th>
                                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                                Standard<br>Practice
                                            </th>
                                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                                Alarm Limit
                                            </th>                                            
                                        </tr>
                                    </thead>
                                            <tr id="tr_PART_COUNT" runat="server">
                                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_PART_COUNT" runat="server">Particles Count</td>
                                                <td style="width:100px;" class="tbEdit_td">Pcs.</td>
                                                <td id="td_PART_COUNT_Value" runat="server" class="tbEdit_td">
                                                       <asp:DropDownList ID="ddl_PART_COUNT_Value" runat="server" AutoPostBack="true" style="border:none; background-color:Transparent;">
                                                        <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="NAS 1"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="NAS 2"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="NAS 3"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="NAS 4"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="NAS 5"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="NAS 6"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="NAS 7"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="NAS 8"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="NAS 9"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="NAS 10"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="NAS 11"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="NAS 12"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width:100px;" id="td_PART_COUNT_Date" runat="server" class="tbEdit_td">
                                                    <asp:TextBox ID="txt_PART_COUNT_Date" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_PART_COUNT_Date_Calendar" runat="server" 
                                                    Format="yyyy-MM-dd" TargetControlID="txt_PART_COUNT_Date" PopupPosition="Right"></cc1:CalendarExtender>
                                                </td>
                                                <td style="width:100px;" class="tbEdit_td">NAS 1638 / <br>ISO 4406</td>
                                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/Particle_Table.png" alt="Maximum Number of Particles Per 100 Milimeters (Counts per 100 ml)" style="width:400px; cursor:pointer;" onclick="window.open(this.src);"/></td>
                                            </tr>     
                                            <tr id="tr_Water" runat="server">
                                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_Water" runat="server">Water</td>
                                                <td style="width:100px;" class="tbEdit_td">
                                                    ppm
                                                </td>
                                                <td id="td_Water_Value" runat="server" class="tbEdit_td"><asp:TextBox ID="txt_Water_Value" MaxLength="10"  runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;" AutoPostBack="true"></asp:TextBox></td>
                                                <td style="width:100px;" id="td_Water_Date" runat="server" class="tbEdit_td">
                                                    <asp:TextBox ID="txt_Water_Date" runat="server" style="border-width:0px; width:100px; text-align:center;" AutoPostBack="true"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_Water_Date_Calendar" runat="server" 
                                                    Format="yyyy-MM-dd" TargetControlID="txt_Water_Date" PopupPosition="Right"></cc1:CalendarExtender>
                                                </td>
                                                <td style="width:100px;" class="tbEdit_td">ASTM E2412</td>
                                                <td class="tbEdit_td">
                                                    <img src="resources/images/LO/Water_Mineral_Table.png" id="imgWater1" runat="server" alt="Water for Mineral Oil" style="cursor:pointer;" onclick="window.open(this.src);"/>
                                                    <img src="resources/images/LO/Water_Synthetic_Table.png" id="imgWater2" runat="server" alt="Water for Synthetic Oil" style="cursor:pointer;" onclick="window.open(this.src);"/>
                                                </td>                                                 
                                            </tr>
                                    </table>
                             </asp:Panel> 
                             
							
							<p align="right">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>
								
				    </div>
		       	 <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>	         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
</ContentTemplate>
</asp:UpdatePanel>  
	 
</asp:Content>