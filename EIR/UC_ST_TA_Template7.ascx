﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_Template7.ascx.vb" Inherits="EIR.UC_ST_TA_Template7" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_TextEditor.ascx" tagname="UC_TextEditor" tagprefix="uc1" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<%--------------------------------------------------------------------------------------%>
  <style>
        /*form textarea,*/
        form .wysiwyg {
            padding: 6px;
            font-size: 13px;
            /*border: 1px solid #d5d5d5;*/
            color: #333;
            border-style: none;
            background: none;
        }
    </style>

<tr  style="text-align: center">
    <td  style ="vertical-align :top;width: 50%;">
        <table >
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td colspan ="2" style =" height: 900px;" >
                    <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview1"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :100% ;" />

                    <asp:Button ID="btnRefreshImage" runat="server" Text="Update/Refresh" Style="visibility: hidden; width: 0px;" />

                </td>
            </tr>
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align :top ;width:80px" class="auto-style1">Condition </td>
                <td colspan="1" style="border-top-color: #C0C0C0; border-left-color: #C0C0C0; border-width: thin; text-align: right; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-bottom-color: #C0C0C0; background-color: #f9fefe;">
                    <asp:TextBox  CssClass ="wysiwyg" ID="txtDetail_Pic1" TextMode="multiline" Rows="3" runat="server" MaxLength="1000"></asp:TextBox>
                </td>
            </tr>
        </table>
    </td>
    <td  style ="vertical-align :top ">
        <table style ="vertical-align :top " >
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td  colspan ="2"  style =" height: 400px;"  >
                    <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview3"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />
                </td>
            </tr>
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align :top ;width:80px" class="auto-style1">Condition </td>
             <td colspan="1" style="border-top-color: #C0C0C0; border-left-color: #C0C0C0; border-width: thin; text-align: right; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-bottom-color: #C0C0C0; background-color: #f9fefe;">
                    <asp:TextBox  CssClass ="wysiwyg" ID="txtDetail_Pic3" TextMode="multiline" Rows="3" runat="server" MaxLength="1000"></asp:TextBox>
                </td></tr>

            </table>
            <table style ="vertical-align :top ;margin-top :30px " >
            <tr   style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td  colspan ="2"  style =" height: 400px;"  >
                     <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview4"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />

                </td>
            </tr>
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align :top ;width:80px"  class="auto-style1">Condition </td>
                <td colspan="1" style="border-top-color: #C0C0C0; border-left-color: #C0C0C0; border-width: thin; text-align: right; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-bottom-color: #C0C0C0; background-color: #f9fefe;">
                    <asp:TextBox CssClass ="wysiwyg" ID="txtDetail_Pic4" TextMode="multiline" Rows="3" runat="server" MaxLength="1000"></asp:TextBox>
                </td></tr>
        </table>
    </td>

</tr>




<asp:Panel ID="pnlFormPic" runat ="server" Visible ="false" >

<tr id="trimage_1" runat="server" visible="false" style="text-align: center">
    <td style="width: 50%; background-color: black;" rowspan ="2">
        <div class="thumbnail">


            <a id="lnk_File_Dialog1" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File1" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>

    <td style="width: 50%; background-color: black;">
        <div class="thumbnail">


            <a id="lnk_File_Dialog3" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File3" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>

</tr>
<%--------------------------------------------------------------------------------------%>
<tr id="trbtn_1" runat="server" visible="false">
    <td style="width: 100%;" class="toolbar">
        <asp:Button ID="btnUpload1" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete1"></cc1:ConfirmButtonExtender>
    </td>  
     <td style="width: 100%;" class="toolbar">
        <asp:Button ID="btnUpload3" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit3" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete3" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete3_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete3"></cc1:ConfirmButtonExtender>
    </td>
</tr>
<!-- End Table Images -->
<%--------------------------------------------------------------------------------------%>
<tr  style="text-align: center">
    

 <td style="width: 50%; background-color: black;text-align: center;" >
      <%-- <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview4"  Width="100%"  runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :50% ;max-height : 400px;min-height :50% ;" />--%>
</td> 

</tr>
<tr id="trimage_2" runat="server" visible="false" style="text-align: center">
 
    <td style="width: 50%; background-color: black;"  >
        <div class="thumbnail">


            <a id="lnk_File_Dialog4" runat="server" target="_blank" title="Figure No.2">
                <asp:Image ID="img_File4" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>
</tr>
<tr id="trbtn_2" runat="server" visible="false">
 
    <td style="width: 100%;" class="toolbar" >
        <asp:Button ID="btnUpload4" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit4" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete4" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete4_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete4"></cc1:ConfirmButtonExtender>
    </td>
</tr>


</asp:Panel>