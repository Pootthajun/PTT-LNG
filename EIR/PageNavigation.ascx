﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PageNavigation.ascx.vb" Inherits="EIR.PageNavigation" %>
<div class="pagination"> 
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
<asp:LinkButton ID="btnFirst" SourceName="" PageSize="20" CurrentPage="0" MaximunPageCount="4" runat="server" Text="< First" ToolTip="First Page"></asp:LinkButton>
<asp:LinkButton ID="btnBack" runat="server" Text="< Previous" ToolTip="Previous Page"></asp:LinkButton>
<asp:Repeater ID="rptPage" runat="server">
    <ItemTemplate>
     <asp:LinkButton ID="btnPage" runat="server" Text="1" ToolTip="Goto Page 1"></asp:LinkButton>
    </ItemTemplate>
</asp:Repeater>
<asp:LinkButton ID="btnNext" runat="server" Text="Next >" ToolTip="Next Page"></asp:LinkButton>
<asp:LinkButton ID="btnLast" runat="server" Text="Last >" ToolTip="Last Page"></asp:LinkButton>
</ContentTemplate>
</asp:UpdatePanel>  
</div>
