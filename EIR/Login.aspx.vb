﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Login
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Dim CurrentVersion As String = ConfigurationManager.AppSettings.Item("CurrentVersion").ToString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtUserName.Text = ""
            txtPassword.Text = ""
            btnLogin.OnClientClick = "$('#lblError').html(""<font style='color:green; '> ... Processing ... </font>"");"
            lblVersion.Text = CurrentVersion
        End If

        '--------------- RestoreJQuery -------------
        Dim Script As String = "$('.bxslider').bxSlider({infiniteLoop: true,auto: true,pager: true });"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RestoreJQuery", Script, True)
        lblError.Text = ""
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        If txtUserName.Text = "admin" And txtPassword.Text = "1qaz@WSX" Then
            Session("USER_ID") = "0"
            Session("User_Login") = "admin"
            Session("USER_Full_Name") = "EIR Administrator"
            Session("USER_Name") = "PID"
            Session("USER_LEVEL") = 0
            Response.Redirect("Default.aspx")
            Exit Sub
        End If

        'Dim SQL As String = "Select * FROM MS_User WHERE USER_Login='" & Replace(txtUserName.Text, "'", "''") & "'"
        'Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        'Dim DT As New DataTable
        'DA.Fill(DT)

        'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Select MS_User');", True)
        'Exit Sub

        'If DT.Rows.Count = 0 Then
        '    LoginFromDomain()
        '    Exit Sub
        'End If

        'If DT.Rows(0).Item("USER_Password") <> txtPassword.Text Then
        '    If Not LoginFromDomain() Then '------------- Check From Domain Again ----------------
        '        Exit Sub
        '    End If
        'End If

        'If Not DT.Rows(0).Item("Active_Status") Then
        '    lblError.Text = "This username is unavailable !!"
        '    Exit Sub
        'End If

        'DT.Rows(0).Item("Last_Login") = Now
        'Dim cmd As New SqlCommandBuilder(DA)
        'DA.Update(DT)

        'Session("USER_ID") = DT.Rows(0).Item("USER_ID")
        'Session("User_Login") = DT.Rows(0).Item("USER_Login")
        'Session("USER_Full_Name") = DT.Rows(0).Item("USER_Prefix") & DT.Rows(0).Item("USER_Name") & " " & DT.Rows(0).Item("USER_Surname")
        'Session("USER_Name") = DT.Rows(0).Item("USER_Name")
        'Select Case DT.Rows(0).Item("Gender").ToString
        '    Case "M"
        '        Session("Gender") = "Mr."
        '    Case "F"
        '        Session("Gender") = "Ms."
        'End Select

        'Session("USER_LEVEL") = DT.Rows(0).Item("LEVEL_ID")

        'Response.Redirect("Default.aspx")


        Dim SQL As String = ""
        Dim MS_PTT_User As DBStructure = Nothing
        Dim MS_User As DBStructure = Nothing
        '------ Check LDAP
        Dim Profile As PTT_AD.DomainUserResult = GetLDAPProfile()
        '------ If Exists LDAP KeepLog
        If Profile.IsEnabled Then '--------- Login ผ่าน LDAP ----------------
            '----------Collect PTT User---------
            MS_PTT_User = GetPTTUserDB(Profile.Username)
            StorePTTProfile(Profile, MS_PTT_User)

            '------------EIR User----------
            MS_User = GetEIRUserDB(txtUserName.Text)
            Dim DR As DataRow
            If MS_User.Table.Rows.Count > 0 Then
                DR = MS_User.Table.Rows(0)
            Else
                DR = MS_User.Table.NewRow
                DR("USER_ID") = GetNewEIRUserID()
                DR("User_Prefix") = Profile.Initials

                DR("Active_Status") = True
                DR("LEVEL_ID") = 4
                MS_User.Table.Rows.Add(DR)
            End If
            DR("User_Name") = Profile.FirstName
            DR("User_Surname") = Profile.LastName
            If Profile.Initials.ToUpper.IndexOf("MR") > -1 Then
                DR("Gender") = "M"
            ElseIf Profile.Initials.ToUpper.IndexOf("MRS") > -1 Or Profile.Initials.ToUpper.IndexOf("MISS") > -1 Or Profile.Initials.ToUpper.IndexOf("MS") > -1 Then
                DR("Gender") = "F"
            End If
            'DR("POS_ID") = ddl_Edit_Position.Items(ddl_Edit_Position.SelectedIndex).Value
            DR("USER_Login") = Profile.Username
            DR("USER_Password") = Profile.Password
            DR("Last_Login") = Now
            DR("Update_By") = 0
            DR("Update_Time") = Now
            Dim _cmd As New SqlCommandBuilder(MS_User.Adapter)
            MS_User.Adapter.Update(MS_User.Table)
            MS_User.Table.AcceptChanges()

            If Not DR("Active_Status") Then
                lblError.Text = "This account is disabled."
                Exit Sub
            End If

            '-------- Redirect Goto EIR -------------
            Session("USER_ID") = DR("USER_ID")
            Session("User_Login") = Profile.Username
            Session("USER_Full_Name") = Trim(Profile.FirstName & " " & Profile.LastName)
            Session("USER_Name") = DR("USER_Name").ToString
            Session("USER_LEVEL") = DR("LEVEL_ID")
            Response.Redirect("Default.aspx")

        ElseIf Profile.Errmsg = "GetProfile : LDAP's user is disabled " Then
            MS_PTT_User = GetPTTUserDB(Profile.Username)
            StorePTTProfile(Profile, MS_PTT_User)
            lblError.Text = Profile.Errmsg
            Exit Sub
        Else

            MS_User = GetEIRUserDB(Profile.Username)
            If MS_User.Table.Rows.Count = 0 Then
                lblError.Text = "Invalid username."
                Exit Sub
            End If

            If IsDBNull(MS_User.Table.Rows(0).Item("Active_Status")) OrElse Not MS_User.Table.Rows(0).Item("Active_Status") Then
                lblError.Text = "This account is disabled."
                Exit Sub
            End If

            If MS_User.Table.Rows(0).Item("USER_Password") <> txtPassword.Text Then
                lblError.Text = "Password is incorrect."
                Exit Sub
            End If

            Dim DR As DataRow = MS_User.Table.Rows(0)
            DR("Last_Login") = Now
            Dim _cmd As New SqlCommandBuilder(MS_User.Adapter)
            MS_User.Adapter.Update(MS_User.Table)
            MS_User.Table.AcceptChanges()

            '-------- Redirect Goto EIR -------------
            Session("USER_ID") = DR("USER_ID")
            Session("User_Login") = Profile.Username
            Session("USER_Full_Name") = Trim(DR("USER_Prefix").ToString & DR("USER_Name").ToString & " " & DR("USER_Surname").ToString)
            Session("USER_Name") = DR("USER_Name").ToString
            Session("USER_LEVEL") = DR("LEVEL_ID")
            Response.Redirect("Default.aspx")
        End If

    End Sub

    Private Function GetLDAPProfile() As PTT_AD.DomainUserResult
        Dim AD As New PTT_AD
        Dim Profile As PTT_AD.DomainUserResult = AD.AuthenticateUser(BL.PTT_LDAP, txtUserName.Text, txtPassword.Text)
        Return Profile
    End Function

    'Public Function LoginFromDomain() As Boolean
    '    '----------------- Check User Login From Domain ---------------
    '    Dim AD As New PTT_AD
    '    Dim Profile As PTT_AD.DomainUserResult = AD.AuthenticateUser(BL.PTT_Domain, txtUserName.Text, txtPassword.Text, BL.PTT_LDAP)
    '    If Not Profile.IsEnabled Then
    '        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to login \n\n" & Profile.Errmsg.Replace(vbLf, "\n").Replace(vbCr, "") & "'); $('#txtUserName').focus();", True)
    '        If Profile.Errmsg <> "" Then
    '            lblError.Text = Profile.Errmsg
    '        Else
    '            lblError.Text = "Invalid Password !!"
    '        End If
    '        Return False
    '    End If

    '    '------------------- Keep Log ------------------------------------
    '    'On Error Resume Next
    '    Dim Sql As String = "SELECT * FROM MS_PTT_User WHERE PTT_User='" & Profile.Username.Replace("'", "''") & "'"
    '    Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)

    '    Dim DR As DataRow
    '    If DT.Rows.Count = 0 Then
    '        DR = DT.NewRow
    '        DR("PTT_User") = Profile.Username
    '        DT.Rows.Add(DR)
    '    Else
    '        DR = DT.Rows(0)
    '    End If

    '    DR("PTT_Password") = Profile.Password
    '    DR("PTT_Title") = Profile.Title
    '    DR("PTT_FirstName") = Profile.FirstName
    '    DR("PTT_Surname") = Profile.LastName
    '    DR("PTT_DisplayName") = Profile.DisplayName
    '    DR("PTT_EMail") = Profile.Mail
    '    DR("PTT_Department") = Profile.Department
    '    DR("PTT_Company") = Profile.Company
    '    DR("PTT_Manager") = Profile.Manager
    '    DR("PTT_Mobile") = Profile.Mobile
    '    DR("PTT_User_Profile_path") = Profile.User_Profile_path

    '    DR("Last_Login") = Now
    '    DR("Update_Time") = Now

    '    Dim _cmd As New SqlCommandBuilder(DA)
    '    DA.Update(DT)
    '    DT.AcceptChanges()

    '    '------------------- Logged on for PTT Authenticated--------------
    '    Session("USER_ID") = 99999
    '    Session("User_Login") = Profile.Username
    '    Session("USER_Full_Name") = Profile.Title & Profile.FirstName & " " & Profile.LastName
    '    Session("USER_Name") = Profile.DisplayName
    '    Session("USER_LEVEL") = -1 '------------ PTT Domain -----------

    '    If Err.Number <> 0 Then
    '        lblError.Text = Err.Description
    '        Return False
    '    End If

    '    Response.Redirect("Dashboard_Current_Status.aspx", True)
    '    Return True
    'End Function

    Private Structure DBStructure
        Public Table As DataTable
        Public Adapter As SqlDataAdapter
    End Structure

    Private Function GetPTTUserDB(ByVal PTT_User As String) As DBStructure
        Dim SQL As String = "SELECT * FROM MS_PTT_User WHERE PTT_User='" & PTT_User.Replace("'", "''") & "'"
        Dim DB As New DBStructure
        DB.Adapter = New SqlDataAdapter(SQL, BL.ConnStr)
        DB.Table = New DataTable
        DB.Adapter.Fill(DB.Table)
        Return DB
    End Function

    Private Function GetEIRUserDB(ByVal USER_Login As String) As DBStructure
        Dim SQL As String = "SELECT * FROM MS_User WHERE USER_Login='" & USER_Login.Replace("'", "''") & "'"
        Dim DB As New DBStructure
        DB.Adapter = New SqlDataAdapter(SQL, BL.ConnStr)
        DB.Table = New DataTable
        DB.Adapter.Fill(DB.Table)
        Return DB
    End Function

    Private Function GetNewEIRUserID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(USER_ID),0)+1 FROM MS_USER "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Sub StorePTTProfile(ByVal Profile As PTT_AD.DomainUserResult, ByRef MS_PTT_User As DBStructure)
        Dim DR As DataRow
        If MS_PTT_User.Table.Rows.Count > 0 Then
            DR = MS_PTT_User.Table.Rows(0)
        Else
            DR = MS_PTT_User.Table.NewRow
            DR("PTT_User") = Profile.Username
            MS_PTT_User.Table.Rows.Add(DR)
        End If
        DR("PTT_Password") = Profile.Password
        DR("PTT_Title") = Profile.Initials
        DR("PTT_FirstName") = Profile.FirstName
        DR("PTT_Surname") = Profile.LastName
        DR("PTT_DisplayName") = Profile.DisplayName
        DR("PTT_EMail") = Profile.Mail
        DR("PTT_Department") = Profile.Department
        DR("PTT_Company") = Profile.Company
        'DR("PTT_Manager") = Profile.Manager
        DR("PTT_Mobile") = Profile.Mobile
        'DR("PTT_User_Profile_path") = Profile.User_Profile_path
        DR("Last_Login") = Now
        DR("Update_Time") = Now
        Dim _cmd As New SqlCommandBuilder(MS_PTT_User.Adapter)
        MS_PTT_User.Adapter.Update(MS_PTT_User.Table)
        MS_PTT_User.Table.AcceptChanges()
    End Sub
End Class
