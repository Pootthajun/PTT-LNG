﻿Imports System.Data.SqlClient
Public Class UC_Pipe_Tag_Info
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE
    Dim C As New Converter

#Region "Public Property"

    Public Enum PropertyItem
        All = 0
        PLANT = 1
        AREA = 2
        LOOP_NO = 3
        PROCESS = 4
        LINE_NO = 5
        SERVICE = 6
        PIPE_SIZE = 7
        SCHEDULE = 8
        INITIAL_YEAR = 9
        LOCATION_FROM = 10
        LOCATION_TO = 11
        NORMINAL_THICKNESS = 12
        CALCULATED_THICKNESS = 13
        INSULATION_ID = 14
        INSULATION_THICKNESS = 15
        DESIGN_PRESSURE = 16
        OPERATING_PRESSURE = 17
        DESIGN_TEMPERATURE = 18
        OPERATING_TEMPERATURE = 19
        MATERIAL = 20
        PRESSURE_CODE = 21
        CORROSION_ALLOWANCE = 22
        SERVICE_MEDIA = 23
        PIPE_CLASS = 24
        P_ID_No = 25
    End Enum

    Public Enum AssignedMode
        Auto = 1
        Manual = 2
    End Enum

    Public Event PropertyChangedByUser(ByRef Sender As UC_Pipe_Tag_Info, ByVal Prop As PropertyItem)

    Public Property UNIQUE_ID As String
        Get
            Return Me.Attributes("UNIQUE_ID")
        End Get
        Set(value As String)
            Me.Attributes("UNIQUE_ID") = value
        End Set
    End Property

    Public Property TAG_ID As Integer
        Get
            Try
                Return Me.Attributes("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                Me.Attributes("TAG_ID") = value
            Else
                Me.Attributes("TAG_ID") = 0
            End If
        End Set
    End Property

    Public Property TAG_CODE As String
        Get
            Return lblCode.Text
        End Get
        Set(value As String)
            lblCode.Text = value
        End Set
    End Property

    Public Property PLANT_ID As Integer
        Get
            Try
                Return ddl_PLANT.Items(ddl_PLANT.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                BL.BindDDlPlant(ddl_PLANT, value)
            Else
                BL.BindDDlPlant(ddl_PLANT)
            End If
            BL.BindDDlArea(PLANT_ID, ddl_AREA, AREA_ID)
        End Set
    End Property

    Public Property AREA_ID As Integer
        Get
            Try
                Return ddl_AREA.Items(ddl_AREA.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                If PLANT_ID = 0 Then
                    BL.BindDDlArea(ddl_AREA, value)
                Else
                    BL.BindDDlArea(PLANT_ID, ddl_AREA, value)
                End If
            Else
                If PLANT_ID = 0 Then
                    BL.BindDDlArea(ddl_AREA)
                Else
                    BL.BindDDlArea(PLANT_ID, ddl_AREA)
                End If
            End If
        End Set
    End Property

    Public ReadOnly Property AREA_CODE As String
        Get
            If AREA_ID = 0 Then Return ""
            Try
                Return ddl_AREA.Items(ddl_AREA.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property LOOP_NO As String
        Get
            Return txt_LoopNo.Text
        End Get
        Set(value As String)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                txt_LoopNo.Text = value
            Else
                txt_LoopNo.Text = ""
            End If
        End Set
    End Property

    Public Property PROCESS_ID As Integer
        Get
            Try
                Return ddl_PROCESS.Items(ddl_PROCESS.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                BL.BindDDlProcess(ddl_PROCESS, value)
            Else
                BL.BindDDlProcess(ddl_PROCESS)
            End If
        End Set
    End Property

    Public ReadOnly Property PROCESS_CODE As String
        Get
            If PROCESS_ID = 0 Then Return ""
            Try
                Return ddl_PROCESS.Items(ddl_PROCESS.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property LINE_NO As Object
        Get
            Try
                Return CInt(txt_LineNo.Text)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                txt_LineNo.Text = value
            Else
                txt_LineNo.Text = ""
            End If
        End Set
    End Property

    Public Property SERVICE_ID As Integer
        Get
            Try
                Return ddl_SERVICE.Items(ddl_SERVICE.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                PIPE.BindDDl_Service(ddl_SERVICE, value)
            Else
                PIPE.BindDDl_Service(ddl_SERVICE)
            End If
        End Set
    End Property

    Public ReadOnly Property SERVICE_CODE As String
        Get
            If SERVICE_ID = 0 Then Return ""
            Try
                Return ddl_SERVICE.Items(ddl_SERVICE.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property PIPE_SIZE As Object
        Get
            If IsNumeric(txt_Size.Text) Then
                Return CDbl(txt_Size.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Size.Text = CDbl(value)
            Else
                txt_Size.Text = ""
            End If
        End Set
    End Property

    Public Property SCHEDULE As String
        Get
            Return txt_Schedule.Text
        End Get
        Set(value As String)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                txt_Schedule.Text = value
            Else
                txt_Schedule.Text = ""
            End If
        End Set
    End Property

    Public Property INITIAL_YEAR As Object
        Get
            If IsNumeric(txt_Initial_Year.Text.Replace(",", "")) Then
                Return CInt(txt_Initial_Year.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Initial_Year.Text = CInt(value)
            Else
                txt_Initial_Year.Text = ""
            End If
        End Set
    End Property

    Public Property LOCATION_FROM As String
        Get
            Return txt_Location_From.Text
        End Get
        Set(value As String)
            txt_Location_From.Text = value
        End Set
    End Property

    Public Property LOCATION_TO As String
        Get
            Return txt_Location_To.Text
        End Get
        Set(value As String)
            txt_Location_To.Text = value
        End Set
    End Property

    Public Property NORMINAL_THICKNESS As Object
        Get
            If IsNumeric(txt_Norminal_Thickness.Text.Replace(",", "")) Then
                Return Val(txt_Norminal_Thickness.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Norminal_Thickness.Text = FormatNumericText(value)
            Else
                txt_Norminal_Thickness.Text = ""
            End If
        End Set
    End Property

    Public Property CALCULATED_THICKNESS As Object
        Get
            If IsNumeric(txt_Calculated_Thickness.Text.Replace(",", "")) Then
                Return Val(txt_Calculated_Thickness.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Calculated_Thickness.Text = FormatNumericText(value)
            Else
                txt_Calculated_Thickness.Text = ""
            End If
        End Set
    End Property

    Public Property INSULATION_ID As Integer
        Get
            Try
                Return ddl_INSULATION.Items(ddl_INSULATION.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                PIPE.BindDDl_Insulation_Code(ddl_INSULATION, value)
            Else
                PIPE.BindDDl_Insulation_Code(ddl_INSULATION)
            End If
        End Set
    End Property

    Public Property INSULATION_THICKNESS As Object
        Get
            If IsNumeric(txt_Insulation_Thickness.Text.Replace(",", "")) Then
                Return Val(txt_Insulation_Thickness.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Insulation_Thickness.Text = FormatNumericText(value)
            Else
                txt_Insulation_Thickness.Text = ""
            End If
        End Set
    End Property

    Public ReadOnly Property INSULATION_CODE As String
        Get
            If INSULATION_ID = 0 Then Return ""
            Try
                Return ddl_INSULATION.Items(ddl_INSULATION.SelectedIndex).Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Public Property PRESSURE_DESIGN As Object
        Get
            If IsNumeric(txt_Pressure_Design.Text.Replace(",", "")) Then
                Return Val(txt_Pressure_Design.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Pressure_Design.Text = FormatNumericText(value)
            Else
                txt_Pressure_Design.Text = ""
            End If
        End Set
    End Property

    Public Property PRESSURE_OPERATING As Object
        Get
            If IsNumeric(txt_Pressure_Operating.Text.Replace(",", "")) Then
                Return Val(txt_Pressure_Operating.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Pressure_Operating.Text = FormatNumericText(value)
            Else
                txt_Pressure_Operating.Text = ""
            End If
        End Set
    End Property

    Public Property TEMPERATURE_DESIGN As Object
        Get
            If IsNumeric(txt_Temperature_Design.Text.Replace(",", "")) Then
                Return Val(txt_Temperature_Design.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Temperature_Design.Text = FormatNumericText(value)
            Else
                txt_Temperature_Design.Text = ""
            End If
        End Set
    End Property

    Public Property TEMPERATURE_OPERATING As Object
        Get
            If IsNumeric(txt_Temperature_Operating.Text.Replace(",", "")) Then
                Return Val(txt_Temperature_Operating.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Temperature_Operating.Text = FormatNumericText(value)
            Else
                txt_Temperature_Operating.Text = ""
            End If
        End Set
    End Property

    Public Property MATERIAL_ID As Integer
        Get
            Try
                Return ddl_MAT.Items(ddl_MAT.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                PIPE.BindDDl_Material(ddl_MAT, value)
            Else
                PIPE.BindDDl_Material(ddl_MAT)
            End If
        End Set
    End Property

    Public ReadOnly Property MATERIAL_CODE As String
        Get
            If MATERIAL_ID = 0 Then Return ""
            Try
                Return ddl_MAT.Items(ddl_MAT.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property PRESSURE_ID As Integer
        Get
            Try
                Return ddl_PRESSURE.Items(ddl_PRESSURE.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                PIPE.BindDDl_Pressure_Code(ddl_PRESSURE, value)
            Else
                PIPE.BindDDl_Pressure_Code(ddl_PRESSURE)
            End If
        End Set
    End Property

    Public ReadOnly Property PRESSURE_CODE As String
        Get
            If PRESSURE_ID = 0 Then Return ""
            Try
                Return ddl_PRESSURE.Items(ddl_PRESSURE.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property CORROSION_ALLOWANCE As Object
        Get
            Try
                Return CDbl(txt_Corrosion_Allowance.Text)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                txt_Corrosion_Allowance.Text = CDbl(value)
            Else
                txt_Corrosion_Allowance.Text = ""
            End If
        End Set
    End Property

    Public Property MEDIA_ID As Integer
        Get
            Try
                Return ddl_MED.Items(ddl_MED.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                PIPE.BindDDl_Media(ddl_MED, value)
            Else
                PIPE.BindDDl_Media(ddl_MED)
            End If
        End Set
    End Property

    Public ReadOnly Property MEDIA_CODE As String
        Get
            If MEDIA_ID = 0 Then Return ""
            Try
                Return ddl_MED.Items(ddl_MED.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public WriteOnly Property RaisePropertyChangedByUser As Boolean
        Set(value As Boolean)
            RaiseExternalEvent = value
            If value Then
                ddl_PLANT.AutoPostBack = True
                ddl_AREA.AutoPostBack = True
                txt_LoopNo.AutoPostBack = True
                ddl_PROCESS.AutoPostBack = True
                txt_LineNo.AutoPostBack = True
                ddl_SERVICE.AutoPostBack = True
                txt_Size.AutoPostBack = True
                txt_Schedule.AutoPostBack = True
                txt_Initial_Year.AutoPostBack = True
                txt_Location_From.AutoPostBack = True
                txt_Location_To.AutoPostBack = True
                txt_Norminal_Thickness.AutoPostBack = True
                txt_Calculated_Thickness.AutoPostBack = True
                ddl_INSULATION.AutoPostBack = True
                txt_Insulation_Thickness.AutoPostBack = True
                txt_Pressure_Design.AutoPostBack = True
                txt_Pressure_Operating.AutoPostBack = True
                txt_Temperature_Design.AutoPostBack = True
                txt_Temperature_Operating.AutoPostBack = True
                ddl_MAT.AutoPostBack = True
                ddl_PRESSURE.AutoPostBack = True
                txt_Corrosion_Allowance.AutoPostBack = True
                ddl_MED.AutoPostBack = True
                rdo_Class_Auto.AutoPostBack = True
                rdo_Class_Manual.AutoPostBack = True
                txt_Class.AutoPostBack = True
                txt_P_ID_No.AutoPostBack = True

                AddHandler ddl_PLANT.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler ddl_AREA.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler txt_LoopNo.TextChanged, AddressOf Property_Changed
                AddHandler ddl_PROCESS.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler txt_LineNo.TextChanged, AddressOf Property_Changed
                AddHandler ddl_SERVICE.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler txt_Size.TextChanged, AddressOf Property_Changed
                AddHandler txt_Schedule.TextChanged, AddressOf Property_Changed
                AddHandler txt_Initial_Year.TextChanged, AddressOf Property_Changed
                AddHandler txt_Location_From.TextChanged, AddressOf Property_Changed
                AddHandler txt_Location_To.TextChanged, AddressOf Property_Changed
                AddHandler txt_Norminal_Thickness.TextChanged, AddressOf Property_Changed
                AddHandler txt_Calculated_Thickness.TextChanged, AddressOf Property_Changed
                AddHandler ddl_INSULATION.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler txt_Insulation_Thickness.TextChanged, AddressOf Property_Changed
                AddHandler txt_Pressure_Design.TextChanged, AddressOf Property_Changed
                AddHandler txt_Pressure_Operating.TextChanged, AddressOf Property_Changed
                AddHandler txt_Temperature_Design.TextChanged, AddressOf Property_Changed
                AddHandler txt_Temperature_Operating.TextChanged, AddressOf Property_Changed
                AddHandler ddl_MAT.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler ddl_PRESSURE.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler txt_Corrosion_Allowance.TextChanged, AddressOf Property_Changed
                AddHandler ddl_MED.SelectedIndexChanged, AddressOf Property_Changed
                AddHandler rdo_Class_Auto.CheckedChanged, AddressOf Property_Changed
                AddHandler rdo_Class_Manual.CheckedChanged, AddressOf Property_Changed
                AddHandler txt_Class.TextChanged, AddressOf Property_Changed
                AddHandler txt_P_ID_No.TextChanged, AddressOf Property_Changed

            Else
                ddl_PLANT.AutoPostBack = False
                ddl_AREA.AutoPostBack = False
                txt_LoopNo.AutoPostBack = False
                ddl_PROCESS.AutoPostBack = False
                txt_LineNo.AutoPostBack = False
                ddl_SERVICE.AutoPostBack = False
                txt_Size.AutoPostBack = False
                txt_Schedule.AutoPostBack = False
                txt_Initial_Year.AutoPostBack = False
                txt_Location_From.AutoPostBack = False
                txt_Location_To.AutoPostBack = False
                txt_Norminal_Thickness.AutoPostBack = False
                txt_Calculated_Thickness.AutoPostBack = False
                ddl_INSULATION.AutoPostBack = False
                txt_Insulation_Thickness.AutoPostBack = False
                txt_Pressure_Design.AutoPostBack = False
                txt_Pressure_Operating.AutoPostBack = False
                txt_Temperature_Design.AutoPostBack = False
                txt_Temperature_Operating.AutoPostBack = False
                ddl_MAT.AutoPostBack = False
                ddl_PRESSURE.AutoPostBack = False
                txt_Corrosion_Allowance.AutoPostBack = False
                ddl_MED.AutoPostBack = False
                rdo_Class_Auto.AutoPostBack = False
                rdo_Class_Manual.AutoPostBack = False
                txt_Class.AutoPostBack = False
                txt_P_ID_No.AutoPostBack = False

                RemoveHandler ddl_PLANT.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler ddl_AREA.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler txt_LoopNo.TextChanged, AddressOf Property_Changed
                RemoveHandler ddl_PROCESS.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler txt_LineNo.TextChanged, AddressOf Property_Changed
                RemoveHandler ddl_SERVICE.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler txt_Size.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Schedule.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Initial_Year.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Location_From.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Location_To.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Norminal_Thickness.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Calculated_Thickness.TextChanged, AddressOf Property_Changed
                RemoveHandler ddl_INSULATION.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler txt_Insulation_Thickness.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Pressure_Design.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Pressure_Operating.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Temperature_Design.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_Temperature_Operating.TextChanged, AddressOf Property_Changed
                RemoveHandler ddl_MAT.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler ddl_PRESSURE.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler txt_Corrosion_Allowance.TextChanged, AddressOf Property_Changed
                RemoveHandler ddl_MED.SelectedIndexChanged, AddressOf Property_Changed
                RemoveHandler rdo_Class_Auto.CheckedChanged, AddressOf Property_Changed
                RemoveHandler rdo_Class_Manual.CheckedChanged, AddressOf Property_Changed
                RemoveHandler txt_Class.TextChanged, AddressOf Property_Changed
                RemoveHandler txt_P_ID_No.TextChanged, AddressOf Property_Changed
            End If
        End Set
    End Property

    Public Property ClassAssignMode As AssignedMode
        Get
            If rdo_Class_Manual.Checked Then
                Return AssignedMode.Manual
            Else
                Return AssignedMode.Auto
            End If
        End Get
        Set(value As AssignedMode)
            Select Case value
                Case AssignedMode.Manual
                    rdo_Class_Manual.Checked = True
                Case Else
                    rdo_Class_Auto.Checked = True
            End Select
            lbl_Class.Visible = rdo_Class_Auto.Checked
            txt_Class.Visible = rdo_Class_Manual.Checked
        End Set
    End Property

    Public Property MANUAL_CLASS As String
        Get
            Return txt_Class.Text
        End Get
        Set(value As String)
            txt_Class.Text = value
        End Set
    End Property

    Public ReadOnly Property PIPE_CLASS As String
        Get
            Select Case ClassAssignMode
                Case AssignedMode.Auto
                    Return lbl_Class.Text
                Case Else
                    Return MANUAL_CLASS
            End Select
        End Get
    End Property

    Public Property P_ID_No As String
        Get
            Return txt_P_ID_No.Text
        End Get
        Set(value As String)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                txt_P_ID_No.Text = value
            Else
                txt_P_ID_No.Text = ""
            End If
        End Set
    End Property


    Public Property DisplayMainPointProperty() As Boolean
        Get
            Return trp1.Visible
        End Get
        Set(value As Boolean)
            trp1.Visible = value
            trp2.Visible = value
            trp3.Visible = value
            trp4.Visible = value
            trp5.Visible = value
            trp6.Visible = value
            trp7.Visible = value
            trp8.Visible = value
            trp9.Visible = value
            trp10.Visible = value
            trp11.Visible = value
        End Set
    End Property

    Private Property RaiseExternalEvent As Boolean
        Get
            Try
                Return Me.ViewState("RaiseExternalEvent")
            Catch ex As Exception
                Return True
            End Try
        End Get
        Set(value As Boolean)
            Me.ViewState("RaiseExternalEvent") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ImplementJavascriptControl()
        Else
            If RaiseExternalEvent Then Me.RaisePropertyChangedByUser = True
        End If

    End Sub

    Private Sub ImplementJavascriptControl()
        ImplementJavaIntegerText(txt_LineNo, False,, "Left")
        ImplementJavaNumericText(txt_Size, "Left")
        ImplementJavaIntegerText(txt_Schedule, False,, "Left")
        ImplementJavaIntegerText(txt_Corrosion_Allowance, False,, "Left")
        ImplementJavaIntegerText(txt_Insulation_Thickness, False,, "Left")
        ImplementJavaNumericText(txt_Norminal_Thickness, "Left")
        ImplementJavaOnlyNumberText(txt_Initial_Year, "Left")
        ImplementJavaNumericText(txt_Calculated_Thickness, "Left")
        ImplementJavaNumericText(txt_Corrosion_Allowance, "Left")
        ImplementJavaNumericText(txt_Pressure_Design, "Left")
        ImplementJavaNumericText(txt_Pressure_Operating, "Left")
        ImplementJavaNumericText(txt_Temperature_Design, "Left")
        ImplementJavaNumericText(txt_Temperature_Operating, "Left")
    End Sub

    Public Sub ClearData()
        ClearPanelEdit()
    End Sub

    Private Sub UpdateTagCode()
        Dim _size As Double = IIf(IsNothing(PIPE_SIZE), 0, PIPE_SIZE)
        Dim _ca As Double = IIf(IsNothing(CORROSION_ALLOWANCE), 0, CORROSION_ALLOWANCE)
        Dim _in_Thick As Integer = IIf(IsNothing(INSULATION_THICKNESS), 0, INSULATION_THICKNESS)
        Dim _line As String = IIf(IsNothing(LINE_NO), "", LINE_NO)
        Dim MClass As String = ""
        If ClassAssignMode = AssignedMode.Manual Then MClass = MANUAL_CLASS

        TAG_CODE = PIPE.Get_Code(AREA_CODE, PROCESS_CODE, _line, _size, MClass, MATERIAL_CODE, PRESSURE_CODE, _ca, MEDIA_CODE, INSULATION_CODE, _in_Thick)
    End Sub

    Private Sub UpdateClass()
        Dim _ca As Double = IIf(IsNothing(CORROSION_ALLOWANCE), 0, CORROSION_ALLOWANCE)
        lbl_Class.Text = PIPE.Get_Class(MATERIAL_CODE, PRESSURE_CODE, _ca, MEDIA_CODE)
    End Sub

    Private Sub ClearPanelEdit()

        TAG_ID = 0

        PLANT_ID = 0
        AREA_ID = 0
        LOOP_NO = ""
        PROCESS_ID = 0
        LINE_NO = Nothing
        SERVICE_ID = 0
        PIPE_SIZE = Nothing
        SCHEDULE = ""
        INITIAL_YEAR = Nothing
        LOCATION_FROM = ""
        LOCATION_TO = ""
        NORMINAL_THICKNESS = Nothing
        CALCULATED_THICKNESS = Nothing
        INSULATION_ID = 0
        INSULATION_THICKNESS = Nothing
        PRESSURE_DESIGN = Nothing
        PRESSURE_OPERATING = Nothing
        TEMPERATURE_DESIGN = Nothing
        TEMPERATURE_OPERATING = Nothing
        MATERIAL_ID = 0
        PRESSURE_ID = 0
        CORROSION_ALLOWANCE = Nothing
        MEDIA_ID = 0

        MANUAL_CLASS = ""
        ClassAssignMode = AssignedMode.Auto

        P_ID_No = Nothing

        UpdateClass()
        UpdateTagCode()

        SetPropertyEditable(PropertyItem.All, True)
    End Sub

    Public Sub SetPropertyEditable(ByVal PropertyItem As PropertyItem, ByVal Editable As Boolean)
        Select Case PropertyItem
            Case PropertyItem.All
                For i As Integer = 1 To 25
                    SetPropertyEditable(i, Editable)
                Next
            Case PropertyItem.PLANT
                ddl_PLANT.Enabled = Editable
            Case PropertyItem.AREA
                ddl_AREA.Enabled = Editable
            Case PropertyItem.LOOP_NO
                txt_LoopNo.ReadOnly = Not Editable
            Case PropertyItem.PROCESS
                ddl_PROCESS.Enabled = Editable
            Case PropertyItem.LINE_NO
                txt_LineNo.ReadOnly = Not Editable
            Case PropertyItem.SERVICE
                ddl_SERVICE.Enabled = Editable
            Case PropertyItem.PIPE_SIZE
                txt_Size.ReadOnly = Not Editable
            Case PropertyItem.SCHEDULE
                txt_Schedule.ReadOnly = Not Editable
            Case PropertyItem.INITIAL_YEAR
                txt_Initial_Year.ReadOnly = Not Editable
            Case PropertyItem.LOCATION_FROM
                txt_Location_From.ReadOnly = Not Editable
            Case PropertyItem.LOCATION_TO
                txt_Location_To.ReadOnly = Not Editable
            Case PropertyItem.NORMINAL_THICKNESS
                txt_Norminal_Thickness.ReadOnly = Not Editable
            Case CALCULATED_THICKNESS
                txt_Calculated_Thickness.ReadOnly = Not Editable
            Case PropertyItem.INSULATION_ID
                ddl_INSULATION.Enabled = Editable
            Case PropertyItem.INSULATION_THICKNESS
                txt_Insulation_Thickness.ReadOnly = Not Editable
            Case PropertyItem.DESIGN_PRESSURE
                txt_Pressure_Design.ReadOnly = Not Editable
            Case PropertyItem.OPERATING_PRESSURE
                txt_Pressure_Operating.ReadOnly = Not Editable
            Case PropertyItem.DESIGN_TEMPERATURE
                txt_Temperature_Design.ReadOnly = Not Editable
            Case PropertyItem.OPERATING_TEMPERATURE
                txt_Temperature_Operating.ReadOnly = Not Editable
            Case PropertyItem.MATERIAL
                ddl_MAT.Enabled = Editable
            Case PropertyItem.PRESSURE_CODE
                ddl_PRESSURE.Enabled = Editable
            Case PropertyItem.CORROSION_ALLOWANCE
                txt_Corrosion_Allowance.ReadOnly = Not Editable
            Case PropertyItem.SERVICE_MEDIA
                ddl_MED.Enabled = Editable
            Case PropertyItem.PIPE_CLASS
                rdo_Class_Auto.Enabled = Editable
                rdo_Class_Manual.Enabled = Editable
                txt_Class.ReadOnly = Not Editable
            Case PropertyItem.P_ID_No
                txt_P_ID_No.Enabled = Editable
        End Select

    End Sub

    Public Function GetPropertyEditable(ByVal PropertyItem As PropertyItem) As Boolean

        Select Case PropertyItem
            Case PropertyItem.PLANT
                Return ddl_PLANT.Enabled
            Case PropertyItem.AREA
                Return ddl_AREA.Enabled
            Case PropertyItem.LOOP_NO
                Return Not txt_LoopNo.ReadOnly
            Case PropertyItem.PROCESS
                Return ddl_PROCESS.Enabled
            Case PropertyItem.LINE_NO
                Return Not txt_LineNo.ReadOnly
            Case PropertyItem.SERVICE
                Return ddl_SERVICE.Enabled
            Case PropertyItem.PIPE_SIZE
                Return Not txt_Size.ReadOnly
            Case PropertyItem.SCHEDULE
                Return Not txt_Schedule.ReadOnly
            Case PropertyItem.INITIAL_YEAR
                Return Not txt_Initial_Year.ReadOnly
            Case PropertyItem.LOCATION_FROM
                Return Not txt_Location_From.ReadOnly
            Case PropertyItem.LOCATION_TO
                Return Not txt_Location_To.ReadOnly
            Case PropertyItem.NORMINAL_THICKNESS
                Return Not txt_Norminal_Thickness.ReadOnly
            Case PropertyItem.CALCULATED_THICKNESS
                Return Not txt_Calculated_Thickness.ReadOnly
            Case PropertyItem.INSULATION_ID
                Return ddl_INSULATION.Enabled
            Case PropertyItem.INSULATION_THICKNESS
                Return Not txt_Insulation_Thickness.ReadOnly
            Case PropertyItem.DESIGN_PRESSURE
                Return Not txt_Pressure_Design.ReadOnly
            Case PropertyItem.OPERATING_PRESSURE
                Return Not txt_Pressure_Operating.ReadOnly
            Case PropertyItem.DESIGN_TEMPERATURE
                Return Not txt_Temperature_Design.ReadOnly
            Case PropertyItem.OPERATING_TEMPERATURE
                Return Not txt_Temperature_Operating.ReadOnly
            Case PropertyItem.MATERIAL
                Return ddl_MAT.Enabled
            Case PropertyItem.PRESSURE_CODE
                Return ddl_PRESSURE.Enabled
            Case PropertyItem.CORROSION_ALLOWANCE
                Return Not txt_Corrosion_Allowance.ReadOnly
            Case PropertyItem.SERVICE_MEDIA
                Return ddl_MED.Enabled
            Case PropertyItem.PIPE_CLASS
                Return rdo_Class_Auto.Enabled And rdo_Class_Manual.Enabled And Not txt_Class.ReadOnly
            Case PropertyItem.P_ID_No
                Return Not txt_P_ID_No.ReadOnly
            Case Else ' PropertyItem.All
                For i As Integer = 1 To 25
                    If Not GetPropertyEditable(i) Then Return False
                Next
                Return True
        End Select
    End Function

    Public Sub BindData(ByVal TAG_ID As Integer)

        ClearPanelEdit()

        Dim SQL As String = " SELECT TAG_ID,TAG_Code,PLANT_ID,AREA_ID,PROC_ID,Line_No,Manual_Class" & vbLf
        SQL &= ", material_id, PRS_ID, MD_ID, IN_ID, IN_Thickness" & vbLf
        SQL &= ",SERVICE_ID,Size,Schedule,Initial_Year,Location_From,Location_To,Loop_No" & vbLf
        SQL &= ",Norminal_Thickness,Calculated_Thickness,Pressure_Design,Pressure_Operating,Temperature_Design,Temperature_Operating,Corrosion_Allowance" & vbLf
        SQL &= " FROM VW_PIPE_TAG" & vbLf
        SQL &= " WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        Me.TAG_ID = DT.Rows(0).Item("TAG_ID")
        TAG_CODE = DT.Rows(0).Item("TAG_CODE")
        If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
        LOOP_NO = DT.Rows(0).Item("Loop_No").ToString
        If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROCESS_ID = DT.Rows(0).Item("PROC_ID")
        LINE_NO = DT.Rows(0).Item("Line_No").ToString
        If Not IsDBNull(DT.Rows(0).Item("SERVICE_ID")) Then SERVICE_ID = DT.Rows(0).Item("SERVICE_ID")
        If Not IsDBNull(DT.Rows(0).Item("Size")) Then PIPE_SIZE = CDbl(DT.Rows(0).Item("Size"))
        SCHEDULE = DT.Rows(0).Item("Schedule").ToString
        If Not IsDBNull(DT.Rows(0).Item("Initial_Year")) Then INITIAL_YEAR = CDbl(DT.Rows(0).Item("Initial_Year"))
        LOCATION_FROM = DT.Rows(0).Item("Location_From").ToString
        LOCATION_TO = DT.Rows(0).Item("Location_To").ToString
        If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then NORMINAL_THICKNESS = DT.Rows(0).Item("Norminal_Thickness")
        If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then CALCULATED_THICKNESS = DT.Rows(0).Item("Calculated_Thickness")
        If Not IsDBNull(DT.Rows(0).Item("IN_ID")) Then INSULATION_ID = DT.Rows(0).Item("IN_ID")
        If Not IsDBNull(DT.Rows(0).Item("IN_Thickness")) Then INSULATION_THICKNESS = CInt(DT.Rows(0).Item("IN_Thickness"))
        If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then PRESSURE_DESIGN = CInt(DT.Rows(0).Item("Pressure_Design"))
        If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then PRESSURE_OPERATING = CInt(DT.Rows(0).Item("Pressure_Operating"))
        If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then TEMPERATURE_DESIGN = CInt(DT.Rows(0).Item("Temperature_Design"))
        If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then TEMPERATURE_OPERATING = CInt(DT.Rows(0).Item("Temperature_Operating"))
        If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then CORROSION_ALLOWANCE = CDbl(DT.Rows(0).Item("Corrosion_Allowance"))
        If Not IsDBNull(DT.Rows(0).Item("PRS_ID")) Then PRESSURE_ID = DT.Rows(0).Item("PRS_ID")
        If Not IsDBNull(DT.Rows(0).Item("MD_ID")) Then MEDIA_ID = DT.Rows(0).Item("MD_ID")
        If Not IsDBNull(DT.Rows(0).Item("material_id")) Then MATERIAL_ID = DT.Rows(0).Item("material_id")

        If IsDBNull(DT.Rows(0).Item("Manual_Class")) OrElse DT.Rows(0).Item("Manual_Class") = "" Then
            rdo_Class_Auto.Checked = True
        Else
            txt_Class.Text = DT.Rows(0).Item("Manual_Class")
        End If

        UpdateClass()
        UpdateTagCode()
        Me.TAG_ID = TAG_ID
        '--------------- Load Drawing --------------

    End Sub




    Public Function ValidateIncompleteMessage() As String
        If PLANT_ID = 0 Then
            Return "Please Select Plant"
        End If
        If AREA_ID = 0 Then
            Return "Please Select Area"
        End If
        If PROCESS_ID = 0 Then
            Return "Please Select Process"
        End If
        If IsNothing(LINE_NO) Then
            Return "Please Insert Line No"
        End If
        If IsNothing(PIPE_SIZE) Then
            Return "Please Insert Pipe Size"
        End If
        If IsNothing(INITIAL_YEAR) Then
            Return "Please Insert Initial Year"
        End If
        If IsNothing(NORMINAL_THICKNESS) Then
            Return "Please Select Norminal Thickness"
        End If
        If IsNothing(CALCULATED_THICKNESS) Then
            Return "Please Select Calculated Thickness"
        End If
        If INSULATION_ID = 0 Then
            Return "Please Select Insulation Code"
        End If
        If IsNothing(INSULATION_THICKNESS) Then
            Return "Please Insert Insulation Thickness"
        End If
        If MATERIAL_ID = 0 Then
            Return "Please Select Material"
        End If
        If PRESSURE_ID = 0 Then
            Return "Please Select Pressure Code"
        End If
        If IsNothing(CORROSION_ALLOWANCE) Then
            Return "Please insert Corrosion Allowance"
        End If
        If MEDIA_ID = 0 Then
            Return "Please Select Service Media"
        End If
        If ClassAssignMode = AssignedMode.Manual And MANUAL_CLASS = "" Then
            Return "Please insert Pipe Class"
        End If
        Return ""
    End Function

    Public Function SaveData() As Integer '---------------- Save Only Tag--------------

        '--------------- Validate Required Field--------------
        Dim Msg As String = ValidateIncompleteMessage()
        If Msg <> "" Then
            Dim ER As New Exception(Msg)
            Throw (ER)
        End If

        '--------------- Check Duplicate--------------
        Dim SQL As String = "Select * FROM VW_PIPE_TAG WHERE TAG_Code='" & TAG_CODE & "' AND TAG_ID<>" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Dim ER As New Exception("This Tag Code is already existed")
            Throw (ER)
        End If

        SQL = "SELECT * FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TAG_ID = GetNewTagID()
            DR("TAG_ID") = TAG_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("PLANT_ID") = PLANT_ID
        DR("AREA_ID") = AREA_ID
        DR("PROC_ID") = PROCESS_ID
        DR("Line_No") = LINE_NO.ToString.PadLeft(5, "0")
        DR("SERVICE_ID") = SERVICE_ID
        DR("LOOP_NO") = LOOP_NO
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            Dim ER As New Exception(ex.Message)
            Throw (ER)
        End Try


        '------------- Save Point(Object Must specific POINT_ID First Before Saving) ---------------
        SQL = "SELECT * FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=0"
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("TAG_ID") = TAG_ID
            DR("POINT_ID") = 0
        Else
            DR = DT.Rows(0)
        End If
        DR("POINT_NAME") = "Main"
        DR("Component_Type") = 1
        DR("MD_ID") = MEDIA_ID
        If ClassAssignMode = AssignedMode.Auto Then DR("Manual_Class") = DBNull.Value Else DR("Manual_Class") = MANUAL_CLASS
        DR("Size") = PIPE_SIZE
        DR("Location_From") = LOCATION_FROM
        DR("Location_To") = LOCATION_TO
        DR("material_id") = MATERIAL_ID
        DR("Schedule") = SCHEDULE
        DR("PRS_ID") = PRESSURE_ID
        DR("IN_ID") = INSULATION_ID
        DR("IN_Thickness") = INSULATION_THICKNESS
        If INITIAL_YEAR > 1900 And INITIAL_YEAR <= Now.Year Then
            DR("Initial_Year") = INITIAL_YEAR
        Else
            DR("Initial_Year") = DBNull.Value
        End If
        DR("Norminal_Thickness") = NORMINAL_THICKNESS
        DR("Corrosion_Allowance") = CORROSION_ALLOWANCE
        DR("Calculated_Thickness") = CALCULATED_THICKNESS
        If Not IsNothing(PRESSURE_DESIGN) Then
            DR("Pressure_Design") = PRESSURE_DESIGN
        Else
            DR("Pressure_Design") = DBNull.Value
        End If
        If Not IsNothing(PRESSURE_OPERATING) Then
            DR("Pressure_Operating") = PRESSURE_OPERATING
        Else
            DR("Pressure_Operating") = DBNull.Value
        End If
        If Not IsNothing(TEMPERATURE_DESIGN) Then
            DR("Temperature_Design") = TEMPERATURE_DESIGN
        Else
            DR("Temperature_Design") = DBNull.Value
        End If
        If Not IsNothing(TEMPERATURE_OPERATING) Then
            DR("Temperature_Operating") = TEMPERATURE_OPERATING
        Else
            DR("Temperature_Operating") = DBNull.Value
        End If
        DR("P_ID_No") = P_ID_No

        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        cmd = New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            Dim ER As New Exception(ex.Message)
            Throw (ER)
        End Try

        Return TAG_ID
    End Function

    Private Function GetNewTagID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM MS_PIPE_TAG "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Sub Property_Changed(sender As Object, e As EventArgs)
        Select Case True
            Case Equals(sender, ddl_PLANT)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PLANT)
            Case Equals(sender, ddl_AREA)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.AREA)
            Case Equals(sender, txt_LoopNo)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.LOOP_NO)
            Case Equals(sender, ddl_PROCESS)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PROCESS)
            Case Equals(sender, txt_LineNo)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.LINE_NO)
            Case Equals(sender, ddl_SERVICE)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.SERVICE)
            Case Equals(sender, txt_Size)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PIPE_SIZE)
            Case Equals(sender, txt_Schedule)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.SCHEDULE)
            Case Equals(sender, txt_Initial_Year)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.INITIAL_YEAR)
            Case Equals(sender, txt_Location_From)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.LOCATION_FROM)
            Case Equals(sender, txt_Location_To)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.LOCATION_TO)
            Case Equals(sender, txt_Norminal_Thickness)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.NORMINAL_THICKNESS)
            Case Equals(sender, txt_Calculated_Thickness)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.CALCULATED_THICKNESS)
            Case Equals(sender, ddl_INSULATION)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.INSULATION_ID)
            Case Equals(sender, txt_Insulation_Thickness)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.INSULATION_THICKNESS)
            Case Equals(sender, txt_Pressure_Design)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.DESIGN_PRESSURE)
            Case Equals(sender, txt_Pressure_Operating)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.OPERATING_PRESSURE)
            Case Equals(sender, txt_Temperature_Design)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.DESIGN_TEMPERATURE)
            Case Equals(sender, txt_Temperature_Operating)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.OPERATING_TEMPERATURE)
            Case Equals(sender, ddl_MAT)
                UpdateClass()
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.MATERIAL)
            Case Equals(sender, ddl_PRESSURE)
                UpdateClass()
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PRESSURE_CODE)
            Case Equals(sender, txt_Corrosion_Allowance)
                UpdateClass()
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.CORROSION_ALLOWANCE)
            Case Equals(sender, ddl_MED)
                UpdateClass()
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.SERVICE_MEDIA)
            Case Equals(sender, rdo_Class_Auto) Or Equals(sender, rdo_Class_Manual) Or Equals(sender, txt_Class)
                UpdateClass()
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PIPE_CLASS)
            Case Equals(sender, txt_P_ID_No)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.P_ID_No)
        End Select

    End Sub

    Private Sub rdo_Class_CheckedChanged(sender As Object, e As EventArgs) Handles rdo_Class_Auto.CheckedChanged, rdo_Class_Manual.CheckedChanged
        lbl_Class.Visible = rdo_Class_Auto.Checked
        txt_Class.Visible = rdo_Class_Manual.Checked
        If txt_Class.Visible Then
            txt_Class.Focus()
        End If
    End Sub

    Private Sub ddl_PLANT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_PLANT.SelectedIndexChanged
        If PLANT_ID = 0 Then
            BL.BindDDlArea(ddl_AREA, AREA_ID)
        Else
            BL.BindDDlArea(PLANT_ID, ddl_AREA, AREA_ID)
        End If

    End Sub
End Class