﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class GL_DialogUploadDocument
    
    '''<summary>
    '''pnlDialog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDialog As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblTAGCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTAGCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ImgPreview control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgPreview As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''ful1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ful1 As Global.AjaxControlToolkit.AsyncFileUpload
    
    '''<summary>
    '''btnUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpload As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''txt_Name control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Name As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_Recomment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Recomment As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdate As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlValidation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlValidation As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnValidationClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnValidationClose As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblValidation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblValidation As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlDialog_DragPanelExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDialog_DragPanelExtender As Global.AjaxControlToolkit.DragPanelExtender
End Class
