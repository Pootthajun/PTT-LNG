﻿Imports System.Data
Imports System.Data.SqlClient

Public Class THM_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
        End If

        HideValidator()
    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListTag.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        BL.BindDDl_THM_Type(ddl_Edit_Type, False)
        BL.BindDDlPlant(ddl_Edit_Plant, False)
        BL.BindDDl_THM_Route(0, 0, ddl_Edit_Route)
        txtTagCode.Text = ""
        txtTagCode.Attributes("TagID") = "0"
        txtTagName.Text = ""
        txtDesc.Text = ""
        chkAvailable.Checked = True
        ddl_Search_Type.Enabled = True
        ddl_Search_Plant.Enabled = True
        ddl_Search_Route.Enabled = True

        btnCreate.Visible = True
        pnlListTag.Enabled = True

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDl_THM_Type(ddl_Search_Type, False)
        BL.BindDDlPlant(ddl_Search_Plant, False)
        BL.BindDDl_THM_Route(0, 0, ddl_Search_Route)
    End Sub

    Protected Sub ddl_Search_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Type.SelectedIndexChanged
        BL.BindDDl_THM_Route(ddl_Search_Type.Items(ddl_Search_Type.SelectedIndex).Value, ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BindTag()
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_THM_Route(ddl_Search_Type.Items(ddl_Search_Type.SelectedIndex).Value, ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BindTag()
    End Sub

    Protected Sub ddl_Search_Route_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged
        BindTag()
    End Sub

    Protected Sub ddl_Edit_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Type.SelectedIndexChanged
        BL.BindDDl_THM_Route(ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value, ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Route)
    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        BL.BindDDl_THM_Route(ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value, ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Route)
    End Sub

#End Region

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTag()

        Dim SQL As String = "SELECT MS_THM_TYPE.THM_TYPE_Name,MS_PLANT.PLANT_Code,MS_THM_ROUTE.ROUTE_Code,TAG_ID," & vbNewLine
        SQL &= "   TAG_Code,TAG_Name,MS_THM_TAG.Active_Status,MS_THM_TAG.Update_Time" & vbNewLine
        SQL &= "   FROM MS_THM_TAG" & vbNewLine
        SQL &= "   LEFT JOIN MS_THM_TYPE ON MS_THM_TAG.THM_TYPE_ID=MS_THM_TYPE.THM_TYPE_ID  " & vbNewLine
        SQL &= "   LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_THM_TAG.PLANT_ID" & vbNewLine
        SQL &= "   LEFT JOIN MS_THM_ROUTE ON MS_THM_TAG.ROUTE_ID=MS_THM_ROUTE.ROUTE_ID" & vbNewLine
        Dim WHERE As String = ""
        If ddl_Search_Type.SelectedIndex > 0 Then
            WHERE &= " MS_THM_TYPE.THM_TYPE_ID=" & ddl_Search_Type.Items(ddl_Search_Type.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " MS_THM_ROUTE.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY THM_TYPE_Name,MS_PLANT.PLANT_Code,MS_THM_ROUTE.ROUTE_Code" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("MS_THM_TAG") = DT

        Navigation.SesssionSourceName = "MS_THM_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblTagCode As Label = e.Item.FindControl("lblTagCode")
        'Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        lblType.Text = e.Item.DataItem("THM_TYPE_Name")
        lblPlant.Text = e.Item.DataItem("PLANT_Code")
        lblRoute.Text = e.Item.DataItem("ROUTE_Code")
        lblTagCode.Text = CStr(e.Item.DataItem("TAG_Code").ToString + " " + e.Item.DataItem("TAG_Name").ToString).Trim
        'lblTagName.Text = e.Item.DataItem("TAG_Name")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        btnDelete.Visible = Session("USER_LEVEL") = EIR_BL.User_Level.Administrator
    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListTag.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_THM_TAG" & vbNewLine
                SQL &= " WHERE TAG_ID=" & TAG_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Dim TYPE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("THM_TYPE_ID")) Then TYPE_ID = DT.Rows(0).Item("THM_TYPE_ID")
                Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
                Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")

                BL.BindDDl_THM_Type(ddl_Edit_Type, TYPE_ID)
                BL.BindDDlPlant(ddl_Edit_Plant, PLANT_ID)
                BL.BindDDl_THM_Route(TYPE_ID, PLANT_ID, ddl_Edit_Route, ROUTE_ID)
                txtTagCode.Text = DT.Rows(0).Item("TAG_CODE").ToString
                txtTagCode.Attributes("TagID") = TAG_ID
                txtTagName.Text = DT.Rows(0).Item("TAG_NAME").ToString
                txtDesc.Text = DT.Rows(0).Item("TAG_Description").ToString
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                btnSave.Focus()

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE MS_THM_TAG Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
            Case "Delete"
                Dim SQL As String = ""
                SQL &= "DELETE FROM RPT_THM_Detail WHERE  TAG_ID=" & TAG_ID & vbNewLine
                SQL &= "DELETE FROM MS_THM_TAG WHERE  TAG_ID=" & TAG_ID & vbNewLine

                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        txtTagCode.ReadOnly = False
        ddl_Search_Type.Enabled = False
        ddl_Search_Plant.Enabled = False
        ddl_Search_Route.Enabled = False
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListTag.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddl_Edit_Type.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Equipement Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagCode.Text = "" Then
            lblValidation.Text = "Please insert Tag code"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagName.Text = "" Then
            lblValidation.Text = "Please insert Tag name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Route.Text = "" Then
            lblValidation.Text = "Please create route"
            pnlValidation.Visible = True
            Exit Sub
        End If


        Dim SQL As String = ""
        Dim TagID As Integer = txtTagCode.Attributes("TagID")
        SQL = "SELECT * FROM MS_THM_TAG WHERE TAG_ID=" & TagID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            SQL = "SELECT * FROM MS_THM_TAG WHERE TAG_CODE = '" & txtTagCode.Text.Replace("'", "''") & "'"
            Dim DA_ As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT_ As New DataTable
            DA_.Fill(DT_)
            If DT_.Rows.Count > 0 Then
                lblValidation.Text = "This tag is already existed"
                pnlValidation.Visible = True
                Exit Sub
            End If

            DR = DT.NewRow
            TagID = GetNewTagID()
            DR("TAG_ID") = TagID
        Else
            DR = DT.Rows(0)
        End If

        DR("THM_TYPE_ID") = ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value
        DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
        DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        DR("TAG_Code") = txtTagCode.Text
        DR("TAG_Name") = txtTagName.Text
        DR("TAG_Description") = txtDesc.Text
        DR("TAG_Order") = TagID
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        DT = Session("MS_THM_TAG")
        DT.DefaultView.RowFilter = "TAG_ID=" & TagID
        If DT.DefaultView.Count > 0 Then
            Dim RowIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            DT.DefaultView.RowFilter = ""
            Navigation.CurrentPage = Math.Ceiling((RowIndex + 1) / Navigation.PageSize)
        End If
        ClearPanelEdit()
    End Sub

    Private Function GetNewTagID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM MS_THM_TAG "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class