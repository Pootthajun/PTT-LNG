﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_Template2.ascx.vb" Inherits="EIR.UC_ST_TA_Template2" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_TextEditor.ascx" tagname="UC_TextEditor" tagprefix="uc1" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<%--------------------------------------------------------------------------------------%>

<tr  style="text-align: center">
    <td style="width: 50%; background-color: black; vertical-align :top  ;" >
        <asp:Image ImageUrl="RenderImage.aspx" ID="ImgPreview1" runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;cursor: pointer; text-align :center;max-width : 100%;min-width :100% ;max-height : 300px;min-height :50% ;" />
        <asp:Button ID="btnRefreshImage" runat="server" Text="Update/Refresh" Style="visibility: hidden; width: 0px;" />


    </td>
    <td style="width: 50%; vertical-align :top ;"  >
        <uc1:UC_TextEditor ID="UC_TextEditor1" runat="server"   />


    </td>   

</tr>




<tr id="trimage_1" runat="server" visible="false" style="text-align: center">
    <td style="width: 50%; background-color: black;" colspan="2" >
        <div class="thumbnail">


            <a id="lnk_File_Dialog1" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File1" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>

</tr>
<%--------------------------------------------------------------------------------------%>
<tr id="trbtn_1" runat="server" visible="false">
    <td style="width: 50%;" class="toolbar" colspan="2">
        <asp:Button ID="btnUpload1" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete1"></cc1:ConfirmButtonExtender>
    </td>
</tr>
<!-- End Table Images -->

<%--------------------------------------------------------------------------------------%>



