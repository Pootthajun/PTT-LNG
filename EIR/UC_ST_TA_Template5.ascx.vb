﻿
Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient

Public Class UC_ST_TA_Template5
    Inherits System.Web.UI.UserControl
    Dim BL As New EIR_BL
    Dim C As New Converter

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property


    Public Property RPT_Absorber_Step_ID() As Integer
        Get
            If IsNumeric(ViewState("RPT_Absorber_Step_ID")) Then
                Return ViewState("RPT_Absorber_Step_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Absorber_Step_ID") = value
        End Set
    End Property

    Private Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_TYPE_ID")) Then
                Return ViewState("TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_TYPE_ID") = value
        End Set
    End Property

    Private Property INSP_ID() As Integer
        Get
            If IsNumeric(ViewState("INSP_ID")) Then
                Return ViewState("INSP_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("INSP_ID") = value
        End Set
    End Property

    Private Property PLANT_ID() As Integer
        Get
            If IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
        End Set
    End Property

    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Private Property RPT_DATE() As String
        Get
            If (ViewState("RPT_DATE") <> "") Then
                Return ViewState("RPT_DATE")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("RPT_DATE") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Public Property TAG_CLASS() As EIR_BL.Tag_Class
        Get
            If ViewState("TagClass") = "RO" Then
                Return EIR_BL.Tag_Class.Rotating
            Else
                Return EIR_BL.Tag_Class.Stationary
            End If
        End Get
        Set(ByVal value As EIR_BL.Tag_Class)
            Select Case value
                Case EIR_BL.Tag_Class.Rotating
                    ViewState("TagClass") = "RO"
                Case Else
                    ViewState("TagClass") = "ST"
            End Select
        End Set
    End Property


    Private Property ImageFile1 As EIR_BL.PipeDrawingDetail
        Get
            Try
                Return Session("PIPE_CUI_Edit2_1_" & UNIQUE_POPUP_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As EIR_BL.PipeDrawingDetail)
            Session("PIPE_CUI_Edit2_1_" & UNIQUE_POPUP_ID) = value
            If Not IsNothing(value) Then
                img_File1.ImageUrl = "RenderPipeDrawing.aspx?Mode=PIPE_CUI_Edit2&UNIQUE_POPUP_ID=" & UNIQUE_POPUP_ID & "&ImageID=1&t=" & Now.ToOADate
                btnDelete1.Visible = True
            Else
                img_File1.ImageUrl = "Resources/Images/Sample_40.png"
                btnDelete1.Visible = False
            End If
        End Set
    End Property

    'Public Property ImageButtonImage As ImageButton
    '    Get
    '        Return imgImage
    '    End Get
    '    Set(value As ImageButton)
    '        imgImage = value
    '    End Set
    'End Property

    Public Property TextDetail As UC_TextEditor
        Get
            Return UC_TextEditor1
        End Get
        Set(value As UC_TextEditor)
            UC_TextEditor1 = value
        End Set
    End Property

    Private Sub UC_StationaryOffRoutineTemplate1_Load(sender As Object, e As EventArgs) Handles Me.Load

        RPT_Year = Request.QueryString("RPT_Year")
        RPT_No = Request.QueryString("RPT_No")

    End Sub

#Region "Static Property"


    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return lblProperty.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            If value <> UNIQUE_POPUP_ID Then
                MY_PREVIEW1 = Nothing
            End If
            lblProperty.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property Sector_ID() As String
        Get
            Return lblProperty.Attributes("Sector_ID")
        End Get
        Set(ByVal value As String)

            lblProperty.Attributes("Sector_ID") = value
        End Set
    End Property
#End Region


#Region "Image"



    Public Sub ImageUrl(Para As String)

        'Dim Para As String = "&DETAIL_ID=" & DETAIL_ID & "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&INSP_ID=" & INSP_ID & "&RPT_DATE=" & RPT_DATE
        ImgPreview1.ImageUrl = "RenderImage_ST_TA_Sector.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & EIR_BL.Tag_Class.Stationary & Para



    End Sub

    Public Property MY_PREVIEW1() As Byte()
        Get
            'If BL.IsInspectionRequirePicture(INSP_ID) Then
            Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1")
            'Else
            '    Return Nothing
            'End If
        End Get
        Set(ByVal value As Byte())
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = value
            Dim Para As String = "&DETAIL_ID=" & DETAIL_ID & "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&INSP_ID=" & INSP_ID
            ImgPreview1.ImageUrl = "RenderImage_ST_TA.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & EIR_BL.Tag_Class.Stationary & Para

        End Set
    End Property

    Protected Sub btnRefreshImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshImage.Click
        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_1")) Then
            MY_PREVIEW1 = Session("TempImage_" & UNIQUE_POPUP_ID & "_1")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_1") = Nothing
        End If
        ImgPreview1.ImageUrl = "../RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & CInt(TAG_CLASS)

        Save_Image()
    End Sub


    Public Property Disabled() As Boolean
        Get
            Return Not False
        End Get
        Set(ByVal value As Boolean)
            btnRefreshImage.Visible = Not value


            If Not value Then
                ImgPreview1.Attributes("OnClick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',1,document.getElementById('" & btnRefreshImage.ClientID & "'));"
                ImgPreview1.ToolTip = "Click to edit picture"
            Else
                ImgPreview1.Attributes("OnClick") = "window.open(this.src);"
                ImgPreview1.ToolTip = "Click to view picture"
            End If

            'btnUpdate.Visible = Not value

        End Set
    End Property



#End Region


#Region "Image Functional"
    Private Sub btnEditFile_Click(sender As Object, e As ImageClickEventArgs) Handles btnEdit1.Click
        Dim btnEdit As ImageButton = sender
        Dim ImageID As Integer = btnEdit.ID.Replace("btnEdit", "")
        Dim RefreshButton As Button = Nothing

        Dim Obj As EIR_BL.PipeDrawingDetail = Nothing
        Select Case ImageID
            Case 1
                Obj = ImageFile1
                RefreshButton = btnUpload1
                'Case 2
                '    Obj = ImageFile2
                '    RefreshButton = btnUpload2
                'Case 3
                '    Obj = ImageFile3
                '    RefreshButton = btnUpload3
                'Case 4
                '    Obj = ImageFile4
                '    RefreshButton = btnUpload4
        End Select
        If Not IsNothing(Obj) Then
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_" & ImageID) = Obj.File_Data
        Else
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_" & ImageID) = Nothing
        End If

        Dim Script As String = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "'," & ImageID & ",document.getElementById('" & RefreshButton.ClientID & "'));"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "DialogEditImage", Script, True)

    End Sub

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload1.Click
        Dim btnUpload As Button = sender
        Dim ImageID As Integer = btnUpload.ID.Replace("btnUpload", "")

        If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_" & ImageID)) Then Exit Sub

        Dim b As Byte() = Session("TempImage_" & UNIQUE_POPUP_ID & "_" & ImageID)
        Dim Obj As New EIR_BL.PipeDrawingDetail
        Obj.File_Data = b
        Obj.File_ID = ImageID
        Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(b))
        Obj.File_Type = BL.GetImageContentType(img)

        Select Case ImageID
            Case 1
                ImageFile1 = Obj
                'Case 2
                '    ImageFile2 = Obj
                'Case 3
                '    ImageFile3 = Obj
                'Case 4
                '    ImageFile4 = Obj
        End Select

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As ImageClickEventArgs) Handles btnDelete1.Click
        Dim btnDelete As ImageButton = sender
        Dim ImageID As Integer = btnDelete.ID.Replace("btnDelete", "")

        Select Case ImageID
            Case 1
                ImageFile1 = Nothing
                'Case 2
                '    ImageFile2 = Nothing
                'Case 3
                '    ImageFile3 = Nothing
                'Case 4
                '    ImageFile4 = Nothing
        End Select
    End Sub


#End Region


    Public Sub Save_Image()

        '-------- Save Picture ---------
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM MS_Template_Sector " & vbNewLine
        SQL &= "    WHERE  Sector_ID=" & Sector_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow

        If (DT.Rows.Count > 0) Then
            DR = DT.Rows(0)
            Dim Path As String = BL.Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\" & Sector_ID & "_"
            If Not IsNothing(MY_PREVIEW1) Then
                If BL.Save_Picture_File_ST_TA_Template(MY_PREVIEW1, RPT_Year, RPT_No, Sector_ID) Then
                    DR("Pic_Detail1") = True
                Else
                    DR("Pic_Detail1") = False
                    'lblValidation.Text = "Unable to save left file"
                    'pnlValidation.Visible = True
                    Exit Sub
                End If
            Else
                BL.Save_Picture_File_ST_TA_Template(Nothing, RPT_Year, RPT_No, Sector_ID) '---------Force Delete ---------
                DR("Pic_Detail1") = False
            End If

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
                DT.AcceptChanges()

            Catch ex As Exception
                'lblValidation.Text = ex.Message
                'pnlValidation.Visible = True
                Exit Sub
            End Try

        End If

    End Sub

End Class