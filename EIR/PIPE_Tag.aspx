﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_Tag.aspx.vb" Inherits="EIR.PIPE_Tag" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register Src="~/UC_Pipe_Point_Info.ascx" TagPrefix="uc1" TagName="UC_Pipe_Point_Info" %>

<%@ Register src="UC_Pipe_Tag_Info.ascx" tagname="UC_Pipe_Tag_Info" tagprefix="uc" %>
<%@ Register Src="~/UC_FileAlbum.ascx" TagPrefix="uc" TagName="UC_FileAlbum" %>


<asp:Content ID="ContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/File_Album.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/Dropdown_Popover.css" type="text/css" media="all" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
			
			<!-- Page Head -->
			<h2>Pipe Setting </h2>			
						
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <asp:Panel ID="pnlSearch" runat="server">
              <div class="content-box-header" style="height:auto; padding-top:5px; padding-bottom:5px;">
                <div>
                    <h3 style="margin-top:-5px; width:120px;">Search TAG</h3>
				    <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Area" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                   <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Process" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:TextBox runat="server" ID="txt_Search_LineNo" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="50px" PlaceHolder="Line No"></asp:TextBox>
                    
                    <asp:TextBox runat="server" ID="txt_Search_Size" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="50px" PlaceHolder="Size"></asp:TextBox>

                    <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Material" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Pressure" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:TextBox runat="server" ID="txt_Search_CA" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="50px" PlaceHolder="CA"></asp:TextBox>
                    
                    <asp:DropDownList CssClass="select"
                      ID="ddl_Search_Service" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Insulation" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:TextBox runat="server" ID="txt_search_Insulation_Thickness" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="100px" PlaceHolder="Ins Thickness"></asp:TextBox>

                </div>
                <div>
                    <h3 style="margin-top:-5px; width:120px;">Search Code</h3>
                    <asp:TextBox runat="server" ID="txt_Search_Code" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="150px" MaxLength="50" PlaceHolder="Tag code"></asp:TextBox>
                 </div>
              </div>
            <div class="content-box-content">
                <div class="tab-content default-tab">
                  <asp:Panel ID="pnlListTag" runat="server">
                       <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                          <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                          <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                       </asp:Panel>
                  
                      <table class="tbTagList">
                        <thead>
                          <tr>
                            <th style="text-align:center;"><a href="#">Tag Code</a></th>
                            <th style="text-align:center;"><a href="#">Plant</a></th>
                            <th style="text-align:center;"><a href="#">Material</a></th>
                            <th style="text-align:center;"><a href="#">Inspection Point(s)</a></th>
                            <th style="text-align:center;"><a href="#">For service</a></th>
                            <th style="text-align:center;"><a href="#">Loop No</a></th>
                            <th style="text-align:center;"><a href="#">Action</a></th>
                          </tr>
                        </thead>
                   
                        <asp:Repeater ID="rptTag" runat="server">
                               <HeaderTemplate>
                                <tbody>
                               </HeaderTemplate>
                               <ItemTemplate>
                                      <tr>
                                        <td><asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblMaterial" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblPoint" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblService" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblLoopNo" runat="server"></asp:Label></td>
                                        <td><!-- Icons -->
                                              <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                              <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                              <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" />
                                              <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete ?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>
                                      </tr>                                  
                                 </ItemTemplate>
                                <FooterTemplate>
                                 </tbody>
                                </FooterTemplate>
                               </asp:Repeater>
                         <tfoot>
                          <tr>
                            <td colspan="6">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                            </div>
                                <uc1:PageNavigation ID="Navigation" runat="server" />
                              <!-- End .pagination -->
                              <div class="clear"></div>  
                            </td>
                          </tr>
                        </tfoot>
                      </table>				  
				         <div class="clear"></div>
				     <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                          <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				          <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				     </asp:Panel>
			        </asp:Panel>
                  
                    </div>
                  </div>
              </asp:Panel>
            
             <asp:Panel ID="pnlEdit" runat="server">
             
                
                  <!-- This is the target div. id must match the href of this div's tab -->			    
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Pipe Info : <asp:Label ID="lblTagCode" runat="server"></asp:Label></h3>
                    <ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabTag" runat="server" CssClass="default-tab current">Tag Info</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabPoint" runat="server">Inspection Point(s)</asp:LinkButton></li>
					</ul>					
					<div class="clear"></div>
                  </div>  
                  
                 <asp:Panel ID="TabTag" runat="server">

                 <uc:UC_Pipe_Tag_Info ID="Pipe_Info" runat="server" />
                 <uc:UC_FileAlbum ID="Pipe_File" runat="server"  />

                </asp:Panel>
                 <asp:Panel ID="TabPoint" runat="server">
                     
                     <asp:Panel ID="pnlListPoint" runat="server">
                     <div class="content-box-header" style="background-color:white; background-image:none;">
                            <h3>Total Inspection Point(s)</h3>
                     </div>  
 
                      <table style="width:100%;">
                          <tr>
                              <td style="width:60%; text-align-last:start; vertical-align:top;">
                                  <table class="tbPointList">
                                    <thead>
                                      <tr>
                                        <th style="text-align-last:center;"><a href="javascript:;">#</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Name</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Location/Component</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Attachment(s)</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Remaining Life(s)</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Action</a></th>
                                      </tr>
                                    </thead>
                   
                                    <asp:Repeater ID="rptPoint" runat="server">
                                           <HeaderTemplate>
                                                <tbody>
                                           </HeaderTemplate>
                                           <ItemTemplate>
                                                  <tr id="trPoint" runat="server">
                                                    <td style="text-align-last:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                    <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><asp:Label ID="lblComponent" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><asp:Label ID="lblFile" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><asp:Label ID="lblLife" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><!-- Icons -->
                                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                                          <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" />
                                                          <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" TargetControlID="btnDelete" ConfirmText="Are you sure you want to delete ?"></Ajax:ConfirmButtonExtender>
                                                    </td>
                                                  </tr>                                  
                                             </ItemTemplate>
                                            <FooterTemplate>
                                             </tbody>
                                            </FooterTemplate>
                                           </asp:Repeater>                                     
                                  </table>
								  <asp:LinkButton ID="btnAddPoint" runat="server" style="margin-top:15px;" CssClass="button" Text="Add Point"></asp:LinkButton>
                              </td>
                              <td style="width:40%">
                                  <asp:Panel ID="posOverview" runat="server" ClientIDMode="Static" width="100%" BorderColor="Gray" BorderStyle="Solid"  BorderWidth="1px">
                                  </asp:Panel>
                              </td>
                          </tr>
                      </table>
                      </asp:Panel>
                      <asp:Panel ID="pnlEditPoint" runat="server">
                          <div class="content-box-header" style="background-color:white; background-image:none;">
                            <h3><asp:Label ID="lblEditPoint" runat="server"></asp:Label></h3>                            
                        </div> 
                          <uc1:UC_Pipe_Point_Info runat="server" ID="Point_Info" />
                          <uc:UC_FileAlbum runat="server" ID="Point_File" />
                          <p align="right" style="padding-right:20px; margin-top:20px; margin-bottom:20px; Style="float:right;">
                            <asp:Button ID="btnCancelPoint" runat="server" CssClass="button" Text="Back to see all point" style="margin-right:10px;" />
                            <asp:Button ID="btnOKPoint" runat="server" CssClass="button" Text="OK" />
                          </p> 
                      </asp:Panel>
                      
                       <asp:Panel ID="pnlBindingPointError" runat="server" CssClass="notification attention png_bg">
                          <asp:ImageButton ID="btnBindingPointError" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                          <div> <asp:Label ID="lblBindingPointError" runat="server"></asp:Label></div>
                       </asp:Panel>
                      
                 </asp:Panel>


                  <asp:Panel ID="pnlSaveTag" runat="server">

                       <p style="width:120px; margin-left:20px; margin-top:20px; float:left;">
                            <label class="column-left" >Available &nbsp;</label>
                            <asp:CheckBox ID="chkAvailable" runat="server" Text=""/>
                        </p>
                        <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg" Style="float:unset;">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <p align="right" style="padding-right:20px; margin-top:20px; margin-bottom:20px; Style="float:right;">
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Back to see all tag" style="margin-right:10px;" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                        </p>     
                  </asp:Panel>
                        
                       
                 
                 
                          		
             </asp:Panel>
		  </div>		  

     <script type="text/javascript">
        function resizeTabPoint() {
            if (!document.getElementById("posOverview")) return;
            $('#posOverview').height($('#posOverview').width());
        }
    </script>

</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>
