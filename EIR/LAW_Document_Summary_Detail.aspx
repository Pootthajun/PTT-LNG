﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LAW_Document_Summary_Detail.aspx.vb" Inherits="EIR.LAW_Document_Summary_Detail" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<%@ Register src="UC_DocumentTreePlanPaper.ascx" tagname="UC_DocumentTreePlanPaper" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
			<!-- Page Head -->
			<h2>
                <asp:Label id="lblDocumentName" runat="server"></asp:Label>
                <asp:Label id="lblDocumentPlanID" runat="server" Text="0" Visible="false" ></asp:Label>
			</h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td colspan="2">
				    <h3 > 
				        <asp:Label ID="Label1" runat="server" Text="Year" ForeColor="#009933"></asp:Label>
				        &nbsp;<asp:Label id="lblYear" runat="server"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;

                        <asp:Label ID="Label2" runat="server" ForeColor="#009933" Text="Plant"></asp:Label>
                        &nbsp;<asp:Label id="lblPlantCode" runat="server"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;

                        <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="Tag No"></asp:Label>
                        &nbsp;<asp:Label id="lblTagNo" runat="server"></asp:Label>

                        <asp:Label ID="Label4" runat="server" ForeColor="#009933" Text="Status"></asp:Label>
                        &nbsp;<asp:Label id="lblPlanStatus" runat="server"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;

                        <asp:Label ID="Label5" runat="server" ForeColor="#009933" Text="Next Notice Date"></asp:Label>
                        &nbsp;<asp:TextBox runat="server" ID="txtNextNoticeDate" CssClass="text-input small-input " MaxLength="20" AutoPostBack="true" ></asp:TextBox>
				        <cc1:CalendarExtender ID="txtNextNoticeDate_CalendarExtender" runat="server" 
                            Format="dd-MMM-yyyy" TargetControlID="txtNextNoticeDate" PopupPosition="Right" >
                        </cc1:CalendarExtender>
                    </h3>         
                </td>
			  </tr>
			  <tr>
			    <td>
			        <asp:Panel ID="pnlDashboard" runat="server">
			            <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
                            <tr>
                                <td style="vertical-align:top; width:700px;">
                                    <asp:Chart ID="ChartMain" runat="server" Height="400px" BackColor="" CssClass="ChartHighligh" Width="700px"  >
                                        <legends>
                                            <asp:Legend Name="Legend1" LegendStyle="Row" 
                                                Docking="Bottom"></asp:Legend>
                                        </legends>
                                        <titles>
                                            <asp:Title Name="Title1" font="Microsoft Sans Serif, 12pt, style=Bold" 
                                                forecolor="SteelBlue">
                                        </asp:Title>
                                        </titles>
                                        <Series>
                                            <asp:Series Name="Series1" ChartType="Line" ChartArea="ChartArea1" 
                                                Color="Green" BorderWidth="3"  Legend="Legend1" LegendText="Plan" ShadowColor="">
                                            </asp:Series>
                                            <asp:Series Name="Series2" ChartType="Line" ChartArea="ChartArea1" 
                                                Color="MediumSlateBlue" BorderWidth="3" Legend="Legend1" LegendText="Actual">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <axisy intervalautomode="VariableCount" IsLabelAutoFit="False" LineColor="Gray" 
                                                    Maximum="100" TitleForeColor="Gray" Title="Progress %">
                                                    <majorgrid linecolor="Gainsboro" />
                                                    <minorgrid enabled="True" interval="5" linecolor="WhiteSmoke" />
                                                    <majortickmark enabled="False" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" ForeColor="Gray" 
                                                        Interval="10" />
                                                </axisy>
                                                <axisx IsLabelAutoFit="False" 
                                                    IsMarginVisible="False" LineColor="Gray" IntervalType="Months" 
                                                    LabelAutoFitStyle="IncreaseFont, DecreaseFont, StaggeredLabels, LabelsAngleStep30, LabelsAngleStep45" 
                                                    TitleAlignment="Far" TitleForeColor="DimGray">
                                                    <majorgrid enabled="False" linecolor="DimGray" />
                                                    <majortickmark enabled="False" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 6.75pt" ForeColor="Gray" 
                                                        Interval="Auto" />
                                                </axisx>
                                                <axisx2 intervalautomode="VariableCount">
                                                </axisx2>
                                                <axisy2 intervalautomode="VariableCount">
                                                </axisy2>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        </asp:Chart>
                                </td>
                            </tr>
                        </table>
			        </asp:Panel>
			    </td> 

                  <td align="center" style="vertical-align: top; text-align: center; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:320px; border:1px solid #efefef;">
                      <tr>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                              Notice Date</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                              Document Name</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                              Plan (%)</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                              Actual (%)</td>
                      </tr>
                      <asp:Repeater ID="rptData" runat="server">
                        <ItemTemplate>
                            <tr id="tbTag" runat="server" style="border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblNoticeDate" runat="server"></asp:Label></td>
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblDocumentName" runat="server"></asp:Label></td>
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblPlan" runat="server" ForeColor="Green"></asp:Label></td>
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblActual" runat="server" ForeColor="Blue"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                      </asp:Repeater>
                    </table> 
                  </td>
		      </tr>
                <tr>
                    <td colspan="2">
                        <uc1:UC_DocumentTreePlanPaper ID="UC_DocumentTreePlanPaper1" runat="server" />
                    </td>
                </tr>
			</table>

</asp:Content>
