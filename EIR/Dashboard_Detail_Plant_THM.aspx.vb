﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Detail_Plant_THM
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim SQL As String = ""
            Dim DT_PLANT As New DataTable
            Dim DA As New SqlDataAdapter
            SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & Request.QueryString("PLANT_ID")
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT_PLANT)
            If DT_PLANT.Rows.Count > 0 Then
                lblPlantName.Text = DT_PLANT.Rows(0).Item("PLANT_Name").ToString
            End If
            lblBack.Text = "Back to see on this plant " & lblPlantName.Text
            lblMonth.Text = Request.QueryString("MONTH")
            lblYear.Text = Request.QueryString("YEAR")
            Select Case Request.QueryString("REPORT")
                Case EIR_BL.ReportName_Problem.NEW_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "New Problem Occurred"
                    lblReport.Text = EIR_BL.Dashboard.New_Problem_Occurred
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Thermography_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                    BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
                Case EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.TOTAL_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_Total_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "Total Problem by Month"
                    lblReport.Text = EIR_BL.Dashboard.Total_Problem_by_month
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Thermography_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                    BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
            End Select
        End If

    End Sub

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month As Integer, ByVal Year As Integer, ByVal Equipment As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        If Year > 2500 Then
            Year = Year - 543
        End If

        Dim DateFrom As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)
        Select Case lblReport.Text
            Case EIR_BL.Dashboard.New_Problem_Occurred
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)
            Case EIR_BL.Dashboard.Total_Problem_by_month
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate)
                DT_Dashboard.DefaultView.RowFilter = "ICLS_ID > 0"
                DT_Dashboard = DT_Dashboard.DefaultView.ToTable
        End Select
        DT_Dashboard.DefaultView.Sort = "INSPECTION,TAG_CODE"
        DT_Dashboard = DT_Dashboard.DefaultView.ToTable

        '--------------- Get Report Code--------------
        Dim R() As String = {"RPT_Year", "RPT_No"}
        Dim RT As DataTable = DT_Dashboard.DefaultView.ToTable(True, R)
        RT.Columns.Add("RPT_Code")
        For i As Integer = 0 To RT.Rows.Count - 1
            RT.Rows(i).Item("RPT_Code") = BL.GetReportCode(RT.Rows(i).Item("RPT_Year"), RT.Rows(i).Item("RPT_No"))
        Next
        DT_Dashboard.Columns.Add("RPT_Code")
        For i As Integer = 0 To RT.Rows.Count - 1
            DT_Dashboard.DefaultView.RowFilter = "RPT_Year=" & RT.Rows(i).Item("RPT_Year") & " AND RPT_No=" & RT.Rows(i).Item("RPT_No")
            For j As Integer = 0 To DT_Dashboard.DefaultView.Count - 1
                DT_Dashboard.DefaultView(j).Item("RPT_Code") = RT.Rows(i).Item("RPT_Code")
            Next
        Next
        DT_Dashboard.AcceptChanges()
        DT_Dashboard.DefaultView.RowFilter = ""

        rptData.DataSource = DT_Dashboard
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblEqm As Label = e.Item.FindControl("lblEqm")
        Dim lblReport As Label = e.Item.FindControl("lblReport")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim tr As HtmlTableRow = e.Item.FindControl("tr")

        lblTag.Text = e.Item.DataItem("TAG_Code").ToString.Trim
        lblEqm.Text = e.Item.DataItem("INSPECTION").ToString
        lblReport.Text = e.Item.DataItem("RPT_Code").ToString
        If e.Item.DataItem("ICLS_ID").ToString <> "" Then
            If e.Item.DataItem("ICLS_ID") = 0 Then
                lblStatus.Text = "-"
            Else
                lblStatus.Text = "Abnormal"
                lblStatus.Style("color") = "Red"
                lblStatus.Style("cursor") = "pointer"
            End If
        Else
            lblStatus.Text = "-"
        End If

        tr.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_THM.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "','Dialog_Tag_THM_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")

    End Sub

End Class