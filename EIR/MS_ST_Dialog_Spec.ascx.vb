﻿Public Class MS_ST_Dialog_Spec
    Inherits System.Web.UI.UserControl
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BL.BindDDlPlant(ddl_Edit_Plant, False)
            BL.BindDDl_ST_Route(0, ddl_Edit_Route)
            BL.BindDDlArea(0, ddl_Edit_Area)
            BL.BindDDlProcess(ddl_Edit_Process)
            BL.BindDDlST_TA_TagType(ddl_Edit_Type)
            txtTagNo.Text = ""
            txtTagNo.Attributes("TagID") = "0"
            txtTagName.Text = ""

            DialogAbsorber_Info.CloseDialog()
            DialogColumn_Info.CloseDialog()
            DialogFilter_Info.CloseDialog()
            DialogDrum_Info.CloseDialog()
            DialogHeat_Exchnager_Info.CloseDialog()
        End If
    End Sub

    Public Sub ClearDialog()
        DialogAbsorber_Info.CloseDialog()
        DialogColumn_Info.CloseDialog()
        DialogFilter_Info.CloseDialog()
        DialogDrum_Info.CloseDialog()
        DialogHeat_Exchnager_Info.CloseDialog()
    End Sub

    Public Sub CloseDialog()
        Me.Visible = False
    End Sub

    Public Sub ShowDialog()
        Me.Visible = True
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Private Sub ddl_Edit_Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Edit_Type.SelectedIndexChanged

        '<uc10:MS_ST_DialogAbsorber_Info ID = "DialogAbsorber_Info" runat="server" />
        '       <uc11:MS_ST_DialogColumn_Info ID = "DialogColumn_Info" runat="server" />
        '       <uc12:MS_ST_DialogFilter_Info ID = "DialogFilter_Info" runat="server" />
        '       <uc13:MS_ST_DialogDrum_Info ID = "DialogDrum_Info" runat="server" />
        '       <uc14:MS_ST_DialogHeat_Exchnager_Info ID = "DialogHeat_Exchnager_Info" runat="server" />

        DialogAbsorber_Info.CloseDialog()
        DialogColumn_Info.CloseDialog()
        DialogFilter_Info.CloseDialog()
        DialogDrum_Info.CloseDialog()
        DialogHeat_Exchnager_Info.CloseDialog()

        Select Case ddl_Edit_Type.SelectedValue
            Case EIR_BL.ST_TAG_TYPE.Drum
                DialogDrum_Info.ShowDialog()

            Case EIR_BL.ST_TAG_TYPE.Absorber
                DialogAbsorber_Info.ShowDialog()

            Case EIR_BL.ST_TAG_TYPE.Column
                DialogColumn_Info.ShowDialog()

            Case EIR_BL.ST_TAG_TYPE.Filter
                DialogFilter_Info.ShowDialog()

            Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager

                DialogHeat_Exchnager_Info.ShowDialog()

            Case EIR_BL.ST_TAG_TYPE.Strainer


            Case Else


        End Select

    End Sub
End Class