﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Pipe_Tag_Info.ascx.vb" Inherits="EIR.UC_Pipe_Tag_Info" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>

    <table cellpadding="0" cellspacing="0" class="propertyTable">
        
        <tr>		        
		    <td class="propertyGroup" style="width:25%; font-weight:bold;">
                Tag Code</td>
					        
		    <td class="propertyGroup" style="border:none; border-top:1px solid #CCCCCC; border-right:1px solid #CCCCCC;" colspan="2" >
                <asp:Label ID="lblCode" runat="server" Text="" Font-Bold="true"></asp:Label>
		    </td>
					        
		    <td class="propertyCaption" style="border:none; border-top:1px solid #CCCCCC; border-right:1px solid #CCCCCC;" >
                Loop No</td>
					        
		    <td class="propertyGroup" style="border:none; border-top:1px solid #CCCCCC; border-right:1px solid #CCCCCC;" colspan="2" >
                <asp:TextBox ID="txt_LoopNo" runat="server" MaxLength="10" AutoPostBack="True"></asp:TextBox>
		    </td>
	    </tr>	
        <tr>
            <td style="width:25%;" class="propertyCaption">Plant <font color="red">**</font></td>
            <td style="width:25%;" colspan="2">
                <asp:DropDownList ID="ddl_PLANT" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>                
            </td>
            <td style="width:25%;" class="propertyCaption">Area <font color="red">**</font></td>
            <td style="width:25%;" colspan="2">
                <asp:DropDownList ID="ddl_AREA" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>            
        </tr>
        <tr>
            <td class="propertyCaption">Process <font color="red">**</font></td>
            <td colspan="2" >
                <asp:DropDownList ID="ddl_PROCESS" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class="propertyCaption">Line No <font color="red">**</font></td>
            <td colspan="2" ><asp:TextBox ID="txt_LineNo" runat="server" Text="" Placeholder="..." MaxLength="5" AutoPostBack="True" ></asp:TextBox></td>
        </tr>
        <tr>					            
					        
		    <td class="propertyCaption">Service</td>					        
		    <td colspan="2">
                <asp:DropDownList ID="ddl_SERVICE" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>
		    <td class="propertyCaption">Initial Year <font color="red">**</font></td>
            <td colspan="2">
                <asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="4"></asp:TextBox>
            </td>
	    </tr>	

        
        <tr id="trp1" runat="server">					            
		<td class="propertyCaption">Service Media <font color="red">**</font></td>
		<td colspan="2">
                <asp:DropDownList ID="ddl_MED" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class="propertyCaption">
                Norminal Thickness <font color="red">**</font></td>
		<td>
                <asp:TextBox ID="txt_Norminal_Thickness" runat="server" MaxLength="10" ></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">mm</td>
	    </tr>	
        <tr id="trp2" runat="server">					            
		<td class="propertyCaption">Material <font color="red">**</font></td>
		<td colspan="2">
                <asp:DropDownList ID="ddl_MAT" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class="propertyCaption">
                Calculated Thickness <font color="red">**</font></td>
		<td>
                <asp:TextBox ID="txt_Calculated_Thickness" runat="server" MaxLength="10"></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">mm</td>
	    </tr>	
        <tr id="trp3" runat="server">						            
		<td class="propertyCaption">Insulation <font color="red">**</font></td>
		<td colspan="2">
                <asp:DropDownList ID="ddl_INSULATION" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class="propertyCaption">
                Insulation Thickness <font color="red">**</font></td>
		<td>
                <asp:TextBox ID="txt_Insulation_Thickness" runat="server" MaxLength="3" AutoPostBack="True"></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">mm</td>
	    </tr>	
        <tr id="trp4" runat="server">					            
		<td class="propertyCaption">Pressure Code <font color="red">**</font></td>
		<td colspan="2">
                <asp:DropDownList ID="ddl_PRESSURE" runat="server" CssClass="select" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class="propertyCaption">
                Corrosion Allowance <font color="red">**</font></td>
		<td>
                <asp:TextBox ID="txt_Corrosion_Allowance" runat="server" MaxLength="5" AutoPostBack="True"></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">mm</td>
	    </tr>	
        <tr id="trp5" runat="server">						            
		<td class="propertyCaption">Size <font color="red">**</font></td>
		<td>
                <asp:TextBox ID="txt_Size" runat="server" MaxLength="5" AutoPostBack="True"></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">inch</td>
            <td class="propertyCaption">
                Design Pressure</td>
		<td>
                <asp:TextBox ID="txt_Pressure_Design" runat="server" MaxLength="10" Style="text-align:left;"></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">Bar.g</td>
	    </tr>	
        <tr id="trp6" runat="server">						            
		<td class="propertyCaption">Schedule</td>
		<td colspan="2">
                <asp:TextBox ID="txt_Schedule" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td class="propertyCaption">
                Operating Pressure</td>
		<td>
                <asp:TextBox ID="txt_Pressure_Operating" runat="server" MaxLength="10" ></asp:TextBox>
            </td>
		<td style="text-align:center;" class="propertyUnit">Bar.g</td>
	    </tr>	
        <tr id="trp7" runat="server">						            
		<td class="propertyCaption">From Location </td>
		<td colspan="2">
                <asp:TextBox ID="txt_Location_From" runat="server" MaxLength="100" Style="text-align:left;"></asp:TextBox>
            </td>
            <td class="propertyCaption">
                Design Temperature</td>
		<td>
                <asp:TextBox ID="txt_Temperature_Design" runat="server" MaxLength="10"></asp:TextBox>
            </td>
			<td style="text-align:center;" class="propertyUnit">° C</td>
	    </tr>	
        <tr id="trp8" runat="server">					            
		<td class="propertyCaption">To Location</td>
		<td colspan="2">
                <asp:TextBox ID="txt_Location_To" runat="server" MaxLength="100" Style="text-align:left;"></asp:TextBox>
            </td>
            <td class="propertyCaption">
                Operating Temperature</td>
		<td>
                <asp:TextBox ID="txt_Temperature_Operating" runat="server" MaxLength="10" ></asp:TextBox>
            </td>
			<td style="text-align:center;" class="propertyUnit">° C</td>
	    </tr>	
        <tr id="trp9" runat="server">	
            <td class="propertyGroup propertyCaption" style="font-weight:bold;" rowspan="2">Class <font color="red">**</font></td>
            <td>
                <asp:RadioButton ID="rdo_Class_Auto" runat="server" GroupName="AM" AutoPostBack="True" /> Auto Generate </td>
            <td style="text-align:center;">
                : </td>
            <td colspan="3">
                <asp:Label ID="lbl_Class" runat="server" Font-Bold="true"></asp:Label></td>
        </tr>
        <tr id="trp10" runat="server">	
            <td>
                <asp:RadioButton ID="rdo_Class_Manual" runat="server" GroupName="AM" AutoPostBack="True" />
                Manual Assign</td>
            <td style="text-align:center;">
                :</td>
            <td colspan="3">
                <asp:TextBox ID="txt_Class" runat="server" MaxLength="50" Width="100%" Font-Bold="true"  style="text-align-last:left;" AutoPostBack="True"></asp:TextBox>
            </td>
        </tr>
        
        <tr id="trp11" runat="server">	
            <td class="propertyGroup propertyCaption" style="font-weight:bold;">CAD / Drawing / P &amp; ID No</td>
            <td colspan="5"><asp:TextBox ID="txt_P_ID_No" runat="server" MaxLength="50" style="text-align-last:left; padding-left:15px; width:100%;"></asp:TextBox></td>
        </tr>
        </asp:Panel>
        </table>