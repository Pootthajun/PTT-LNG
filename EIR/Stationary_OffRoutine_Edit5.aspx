﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Stationary_OffRoutine_Edit5.aspx.vb" Inherits="EIR.Stationary_OffRoutine_Edit5" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="GL_DialogInputValue.ascx" tagname="GL_DialogInputValue" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<!-- Page Head -->
    <asp:UpdatePanel ID="UDPMain" runat="Server">
	    <ContentTemplate>
	    
	    
			<h2>Edit Stationary Off-Routine Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" >Tag Status</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabPhoto" runat="server">Photography Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabDocument" runat="server">Reference Document</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server" CssClass="default-tab current">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">

  
				  <div class="tab-content current">
				  
				  		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
										
										<p style="font-weight:bold;">
								        <label class="column-left" style="width:120px;" >Report for: </label>
								        <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								        <asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								            | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								            | Tag <asp:Label ID="lbl_TAG" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								        </p>
										
							<ul class="shortcut-buttons-set">
							     
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  <li>
								    <asp:LinkButton ID="lnkSave" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/save_48.png" alt="icon" width="48" height="48" /><br />
								         Save changed
								        </span>
								    </asp:LinkButton>
								  </li>
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>

								<div class="clear"></div><!-- End .clear -->
                                <%--<p>&nbsp;</p>
                                <table>
                                    <thead>
                                         <tr>
                                          <th colspan="5" style="height:24px;">Problem Summary</th>
                                        </tr>
                                        <tr>
                                          <th>Tag.No.</th>
                                          <th>Problems</th>
                                          <th>Class</th>
                                          <th>Comment</th>
                                          <th>Resp.</th>
                                         </tr>
                                    </thead>
                                    <asp:Repeater ID="rptSummary" runat="server">
                                    <ItemTemplate>
                                        <tr id="trRow" runat="server">
                                              <td style="width:120px; vertical-align:top;"><asp:label ID="lblTag" runat="server"></asp:label></td>
                                              <td style="width:200px; vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label ID="lblProblem" runat="server"></asp:label></td>
                                              <td style="width:30px; vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label Width="40px" ID="lblClass" runat="server"></asp:label></td>
                                              <td style="width:250px; vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label ID="lblComment" Width="250px" BorderStyle="None" runat="server"></asp:label></td>
                                              <td style="width:40px; vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:TextBox Width="40px" ID="txtResponse" MaxLength="50" runat="server"></asp:TextBox></td>
                                         </tr>
                                    </ItemTemplate>
                                    </asp:Repeater>                                     
                                </table>--%>
								<p>&nbsp;</p>
								
															
								<p>
									<label style="width:300px;" >Conclusion and Recommendation: </label>
									<asp:TextBox  TextMode="MultiLine" Width="800px" Height="80px" CssClass="text-input" ID="txt_RPT_Result" MaxLength="1000" runat="server"></asp:TextBox>
								</p>
								
								<asp:Panel ID="pnl_Collector" runat="server">
								    <p>
									    <label class="column-left" style="width:120px;" >Colleted date: </label>
									    <asp:TextBox CssClass="text-input small-input" ID="txt_RPT_COL_Date" ReadOnly="true" runat="server"></asp:TextBox>
								    </p>
    								
								    <p>
									    <label class="column-left" style="width:120px;" >Colleted by: </label>
									    <asp:TextBox CssClass="text-input small-input" ID="txt_RPT_COL_By" ReadOnly="true" runat="server"></asp:TextBox>
								    </p>
    								
								    <p>
									    <label style="width:300px;" >Collector Recommendation: </label>
									    <asp:TextBox  TextMode="MultiLine" Width="800px" Height="80px" 
                                            CssClass="text-input" ID="txt_RPT_COL_Comment" MaxLength="500" runat="server"></asp:TextBox>
    									
								    </p>
								</asp:Panel>
								
								<asp:Panel ID="pnl_Inspector" runat="server">
								    <p>
									    <label class="column-left" style="width:120px;" >Inspected date: </label>
									    <asp:TextBox CssClass="text-input small-input" ID="txt_RPT_INSP_Date" ReadOnly="true" runat="server"></asp:TextBox>
								    </p>
    								
								    <p>
									    <label class="column-left" style="width:120px;" >Inspector: </label>
									    <asp:TextBox CssClass="text-input small-input" ID="txt_RPT_INSP_By" ReadOnly="true" runat="server"></asp:TextBox>
								    </p>
								    <p>
									    <label style="width:300px;" >Inspector Recommendation: </label>
									    <asp:TextBox  TextMode="MultiLine" Width="800px" Height="80px" CssClass="text-input" ID="txt_RPT_INSP_Comment" MaxLength="500" runat="server"></asp:TextBox>
    									
								    </p>
								</asp:Panel>
								
								<asp:Panel ID="pnl_Analyst" runat="server">
								    <p>
									    <label class="column-left" style="width:120px;" >Approved date: </label>
									    <asp:TextBox CssClass="text-input small-input" ID="txt_RPT_ANL_Date" ReadOnly="true" runat="server"></asp:TextBox>
								    </p>
    								
								    <p>
									    <label class="column-left" style="width:120px;" >Approver: </label>
									    <asp:TextBox CssClass="text-input small-input" ID="txt_RPT_ANL_By" ReadOnly="true" runat="server"></asp:TextBox>
								    </p>
								    <p>
									    <label style="width:300px;" >Approver Recommendation: </label>
									    <asp:TextBox  TextMode="MultiLine" Width="800px" Height="80px" CssClass="text-input" ID="txt_RPT_ANL_Comment" MaxLength="500" runat="server"></asp:TextBox>
    									
								    </p>
								</asp:Panel>					
								
								    <div class="clear">
                                        
                            </div><!-- End .clear -->
									
									<p align="right">
										<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
										<asp:Button ID="btn_Send_Collector" runat="server" CssClass="button" Text="Send to collector" />
										<asp:Button ID="btn_Send_Inspector" runat="server" CssClass="button" Text="Send to inspector" />
										<asp:Button ID="btn_Send_Analyst" runat="server" CssClass="button" Text="send to approver" />
										<asp:Button ID="btn_Posted" runat="server" CssClass="button" Text="Post approved" />
									</p>
									
                    
							</fieldset>
		            </div>
		          				   
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
                
                <uc1:GL_DialogInputValue ID="DialogInput" runat="server" Visible="false" />
                </div> <!-- End .content-box-content -->
			</ContentTemplate>
	    </asp:UpdatePanel>
			  
				
			</div> <!-- End .content-box -->
			
			
			
</asp:Content>
