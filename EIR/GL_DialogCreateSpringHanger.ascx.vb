﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_DialogCreateSpringHanger
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Public Event UpdateCompleted(ByRef sender As GL_DialogCreateSpringHanger, ByVal RPT_ID As Integer)
    Public Event CloseCompleted(ByRef sender As GL_DialogCreateSpringHanger)

#Region "Dynamic Property"

    Public Property RPT_Year() As Integer
        Get
            Return ddl_Year.Items(ddl_Year.SelectedIndex).Value
        End Get
        Set(ByVal value As Integer)
            '------------------- Initiate Year -----------------
            ddl_Year.Items.Clear()
            Dim _item As New ListItem("Choose Year...", 0)
            ddl_Year.Items.Add(_item)
            For i As Integer = Now.Year + 542 To Now.Year + 544 '--- Add To Next Year
                Dim Item As New ListItem(i, i)
                ddl_Year.Items.Add(Item)
                If i = value Then Item.Selected = True
            Next
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            Return Me.Attributes("RPT_No")
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property RPT_ID() As Integer
        Get
            Return Me.Attributes("RPT_ID")
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_ID") = value
        End Set
    End Property

    Public Property PLANT_ID() As Integer
        Get
            If ddl_Plant.SelectedIndex >= 0 Then
                Return ddl_Plant.Items(ddl_Plant.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BL.BindDDlPlant(ddl_Plant, value)
            ROUTE_ID = ROUTE_ID
        End Set
    End Property

    Public Property ROUTE_ID() As Integer
        Get
            If ddl_Route.SelectedIndex >= 0 Then
                Return ddl_Route.Items(ddl_Route.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BL.BindDDlSpringRoute(ddl_Route, PLANT_ID, value)
        End Set
    End Property

#End Region

#Region "Static Property"
    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return Me.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            Me.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Public Sub CloseDialog()
        Me.Visible = False
        RaiseEvent CloseCompleted(Me)
    End Sub

    Public Sub ShowDialog(Optional ByVal Init_PLANT_ID As Integer = 0,
                            Optional ByVal Init_ROUTE_ID As Integer = 0,
                            Optional ByVal Init_YEAR As Integer = 0)

        PLANT_ID = Init_PLANT_ID
        ROUTE_ID = Init_ROUTE_ID
        RPT_Year = Init_YEAR
        Me.Visible = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
        '---------------- Add Trigger ----------------
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        '--------------Validate --------------
        If PLANT_ID = 0 Then
            lblValidation.Text = "Please select plant."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ROUTE_ID = 0 Then
            lblValidation.Text = "Please select route."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If RPT_Year = 0 Then
            lblValidation.Text = "Please select Year."
            pnlValidation.Visible = True
            Exit Sub
        End If

        ''--------------Check Previous Incomplete Report ---------
        Dim Sql As String = "SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) ReportNo FROM SPH_RPT_Header WHERE RPT_Year=" & RPT_Year & " AND ROUTE_ID=" & ROUTE_ID
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This report cannot be create<br>" & vbNewLine
            lblValidation.Text &= "<li>Report for this year and area is already created <b>(" & DT.Rows(0).Item("ReportNo") & ")</b></li>"
            pnlValidation.Visible = True
            Exit Sub
        End If

        '----------------- Check Plant And Route contained spring and support -------------------
        Sql = "SELECT SPH_MS_Spring.* " & vbLf
        Sql &= "FROM SPH_MS_Spring " & vbLf
        Sql &= "INNER JOIN SPH_MS_ROUTE ON SPH_MS_ROUTE.ROUTE_ID=SPH_MS_Spring.ROUTE_ID" & vbLf
        Sql &= "INNER JOIN MS_Plant ON SPH_MS_ROUTE.PLANT_ID=MS_Plant.PLANT_ID AND MS_Plant.Active_Status=1" & vbLf
        Sql &= "WHERE SPH_MS_Spring.Active_Status=1 AND SPH_MS_ROUTE.ROUTE_ID=" & ROUTE_ID & vbLf
        DA = New SqlDataAdapter(Sql, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            lblValidation.Text = "This report cannot be create<br>" & vbNewLine
            lblValidation.Text &= "<li>There is no spring hanger or supports in this area.</li>"
            pnlValidation.Visible = True
            Exit Sub
        End If

        '---------- Create Starter Detail ---------

        Sql = "SELECT * FROM SPH_RPT_Header WHERE 1=0 "
        DA = New SqlDataAdapter(Sql, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        RPT_No = BL.GetNewReportNumber(RPT_Year)
        RPT_ID = BL.GetNew_SPH_ReportID

        DR("RPT_ID") = RPT_ID
        DR("RPT_Year") = RPT_Year
        DR("RPT_No") = RPT_No
        DR("ROUTE_ID") = ROUTE_ID

        DR("RPT_Subject") = "รายงานผลการตรวจสอบ SPRING HANGERS AND SPRING SUPPORTS : ภายในพื้นที่ " & ddl_Plant.Items(ddl_Plant.SelectedIndex).Text & " " & ddl_Route.Items(ddl_Route.SelectedIndex).Text
        DR("RPT_To") = "ผจ.ตร."
        DR("RPT_Cause") = "ตามแผนงานการตรวจสอบ และติดตามสภาพของอุปกรณ์ Spring Hangers and Spring Supports " & vbLf
        DR("RPT_Cause") &= "ภายในพื้นที่ " & ddl_Plant.Items(ddl_Plant.SelectedIndex).Text & " " & ddl_Route.Items(ddl_Route.SelectedIndex).Text & " ตามวาระ ประจำปี " & (RPT_Year - 543)
        DR("RPT_Result") = ""
        DR("RPT_STEP") = 1

        '--------------------Set RPT_LOCK_BY---------------
        Select Case USER_LEVEL
            Case EIR_BL.User_Level.Administrator

                DR("RPT_STEP") = EIR_BL.User_Level.Collector
                'DR("RPT_LOCK_BY") = Session("USER_ID")
                DR("RPT_COL_By") = Session("USER_ID")
                DR("RPT_COL_Date") = Now
                'DR("RPT_INSP_By
                'DR("RPT_INSP_Date
                'DR("RPT_INSP_Comment
                'DR("RPT_ANL_By
                'DR("RPT_ANL_Date
                'DR("RPT_ANL_Comment
                'DR("RPT_STEP
                'DR("RPT_LOCK_BY

            Case EIR_BL.User_Level.Collector
                DR("RPT_STEP") = EIR_BL.User_Level.Collector
                DR("RPT_LOCK_BY") = Session("USER_ID")
                DR("RPT_COL_By") = Session("USER_ID")
                DR("RPT_COL_Date") = Now
                'DR("RPT_INSP_By
                'DR("RPT_INSP_Date
                'DR("RPT_ANL_By
                'DR("RPT_ANL_Date
                'DR("RPT_STEP
                'DR("RPT_LOCK_BY
            Case EIR_BL.User_Level.Inspector

                DR("RPT_STEP") = EIR_BL.User_Level.Inspector
                DR("RPT_LOCK_BY") = Session("USER_ID")
                'DR("RPT_COL_By") = Session("USER_ID")
                'DR("RPT_COL_Date") = Now
                DR("RPT_INSP_By") = Session("USER_ID")
                DR("RPT_INSP_Date") = Now
                'DR("RPT_ANL_By")= Session("USER_ID")
                'DR("RPT_ANL_Date")= Now

            Case EIR_BL.User_Level.Approver
                DR("RPT_STEP") = EIR_BL.User_Level.Approver
                DR("RPT_LOCK_BY") = Session("USER_ID")
                'DR("RPT_COL_By") = Session("USER_ID")
                'DR("RPT_COL_Date") = Now
                'DR("RPT_INSP_By") = Session("USER_ID")
                'DR("RPT_INSP_Date") = Now
                DR("RPT_ANL_By") = Session("USER_ID")
                DR("RPT_ANL_Date") = Now

            Case EIR_BL.User_Level.Viewer, EIR_BL.User_Level.PTT_Authenticated
                lblValidation.Text = "You have no authorization to create report."
                pnlValidation.Visible = True
                Exit Sub
        End Select

        DR("RPT_By") = Session("USER_ID")
        DR("RPT_Date") = Now
        DR("Created_By") = Session("USER_ID")
        DR("Created_Time") = Now
        DR("Update_Time") = Now
        DR("Update_By") = Session("USER_ID")

        '------------ Lock Report ---------------
        If Session("USER_ID") <> 0 Then DR.Item("RPT_LOCK_BY") = Session("USER_ID")

        DT.Rows.Add(DR)
        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '---------- Create Report Detail ---------
        BL.Construct_Report_SPH(Session("USER_ID"), RPT_ID)

        Me.Visible = False
        RaiseEvent UpdateCompleted(Me, RPT_ID)

    End Sub

    Protected Sub ddl_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Plant.SelectedIndexChanged
        PLANT_ID = PLANT_ID
    End Sub


End Class