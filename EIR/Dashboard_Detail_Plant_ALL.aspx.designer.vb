﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Dashboard_Detail_Plant_ALL
    
    '''<summary>
    '''UDP1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UDP1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBack As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lblHead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHead As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Chart1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Chart1 As Global.System.Web.UI.DataVisualization.Charting.Chart
    
    '''<summary>
    '''Chart2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Chart2 As Global.System.Web.UI.DataVisualization.Charting.Chart
    
    '''<summary>
    '''Chart3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Chart3 As Global.System.Web.UI.DataVisualization.Charting.Chart
    
    '''<summary>
    '''Chart4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Chart4 As Global.System.Web.UI.DataVisualization.Charting.Chart
    
    '''<summary>
    '''Chart5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Chart5 As Global.System.Web.UI.DataVisualization.Charting.Chart
    
    '''<summary>
    '''lbl1_C control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl1_C As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl1_B control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl1_B As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl1_A control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl1_A As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl2_C control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl2_C As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl2_B control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl2_B As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl2_A control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl2_A As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl3_A control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl3_A As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl4_A control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl4_A As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl5_A control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl5_A As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMonth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMonth As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblYear As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPlantName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPlantName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblReportName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReportName As Global.System.Web.UI.WebControls.Label
End Class
