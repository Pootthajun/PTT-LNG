﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_DialogFilter_Spec.ascx.vb" Inherits="EIR.MS_ST_DialogFilter_Spec" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%--<div class="MaskDialog"></div>--%>
<asp:Panel ID="pnl" runat="server">
<asp:Label ID="lblTag" runat="server" Text="" Visible ="false" ></asp:Label>
</asp:Panel>


    <table cellpadding="0" cellspacing="0" class="propertyTable">
        <tr>
            <td class="propertyGroup" style="border: none;" colspan="6">Description</td>
            <td class="propertyGroup" style="border: none;">Cad / Drawing&nbsp;
        <asp:Button ID="btnUploadFile" runat="server" Text="" Style="display: none;" />
            </td>
            <td class="propertyGroup" style="border: none; text-align: right;">
                <input type="button" id="btnAddFile" tag_id="0" active_status="True" tag_code="" tag_name=""
                    runat="server" class="button" value="+ Add" style="cursor: pointer;" />
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" class="propertyCaption">Plant <font color="red">**</font></td>
            <td style="width: 15%;" colspan="2">
                <asp:DropDownList ID="ddl_Edit_Plant" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>
            </td>
            <td style="width: 15%;" class="propertyCaption">Route <font color="red">**</font></td>
            <td style="width: 15%;" colspan="2">
                <asp:DropDownList ID="ddl_Edit_Route" runat="server" CssClass="select">
                </asp:DropDownList>
            </td>
            <td style="width: 40%;" rowspan="20" colspan="2">
                <asp:Panel ID="DrawingAlbum" runat="Server" CssClass="FileAlbum">
                    <asp:Repeater ID="rptDrawing" runat="server">
                        <ItemTemplate>
                            <div class="item" id="item" runat="server">
                                <div class="thumbnail">
                                    <a id="lnk_File_Dialog" runat="server" target="_blank" title="Drawing">
                                        <asp:Image ID="img_File" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
                                    </a>
                                </div>
                                <div id="pnlEditFile" runat="server" class="toolbar">
                                    <asp:Button ID="btnUpload" runat="server" Text="" Style="display: none;" CommandName="Edit" />
                                    <input type="image" id="btnEdit" runat="server" src="resources/images/icons/edit_white_16.png" style="margin-right: 10px;" />
                                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
                                    <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Tag No <font color="red">**</font></td>
            <td colspan="2">
                <asp:DropDownList ID="ddl_Edit_Area" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddl_Edit_Process" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtTagNo" runat="server" MaxLength="10" Font-Size="10" Style="margin-left: 7px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Tag Name <font color="red">**</font></td>
            <td colspan="5">
                <asp:TextBox ID="txtTagName" runat="server" MaxLength="50" Font-Size="10" Style="text-align: left; margin-left: 7px;"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Equipement <font color="red">**</font></td>
            <td colspan="2" style="border-right-style: none;">
                <asp:DropDownList ID="ddl_Edit_Type" runat="server" CssClass="select">
                </asp:DropDownList>
            </td>

            <td colspan="3" style="border-left-style: none;"></td>

        </tr>
        <tr>
            <td class="propertyGroup" style="border: none;" colspan="6">Specification</td>
        </tr>
     <tr>					            
					        
		<td class="propertyCaption">Initial Year</td>					        
		<td colspan="2"><asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td class="propertyCaption">Pneum Test Pressure</td>
            <td>
                <asp:TextBox ID="txt_PNEUM_TEST_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
	</tr>


        <tr>

            <td class="propertyCaption">Design Pressure</td>
            <td>
                <asp:TextBox ID="txt_DESIGN_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
           <td class="propertyCaption">Operating Pressure</td>
            <td>
                <asp:TextBox ID="txt_OPERATING_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
        </tr>
        <tr>
            <td class="propertyCaption">Design Temperature</td>
            <td>
                <asp:TextBox ID="txt_DESIGN_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td style="text-align: center;">° C</td>
            <td class="propertyCaption">Operating Temperature </td>
            <td>
                <asp:TextBox ID="txt_OPERATING_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td style="text-align: center;">° C</td>
        </tr>
        <tr>
            <td class="propertyCaption">Hydro Test Pressure </td>
            <td>
                <asp:TextBox ID="txt_HYDRO_TEST_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
            <td class="propertyCaption">Corrosion Allowance</td>            
            <td>
                <asp:TextBox ID="txt_CORROSION_ALLOWANCE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">mm</td>
        </tr>
        	<tr>
        <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" Visible ="false"  />
                                                        &nbsp;
                                                        <asp:Button ID="btnUpdate" runat="server" Class="button" 
                                                            Text="Update to Tag"  Visible ="false"  />

	</tr>
    </table>

<asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
<asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>



