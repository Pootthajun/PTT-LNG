﻿
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Public Class RenderImage_ST_TA_Sector
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '---------- Render Temporary Session ------------
        Dim UNIQUE_ID As String = Request.QueryString("UNIQUE_ID")
        Dim TEMP_ID As String = Request.QueryString("TEMP_ID")
        '---------- Render Database Session ------------
        Dim RPT_Year As String = Request.QueryString("RPT_Year")
        Dim RPT_No As String = Request.QueryString("RPT_No")

        Dim Sector_ID As String = Request.QueryString("Sector_ID")



        Dim PIC_ID As String = Request.QueryString("PIC_ID")

        Dim TAG_CLASS As EIR_BL.Tag_Class = Request.QueryString("Class")

        Dim IMAGE As String = Request.QueryString("Image")
        Dim IMG As Byte() = {}
        Try
            If IsNothing(Sector_ID) = False And IsNothing(RPT_Year) = False And IsNothing(RPT_No) = False Then


                Dim ImgPath As String = BL.Picture_Path & "\ST_TA_Template\" & RPT_Year & "\" & RPT_No & "\" & Sector_ID
                If (Convert.ToInt16(IMAGE) > 1) Then
                    ImgPath += "_" & IMAGE
                End If

                If IO.File.Exists(ImgPath) = True Then
                    IMG = IO.File.ReadAllBytes(ImgPath)
                    Session("PREVIEW_IMG_" & UNIQUE_ID & "_" & IMAGE) = IMG
                End If
            ElseIf UNIQUE_ID <> "" Then
                IMG = Session("PREVIEW_IMG_" & UNIQUE_ID & "_" & IMAGE)
            ElseIf TEMP_ID <> "" Then
                IMG = Session("TempImage_" & TEMP_ID & "_" & IMAGE)
            ElseIf PIC_ID <> "" Then

                Select Case TAG_CLASS
                    Case EIR_BL.Tag_Class.Stationary
                        IMG = BL.Get_ST_TA_As_Found_Image_Template(RPT_Year, RPT_No, Sector_ID, IMAGE)

                    Case EIR_BL.Tag_Class.Rotating
                        'IMG = BL.Get_RO_Image(DETAIL_ID, PIC_ID)
                End Select
            End If
            If IMG.Length < 50 Then
                Response.Redirect("resources/images/Sample_40.png", True)
                Exit Sub
            End If
            Response.Clear()
            Response.BinaryWrite(IMG)
        Catch ex As Exception
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End Try
    End Sub
End Class