﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Improvement_Report_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            Dim PLANT_ID As Integer = Request.QueryString("PLANT_ID")
            Dim Month As Integer = Request.QueryString("MM")
            Dim Year As Integer = Request.QueryString("YY")

            Dim Month_F As Integer = Request.QueryString("Month_F")
            Dim Month_T As Integer = Request.QueryString("Month_T")
            Dim Year_F As Integer = Request.QueryString("Year_F")
            Dim Year_T As Integer = Request.QueryString("Year_T")

            If Year > 2000 Then
                Year = Year - 543
            End If
            Dim Eqm As Integer = Request.QueryString("Eqm")

            lblBack.PostBackUrl = "Dashboard_Improvement_Report_Plant.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Eqm

            Dim Sql As String = ""
            Dim PlantName As String = ""
            Sql = "SELECT PLANT_NAME FROM MS_Plant WHERE PLANT_ID =" & PLANT_ID
            Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            PlantName = DT.Rows(0).Item("PLANT_NAME").ToString

            Sql = ""
            Select Case Eqm
                Case EIR_BL.Report_Type.Stationary_Routine_Report
                    Sql &= "select RPT_Year,RPT_No,TAG_ID,TAG_Code,INSP_Name,'Stationary' TAG_TYPE_Name" & vbCrLf
                    Sql &= "from VW_DASHBOARD_ST" & vbCrLf
                    Sql &= "where MONTH(RPT_Period_Start) = " & Month & " and" & vbCrLf
                    Sql &= "YEAR(RPT_Period_Start) = " & Year & " and" & vbCrLf
                    Sql &= "ISSUE = 3 and PLANT_ID =" & PLANT_ID & vbCrLf
                    Sql &= "ORDER BY TAG_Code,INSP_Name" & vbCrLf
                Case EIR_BL.Report_Type.Rotating_Routine_Report
                    Sql &= "select RPT_Year,RPT_No,TAG_ID,TAG_Code,INSP_Name,'Rotating' TAG_TYPE_Name" & vbCrLf
                    Sql &= "from VW_DASHBOARD_RO" & vbCrLf
                    Sql &= "where MONTH(RPT_Period_Start) = " & Month & " and" & vbCrLf
                    Sql &= "YEAR(RPT_Period_Start) = " & Year & " and" & vbCrLf
                    Sql &= "ISSUE = 3 and PLANT_ID =" & PLANT_ID & vbCrLf
                    Sql &= "ORDER BY TAG_Code,INSP_Name" & vbCrLf
            End Select

            DA = New SqlDataAdapter(Sql, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)

            Dim Equipement As String = ""
            Select Case Eqm
                Case EIR_BL.Report_Type.Stationary_Routine_Report
                    Equipement = " Stationary"
                Case EIR_BL.Report_Type.Rotating_Routine_Report
                    Equipement = " Rotating"
            End Select
            lblHead.Text = "Found " & DT.Rows.Count & Equipement & " Equipement Problem(S) Improved Completely <br> At " & PlantName & " on " & Dashboard.FindMonthNameEng(Month) & " " & Year

            rptData.DataSource = DT
            rptData.DataBind()

        End If

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblIns As Label = e.Item.FindControl("lblIns")
        Dim lblEqm As Label = e.Item.FindControl("lblEqm")

        lblTag.Text = e.Item.DataItem("Tag_Code")
        lblIns.Text = e.Item.DataItem("INSP_Name")
        lblEqm.Text = e.Item.DataItem("TAG_TYPE_Name")

        If e.Item.DataItem("TAG_TYPE_Name") = "Stationary" Then
            tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_ST.aspx?TAG_ID=" & e.Item.DataItem("TAG_ID") & "&RPT_YEAR=" & e.Item.DataItem("RPT_YEAR") & "&RPT_NO=" & e.Item.DataItem("RPT_NO") & "','Dialog_Tag_ST_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        Else
            tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_RO.aspx?TAG_ID=" & e.Item.DataItem("TAG_ID") & "&RPT_YEAR=" & e.Item.DataItem("RPT_YEAR") & "&RPT_NO=" & e.Item.DataItem("RPT_NO") & "','Dialog_Tag_RO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
    End Sub

End Class