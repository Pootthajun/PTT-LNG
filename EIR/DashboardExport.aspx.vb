﻿Public Class DashboardExport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim HID As Integer = Val(Request.QueryString("HID"))
        If HID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseWindow", "window.close();", True)
            Exit Sub
        End If

        Dim HTML As String = Session("Dashboard_HTML_" & HID)
        pnlHTML.InnerHtml = HTML
    End Sub

End Class