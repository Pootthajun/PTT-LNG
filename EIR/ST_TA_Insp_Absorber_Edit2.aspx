﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_Insp_Absorber_Edit2.aspx.vb" Inherits="EIR.ST_TA_Insp_Absorber_Edit2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <!-- Page Head -->
    <asp:UpdatePanel ID="udp1" runat="server">
        <ContentTemplate>


            <h2>Create Turnaround & Shutdown Routine Report</h2>
            <asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" />
            <div class="clear"></div>
            <!-- End .clear -->


            <div class="content-box">
                <!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>
                        <asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>


                    <ul class="content-box-tabs">
                        
                        <!-- href must be unique and match the id of target div -->
                        <li>
                            <asp:LinkButton ID="HTabHeader" runat="server">Report Header</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabDetail" runat="server" CssClass="default-tab current">Report Detail</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
                    </ul>

                    <div class="clear"></div>

                </div>
                <!-- End .content-box-header -->

                <div class="content-box-content">

                    <div class="tab-content current">
                        <fieldset>
                            <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->

                            <p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								    | Equipment-Type Name <asp:Label ID="lbl_Equipment" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ></asp:Label>
								</p>

                            <ul class="shortcut-buttons-set">
                                
                                <li>
                                    <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
                                    </asp:LinkButton>
                                    <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender"
                                        runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this report permanently?">
                                    </cc1:ConfirmButtonExtender>
                                </li>
                                <li>
                                    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
									    Reset this tab
									    </span>
                                    </asp:LinkButton>
                                </li>

                                <li>
                                    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon"/><br />
									        Preview report
								        </span>
                                    </asp:LinkButton>
                                </li>
                            </ul>


                                              <asp:Panel ID="pnlListTag" runat="server">
                   <table style=" background-color: White; border-top: None;">
                    <thead>
                      <tr>
                        <th rowspan ="2"><a href="#">Tag-No </a></th>
                        <th rowspan ="2"><a href="#">Tag Name </a></th>
                        <th colspan="6" align="center" style="text-align:center;"><a href="#">Inspection</a></th>
                      </tr>
                      <tr>                        
                        <th style="text-align:center;"><a href="#" >As found</a></th>
                        <th style="text-align:center;"><a href="#" >After clean</a></th>
                        <th style="text-align:center;"><a href="#" >NDE</a></th>
                        <th style="text-align:center;"><a href="#" >Repair</a></th>
                        <th style="text-align:center;"><a href="#" >After Repair</a></th>
                        <th style="text-align:center;"><a href="#" >Final</a></th>
                      </tr>
                    </thead>
                    <%--As found--%>



                    <asp:Repeater ID="rptTag" runat="server">
                          <%-- <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>--%>
                           <ItemTemplate>
                                <tbody>

                                  <tr >
                                    <td><asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                    </td>
                                    
                                    
                                    <td ID="TdAsFound" runat="server" class ="tdStep"     style=" font-size :12px;    color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; ">
                                        <asp:Label ID="lblStatus_1" runat="server"></asp:Label>
                                        <b><h5><asp:LinkButton ID="lnkAsFound" runat="server" CommandName="AddAsFound"   class=" glyphicons no-js user_add  "  style=" "><i></i></asp:LinkButton></h5></b>
                                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="resources/images/icons/user.png" BorderStyle="None" CommandName="AddAsFound" style="display :none ;" />
                                    </td>
                                    <td ID="TdAfterclean" runat="server" class ="tdStep"     style="  font-size :12px;   color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; " onclick="document.getElementById('lnkAfterclean').click();">
                                         <asp:Label ID="lblStatus_2" runat="server"></asp:Label>
                                        <b><h5> <asp:LinkButton ID="lnkAfterclean" runat="server"  CommandName="AddAfterClean"  class=" glyphicons no-js user_add  "  style=" "><i></i></asp:LinkButton></h5></b>
                                        
                                    </td>
                                    <td ID="TdNDE" runat="server"   class ="tdStep"      style="  font-size :12px;  color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; " onclick="document.getElementById('lnkNDE').click();">
                                        <asp:Label ID="lblStatus_3" runat="server"></asp:Label>
                                        <b><h5><asp:LinkButton ID="lnkNDE" runat="server"  CommandName="AddNDE"  class=" glyphicons no-js user_add  " style=" "><i></i></asp:LinkButton></h5></b>
                                        
                                    </td>
                                    <td ID="TdRepair" runat="server"   class ="tdStep"   style="   font-size :12px; color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; " onclick="document.getElementById('lnkRepair').click();">
                                        <asp:Label ID="lblStatus_4" runat="server"></asp:Label>
                                        <b><h5><asp:LinkButton ID="lnkRepair" runat="server"   CommandName="AddRepair"  class=" glyphicons no-js user_add  "  style=" "><i></i></asp:LinkButton></h5></b>
                                        <asp:Image ID="imgWarning" runat="server" style="cursor:help;" ImageUrl="resources/images/icons/alert.gif" ToolTip="Unable to autosave Please completed all require detail !!" />
                                    </td>
                                    <td ID="TdAfterRepair" runat="server"  class ="tdStep"    style="   font-size :12px; color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; " onclick="document.getElementById('lnkAfterRepair').click();">
                                        <asp:Label ID="lblStatus_5" runat="server"></asp:Label>
                                        <b><h5><asp:LinkButton ID="lnkAfterRepair" runat="server"  CommandName="AddAfterRepair"  class=" glyphicons no-js user_add  "  style=" "><i></i></asp:LinkButton></h5></b>
                                        
                                    </td>
                                    <td ID="TdFinal" runat="server"  class ="tdStep"    style="  font-size :12px;  color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; " onclick="document.getElementById('lnkFinal').click();">
                                        <asp:Label ID="lblStatus_6" runat="server"></asp:Label>
                                        <b><h5><asp:LinkButton ID="lnkFinal" runat="server"  CommandName="AddFinal"  class=" glyphicons no-js user_add  "   style=" "><i></i></asp:LinkButton></h5></b>
                                        
                                    </td>
                                  </tr>
                                </tbody>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="6">
                        <div class="bulk-actions align-left">                             
<%--                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>--%>
                        </div>
                            <uc2:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				    </asp:Panel>













                            <br>
                            <p align="right">
                                <asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
                                <asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
                            </p>



                        </fieldset>
                    </div>
                    <!-- End #tabDetail -->

                    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                        <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close"
                            ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                        <div>
                            <asp:Label ID="lblValidation" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>


                </div>
                <!-- End .content-box-content -->

            </div>
            <!-- End .content-box -->
            <script >
                function StepClick(btn) {
                    document.getElementById(btn).click();
                    return true;
                }

            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolderHead">
    <link href="resources/css/StyleTurnaround.css" rel="stylesheet" type="text/css" />

</asp:Content>


