﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_MS_Tag_UTM.aspx.vb" Inherits="EIR.ST_TA_MS_Tag_UTM" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>

	<!-- Page Head -->
			<h2>UTM Template Setting </h2>
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
				
				    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_Tag_Type" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
    				
    				<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Area" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
				            <div class="clear"></div>
                  </div>
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListTag" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Tag-No </a></th>
                        <th><a href="#">Tag Name </a></th>
                        <th><a href="#">Equipement-Type</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Updated</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptTag" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                        <asp:Label ID="lblTo_Table" runat="server" Visible="false"></asp:Label>
                                    </td>
                                    <td><asp:Label ID="lblTagType" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                          <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                          
                                    </td>
                                  </tr>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="6">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				    </asp:Panel>
				    
				 <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
			    
			    <asp:Panel ID="pnlEdit" runat="server" >
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Tag : <asp:Label ID="lblTag_Edit" runat="server" ></asp:Label> </h3>
                    
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                   
                    <%--ตาราง Setting Template--%>

                                          <table>
                    <thead>
                      <tr>
                        <th><a href="#">Location Name </a></th>
                          <th><a href="#">Point Code</a></th>
                          <th><a href="#">Point Amount</a></th>

                        <%--<th><a href="#">X</a></th>--%>
                        <th><a href="#">Y</a></th>
                        <%--<th><a href="#">Action</a></th>--%>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptTemplate" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td class="propertyCaption" ><asp:TextBox ID="txtLocation_Name" runat="server" ></asp:TextBox></td>
                                    <%--<td class="propertyCaption" ><asp:TextBox ID="txtX_Start" runat="server" Width ="30px" ></asp:TextBox><span> - </span><asp:TextBox ID="txtX_End" runat="server" Width ="30px"  ></asp:TextBox></td>--%>
                                    <td class="propertyCaption" ><asp:TextBox ID="txtPoint_Code" runat="server" Width ="50px" ></asp:TextBox></td>
                                      <td class="propertyCaption" ><asp:TextBox ID="txtPoint_number" runat="server" Width ="50px" ></asp:TextBox></td>
                                    
                                      <td class="propertyCaption" ><asp:DropDownList ID="ddlY" runat="server"  Width ="100px" >
                                        <asp:ListItem Value ="0" Text ="Select" ></asp:ListItem>
                                        <asp:ListItem Value ="1" Text ="A-D" ></asp:ListItem>
                                        <asp:ListItem Value ="2" Text ="A-H" ></asp:ListItem>
                                        </asp:DropDownList></td>
                                    <%--<td>
                                          <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                    </td>--%>
                                  </tr>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="3">
                        <td class="propertyGroup" style="border:none; text-align:right;">                            
                            <asp:LinkButton ID="btnAddLocation" runat="server" CssClass="button" Text="+ Add"></asp:LinkButton>
                        </div>
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>

                    </tfoot>
                  </table>









				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnCreate_Template" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset>

                






			    </asp:Panel>



                </div>
                <!-- End #tab1 -->
					
				
			  </div> <!-- End .content-box-content -->
				
		  </div>
	
		  
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>

