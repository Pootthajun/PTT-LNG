﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class UC_ST_TA_Template8
    
    '''<summary>
    '''lblProperty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProperty As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ImgPreview1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgPreview1 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''btnRefreshImage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRefreshImage As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ImgPreview2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgPreview2 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''trimage_1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trimage_1 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''lnk_File_Dialog1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnk_File_Dialog1 As Global.System.Web.UI.HtmlControls.HtmlAnchor
    
    '''<summary>
    '''img_File1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents img_File1 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''lnk_File_Dialog2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnk_File_Dialog2 As Global.System.Web.UI.HtmlControls.HtmlAnchor
    
    '''<summary>
    '''img_File2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents img_File2 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''trbtn_1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trbtn_1 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''btnUpload1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpload1 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnEdit1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEdit1 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnDelete1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete1 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnDelete1_Confirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete1_Confirm As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''btnUpload2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpload2 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnEdit2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEdit2 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnDelete2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete2 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnDelete2_Confirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete2_Confirm As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''txtDetail_Pic1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDetail_Pic1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtDetail_Pic2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDetail_Pic2 As Global.System.Web.UI.WebControls.TextBox
End Class
