﻿Imports System.Data.SqlClient
Imports System.Data
Public Class EIR_Business_Consequence

    Dim BL As New EIR_BL

    Public Enum BusinessConsLevel
        ClassC = 1
        ClassB1 = 2
        ClassB2 = 3
        ClassA = 4
    End Enum

    Public Function Get_Biz_Level_Name(ByVal LEVEL As BusinessConsLevel) As String
        Select Case LEVEL
            Case BusinessConsLevel.ClassC
                Return "ClassC"
            Case BusinessConsLevel.ClassB1
                Return "ClassB"
            Case BusinessConsLevel.ClassB2
                Return "ClassB"
            Case BusinessConsLevel.ClassA
                Return "ClassA"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_Biz_Css_Box_By_Level(ByVal Level As BusinessConsLevel) As String
        Select Case Level
            Case BusinessConsLevel.ClassC
                Return "LevelClassC"
            Case BusinessConsLevel.ClassB1
                Return "LevelClassB"
            Case BusinessConsLevel.ClassB2
                Return "LevelClassB"
            Case BusinessConsLevel.ClassA
                Return "LevelClassA"
            Case Else
                Return "LevelDeselect"
        End Select
    End Function

    Public Function Get_Biz_Css_Text_By_Level(ByVal Level As BusinessConsLevel) As String
        Select Case Level
            Case BusinessConsLevel.ClassC
                Return "TextClassC"
            Case BusinessConsLevel.ClassB1
                Return "TextClassB"
            Case BusinessConsLevel.ClassB2
                Return "TextClassB"
            Case BusinessConsLevel.ClassA
                Return "TextClassA"
            Case Else
                Return "LevelDeselect"
        End Select
    End Function

    Public Function Get_MA_Day_Css_Text_By_Day(ByVal MA_Day As Integer) As String
        Select Case MA_Day
            Case 2, 7, 30, 45, 180, 240, 360
                Return "TextMA_" & MA_Day
            Case Else
                Return "LevelDeselect"
        End Select
    End Function

    Public Function Get_MA_Day_Css_Box_By_Day(ByVal MA_Day As Integer) As String
        Select Case MA_Day
            Case 2, 7, 30, 45, 180, 240, 360
                Return "BoxMA_" & MA_Day
            Case Else
                Return "LevelDeselect"
        End Select
    End Function

    Public Sub BindDDl_BizLevel(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_Business_Consequence ORDER BY BIZ_LEVEL"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("LEVEL_Name"), DT.Rows(i).Item("BIZ_LEVEL"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_MA_ActionDay(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_MA_Action_Day ORDER BY MA_Day"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("MA_Day") & " d", DT.Rows(i).Item("MA_Day"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

End Class
