﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Master_Utility.aspx.vb" Inherits="EIR.Master_Utility" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>EIR System Maintenance Utilities</h2>
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> 
								<ul class="shortcut-buttons-set">
							      <li>
							      <asp:LinkButton ID="btnDeleteTemp" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/utility/Delete.png" width="100" alt="icon" /><br />
							            Clean Temporary Files
							            <cc1:ConfirmButtonExtender ID="btnDeleteTemp_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="btnDeleteTemp" ConfirmText="Are you sure to clean temporary files?">
                                        </cc1:ConfirmButtonExtender>
							        </span>
							      </asp:LinkButton>
							      </li>
								  <li>
								    <asp:LinkButton ID="btnBackupDB" runat="server" Visible="false" CssClass="shortcut-button">
								      <span>
									    <img src="resources/images/utility/Downloads.png" width="100" alt="icon" /><br />
									    Backup Current Database
									    <cc1:ConfirmButtonExtender ID="btnBackupDB_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="btnBackupDB" ConfirmText="Backup may take several minutes, Are you sure you want to continue?">
                                        </cc1:ConfirmButtonExtender>
									    </span>
								    </asp:LinkButton>
								  </li>
								 
								  <li>
								    <asp:LinkButton ID="btnShinkDB" runat="server" CssClass="shortcut-button">
								        <span>
									    <img src="resources/images/utility/Utilities.png" width="100" alt="icon" /><br />
									        Compact/Optimize Database
									        <cc1:ConfirmButtonExtender ID="btnShirkDB_ConfirmButtonExtender" 
                                             runat="server" Enabled="True" TargetControlID="btnShinkDB" ConfirmText="This operation make system hang-down for several minutes, Are you sure you want to continue?">
                                            </cc1:ConfirmButtonExtender>
								        </span>
								    </asp:LinkButton>
								  </li>
								  
								   <li>
								    <asp:LinkButton ID="btnClearUnusdMaster" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/utility/FullTrash.png" width="100" alt="icon" /><br />
									        Clear Inactive Master Data
									        <cc1:ConfirmButtonExtender ID="btnClearUnusdMaster_ConfirmButtonExtender" 
                                             runat="server" Enabled="True" TargetControlID="btnClearUnusdMaster" ConfirmText="This operation will clean-up all disabled master data\nWe prefer you to backup database first.Are you sure you want to continue?">
                                            </cc1:ConfirmButtonExtender>
								        </span>
								    </asp:LinkButton>
								    
								  </li>
								  
					        </ul>
								
						</fieldset>
				    </div>
				  <!-- End #tabDetail -->        
		          
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			

</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>