﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class Dashboard_Current_Status_Plant
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CV As New Converter
    Dim Dashboard As New DashboardClass


    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("PLANT_ID")) Then
                PLANT_ID = Request.QueryString("PLANT_ID")
            End If
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable
        Dim SQL As String = ""
        SQL &= "DECLARE @PLANT_ID INT=" & PLANT_ID & vbNewLine
        SQL &= "SELECT *,Normal + ClassC + ClassB + ClassA Total FROM" & vbNewLine
        SQL &= "(" & vbNewLine
        '----------------------- Stationary ------------------------ 
        SQL &= "SELECT 'Stationary' Type,PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= "	 SELECT DISTINCT MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_ST_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
        SQL &= "	 FROM MS_Plant" & vbLf
        SQL &= "	 LEFT JOIN MS_ST_Route ON MS_Plant.PLANT_ID=MS_ST_Route.PLANT_ID AND MS_ST_Route.Active_Status=1" & vbLf
        SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
        SQL &= "	 INNER JOIN MS_ST_TAG ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID AND MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID AND MS_ST_TAG.Active_Status=1" & vbLf
        SQL &= "     LEFT JOIN (" & vbNewLine
        SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
        SQL &= "        FROM" & vbNewLine
        SQL &= "        (" & vbNewLine
        SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM" & vbNewLine
        SQL &= "            FROM" & vbNewLine
        SQL &= "            (" & vbNewLine
        SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
        SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
        SQL &= "                FROM" & vbNewLine
        SQL &= "     		    (" & vbNewLine
        SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_ST_Detail.TAG_TYPE_ID,RPT_ST_Detail.INSP_ID," & vbNewLine
        SQL &= "     				ICLS_ID CUR_LEVEL" & vbNewLine
        SQL &= "                    FROM RPT_ST_Detail" & vbNewLine
        SQL &= "     		    ) TB1" & vbNewLine
        SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
        SQL &= "     	    ) TB2" & vbNewLine
        SQL &= "        ) TB3" & vbNewLine
        SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "     ) VW ON MS_ST_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
        SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " WHERE PLANT_ID = @PLANT_ID" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= "UNION ALL" & vbNewLine
        '----------------------- Rotating ------------------------
        SQL &= "SELECT 'Rotating' Type,PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= "	 SELECT DISTINCT MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_RO_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
        SQL &= "	 FROM MS_Plant" & vbLf
        SQL &= "	 LEFT JOIN MS_RO_Route ON MS_Plant.PLANT_ID=MS_RO_Route.PLANT_ID AND MS_RO_Route.Active_Status=1" & vbLf
        SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
        SQL &= "	 INNER JOIN MS_RO_TAG ON MS_RO_TAG.AREA_ID=MS_Area.AREA_ID AND MS_RO_TAG.ROUTE_ID=MS_RO_Route.ROUTE_ID AND MS_RO_TAG.Active_Status=1" & vbLf
        SQL &= "     LEFT JOIN (" & vbNewLine
        SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
        SQL &= "        FROM" & vbNewLine
        SQL &= "        (" & vbNewLine
        SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM" & vbNewLine
        SQL &= "            FROM" & vbNewLine
        SQL &= "            (" & vbNewLine
        SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
        SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
        SQL &= "                FROM" & vbNewLine
        SQL &= "     		    (" & vbNewLine
        SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_RO_Detail.TAG_TYPE_ID,RPT_RO_Detail.INSP_ID," & vbNewLine
        SQL &= "     				CASE WHEN RPT_RO_Detail.INSP_ID = 12 THEN" & vbNewLine
        SQL &= "     					CASE ICLS_ID WHEN 3 THEN 3 WHEN 2 THEN 1 WHEN 1 THEN 0 WHEN 0 THEN 0 ELSE ICLS_ID END" & vbNewLine
        SQL &= "     				ELSE" & vbNewLine
        SQL &= "                        ICLS_ID" & vbNewLine
        SQL &= "     				END CUR_LEVEL" & vbNewLine
        SQL &= "                    FROM RPT_RO_Detail" & vbNewLine
        SQL &= "     		    ) TB1" & vbNewLine
        SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
        SQL &= "     	    ) TB2" & vbNewLine
        SQL &= "        ) TB3" & vbNewLine
        SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "     ) VW ON MS_RO_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
        SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " WHERE PLANT_ID = @PLANT_ID" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= "UNION ALL" & vbNewLine
        '----------------------- Lube Oil ------------------------
        SQL &= "SELECT 'Lube Oil' Type,PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
        SQL &= "FROM" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT *," & vbNewLine
        SQL &= "	CASE WHEN	TAN_Value>=1.5 OR " & vbNewLine
        SQL &= "				(Oil_Cat=1 AND Ox_Value>=30) OR  ---------- Oxidation Abnormal----------" & vbNewLine
        SQL &= "				(Oil_Cat=2 AND Ox_Value<=30) OR " & vbNewLine
        SQL &= "				VANISH_Value>30 OR ---------- Varnish Abnormal----------" & vbNewLine
        SQL &= "				(Oil_Cat=1 AND WATER_Value>=750) OR" & vbNewLine
        SQL &= "				(Oil_Cat=2 AND WATER_Value>=1500)" & vbNewLine
        SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
        SQL &= "" & vbNewLine
        SQL &= "	 FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG.LO_TAG_ID TAG_ID,TAG.PLANT_ID,PLANT_Code,PLANT_Name,Oil_Cat,TAN_Value,OX_Value,VANISH_Value,WATER_Value" & vbNewLine
        SQL &= "		FROM MS_LO_TAG TAG" & vbNewLine
        SQL &= "		LEFT JOIN MS_LO_Oil_Type ON TAG.Oil_TYPE_ID = MS_LO_Oil_Type.Oil_TYPE_ID" & vbNewLine
        SQL &= "		LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,TAN_Value " & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,TAN_Value " & vbNewLine
        SQL &= "			FROM RPT_LO_Detail " & vbNewLine
        SQL &= "			) TB1" & vbNewLine
        SQL &= "			WHERE TAN_Value IS NOT NULL" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "		) TAN_Value" & vbNewLine
        SQL &= "		ON TAG.LO_TAG_ID = TAN_Value.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,OX_Value " & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,OX_Value " & vbNewLine
        SQL &= "			FROM RPT_LO_Detail " & vbNewLine
        SQL &= "			) TB1" & vbNewLine
        SQL &= "			WHERE OX_Value IS NOT NULL" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "		) OX_Value" & vbNewLine
        SQL &= "		ON TAG.LO_TAG_ID = OX_Value.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,VANISH_Value " & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,VANISH_Value " & vbNewLine
        SQL &= "			FROM RPT_LO_Detail " & vbNewLine
        SQL &= "			) TB1" & vbNewLine
        SQL &= "			WHERE VANISH_Value IS NOT NULL" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "		) VANISH_Value" & vbNewLine
        SQL &= "		ON TAG.LO_TAG_ID = VANISH_Value.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,WATER_Value " & vbNewLine
        SQL &= "		FROM" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,WATER_Value " & vbNewLine
        SQL &= "			FROM RPT_LO_Detail " & vbNewLine
        SQL &= "			) TB1" & vbNewLine
        SQL &= "			WHERE WATER_Value IS NOT NULL" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "		) WATER_Value" & vbNewLine
        SQL &= "		ON TAG.LO_TAG_ID = WATER_Value.TAG_ID" & vbNewLine
        SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
        SQL &= "	) ALL_TAG" & vbNewLine
        SQL &= ") TAG_FILTER" & vbNewLine
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " WHERE PLANT_ID = @PLANT_ID" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= "UNION ALL" & vbNewLine
        '----------------------- PdMA ------------------------
        SQL &= "SELECT 'PdMA & MTap' Type,PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
        SQL &= "FROM" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT *," & vbNewLine
        SQL &= "	CASE WHEN	PowerQuality = 1 OR Insulation = 1 OR PowerCircuit = 1 OR" & vbNewLine
        SQL &= "			Stator = 1 OR Rotor = 1 OR AirGap = 1" & vbNewLine
        SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
        SQL &= "" & vbNewLine
        SQL &= "	 FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG.TAG_ID,MS_PDMA_Route.PLANT_ID,PLANT_Code,PLANT_Name," & vbNewLine
        SQL &= "		CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END PowerQuality," & vbNewLine
        SQL &= "		CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Insulation," & vbNewLine
        SQL &= "		CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END PowerCircuit," & vbNewLine
        SQL &= "		CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Stator," & vbNewLine
        SQL &= "		CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rotor," & vbNewLine
        SQL &= "		CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END AirGap" & vbNewLine
        SQL &= "		FROM MS_PDMA_TAG TAG " & vbNewLine
        SQL &= "		LEFT JOIN MS_PDMA_Route ON TAG.ROUTE_ID = MS_PDMA_Route.ROUTE_ID" & vbNewLine
        SQL &= "		LEFT JOIN MS_Plant ON MS_PDMA_Route.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
        SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= "		) PWQ" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
        SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
        SQL &= "		) INS" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,PowerCircuit" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
        SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
        SQL &= "		) PWC" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,Stator" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
        SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
        SQL &= "		) STA" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
        SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= "		) ROT" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
        SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= "		) AIR" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
        SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
        SQL &= "	) ALL_TAG" & vbNewLine
        SQL &= ") TAG_FILTER" & vbNewLine
        SQL &= "UNION ALL" & vbNewLine
        '----------------------- MTap ------------------------
        SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
        SQL &= "FROM" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT *," & vbNewLine
        SQL &= "	CASE WHEN	PowerQuality = 1 OR Insulation = 1 OR PowerCircuit = 1 OR" & vbNewLine
        SQL &= "			Stator = 1 OR Rotor = 1 OR AirGap = 1" & vbNewLine
        SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
        SQL &= "" & vbNewLine
        SQL &= "	 FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG.TAG_ID,TAG.PLANT_ID,PLANT_Code,PLANT_Name," & vbNewLine
        SQL &= "		CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END PowerQuality," & vbNewLine
        SQL &= "		CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Insulation," & vbNewLine
        SQL &= "		CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END PowerCircuit," & vbNewLine
        SQL &= "		CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Stator," & vbNewLine
        SQL &= "		CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rotor," & vbNewLine
        SQL &= "		CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END AirGap" & vbNewLine
        SQL &= "		FROM MS_MTAP_TAG TAG " & vbNewLine
        SQL &= "		LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
        SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= "		) PWQ" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
        SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
        SQL &= "		) INS" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,PowerCircuit" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
        SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
        SQL &= "		) PWC" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,Stator" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
        SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
        SQL &= "		) STA" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
        SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= "		) ROT" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
        SQL &= "		LEFT JOIN" & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
        SQL &= "			FROM" & vbNewLine
        SQL &= "			(" & vbNewLine
        SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "				(" & vbNewLine
        SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
        SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "				) TB1" & vbNewLine
        SQL &= "			) TB2" & vbNewLine
        SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= "		) AIR" & vbNewLine
        SQL &= "		ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
        SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
        SQL &= "	) ALL_TAG" & vbNewLine
        SQL &= ") TAG_FILTER" & vbNewLine
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " WHERE PLANT_ID = @PLANT_ID" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= "UNION ALL" & vbNewLine
        '----------------------- Thermography ------------------------
        SQL &= "SELECT 'Thermography' Type,PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= "SELECT TAG.PLANT_ID,PLANT_Code,PLANT_Name,TAG.TAG_ID," & vbNewLine
        SQL &= "CASE TAG_STATUS WHEN 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
        SQL &= "FROM MS_THM_Tag TAG" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "    SELECT RPT_Year,RPT_No,TAG_ID,TAG_STATUS" & vbNewLine
        SQL &= "    FROM" & vbNewLine
        SQL &= "    (" & vbNewLine
        SQL &= " 	SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
        SQL &= "        FROM" & vbNewLine
        SQL &= "        (" & vbNewLine
        SQL &= "            SELECT TAG_ID,RPT_Year,RPT_No,TAG_STATUS," & vbNewLine
        SQL &= " 	    CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
        SQL &= "            FROM" & vbNewLine
        SQL &= "	    (" & vbNewLine
        SQL &= " 		SELECT RPT_Year,RPT_No,RPT_THM_Detail.TAG_ID,TAG_STATUS" & vbNewLine
        SQL &= "                FROM RPT_THM_Detail INNER JOIN MS_THM_Tag ON RPT_THM_Detail.TAG_ID = MS_THM_Tag.TAG_ID AND MS_THM_Tag.Active_Status = 1" & vbNewLine
        SQL &= " 	    ) TB1" & vbNewLine
        SQL &= " 	    GROUP BY TAG_ID,RPT_Year,RPT_No,TAG_STATUS" & vbNewLine
        SQL &= " 	 ) TB2" & vbNewLine
        SQL &= "    ) TB3" & vbNewLine
        SQL &= "    WHERE ROW_NUM = 1" & vbNewLine
        SQL &= " ) TAG_STATUS" & vbNewLine
        SQL &= " ON TAG.TAG_ID = TAG_STATUS.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN MS_Plant " & vbNewLine
        SQL &= "ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " WHERE PLANT_ID = @PLANT_ID" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= " ) TB " & vbLf
        SQL &= " ORDER BY Total DESC" & vbLf

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        '----------------- Chart Funnel ------------------
        Chart6.Series("Series1").Points.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            Chart6.Series("Series1").Points.AddXY(DT.Rows(i).Item("Type").ToString, DT.Rows(i).Item("Total").ToString)
            Chart6.Series("Series1").Points(i).Label = DT.Rows(i).Item("Total").ToString

            Dim Url As String = ""
            Select Case DT.Rows(i).Item("Type")
                Case "Stationary"
                    Url = "Dashboard_Current_Status_Plant_ST.aspx?PLANT_ID=" & PLANT_ID
                Case "Rotating"
                    Url = "Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & PLANT_ID
                Case "Lube Oil"
                    Url = "Dashboard_Current_Status_AllTag_LO.aspx?PLANT_ID=" & PLANT_ID
                Case "PdMA"
                    Url = "Dashboard_Current_Status_AllTag_PdMA.aspx?PLANT_ID=" & PLANT_ID
                Case "Thermography"
                    Url = "Dashboard_Current_Status_AllTag_THM.aspx?PLANT_ID=" & PLANT_ID
            End Select
            Chart6.Series("Series1").Points(i).Url = Url
            Chart6.Series("Series1").Points(i).ToolTip = DT.Rows(i).Item("Type").ToString & " : " & DT.Rows(i).Item("Total").ToString & " tag(s)"
        Next

        '----------------- Anuan Progress ---------------
        DisplayAnnual()
        '----------------- All Pie Chart ----------------
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim DR As DataRow
            DR = DT.Rows(i)
            DisplayChart(i + 1, DR)
        Next
    End Sub

    Protected Sub DisplayAnnual()
        Dim SQL As String = ""

        Dim DA As New SqlDataAdapter

        Dim culture As New CultureInfo("en-us", False)
        Dim Date_Start As Date = Date.ParseExact((Now.Year + 543) & "0101", "yyyyMMdd", culture)
        Dim Date_End As Date = Date.ParseExact((Now.Year + 543) & "1231", "yyyyMMdd", culture)

        '--------------------- Improvement --------------
        Dim IM_ST As DataTable = Dashboard.ImprovementPlant(PLANT_ID, 1, Now.Month, Now.Year, Now.Year, EIR_BL.Report_Type.Stationary_Routine_Report)
        Dim IM_RO As DataTable = Dashboard.ImprovementPlant(PLANT_ID, 1, Now.Month, Now.Year, Now.Year, EIR_BL.Report_Type.Rotating_Routine_Report)
        IM_ST.Merge(IM_RO)
        Dim IM As DataTable = IM_ST.Copy

        For i As Integer = 1 To 12
            IM.DefaultView.RowFilter = "MM=" & i
            If IM.DefaultView.Count = 0 Then
                Dim DR As DataRow = IM.NewRow
                DR("MM") = i
                DR("Improvement") = 0
                DR("YY") = Now.Year
                IM.Rows.Add(DR)
                IM.AcceptChanges()
            End If
        Next

        ChartAnnual.Series("Series1").Points.Clear()
        ChartAnnual.ChartAreas(0).AxisX.Title = "Year " & (Now.Year + 543)
        lblCurrentYear.Text = (Now.Year + 543)
        Dim FP(12) As Integer
        For i As Integer = 0 To IM.Rows.Count - 1

            Dim MM As String = CV.ToMonthNameEN(IM.Rows(i).Item("MM")).ToString
            If IsDBNull(IM.Rows(i).Item("Improvement")) OrElse IM.Rows(i).Item("Improvement") = 0 Then
                ChartAnnual.Series("Series1").Points.AddXY(MM.Substring(0, 3), 0)
                ChartAnnual.Series("Series1").Points(i).ToolTip = MM & " fixed nothing."
            Else
                ChartAnnual.Series("Series1").Points.AddXY(MM.Substring(0, 3), IM.Rows(i).Item("Improvement"))
                ChartAnnual.Series("Series1").Points(i).ToolTip = MM & " fixed " & IM.Rows(i).Item("Improvement") & " problems"
                ChartAnnual.Series("Series1").Points(i).Label = IM.Rows(i).Item("Improvement")
            End If

            ChartAnnual.Series("Series1").Points(i).Color = Drawing.Color.Green

        Next

        '------------------ New Problem ------------------
        Dim PB_ST As DataTable = Dashboard.NewProblemPlantAllYear(PLANT_ID, Now.Year, EIR_BL.Report_Type.Stationary_Routine_Report)
        Dim PB_RO As DataTable = Dashboard.NewProblemPlantAllYear(PLANT_ID, Now.Year, EIR_BL.Report_Type.Rotating_Routine_Report)
        PB_ST.Merge(PB_RO)

        Dim PB As DataTable = PB_ST.Copy
        Dim NP(12) As Integer
        For i As Integer = 1 To 12

            Dim Filter As String = "MM=" & i
            Dim C As Object = 0
            Dim B As Object = 0
            Dim A As Object = 0
            If PB.DefaultView.Count > 0 Then
                If Not IsDBNull(PB.Compute("SUM(ClassC)", Filter)) Then C = PB.Compute("SUM(ClassC)", Filter)
                If Not IsDBNull(PB.Compute("SUM(ClassB)", Filter)) Then B = PB.Compute("SUM(ClassB)", Filter)
                If Not IsDBNull(PB.Compute("SUM(ClassA)", Filter)) Then A = PB.Compute("SUM(ClassA)", Filter)
            End If

            Dim MM As String = CV.ToMonthNameEN(i).ToString
            ''--------- Add Stacked Class C -------------
            'ChartAnnual.Series("Series2").Points.AddXY(MM.Substring(0, 3), C)
            'ChartAnnual.Series("Series2").Points(i - 1).ToolTip = "Class C " & C & " problem(s)"

            ''--------- Add Stacked Class B -------------
            'ChartAnnual.Series("Series3").Points.AddXY(MM.Substring(0, 3), B)
            'ChartAnnual.Series("Series3").Points(i - 1).ToolTip = "Class B " & B & " problem(s)"

            ''--------- Add Stacked Class A -------------
            'ChartAnnual.Series("Series4").Points.AddXY(MM.Substring(0, 3), A)
            'ChartAnnual.Series("Series4").Points(i - 1).ToolTip = "Class A " & A & " problem(s)"

            NP(i) = A + B + C
            ChartAnnual.Series("Series5").Points.AddXY(MM.Substring(0, 3), NP(i))
            ChartAnnual.Series("Series5").Points(i - 1).ToolTip = "New " & NP(i) & " problem(s) on " & MM


            If NP(i) > 0 And i Mod 2 = 1 Then
                ChartAnnual.Series("Series5").Points(i - 1).Label = NP(i)
            Else
                ChartAnnual.Series("Series5").Points(i - 1).MarkerStyle = DataVisualization.Charting.MarkerStyle.None
            End If

        Next

        '---------------- หาค่าเฉลี่ย แก้ปัญหาตรง รอบ Station ตรวจน้อยกว่า Rotating---------------
        For i As Integer = 1 To 6
            ChartAnnual.Series("Series5").Points((2 * i) - 1).SetValueY(CInt((NP((2 * i) - 1) + NP(2 * i)) / 2))
        Next
    End Sub

    Private Sub DisplayChart(ByVal ChartNo As Integer, ByVal DR As DataRow)

        Dim Normal As Double = DR.Item("Normal")
        Dim ClassC As Double = DR.Item("ClassC")
        Dim ClassB As Double = DR.Item("ClassB")
        Dim ClassA As Double = DR.Item("ClassA")

        Dim Url As String = ""
        Select Case DR.Item("Type")
            Case "Stationary"
                Url = "Dashboard_Current_Status_Plant_ST.aspx?PLANT_ID=" & PLANT_ID

                Dim YValue() As Double = {Normal, ClassC, ClassB, ClassA}
                Dim XValue() As String = {"Normal", "ClassC", "ClassB", "ClassA"}

                Chart1.Series("Series1").Points.DataBindY(YValue)
                Chart1.Titles("Title1").Text = DR.Item("Type").ToString
                Chart1.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
                Chart1.Series("Series1").Points(1).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
                Chart1.Series("Series1").Points(2).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
                Chart1.Series("Series1").Points(3).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("Normal") > 0 Then
                    Chart1.Series("Series1").Points(0).Label = DR.Item("Normal")
                End If
                If DR.Item("ClassC") > 0 Then
                    Chart1.Series("Series1").Points(1).Label = DR.Item("ClassC")
                End If
                If DR.Item("ClassB") > 0 Then
                    Chart1.Series("Series1").Points(2).Label = DR.Item("ClassB")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart1.Series("Series1").Points(3).Label = DR.Item("ClassA")
                End If

                Chart1.Series("Series1").Points(0).Color = Drawing.Color.Green
                Chart1.Series("Series1").Points(1).Color = Drawing.Color.Yellow
                Chart1.Series("Series1").Points(2).Color = Drawing.Color.Orange
                Chart1.Series("Series1").Points(3).Color = Drawing.Color.Red

                Chart1.Series("Series1").Url = Url

                lbl1_N.Text = DR.Item("Normal").ToString
                lbl1_C.Text = DR.Item("ClassC").ToString
                lbl1_B.Text = DR.Item("ClassB").ToString
                lbl1_A.Text = DR.Item("ClassA").ToString
                lbl1_T.Text = DR.Item("Total").ToString
            Case "Rotating"
                Url = "Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & PLANT_ID

                Dim YValue() As Double = {Normal, ClassC, ClassB, ClassA}
                Dim XValue() As String = {"Normal", "ClassC", "ClassB", "ClassA"}

                Chart2.Series("Series1").Points.DataBindY(YValue)
                Chart2.Titles("Title1").Text = DR.Item("Type").ToString
                Chart2.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
                Chart2.Series("Series1").Points(1).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
                Chart2.Series("Series1").Points(2).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
                Chart2.Series("Series1").Points(3).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("Normal") > 0 Then
                    Chart2.Series("Series1").Points(0).Label = DR.Item("Normal")
                End If
                If DR.Item("ClassC") > 0 Then
                    Chart2.Series("Series1").Points(1).Label = DR.Item("ClassC")
                End If
                If DR.Item("ClassB") > 0 Then
                    Chart2.Series("Series1").Points(2).Label = DR.Item("ClassB")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart2.Series("Series1").Points(3).Label = DR.Item("ClassA")
                End If

                Chart2.Series("Series1").Points(0).Color = Drawing.Color.Green
                Chart2.Series("Series1").Points(1).Color = Drawing.Color.Yellow
                Chart2.Series("Series1").Points(2).Color = Drawing.Color.Orange
                Chart2.Series("Series1").Points(3).Color = Drawing.Color.Red

                Chart2.Series("Series1").Url = Url

                lbl2_N.Text = DR.Item("Normal").ToString
                lbl2_C.Text = DR.Item("ClassC").ToString
                lbl2_B.Text = DR.Item("ClassB").ToString
                lbl2_A.Text = DR.Item("ClassA").ToString
                lbl2_T.Text = DR.Item("Total").ToString
            Case "Lube Oil"
                Url = "Dashboard_Current_Status_AllTag_LO.aspx?PLANT_ID=" & PLANT_ID

                Dim YValue() As Double = {Normal, ClassA}
                Dim XValue() As String = {"Normal", "ClassA"}

                Chart3.Series("Series1").Points.DataBindY(YValue)
                Chart3.Titles("Title1").Text = DR.Item("Type").ToString
                Chart3.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
                Chart3.Series("Series1").Points(1).ToolTip = "Abnormal : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("Normal") > 0 Then
                    Chart3.Series("Series1").Points(0).Label = DR.Item("Normal")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart3.Series("Series1").Points(1).Label = DR.Item("ClassA")
                End If

                Chart3.Series("Series1").Points(0).Color = Drawing.Color.Green
                Chart3.Series("Series1").Points(1).Color = Drawing.Color.Red

                Chart3.Series("Series1").Url = Url

                lbl3_N.Text = DR.Item("Normal").ToString
                lbl3_A.Text = DR.Item("ClassA").ToString
                lbl3_T.Text = DR.Item("Total").ToString
            Case "PdMA & MTap"
                Url = "Dashboard_Current_Status_AllTag_PdMA.aspx?PLANT_ID=" & PLANT_ID

                Dim YValue() As Double = {Normal, ClassA}
                Dim XValue() As String = {"Normal", "ClassA"}

                Chart4.Series("Series1").Points.DataBindY(YValue)
                Chart4.Titles("Title1").Text = DR.Item("Type").ToString
                Chart4.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
                Chart4.Series("Series1").Points(1).ToolTip = "Abnormal : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("Normal") > 0 Then
                    Chart4.Series("Series1").Points(0).Label = DR.Item("Normal")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart4.Series("Series1").Points(1).Label = DR.Item("ClassA")
                End If

                Chart4.Series("Series1").Points(0).Color = Drawing.Color.Green
                Chart4.Series("Series1").Points(1).Color = Drawing.Color.Red

                Chart4.Series("Series1").Url = Url

                lbl4_N.Text = DR.Item("Normal").ToString
                lbl4_A.Text = DR.Item("ClassA").ToString
                lbl4_T.Text = DR.Item("Total").ToString
            Case "Thermography"
                Url = "Dashboard_Current_Status_AllTag_THM.aspx?PLANT_ID=" & PLANT_ID

                Dim YValue() As Double = {Normal, ClassA}
                Dim XValue() As String = {"Normal", "ClassA"}

                Chart5.Series("Series1").Points.DataBindY(YValue)
                Chart5.Titles("Title1").Text = DR.Item("Type").ToString
                Chart5.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
                Chart5.Series("Series1").Points(1).ToolTip = "Abnormal : " & FormatNumber(ClassA, 0) & " tag(s)"

                If DR.Item("Normal") > 0 Then
                    Chart5.Series("Series1").Points(0).Label = DR.Item("Normal")
                End If
                If DR.Item("ClassA") > 0 Then
                    Chart5.Series("Series1").Points(1).Label = DR.Item("ClassA")
                End If

                Chart5.Series("Series1").Points(0).Color = Drawing.Color.Green
                Chart5.Series("Series1").Points(1).Color = Drawing.Color.Red

                Chart5.Series("Series1").Url = Url

                lbl5_N.Text = DR.Item("Normal").ToString
                lbl5_A.Text = DR.Item("ClassA").ToString
                lbl5_T.Text = DR.Item("Total").ToString
        End Select

    End Sub

End Class