﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class UC_Dashboard_New_Problem_Report_Plant
    Inherits System.Web.UI.UserControl

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL
    Dim CV As New Converter

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer)
        Dim DT_Dashboard As New DataTable

        If Year_F > 2500 Then
            Year_F = Year_F - 543
        End If
        If Year_T > 2500 Then
            Year_T = Year_T - 543
        End If

        lblMONTH_F.Text = Month_F
        lblMONTH_T.Text = Month_T
        lblYEAR_F.Text = Year_F
        lblYEAR_T.Text = Year_T
        lblEQUIPMENT.Text = Equipment
        lblPlantID.Text = Plant_ID

        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & lblPlantID.Text
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblPlantName.Text = DT.Rows(0).Item("PLANT_Name").ToString
        End If

        Dim DateFrom As Date = CV.StringToDate(Year_F.ToString & "-" & Month_F.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year_T.ToString & "-" & Month_T.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)

        DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)
        DT_Dashboard.Columns.Add("Rpt_Month")
        For i As Int32 = 0 To DT_Dashboard.Rows.Count - 1
            DT_Dashboard.Rows(i).Item("Rpt_Month") = Month(CDate(DT_Dashboard.Rows(i).Item("RPT_Period_Start")))
        Next

        Dim DT_DATA As New DataTable
        DT_DATA.Columns.Add("Month")
        DT_DATA.Columns.Add("Year")
        DT_DATA.Columns.Add("ClassC")
        DT_DATA.Columns.Add("ClassB")
        DT_DATA.Columns.Add("ClassA")

        Dim culture As New CultureInfo("en-us", False)
        Dim StrDate As String = ""
        StrDate = Year_F & CStr(Month_F).PadLeft(2, "0") & "01"
        Dim Date_Start As Date = Date.ParseExact(StrDate, "yyyyMMdd", culture)

        StrDate = Year_T & CStr(Month_T).PadLeft(2, "0") & "01"
        Dim Date_End As Date = Date.ParseExact(StrDate, "yyyyMMdd", culture)

        Do While Date_Start <= Date_End
            Dim Dr As DataRow
            Dr = DT_DATA.NewRow
            Dr("Month") = Date_Start.Month
            Dr("Year") = Date_Start.Year
            Dr("ClassC") = 0
            Dr("ClassB") = 0
            Dr("ClassA") = 0
            DT_DATA.Rows.Add(Dr)
            Date_Start = DateAdd(DateInterval.Month, 1, Date_Start)
        Loop

        For i As Integer = 0 To DT_DATA.Rows.Count - 1
            DT_DATA.Rows(i).Item("ClassA") = DT_Dashboard.Compute("COUNT(TAG_ID)", "Rpt_Month = " & DT_DATA.Rows(i).Item("Month").ToString & " AND (Rpt_Year - 543) = " & DT_DATA.Rows(i).Item("Year").ToString & " AND ICLS_ID=3")
            DT_DATA.Rows(i).Item("ClassB") = DT_Dashboard.Compute("COUNT(TAG_ID)", "Rpt_Month = " & DT_DATA.Rows(i).Item("Month").ToString & " AND (Rpt_Year - 543) = " & DT_DATA.Rows(i).Item("Year").ToString & " AND ICLS_ID=2")
            DT_DATA.Rows(i).Item("ClassC") = DT_Dashboard.Compute("COUNT(TAG_ID)", "Rpt_Month = " & DT_DATA.Rows(i).Item("Month").ToString & " AND (Rpt_Year - 543) = " & DT_DATA.Rows(i).Item("Year").ToString & " AND ICLS_ID=1")
        Next

        Session("Dashboard_New_Problem_Report_Plant") = DT_DATA

        If DT_DATA.Rows.Count > 6 Then
            Dim NewWidth As Unit = Unit.Pixel(77 * DT_DATA.Rows.Count)
            ChartMain.Width = NewWidth
        End If


        DisplayChart(ChartMain, New EventArgs, Plant_ID, Month_F, Month_T, Year_F, Year_T, Equipment)

        rptData.DataSource = DT_DATA
        rptData.DataBind()

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type)
        Dim DisplayText As String = ""
        Dim DT As New DataTable

        If Year_F < 2500 Then
            Year_F = Year_F + 543
        End If
        If Year_T < 2500 Then
            Year_T = Year_T + 543
        End If

        If Year_F = Year_T Then
            If Month_F = Month_T Then
                DisplayText = "on  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F
            Else
                DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_F
            End If
        Else
            DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_T
        End If

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)

        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain.Titles("Title1").Text = "New Problem  for  " & Dashboard.FindEquipmentName(Equipment) & vbNewLine & vbNewLine & "Occurred  " & DisplayText & "  on  " & lblPlantName.Text
        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter

        ChartMain.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain.ChartAreas("ChartArea1").AxisY.Title = "Problem(s)"
        ChartMain.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver

        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series2").Points.Clear()
        ChartMain.Series("Series3").Points.Clear()
        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        ChartMain.Series("Series2").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        ChartMain.Series("Series3").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        DT = New DataTable
        DT = Session("Dashboard_New_Problem_Report_Plant")
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Url As String = ""
            Select Case Equipment
                Case EIR_BL.Report_Type.Stationary_Routine_Report
                    Url = "Dashboard_Detail_Plant_ST.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & Equipment & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & DT.Rows(i).Item("Month").ToString & "&YEAR=" & DT.Rows(i).Item("Year").ToString
                Case EIR_BL.Report_Type.Rotating_Routine_Report
                    Url = "Dashboard_Detail_Plant_RO.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & Equipment & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & DT.Rows(i).Item("Month").ToString & "&YEAR=" & DT.Rows(i).Item("Year").ToString
                Case EIR_BL.Report_Type.Lube_Oil_Report
                    Url = "Dashboard_Detail_Plant_LO.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & Equipment & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & DT.Rows(i).Item("Month").ToString & "&YEAR=" & DT.Rows(i).Item("Year").ToString
                Case EIR_BL.Report_Type.PdMA_MTap_Report

                    Url = "Dashboard_Detail_Plant_PDMA.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & Equipment & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & DT.Rows(i).Item("Month").ToString & "&YEAR=" & DT.Rows(i).Item("Year").ToString

                Case EIR_BL.Report_Type.Thermography_Report
                    Url = "Dashboard_Detail_Plant_THM.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & Equipment & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & DT.Rows(i).Item("Month").ToString & "&YEAR=" & DT.Rows(i).Item("Year").ToString
                Case EIR_BL.Report_Type.All
                    Url = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & Equipment & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & DT.Rows(i).Item("Month").ToString & "&YEAR=" & DT.Rows(i).Item("Year").ToString
            End Select

            ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("Month").ToString & "/" & DT.Rows(i).Item("Year").ToString, DT.Rows(i).Item("ClassC"))
            ChartMain.Series("Series2").Points.AddXY(DT.Rows(i).Item("Month").ToString & "/" & DT.Rows(i).Item("Year").ToString, DT.Rows(i).Item("ClassB"))
            ChartMain.Series("Series3").Points.AddXY(DT.Rows(i).Item("Month").ToString & "/" & DT.Rows(i).Item("Year").ToString, DT.Rows(i).Item("ClassA"))

            Dim Tooltip_ClassC As String = "ClassC : " & DT.Rows(i).Item("ClassC").ToString & " Tag(s)"
            Dim Tooltip_ClassB As String = "ClassB : " & DT.Rows(i).Item("ClassB").ToString & " Tag(s)"
            Dim Tooltip_ClassA As String = "ClassA : " & DT.Rows(i).Item("ClassA").ToString & " Tag(s)"

            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip_ClassC
            ChartMain.Series("Series2").Points(i).ToolTip = Tooltip_ClassB
            ChartMain.Series("Series3").Points(i).ToolTip = Tooltip_ClassA
            ChartMain.Series("Series1").Points(i).Url = Url
            ChartMain.Series("Series2").Points(i).Url = Url
            ChartMain.Series("Series3").Points(i).Url = Url
        Next

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblMonth As Label = e.Item.FindControl("lblMonth")
        Dim lblYear As Label = e.Item.FindControl("lblYear")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")

        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0

        lblMonth.Text = e.Item.DataItem("Month")
        lblYear.Text = e.Item.DataItem("Year")

        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If

        Select Case lblEQUIPMENT.Text
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Detail_Plant_ST.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & lblMonth.Text & "&YEAR=" & lblYear.Text & "';"
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Detail_Plant_RO.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & lblMonth.Text & "&YEAR=" & lblYear.Text & "';"
            Case EIR_BL.Report_Type.Lube_Oil_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Detail_Plant_LO.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & lblMonth.Text & "&YEAR=" & lblYear.Text & "';"
            Case EIR_BL.Report_Type.PdMA_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Detail_Plant_PDMA.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & lblMonth.Text & "&YEAR=" & lblYear.Text & "';"
            Case EIR_BL.Report_Type.Thermography_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Detail_Plant_THM.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & lblMonth.Text & "&YEAR=" & lblYear.Text & "';"
            Case EIR_BL.Report_Type.All
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & lblMonth.Text & "&YEAR=" & lblYear.Text & "';"
        End Select

    End Sub

End Class