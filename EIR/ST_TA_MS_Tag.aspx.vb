﻿
Imports System.Data
Imports System.Data.SqlClient
Public Class ST_TA_MS_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL


    Private Property EDIT_TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("EDIT_TAG_TYPE_ID")) Then
                Return ViewState("EDIT_TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("EDIT_TAG_TYPE_ID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        'DialogAbsorber.CloseDialog()
        'DialogDrum.CloseDialog()
        'DialogColumn.CloseDialog()
        'DialogFilter.CloseDialog()
        'DialogHeat_Exchnager.CloseDialog()

        If Not IsPostBack Then

            DialogAbsorber.CloseDialog()
            DialogDrum.CloseDialog()
            DialogColumn.CloseDialog()
            DialogFilter.CloseDialog()
            DialogHeat_Exchnager.CloseDialog()

            Dialog_Spec.CloseDialog()


            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTag()

        Dim SQL As String = " SELECT * FROM (" & vbNewLine
        SQL &= "   Select AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TAG.ROUTE_ID,MS_ST_TAG.AREA_ID,MS_ST_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TAG' To_Table " & vbNewLine
        SQL &= "   FROM MS_ST_TAG" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine

        SQL &= "   UNION ALL" & vbNewLine
        SQL &= "   Select  AREA_CODE +'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TA_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TA_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TA_TAG.ROUTE_ID,MS_ST_TA_TAG.AREA_ID,MS_ST_TA_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TA_TAG' To_Table " & vbNewLine
        SQL &= "   From MS_ST_TA_TAG" & vbNewLine
        SQL &= "   INNER Join MS_ST_TAG_TYPE On  MS_ST_TA_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER Join MS_ST_Route ON MS_ST_TA_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER Join MS_PLANT On MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER Join MS_Area ON MS_ST_TA_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER Join MS_Process On MS_ST_TA_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
        SQL &= " ) TB " & vbNewLine

        Dim ST_TA_TAG_TYPE As String = EIR_BL.ST_TAG_TYPE.Drum & "," & EIR_BL.ST_TAG_TYPE.Absorber & "," & EIR_BL.ST_TAG_TYPE.Column & "," & EIR_BL.ST_TAG_TYPE.Filter & "," & EIR_BL.ST_TAG_TYPE.Heat_Exchnager & "," & EIR_BL.ST_TAG_TYPE.Strainer

        SQL &= " WHERE TAG_TYPE_ID in (" & ST_TA_TAG_TYPE & ") " & vbNewLine



        Dim WHERE As String = ""

        If ddl_Search_Tag_Type.SelectedIndex > 0 Then
            WHERE &= " TB.TAG_TYPE_ID=" & ddl_Search_Tag_Type.Items(ddl_Search_Tag_Type.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " TB.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " TB.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " TB.AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " And "
        End If

        If WHERE <> "" Then
            SQL &= " AND " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("MS_ST_TA_TAG") = DT

        Navigation.SesssionSourceName = "MS_ST_TA_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagName.Text = e.Item.DataItem("TAG_Name")
        lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")
        lblTo_Table.Text = e.Item.DataItem("To_Table")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")

        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim To_Table As String = lblTo_Table.Text

        Dialog_Spec.CloseDialog()
        Dialog_Spec.ClearDialog()
        DialogAbsorber.CloseDialog()
        DialogDrum.CloseDialog()
        DialogColumn.CloseDialog()
        DialogFilter.CloseDialog()
        DialogHeat_Exchnager.CloseDialog()

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'ddl_Edit_Area.Enabled = False
                'ddl_Edit_Process.Enabled = False
                'txtTagNo.ReadOnly = True

                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListTag.Enabled = False


                '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------

                Dim SQL As String = ""
                SQL = " "
                SQL &= " SELECT " & To_Table & ".* ,MS_PLANT.PLANT_ID" & vbNewLine
                SQL &= " FROM " & To_Table & " LEFT JOIN MS_ST_ROUTE ON " & To_Table & ".ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_ST_Tag_Type ON " & To_Table & ".TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
                SQL &= " WHERE " & To_Table & ".TAG_ID=" & TAG_ID

                'If (lblTo_Table.Text = "MS_ST_TAG") Then
                '    '--------------Bind Value------------
                '    SQL = " "
                '    SQL &= " SELECT MS_ST_TAG.* ,MS_PLANT.PLANT_ID" & vbNewLine
                '    SQL &= " FROM MS_ST_TAG LEFT JOIN MS_ST_ROUTE ON MS_ST_TAG.ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
                '    SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
                '    SQL &= " LEFT JOIN MS_ST_Tag_Type ON MS_ST_TAG.TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
                '    SQL &= " WHERE MS_ST_TAG.TAG_ID=" & TAG_ID

                'ElseIf (lblTo_Table.Text = "MS_ST_TA_TAG") Then
                '    SQL = " "
                '    SQL &= " Select MS_ST_TA_TAG.* ,MS_PLANT.PLANT_ID" & vbNewLine
                '    SQL &= " From MS_ST_TA_TAG LEFT Join MS_ST_ROUTE On MS_ST_TA_TAG.ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
                '    SQL &= " Left Join MS_PLANT On MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
                '    SQL &= " Left Join MS_ST_Tag_Type ON MS_ST_TA_TAG.TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
                '    SQL &= " WHERE MS_ST_TA_TAG.TAG_ID =" & TAG_ID

                'End If

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
                Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")
                Dim AREA_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
                Dim PROC_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROC_ID = DT.Rows(0).Item("PROC_ID")
                Dim TAG_TYPE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_ID")) Then TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")

                BL.BindDDlPlant(ddl_Edit_Plant, PLANT_ID)
                BL.BindDDl_ST_Route(PLANT_ID, ddl_Edit_Route, ROUTE_ID)
                BL.BindDDlArea(PLANT_ID, ddl_Edit_Area, AREA_ID)
                BL.BindDDlProcess(ddl_Edit_Process, PROC_ID)
                BL.BindDDlTagType("ST", ddl_Edit_Type, TAG_TYPE_ID)
                txtTagNo.Text = DT.Rows(0).Item("TAG_NO")
                txtTagNo.Attributes("TagID") = TAG_ID
                txtTagName.Text = DT.Rows(0).Item("TAG_NAME")
                lblEditTo_Table.Text = lblTo_Table.Text
                txtDesc.Text = DT.Rows(0).Item("TAG_Description")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                btnSave.Focus()
                EDIT_TAG_TYPE_ID = TAG_TYPE_ID
                Select Case TAG_TYPE_ID
                    Case EIR_BL.ST_TAG_TYPE.Drum
                        DialogDrum.TAG_CODE = lblTagNo.Text
                        DialogDrum.TAG_ID = TAG_ID
                        DialogDrum.TAG_TYPE_ID = TAG_TYPE_ID
                        DialogDrum.TAG_Name = lblTagName.Text
                        DialogDrum.TAG_TYPE_Name = lblTagType.Text
                        DialogDrum.TO_TABLE_MS = To_Table
                        DialogDrum.TO_TABLE_SPEC = "MS_ST_DRUM_SPEC"
                        DialogDrum.BindHeader()
                        DialogDrum.ShowDialog()

                    Case EIR_BL.ST_TAG_TYPE.General_Stationary

                        'DialogAbsorber.TO_TABLE = "MS_ST_ABSORBER_SPEC"
                        'DialogColumn.ShowDialog()

                    Case EIR_BL.ST_TAG_TYPE.Absorber
                        DialogAbsorber.TAG_CODE = lblTagNo.Text
                        DialogAbsorber.TAG_ID = TAG_ID
                        DialogAbsorber.TAG_TYPE_ID = TAG_TYPE_ID
                        DialogAbsorber.TAG_Name = lblTagName.Text
                        DialogAbsorber.TAG_TYPE_Name = lblTagType.Text
                        DialogAbsorber.TO_TABLE_MS = To_Table
                        DialogAbsorber.TO_TABLE_SPEC = "MS_ST_ABSORBER_SPEC"
                        DialogAbsorber.BindHeader()
                        DialogAbsorber.ShowDialog()

                    Case EIR_BL.ST_TAG_TYPE.Column
                        DialogColumn.TAG_CODE = lblTagNo.Text
                        DialogColumn.TAG_ID = TAG_ID
                        DialogColumn.TAG_TYPE_ID = TAG_TYPE_ID
                        DialogColumn.TAG_Name = lblTagName.Text
                        DialogColumn.TAG_TYPE_Name = lblTagType.Text
                        DialogColumn.TO_TABLE_MS = To_Table
                        DialogColumn.TO_TABLE_SPEC = "MS_ST_COLUMN_SPEC"
                        DialogColumn.BindHeader()
                        DialogColumn.ShowDialog()

                    Case EIR_BL.ST_TAG_TYPE.Filter
                        DialogFilter.TAG_CODE = lblTagNo.Text
                        DialogFilter.TAG_ID = TAG_ID
                        DialogFilter.TAG_TYPE_ID = TAG_TYPE_ID
                        DialogFilter.TAG_Name = lblTagName.Text
                        DialogFilter.TAG_TYPE_Name = lblTagType.Text
                        DialogFilter.TO_TABLE_MS = To_Table
                        DialogFilter.TO_TABLE_SPEC = "MS_ST_FILTER_SPEC"
                        DialogFilter.BindHeader()
                        DialogFilter.ShowDialog()

                    Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager
                        DialogHeat_Exchnager.TAG_CODE = lblTagNo.Text
                        DialogHeat_Exchnager.TAG_ID = TAG_ID
                        DialogHeat_Exchnager.TAG_TYPE_ID = TAG_TYPE_ID
                        DialogHeat_Exchnager.TAG_Name = lblTagName.Text
                        DialogHeat_Exchnager.TAG_TYPE_Name = lblTagType.Text
                        DialogHeat_Exchnager.TO_TABLE_MS = To_Table
                        DialogHeat_Exchnager.TO_TABLE_SPEC = "MS_ST_HEAT_EXCHNAGER_SPEC"
                        DialogHeat_Exchnager.BindHeader()
                        DialogHeat_Exchnager.ShowDialog()

                    Case EIR_BL.ST_TAG_TYPE.Strainer


                    Case Else


                End Select

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE " & To_Table & " Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------

        pnlListTag.Enabled = True
    End Sub


    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        BL.BindDDlPlant(ddl_Edit_Plant, False)
        BL.BindDDl_ST_Route(0, ddl_Edit_Route)
        BL.BindDDlArea(0, ddl_Edit_Area)
        BL.BindDDlProcess(ddl_Edit_Process)
        BL.BindDDlTagType("ST", ddl_Edit_Type)
        txtTagNo.Text = ""
        txtTagNo.Attributes("TagID") = "0"
        txtTagName.Text = ""

        txtDesc.Text = ""
        chkAvailable.Checked = True

        btnCreate.Visible = True

        '-------------InStall Javascript------------
        'PTClass.ImplementJavaIntegerText(txtTagNo)

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlST_TA_TagType(ddl_Search_Tag_Type)
        BL.BindDDlPlant(ddl_Search_Plant, False)
        BL.BindDDl_ST_Route(0, ddl_Search_Route)
        BL.BindDDlArea(0, ddl_Search_Area)
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BL.BindDDlArea(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Area)
        BindTag()
    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Route)
        BL.BindDDlArea(ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Area)
        BindTag()
        btnSave.Focus()
    End Sub

    Protected Sub ddl_Search_Area_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Area.SelectedIndexChanged
        BindTag()
    End Sub

    Protected Sub ddl_Search_Tag_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Tag_Type.SelectedIndexChanged
        BindTag()
    End Sub

    Protected Sub ddl_Search_Route_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged
        BindTag()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click


        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        ddl_Edit_Area.Enabled = True
        ddl_Edit_Process.Enabled = True
        txtTagNo.ReadOnly = False
        ddl_Edit_Area.Focus()
        lblUpdateMode.Text = "Create"
        lblEditTo_Table.Text = "MS_ST_TA_TAG"  '---------Save ลงตาราง MS_ST_TA_TAG
        '-----------------------------------
        pnlListTag.Enabled = False


        Dialog_Spec.ShowDialog()
        Dialog_Spec.ClearDialog()
        DialogAbsorber.CloseDialog()
        DialogDrum.CloseDialog()
        DialogColumn.CloseDialog()
        DialogFilter.CloseDialog()
        DialogHeat_Exchnager.CloseDialog()


    End Sub

    Protected Sub ddl_Edit_Area_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Area.SelectedIndexChanged

    End Sub

    Protected Sub ddl_Edit_Process_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Process.SelectedIndexChanged

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Select Case EDIT_TAG_TYPE_ID
            Case EIR_BL.ST_TAG_TYPE.Drum
                DialogDrum.Save()

            Case EIR_BL.ST_TAG_TYPE.Absorber
                DialogAbsorber.Save()

            Case EIR_BL.ST_TAG_TYPE.Column
                DialogColumn.Save()

            Case EIR_BL.ST_TAG_TYPE.Filter

                DialogFilter.Save()

            Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager

                DialogHeat_Exchnager.Save()

            Case EIR_BL.ST_TAG_TYPE.Strainer


            Case Else


        End Select



        Dim TagID As Integer = DialogAbsorber.New_TagID


        Dim To_Table As String = lblEditTo_Table.Text
        '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------

        Dim SQL As String = "SELECT * FROM " & To_Table & " WHERE AREA_ID=" & ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value & " AND PROC_ID=" & ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value & " AND TAG_No='" & txtTagNo.Text & "' AND TAG_ID<>" & TagID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        SQL = "SELECT * FROM " & To_Table & " WHERE TAG_ID=" & TagID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TagID = GetNewTagID(To_Table)
            DR("TAG_ID") = TagID
        Else
            DR = DT.Rows(0)
        End If

        'DR("AREA_ID") = ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value
        'DR("PROC_ID") = ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value
        'DR("TAG_No") = txtTagNo.Text '.ToString.PadLeft(4, "0")
        'DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        'DR("TAG_Name") = txtTagName.Text
        'DR("TAG_TYPE_ID") = ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value
        'DR("TAG_Description") = txtDesc.Text
        'DR("TAG_Order") = TagID
        DR("Active_Status") = chkAvailable.Checked
        'DR("Update_By") = Session("USER_ID")
        'DR("Update_Time") = Now

        'If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        'Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            'lblValidation.Text = ex.Message
            'pnlValidation.Visible = True
            'Exit Sub
        End Try

        ResetTag(Nothing, Nothing)

        'lblBindingSuccess.Text = "Save successfully"
        'pnlBindingSuccess.Visible = True



        ''------------ Focus Last Edit -----------
        'DT = Session("MS_ST_TA_TAG")
        'DT.DefaultView.RowFilter = "TAG_ID=" & TagID
        'If DT.DefaultView.Count > 0 Then
        '    Dim RowIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
        '    DT.DefaultView.RowFilter = ""
        '    Navigation.CurrentPage = Math.Ceiling((RowIndex + 1) / Navigation.PageSize)
        'End If
        '------------ Focus Last Edit -----------
        DT = Session("MS_ST_TA_TAG")
        DT.DefaultView.RowFilter = "TAG_ID=" & TagID
        If DT.DefaultView.Count > 0 Then
            Dim RowIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            DT.DefaultView.RowFilter = ""
            Navigation.CurrentPage = Math.Ceiling((RowIndex + 1) / Navigation.PageSize)
        End If


        DialogAbsorber.CloseDialog()
        DialogDrum.CloseDialog()
        DialogColumn.CloseDialog()
        DialogFilter.CloseDialog()
        DialogHeat_Exchnager.CloseDialog()

    End Sub

    Private Function GetNewTagID(ByVal Table_Name As String) As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM " & Table_Name & " "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function


End Class