﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_New_Problem_Report
    Inherits System.Web.UI.UserControl

    Dim Dashboard As New DashboardClass
    Dim CV As New Converter
    Dim BL As New EIR_BL

    Public Sub BindData(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PrintPage As Boolean = False)
        lblMONTH_F.Text = Month_F
        lblMONTH_T.Text = Month_T
        lblYEAR_F.Text = Year_F
        lblYEAR_T.Text = Year_T
        lblEQUIPMENT.Text = Equipment
        lblPrintPage.Text = PrintPage

        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        Dim DateFrom As Date = CV.StringToDate(Year_F.ToString & "-" & Month_F.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year_T.ToString & "-" & Month_T.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)

        Dim PLANT As New DataTable
        SQL = "select PLANT_ID,PLANT_Name from MS_Plant where Active_status = 1"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(PLANT)
        PLANT.Columns.Add("ClassA", GetType(Integer))
        PLANT.Columns.Add("ClassB", GetType(Integer))
        PLANT.Columns.Add("ClassC", GetType(Integer))

        DT_Dashboard = Dashboard.GetDashboardData(Equipment, 0, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)

        For i As Integer = 0 To PLANT.Rows.Count - 1
            PLANT.Rows(i).Item("ClassA") = DT_Dashboard.Compute("COUNT(TAG_ID)", "PLANT_ID=" & PLANT.Rows(i).Item("PLANT_ID") & " AND ICLS_ID=3")
            PLANT.Rows(i).Item("ClassB") = DT_Dashboard.Compute("COUNT(TAG_ID)", "PLANT_ID=" & PLANT.Rows(i).Item("PLANT_ID") & " AND ICLS_ID=2")
            PLANT.Rows(i).Item("ClassC") = DT_Dashboard.Compute("COUNT(TAG_ID)", "PLANT_ID=" & PLANT.Rows(i).Item("PLANT_ID") & " AND ICLS_ID=1")
        Next

        PLANT.DefaultView.Sort = "PLANT_ID"
        PLANT = PLANT.DefaultView.ToTable

        Session("Dashboard_New_Problem_Report") = PLANT
        DisplayChart(ChartMain, New EventArgs, Month_F, Month_T, Year_F, Year_T, Equipment, PrintPage)
        rptData.DataSource = PLANT
        rptData.DataBind()


    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer, ByVal PrintPage As Boolean)
        Dim DisplayText As String = ""
        If Year_F < 2500 Then
            Year_F = Year_F + 543
        End If
        If Year_T < 2500 Then
            Year_T = Year_T + 543
        End If

        If Year_F = Year_T Then
            If Month_F = Month_T Then
                DisplayText = "on  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F
            Else
                DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_F
            End If
        Else
            DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_T
        End If

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)

        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy

        ChartMain.Titles("Title1").Text = "New Problem  for  " & Dashboard.FindEquipmentName(Equipment) & vbNewLine & vbNewLine & "Occurred  " & DisplayText
        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter

        ChartMain.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain.ChartAreas("ChartArea1").AxisY.Title = "Problem(s)"
        ChartMain.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver

        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        ChartMain.Series("Series2").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
        ChartMain.Series("Series3").ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn

        Dim DT As DataTable = Session("Dashboard_New_Problem_Report")

        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series2").Points.Clear()
        ChartMain.Series("Series3").Points.Clear()

        If DT.Rows.Count > 6 Then
            Dim NewWidth As Unit = Unit.Pixel(77 * DT.Rows.Count)
            ChartMain.Width = NewWidth
        End If

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Url As String = "Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & DT.Rows(i).Item("PLANT_ID") & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Equipment

            ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("PLANT_Name"), DT.Rows(i).Item("ClassC"))
            ChartMain.Series("Series2").Points.AddXY(DT.Rows(i).Item("PLANT_Name"), DT.Rows(i).Item("ClassB"))
            ChartMain.Series("Series3").Points.AddXY(DT.Rows(i).Item("PLANT_Name"), DT.Rows(i).Item("ClassA"))

            Dim Tooltip_ClassC As String = "ClassC : " & DT.Rows(i).Item("ClassC").ToString & " Tag(s)"
            Dim Tooltip_ClassB As String = "ClassB : " & DT.Rows(i).Item("ClassB").ToString & " Tag(s)"
            Dim Tooltip_ClassA As String = "ClassA : " & DT.Rows(i).Item("ClassA").ToString & " Tag(s)"

            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip_ClassC
            ChartMain.Series("Series2").Points(i).ToolTip = Tooltip_ClassB
            ChartMain.Series("Series3").Points(i).ToolTip = Tooltip_ClassA
            ChartMain.Series("Series1").Points(i).Url = Url
            ChartMain.Series("Series2").Points(i).Url = Url
            ChartMain.Series("Series3").Points(i).Url = Url
        Next
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")

        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0

        lblPlant.Text = e.Item.DataItem("PLANT_NAME")

        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If
        tbTag.Attributes("onclick") = "window.location.href='Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "';"
    End Sub

End Class