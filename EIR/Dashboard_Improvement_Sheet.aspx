﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Improvement_Sheet.aspx.vb" Inherits="EIR.Dashboard_Improvement_Sheet" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<%@ Register src="UC_Dashboard_Improvement_Sheet.ascx" tagname="UC_Dashboard_Improvement_Sheet" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2>Problem Improvement By Year</h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:0px solid #efefef;">
			  <tr>
				<td colspan="3">
				    <h3>
                    <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="Year"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Year" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                        &nbsp;<asp:Label ID="Label2" runat="server" ForeColor="#009933" 
                        Text="Equipment Category"></asp:Label>
                    &nbsp;<asp:DropDownList ID="ddl_Equipment" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                        &nbsp;</h3> 
                                
                 </td>
			  </tr>
			  <tr>
			    <td>
			        <asp:Panel ID="pnlDashboard" runat="server">
			            <uc1:UC_Dashboard_Improvement_Sheet ID="UC_Dashboard_Improvement_Sheet" runat="server" />
			        </asp:Panel>
			    </td>
		      </tr>
			</table>
            
            <h2 align="right">
                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="button" style="width:100px;" OnClick="btnExport_Click" />  
                <asp:TextBox ID="txtHTML" runat="server" style="display:none;" TextMode="MultiLine"></asp:TextBox>
                <div id="ScriptContainer" runat="server" style="display:none;"></div>              
            </h2>
            
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

