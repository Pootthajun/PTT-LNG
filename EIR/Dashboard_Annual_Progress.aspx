﻿<%@ Page Language="vb" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeBehind="Dashboard_Annual_Progress.aspx.vb" Inherits="EIR.Dashboard_Annual_Progress" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<%@ Register src="UC_Dashboard_Annual_Progress.ascx" tagname="UC_Dashboard_Annual_Progress" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>

			<!-- Page Head -->
			<h2>Annual Progress Dashboard</h2>

        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td>
				    <h3 > 
				        <asp:Label ID="Label1" runat="server" Text="Year" ForeColor="#009933"></asp:Label>
				        &nbsp;<asp:DropDownList ID="ddl_Year" runat="server" AutoPostBack="true" >
                    </asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" ForeColor="#009933" 
                            Text="Equipment Category"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Equipment" runat="server" AutoPostBack="true" >
                    </asp:DropDownList>
                                    &nbsp;&nbsp;
                        <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="Plant"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Plant" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </h3>         
                </td>
			  </tr>
			  <tr>
			    <td>
			        <asp:Panel ID="pnlDashboard" runat="server">
			            <uc1:UC_Dashboard_Annual_Progress ID="UC_Dashboard_Annual_Progress" runat="server" />
			        </asp:Panel>
			    </td> 
		      </tr>
			</table>
			<h2 align="right">
                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="button" style="width:100px;" OnClick="btnExport_Click" />  
                <asp:TextBox ID="txtHTML" runat="server" style="display:none;" TextMode="MultiLine"></asp:TextBox>
                <div id="ScriptContainer" runat="server" style="display:none;"></div>
            </h2>
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>