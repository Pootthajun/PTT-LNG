﻿Imports System.Data.SqlClient

Public Class PIPE_Component_Type
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ResetType(Nothing, Nothing)
            ClearPanelSearch()
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindType()

        Dim SQL As String = ""
        SQL &= " Select COMP.Component_Type,COMP.Component_Type_Name,COMP.Active_Status,COMP.Update_By,COMP.Update_Time," & vbLf
        SQL &= " COUNT(TAG.TAG_ID) TotalTag" & vbLf
        SQL &= " FROM MS_PIPE_Component_Type COMP" & vbLf
        SQL &= " LEFT JOIN " & vbLf
        SQL &= " (SELECT DISTINCT MS_PIPE_POINT.TAG_ID,MS_PIPE_POINT.Component_Type" & vbLf
        SQL &= " 	FROM MS_PIPE_TAG " & vbLf
        SQL &= " 	INNER JOIN MS_PIPE_POINT ON  MS_PIPE_TAG.TAG_ID=MS_PIPE_POINT.TAG_ID" & vbLf
        SQL &= " 	WHERE MS_PIPE_TAG.Active_Status=1" & vbLf
        SQL &= " ) TAG On TAG.Component_Type=COMP.Component_Type" & vbLf

        Dim WHERE As String = ""
        If txt_Search.Text <> "" Then
            WHERE &= " COMP.Component_Type_Name Like '%" & txt_Search.Text.Replace("'", "''") & "%' AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " GROUP BY COMP.Component_Type,COMP.Component_Type_Name,COMP.Active_Status,COMP.Update_By,COMP.Update_Time" & vbLf
        SQL &= " ORDER BY COMP.Component_Type_Name" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("PIPE_Component_Type") = DT

        Navigation.SesssionSourceName = "PIPE_Component_Type"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptType
    End Sub

    Protected Sub rptrptType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptType.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfbDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblType.Text = e.Item.DataItem("Component_Type_Name").ToString

        If Not IsDBNull(e.Item.DataItem("TotalTag")) AndAlso e.Item.DataItem("TotalTag") > 0 Then
            lblPipe.Text = FormatNumber(e.Item.DataItem("TotalTag"), 0)
            cfbDelete.Enabled = False
            btnDelete.Visible = False
        Else
            lblPipe.Text = "-"
            cfbDelete.Enabled = True
            btnDelete.Visible = True
        End If

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        If Not IsDBNull(e.Item.DataItem("Update_Time")) Then
            lblUpdateTime.Text = C.DateToString(e.Item.DataItem("Update_Time"), "dd MMM yyyy")
        Else
            lblUpdateTime.Text = "-"
        End If

        btnEdit.Attributes("Component_Type") = e.Item.DataItem("Component_Type")

    End Sub

    Protected Sub rptType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptType.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Component_Type As Integer = btnEdit.Attributes("Component_Type")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("Component_Type") = Component_Type
                '------------------------------------
                pnlListType.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT Component_Type,Component_Type_Name,Active_Status,Update_By,Update_Time" & vbLf
                SQL &= " FROM MS_PIPE_Component_Type COMP" & vbLf
                SQL &= " WHERE Component_Type = " & Component_Type & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "component Type Name Not Found"
                    pnlBindingError.Visible = True
                    BindType()
                    Exit Sub
                End If

                txtType.Text = DT.Rows(0).Item("Component_Type_Name")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_PIPE_Component_Type Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE Component_Type=" & Component_Type
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindType()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try

            Case "Delete"
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM MS_PIPE_Component_Type WHERE Component_Type=" & Component_Type
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindType()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try
        End Select
    End Sub

    Protected Sub ResetType(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindType()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListType.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()

        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        txtType.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        txt_Search.Text = ""
        BindType()
    End Sub

    Private Sub txt_Search_TextChanged(sender As Object, e As EventArgs) Handles txt_Search.TextChanged
        BindType()
    End Sub
#End Region

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("Component_Type") = 0

        '-----------------------------------
        pnlListType.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtType.Text = "" Then
            lblValidation.Text = "Please insert component type name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim Component_Type As Integer = lblUpdateMode.Attributes("Component_Type")

        Dim SQL As String = "SELECT * FROM MS_PIPE_Component_Type WHERE Component_Type_Name='" & txtType.Text.Replace("'", "''") & "' AND Component_Type<>" & Component_Type
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This component type is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_PIPE_Component_Type WHERE Component_Type=" & Component_Type
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            Component_Type = GetNewcomponentType()
            DR("Component_Type") = Component_Type
        Else
            DR = DT.Rows(0)
        End If

        DR("Component_Type_Name") = txtType.Text

        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetType(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewcomponentType() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(Component_Type),0)+1 FROM MS_PIPE_Component_Type "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

End Class