﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_TagInspection
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL

#Region "Property"

    Public Property TAG_CLASS() As EIR_BL.Tag_Class
        Get
            If ViewState("TagClass") = "RO" Then
                Return EIR_BL.Tag_Class.Rotating
            Else
                Return EIR_BL.Tag_Class.Stationary
            End If
        End Get
        Set(ByVal value As EIR_BL.Tag_Class)
            Select Case value
                Case EIR_BL.Tag_Class.Rotating
                    ViewState("TagClass") = "RO"
                Case Else
                    ViewState("TagClass") = "ST"
            End Select
        End Set
    End Property

    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_ID") = value
        End Set
    End Property

    Public Property TAG_CODE() As String
        Get
            Return lbl_Code.InnerHtml
        End Get
        Set(ByVal value As String)
            lbl_Code.InnerHtml = value
        End Set
    End Property
    Public Property TAG_NAME() As String
        Get
            Return lblTagName.InnerHtml
        End Get
        Set(ByVal value As String)
            lblTagName.InnerHtml = value
            lblTagName.Title = value
        End Set
    End Property

    Public Property TAG_TYPE_ID() As Integer
        Get
            If Not IsNumeric(lbl_Type.Attributes("TAG_TYPE_ID")) Then
                Return 0
            Else
                Return lbl_Type.Attributes("TAG_TYPE_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_Type.Attributes("TAG_TYPE_ID") = value
        End Set
    End Property

    Public Property TAG_TYPE_NAME() As String
        Get
            Return lbl_Type.InnerHtml
        End Get
        Set(ByVal value As String)
            lbl_Type.InnerHtml = value
            lbl_Type.Title = value
        End Set
    End Property

    Public Property REMAIN_PROBLEM() As Integer
        Get
            If Not IsNumeric(lbl_Remain.InnerHtml) Then
                Return 0
            Else
                Return lbl_Remain.InnerHtml
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_Remain.InnerHtml = value
        End Set
    End Property

    Public Property NEW_UPLOAD() As Integer
        Get
            If Not IsNumeric(lbl_New.InnerHtml) Then
                Return 0
            Else
                Return lbl_New.InnerHtml
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_New.InnerHtml = value
        End Set
    End Property

    Public Property TRACE_COUNT() As Integer
        Get
            If Not IsNumeric(lbl_TraceTag.InnerHtml) Then
                Return 0
            Else
                Return lbl_TraceTag.InnerHtml
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_TraceTag.InnerHtml = value
        End Set
    End Property

    Public Property LEVEL() As EIR_BL.InspectionLevel
        Get
            Select Case lbl_Level.InnerHtml
                Case "Leak"
                    Return EIR_BL.InspectionLevel.Leak
                Case "ClassA"
                    Return EIR_BL.InspectionLevel.ClassA
                Case "ClassB"
                    Return EIR_BL.InspectionLevel.ClassB
                Case "ClassC"
                    Return EIR_BL.InspectionLevel.ClassC
                Case Else
                    Return EIR_BL.InspectionLevel.Normal
            End Select
        End Get
        Set(ByVal value As EIR_BL.InspectionLevel)
            Select Case value
                Case EIR_BL.InspectionLevel.Leak
                    lbl_Level.InnerHtml = "Leak"
                Case EIR_BL.InspectionLevel.ClassA
                    lbl_Level.InnerHtml = "ClassA"
                Case EIR_BL.InspectionLevel.ClassB
                    lbl_Level.InnerHtml = "ClassB"
                Case EIR_BL.InspectionLevel.ClassC
                    lbl_Level.InnerHtml = "ClassC"
                Case EIR_BL.InspectionLevel.Normal, -1
                    lbl_Level.InnerHtml = "Normal"
                    value = EIR_BL.InspectionLevel.Normal
            End Select
            lbl_Level.Attributes("class") = BL.Get_Inspection_Css_Text_By_Level(value)
            TDTree.Attributes("Class") = BL.Get_Inspection_Css_Box_By_Level(value)
        End Set
    End Property

    Public Property IsExpand() As Boolean
        Get
            Return TrInspection.Visible
        End Get
        Set(ByVal value As Boolean)
            TrInspection.Visible = value
            '------------------Bind Inspection ----------------
            If Not value Then Exit Property

            Dim SQL As String = "SELECT * FROM VW_REPORT_" & ViewState("TagClass") & "_DETAIL " & vbNewLine
            SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & vbNewLine
            SQL &= "ORDER BY INSP_ID" & vbNewLine

            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)

            DT.DefaultView.RowFilter = ""
            rptInspection.DataSource = DT
            rptInspection.DataBind()
            UpdateAllInspection() '----------------Remain
        End Set
    End Property

    Public Property WarningType() As EIR_BL.Warning
        Get
            If Not imgWarning.Visible Then Return EIR_BL.Warning.Normal
            Select Case imgWarning.ImageUrl
                Case "resources/images/icons/alert.gif"
                    Return EIR_BL.Warning.Alert
                Case "resources/images/icons/warning.gif"
                    Return EIR_BL.Warning.Warning
            End Select
            Return EIR_BL.Warning.Normal
        End Get
        Set(ByVal value As EIR_BL.Warning)
            Select Case value
                Case EIR_BL.Warning.Normal
                    imgWarning.Visible = False
                Case EIR_BL.Warning.Warning
                    imgWarning.Visible = True
                    imgWarning.ImageUrl = "resources/images/icons/warning.gif"
                Case EIR_BL.Warning.Alert
                    imgWarning.Visible = True
                    imgWarning.ImageUrl = "resources/images/icons/alert.gif"
            End Select
        End Set
    End Property

#End Region

    Public Sub ClearDetail()
        TAG_CODE = ""
        TAG_NAME = ""
        TAG_TYPE_ID = 0
        TAG_TYPE_NAME = 0
        REMAIN_PROBLEM = 0
        NEW_UPLOAD = 0
        TRACE_COUNT = 0
        WarningType = EIR_BL.Warning.Normal
    End Sub

    Public Sub BindHeader()

        ClearDetail()
        '---------------- Bind Tag Detail ---------------
        Dim SQL As String = "SELECT TAG_CODE,TAG_NAME,TAG.TAG_TYPE_ID,TAG_TYPE_NAME,COUNT(1) TotalINSP," & vbNewLine
        SQL &= "dbo.UDF_Calculate_Incomplete_Tag_Info_In_Report(" & RPT_Year & "," & RPT_No & "," & TAG_ID & ") Issue" & vbNewLine & vbNewLine
        SQL &= "FROM VW_ALL_ACTIVE_" & ViewState("TagClass") & "_TAG TAG" & vbNewLine
        SQL &= "LEFT JOIN (SELECT DISTINCT TAG_TYPE_ID,INSP_ID FROM MS_" & ViewState("TagClass") & "_TAG_Inspection ) INSP ON TAG.TAG_TYPE_ID=INSP.TAG_TYPE_ID" & vbNewLine
        SQL &= "WHERE TAG_ID=" & TAG_ID & vbNewLine
        SQL &= "GROUP BY TAG_CODE,TAG_NAME,TAG.TAG_TYPE_ID,TAG_TYPE_NAME" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        TAG_CODE = DT.Rows(0).Item("TAG_CODE")
        TAG_NAME = DT.Rows(0).Item("TAG_NAME")
        TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")
        TAG_TYPE_NAME = DT.Rows(0).Item("TAG_TYPE_NAME")

        If Not IsDBNull(DT.Rows(0).Item("Issue")) AndAlso DT.Rows(0).Item("Issue") > 0 Then
            imgWarning.ToolTip = "Please upload image and more detail for all required inspection point !!" & vbNewLine & "Click expand tag detail to see more information"
            WarningType = EIR_BL.Warning.Alert
        End If
        '------Total -------------
        lbl_Item.InnerHtml = FormatNumber(DT.Rows(0).Item("TotalINSP"), 0)

        '--------------------- Bind Current Status ------------------
        SQL = "SELECT INSP_ID,LAST_LEVEL,DETAIL_ID,CURRENT_COMPONENT,LAST_COMPONENT," & vbNewLine
        'SQL &= "CASE INSP_ID WHEN 12 THEN CURRENT_LEVEL-1 ELSE CURRENT_LEVEL END  CURRENT_LEVEL" & vbNewLine
        SQL &= "CASE WHEN INSP_ID = 12 THEN" & vbNewLine
        SQL &= "    CASE CURRENT_LEVEL WHEN 3 THEN 3 WHEN 2 THEN 1 WHEN 1 THEN 0 WHEN 0 THEN 0 ELSE CURRENT_LEVEL END" & vbNewLine
        SQL &= "ELSE" & vbNewLine
        SQL &= "    CURRENT_LEVEL " & vbNewLine
        SQL &= "END CURRENT_LEVEL" & vbNewLine
        SQL &= "FROM VW_REPORT_" & ViewState("TagClass") & "_DETAIL " & vbNewLine
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID


        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            REMAIN_PROBLEM = 0
            NEW_UPLOAD = 0
            TRACE_COUNT = 0
            Exit Sub
        End If

        '---------------------- Get Summary -----------------
        Dim HCol() As String = {"INSP_ID"}
        Dim AllCol() As String = {"INSP_ID", "LAST_LEVEL", "CURRENT_LEVEL", "DETAIL_ID"}
        '------Remain-------------
        DT.DefaultView.RowFilter = "LAST_LEVEL>0"
        lbl_Remain.InnerHtml = FormatNumber(DT.DefaultView.ToTable(True, AllCol).Copy.Rows.Count, 0)
        '------New ---------------
        DT.DefaultView.RowFilter = "(LAST_LEVEL IS NULL OR LAST_LEVEL=0) AND CURRENT_LEVEL>0"
        lbl_New.InnerHtml = FormatNumber(DT.DefaultView.ToTable(True, AllCol).Copy.Rows.Count, 0)
        '------Trace -------------
        DT.DefaultView.RowFilter = "LAST_LEVEL>0 AND CURRENT_LEVEL IS NOT NULL"
        lbl_TraceTag.InnerHtml = FormatNumber(DT.DefaultView.ToTable(True, AllCol).Copy.Rows.Count, 0)

        DT.Columns.Add("L", GetType(Integer), "IIf(CURRENT_LEVEL IS NULL,LAST_LEVEL,CURRENT_LEVEL)")
        Dim Cur_Level As Object = DT.Compute("MAX(L)", "")
        If Not IsDBNull(Cur_Level) Then
            LEVEL = Cur_Level
        Else
            LEVEL = EIR_BL.InspectionLevel.Normal
        End If

        GL_DialogUploadImage1.CloseDialog()
        UpdateAllInspection()
    End Sub

    Public Sub UpdateAllInspection()
        If IsExpand Then
            For Each Item As RepeaterItem In rptInspection.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim GLI As GL_Inspection = Item.FindControl("GLI")
                If GLI.REF_INSP_ID <> 0 Then
                    For Each REF As RepeaterItem In rptInspection.Items
                        If REF.ItemType <> ListItemType.Item And REF.ItemType <> ListItemType.AlternatingItem Then Continue For
                        Dim _ref As GL_Inspection = REF.FindControl("GLI")
                        If GLI.REF_INSP_ID = _ref.INSP_ID Then
                            GLI.Disabled = GLI.REF_STATUS_ID <> _ref.CURRENT_STATUS

                            '----------- Add Hidding If don't need to display ----------
                            If GLI.WarningType = EIR_BL.Warning.Normal Then
                                GLI.Visible = Not GLI.Disabled
                            End If
                            Exit For
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        TDTree.Attributes("onclick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lbl_Code.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Item.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Level.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_New.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Remain.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_TraceTag.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Type.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lblTagName.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
    End Sub

    Protected Sub Expand_Command(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpand.Click
        IsExpand = Not IsExpand
    End Sub

    Protected Sub rptInspection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInspection.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim GLI As GL_Inspection = e.Item.FindControl("GLI")

        GLI.RPT_Year = RPT_Year
        GLI.RPT_No = RPT_No
        GLI.TAG_ID = TAG_ID
        GLI.TAG_CLASS = Me.TAG_CLASS
        GLI.TAG_TYPE_ID = TAG_TYPE_ID
        GLI.INSP_ID = e.Item.DataItem("INSP_ID")

        If Not IsDBNull(e.Item.DataItem("REF_INSP_ID")) Then
            GLI.REF_INSP_ID = e.Item.DataItem("REF_INSP_ID")
        End If
        If Not IsDBNull(e.Item.DataItem("REF_STATUS_ID")) Then
            GLI.REF_STATUS_ID = e.Item.DataItem("REF_STATUS_ID")
        End If

        If Not IsDBNull(e.Item.DataItem("DETAIL_ID")) AndAlso e.Item.DataItem("DETAIL_ID") > 0 Then
            GLI.DETAIL_ID = e.Item.DataItem("DETAIL_ID")
        Else
            GLI.DETAIL_ID = 0
        End If

        GLI.BindData()

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click

        Select Case TAG_CLASS
            Case EIR_BL.Tag_Class.Stationary
                BL.Drop_RPT_ST_Detail(RPT_Year, RPT_No, TAG_ID)
                BL.Construct_ST_Report_Detail(Session("User_ID"), RPT_Year, RPT_No, TAG_ID)
            Case EIR_BL.Tag_Class.Rotating
                BL.Drop_RPT_RO_Detail(RPT_Year, RPT_No, TAG_ID)
                BL.Construct_RO_Report_Detail(Session("User_ID"), RPT_Year, RPT_No, TAG_ID)
        End Select

        BindHeader()
        IsExpand = IsExpand
    End Sub

    Protected Sub GL_DialogUploadImage1_UpdateCompleted(ByRef sender As GL_DialogUploadImage) Handles GL_DialogUploadImage1.UpdateCompleted
        BindHeader()
        IsExpand = IsExpand
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click
        Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
        GL_DialogUploadImage1.UNIQUE_POPUP_ID = UNIQUEKEY
        GL_DialogUploadImage1.TAG_CLASS = TAG_CLASS
        GL_DialogUploadImage1.ShowDialog(RPT_Year, RPT_No, TAG_ID)
    End Sub


End Class