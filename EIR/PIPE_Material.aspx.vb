﻿Imports System.Data.SqlClient
Public Class PIPE_Material
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ResetMaterial(Nothing, Nothing)
            ClearPanelSearch()
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub Search(sender As Object, e As EventArgs) Handles btnSearch.Click, txt_Search_Name.TextChanged, ddl_Search_Type.SelectedIndexChanged
        BindMaterial()
    End Sub

    Private Sub BindMaterial()

        Dim SQL As String = "  Select MAT.material_id,MAT.material_name,MAT.material_type_id,material_type_name,MAT.Active_Status,MAT.Updated_By,MAT.Updated_Date," & vbLf
        SQL &= " COUNT(TAG.TAG_ID) TotalTag" & vbLf
        SQL &= " FROM MS_MATERIAL MAT" & vbLf
        SQL &= " LEFT JOIN " & vbLf
        SQL &= "(SELECT DISTINCT MS_PIPE_POINT.TAG_ID, MS_PIPE_POINT.material_id" & vbLf
        SQL &= " 	FROM MS_PIPE_TAG " & vbLf
        SQL &= " 	INNER JOIN MS_PIPE_POINT On  MS_PIPE_TAG.TAG_ID=MS_PIPE_POINT.TAG_ID" & vbLf
        SQL &= " 	WHERE MS_PIPE_TAG.Active_Status=1" & vbLf
        SQL &= " ) TAG ON Mat.material_id=TAG.material_id" & vbLf
        SQL &= " INNER JOIN MS_MATERIAL_TYPE MAT_TYPE ON MAT.material_type_id=MAT_TYPE.material_type_id"

        Dim WHERE As String = ""
        If ddl_Search_Type.SelectedIndex > 0 Then
            WHERE &= " MAT.material_type_id=" & ddl_Search_Type.Items(ddl_Search_Type.SelectedIndex).Value & " AND "
        End If
        If txt_Search_Name.Text <> "" Then
            WHERE &= " (MAT.material_name Like '%" & txt_Search_Name.Text.Replace("'", "''") & "%') AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " GROUP BY MAT.material_id,MAT.material_name,MAT.material_type_id,material_type_name,MAT.Active_Status,MAT.Updated_By,MAT.Updated_Date" & vbLf
        SQL &= " ORDER BY MAT.material_name" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("PIPE_Material") = DT

        Navigation.SesssionSourceName = "PIPE_Material"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptMaterial
    End Sub

    Protected Sub rptMaterial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMaterial.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblMaterial As Label = e.Item.FindControl("lblMaterial")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfbDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblMaterial.Text = e.Item.DataItem("material_name").ToString
        lblType.Text = e.Item.DataItem("material_type_name").ToString

        If Not IsDBNull(e.Item.DataItem("TotalTag")) AndAlso e.Item.DataItem("TotalTag") > 0 Then
            lblPipe.Text = FormatNumber(e.Item.DataItem("TotalTag"), 0)
            cfbDelete.Enabled = False
            btnDelete.Visible = False
        Else
            lblPipe.Text = "-"
            cfbDelete.Enabled = True
            btnDelete.Visible = True
        End If

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status").ToString.ToUpper = "Y" Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        If Not IsDBNull(e.Item.DataItem("Updated_Date")) Then
            lblUpdateTime.Text = C.DateToString(e.Item.DataItem("Updated_Date"), "dd MMM yyyy")
        Else
            lblUpdateTime.Text = "-"
        End If
        btnEdit.Attributes("material_id") = e.Item.DataItem("material_id")

    End Sub

    Private Sub rptMaterial_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptMaterial.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim material_id As Integer = btnEdit.Attributes("material_id")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("material_id") = material_id
                '------------------------------------
                pnlListService.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT material_id,material_name,material_type_id,Active_Status,Updated_By,Updated_Date" & vbLf
                SQL &= " FROM MS_MATERIAL" & vbLf
                SQL &= " WHERE material_id = " & material_id & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Material Not Found"
                    pnlBindingError.Visible = True
                    BindMaterial()
                    Exit Sub
                End If

                txtMaterial.Text = DT.Rows(0).Item("material_name").ToString
                PIPE.BindDDl_Material_Type(ddlMaterialType, DT.Rows(0).Item("material_type_id"))
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status").ToString = "Y"
                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_MATERIAL Set active_status=CASE active_status WHEN 'Y' THEN 'N' ELSE 'Y' END" & vbNewLine
                SQL &= " WHERE material_id=" & material_id
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindMaterial()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try

            Case "Delete"
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM MS_MATERIAL WHERE material_id=" & material_id
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindMaterial()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try
        End Select

    End Sub

    Protected Sub ResetMaterial(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindMaterial()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListService.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtMaterial.Text = ""
        PIPE.BindDDl_Material_Type(ddlMaterialType)
        chkAvailable.Checked = True
        btnCreate.Visible = True
    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        txt_Search_Name.Text = ""
        PIPE.BindDDl_Material_Type(ddl_Search_Type)
        BindMaterial()
    End Sub


#End Region

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("material_id") = 0

        '-----------------------------------
        pnlListService.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtMaterial.Text = "" Then
            lblValidation.Text = "Please insert material"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddlMaterialType.SelectedIndex = 0 Then
            lblValidation.Text = "Please select material type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim material_id As Integer = lblUpdateMode.Attributes("material_id")

        Dim SQL As String = "SELECT * FROM MS_Material WHERE material_name='" & txtMaterial.Text.Replace("'", "''") & "' AND material_id<>" & material_id
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Material is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_MATERIAL WHERE material_id=" & material_id
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            material_id = GetNewMaterialID()
            DR("material_id") = material_id
            DR("created_by") = Session("USER_ID")
            DR("created_date") = Now
        Else
            DR = DT.Rows(0)
        End If

        DR("material_name") = txtMaterial.Text
        DR("material_type_id") = ddlMaterialType.Items(ddlMaterialType.SelectedIndex).Value
        If chkAvailable.Checked Then
            DR("Active_Status") = "Y"
        Else
            DR("Active_Status") = "N"
        End If

        DR("Updated_By") = Session("USER_ID")
        DR("Updated_Date") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetMaterial(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewMaterialID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(material_id),0)+1 FROM MS_MATERIAL "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function


End Class