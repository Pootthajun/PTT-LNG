﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Detail_Plant_PDMA
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim SQL As String = ""
            Dim DT_PLANT As New DataTable
            Dim DA As New SqlDataAdapter
            SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & Request.QueryString("PLANT_ID")
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT_PLANT)
            If DT_PLANT.Rows.Count > 0 Then
                lblPlantName.Text = DT_PLANT.Rows(0).Item("PLANT_Name").ToString
            End If
            lblBack.Text = "Back to see on this plant " & lblPlantName.Text
            lblMonth.Text = Request.QueryString("MONTH")
            lblYear.Text = Request.QueryString("YEAR")
            Select Case Request.QueryString("REPORT")
                Case EIR_BL.ReportName_Problem.NEW_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "New Problem Occurred"
                    lblReport.Text = EIR_BL.Dashboard.New_Problem_Occurred
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.PdMA_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                    BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
                Case EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.TOTAL_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_Total_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "Total Problem by Month"
                    lblReport.Text = EIR_BL.Dashboard.Total_Problem_by_month
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.PdMA_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                    BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
            End Select
        End If

    End Sub

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month As Integer, ByVal Year As Integer, ByVal Equipment As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        If Year > 2500 Then
            Year = Year - 543
        End If

        Dim DateFrom As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)
        Select Case lblReport.Text
            Case EIR_BL.Dashboard.New_Problem_Occurred
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)
            Case EIR_BL.Dashboard.Total_Problem_by_month
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate)
                DT_Dashboard.DefaultView.RowFilter = "ICLS_ID > 0"
                DT_Dashboard = DT_Dashboard.DefaultView.ToTable
        End Select
        DT_Dashboard.DefaultView.Sort = "TAG_CODE"
        DT_Dashboard = DT_Dashboard.DefaultView.ToTable

        Dim Col() As String = {"TAG_ID", "TAG_CODE", "RPT_YEAR", "RPT_NO", "RPT_Type"}
        Dim TAG As DataTable = DT_Dashboard.DefaultView.ToTable(True, Col)

        TAG.Columns.Add("Pwq")
        TAG.Columns.Add("Ins")
        TAG.Columns.Add("Pwc")
        TAG.Columns.Add("Sta")
        TAG.Columns.Add("Rot")
        TAG.Columns.Add("Air")

        For i As Integer = 0 To TAG.Rows.Count - 1
            TAG.Rows(i).Item("Pwq") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='Pwq'")
            TAG.Rows(i).Item("Ins") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='Ins'")
            TAG.Rows(i).Item("Pwc") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='Pwc'")
            TAG.Rows(i).Item("Sta") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='Sta'")
            TAG.Rows(i).Item("Rot") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='Rot'")
            TAG.Rows(i).Item("Air") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='Air'")
        Next

        rptData.DataSource = TAG
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblPwq As Label = e.Item.FindControl("lblPwq")
        Dim lblIns As Label = e.Item.FindControl("lblIns")
        Dim lblPwc As Label = e.Item.FindControl("lblPwc")
        Dim lblSta As Label = e.Item.FindControl("lblSta")
        Dim lblRot As Label = e.Item.FindControl("lblRot")
        Dim lblAir As Label = e.Item.FindControl("lblAir")

        Dim tr As HtmlTableRow = e.Item.FindControl("tr")

        lblTag.Text = e.Item.DataItem("TAG_Code").ToString
        lblType.Text = e.Item.DataItem("RPT_Type")

        Dim Tag_Type As EIR_BL.Report_Type
        If lblType.Text.ToUpper.Trim = "PDMA" Then
            Tag_Type = EIR_BL.Report_Type.PdMA_Report
        Else
            Tag_Type = EIR_BL.Report_Type.PdMA_MTap_Report
        End If

        tr.Attributes("onClick") = "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & Tag_Type & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');"

        If e.Item.DataItem("Pwq").ToString <> "" Then
            If e.Item.DataItem("Pwq") = 0 Then
                lblPwq.Text = "-"
            Else
                lblPwq.CssClass = "TextClassA"
                lblPwq.Text = "Abnormal"
                lblPwq.Style("cursor") = "pointer"
            End If
        End If
        If e.Item.DataItem("Ins").ToString <> "" Then
            If e.Item.DataItem("Ins") = 0 Then
                lblIns.Text = "-"
            Else
                lblIns.CssClass = "TextClassA"
                lblIns.Text = "Abnormal"
                lblIns.Style("cursor") = "pointer"
            End If
        End If
        If e.Item.DataItem("Pwc").ToString <> "" Then
            If e.Item.DataItem("Pwc") = 0 Then
                lblPwc.Text = "-"
            Else
                lblPwc.CssClass = "TextClassA"
                lblPwc.Text = "Abnormal"
                lblPwc.Style("cursor") = "pointer"
            End If
        End If
        If e.Item.DataItem("Sta").ToString <> "" Then
            If e.Item.DataItem("Sta") = 0 Then
                lblSta.Text = "-"
            Else
                lblSta.CssClass = "TextClassA"
                lblSta.Text = "Abnormal"
                lblSta.Style("cursor") = "pointer"
            End If
        End If
        If e.Item.DataItem("Rot").ToString <> "" Then
            If e.Item.DataItem("Rot") = 0 Then
                lblRot.Text = "-"
            Else
                lblRot.CssClass = "TextClassA"
                lblRot.Text = "Abnormal"
                lblRot.Style("cursor") = "pointer"
            End If
        End If
        If e.Item.DataItem("Air").ToString <> "" Then
            If e.Item.DataItem("Air") = 0 Then
                lblAir.Text = "-"
            Else
                lblAir.CssClass = "TextClassA"
                lblAir.Text = "Abnormal"
                lblAir.Style("cursor") = "pointer"
            End If
        End If
    End Sub

End Class