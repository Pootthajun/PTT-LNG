﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="SPH_Edit3.aspx.vb" Inherits="EIR.SPH_Edit3" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<!-- Page Head -->
       
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>Edit Spring Hangers & Supports Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
				
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" >Inspection Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabPicture" runat="server" CssClass="default-tab current">Photography Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader" ></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								    | <asp:Label ID="lbl_Spring" runat="server" Text="Spring" CssClass="EditReportHeader" ></asp:Label> 
                                    &nbsp;Spring(s) 
								</p>	
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
							        </asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all vibration information for this report permanently?">
                                      </cc1:ConfirmButtonExtender>
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
								  
					        </ul>
								
							  
                                <table>
                                  <thead>
                                    <tr>
                                      <th colspan="8" align="center">Photography Report</th>
                                    </tr>
									</thead>
								 </table>
								 
                                  <asp:Repeater ID="rptINSP" runat="server">
                                  <ItemTemplate>
                                         <table border="0" cellspacing="0" cellpadding="5" style="width:650px;">
                                              <tr>
                                                <td  colspan="2" style="font-weight:bold;"><asp:Label ID="lblNo" runat="server"></asp:Label>. Tag.No.<asp:Label ID="lblName" runat="server"></asp:Label> <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                <asp:Label ID="lblCorrosion" runat="server"></asp:Label>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td><asp:ImageButton ID="imgLeft" runat="server" Width="300px" Height="300px" ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" CommandName="Upload" CommandArgument="1" ToolTip="Click to edit picture" /></td>
                                                <td><asp:ImageButton ID="imgRight" runat="server" Width="300px" Height="300px" ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" CommandName="Upload"  CommandArgument="2" ToolTip="Click to edit picture" /></td>
                                              </tr>
                                              <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:button ID="btnSaveImage1" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;" CommandName="SaveImage" CommandArgument="1" />
                                                    <asp:button ID="btnSaveImage2" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;" CommandName="SaveImage" CommandArgument="2" />
                                                </td>
                                              </tr>                                            
                                            </table>
                                  </ItemTemplate>
                                  </asp:Repeater>
                       
								 
						
							<p align="right">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>
								
							</fieldset>
				    </div>
				 
		       	 <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
            
</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>