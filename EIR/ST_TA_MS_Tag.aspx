﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_MS_Tag.aspx.vb" Inherits="EIR.ST_TA_MS_Tag" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register Src="~/UC_Pipe_Tag_Info.ascx" TagPrefix="uc1" TagName="UC_Pipe_Tag_Info" %>

<%@ Register Src="MS_ST_DialogAbsorber_Spec.ascx" TagName="MS_ST_DialogAbsorber_Spec" TagPrefix="uc4" %>
<%@ Register Src="MS_ST_DialogColumn_Spec.ascx" TagName="MS_ST_DialogColumn_Spec" TagPrefix="uc5" %>
<%@ Register Src="MS_ST_DialogFilter_Spec.ascx" TagName="MS_ST_DialogFilter_Spec" TagPrefix="uc6" %>
<%@ Register Src="MS_ST_DialogDrum_Spec.ascx" TagName="MS_ST_DialogDrum_Spec" TagPrefix="uc7" %>
<%@ Register Src="MS_ST_DialogHeat_Exchnager_Spec.ascx" TagName="MS_ST_DialogHeat_Exchnager_Spec" TagPrefix="uc8" %>
<%@ Register Src="MS_ST_Dialog_Spec.ascx" TagName="MS_ST_Dialog_Spec" TagPrefix="uc9" %>










<asp:Content ID="ContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <link rel="stylesheet" href="resources/css/StyleTurnaround.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
			
			<!-- Page Head -->
			<h2>Tag Setting </h2>
			
						
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
				
				    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_Tag_Type" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
    				
    				<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Area" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
				            <div class="clear"></div>
                  </div>
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListTag" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Tag-No </a></th>
                        <th><a href="#">Tag Name </a></th>
                        <th><a href="#">Equipement-Type</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Updated</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptTag" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                        <asp:Label ID="lblTo_Table" runat="server" Visible="false"></asp:Label>
                                    </td>
                                    <td><asp:Label ID="lblTagType" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                          <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                          
                                    </td>
                                  </tr>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="6">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				    </asp:Panel>
				    
				 <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
			    
			    <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Tag </h3>
                    <asp:Label ID="lblEditTo_Table" runat="server" Visible="false"></asp:Label>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                   
                    
                        <asp:Panel ID="pnlHind" Visible ="false"  runat="server">
                    <p>
                      <label class="column-left" style="width:120px;" >Define to Plant : </label>
                        <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                             ID="ddl_Edit_Plant" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </p>                    
                    <p>
                      <label class="column-left" style="width:120px;" >Define to Route : </label>
                        <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                             ID="ddl_Edit_Route" runat="server">
                        </asp:DropDownList>
                    </p>
                    
                    <p>
                      <label class="column-left" style="width:120px;" >Tag No : </label>
                     	 <asp:DropDownList CssClass="select" 
                          ID="ddl_Edit_Area" runat="server">
                        </asp:DropDownList>
						<asp:DropDownList CssClass="select"
                          ID="ddl_Edit_Process" runat="server">
                        </asp:DropDownList>
						<asp:TextBox runat="server" ID="txtTagNo" CssClass="text-input " 
                            style="Width:100px;"></asp:TextBox>
						
						
				      <!-- End .clear -->
                  </p>
				    <p>
                      <label class="column-left" style="width:120px;" >Tag Name : </label>
                      <asp:TextBox runat="server" ID="txtTagName" CssClass="text-input small-input " Width="400px" MaxLength="100"></asp:TextBox>
			          <!-- End .clear -->
                    </p>
                    
                     <p>
                       <label class="column-left" style="width:120px;" >Equipement-Type: </label>
                        <asp:DropDownList CssClass="select" ID="ddl_Edit_Type" runat="server">
                      </asp:DropDownList>
                     </p>                                         
                    
					<p>
				      <label style="width:300px;" >Description: </label>
				        <asp:TextBox runat="server" ID="txtDesc" TextMode="MultiLine" CssClass="text-input" Width="100%" Height="80px" MaxLength="500"></asp:TextBox>
					</p>


                            </asp:Panel>
                    <p>
                        <uc4:MS_ST_DialogAbsorber_Spec ID="DialogAbsorber" runat="server" />
                            <uc5:MS_ST_DialogColumn_Spec ID="DialogColumn" runat="server" />
                            <uc6:MS_ST_DialogFilter_Spec ID="DialogFilter" runat="server" />
                            <uc7:MS_ST_DialogDrum_Spec ID="DialogDrum" runat="server" />
                            <uc8:MS_ST_DialogHeat_Exchnager_Spec ID="DialogHeat_Exchnager" runat="server" />

                            <uc9:MS_ST_Dialog_Spec ID="Dialog_Spec" runat="server" />

                    </p>

					<p>
                      <label class="column-left" style="width:120px;" >Available : </label>
                        &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
				    </p>
				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset>

                






			    </asp:Panel>



                </div>
                <!-- End #tab1 -->
					
				
			  </div> <!-- End .content-box-content -->
				
		  </div>
		  
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>

