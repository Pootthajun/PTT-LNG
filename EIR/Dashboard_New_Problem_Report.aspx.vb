﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_New_Problem_Report
    Inherits System.Web.UI.Page

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim Month_F As Integer = 0
            Dim Month_T As Integer = 0
            Dim Year_F As Integer = 0
            Dim Year_T As Integer = 0
            Dim Equipment As EIR_BL.Report_Type

            If Request.QueryString("EQUIPMENT") = Nothing Then
                Month_F = 1
                Month_T = Date.Now.Month
                Year_F = Date.Now.Year
                Year_T = Date.Now.Year
                Equipment = EIR_BL.Report_Type.All
            Else
                Month_F = Request.QueryString("MONTH_F")
                Month_T = Request.QueryString("MONTH_T")
                Year_F = Request.QueryString("YEAR_F")
                Year_T = Request.QueryString("YEAR_T")
                Equipment = Request.QueryString("EQUIPMENT")
            End If

            Dashboard.BindDDlMonthEng(ddl_Month_F, Month_F)
            Dashboard.BindDDlMonthEng(ddl_Month_T, Month_T)
            Dashboard.BindDDlYear(ddl_Year_F, Year_F)
            Dashboard.BindDDlYear(ddl_Year_T, Year_T)
            Dashboard.BindDDlEquipment(ddl_Equipment, Equipment)
            BindData()
        End If
    End Sub

    Private Sub BindData()
        UC_Dashboard_New_Problem_Report.BindData(ddl_Month_F.SelectedValue, ddl_Month_T.SelectedValue, ddl_Year_F.SelectedValue, ddl_Year_T.SelectedValue, ddl_Equipment.SelectedValue)
    End Sub

    Protected Sub ALL_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Month_F.SelectedIndexChanged, ddl_Month_T.SelectedIndexChanged, ddl_Year_F.SelectedIndexChanged, ddl_Year_T.SelectedIndexChanged, ddl_Equipment.SelectedIndexChanged
        BindData()
    End Sub

End Class