﻿Imports System.Data
Public Class Dashboard_Annual_Progress_Dialog
    Inherits System.Web.UI.Page
    Dim Dashboard As New DashboardClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Year As String = CInt(Request.QueryString("Year"))
            Dim Equipment As String = Request.QueryString("Equipment")
            Dim Plant As String = Request.QueryString("Plant")

            lblHeader.Text = "Annual Inspection  Progress for " & vbNewLine & vbNewLine & Dashboard.FindEquipmentName(Equipment) & " Equipment   Year  " & CInt(Year) + 543 & vbNewLine

            Dim dt As New DataTable
            dt = Dashboard.AnnualProgressDetail(Year, Equipment, Plant) 'ข้อมูลดิบ

            Dim dt_data As New DataTable
            dt_data.Columns.Add("PeriodEnd")
            dt_data.Columns.Add("Report")
            dt_data.Columns.Add("Progess", GetType(Double))


            Dim Complete As Int32 = 0
            For i As Int32 = 0 To dt.Rows.Count - 1
                Dim dr As DataRow
                dr = dt_data.NewRow

                dr("PeriodEnd") = dt.Rows(i).Item("RPT_Period_End").ToString.Substring(6, 2) & "-" & dt.Rows(i).Item("RPT_Period_End").ToString.Substring(4, 2) & "-" & dt.Rows(i).Item("RPT_Period_End").ToString.Substring(0, 4)
                If dt.Rows(i).Item("RPT_Period_End").ToString <> "" Then
                    Complete = Complete + 1
                End If
                dr("Report") = CStr(Complete) & "/" & CInt(i + 1)
                dr("Progess") = (Complete * 100) / i + 1
                dt_data.Rows.Add(dr)
            Next


            rptData.DataSource = dt_data
            rptData.DataBind()

        End If
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblPeriodEndDate As Label = e.Item.FindControl("lblPeriodEndDate")
        Dim lblActualDate As Label = e.Item.FindControl("lblActualDate")

        Dim PeriodEndDate As String = e.Item.DataItem("RPT_Period_End").ToString
        Dim ActualDate As String = e.Item.DataItem("RPT_ANL_Date").ToString

        If PeriodEndDate <> "" Then
            PeriodEndDate = PeriodEndDate.Substring(6, 2) & "-" & PeriodEndDate.Substring(4, 2) & "-" & PeriodEndDate.Substring(0, 4)
        End If
        If ActualDate <> "" Then
            ActualDate = ActualDate.Substring(6, 2) & "-" & ActualDate.Substring(4, 2) & "-" & ActualDate.Substring(0, 4)
        End If

        lblPeriodEndDate.Text = PeriodEndDate
        lblActualDate.Text = ActualDate
    End Sub


End Class