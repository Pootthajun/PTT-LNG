﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_LawShowProcess.ascx.vb" Inherits="EIR.UC_LawShowProcess" %>

<style>
    .processBGImage {
        background-size:100px;
        background-repeat:no-repeat;
        width:100px;
        height:100px;
    }
</style>
<table  cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="background-color:White;">
    <tr>
        <asp:Repeater ID="rptShowProcess" runat="server" >
            <ItemTemplate>   
                <asp:Label ID="lblProcess" runat="server"></asp:Label>
            </ItemTemplate>                                                                                                                                            
        </asp:Repeater>   
    </tr>
</table>