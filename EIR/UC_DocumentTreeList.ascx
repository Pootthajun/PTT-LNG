﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_DocumentTreeList.ascx.vb" Inherits="EIR.UC_DocumentTreeList" %>
 
<%@ Register Src="UC_DocumentTreeNode.ascx" TagPrefix="UC" TagName="UC_DocumentTreeNode" %>

<table>
        <thead>
            <tr>
            <th><a href="javascript:;">Documents</a></th>
            <th><a href="javascript:;">Status</a></th>
            <th><a href="javascript:;">Notice Date</a></th>
            <th><a href="javascript:;">Critical Date</a></th>
            <th><a href="javascript:;">Last Update</a></th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rpt" runat="server">
                <ItemTemplate>
                    <UC:UC_DocumentTreeNode runat="server" ID="DocItem" />
                </ItemTemplate>
                <FooterTemplate></FooterTemplate>
            </asp:Repeater>
        </tbody>                    
</table>
<div class="clear"></div>
  