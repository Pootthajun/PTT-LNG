﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="SPH_Edit2.aspx.vb" Inherits="EIR.SPH_Edit2" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<!-- Page Head -->
       
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>Edit Spring Hangers & Supports Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <cc1:AsyncFileUpload ID="ful1" runat="server" CssClass="button" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" CssClass="default-tab current">Inspection Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabPicture" runat="server">Photography Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader" ></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								    | <asp:Label ID="lbl_Spring" runat="server" Text="Spring" CssClass="EditReportHeader" ></asp:Label> 
                                    &nbsp;Spring(s) 
								</p>	
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
							        </asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all vibration information for this report permanently?">
                                      </cc1:ConfirmButtonExtender>
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
								  
								 
					        </ul>
								
							  
                                <%--<table>
                                  <thead>
                                    <tr>
                                      <th colspan="8" align="center">Inspection Report
									  <br>
									  
									  </th>
                                    </tr>
									</thead>
								 </table>
								 <table width="800" border="0" cellpadding="5" cellspacing="0" class="table_vibration">
                                  <tr>
                                    <td rowspan="2" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">No.</td>
                                    <td rowspan="2" style="width:120px; text-align:center !important; font-weight:bold; font-size:10px;">Pipe No.</td>
                                    <td rowspan="2" style="width:120px; text-align:center !important; font-weight:bold; font-size:10px;">Spring No.</td>
                                    <td rowspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Size (inch)</td>
                                    <td colspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Std.Load (Kg.)</td>
                                    <td colspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Std.Travel Length (mm)</td>
                                    <td rowspan="2" style="width:150px; text-align:center !important; font-weight:bold; font-size:10px;">Inspected Travel Level (mm)</td>
                                    <td rowspan="2" style="width:100px; text-align:center !important; font-weight:bold; font-size:10px;">Inspected Results</td>
                                    <td rowspan="2" style="width:150px; text-align:center !important; font-weight:bold; font-size:10px;">Class  Corrosion</td>                                    
                                    <td rowspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Possible Cause/Problem
                                    <td rowspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Comment
                                  </tr>
                                  <tr>
                                    <td style="width:80px; text-align:center !important; font-weight:bold; font-size:10px;">Instll.</td>
                                    <td style="width:80px; text-align:center !important; font-weight:bold; font-size:10px;">Oper.</td>
                                    <td style="width:80px; text-align:center !important; font-weight:bold; font-size:10px;">Cold</td>
                                    <td style="width:80px; text-align:center !important; font-weight:bold; font-size:10px;">Hot</td>
                                  </tr>
                                  <asp:Repeater ID="rptINSP" runat="server">
                                  <ItemTemplate>
                                  <tr>
                                    <td style="text-align:center !important;"><asp:label ID="lblNo" runat="server" Width="100%" Height="80%"></asp:label>
                                    <asp:button ID="btnUpdate" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;" CommandName="Save" />
                                    </td>
                                    <td style="text-align:center !important;"><asp:label ID="lblPipe" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblSpring" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblSize" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblInst" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblOper" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblCold" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblHot" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtINSP" MaxLength="20" runat="server" Width="100%" Height="20px" BorderStyle="None" BorderWidth="0px" Style="text-align:center"> </asp:TextBox></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblResult" runat="server" Width="100%" Height="20px"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtCorrosion" MaxLength="50" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtCause" MaxLength="1000" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtComment" MaxLength="1000" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                  </tr>
                                  </ItemTemplate>
                                  </asp:Repeater>
                                  
                                   <tfoot>
                                    <tr>
                                      <td colspan="10">&nbsp;</td>
                                    </tr>
                                  </tfoot>
                                </table>--%>
                                                 
                                <table width="800" border="0" cellpadding="5" cellspacing="0" class="table_vibration">
                                  <tr>
                                    <td rowspan="2" style="width:30px; text-align:center !important; font-weight:bold; font-size:10px;">No.</td>
                                    <td rowspan="2" style="width:120px; text-align:center !important; font-weight:bold; font-size:10px;">Pipe No.</td>
                                    <td rowspan="2" style="width:120px; text-align:center !important; font-weight:bold; font-size:10px;">Spring No.</td>
                                    <td colspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Std.Travel Length (mm)</td>
                                    <td rowspan="2" style="width:100px; text-align:center !important; font-weight:bold; font-size:10px;">Inspected Travel Level (mm)</td>
                                    <td rowspan="2" style="width:80px; text-align:center !important; font-weight:bold; font-size:10px;">Inspected Results</td>
                                    <td rowspan="2" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">Class  Corrosion</td>                                    
                                    <td rowspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Possible Cause/Problem
                                    <td rowspan="2" style="text-align:center !important; font-weight:bold; font-size:10px;">Comment
                                  </tr>
                                  <tr>
                                    <td style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">Cold</td>
                                    <td style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">Hot</td>
                                  </tr>
                                  
                                  
                                  <asp:Repeater ID="rptINSP" runat="server">
                                  <ItemTemplate>
                                  <tr>
                                    <td style="text-align:center !important;"><asp:label ID="lblNo" runat="server" Width="100%" Height="80%"></asp:label>
                                    <asp:button ID="btnUpdate" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;" CommandName="Save" />
                                    </td>
                                    <td style="text-align:center !important;"><asp:label ID="lblPipe" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblSpring" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblCold" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblHot" runat="server" Width="100%" Height="100%"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtINSP" MaxLength="20" runat="server" Width="100%" Height="20px" BorderStyle="None" BorderWidth="0px" Style="text-align:center" ToolTip="Possible entry : numeric, ส้ม, สีส้ม, ฟ้า, สีฟ้า, เขียว, สีเขียว, in-range, out-range"> </asp:TextBox></td>
                                    <td style="text-align:center !important;"><asp:label ID="lblResult" runat="server" Width="100%" Height="20px"></asp:label></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtCorrosion" MaxLength="50" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtCause" MaxLength="1000" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                    <td style="text-align:center !important;"><asp:TextBox ID="txtComment" MaxLength="1000" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                  </tr>
                                  </ItemTemplate>
                                  </asp:Repeater>                                 
                                  
                                   <tfoot>
                                    <tr>
                                      <td colspan="10">&nbsp;</td>
                                    </tr>
                                  </tfoot>
                                </table>								 
						
							<p align="right">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>
								
							</fieldset>
				    </div>
				 
		       	 <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
            
</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>