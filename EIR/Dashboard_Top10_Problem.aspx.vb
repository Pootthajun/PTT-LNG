﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Dashboard_Top10_Problem
    Inherits System.Web.UI.Page

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim Month_F As Integer = 0
            Dim Month_T As Integer = 0
            Dim Year_F As Integer = 0
            Dim Year_T As Integer = 0
            Dim Equipment As EIR_BL.Report_Type

            If Request.QueryString("EQUIPMENT") = Nothing Then
                Month_F = 1
                Month_T = Date.Now.Month
                Year_F = Date.Now.Year
                Year_T = Date.Now.Year
                Equipment = EIR_BL.Report_Type.All
            Else
                Month_F = Request.QueryString("MONTH_F")
                Month_T = Request.QueryString("MONTH_T")
                Year_F = Request.QueryString("YEAR_F")
                Year_T = Request.QueryString("YEAR_T")
                Equipment = Request.QueryString("EQUIPMENT")
            End If

            Dashboard.BindDDlMonthEng(ddl_Month_F, Month_F)
            Dashboard.BindDDlMonthEng(ddl_Month_T, Month_T)
            Dashboard.BindDDlYear(ddl_Year_F, Year_F)
            Dashboard.BindDDlYear(ddl_Year_T, Year_T)
            BindData()
        End If
    End Sub

    Private Sub BindData()
        With Dashboard_Top10_Problem1
            .Month_From = ddl_Month_F.SelectedValue
            .Month_To = ddl_Month_T.SelectedValue
            .Year_From = ddl_Year_F.SelectedValue
            .Year_To = ddl_Year_T.SelectedValue
            .Equipment = ddl_Equipment.SelectedValue
            .IsPrintPage = False
            .BindData()
        End With
    End Sub

    Protected Sub ALL_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Month_F.SelectedIndexChanged, ddl_Month_T.SelectedIndexChanged, ddl_Year_F.SelectedIndexChanged, ddl_Year_T.SelectedIndexChanged, ddl_Equipment.SelectedIndexChanged
        BindData()
    End Sub

End Class