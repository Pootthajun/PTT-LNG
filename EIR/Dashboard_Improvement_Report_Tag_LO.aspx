﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Improvement_Report_Tag_LO.aspx.vb" Inherits="EIR.Dashboard_Improvement_Report_Tag_LO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Problem Improved By Plant</h2>
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
  <tr>
	<td>          
        <asp:LinkButton ID="lblBack" runat="server" Text="Back to see all month"></asp:LinkButton>
    </td>
  </tr>
  <tr>
    <td>
         <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #efefef; width:600px;">
              <tr>
	            <td style="background-color:#00cc33; text-align:center; color:White; font-weight:bold;" colspan="5">          
                    <asp:Label ID="lblHead" runat="server" ></asp:Label>
                </td>
              </tr>
              <tr>
                  <td style="text-align:center; background-color:#003366; color:White;">
                      Tag</td> 
                  <td style="text-align:center; background-color:#003366; color:White;">
                      Tan</td>
                  <td style="text-align:center; background-color:#003366; color:White;">
                      Oxd/Anti</td>
                  <td style="text-align:center; background-color:#003366; color:White;">
                      Varnish</td>
                  <td style="text-align:center; background-color:#003366; color:White;">
                      Water</td>
              </tr>
              <asp:Repeater ID="rptData" runat="server">
                <ItemTemplate>
                    <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                        <td style="border-left:1px solid #eeeeee; text-align:left;">
                            <asp:Label ID="lblTag" Text="-" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td style="text-align:center; color:Green">
                            <asp:Label ID="lblTAN" runat="server" Text="-"></asp:Label>
                        </td>
                        <td style="text-align:center; color:Green">
                            <asp:Label ID="lblOx" runat="server" Text="-"></asp:Label>
                        </td>
                        <td style="text-align:center; color:Green">
                            <asp:Label ID="lblVarnish" runat="server" Text="-"></asp:Label>
                        </td>
                        <td style="text-align:center; color:Green">
                            <asp:Label ID="lblWater" runat="server" Text="-"></asp:Label>
                        </td>    
                    </tr>
                </ItemTemplate>
              </asp:Repeater>
          </table>
    </td>
  </tr>
</table>
</asp:Content>