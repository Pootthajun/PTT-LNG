﻿Imports System.Drawing
Public Class Render_SPH_Picture
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load
        Try
            Dim IMG As Byte() = BL.Get_SPH_Image(Request.QueryString("D"), Request.QueryString("S"))
            If Not IsNothing(IMG) Then
                Response.Clear()
                Response.BinaryWrite(IMG)
            Else
                Response.Redirect("resources/images/Sample_40.png", True)
            End If
        Catch ex As Exception
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End Try
    End Sub

End Class