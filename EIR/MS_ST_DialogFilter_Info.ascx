﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_DialogFilter_Info.ascx.vb" Inherits="EIR.MS_ST_DialogFilter_Info" %>
<table cellpadding="0" cellspacing="0" >
            <tbody>
                <tr>					            
					        
		<td class="propertyCaption">Initial Year</td>					        
		<td colspan="2"><asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td class="propertyCaption">Pneum Test Pressure</td>
            <td>
                <asp:TextBox ID="txt_PNEUM_TEST_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
	</tr>


        <tr>

            <td class="propertyCaption">Design Pressure</td>
            <td>
                <asp:TextBox ID="txt_DESIGN_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
           <td class="propertyCaption">Operating Pressure</td>
            <td>
                <asp:TextBox ID="txt_OPERATING_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
        </tr>
        <tr>
            <td class="propertyCaption">Design Temperature</td>
            <td>
                <asp:TextBox ID="txt_DESIGN_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td style="text-align: center;">° C</td>
            <td class="propertyCaption">Operating Temperature </td>
            <td>
                <asp:TextBox ID="txt_OPERATING_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td style="text-align: center;">° C</td>
        </tr>
        <tr>
            <td class="propertyCaption">Hydro Test Pressure </td>
            <td>
                <asp:TextBox ID="txt_HYDRO_TEST_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">Bar.g</td>
            <td class="propertyCaption">Corrosion Allowance</td>            
            <td>
                <asp:TextBox ID="txt_CORROSION_ALLOWANCE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align: center;">mm</td>
        </tr>
            </tbody>
    </table>