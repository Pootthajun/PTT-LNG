﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization

Public Class GL_Dialog_ST_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim BIZ As New EIR_Consequence
    Dim C As New Converter

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Public Property TAG_ID() As Integer
        Get
            If Not IsNothing(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Close", "alert('Please login and try again'); window.close();", True)
            Exit Sub
        End If

        Threading.Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")

        If Not IsPostBack Then
            If Request.QueryString("TAG_ID") <> "" AndAlso IsNumeric(Request.QueryString("TAG_ID")) Then
                TAG_ID = Request.QueryString("TAG_ID")
            Else
                TAG_ID = 0
            End If
            BindTag()
            DialogDetail.CloseDialog()
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Focus", "window.focus();", True)
    End Sub

    Private Sub BindTag()
        Dim SQL As String = ""
        SQL &= "SELECT * FROM VW_ALL_ACTIVE_ST_TAG WHERE TAG_ID=" & TAG_ID & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        If Not IsDBNull(DT.Rows(0).Item("TAG_CODE")) Then
            lblTagCode.Text = DT.Rows(0).Item("TAG_CODE")
        Else
            lblTagCode.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("TAG_NAME")) Then
            lblTagName.Text = DT.Rows(0).Item("TAG_NAME")
        Else
            lblTagName.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("ROUTE_CODE")) Then
            lblRoute.Text = DT.Rows(0).Item("ROUTE_CODE")
        Else
            lblRoute.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("PROC_CODE")) Then
            lblProcess.Text = DT.Rows(0).Item("PROC_CODE")
            If Not IsDBNull(DT.Rows(0).Item("PROC_NAME")) AndAlso DT.Rows(0).Item("PROC_NAME") <> "-" Then
                lblProcess.Text &= "(" & DT.Rows(0).Item("PROC_NAME") & ")"
            End If
        Else
            lblProcess.Text = ""
        End If

        'lblRoute

        If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_Name")) Then
            lblType.Text = DT.Rows(0).Item("TAG_TYPE_Name")
        Else
            lblType.Text = ""
        End If

        lblLocation.Text = ""
        If Not IsDBNull(DT.Rows(0).Item("PLANT_Name")) Then
            lblLocation.Text &= DT.Rows(0).Item("PLANT_Name") & " "
        End If
        If Not IsDBNull(DT.Rows(0).Item("ROUTE_Name")) Then
            lblLocation.Text &= DT.Rows(0).Item("ROUTE_Name") & " "
        End If
        If Not IsDBNull(DT.Rows(0).Item("AREA_Name")) Then
            lblLocation.Text &= " area " & DT.Rows(0).Item("AREA_Name") & " "
        End If

        '------------------ Get All History -------------------
        'Dim Filter As String = ""
        'If txt_Prob_Start.Text <> "" Then
        '    Dim d As DateTime = C.StringToDate(txt_Prob_Start.Text, txt_Prob_Start_CalendarExtender.Format)
        '    Filter &= " RPT_Period_End>='" & d.Year & "-" & d.Month.ToString.PadLeft(2, "0") & "-" & d.Day.ToString.PadLeft(2, "0") & "' AND "
        'End If
        'If txt_Prob_End.Text <> "" Then
        '    Dim d As DateTime = C.StringToDate(txt_Prob_End.Text, txt_Prob_End_CalendarExtender.Format)
        '    Filter &= " RPT_Period_Start<='" & d.Year & "-" & d.Month.ToString.PadLeft(2, "0") & "-" & d.Day.ToString.PadLeft(2, "0") & "' AND "
        'End If

        SQL = ""
        SQL &= "SELECT VW_REPORT_ST_DETAIL.RPT_Year,VW_REPORT_ST_DETAIL.RPT_No, VW_REPORT_ST_DETAIL.RPT_CODE,Detail_ID" & vbNewLine
        SQL &= ",INSP_ID,INSP_Name" & vbNewLine
        SQL &= ",CURRENT_STATUS_ID" & vbNewLine
        SQL &= ",CURRENT_STATUS_Name" & vbNewLine
        SQL &= ",CURRENT_LEVEL" & vbNewLine
        SQL &= ",CURRENT_LEVEL_Desc" & vbNewLine
        SQL &= ",CURRENT_BIZ_LEVEL, CURRENT_BIZ_LEVEL_Name, CURRENT_MA_DAY" & vbNewLine '----------- Add Biz Consequence AND MA_Day
        SQL &= ",CURRENT_SAFETY_Name" & vbNewLine '----------- Add Safety Consequence
        SQL &= ",LAST_BIZ_LEVEL, LAST_BIZ_LEVEL_Name, LAST_MA_DAY" & vbNewLine '----------- Add Biz Consequence AND MA_Day
        SQL &= ",CURRENT_PROB_Detail" & vbNewLine
        SQL &= ",LAST_STATUS_Name" & vbNewLine
        SQL &= ",LAST_LEVEL" & vbNewLine
        SQL &= ",LAST_LEVEL_Desc" & vbNewLine
        SQL &= ",RPT_Period_Start" & vbNewLine
        SQL &= ",ISNULL(MS_User.User_Prefix,'') + ISNULL(MS_User.USER_Name,'') + ' ' + ISNULL(MS_User.User_Surname,'') USER_Full_Name" & vbNewLine
        SQL &= "FROM VW_REPORT_ST_DETAIL INNER JOIN VW_REPORT_ST_HEADER ON VW_REPORT_ST_DETAIL.RPT_CODE=VW_REPORT_ST_HEADER.RPT_CODE" & vbNewLine
        SQL &= "LEFT JOIN MS_User ON VW_REPORT_ST_HEADER.Created_By=MS_User.USER_ID " & vbNewLine
        SQL &= "WHERE TAG_ID =" & TAG_ID & vbNewLine
        SQL &= "AND Detail_ID IS NOT NULL AND (VW_REPORT_ST_DETAIL.CURRENT_LEVEL IS NOT NULL OR VW_REPORT_ST_DETAIL.LAST_LEVEL IS NOT NULL)" & vbNewLine
        SQL &= "ORDER BY VW_REPORT_ST_DETAIL.RPT_CODE DESC,INSP_ID" & vbNewLine

        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)

        DA.Fill(DT)
        DT.DefaultView.Sort = "RPT_Year DESC,RPT_No DESC,RPT_Period_Start DESC"
        DT = DT.DefaultView.ToTable
        rptHistory.DataSource = DT
        rptHistory.DataBind()

        '--------------------- Get Current Status ------------------
        lblCurrent.Text = "Normal"
        lblCurrent.CssClass = BL.Get_Inspection_Css_Text_By_Level(0)
        If DT.Rows.Count > 0 Then
            DT.DefaultView.Sort = "RPT_Year DESC,RPT_No DESC"
            Dim RPT_Code As String = ""
            If Not IsDBNull(DT.Rows(0).Item("RPT_CODE")) Then
                RPT_Code = DT.Rows(0).Item("RPT_CODE")
            End If
            DT.Columns.Add("L", GetType(Integer), "IIF(CURRENT_LEVEL IS NOT NULL,CURRENT_LEVEL,LAST_LEVEL)")
            Dim LEVEL As Object = DT.Compute("MAX(L)", "RPT_Code='" & RPT_Code & "'")
            If Not IsDBNull(LEVEL) Then
                lblCurrent.CssClass = BL.Get_Inspection_Css_Text_By_Level(LEVEL)
                lblCurrent.Text = lblCurrent.CssClass.Replace("Text", "")
            End If
        End If

    End Sub

    Dim LastReport As String = ""
    Dim LastDate As String = ""

    Protected Sub rptHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptHistory.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tdDate As HtmlTableCell = e.Item.FindControl("tdDate")
        Dim tdReport As HtmlTableCell = e.Item.FindControl("tdReport")
        Dim tdLevel As HtmlTableCell = e.Item.FindControl("tdLevel")
        Dim tdSeverity As HtmlTableCell = e.Item.FindControl("tdSeverity")
        Dim tdBiz As HtmlTableCell = e.Item.FindControl("tdBiz")
        Dim tdSafety As HtmlTableCell = e.Item.FindControl("tdSafety")
        Dim tdMADay As HtmlTableCell = e.Item.FindControl("tdMADay")
        Dim tdDetail As HtmlTableCell = e.Item.FindControl("tdDetail")
        Dim tdBy As HtmlTableCell = e.Item.FindControl("tdBy")
        Dim btnView As Button = e.Item.FindControl("btnView")

        Dim lblDate As HtmlAnchor = e.Item.FindControl("lblDate")
        Dim lblReport As HtmlAnchor = e.Item.FindControl("lblReport")
        Dim lblBy As Label = e.Item.FindControl("lblBy")
        Dim lblDetail As Label = e.Item.FindControl("lblDetail")

        Dim ReportDate As String = C.DateToString(e.Item.DataItem("RPT_Period_Start"), "dd MMM yyyy")
        If LastDate <> ReportDate Then
            lblDate.InnerHtml = ReportDate
            LastDate = ReportDate
        End If
        If e.Item.DataItem("RPT_CODE") <> LastReport Then
            lblReport.InnerHtml = e.Item.DataItem("RPT_CODE")
            LastReport = e.Item.DataItem("RPT_CODE")
        End If

        lblBy.Text = e.Item.DataItem("USER_Full_Name").ToString

        Dim lblInspection As Label = e.Item.FindControl("lblInspection")
        If Not IsDBNull(e.Item.DataItem("INSP_Name")) Then
            lblInspection.Text = e.Item.DataItem("INSP_Name")
        End If
        If Not IsDBNull(e.Item.DataItem("CURRENT_STATUS_Name")) Then
            lblInspection.Text &= " " & e.Item.DataItem("CURRENT_STATUS_Name")
        ElseIf Not IsDBNull(e.Item.DataItem("LAST_STATUS_Name")) Then
            lblInspection.Text &= " " & e.Item.DataItem("LAST_STATUS_Name")
        End If



        Dim lblBiz As Label = e.Item.FindControl("lblBiz")
        If Not IsDBNull(e.Item.DataItem("CURRENT_BIZ_LEVEL")) And Not IsDBNull(e.Item.DataItem("CURRENT_BIZ_LEVEL_Name")) Then
            lblBiz.Text = e.Item.DataItem("CURRENT_BIZ_LEVEL_Name")
            tdBiz.Attributes("Class") = BIZ.Get_Biz_Css_Text_By_Level(e.Item.DataItem("CURRENT_BIZ_LEVEL"))
        ElseIf Not IsDBNull(e.Item.DataItem("LAST_BIZ_LEVEL_Name")) And Not IsDBNull(e.Item.DataItem("LAST_BIZ_LEVEL")) Then
            lblBiz.Text = e.Item.DataItem("LAST_BIZ_LEVEL_Name")
            tdBiz.Attributes("Class") = BIZ.Get_Biz_Css_Text_By_Level(e.Item.DataItem("LAST_BIZ_LEVEL"))
        End If

        Dim lblSafety As Label = e.Item.FindControl("lblSafety")
        If Not IsDBNull(e.Item.DataItem("CURRENT_SAFETY_Name")) Then
            lblSafety.Text = e.Item.DataItem("CURRENT_SAFETY_Name")
        End If

        Dim lblMADay As Label = e.Item.FindControl("lblMADay")
        If Not IsDBNull(e.Item.DataItem("CURRENT_MA_DAY")) Then
            lblMADay.Text = e.Item.DataItem("CURRENT_MA_DAY")
            tdMADay.Attributes("Class") = BIZ.Get_MA_Day_Css_Box_By_Day(e.Item.DataItem("CURRENT_MA_DAY"))
        ElseIf Not IsDBNull(e.Item.DataItem("LAST_MA_DAY")) Then
            lblMADay.Text = e.Item.DataItem("LAST_MA_DAY")
            tdMADay.Attributes("Class") = BIZ.Get_MA_Day_Css_Box_By_Day(e.Item.DataItem("LAST_MA_DAY"))
        End If

        Dim LAST_LEVEL As Integer = -1
        Dim CURRENT_LEVEL As Integer = -1
        If Not IsDBNull(e.Item.DataItem("LAST_LEVEL")) Then
            LAST_LEVEL = e.Item.DataItem("LAST_LEVEL")
        End If
        If Not IsDBNull(e.Item.DataItem("CURRENT_LEVEL")) Then
            CURRENT_LEVEL = e.Item.DataItem("CURRENT_LEVEL")
        End If
        Dim lblSeverity As Label = e.Item.FindControl("lblSeverity")
        If CURRENT_LEVEL <> -1 Then
            lblSeverity.Text = BL.Get_Problem_Level_Name(CURRENT_LEVEL)
            tdSeverity.Attributes("Class") = BL.Get_Inspection_Css_Text_By_Level(CURRENT_LEVEL)
        ElseIf LAST_LEVEL <> -1 Then
            lblSeverity.Text = BL.Get_Problem_Level_Name(LAST_LEVEL)
            tdSeverity.Attributes("Class") = BL.Get_Inspection_Css_Text_By_Level(LAST_LEVEL)
        End If
        '" & vbNewLine

        Dim Tooltip As String = ""
        If Not IsDBNull(e.Item.DataItem("INSP_ID")) Then
            Select Case CInt(e.Item.DataItem("INSP_ID"))
                Case 12
                    If CURRENT_LEVEL = -1 Then
                        tdLevel.Attributes("Class") = BL.Get_Inspection_Css_Text_By_Zone(LAST_LEVEL)
                        Tooltip = BL.Get_Inspection_Css_Box_By_Zone(LAST_LEVEL).Replace("Level", "")
                    Else
                        lblInspection.CssClass = BL.Get_Inspection_Css_Text_By_Zone(CURRENT_LEVEL)
                        Tooltip = BL.Get_Inspection_Css_Box_By_Zone(CURRENT_LEVEL).Replace("Level", "")
                    End If
                Case Else
                    If CURRENT_LEVEL = -1 Then
                        tdLevel.Attributes("Class") = BL.Get_Inspection_Css_Text_By_Level(LAST_LEVEL)
                        Tooltip = BL.Get_Inspection_Css_Box_By_Level(LAST_LEVEL).Replace("Level", "")
                    Else
                        tdLevel.Attributes("Class") = BL.Get_Inspection_Css_Text_By_Zone(CURRENT_LEVEL)
                        Tooltip = BL.Get_Inspection_Css_Box_By_Level(CURRENT_LEVEL).Replace("Level", "")
                    End If
            End Select
        End If


        If Not IsDBNull(e.Item.DataItem("CURRENT_PROB_Detail")) Then lblDetail.Text = e.Item.DataItem("CURRENT_PROB_Detail")

        '--------------- Set Control And Link-------------
        lblBy.ToolTip = Tooltip
        lblDetail.ToolTip = Tooltip
        lblInspection.ToolTip = Tooltip

        lblDate.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"
        lblReport.Attributes("onClick") = lblDate.Attributes("onClick")

        btnView.CommandArgument = e.Item.DataItem("DETAIL_ID")
        tdLevel.Attributes("onClick") = "document.getElementById('" & btnView.ClientID & "').click();"
        tdDetail.Attributes("onClick") = tdLevel.Attributes("onClick")
        tdBy.Attributes("onClick") = tdLevel.Attributes("onClick")
    End Sub

    Protected Sub txt_Prob_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Prob_Start.TextChanged, txt_Prob_End.TextChanged
        BindTag()
    End Sub

    Private Sub rptHistory_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptHistory.ItemCommand
        Dim btnView As Button = e.Item.FindControl("btnView")
        Dim DETAIL_ID As Integer = btnView.CommandArgument

        Select Case e.CommandName
            Case "View"
                Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                DialogDetail.UNIQUE_POPUP_ID = UNIQUEKEY
                DialogDetail.TAG_CLASS = EIR_BL.Tag_Class.Stationary

                DialogDetail.ShowDialog(DETAIL_ID)
                DialogDetail.Disabled = True
        End Select
    End Sub
End Class