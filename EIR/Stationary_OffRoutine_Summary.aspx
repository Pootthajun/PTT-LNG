﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Stationary_OffRoutine_Summary.aspx.vb" Inherits="EIR.Stationary_OffRoutine_Summary" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
<!-- Page Head -->
			<h2>Summary Stationary Off-Routine Reports</h2>
					
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Filter</h3>
                
				        <asp:DropDownList CssClass="select" style="position:relative; top:5px; left: 0px;" 
                          ID="ddl_Search_Year" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
    						
						<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
						
						<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                        </asp:DropDownList>    					
    					<asp:DropDownList CssClass="select" style="position:relative; top:5px; left: 0px;" 
                          ID="ddl_Search_Tag" runat="server" AutoPostBack="True">
                        </asp:DropDownList>						
                        <asp:Label ID="lbl1" runat="server" style="position:relative; top:5px;" Text="Created"></asp:Label>
    		            <asp:TextBox runat="server" ID="txt_Search_Start" AutoPostBack="True"
                                style="position:relative; top:5px; left: 0px;" 
                                CssClass="text-input small-input " Width="70px" MaxLength="15"></asp:TextBox>
    		                <cc1:CalendarExtender ID="txt_Search_Start_CalendarExtender" runat="server" 
                                Format="yyyy-MM-dd" TargetControlID="txt_Search_Start">
                            </cc1:CalendarExtender>
    		            <asp:Label ID="lbl2" runat="server" style="position:relative; top:5px;" Text="to"></asp:Label> 
    		            <asp:TextBox runat="server" ID="txt_Search_End" style="position:relative; top:5px;"  AutoPostBack="True"
                                CssClass="text-input small-input " Width="70px" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txt_Search_End_CalendarExtender" runat="server" 
                                TargetControlID="txt_Search_End" Format="yyyy-MM-dd" >
                            </cc1:CalendarExtender>
                       &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_Edit" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="lblEditable" runat="server">
                  Editable</b>
                  <div class="clear"></div>
              </div>
			             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                 <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Report No.</a> </th>
                        <th><a href="#">Plant</a></th>
                        <th><a href="#">Route</a></th>
                        <th><a href="#">Tag</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Created</a></th>
                        <th><a href="#">Action</a></th>
                      </tr>                      
                    </thead>
                   
                    <asp:Repeater ID="rptPlan" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                              <tr>
                                <td style="text-align:center;"><asp:Label ID="lblRptNo" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblRoute" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblTag" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblCreated" runat="server"></asp:Label></td>
                                <td><!-- Icons -->
                                  <asp:Image ID="imgLock" runat="server" ImageUrl="resources/images/icons/lock.png" BorderStyle="None" ToolTip="Lock" />
                                  <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="resources/images/icons/user.png" BorderStyle="None" CommandName="Edit" />
                                  <a ID="btnReport" runat="server" href="javascript:;"><img src="resources/images/icons/printer.png" border="0"></a>
                                  
                                  <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                  <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this report permanently?" />
                                </td>
                              </tr>
                              
                    </ItemTemplate>
                    <FooterTemplate>
                     </tbody>
                    </FooterTemplate>
                   </asp:Repeater>
                   <tfoot>
                      <tr>
                        <td colspan="7">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" PostBackUrl="~/Stationary_OffRoutine_Edit1.aspx"
                                Text="Create report"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- End #tab1 -->
               
                
    </div>
    </div>
    
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>