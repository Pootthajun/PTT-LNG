﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogLawSelecteFolder.ascx.vb" Inherits="EIR.GL_DialogLawSelecteFolder" %>
<%@ Register Assembly="Goldtect.ASTreeView" Namespace="Goldtect" TagPrefix="astv" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Panel ID="pnlDialog" runat="server" BackColor="White" Width= "500">
    <table style="border-style: solid; border-width: 1px; width:100%; background-color:white" >
        <tr>
            <td>
                <h2>Select Destination Folder</h2>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <astv:ASTreeView ID="treeLawDocument" 
	                runat="server"
	                BasePath="~/resources/astreeview/"
	                DataTableRootNodeValue="0"
                    RootNodeValue="0"
	                EnableRoot="true" 
	                EnableNodeSelection="true" 
	                EnableCheckbox="false" 
	                EnableDragDrop="false" 
	                EnableTreeLines="true"
	                EnableNodeIcon="true"
	                EnableCustomizedNodeIcon="true"
	                EnableDebugMode="false"
	                EnableParentNodeExpand="false"
                    EnableContextMenu="false"
                    AutoPostBack="false"
                    >
                </astv:ASTreeView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" />
            &nbsp;
                <asp:Button ID="btnSelect" runat="server" Class="button" Text="Select" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg" 
                    Visible="False">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Button ID="Button1" runat="server" Text="Button" Width="0px" style="display:none"  />
<cc1:ModalPopupExtender ID="dialogLawFolder" runat="server" 
    BackgroundCssClass="MaskDialog" Drag="true"  DropShadow="true" PopupControlID="pnlDialog" 
    TargetControlID="Button1">
</cc1:ModalPopupExtender>
