﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Stationary_OffRoutine_Edit_Document.aspx.vb" Inherits="EIR.Stationary_OffRoutine_Edit_Document" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_StationaryOffRoutineSelectTemplate.ascx" tagname="UC_StationaryOffRoutineSelectTemplate" tagprefix="uc1" %>
<%@ Register src="UC_StationaryOffRoutineTemplate1.ascx" tagname="UC_StationaryOffRoutineTemplate1" tagprefix="ruc1" %>
<%@ Register src="UC_StationaryOffRoutineTemplate2.ascx" tagname="UC_StationaryOffRoutineTemplate2" tagprefix="ruc2" %>
<%@ Register src="UC_StationaryOffRoutineTemplate3.ascx" tagname="UC_StationaryOffRoutineTemplate3" tagprefix="ruc3" %>
<%@ Register src="UC_StationaryOffRoutineTemplate4.ascx" tagname="UC_StationaryOffRoutineTemplate4" tagprefix="ruc4" %>
<%@ Register src="UC_StationaryOffRoutineTemplate5.ascx" tagname="UC_StationaryOffRoutineTemplate5" tagprefix="ruc5" %>
<%@ Register src="UC_StationaryOffRoutineTemplate6.ascx" tagname="UC_StationaryOffRoutineTemplate6" tagprefix="ruc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>
	    
			<h2>Edit Stationary Off-Routine Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" >Tag Status</asp:LinkButton></li>
                        <li><asp:LinkButton id="HTabDocumentTemplate" runat="server"  CssClass="default-tab current">Document Template</asp:LinkButton></li>
						<%--<li><asp:LinkButton id="HTabPhoto" runat="server" >Photography Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabDocument" runat="server">Reference Document</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>--%>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Tag <asp:Label ID="lbl_TAG" runat="server" Text="Tag" CssClass="EditReportHeader"></asp:Label>
								</p>
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
							        </asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all vibration information for this report permanently?">
                                  </cc1:ConfirmButtonExtender>
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
								  
					        </ul>
						    
						        <asp:Repeater ID="rptINSP" runat="server">
                                  <ItemTemplate>
                                      <table border="1" cellspacing="0" cellpadding="5" >
                                        <tr>
                                            <td  style="font-weight:bold;">
                                                <asp:Label ID="lblNo" runat="server"></asp:Label>. 
                                                <asp:Label ID="lblINSP" runat="server"></asp:Label> 
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                <asp:Label ID="lblClass" runat="server"></asp:Label>
                                                <asp:Label ID="lblIclsID" runat="server" style="display:none" ></asp:Label>
                                                <asp:Label ID="lblDetailID" runat="server" style="display:none" ></asp:Label>
                                                <asp:Label ID="lblOffRoutineTemplateType" runat="server" style="display:none" ></asp:Label>
                                                <asp:Label ID="lblPROB_Detail" runat="server" style="display:none" ></asp:Label>
                                            </td>
                                            <td style="text-align:right">
                                                <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/cross_48.png" Width="32" Height="32" />
                                                <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this section permanently?" />
                                            </td>
                                        </tr>
                                          <ruc1:UC_StationaryOffRoutineTemplate1 ID="UC_StationaryOffRoutineTemplate1" runat="server" Visible="false" />
                                          <ruc2:UC_StationaryOffRoutineTemplate2 ID="UC_StationaryOffRoutineTemplate2" runat="server" Visible="false" />
                                          <ruc3:UC_StationaryOffRoutineTemplate3 ID="UC_StationaryOffRoutineTemplate3" runat="server" Visible="false" />
                                          <ruc4:UC_StationaryOffRoutineTemplate4 ID="UC_StationaryOffRoutineTemplate4" runat="server" Visible="false" />
                                          <ruc5:UC_StationaryOffRoutineTemplate5 ID="UC_StationaryOffRoutineTemplate5" runat="server" Visible="false" />
                                          <ruc6:UC_StationaryOffRoutineTemplate6 ID="UC_StationaryOffRoutineTemplate6" runat="server" Visible="false" />
                                        </table>
                                  </ItemTemplate>
                                  </asp:Repeater>

                                    <table style="width:100%;border:0px;padding:0;border-spacing:0">
                                        <tr>
                                            <td style="text-align:right">
                                                <asp:LinkButton ID="lnkAddSection" runat="server" >
				                                    <span>
					                                    <img src="resources/images/icons/icon_arrow_down.jpg" alt="Add Section" width="32" height="32" />
				                                    </span>
			                                    </asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </table>

                                    <uc1:UC_StationaryOffRoutineSelectTemplate ID="UC_SelectTemplate" runat="server" />
                                  
                                    <p align="right">
	                                    <asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
	                                    <asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
                                    </p>
								
							
								
							</fieldset>
				    </div>
				  <!-- End #tabDetail -->        
	          
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			

</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>
