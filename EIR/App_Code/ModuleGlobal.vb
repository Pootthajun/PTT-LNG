﻿Imports System.Data
Imports System.Web.UI
Imports System
Imports Microsoft.VisualBasic
Imports OfficeOpenXml
Imports System.Web.SessionState

Module ModuleGlobal


    Public Const PTT_Domain As String = ""
    Public Const PTT_LDAP As String = "LDAP://" & PTT_Domain


#Region "Script"

    Public Sub ImplementJavaMoneyText(ByRef Obj As WebControls.TextBox, Optional ByVal MaxValue As Double = Double.MaxValue, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") &= "this.value=formatmoney(this.value,'0','" & MaxValue & "');"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaIntegerText(ByRef Obj As WebControls.TextBox, ByVal IncludeComma As Boolean, Optional ByVal MaxValue As Long = Long.MaxValue, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") &= "this.value=formatinteger(this.value,0," & MaxValue & "," & IncludeComma.ToString.ToLower & ");"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaOnlyNumberText(ByRef Obj As WebControls.TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") &= "this.value=formatonlynumber(this.value);"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaNumericText(ByRef Obj As WebControls.TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") &= "this.value=formatnumeric(this.value,'-999999999999','999999999999');"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaNumericText(ByRef Obj As WebControls.TextBox, ByVal MaxDecimalPlace As Integer, ByVal Align As String)
        Obj.Attributes("OnChange") &= "this.value=formatnumericLimitPlace(this.value,'-999999999999','999999999999'," & MaxDecimalPlace & ");"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaFloatText(ByRef Obj As WebControls.TextBox, ByVal DecimalPlace As UInteger, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") &= "this.value=formatfloat(this.value,'-999999999999','999999999999'," & DecimalPlace & ");"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaVibrationZone(ByRef Obj As TextBox)
        Obj.Attributes("OnChange") = "this.value=formatVibrationZone(this.value);" & Obj.Attributes("OnChange")
        Obj.Style.Item("Text-Align") = "Center"
    End Sub

    Public Sub ImplementJavaSpringHangerInspection(ByRef Obj As TextBox)
        Obj.Attributes("OnChange") = "this.value=format_SPH_INSP(this.value);" & Obj.Attributes("OnChange")
        Obj.Style.Item("Text-Align") = "Center"
    End Sub

    Public Function FormatNumericText(ByVal Value As Double, Optional ByVal IncludedComma As Boolean = True) As String
        Dim Crop As String = Value.ToString
        Dim DecPlace As Integer = 0
        Dim SpltPath As String() = Crop.Split(".")
        If SpltPath.Length > 1 Then
            DecPlace = Len(SpltPath(1))
        End If
        Return FormatNumber(Value, DecPlace,,, IncludedComma)
    End Function

    Public Function FormatNumericTextLimitPlace(ByVal Value As Double, ByVal IncludedComma As Boolean, ByVal MaxDecimalPlace As Integer) As String
        Dim Crop As String = Value.ToString
        Dim DecPlace As Integer = 0
        Dim SpltPath As String() = Crop.Split(".")
        If SpltPath.Length > 1 Then
            DecPlace = Len(SpltPath(1))
            If DecPlace > MaxDecimalPlace Then DecPlace = MaxDecimalPlace
        End If
        Return FormatNumber(Value, DecPlace,,, IncludedComma)
    End Function

    Public Sub Redirect(ByVal Page As Page, ByVal URL As String)
        ScriptManager.RegisterStartupScript(Page, GetType(String), "Redirect", "window.location.href='" & URL & "';", True)
    End Sub

    Public Sub Alert(ByVal Page As Page, ByVal Message As String)
        ScriptManager.RegisterStartupScript(Page, GetType(String), "Alert", "alert('" & Message & "');", True)
    End Sub

    Public Sub CloseWindow(ByVal Page As Page)
        ScriptManager.RegisterStartupScript(Page, GetType(String), "CloseWindow", "window.close();", True)
    End Sub

    Public Sub CloseTopWindow(ByVal Page As Page)
        ScriptManager.RegisterStartupScript(Page, GetType(String), "CloseWindow", "top.close();", True)
    End Sub

    Public Sub ShowDialogEditSVG(ByVal Page As Page, ByVal UNIQUE_POPUP_ID As String, ByVal CancelButton As String, ByVal OKButton As String, ByVal InputTextbox As String, Optional ByVal DisplayTitle As Boolean = True, Optional ByVal DisplayType As Boolean = True, Optional ByVal DisplayDesc As Boolean = True, Optional ByVal DisplayTag As Boolean = True, Optional ByVal _ReadOnly As Boolean = False)
        Dim Script As String = "ShowDialogEditSVG('" & UNIQUE_POPUP_ID.Replace("'", "\'") & "','" & CancelButton & "','" & OKButton & "','" & InputTextbox & "','" & DisplayTitle.ToString & "','" & DisplayType.ToString & "','" & DisplayDesc.ToString & "','" & DisplayTag & "','" & _ReadOnly & "','" & Now.ToOADate.ToString.Replace(".", "") & "');"
        ScriptManager.RegisterStartupScript(Page, GetType(String), UNIQUE_POPUP_ID, Script, True)
    End Sub

#End Region

    Public Function MonthEng(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
            Case Else
                Return ""
        End Select
    End Function

    Public Function MonthThai(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "มกราคม"
            Case 2
                Return "กุมภาพันธ์"
            Case 3
                Return "มีนาคม"
            Case 4
                Return "เมษายน"
            Case 5
                Return "พฤษภาคม"
            Case 6
                Return "มิถุานยน"
            Case 7
                Return "กรกฎาคม"
            Case 8
                Return "สิงหาคม"
            Case 9
                Return "กันยายน"
            Case 10
                Return "ตุลาคม"
            Case 11
                Return "พฤศจิกายน"
            Case 12
                Return "ธันวาคม"
            Case Else
                Return ""
        End Select
    End Function

    Public Function FormatEnglishDate(ByVal D As DateTime) As String
        Return MonthEng(D.Month) & " " & D.Day & ", " & D.Year
    End Function

    Public Function FormatSystemDate(ByVal D As DateTime) As String
        Return D.Year & "-" & D.Month.ToString.PadLeft(2, "0") & "-" & D.Day.ToString.PadLeft(2, "0")
    End Function

    Public Function OriginalFileName(ByVal FullPath As String) As String
        Return FullPath.Substring(FullPath.LastIndexOf("\") + 1)
    End Function

    Public Function OriginalFileType(ByVal FullPath As String) As String
        Return FullPath.Substring(FullPath.LastIndexOf(".") + 1)
    End Function


    Public Function GetIPAddress() As String
        Return HttpContext.Current.Request.UserHostAddress
    End Function

    Public Function GetCurrentPageName() As String
        Dim URL As String = ""
        Try
            URL = HttpContext.Current.Request.ServerVariables("URL")
            URL = URL.Substring(URL.LastIndexOf("/") + 1)
        Catch : End Try
        Return URL
    End Function

    Public Function GenerateNewUniqueID() As String
        Return Guid.NewGuid.ToString
        'Return HttpContext.Current.Session.SessionID & Now.ToOADate.ToString.Replace(".", "")
    End Function

    Public Sub DeleteFile(ByVal Path As String)
        My.Computer.FileSystem.DeleteFile(Path)
    End Sub

    Public Function ReadExcelToDataTable(ByVal ExcelPath As String) As DataTable
        On Error Resume Next
        Dim FIO As New System.IO.FileInfo(ExcelPath)
        Dim DT As New DataTable
        Dim excel = New ExcelPackage(FIO)
        Dim worksheet = excel.Workbook.Worksheets.First()
        '---------Build Column ------------
        For i As Integer = 0 To worksheet.Dimension.[End].Column - 1
            DT.Columns.Add("C" & (i + 1))
        Next
        For i As Integer = 0 To worksheet.Dimension.[End].Row - 1
            Dim DR As DataRow = DT.NewRow
            For j As Integer = 0 To worksheet.Dimension.[End].Column
                DR(j) = worksheet.Cells(i + 1, j + 1).Value
            Next
            DT.Rows.Add(DR)
        Next
        worksheet = Nothing
        excel.Dispose()

        Return DT

    End Function

    Public Function TrimExcelToDataTable(ByVal DT As DataTable) As DataTable
        '-------------- Trim Empty Row End->Start------------------
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            Dim _pass As Boolean = False
            For j As Integer = 0 To DT.Columns.Count - 1
                If Not IsDBNull(DT.Rows(i).Item(j)) AndAlso DT.Rows(i).Item(j) <> "" Then
                    _pass = True
                    Exit For
                End If
            Next
            If Not _pass Then
                DT.Rows(i).Delete()
                DT.AcceptChanges()
            Else
                Exit For
            End If
        Next

        '-------------- Trim Empty Row End->Start------------------
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim _pass As Boolean = False
            For j As Integer = 0 To DT.Columns.Count - 1
                If Not IsDBNull(DT.Rows(0).Item(j)) AndAlso DT.Rows(0).Item(j) <> "" Then
                    _pass = True
                    Exit For
                End If
            Next
            If Not _pass Then
                DT.Rows(0).Delete()
                DT.AcceptChanges()
            Else
                Exit For
            End If
        Next

        '----------------- RTrim --------------
        For i As Integer = DT.Columns.Count - 1 To 0 Step -1
            Dim _pass As Boolean = False
            For j As Integer = 0 To DT.Rows.Count - 1
                If Not IsDBNull(DT.Rows(j).Item(i)) AndAlso DT.Rows(j).Item(i) <> "" Then
                    _pass = True
                    Exit For
                End If
            Next
            If Not _pass Then
                DT.Columns.RemoveAt(i)
            Else
                Exit For
            End If
        Next

        '----------------- LTrim --------------
        For i As Integer = 0 To DT.Columns.Count - 1
            Dim _pass As Boolean = False
            For j As Integer = 0 To DT.Rows.Count - 1
                If Not IsDBNull(DT.Rows(j).Item(0)) AndAlso DT.Rows(j).Item(0) <> "" Then
                    _pass = True
                    Exit For
                End If
            Next
            If Not _pass Then
                DT.Columns.RemoveAt(0)
            Else
                Exit For
            End If
        Next

        Return DT
    End Function

End Module
