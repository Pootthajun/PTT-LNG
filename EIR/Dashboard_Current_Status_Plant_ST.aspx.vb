﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status_Plant_ST
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL

    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)

        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("PLANT_ID")) Then
                PLANT_ID = Request.QueryString("PLANT_ID")
            End If
            lblBack.Text = "Back to see all current status for " & lblPlant.Text
            lblBack.PostBackUrl = "Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & PLANT_ID
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = ""
        SQL &= "DECLARE @PLANT_ID INT=" & PLANT_ID & vbNewLine
        SQL &= "SELECT AREA_ID,AREA_Code,AREA_Name,PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= "	 SELECT DISTINCT MS_Area.AREA_ID,MS_Area.AREA_Code,AREA_Name,MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_ST_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
        SQL &= "	 FROM MS_Plant" & vbLf
        SQL &= "	 LEFT JOIN MS_ST_Route ON MS_Plant.PLANT_ID=MS_ST_Route.PLANT_ID AND MS_ST_Route.Active_Status=1" & vbLf
        SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
        SQL &= "	 INNER JOIN MS_ST_TAG ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID AND MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID AND MS_ST_TAG.Active_Status=1" & vbLf
        SQL &= "     LEFT JOIN (" & vbNewLine
        SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
        SQL &= "        FROM" & vbNewLine
        SQL &= "        (" & vbNewLine
        SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
        SQL &= "            FROM" & vbNewLine
        SQL &= "            (" & vbNewLine
        SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
        SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
        SQL &= "                FROM" & vbNewLine
        SQL &= "     		    (" & vbNewLine
        SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_ST_Detail.TAG_TYPE_ID,RPT_ST_Detail.INSP_ID," & vbNewLine
        SQL &= "     				ICLS_ID CUR_LEVEL" & vbNewLine
        SQL &= "                    FROM RPT_ST_Detail" & vbNewLine
        SQL &= "     		    ) TB1" & vbNewLine
        SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
        SQL &= "     	    ) TB2" & vbNewLine
        SQL &= "        ) TB3" & vbNewLine
        SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
        SQL &= "     ) VW ON MS_ST_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
        SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " WHERE PLANT_ID = @PLANT_ID" & vbLf
        SQL &= " GROUP BY AREA_ID,AREA_Code,AREA_Name,PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= " ORDER BY AREA_Name" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("Dashboard_Current_Status_Plant_" & PLANT_ID) = DT
        Dim NewHeight As Unit = Unit.Pixel(26 * DT.Rows.Count)
        If ChartMain.Height.Value < NewHeight.Value Then ChartMain.Height = NewHeight

        DisplayChart(ChartMain, New EventArgs)

        rptData.DataSource = DT
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblArea As Label = e.Item.FindControl("lblArea")
        Dim lblNormal As Label = e.Item.FindControl("lblNormal")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")

        lblArea.Text = e.Item.DataItem("AREA_NAME")

        Dim Normal As Integer = 0
        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0
        If Not IsDBNull(e.Item.DataItem("Normal")) AndAlso e.Item.DataItem("Normal") <> 0 Then
            Normal = e.Item.DataItem("Normal")
            lblNormal.Text = FormatNumber(Normal, 0)
        Else
            lblNormal.Text = "-"
        End If
        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If

        If Normal + ClassC + ClassB + ClassA <> 0 Then
            lblTotal.Text = FormatNumber(Normal + ClassC + ClassB + ClassA, 0)
        Else
            lblTotal.Text = "-"
        End If

        tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Area_ST.aspx?AREA_ID=" & e.Item.DataItem("AREA_ID") & "';"

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_ChartType.SelectedIndexChanged

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartMain.Series(0).ChartType
        Select Case ddl_ChartType.Items(ddl_ChartType.SelectedIndex).Value
            Case "Bar"
                ChartType = DataVisualization.Charting.SeriesChartType.Bar
            Case "StackedBar"
                ChartType = DataVisualization.Charting.SeriesChartType.StackedBar
            Case "Column"
                ChartType = DataVisualization.Charting.SeriesChartType.Column
            Case "StackedColumn"
                ChartType = DataVisualization.Charting.SeriesChartType.StackedColumn
            Case "StackedArea"
                ChartType = DataVisualization.Charting.SeriesChartType.StackedArea
        End Select
        For i As Integer = 0 To ChartMain.Series.Count - 1
            ChartMain.Series(i).ChartType = ChartType
        Next

        Dim DT As DataTable = Session("Dashboard_Current_Status_Plant_" & PLANT_ID)
        For i As Integer = 0 To DT.Rows.Count - 1

            ChartMain.Series("Series1").Points.AddY(DT.Rows(i).Item("Normal"))
            ChartMain.Series("Series2").Points.AddY(DT.Rows(i).Item("ClassC"))
            ChartMain.Series("Series3").Points.AddY(DT.Rows(i).Item("ClassB"))
            ChartMain.Series("Series4").Points.AddY(DT.Rows(i).Item("ClassA"))

            Dim Label As String = DT.Rows(i).Item("AREA_Code")
            Dim Url As String = "Dashboard_Current_Status_AREA_ST.aspx?AREA_ID=" & DT.Rows(i).Item("AREA_ID")
            Dim Tooltip As String = "Area : " & DT.Rows(i).Item("AREA_Code") & vbNewLine
            Tooltip &= "Total =" & DT.Rows(i).Item("Normal") + DT.Rows(i).Item("ClassC") + DT.Rows(i).Item("ClassB") + DT.Rows(i).Item("ClassA") & vbNewLine
            Tooltip &= "Normal=" & DT.Rows(i).Item("Normal") & vbNewLine
            Tooltip &= "ClassC=" & DT.Rows(i).Item("ClassC") & vbNewLine
            Tooltip &= "ClassB=" & DT.Rows(i).Item("ClassB") & vbNewLine
            Tooltip &= "ClassA=" & DT.Rows(i).Item("ClassA") & vbNewLine

            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip
            ChartMain.Series("Series1").Points(i).AxisLabel = Label
            ChartMain.Series("Series1").Points(i).Url = Url
            ChartMain.Series("Series2").Points(i).ToolTip = Tooltip
            ChartMain.Series("Series2").Points(i).AxisLabel = Label
            ChartMain.Series("Series2").Points(i).Url = Url
            ChartMain.Series("Series3").Points(i).ToolTip = Tooltip
            ChartMain.Series("Series3").Points(i).AxisLabel = Label
            ChartMain.Series("Series3").Points(i).Url = Url
            ChartMain.Series("Series4").Points(i).ToolTip = Tooltip
            ChartMain.Series("Series4").Points(i).AxisLabel = Label
            ChartMain.Series("Series4").Points(i).Url = Url
        Next
    End Sub
End Class