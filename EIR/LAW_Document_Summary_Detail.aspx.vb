﻿Imports EIR

Public Class LAW_Document_Summary_Detail
    Inherits System.Web.UI.Page

    Dim CL As New LawClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            lblDocumentPlanID.Text = Request("id")
            BindDocumentInfo(Request("id"))
        End If
    End Sub

    Private Sub BindDocumentInfo(DocumentPlanID As Long)
        Dim dt As DataTable = CL.GetDataDocumentPlan(DocumentPlanID)
        If dt.Rows.Count > 0 Then
            lblDocumentName.Text = dt.Rows(0)("document_name")
            lblPlantCode.Text = dt.Rows(0)("plant_code")
            lblTagNo.Text = dt.Rows(0)("tag_no")
            lblYear.Text = dt.Rows(0)("document_year")
            If Convert.IsDBNull(dt.Rows(0)("next_notice_date")) = False Then txtNextNoticeDate.Text = Convert.ToDateTime(dt.Rows(0)("next_notice_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

            Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPlanStatus(DocumentPlanID)
            Select Case pStatus.PlanStatus
                Case "WAITING"
                    lblPlanStatus.Text = "<span style='color:blue'><b>WAITING</b></span>"
                Case "NOTICE"
                    lblPlanStatus.Text = "<span style='color:orange'><b>NOTICE</b></span>"
                Case "CRITICAL"
                    lblPlanStatus.Text = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
                Case "COMPLETE"
                    lblPlanStatus.Text = "<span style='color:green'><b>COMPLETE</b></span>"
            End Select

            'Display Chart

            Dim PlantDT As DataTable = CL.GetDocumentPlanTable(DocumentPlanID)
            rptData.DataSource = PlantDT
            rptData.DataBind()

            Dim ChartDT As DataTable = CL.GetDocumentChartData(DocumentPlanID)
            DisplayChart(ChartDT, lblYear.Text, dt.Rows(0)("plant_id"), lblPlantCode.Text)

            Dim pDt As DataTable = CL.GetListDocumentPlanPaper(DocumentPlanID)
            UC_DocumentTreePlanPaper1.SetPlanPaperData(pDt)
        End If
        dt.Dispose()
    End Sub

    Private Sub rptData_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblNoticeDate As Label = e.Item.FindControl("lblNoticeDate")
        Dim lblDocumentName As Label = e.Item.FindControl("lblDocumentName")
        Dim lblPlan As Label = e.Item.FindControl("lblPlan")
        Dim lblActual As Label = e.Item.FindControl("lblActual")

        lblNoticeDate.Text = Convert.ToDateTime(e.Item.DataItem("plan_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
        lblDocumentName.Text = e.Item.DataItem("paper_name")
        lblPlan.Text = e.Item.DataItem("plan_percent")
        lblActual.Text = e.Item.DataItem("actual_percent")
    End Sub

    Protected Sub DisplayChart(ByVal DT As DataTable, ByVal Year As Integer, ByVal Plant_ID As Integer, ByVal Plant_Name As String)
        ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Clear()

        Dim culture As New System.Globalization.CultureInfo("en-US")
        Dim C As New Converter

        Dim Plant As String = ""
        If Plant_ID > 0 Then
            Plant = "  Plant  " & Plant_Name
        End If

        Dim PlanValue As Integer = 0
        Dim ActualValue As Integer = 0

        'ChartMain.Series("Series1").IsValueShownAsLabel = True
        'ChartMain.Series("Series2").IsValueShownAsLabel = True

        'ChartMain.Titles("Title1").Text = "Annual Inspection  Progress for " & vbNewLine & vbNewLine & Dashboard.FindEquipmentName(Equipment) & " Equipment   Year  " & Year
        For i As Integer = 0 To DT.Rows.Count - 1
            PlanValue = (PlanValue + DT.Rows(i).Item("plan_percent"))
            ActualValue = (ActualValue + DT.Rows(i).Item("actual_percent"))

            Dim PlanDate As String = Convert.ToDateTime(DT.Rows(i).Item("action_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

            ChartMain.Series("Series1").Points.AddXY(i + 1, PlanValue)
            ChartMain.Series("Series1").Points(i).ToolTip = "Plan" & vbNewLine & PlanDate & vbNewLine & DT.Rows(i)("paper_name") & vbNewLine & PlanValue & "%"

            ChartMain.Series("Series2").Points.AddXY(i + 1, ActualValue)
            ChartMain.Series("Series2").Points(i).ToolTip = "Actual" & vbNewLine & PlanDate & vbNewLine & DT.Rows(i)("paper_name") & vbNewLine & ActualValue & "%"

            ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, PlanDate)
        Next
    End Sub

    Private Sub UC_DocumentTreePlanPaper1_SetPercentComplete(sender As UC_DocumentTreePlanPaper, PercentComplete As Integer) Handles UC_DocumentTreePlanPaper1.SetPercentComplete
        BindDocumentInfo(lblDocumentPlanID.Text)

        'Dim UC As UC_DocumentTreePlanPaper = DirectCast(sender, UC_DocumentTreePlanPaper)
        'Dim RowItem As RepeaterItem = UC.Parent.Parent.Parent

        'Dim lblPercentComplete As Label = RowItem.FindControl("lblPercentComplete")
        'Dim lblUploadDate As Label = RowItem.FindControl("lblUploadDate")

        'lblPercentComplete.Text = PercentComplete
        'lblUploadDate.Text = DateTime.Now.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
    End Sub

    Private Sub txtNextNoticeDate_TextChanged(sender As Object, e As EventArgs) Handles txtNextNoticeDate.TextChanged
        If txtNextNoticeDate.Text <> "" Then
            If IsDate(txtNextNoticeDate.Text) = True Then
                Dim c As New Converter
                CL.UpdateNextNoticeDate(lblDocumentPlanID.Text, c.StringToDate(txtNextNoticeDate.Text, "dd-MMM-yyyy"))
            End If
        End If

        BindDocumentInfo(lblDocumentPlanID.Text)
    End Sub
End Class