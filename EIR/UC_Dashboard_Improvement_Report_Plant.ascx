﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Dashboard_Improvement_Report_Plant.ascx.vb" Inherits="EIR.UC_Dashboard_Improvement_Report_Plant" %>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
    <tr>
        <td width="500px" style="vertical-align:top;">
            <asp:Chart ID="ChartMain" runat="server" Width="500px" Height="400px" CssClass="ChartHighligh" >
                <Series>
                    <asp:Series ChartArea="ChartArea1" ChartType="Column" Color="DodgerBlue" Name="Series1">
                    </asp:Series>
                </Series>
                <titles>
                    <asp:Title Name="Title1">
                </asp:Title>
                </titles>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <axisy intervalautomode="VariableCount">
                        </axisy>
                        <axisx intervalautomode="VariableCount">
                        </axisx>
                        <axisx2 intervalautomode="VariableCount">
                        </axisx2>
                        <axisy2 intervalautomode="VariableCount">
                        </axisy2>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </td>
        <td style="vertical-align:top; text-align:center; width:100%">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" style="width:220px; border:1px solid #efefef;">
                   <tr>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  width: 60px; border-bottom:1px solid #003366;">
                          Month</td>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  width: 60px; border-bottom:1px solid #003366;">
                          Year</td>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  width: 100px; border-bottom:1px solid #003366;">
                          Problem(s)</td>
                   </tr>
                   <asp:Repeater ID="rptData" runat="server">
                        <ItemTemplate>
                            <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblMonth" runat="server"></asp:Label></td>
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblYear" runat="server"></asp:Label></td>
                              <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                  <asp:Label ID="lblImprovement" runat="server" ForeColor = "DodgerBlue"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                   </asp:Repeater>
                </table>
            </center>
        </td>
    </tr>
</table>