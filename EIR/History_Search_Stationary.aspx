﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="History_Search_Stationary.aspx.vb" Inherits="EIR.History_Search_Stationary" %>
<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolderHead">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
        <ContentTemplate>

            <!-- Page Head -->
			<h2>History Search Stationary Reports</h2>
									
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div style="background-color:#efefef">
        
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>Plant</td>
                        <td><asp:DropDownList CssClass="select" ID="ddl_Plant" runat="server" AutoPostBack="True" Width="200px"></asp:DropDownList></td>
                        <td>Route</td>
                        <td align="left">
                            <asp:DropDownList CssClass="select" ID="ddl_Route" runat="server" Width="200px"></asp:DropDownList></td>
                      </tr>
                      <tr>
                        <td>Area</td>
                        <td><asp:DropDownList CssClass="select" ID="ddl_Area" runat="server" Width="200px"></asp:DropDownList></td>
                        <td>Process</td>
                        <td align="left">
                            <asp:DropDownList CssClass="select" ID="ddl_Process" runat="server" Width="200px"></asp:DropDownList></td>
                      </tr>
                      <tr>
                        <td>Type</td>
                        <td><asp:DropDownList CssClass="select" ID="ddl_Type" runat="server" Width="200px"></asp:DropDownList></td>
                        <td>Tag Code</td>
                        <td align="left"><asp:TextBox Width="190px" Font-Bold=true ID="txt_Code" CssClass="text-input small-input" MaxLength="50" runat="server"></asp:TextBox></td>
                      </tr>                     
                      <tr>
                        <td>Description/Remark</td>
                        <td  align="left" ><asp:TextBox Width="190px" Font-Bold="true" CssClass="text-input small-input" ID="txt_Desc" MaxLength="200" runat="server"></asp:TextBox></td>
                        <td>Affiliate officer </td>
                        <td><asp:TextBox Width="190px" Font-Bold=true  ID="txt_User" CssClass="text-input small-input" MaxLength="50" runat="server"></asp:TextBox></td>
                          
                      </tr>                     
                      <tr>
                        <td>Status </td>
                        <td align="left">
                            <span class="TextNormal"><asp:CheckBox ID="chkOK" runat="server" Text="" AutoPostBack="true"  />OK &nbsp; </span>
                            <span class="TextClassA"><asp:CheckBox ID="chkClassA" runat="server" Text="" AutoPostBack="true" /> Class A &nbsp; </span> 
                            <span class="TextClassB"><asp:CheckBox ID="chkClassB" runat="server" Text="" AutoPostBack="true" /> Class B &nbsp; </span>
                            <span class="TextClassC"><asp:CheckBox ID="chkClassC" runat="server" Text="" AutoPostBack="true" /> Class C</span>
                        <td id="tdCaptionEvent" runat="server">Problem(s) occured between</td>
                        <td align="left" id="tdTimeEvent" runat="server">
                            <asp:TextBox runat="server" ID="txt_Prob_Start" style="position:relative; left: 0px; top: 0px;"  Font-Bold="true" CssClass="text-input small-input " Width="75px" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txt_Prob_Start_CalendarExtender" runat="server" Format="dd MMM yyyy" TargetControlID="txt_Prob_Start"></cc1:CalendarExtender>
                            &nbsp; to &nbsp; 
                            <asp:TextBox runat="server" ID="txt_Prob_End" style="position:relative; left: 0px;"  Font-Bold="true" CssClass="text-input small-input " Width="75px" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txt_Prob_End_CalendarExtender" runat="server" Format="dd MMM yyyy" TargetControlID="txt_Prob_End"></cc1:CalendarExtender>
                            </td>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pnl_Inspection" runat="server">
                      <tr>
                        <td valign="top" width="150">Contained Problem(s) for</td>
                        <td align="left" valign="top" style="vertical-align:top;">
                           <asp:Repeater ID="rptStationary" runat="server">
                            <ItemTemplate>
                                <label style="width:180px; display:inline; float:left;">
                                <asp:CheckBox ID="chbINSP" runat="server" Width="25px" Text="Inspection" />       
                                <asp:Label ID="lblINSP" runat="server" Width="150px" Font-Bold=false></asp:Label>                       
                                </label>
                            </ItemTemplate>
                           </asp:Repeater>                            
                        </td>
                          
                      </tr>
                     </table>
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td colspan="4" style="text-align:center">
                        <asp:LinkButton ID="btnClear" runat="server" CssClass="button" Text="Clear filter"></asp:LinkButton>
                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="button" Text="Apply Filter"></asp:LinkButton>
                        </td>
                      </tr>
                    </table>
              </div>
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListTag" runat="server">
                  
                  <span style="font-size:14px;">
                          <strong>Current condition</strong> : <em style="font-style:italic;"><asp:Label ID="lblCondition" runat="server"></asp:Label></em><br><br>
				  </span>
				  
                      <style type="text/css">
                          .divTag {
                            width:100%;
                            background-color:#FFFFF2;
                            cursor:pointer;
                            padding: 10px;
                            border-bottom:1px solid white;
                          }

                              .divTag:hover {
                              background-color:lavender;
                              }

                              .divTag div{
                                width:90%;
                                margin-top:5px;
                              }
                      </style>

                            <asp:Repeater ID="rptTag" runat="server">
                             <ItemTemplate>
                                            <div id="divTag" runat="server" class="divTag">
                                                <div style="width:90%">
                                                    <asp:Label ID="lblTagCode" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label> : 
                                                    <asp:Label ID="lblTagName" runat="server"></asp:Label> located on
                                                    <asp:Label ID="lblPlant" runat="server" ForeColor="DarkGreen" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblArea" runat="server" ForeColor="Brown" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblRoute" runat="server" ForeColor="SteelBlue" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div>
                                                    This <asp:Label ID="lblType" runat="server" ForeColor="SlateBlue" ></asp:Label> tag
                                                    use for process <asp:Label ID="lblProcess" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>                                           
                             </ItemTemplate>                             
                       </asp:Repeater>
                        <uc1:PageNavigation ID="Navigation" runat="server" PageSize="20" />
			    </asp:Panel>
				    
				 <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
			    
                </div>
                <!-- End #tab1 -->
					
				
			  </div> <!-- End .content-box-content -->
				
		  </div>
                
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



