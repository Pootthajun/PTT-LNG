﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_CUI_Edit3.aspx.vb" Inherits="EIR.PIPE_CUI_Edit3" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register Src="~/UC_Pipe_Tag_Info.ascx" TagPrefix="Pipe" TagName="UC_Pipe_Tag_Info" %>
<%@ Register Src="~/UC_Pipe_Point_Info.ascx" TagPrefix="uc1" TagName="UC_Pipe_Point_Info" %>
<%@ Register src="GL_DialogInputValue.ascx" tagname="GL_DialogInputValue" tagprefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/File_Album.css" type="text/css" media="all" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<Triggers>
    <asp:PostBackTrigger ControlID="btnUploadExcel" />
</Triggers>
<ContentTemplate>
    <!-- Page Head -->
	<h2>Corrosion Under Insulation Report</h2>
    <div class="clear"></div> <!-- End .clear -->
    <div class="content-box"><!-- Start Content Box -->
         <div class="content-box-header">					
			<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label> :: <asp:Label ID="lblStep" runat="server"></asp:Label></h3>	
            <ul class="content-box-tabs">
                <asp:Repeater ID="rptTab" runat="server">
                    <ItemTemplate>
                        <li><a id="HTab" runat="server" >xxxxxxxxxxxx</a></li>   
                    </ItemTemplate>
                </asp:Repeater>				
			</ul>					
			<div class="clear"></div>					
		</div> <!-- End .content-box-header -->
        <div class="content-box-content">
            <div class="tab-content current">
                <p style="font-weight:bold; font-size:16px;">
				<label class="column-left" style="width:120px; font-size:16px;" >Report for: </label>
					<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>	               						
					| Tag : <asp:Label ID="lbl_Tag" runat="server" Text="Tag" CssClass="EditReportHeader"></asp:Label>
					| Point : <asp:Label ID="lbl_Point" runat="server" Text="Point" CssClass="EditReportHeader" ></asp:Label>
                    | Report Date :
                    <asp:TextBox runat="server" ID="txt_Rpt_Date" Width="100px" CssClass="text-input small-input "  Font-Bold="true" AutoPostBack="True" PlaceHolder="..." style="text-align:center;"></asp:TextBox>  
                    <Ajax:CalendarExtender ID="txt_Rpt_Date_Extender" runat="server" 
                             Format="dd MMM yyyy" TargetControlID="txt_Rpt_Date" PopupPosition="Right" >
                    </Ajax:CalendarExtender>
                    <font color="red">**</font>              
				</p>	
                <div class="clear"></div><!-- End .clear -->

                <!-- This is the target div. id must match the href of this div's tab -->
                <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                    <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                    <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                </asp:Panel>

                <ul class="shortcut-buttons-set">						
						<li>
						    <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
						        <span> 
							        <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							        Clear all
							    </span>
						    </asp:LinkButton>
							<Ajax:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this step permanently?">
                            </Ajax:ConfirmButtonExtender>
						</li>
						<li>
						    <a class="shortcut-button" href="javascript:window.location.href=window.location.href;">
							    <span>
								    <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
							        Reset this tab
							    </span>
						    </a>
						</li>								 
						<li>
						    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
							    <span>
								    <img src="resources/images/icons/print_48.png" alt="icon"/><br />
								    Preview report
							    </span>
						    </asp:LinkButton>
						</li>				        
				</ul>

                <Pipe:UC_Pipe_Tag_Info ID="Pipe_Info" runat="server" />
                <uc1:UC_Pipe_Point_Info runat="server" ID="Point_Info" />

                <style type="text/css">
                    .File_Zoom_Mask {
                        background-image:url('resources/images/icons/zoom_white_16.png');
                    }

                    .File_Command {
                        width:100%;
                        display: block; 
                        opacity:unset;
                        z-index:3
                    }

                .File_Command input[type="image"] {
                    margin-top:5px;
                }

                .File_Image {
                    width:unset;
                    height:unset;
                    position:absolute;    
                    z-index:1;                
                }

                 .content-box-header {
                    height:unset;
                    }

                </style>

                <table cellpadding="0" cellspacing="0" class="propertyTable" >
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="2">
                            Measurement Instrument :
                        </td>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="5">
                            Calibration of Thickness in mm.
                        </td>
                    </tr>
                    <tr>
                        <td style="width:20%; background-color:#f5f5f5;">Instrument:</td>
                        <td style="width:30%;">
                            <asp:DropDownList ID="ddlInstrument" AutoPostBack="true" style="text-align:center;" runat="server" CssClass="text-input small-input">
                            </asp:DropDownList>
                        </td>
                        <td style="width:20%; background-color:#f5f5f5;">Stepweight thickness</td>
                        <td style="width:7%;"><asp:TextBox ID="txtStep1" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td style="width:7%;"><asp:TextBox ID="txtStep2" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td style="width:7%;"><asp:TextBox ID="txtStep3" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td style="width:8%;"><asp:TextBox ID="txtStep4" runat="server" MaxLength="10"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Serail No :</td>
                        <td style="padding-left:10px;"><asp:Label ID="lblTMSerial" runat="server"></asp:Label></td>
                        <td style="background-color:#f5f5f5;">Measurement</td>
                        <td><asp:TextBox ID="txtMeasure1" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td><asp:TextBox ID="txtMeasure2" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td><asp:TextBox ID="txtMeasure3" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td><asp:TextBox ID="txtMeasure4" runat="server" MaxLength="10"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <%--<td style="background-color:#f5f5f5;">Measurement Location :</td>
                        <td style="text-align:center;">
                            <asp:DropDownList ID="ddlMeasureLocation" runat="server" CssClass="text-input small-input">
                            </asp:DropDownList>
                        </td>--%>
                        <td colspan="2"></td>
                        <td style="background-color:#f5f5f5;">Velocity</td>
                        <td><asp:TextBox ID="txtVelocity" runat="server" MaxLength="10"></asp:TextBox></td>
                        <td style="text-align:center;">mm/µs.</td>                        
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="propertyTable photograph_Sector">
                    
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;">
                            Photographic location of CUI inspection:
                        </td>
                        <td class="propertyGroup" style="border:none; padding-top:20px;">
                            Thickness measurement position:
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%; background-color:black; ">
                            <div class="File_Zoom_Mask" ID="ZoomMask1" runat="server" ></div>
                            <asp:Panel CssClass="File_Image" runat="server" ID="img_File1"></asp:Panel>                            
                            <div class="File_Command">
                                <asp:Button ID="btnRefresh1" runat="server" Text="" style="display:none;"/>
                                <asp:ImageButton id="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                <Ajax:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete1"></Ajax:ConfirmButtonExtender>
                            </div>  
                        </td>
                        <td style="width:50%; background-color:black; ">
                            <div class="File_Zoom_Mask" ID="ZoomMask2" runat="server" ></div>
                            <asp:Panel CssClass="File_Image" runat="server" ID="img_File2"></asp:Panel> 
                            <div class="File_Command">
                                <asp:Button ID="btnRefresh2" runat="server" Text="" style="display:none;"/>
                                <asp:ImageButton id="btnEdit2" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                <asp:ImageButton ID="btnDelete2" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                <Ajax:ConfirmButtonExtender ID="btnDelete2_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete2"></Ajax:ConfirmButtonExtender>
                            </div>  
                        </td>
                    </tr>                                 
                     <tr>
                        <td>
                            <asp:TextBox runat="server" ID="txtFileDesc1" TextMode="MultiLine" Rows="3" Width="100%" PlaceHolder="..." ToolTip="Note any information here" MaxLength="500"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFileDesc2" TextMode="MultiLine" Rows="3" Width="100%"  PlaceHolder="..." ToolTip="Note any information here" MaxLength="500"></asp:TextBox>
                        </td>
                    </tr>
                </table>

                <table cellpadding="0" cellspacing="0" class="propertyTable measurementGrid">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px; text-align:left;" colspan="5">
                           Thickness measurement data in mm:
                        </td>
                        <td class="propertyGroup" style="border:none; padding-top:20px; text-align:right;" colspan="5">
                           <asp:Button ID="btnClearURM" runat="server" CssClass="button" Text="Clear" />
                            <input type="button" id="btnPreUploadExcel" runat="server" class="button" value="Upload file" />
                            <asp:FileUpload ID="fulExcel" runat="server" style="display:none;"/>
                            <asp:Button id="btnUploadExcel" runat="server" style="display:none;"/>
                        </td>                       
                    </tr>
                    <tr>
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">1</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">2</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">3</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">4</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">5</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">6</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">7</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">8</td>
                        <td style="font-weight:bold; background-color:#f5f5f5;">9</td>
                    </tr>
                    <asp:Repeater ID="rptMeasure" runat="server">
                        <ItemTemplate>
                            <tr>
                              <td style="font-weight:bold; background-color:#f5f5f5;"><asp:Label ID="lblPoint" runat="server"></asp:Label></td>
                              <td><asp:TextBox ID="txt1" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt2" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt3" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt4" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt5" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt6" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt7" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt8" runat="server"></asp:TextBox></td>
                              <td><asp:TextBox ID="txt9" runat="server"></asp:TextBox></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate></FooterTemplate>
                    </asp:Repeater>
                    <asp:Button ID="btnCalculate" runat="server" style="display:none;" />
                    <tr>
                      <td>Average</td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG1" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG2" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG3" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG4" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG5" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG6" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG7" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG8" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="width:10%; background-color:#f5f5f5;"><asp:Label ID="lblAVG9" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                      <td>Maximum</td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX1" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX2" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX3" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX4" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX5" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX6" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX7" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX8" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:#f5f5f5;"><asp:Label ID="lblMAX9" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                      <td>Minimum</td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN1" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN2" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN3" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN4" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN5" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN6" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN7" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN8" runat="server" Font-Bold="true"></asp:Label></td>
                      <td style="background-color:lemonchiffon;"><asp:Label ID="lblMIN9" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="propertyTable">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="6">
                           Corrosion Rate & Remaining Life Calculation by Require Thickness = 
                            <asp:DropDownList ID="ddl_Treq" runat="server" AutoPostBack="true" style="width:auto; background-color:yellow;">
                                <asp:ListItem Text="(Norminal Thickness) - (Corrosion Allowance)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness) + (Corrosion Allowance)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness)" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="propertyGroup" colspan="3">
                          <asp:RadioButton ID="rdoShort"  runat="server" GroupName="Term" AutoPostBack="true" /> Short Term
                        </td>
                        <td class="propertyGroup" colspan="3">
                           <asp:RadioButton ID="rdoLong"  runat="server" GroupName="Term" AutoPostBack="true" /> Long Term
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement thickness(Tlast)</b>:</td>
                        <td style="text-align:center;"><asp:LinkButton ID="lbl_s_LastThick" runat="server"  Font-Bold="true"></asp:LinkButton></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;"><b>Initial thickness(Tinitail)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement year</b></td>
                        <td style="text-align:center;"><asp:LinkButton ID="lbl_s_LastYear" runat="server"  Font-Bold="true"></asp:LinkButton></td>
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="background-color:#f5f5f5;"><b>Initial year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastYear" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tlast and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tinitail and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_S_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center; width:7%;">mm.</td>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_l_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="width:7%; text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                </table>

                <h3 style="padding-top:20px;">Note: </h3>
				<asp:TextBox Width="100%" TextMode="MultiLine" ID="txt_Note"  Height="100px" CssClass="text-input" MaxLength="500" runat="server"></asp:TextBox>
			
                  

                <h3 style="margin-top:20px;">Officer</h3>
                                    <p>
                                    <label class="column-left" style="width:120px;">
                                    Prepare By :
                                    </label>
                                    <asp:DropDownList ID="cmbCollector" runat="server" CssClass="text-input small-input" Font-Bold="True" Width="250px">
                                    </asp:DropDownList>
                                    </p>
                                    <p>
                                    <label class="column-left" style="width:120px;">
                                    Evaluate By :
                                    </label>
                                    <asp:DropDownList ID="cmbInspector" runat="server" CssClass="text-input small-input" Font-Bold="True" Width="250px">
                                    </asp:DropDownList>
                                    </p>
                                    <p>
                                    <label class="column-left" style="width:120px;">
                                    Approve By :
                                    </label>
                                    <asp:DropDownList ID="cmbAnalyst" runat="server" CssClass="text-input small-input" Font-Bold="True" Width="250px">
                                    </asp:DropDownList>
                                    </p>
                 <div class="clear"></div>

                 <p align="right">
                        <asp:Button id="btnHome" runat="server" CssClass="button" Text="All CUI reports" />
                        <asp:Button id="btnBack" runat="server" CssClass="button" Text="Back step (After remove)" />
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save Only" />
                        <asp:Button ID="btnNext" runat="server" CssClass="button" Text="Next to After Repair" />
                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Posted to Finish on This Step" />
                  </p>
                               
                  <!-- End .clear -->    

            </div>

            <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                    ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                <div>
                    <asp:Label ID="lblValidation" runat="server"></asp:Label>                    
                </div>
            </asp:Panel>

            <uc1:GL_DialogInputValue ID="DialogInput" runat="server" Visible="false" />
        </div>
    </div><!-- End Content Box -->

    <asp:TextBox ID="txtLastControl" runat="server" style="display:none;"></asp:TextBox>
    
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
