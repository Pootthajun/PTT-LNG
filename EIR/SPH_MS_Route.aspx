﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="SPH_MS_Route.aspx.vb" Inherits="EIR.SPH_MS_Route" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
        			<!-- Page Head -->
			<h2>Setting Route for spring hanger</h2>
			
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
                
                <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                </asp:DropDownList>
    	        <div class="clear"></div>
              </div>
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListRoute" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Plant Code </a> </th>
                        <th><a href="#">Route Code </a></th>                        
                        <th><a href="#">Route Name </a></th>
                        <th><a href="#">Location</a></th>
                        <th><a href="#">Spring(s)</a></th>                        
                        <th><a href="#">Description</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Updated</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>                   
                     <asp:Repeater ID="rptRoute" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                              <tr>
                                <td><asp:Label ID="lblPlantCode" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblRouteCode" runat="server"></asp:Label></td>                                
                                <td><asp:Label ID="lblRouteName" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                <td style="text-align:center;"><asp:Label ID="lblTag" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDesc" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                <td><!-- Icons -->
                                  <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                  <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                  
                               </td>
                              </tr>
                      
                            </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="9">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>   
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				   </asp:Panel>
			        
			     <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
				 <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Route for spring hanger</h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                     <p>&nbsp;</p>
                     <p>
                      <label class="column-left" style="width:120px;" >Define for Plant : </label>
                        <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                             ID="ddl_Edit_Plant" runat="server">
                        </asp:DropDownList>
                    </p>
                    
                   <p>
                        <label class="column-left" style="width:120px;" >Route Code : </label>
                        <asp:TextBox runat="server" ID="txtRouteCode" CssClass="text-input small-input " MaxLength="20"></asp:TextBox>
                    </p>
                      <p>
                      <label class="column-left" style="width:120px;" >Route Name : </label>
                      <asp:TextBox runat="server" ID="txtRouteName" CssClass="text-input small-input " MaxLength="100"></asp:TextBox>
                    </p>
					
					 <p>
                      <label class="column-left" style="width:120px;" >Location : </label>
                      <asp:TextBox runat="server" ID="txtLocation" CssClass="text-input small-input " MaxLength="100"></asp:TextBox>
                    </p>
					<p>
						<label style="width:300px;" >Description: </label>
						<asp:TextBox runat="server" ID="txtDesc" TextMode="MultiLine" CssClass="text-input" Width="100%" Height="80px" MaxLength="500"></asp:TextBox>
					</p>
					 <p>
                      <label class="column-left" style="width:120px;" >Available : </label>
                      &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
                    </p>
				    
				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                      
                   <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset>
                </asp:Panel>
                </div>
                <!-- End #tab1 -->
				 
					
			  </div> <!-- End .content-box-content -->
				
		  </div> 
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>

