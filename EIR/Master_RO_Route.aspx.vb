﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_RO_Route
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetRoute(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindRoute()

        Dim SQL As String = "SELECT " & vbNewLine
        SQL &= " MS_RO_ROUTE.ROUTE_ID," & vbNewLine
        SQL &= "  ROUTE_Code," & vbNewLine
        SQL &= " ROUTE_Name," & vbNewLine
        SQL &= " PLANT_Code," & vbNewLine
        SQL &= " COUNT(TAG_ID) TotalTag," & vbNewLine
        SQL &= " MS_RO_ROUTE.active_status," & vbNewLine
        SQL &= " MS_RO_ROUTE.Update_By," & vbNewLine
        SQL &= " MS_RO_ROUTE.Update_Time" & vbNewLine
        SQL &= " FROM MS_RO_ROUTE" & vbNewLine
        SQL &= " LEFT JOIN MS_Plant ON MS_Plant.PLANT_ID=MS_RO_ROUTE.PLANT_ID " & vbNewLine
        SQL &= " LEFT JOIN MS_RO_TAG ON MS_RO_TAG.ROUTE_ID=MS_RO_ROUTE.ROUTE_ID" & vbNewLine
        Dim WHERE As String = ""
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_Plant.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If
        SQL &= " GROUP BY  MS_RO_ROUTE.ROUTE_ID," & vbNewLine
        SQL &= " ROUTE_Code," & vbNewLine
        SQL &= " ROUTE_Name," & vbNewLine
        SQL &= " PLANT_Code," & vbNewLine
        SQL &= " MS_RO_ROUTE.active_status," & vbNewLine
        SQL &= " MS_RO_ROUTE.Update_By," & vbNewLine
        SQL &= " MS_RO_ROUTE.Update_Time" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("MS_RO_ROUTE") = DT

        Navigation.SesssionSourceName = "MS_RO_ROUTE"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptRoute
    End Sub

    Protected Sub rptRoute_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptRoute.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRouteCode As Label = e.Item.FindControl("lblRouteCode")
        Dim lblPlantCode As Label = e.Item.FindControl("lblPlantCode")
        Dim lblRouteName As Label = e.Item.FindControl("lblRouteName")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblRouteCode.Text = e.Item.DataItem("Route_Code")
        lblPlantCode.Text = e.Item.DataItem("Plant_Code")
        lblRouteName.Text = e.Item.DataItem("Route_Name")
        lblTag.Text = e.Item.DataItem("TotalTag")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("Route_ID") = e.Item.DataItem("Route_ID")

    End Sub

    Protected Sub rptRoute_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptRoute.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Route_ID As Integer = btnEdit.Attributes("Route_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'txtRouteCode.ReadOnly = True
                txtRouteName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListRoute.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_RO_ROUTE WHERE Route_ID=" & Route_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Route Not Found"
                    pnlBindingError.Visible = True
                    BindRoute()
                    Exit Sub
                End If

                txtRouteCode.Text = DT.Rows(0).Item("ROUTE_Code")
                txtRouteCode.Attributes("RouteID") = DT.Rows(0).Item("ROUTE_ID")
                txtRouteName.Text = DT.Rows(0).Item("ROUTE_Name")
                If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then
                    BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
                Else
                    BL.BindDDlPlant(ddl_Edit_Plant, False)
                End If
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_RO_ROUTE Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  ROUTE_ID=" & Route_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindRoute()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select


    End Sub

    Protected Sub ResetRoute(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindRoute()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        ClearPanelSearch()

        pnlListRoute.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtRouteCode.Text = ""
        txtRouteCode.Attributes("RouteID") = "0"
        txtRouteName.Text = ""
        BL.BindDDlPlant(ddl_Edit_Plant, False)
        chkAvailable.Checked = True

        btnCreate.Visible = True
    End Sub

    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Search_Plant)
        BindRoute()
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False

        txtRouteCode.ReadOnly = False
        txtRouteCode.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListRoute.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtRouteCode.Text = "" Then
            lblValidation.Text = "Please insert Route Code"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtRouteName.Text = "" Then
            lblValidation.Text = "Please insert Route Name"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Plant.SelectedIndex < 1 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim Route_ID As Integer = txtRouteCode.Attributes("RouteID")

        Dim SQL As String = "SELECT * FROM MS_RO_ROUTE WHERE Route_Code='" & txtRouteCode.Text & "' AND ROUTE_ID<>" & Route_ID & " AND PLANT_ID=" & ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Route Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_RO_ROUTE WHERE Route_ID=" & Route_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            Route_ID = GetNewRouteID()
            DR("ROUTE_ID") = Route_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("ROUTE_ID") = Route_ID
        DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
        DR("ROUTE_Code") = txtRouteCode.Text
        DR("ROUTE_Name") = txtRouteName.Text
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID") ' Remain
        DR("Update_Time") = Now


        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetRoute(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewRouteID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(Route_ID),0)+1 FROM MS_RO_ROUTE "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function


    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BindRoute()
    End Sub

End Class