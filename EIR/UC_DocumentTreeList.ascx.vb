﻿Public Class UC_DocumentTreeList
    Inherits System.Web.UI.UserControl

    Public Event NodeAdded(ByRef sender As UC_DocumentTreeList, ByRef AddedNode As UC_DocumentTreeNode)
    Public Event NodeRemoved(ByRef sender As UC_DocumentTreeList, ByVal RemovedNodeData As DataTable)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub SetExpandCollapseTree()
        Dim DT As DataTable = CurrrentDocumentsData

    End Sub

    Private Function BlankNodesData() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("NodeID", GetType(Integer))
        DT.Columns.Add("NodeName", GetType(String))
        DT.Columns.Add("NodeStatus", GetType(String))
        DT.Columns.Add("Level", GetType(Integer))
        DT.Columns.Add("ParentID", GetType(Integer))
        DT.Columns.Add("PlanDate", GetType(Date))
        DT.Columns.Add("FirstUploadDate", GetType(Date))
        DT.Columns.Add("LastUpdate", GetType(Date))
        DT.Columns.Add("IconURL", GetType(String))
        DT.Columns.Add("IsExpanded", GetType(Boolean))
        DT.TableName = UniqueID
        Return DT
    End Function

    Public ReadOnly Property CurrrentDocumentsData As DataTable
        Get
            Dim DT As DataTable = BlankNodesData()
            For Each Item As RepeaterItem In rpt.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim DR As DataRow = DT.NewRow
                Dim DocItem As UC_DocumentTreeNode = Item.FindControl("DocItem")
                With DocItem
                    DR("NodeID") = .NodeID
                    DR("NodeName") = .NodeName
                    DR("NodeStatus") = .NodeStatus
                    DR("Level") = .Level
                    DR("ParentID") = .ParentID
                    If Not IsDBNull(.NoticeDate) Then DR("PlanDate") = .NoticeDate
                    If Not IsDBNull(.FirstUploadDate) Then DR("FirstUploadDate") = .FirstUploadDate
                    If Not IsDBNull(.LastUpdate) Then DR("LastUpdate") = .LastUpdate
                    DR("IconURL") = .IconURL
                    DR("IsExpanded") = .IsExpanded
                    DT.Rows.Add(DR)
                End With
            Next
            Return DT
        End Get
    End Property

    Public Function AddNode(ByVal NodeID As Integer, ByVal NodeName As String, NodeStatus As String,
                            ByVal ParentID As Integer, ByVal NoticeDate As Date, ByVal CriticalDate As Date, ByVal LastUpdate As Date,
                            ByVal IconURL As String, ByVal ExpandParent As Boolean) As UC_DocumentTreeNode


        '---------- Validation---------
        If NodeID <= 0 Then
            Dim ER As New Exception("NodeID must > 0")
            Throw ER
        End If
        If ParentID < 0 Then
            Dim ER As New Exception("ParentID must >= 0")
            Throw ER
        End If

        Dim DT As DataTable = CurrrentDocumentsData
        DT.DefaultView.RowFilter = "NodeID=" & NodeID & " and ParentID=" & ParentID
        If DT.DefaultView.Count > 0 Then
            Throw New Exception("NodeID:" & NodeID & " is already exists")
        End If
        DT.DefaultView.RowFilter = "NodeID=" & ParentID

        '----------- Get Parent Level -----------
        Dim NewLevel As Integer = 0
        If DT.DefaultView.Count > 0 Then
            NewLevel = DT.DefaultView(0).Item("Level") + 1
        ElseIf ParentID <> 0 Then
            Throw New Exception("ParentID:" & ParentID & " does not existed")
        End If

        Dim DR As DataRow = DT.NewRow
        '----------- Set Node Property -----------
        DR("NodeID") = NodeID
        DR("NodeName") = NodeName
        DR("NodeStatus") = NodeStatus
        DR("Level") = NewLevel
        DR("ParentID") = ParentID
        If Not IsNothing(NoticeDate) AndAlso NoticeDate <> DateTime.FromOADate(0) Then DR("PlanDate") = NoticeDate
        If Not IsNothing(CriticalDate) AndAlso CriticalDate <> DateTime.FromOADate(0) Then DR("FirstUploadDate") = CriticalDate
        If Not IsNothing(LastUpdate) AndAlso LastUpdate <> DateTime.FromOADate(0) Then DR("LastUpdate") = LastUpdate
        DR("IconURL") = IconURL
        DR("IsExpanded") = False
        '----------- Get New Item Position -------
        Dim NewPosition As Integer = GetNewChildPosition(ParentID)
        DT.Rows.InsertAt(DR, NewPosition) '---------- Appended Last Record ---------

        DT.DefaultView.RowFilter = ""
        rpt.DataSource = DT
        rpt.DataBind()

        Dim Node As UC_DocumentTreeNode = rpt.Items(NewPosition).FindControl("DocItem")
        '----------- Check Expand Parent -------------
        If Not IsNothing(Node.ParentNode) AndAlso ExpandParent Then
            Node.ParentNode.Expand()
        End If

        RaiseEvent NodeAdded(Me, Node) '------------ Raise Event To Parent ------------
        Return Node
    End Function

    Public Function RemoveNode(ByVal NodeID As Integer) As DataTable
        Dim DT As DataTable = CurrrentDocumentsData
        Dim Result As DataTable = DT.DefaultView.ToTable
        Result.Rows.Clear()
        DT.DefaultView.RowFilter = "NodeID=" & NodeID
        If DT.DefaultView.Count = 0 Then
            Throw New Exception("NodeID:" & NodeID & " is not found")
        End If
        Dim NodeIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
        '-------- Add NodeDataTo Result-------
        Result.Rows.Add(DT.DefaultView(0).Row.ItemArray)
        Result.Merge(Nodes(NodeIndex).ChildsDataAll)
        '-------- Add Childs Result-------
        For i As Integer = 0 To Result.Rows.Count - 1
            DT.DefaultView.RowFilter = "NodeID=" & Result.Rows(i).Item("NodeID")
            DT.DefaultView(0).Row.Delete()
        Next
        '--------- Rebind Data --------
        DT.DefaultView.RowFilter = ""
        rpt.DataSource = DT
        rpt.DataBind()

        RaiseEvent NodeRemoved(Me, Result) '------------ Raise Event To Parent ------------
        Return Result
    End Function

    Public Function RemoveAllNodes() As DataTable
        Dim DT As DataTable = CurrrentDocumentsData
        rpt.DataSource = Nothing
        rpt.DataBind()
        RaiseEvent NodeRemoved(Me, DT) '------------ Raise Event To Parent ------------
        Return DT
    End Function

    Public Sub ExpandToNode(ByVal NodeID As Integer)
        Dim DT As DataTable = CurrrentDocumentsData
        DT.DefaultView.RowFilter = "NodeID=" & NodeID
        If DT.DefaultView.Count = 0 Then
            Dim ER As New Exception("NodeID : 0 " & NodeID & " is not found.")
            Throw ER
            Exit Sub
        End If

        Dim NodeIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
        Dim TheNode As UC_DocumentTreeNode = Me.Nodes(NodeIndex)
        Dim ParentID As Integer = TheNode.ParentID
        While ParentID <> 0
            DT.DefaultView.RowFilter = "NodeID=" & ParentID
            If DT.DefaultView.Count = 0 Then Exit Sub
            NodeIndex = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            Nodes(NodeIndex).Expand()
            ParentID = Me.Nodes(NodeIndex).ParentID
        End While
        TheNode.Focus()
    End Sub

    Public Sub ExpandAll(Optional ByVal NodeID As Integer = 0)
        Dim N As List(Of UC_DocumentTreeNode)
        If NodeID = 0 Then
            N = Nodes '----------- All Nodes --------------
        Else
            Dim DT As DataTable = CurrrentDocumentsData
            DT.DefaultView.RowFilter = "NodeID=" & NodeID
            If DT.DefaultView.Count = 0 Then
                Dim ER As New Exception("NodeID : 0 " & NodeID & " is not found.")
                Throw ER
                Exit Sub
            End If
            N = Nodes(DT.Rows.IndexOf(DT.DefaultView(0).Row)).ChildsAll
        End If
        For i As Integer = 0 To N.Count - 1
            Dim Node As UC_DocumentTreeNode = N(i)
            Node.Expand()
            If i = N.Count - 1 Then Node.Focus()
        Next

    End Sub

    Private Sub rpt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpt.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim Node As UC_DocumentTreeNode = e.Item.FindControl("DocItem")
                With Node
                    .NodeID = e.Item.DataItem("NodeID")
                    .NodeName = e.Item.DataItem("NodeName").ToString
                    .NodeStatus = e.Item.DataItem("NodeStatus").ToString
                    .Level = e.Item.DataItem("Level")
                    .ParentID = e.Item.DataItem("ParentID")
                    If Not IsDBNull(e.Item.DataItem("PlanDate")) Then .NoticeDate = e.Item.DataItem("PlanDate")
                    If Not IsDBNull(e.Item.DataItem("FirstUploadDate")) Then .FirstUploadDate = e.Item.DataItem("FirstUploadDate")
                    If Not IsDBNull(e.Item.DataItem("LastUpdate")) Then .LastUpdate = e.Item.DataItem("LastUpdate")
                    .IconURL = e.Item.DataItem("IconURL").ToString
                    If Not IsDBNull(e.Item.DataItem("IsExpanded")) AndAlso e.Item.DataItem("IsExpanded") Then
                        .UpdateModeFlag(UC_DocumentTreeNode.ModeState.Expanded) '--------- Default Is NoChild-----------
                    End If
                End With
            Case ListItemType.Footer
                '------------ onBindingCompleted -----------
                Dim CH As List(Of UC_DocumentTreeNode) = Nodes
                For i As Integer = CH.Count - 1 To 0 Step -1
                    If CH(i).IsExpanded Then
                        CH(i).Expand()
                    ElseIf CH(i).Childs.Count > 0 Then
                        CH(i).Collapse()
                    Else
                        CH(i).UpdateMode(UC_DocumentTreeNode.ModeState.NoChild)
                    End If
                Next
        End Select

    End Sub

    Public ReadOnly Property RootNodes As List(Of UC_DocumentTreeNode)
        Get
            Dim Result As New List(Of UC_DocumentTreeNode)
            For Each Item As RepeaterItem In rpt.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim DocItem As UC_DocumentTreeNode = Item.FindControl("DocItem")
                If DocItem.ParentID = 0 Then
                    Result.Add(DocItem)
                End If
            Next
            Return Result
        End Get
    End Property

    Public ReadOnly Property Nodes As List(Of UC_DocumentTreeNode)
        Get
            Dim Result As New List(Of UC_DocumentTreeNode)
            For Each Item As RepeaterItem In rpt.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Result.Add(Item.FindControl("DocItem"))
            Next
            Return Result
        End Get
    End Property

    Public Function GetNewChildPosition(ByVal ParentID As Integer) As Integer

        Dim DT As DataTable = CurrrentDocumentsData
        DT.DefaultView.RowFilter = "NodeID=" & ParentID

        '----------- Get Parent Level -----------
        Dim ParentLevel As Integer = 0
        Dim NewLevel As Integer = 0
        If DT.DefaultView.Count > 0 Then
            ParentLevel = DT.DefaultView(0).Item("Level")
            NewLevel = ParentLevel + 1
        ElseIf ParentID <> 0 Then
            Throw New Exception("ParentID:" & ParentID & " does not existed")
        End If
        '----------- Get New Item Position -------
        Dim NewPosition As Integer
        If DT.DefaultView.Count > 0 Then '-----------  Existing Parent ----------
            Dim ParentPosition As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            NewPosition = ParentPosition + 1
            Dim FoundNextParent As Boolean = False
            For i As Integer = NewPosition To DT.Rows.Count - 1
                If DT.Rows(i).Item("Level") <= ParentLevel Then
                    NewPosition = i
                    FoundNextParent = True
                    Exit For
                End If
            Next
            If Not FoundNextParent Then NewPosition = DT.Rows.Count '-------- ต่อท้ายตาราง
        Else '------------ Have No Parent ------------
                NewPosition = DT.Rows.Count
        End If
        Return NewPosition

    End Function

End Class