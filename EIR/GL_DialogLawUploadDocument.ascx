﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogLawUploadDocument.ascx.vb" Inherits="EIR.GL_DialogLawUploadDocument" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Panel ID="pnlDialog" runat="server" BackColor="White" Width= "700">
    <table style="border-style: solid; border-width: 1px; width:100%; background-color:white" >
        <tr>
            <td>
                <h2><asp:Label ID="lblAction" runat="server"></asp:Label> Document </h2>
                Path : <asp:Label ID="lblDocPath" runat="server"></asp:Label>
                <asp:Label ID="lblFolderID" runat="server" Visible="false" Text="0" ></asp:Label>
                <asp:Label ID="lblDocumentID" runat="server" Visible="false" Text="0" ></asp:Label>
                 
                <div class="dialog-col-right" style="width:680px;height:420px; border:solid;border-width:1px;">
                    <table align="left"  width="100%" style="border-collapse:initial"   cellpadding="0" cellspacing="0">
                        <tbody >
                            <tr>
                                <td style="width:15%">&nbsp;</td>
                                <td style="width:85%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <b>Document Name</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_DocumentName" runat="server" CssClass="text-input" 
                                        MaxLength="255" Width="640px" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <b>Description</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txt_Description" runat="server" CssClass="text-input" 
                                        Height="30px" MaxLength="500" TextMode="MultiLine" Width="430px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <b>Browse File</b>
                                </td>
                                <td>
                                    <cc1:AsyncFileUpload ID="ful" runat="server" CompleteBackColor="DarkGreen" 
                                        ErrorBackColor="Red" ForeColor="white" Width="450px"  />
                                    <br />
                                    <asp:Label ID="lblUploadFileName" runat="server"></asp:Label>
                                    <asp:Label ID="lblTempFilePath" runat="server" Visible="false" ></asp:Label>
                                </td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td colspan="2">
                                    <table align="left"  width="100%" style="border:none;border-collapse: initial;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>Notice Date</td>
                                            <td>Critical Date</td>
                                            <td>Actual Date</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="txt_Notice_Date" AutoPostBack="false"
                                                    style="position:relative; top:5px; left: 0px;" 
                                                    CssClass="text-input small-input " Width="100px" MaxLength="15"></asp:TextBox>
    		                                    <cc1:CalendarExtender ID="txt_Notice_Date_CalendarExtender" runat="server" 
                                                    Format="dd-MMM-yyyy" TargetControlID="txt_Notice_Date">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txt_Critical_Date" AutoPostBack="false"
                                                    style="position:relative; top:5px; left: 0px;" 
                                                    CssClass="text-input small-input " Width="100px" MaxLength="15"></asp:TextBox>
    		                                    <cc1:CalendarExtender ID="txt_Critical_Date_CalendarExtender" runat="server" 
                                                    Format="dd-MMM-yyyy" TargetControlID="txt_Critical_Date">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblActualDate" runat="server" ></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td align="right" style="text-align:right;">
                                    <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" />
                &nbsp;
                                    <asp:Button ID="btnSave" runat="server" Class="button" Text="Save" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg" 
                                        Visible="false" >
                                        <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                            ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                        <div>
                                            <asp:Label ID="lblValidation" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Button ID="Button1" runat="server" Text="Button" Width="0px" style="display:none"  />
<cc1:ModalPopupExtender ID="dialogLowDocument" runat="server" 
    BackgroundCssClass="MaskDialog" Drag="true"  DropShadow="true" PopupControlID="pnlDialog" 
    TargetControlID="Button1">
</cc1:ModalPopupExtender>
