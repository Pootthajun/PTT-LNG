﻿Imports System.Data
Imports System.Data.SqlClient
Public Class MasterPage
    Inherits System.Web.UI.MasterPage

    Dim BL As New EIR_BL
    Dim CurrentVersion As String = ConfigurationManager.AppSettings.Item("CurrentVersion").ToString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblVersion.Text = CurrentVersion
            SetAuthorization()
            Select Case Page.TemplateControl.ToString
                Case "ASP." + "search_tag_aspx"
                    ExpandPane("mnu_Search")

                Case "ASP." + "master_area_aspx",
                     "ASP." + "master_plant_aspx",
                     "ASP." + "master_inspection_aspx",
                     "ASP." + "master_inspection_status_aspx",
                     "ASP." + "master_process_aspx",
                     "ASP." + "master_user_setting_aspx",
                     "ASP." + "master_utility_aspx"
                    ExpandPane("mnu_MS")

                    '----------- MENU Dashboard ----------------
                Case "ASP.dashboard_current_status_aspx",
                    "ASP" + ".Dashboard_Total_Problem_Report_aspx".ToLower(),
                    "ASP" + ".Dashboard_Summary_Route_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Summary_Route_aspx".ToLower(),
                    "ASP" + ".Dashboard_Summary_Plant_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Summary_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_Summary_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Summary_aspx".ToLower(),
                    "ASP" + ".Dashboard_SearchTagClass_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_SearchTagClass_aspx".ToLower(),
                    "ASP" + ".Dashboard_Problem_Report_Plant_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Problem_Report_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_Problem_Report_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Problem_Report_aspx".ToLower(),
                    "ASP" + ".Dashboard_New_Problem_Report_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_New_Problem_Report_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Sheet_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Sheet_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Tag_THM_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Tag_PdMA_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Tag_LO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Tag_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Plant_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Improvement_Report_aspx".ToLower(),
                    "ASP" + ".Dashboard_Detail_Plant_THM_aspx".ToLower(),
                    "ASP" + ".Dashboard_Detail_Plant_ST_aspx".ToLower(),
                    "ASP" + ".Dashboard_Detail_Plant_RO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Detail_Plant_PDMA_aspx".ToLower(),
                    "ASP" + ".Dashboard_Detail_Plant_LO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Detail_Plant_ALL_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Tag_THM_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Tag_ST_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Tag_RO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Tag_PdMA_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Tag_LO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Plant_ST_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Plant_RO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Area_ST_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_Area_RO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_AllTag_THM_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_AllTag_PdMA_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_AllTag_LO_aspx".ToLower(),
                    "ASP" + ".Dashboard_Current_Status_aspx".ToLower(),
                    "ASP" + ".Dashboard_Annual_Progress_Export_aspx".ToLower(),
                    "ASP" + ".Dashboard_Total_Problem_Report_Plant_aspx".ToLower(),
                    "ASP" + ".Dashboard_Annual_Progress_aspx".ToLower(),
                    "ASP" + ".Dashboard_Top10_Problem_aspx".ToLower()
                    ExpandPane("mnu_DB")
                    '----------- Set Dashboard Tooltip ----------------
                    script_dashboard_tooltip.Visible = True

                    '----------- MENU Station ----------------
                Case "ASP." + "stationary_routine_summary".ToLower() + "_aspx",
                     "ASP." + "stationary_routine_checksheet".ToLower() + "_aspx",
                     "ASP." + "stationary_routine_plan".ToLower() + "_aspx",
                     "ASP." + "stationary_routine_edit1".ToLower() + "_aspx",
                     "ASP." + "stationary_routine_edit2".ToLower() + "_aspx",
                     "ASP." + "stationary_routine_edit3".ToLower() + "_aspx",
                     "ASP." + "stationary_offroutine_summary".ToLower() + "_aspx",
                     "ASP." + "stationary_offroutine_edit1".ToLower() + "_aspx",
                     "ASP." + "stationary_offroutine_edit2".ToLower() + "_aspx",
                     "ASP." + "stationary_offroutine_edit3".ToLower() + "_aspx",
                     "ASP." + "stationary_offroutine_edit4".ToLower() + "_aspx",
                     "ASP." + "master_st_route".ToLower() + "_aspx",
                     "ASP." + "master_st_tagtype".ToLower() + "_aspx",
                     "ASP." + "master_st_tag".ToLower() + "_aspx",
                     "ASP." + "History_Search_Stationary".ToLower() + "_aspx"
                    ExpandPane("mnu_ST")

                Case "ASP." + "stationary_routine_summary" + "_aspx",
                     "ASP." + "stationary_routine_checksheet" + "_aspx",
                     "ASP." + "stationary_routine_plan" + "_aspx",
                     "ASP." + "stationary_routine_edit1" + "_aspx",
                     "ASP." + "stationary_routine_edit2" + "_aspx",
                     "ASP." + "stationary_routine_edit3" + "_aspx",
                     "ASP." + "stationary_offroutine_summary" + "_aspx",
                     "ASP." + "stationary_offroutine_edit1" + "_aspx",
                     "ASP." + "stationary_offroutine_edit2" + "_aspx",
                     "ASP." + "stationary_offroutine_edit3" + "_aspx",
                     "ASP." + "stationary_offroutine_edit4" + "_aspx",
                     "ASP." + "master_st_route" + "_aspx",
                     "ASP." + "master_st_tagtype" + "_aspx",
                     "ASP." + "master_st_tag" + "_aspx",
                     "ASP." + "history_search_stationary" + "_aspx"

                    ExpandPane("mnu_ST")

                    '----------- MENU Pipe ----------------
                Case "ASP." + "PIPE_Remaining_Life.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Routine_Report.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Summary.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit1.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit2.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit3.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit4.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit5.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit6.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_CUI_Edit7.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Tag.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Component_Type.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Material.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Pressure.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Service.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Service_Media.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_Insulation.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "PIPE_TM.aspx".Replace(".", "_").ToLower(),
                     "ASP." + "History_Search_PIPE.aspx".Replace(".", "_").ToLower()
                    ExpandPane("mnu_PIPE")

                    '----------- MENU Rotating ----------------

                Case "ASP." + "rotating_routine_summary_aspx".ToLower(),
                     "ASP." + "rotating_routine_checksheet_aspx".ToLower(),
                     "ASP." + "rotating_routine_plan_aspx".ToLower(),
                     "ASP." + "rotating_routine_edit1_aspx".ToLower(),
                     "ASP." + "rotating_routine_edit2_aspx".ToLower(),
                     "ASP." + "rotating_routine_edit3_aspx".ToLower(),
                     "ASP." + "rotating_routine_edit4_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_summary_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_edit1_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_edit2_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_edit3_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_edit4_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_edit5_aspx".ToLower(),
                     "ASP." + "rotating_offroutine_edit6_aspx".ToLower(),
                     "ASP." + "master_ro_route_aspx".ToLower(),
                     "ASP." + "master_ro_tagtype_aspx".ToLower(),
                     "ASP." + "master_ro_tag_aspx".ToLower(),
                     "ASP." + "History_Search_Rotating".ToLower() + "_aspx"
                    ExpandPane("mnu_RO")

                    '----------- MENU Spring Hangers & Supports Reports ----------------
                Case "ASP." + "spring_hanger_summary_aspx".ToLower(),
                     "ASP." + "sph_ms_route_aspx".ToLower(),
                     "ASP." + "sph_ms_spring_aspx".ToLower(),
                     "ASP." + "sph_summary_aspx".ToLower(),
                     "ASP." + "sph_edit1_aspx".ToLower(),
                     "ASP." + "sph_edit2_aspx".ToLower(),
                     "ASP." + "sph_edit3_aspx".ToLower(),
                     "ASP." + "sph_edit4_aspx".ToLower(),
                     "ASP." + "History_Search_SPH".ToLower() + "_aspx"
                    ExpandPane("mnu_SH")

                    '----------- MENU Lube Oil Reports ----------------
                Case "ASP." + "lo_summary_aspx".ToLower(),
                     "ASP." + "lo_routine_plan_aspx".ToLower(),
                     "ASP." + "lo_tag_aspx".ToLower(),
                     "ASP." + "lo_oil_type_aspx".ToLower(),
                     "ASP." + "lo_routine_edit1_aspx".ToLower(),
                     "ASP." + "lo_routine_edit2_aspx".ToLower(),
                     "ASP." + "lo_routine_edit3_aspx".ToLower(),
                     "ASP." + "History_Search_LO".ToLower() + "_aspx"
                    ExpandPane("mnu_LO")

                    '----------- MENU PdMA Reports ----------------
                Case "ASP." + "pdma_tag_aspx".ToLower(),
                     "ASP." + "pdma_route_aspx".ToLower(),
                     "ASP." + "pdma_plan_aspx".ToLower(),
                     "ASP." + "pdma_routine_summary_aspx".ToLower(),
                     "ASP." + "pdma_routine_edit1_aspx".ToLower(),
                     "ASP." + "pdma_routine_edit2_aspx".ToLower(),
                     "ASP." + "pdma_routine_edit3_aspx".ToLower(),
                     "ASP." + "History_Search_PDMA".ToLower() + "_aspx"
                    ExpandPane("mnu_PDMA")

                    '----------- MENU MTAP Reports ----------------
                Case "ASP." + "mtap_tag_aspx".ToLower(),
                     "ASP." + "mtap_plan_aspx".ToLower(),
                     "ASP." + "mtap_routine_summary_aspx".ToLower(),
                     "ASP." + "mtap_routine_edit1_aspx".ToLower(),
                     "ASP." + "mtap_routine_edit2_aspx".ToLower(),
                     "ASP." + "mtap_routine_edit3_aspx".ToLower(),
                     "ASP." + "History_Search_MTAP".ToLower() + "_aspx"
                    ExpandPane("mnu_MTAP")

                    '----------- MENU Thermography Reports ----------------
                Case "ASP." + "thm_routine_summary_aspx".ToLower(),
                     "ASP." + "thm_routine_edit1_aspx".ToLower(),
                     "ASP." + "thm_routine_edit2_aspx".ToLower(),
                     "ASP." + "thm_routine_edit3_aspx".ToLower(),
                     "ASP." + "thm_plan_aspx".ToLower(),
                     "ASP." + "thm_type_aspx".ToLower(),
                     "ASP." + "thm_route_aspx".ToLower(),
                     "ASP." + "thm_tag_aspx".ToLower(),
                     "ASP." + "History_Search_THM".ToLower() + "_aspx"

					 Case "ASP." + "rotating_routine_summary_aspx",
                     "ASP." + "rotating_routine_checksheet_aspx",
                     "ASP." + "rotating_routine_plan_aspx",
                     "ASP." + "rotating_routine_edit1_aspx",
                     "ASP." + "rotating_routine_edit2_aspx",
                     "ASP." + "rotating_routine_edit3_aspx",
                     "ASP." + "rotating_routine_edit4_aspx",
                     "ASP." + "rotating_offroutine_summary_aspx",
                     "ASP." + "rotating_offroutine_edit1_aspx",
                     "ASP." + "rotating_offroutine_edit2_aspx",
                     "ASP." + "rotating_offroutine_edit3_aspx",
                     "ASP." + "rotating_offroutine_edit4_aspx",
                     "ASP." + "rotating_offroutine_edit5_aspx",
                     "ASP." + "rotating_offroutine_edit6_aspx",
                     "ASP." + "master_ro_route_aspx",
                     "ASP." + "master_ro_tagtype_aspx",
                     "ASP." + "master_ro_tag_aspx",
                     "ASP." + "history_search_rotating" + "_aspx"
                    ExpandPane("mnu_RO")

                    '----------- MENU Spring Hangers & Supports Reports ----------------
                Case "ASP." + "spring_hanger_summary_aspx",
                     "ASP." + "sph_ms_route_aspx",
                     "ASP." + "sph_ms_spring_aspx",
                     "ASP." + "sph_summary_aspx",
                     "ASP." + "sph_edit1_aspx",
                     "ASP." + "sph_edit2_aspx",
                     "ASP." + "sph_edit3_aspx",
                     "ASP." + "sph_edit4_aspx",
                     "ASP." + "history_search_sph" + "_aspx"
                    ExpandPane("mnu_SH")

                    '----------- MENU Lube Oil Reports ----------------
                Case "ASP." + "lo_summary_aspx",
                     "ASP." + "lo_routine_plan_aspx",
                     "ASP." + "lo_tag_aspx",
                     "ASP." + "lo_oil_type_aspx",
                     "ASP." + "lo_routine_edit1_aspx",
                     "ASP." + "lo_routine_edit2_aspx",
                     "ASP." + "lo_routine_edit3_aspx",
                     "ASP." + "history_search_lo" + "_aspx"
                    ExpandPane("mnu_LO")

                    '----------- MENU PdMA Reports ----------------
                Case "ASP." + "pdma_tag_aspx",
                     "ASP." + "pdma_route_aspx",
                     "ASP." + "pdma_plan_aspx",
                     "ASP." + "pdma_routine_summary_aspx",
                     "ASP." + "pdma_routine_edit1_aspx",
                     "ASP." + "pdma_routine_edit2_aspx",
                     "ASP." + "pdma_routine_edit3_aspx",
                     "ASP." + "history_search_pdma" + "_aspx"
                    ExpandPane("mnu_PDMA")

                    '----------- MENU MTAP Reports ----------------
                Case "ASP." + "mtap_tag_aspx",
                     "ASP." + "mtap_plan_aspx",
                     "ASP." + "mtap_routine_summary_aspx",
                     "ASP." + "mtap_routine_edit1_aspx",
                     "ASP." + "mtap_routine_edit2_aspx",
                     "ASP." + "mtap_routine_edit3_aspx",
                     "ASP." + "history_search_mtap" + "_aspx"
                    ExpandPane("mnu_MTAP")

                    '----------- MENU Thermography Reports ----------------
                Case "ASP." + "thm_routine_summary_aspx",
                     "ASP." + "thm_routine_edit1_aspx",
                     "ASP." + "thm_routine_edit2_aspx",
                     "ASP." + "thm_routine_edit3_aspx",
                     "ASP." + "thm_plan_aspx",
                     "ASP." + "thm_type_aspx",
                     "ASP." + "thm_route_aspx",
                     "ASP." + "thm_tag_aspx",
                     "ASP." + "history_search_thm" + "_aspx"

                    ExpandPane("mnu_THM")

                    '----------- MENU Law Document ----------------
                Case "ASP.law_document_summary_aspx",
                     "ASP.law_document_plan_aspx",
                     "ASP.law_document_template_setting_aspx",
                     "ASP.law_organization_setting_aspx",
                     "ASP.law_document_summary_detail_aspx"
                    ExpandPane("mnu_LAW")

                    '----------- MENU Turnaround & Shutdown Report  TURN ----------------
                Case "ASP.st_ta_inspection_summary_aspx".ToLower(),
                     "ASP.st_ta_inspection_plan_aspx".ToLower(),
                     "ASP.st_ta_inspection_plan_edit_aspx".ToLower(),
                     "ASP.st_ta_ms_tagtype_aspx".ToLower(),
                     "ASP.st_ta_ms_tag_aspx".ToLower(),
                     "ASP.ST_TA_MS_Tag_UTM_aspx".ToLower(),
                     "ASP" + ".ST_TA_Inspection_Edit1_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_AfterClean_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_AfterRepair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_AsFound_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_Final_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_NDE_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_Repair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Absorber_Summary_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_AfterClean_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_AfterRepair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_AsFound_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_Final_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_NDE_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_Repair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Column_Summary_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_AfterClean_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_AfterRepair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_AsFound_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_Final_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_NDE_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_Repair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Drum_Summary_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_AfterClean_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_AfterRepair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_AsFound_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_Final_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_NDE_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_Repair_aspx".ToLower(),
                     "ASP" + ".ST_TA_Filter_Summary_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_AfterClean_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_AfterRepair_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_AsFound_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_Final_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_NDE_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_Repair_aspx".ToLower(),
                     "ASP" + ".ST_TA_HeatExchnage_Summary_aspx".ToLower(),
                     "ASP" + ".ST_TA_Insp_Absorber_Edit2_aspx".ToLower(),
                     "ASP" + ".ST_TA_Insp_Column_Edit2_aspx".ToLower(),
                     "ASP" + ".ST_TA_Insp_Drum_Edit2_aspx".ToLower(),
                     "ASP" + ".ST_TA_Insp_Filter_Edit2_aspx".ToLower(),
                     "ASP" + ".ST_TA_Insp_HeatExchnager_Edit2_aspx".ToLower(),
                     "ASP" + ".ST_TA_MS_Tag_aspx".ToLower(),
                     "ASP" + ".ST_TA_MS_TagType_aspx".ToLower(),
                     "ASP" + ".ST_TA_Inspection_Plan_aspx".ToLower(),
                     "ASP" + ".ST_TA_Inspection_Plan_Edit_aspx".ToLower(),
                     "ASP" + ".ST_TA_Inspection_Summary_aspx".ToLower()
                    ExpandPane("mnu_ST_TA")

            End Select

            LeftMenu.Attributes("style") = ""

            ''---------------- Save User Stat ---------------------
            'If Not IsNothing(Session("USER_ID")) Then
            '    BL.SaveLog(Session("USER_ID"), Session.SessionID, GetCurrentPageName, GetIPAddress)
            'End If

        End If


    End Sub

    Private Sub ExpandPane(ByVal PaneID As String)
        Dim CurIndex As Integer = -1
        For i As Integer = 0 To LeftMenu.Panes.Count - 1
            If LeftMenu.Panes(i).Visible Then
                CurIndex += 1
                If LeftMenu.Panes(i).ID = PaneID Then
                    LeftMenu.SelectedIndex = CurIndex
                    Exit Sub
                End If
            End If
        Next
    End Sub

    Private Sub SetAuthorization()
        'lblUserLogin.Text = Session("Gender") & Session("USER_Full_Name")
        lblUserLogin.Text = Session("USER_Full_Name")
        lblUserLevel.Text = BL.Get_User_Level_Name(Session("USER_LEVEL"))

        Dim Level As EIR_BL.User_Level = Session("USER_LEVEL")
        Select Case Level
            Case EIR_BL.User_Level.Administrator

                LeftMenu.Panes("mnu_MS").Visible = True '---------- Header -------------
                mnu_MS_Plant.Visible = True
                'mnu_MS_Route.Visible = True
                mnu_MS_Area.Visible = True
                mnu_MS_Inspection.Visible = True
                mnu_MS_Inspection_Status.Visible = True

                mnu_MS_Process.Visible = True
                'mnu_MS_TagType.Visible = True
                'mnu_MS_Tag.Visible = True

                mnu_SPH_MS_Route.Visible = True
                mnu_SPH_MS_Spring.Visible = True

                mnu_MS_User.Visible = True
                mnu_MS_Utility.Visible = True

                LeftMenu.Panes("mnu_ST").Visible = True '---------- Header -------------
                mnu_SR_Summary.Visible = True
                mnu_SR_Master.Visible = True
                'mnu_SR_CheckSheet.Visible = True
                mnu_MS_ST_Route.Visible = True
                mnu_MS_ST_TagType.Visible = True
                mnu_MS_ST_Tag.Visible = True

                mnu_PIPE_RemainigLife.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Routine.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_CUI.Visible = True '-------------- Pipe ---------------------
                'mnu_PIPE_ERO.Visible = True '-------------- Pipe ---------------------

                mnu_PIPE_Tag.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Component_Type.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Material.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Pressure.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service_Media.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Insulation.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_TM.Visible = True '-------------- Pipe ---------------------

                LeftMenu.Panes("mnu_RO").Visible = True '---------- Header -------------
                mnu_RR_Summary.Visible = True
                mnu_RR_Master.Visible = True
                'mnu_RR_CheckSheet.Visible = True
                mnu_MS_RO_Route.Visible = True
                mnu_MS_RO_TagType.Visible = True
                mnu_MS_RO_Tag.Visible = True

                LeftMenu.Panes("mnu_SH").Visible = True '---------- Header -------------
                mnu_SH_Summary.Visible = True
                mnu_SPH_MS_Route.Visible = True
                mnu_SPH_MS_Spring.Visible = True

            Case EIR_BL.User_Level.Approver

                LeftMenu.Panes("mnu_MS").Visible = False

                LeftMenu.Panes("mnu_ST").Visible = True '---------- Header -------------
                mnu_SR_Summary.Visible = True
                mnu_SR_Master.Visible = False
                'mnu_SR_CheckSheet.Visible = False
                mnu_MS_ST_Route.Visible = True
                mnu_MS_ST_TagType.Visible = True
                mnu_MS_ST_Tag.Visible = True

                mnu_PIPE_RemainigLife.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Routine.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_CUI.Visible = True '-------------- Pipe ---------------------
                'mnu_PIPE_ERO.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Tag.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Component_Type.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Material.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Pressure.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service_Media.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Insulation.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_TM.Visible = True '-------------- Pipe ---------------------

                LeftMenu.Panes("mnu_RO").Visible = True '---------- Header -------------
                mnu_RR_Summary.Visible = True
                mnu_RR_Master.Visible = False
                'mnu_RR_CheckSheet.Visible = False
                mnu_MS_RO_Route.Visible = True
                mnu_MS_RO_TagType.Visible = True
                mnu_MS_RO_Tag.Visible = True

                LeftMenu.Panes("mnu_SH").Visible = True '---------- Header -------------
                mnu_SH_Summary.Visible = True
                mnu_SPH_MS_Route.Visible = True
                mnu_SPH_MS_Spring.Visible = True

            Case EIR_BL.User_Level.Inspector

                LeftMenu.Panes("mnu_MS").Visible = False '---------- Header -------------

                LeftMenu.Panes("mnu_ST").Visible = True '---------- Header -------------
                mnu_SR_Summary.Visible = True
                mnu_SR_Master.Visible = True
                'mnu_SR_CheckSheet.Visible = True
                mnu_MS_ST_Route.Visible = True
                mnu_MS_ST_TagType.Visible = True
                mnu_MS_ST_Tag.Visible = True

                mnu_PIPE_RemainigLife.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Routine.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_CUI.Visible = True '-------------- Pipe ---------------------
                'mnu_PIPE_ERO.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Tag.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Component_Type.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Material.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Pressure.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service_Media.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Insulation.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_TM.Visible = True '-------------- Pipe ---------------------

                LeftMenu.Panes("mnu_RO").Visible = True '---------- Header -------------
                mnu_RR_Summary.Visible = True
                mnu_RR_Master.Visible = True
                'mnu_RR_CheckSheet.Visible = True
                mnu_MS_RO_Route.Visible = True
                mnu_MS_RO_TagType.Visible = True
                mnu_MS_RO_Tag.Visible = True

                LeftMenu.Panes("mnu_SH").Visible = True '---------- Header -------------
                mnu_SH_Summary.Visible = True
                mnu_SPH_MS_Route.Visible = True
                mnu_SPH_MS_Spring.Visible = True

            Case EIR_BL.User_Level.Collector

                LeftMenu.Panes("mnu_MS").Visible = False '---------- Header -------------

                LeftMenu.Panes("mnu_ST").Visible = True '---------- Header -------------
                mnu_SR_Summary.Visible = True
                mnu_SR_Master.Visible = False
                'mnu_SR_CheckSheet.Visible = True
                mnu_MS_ST_Route.Visible = False
                mnu_MS_ST_TagType.Visible = False
                mnu_MS_ST_Tag.Visible = False

                mnu_PIPE_RemainigLife.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Routine.Visible = True '-------------- Pipe -------------------
                mnu_PIPE_CUI.Visible = True '-------------- Pipe ---------------------
                'mnu_PIPE_ERO.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Tag.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Component_Type.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Material.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Pressure.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service_Media.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Insulation.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_TM.Visible = True '-------------- Pipe ---------------------

                LeftMenu.Panes("mnu_RO").Visible = True '---------- Header -------------
                mnu_RR_Summary.Visible = True
                mnu_RR_Master.Visible = False
                'mnu_RR_CheckSheet.Visible = True
                mnu_MS_RO_Route.Visible = False
                mnu_MS_RO_TagType.Visible = False
                mnu_MS_RO_Tag.Visible = False

                LeftMenu.Panes("mnu_SH").Visible = True '---------- Header -------------
                mnu_SH_Summary.Visible = True
                mnu_SPH_MS_Route.Visible = True
                mnu_SPH_MS_Spring.Visible = True

            Case EIR_BL.User_Level.Viewer

                LeftMenu.Panes("mnu_MS").Visible = False '---------- Header -------------

                LeftMenu.Panes("mnu_ST").Visible = True '---------- Header -------------
                mnu_SR_Summary.Visible = True
                mnu_SR_Master.Visible = False
                'mnu_SR_CheckSheet.Visible = False
                mnu_MS_ST_Route.Visible = False
                mnu_MS_ST_TagType.Visible = False
                mnu_MS_ST_Tag.Visible = False

                mnu_PIPE_RemainigLife.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Routine.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_CUI.Visible = True '-------------- Pipe ---------------------
                'mnu_PIPE_ERO.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Tag.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Component_Type.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Material.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Pressure.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Service_Media.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_Insulation.Visible = True '-------------- Pipe ---------------------
                mnu_PIPE_TM.Visible = True '-------------- Pipe ---------------------

                LeftMenu.Panes("mnu_RO").Visible = True '---------- Header -------------
                mnu_RR_Summary.Visible = True
                mnu_RR_Master.Visible = False
                'mnu_RR_CheckSheet.Visible = False
                mnu_MS_RO_Route.Visible = False
                mnu_MS_RO_TagType.Visible = False
                mnu_MS_RO_Tag.Visible = False

                LeftMenu.Panes("mnu_SH").Visible = True '---------- Header -------------
                mnu_SH_Summary.Visible = True
                mnu_SPH_MS_Route.Visible = False
                mnu_SPH_MS_Spring.Visible = False

            Case EIR_BL.User_Level.PTT_Authenticated
                ExpandPane("mnu_DB")
                mnu_DB.Visible = True
                mnu_Search.Visible = False
                mnu_ST.Visible = False
                mnu_PIPE.Visible = False '-------------- Pipe ---------------------
                mnu_RO.Visible = False
                mnu_SH.Visible = False
                mnu_LO.Visible = False
                mnu_PDMA.Visible = False
                mnu_THM.Visible = False
                mnu_MS.Visible = False

                mnu_db_CS.Visible = True
                mnu_db_AP.Visible = False
                'mnu_db_PR.Visible = False
                mnu_db_IR.Visible = False
                mnu_db_IS.Visible = False
                mnu_db_NPR.Visible = True
                mnu_db_TPR.Visible = False
                'mnu_db_TPRA.Visible = False
        End Select
        LeftMenu.Panes("mnu_DB").Visible = True
        mnu_Search.Visible = False

        '------------ Set For LNG------------
        mnu_PIPE.Visible = False
        mnu_RO.Visible = False
        mnu_SH.Visible = False
        mnu_LO.Visible = False
        mnu_PDMA.Visible = False
        mnu_MTAP.Visible = False
        mnu_THM.Visible = False
        mnu_LAW.Visible = False
        mnu_ST_TA.Visible = False
    End Sub

    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogout.Click

        If Not IsNothing(Session("USER_ID")) Then
            Dim COM As New SqlCommand
            Dim CON As New SqlConnection(BL.ConnStr)
            CON.Open()

            With COM
                .Connection = CON
                .CommandText = "UPDATE RPT_ST_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                .CommandText &= "UPDATE RPT_RO_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                .CommandText &= "UPDATE SPH_RPT_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf

                .CommandText &= "UPDATE RPT_MTAP_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                .CommandText &= "UPDATE RPT_PDMA_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                .CommandText &= "UPDATE RPT_THM_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                .CommandText &= "UPDATE RPT_LO_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                .CommandText &= "UPDATE RPT_PIPE_CUI_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                '.CommandText &= "UPDATE RPT_PIPE_ERO_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf
                '.CommandText &= "UPDATE RPT_PIPE_Routine_Header SET RPT_LOCK_BY=Null WHERE RPT_LOCK_BY=" & Session("USER_ID") & vbLf

                .CommandType = CommandType.Text
                .ExecuteNonQuery()
                .Dispose()
            End With
            CON.Close()
        End If


        Session.Abandon()
        Response.Redirect("Login.aspx", True)

    End Sub

End Class