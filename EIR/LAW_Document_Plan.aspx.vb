﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LAW_Document_Plan
    Inherits System.Web.UI.Page

    Dim CL As New LawClass
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetForm(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindDocumentPlan()
        Dim SearchYear As Integer = 0
        If ddlSearchYear.SelectedIndex > 0 Then
            SearchYear = ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value
        End If
        Dim DT As DataTable = CL.GetListDocumentAllPlan(ddlSearchDocTemplete.SelectedValue, SearchYear, ddlSearchPlant.SelectedValue, ddlSearchTag.SelectedValue)
        If DT.Rows.Count > 0 Then
            pnlBindingError.Visible = False

            Session("LAW_DOCUMENT_PLAN") = DT
            Navigation.SesssionSourceName = "LAW_DOCUMENT_PLAN"
            Navigation.RenderLayout()
        Else
            Session("LAW_DOCUMENT_PLAN") = Nothing
            Navigation.SesssionSourceName = "LAW_DOCUMENT_PLAN"
            Navigation.RenderLayout()
        End If
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub


    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDocumentPlanID As Label = e.Item.FindControl("lblDocumentPlanID")
        Dim lblDocumentName As Label = e.Item.FindControl("lblDocumentName")
        Dim lblDocumentYear As Label = e.Item.FindControl("lblDocumentYear")
        Dim lblPlantCode As Label = e.Item.FindControl("lblPlantCode")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblPercentComplete As Label = e.Item.FindControl("lblPercentComplete")

        lblDocumentPlanID.Text = e.Item.DataItem("document_plan_id")
        lblDocumentName.Text = e.Item.DataItem("document_name")
        lblDocumentYear.Text = e.Item.DataItem("document_year")
        lblPlantCode.Text = e.Item.DataItem("plant_code")
        If Convert.IsDBNull(e.Item.DataItem("tag_no")) = False Then lblTagNo.Text = e.Item.DataItem("tag_no")

        Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPlanStatus(e.Item.DataItem("document_plan_id"))
        Select Case pStatus.PlanStatus
            Case "WAITING"
                lblStatus.Text = "<span style='color:blue'><b>WAITING</b></span>"
            Case "NOTICE"
                lblStatus.Text = "<span style='color:orange'><b>NOTICE</b></span>"
            Case "CRITICAL"
                lblStatus.Text = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
            Case "COMPLETE"
                lblStatus.Text = "<span style='color:green'><b>COMPLETE</b></span>"
        End Select

        lblPercentComplete.Text = pStatus.PercentComplete
    End Sub

    Private Sub rptPlan_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        Dim lblSelectDocumentPlanID As Label = e.Item.FindControl("lblDocumentPlanID")

        Select Case e.CommandName
            Case "Edit"
                Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                lblUpdateMode.Text = "Update"

                '--------------Bind Value------------
                Dim DT As DataTable = CL.GetDataDocumentPlan(lblSelectDocumentPlanID.Text)
                If DT.Rows.Count > 0 Then
                    Dim dr As DataRow = DT.Rows(0)
                    lblDocumentPlanID.Text = dr("document_plan_id")
                    ddl_Edit_Document_Template.SelectedValue = dr("document_setting_id")
                    ddl_Edit_Document_Template_SelectedIndexChanged(Nothing, Nothing)
                    txtDocumentName.Text = dr("document_name")
                    txtDocumentYear.Text = dr("document_year")

                    ddlPlantID.SelectedValue = dr("plant_id")
                    ddlPlantID_SelectedIndexChanged(Nothing, Nothing)
                    ddlPlantArea.SelectedValue = dr("area_id")
                    ddlProcess.SelectedValue = dr("proc_id")
                    CL.BindDDLTagID(ddlTagID, ddlPlantID.SelectedValue, ddlPlantArea.SelectedValue, ddlProcess.SelectedValue)
                    ddlTagID.SelectedValue = dr("tag_id")
                End If
                DT.Dispose()
            Case "Delete"
                Dim ret As String = CL.DeleteDocumentPlant(lblSelectDocumentPlanID.Text)
                If ret = "true" Then
                    ResetForm(Nothing, Nothing)
                End If
        End Select
    End Sub

    Protected Sub ResetForm(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ClearPanelSearch()
        BindDocumentPlan()
        ''-----------------------------------
        ClearPanelEdit()
        ''----------------Bind Panel Search-------------------

        pnlListDocument.Enabled = True
    End Sub



    Private Sub ClearPanelEdit()
        lblDocumentPlanID.Text = "0"
        txtDocumentName.Text = ""
        txtDocumentYear.Text = ""
        ImplementJavaIntegerText(txtDocumentYear, False)
        CL.BindMasterDocument(ddl_Edit_Document_Template)
        BL.BindDDlPlant(ddlPlantID)
        BL.BindDDlProcess(ddlProcess)
        pnlEdit.Visible = False
        ddlPlantID.SelectedValue = 0
        ddlPlantID_SelectedIndexChanged(Nothing, Nothing)
        ddlPlantArea.SelectedValue = 0
        ddlProcess.SelectedValue = 0
        CL.BindDDLTagID(ddlTagID, ddlPlantID.SelectedValue, ddlPlantArea.SelectedValue, ddlProcess.SelectedValue)

        rptPaperList.DataSource = Nothing
        rptPaperList.DataBind()

        lblUpdateMode.Text = ""

        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")

        btnCreate.Visible = USER_LEVEL = EIR_BL.User_Level.Administrator Or
                            USER_LEVEL = EIR_BL.User_Level.Inspector Or
                            USER_LEVEL = EIR_BL.User_Level.Approver
    End Sub




#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "SELECT ISNULL(MIN(document_year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(document_year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM LAW_Document_Plan"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddlSearchYear.Items.Clear()
        ddlSearchYear.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddlSearchYear.Items.Add(i)
        Next
        ddlSearchYear.Text = Now.Year + 543

        CL.BindMasterDocument(ddlSearchDocTemplete)
        BL.BindDDlPlant(ddlSearchPlant, False)
        CL.BindDDLTagID(ddlSearchTag, 0, 0, 0)
    End Sub

    Private Sub ddlSearchPlant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchPlant.SelectedIndexChanged
        If ddlSearchPlant.SelectedIndex > -1 Then
            CL.BindDDLTagID(ddlSearchTag, ddlSearchPlant.SelectedValue, 0, 0)
            BindDocumentPlan()
        End If
    End Sub

    Private Sub ddlSearchDocTemplete_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchDocTemplete.SelectedIndexChanged
        If ddlSearchDocTemplete.SelectedIndex > -1 Then
            BindDocumentPlan()
        End If
    End Sub

    Private Sub ddlSearchYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchYear.SelectedIndexChanged
        If ddlSearchYear.SelectedIndex > -1 Then
            BindDocumentPlan()
        End If
    End Sub

    Private Sub ddlSearchTag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchTag.SelectedIndexChanged
        If ddlSearchTag.SelectedIndex > -1 Then
            BindDocumentPlan()
        End If
    End Sub


#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        lblUpdateMode.Text = "Create"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If ddl_Edit_Document_Template.SelectedIndex < 1 Then
            lblValidation.Text = "Please select document template"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtDocumentName.Text = "" Then
            lblValidation.Text = "Please input document name"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtDocumentYear.Text = "" Then
            lblValidation.Text = "Please input year"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddlPlantID.SelectedValue = 0 Then
            lblValidation.Text = "Please select plant"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddlPlantArea.SelectedValue > 0 Or ddlProcess.SelectedValue > 0 Then
            If ddlTagID.SelectedValue = 0 Then
                lblValidation.Text = "Please select Tag No"
                pnlValidation.Visible = True
                Exit Sub
            End If
        End If

        Dim totPercent As Integer = 0
        For Each rItem As RepeaterItem In rptPaperList.Items
            Dim txtPercentComplete As TextBox = rItem.FindControl("txtPercentComplete")
            Dim txtPlanNoticeDate As TextBox = rItem.FindControl("txtPlanNoticeDate")
            Dim txtPlanCriticalDate As TextBox = rItem.FindControl("txtPlanCriticalDate")

            If txtPercentComplete.Text = "" Then
                lblValidation.Text = "Please input %"
                pnlValidation.Visible = True
                Exit Sub
            End If

            If Not BL.IsProgrammingDate(txtPlanNoticeDate.Text) Then
                lblValidation.Text = "Please select Notice Date"
                pnlValidation.Visible = True
                Exit Sub
            End If

            If Not BL.IsProgrammingDate(txtPlanCriticalDate.Text) Then
                lblValidation.Text = "Please select Critical Date"
                pnlValidation.Visible = True
                Exit Sub
            End If

            totPercent += Convert.ToInt16(txtPercentComplete.Text)
        Next

        If totPercent <> 100 Then
            lblValidation.Text = "Invalid Percent Complete"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim TagTableName As String = ""
        If ddlTagID.SelectedValue > 0 Then
            TagTableName = "MS_ST_TAG"
        End If
        Dim ret() As String = Split(CL.SaveDocumentPlan(lblDocumentPlanID.Text, ddl_Edit_Document_Template.SelectedValue, txtDocumentName.Text, txtDocumentYear.Text, ddlPlantID.SelectedValue, TagTableName, ddlTagID.SelectedValue, BuiltPaperDT(), Session("USER_ID")), "|")
        If ret.Length = 2 Then
            If ret(0).ToLower = "true" Then
                ResetForm(Nothing, Nothing)
                lblBindingSuccess.Text = "Save successfully"
                pnlBindingSuccess.Visible = True
            End If
        End If

    End Sub


    Private Function BuiltPaperDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("document_setting_paper_id", GetType(Long))
        dt.Columns.Add("percent_complete", GetType(Integer))
        dt.Columns.Add("plan_notice_date", GetType(DateTime))
        dt.Columns.Add("plan_critical_date", GetType(DateTime))

        Dim dr As DataRow
        For Each rItem As RepeaterItem In rptPaperList.Items
            Dim txtPercentComplete As TextBox = rItem.FindControl("txtPercentComplete")
            Dim txtPlanNoticeDate As TextBox = rItem.FindControl("txtPlanNoticeDate")
            Dim txtPlanCriticalDate As TextBox = rItem.FindControl("txtPlanCriticalDate")
            Dim lblDocumentSettingPaperID As Label = rItem.FindControl("lblDocumentSettingPaperID")

            Dim c As New Converter
            dr = dt.NewRow
            dr("document_setting_paper_id") = lblDocumentSettingPaperID.Text
            dr("percent_complete") = txtPercentComplete.Text
            dr("plan_notice_date") = c.StringToDate(txtPlanNoticeDate.Text, "dd-MMM-yyyy")
            dr("plan_critical_date") = c.StringToDate(txtPlanCriticalDate.Text, "dd-MMM-yyyy")
            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Private Sub rptPaperList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPaperList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header

            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lblNo As Label = e.Item.FindControl("lblNo")
                Dim lbl_Paper_Name As Label = e.Item.FindControl("lbl_Paper_Name")
                '
                Dim lblOrgName As Label = e.Item.FindControl("lblOrgName")
                Dim txtPercentComplete As TextBox = e.Item.FindControl("txtPercentComplete")
                Dim txtPlanNoticeDate As TextBox = e.Item.FindControl("txtPlanNoticeDate")
                Dim txtPlanCriticalDate As TextBox = e.Item.FindControl("txtPlanCriticalDate")
                Dim lblDocumentSettingPaperID As Label = e.Item.FindControl("lblDocumentSettingPaperID")

                lblNo.Text = e.Item.ItemIndex + 1
                lbl_Paper_Name.Text = e.Item.DataItem("paper_name")
                lblOrgName.Text = e.Item.DataItem("org_name")
                If Convert.IsDBNull(e.Item.DataItem("percent_complete")) = False Then
                    txtPercentComplete.Text = e.Item.DataItem("percent_complete")
                End If

                If Convert.IsDBNull(e.Item.DataItem("plan_notice_date")) = False Then
                    txtPlanNoticeDate.Text = Convert.ToDateTime(e.Item.DataItem("plan_notice_date")).ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US"))
                End If

                If Convert.IsDBNull(e.Item.DataItem("plan_critical_date")) = False Then
                    txtPlanCriticalDate.Text = Convert.ToDateTime(e.Item.DataItem("plan_critical_date")).ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US"))
                End If
                lblDocumentSettingPaperID.Text = e.Item.DataItem("document_setting_paper_id")

                ImplementJavaIntegerText(txtPercentComplete, False)
            Case ListItemType.Footer

        End Select
    End Sub

    Private Sub ddl_Edit_Document_Template_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Edit_Document_Template.SelectedIndexChanged
        If ddl_Edit_Document_Template.SelectedValue <> 0 Then
            txtDocumentName.Text = ddl_Edit_Document_Template.SelectedItem.Text

            Dim dt As DataTable
            If lblDocumentPlanID.Text = "0" Then
                dt = CL.GetListDocumentSettingPaper(ddl_Edit_Document_Template.SelectedValue)
                dt.Columns.Add("plan_notice_date", GetType(DateTime))
                dt.Columns.Add("plan_critical_date", GetType(DateTime))
            Else
                dt = CL.GetListDocumentPlanPaper(lblDocumentPlanID.Text)
                If dt.Rows.Count = 0 Then
                    dt = CL.GetListDocumentSettingPaper(ddl_Edit_Document_Template.SelectedValue)
                    dt.Columns.Add("plan_notice_date", GetType(DateTime))
                    dt.Columns.Add("plan_critical_date", GetType(DateTime))
                    dt.Columns.Add("plan_upload_date", GetType(DateTime))
                End If
            End If

            If dt.Rows.Count > 0 Then
                rptPaperList.DataSource = dt
                rptPaperList.DataBind()
            Else
                rptPaperList.DataSource = Nothing
                rptPaperList.DataBind()
            End If
        Else
            rptPaperList.DataSource = Nothing
            rptPaperList.DataBind()
        End If
    End Sub

    Private Sub ddlPlantID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPlantID.SelectedIndexChanged
        If ddlPlantID.SelectedIndex > -1 Then
            BL.BindDDlArea(ddlPlantID.SelectedValue, ddlPlantArea)
            CL.BindDDLTagID(ddlTagID, ddlPlantID.SelectedValue, 0, ddlProcess.SelectedValue)
        End If
    End Sub

    Private Sub ddlPlantArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPlantArea.SelectedIndexChanged
        If ddlPlantArea.SelectedIndex > -1 Then
            CL.BindDDLTagID(ddlTagID, ddlPlantID.SelectedValue, ddlPlantArea.SelectedValue, ddlProcess.SelectedValue)
        End If
    End Sub

    Private Sub ddlProcess_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProcess.SelectedIndexChanged
        If ddlProcess.SelectedIndex > -1 Then
            CL.BindDDLTagID(ddlTagID, ddlPlantID.SelectedValue, ddlPlantArea.SelectedValue, ddlProcess.SelectedValue)
        End If
    End Sub


End Class