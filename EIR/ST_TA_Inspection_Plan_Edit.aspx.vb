﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ST_TA_Inspection_Plan_Edit
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Turnaround_Inspection_Reports

    Private Property Mode() As String
        Get
            If Request.QueryString("Mode") <> Nothing Then
                Return Request.QueryString("Mode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("Mode") = value
        End Set
    End Property

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '---------------Hide Dialog-------------------------


        DialogAbsorber.CloseDialog()
        DialogDrum.CloseDialog()
        DialogColumn.CloseDialog()
        DialogFilter.CloseDialog()
        DialogHeat_Exchnager.CloseDialog()

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")

            '----สร้างตารางเก็ยรายการที่เลือก
            Dim DT As DataTable
            DT = New DataTable
            DT.Columns.Add("TAG_ID", GetType(Int64))
            DT.Columns.Add("TAG_TYPE_ID", GetType(Int64))
            Session("DT_CurrentSelect") = DT

            If Mode = "Create" Then
                'Clear Form
                ClearPanelEdit()

            ElseIf Mode = "Edit" Then
                BindHeader()
                BindTag_Select()
            End If

            BindTag()
            ClearPanelSearch()
        End If

        HideValidator()

    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = True
        lblUpdateMode.Text = "Create"
        BL.BindDDlPlant(ddl_Edit_Plant)
        BL.BindDDlST_TA_TagType(ddl_Edit_Tag_Type)
        ddl_Edit_Tag_Type.Enabled = False
        txtStart.Text = ""
        txtStart.Attributes("RPT_Year") = "0"
        txtStart.Attributes("RPT_No") = "0"

        txtEstimate.Text = ""
        txtEnd.Text = ""
        ImplementJavaIntegerText(txtEstimate, False)

    End Sub

    Private Sub ClearPanelSearch()

        ckStatus_OK.Checked = False
        ckStatus_Abnormal.Checked = False
        ckStatus_ClassA.Checked = False
        ckStatus_ClassB.Checked = False
        ckStatus_ClassC.Checked = False
        ckStatus_ClassD.Checked = False
    End Sub

    Protected Sub txtStart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStart.TextChanged
        If Not BL.IsProgrammingDate(txtStart.Text) Then
            txtStart.Text = ""
            txtEstimate.Text = ""
            Exit Sub
        End If

        Dim StartDate As Date = DateTime.Parse(txtStart.Text)

        If Not BL.IsProgrammingDate(txtEnd.Text) Then
            If IsNumeric(txtEstimate.Text) Then
                Dim EndDate = DateAdd(DateInterval.Day, CInt(txtEstimate.Text) - 1, StartDate)
                txtEnd.Text = BL.ReportProgrammingDate(EndDate)
                Exit Sub
            Else
                txtEstimate.Text = ""
                txtEnd.Text = ""
                Exit Sub
            End If
        Else
            Dim EndDate As Date = DateTime.Parse(txtEnd.Text)
            txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, StartDate, EndDate) + 1, 0)
            Exit Sub
        End If

    End Sub




    Protected Sub txtEnd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEnd.TextChanged
        If Not BL.IsProgrammingDate(txtEnd.Text) Then
            txtEnd.Text = ""
            txtEstimate.Text = ""
            Exit Sub
        End If

        Dim EndDate As Date = DateTime.Parse(txtEnd.Text)

        If Not BL.IsProgrammingDate(txtStart.Text) Then
            If IsNumeric(txtEstimate.Text) Then
                Dim StartDate As Date = DateAdd(DateInterval.Day, -CInt(txtEstimate.Text) - 1, EndDate)
                txtStart.Text = BL.ReportProgrammingDate(StartDate)
                Exit Sub
            Else
                txtEstimate.Text = ""
                txtStart.Text = ""
                Exit Sub
            End If
        Else
            Dim StartDate As Date = DateTime.Parse(txtStart.Text)
            txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, StartDate, EndDate) + 1, 0)
            Exit Sub
        End If

    End Sub

    Protected Sub txtEstimate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEstimate.TextChanged
        If Not IsNumeric(txtEstimate.Text) Then
            txtEstimate.Text = ""
            If BL.IsProgrammingDate(txtStart.Text) Then
                txtEnd.Text = ""
                Exit Sub
            End If
            If BL.IsProgrammingDate(txtEnd.Text) Then
                txtStart.Text = ""
                Exit Sub
            End If
            Exit Sub
        End If

        If BL.IsProgrammingDate(txtStart.Text) Then
            Dim StartDate As Date = DateTime.Parse(txtStart.Text)
            Dim EndDate As Date = DateAdd(DateInterval.Day, CInt(txtEstimate.Text) - 1, StartDate)
            txtEnd.Text = BL.ReportProgrammingDate(EndDate)
            Exit Sub
        End If

        If BL.IsProgrammingDate(txtEnd.Text) Then
            Dim EndDate As Date = DateTime.Parse(txtEnd.Text)
            Dim StartDate As Date = DateAdd(DateInterval.Day, -CInt(txtEstimate.Text) - 1, EndDate)
            txtStart.Text = BL.ReportProgrammingDate(StartDate)
            Exit Sub
        End If

        txtStart.Text = ""
        txtEnd.Text = ""

    End Sub

    Private Sub BindHeader()
        pnlEdit.Visible = True
        lblUpdateMode.Text = "Update"

        '--------------Bind Value------------
        Dim SQL As String = "SELECT * " & vbNewLine
        SQL &= " FROM RPT_ST_TA_Header " & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Response.Redirect("ST_TA_Inspection_Plan.aspx?StatusPlan=Fail")
            Exit Sub

        Else
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")

            txtStart.Attributes("RPT_Year") = RPT_Year
            txtStart.Attributes("RPT_No") = RPT_No


        End If

        BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
        BL.BindDDlST_TA_TagType(ddl_Edit_Tag_Type, DT.Rows(0).Item("TAG_TYPE_ID"))

        txtStart.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("RPT_Period_Start"))
        txtEnd.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("RPT_Period_End"))
        txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, DT.Rows(0).Item("RPT_Period_Start"), DT.Rows(0).Item("RPT_Period_End")), 0)

        ImplementJavaIntegerText(txtEstimate, False)

    End Sub
    Private Sub BindTag_Select()

        Dim DT_CurrentSelect As DataTable = Session("DT_CurrentSelect")
        Dim SQL_Detail As String = "SELECT * FROM RPT_ST_TA_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA_Detail As New SqlDataAdapter(SQL_Detail, BL.ConnStr)
        Dim DT_Detail As New DataTable
        DA_Detail.Fill(DT_Detail)
        If (DT_Detail.Rows.Count > 0) Then
            Dim DR As DataRow
            For i As Integer = 0 To DT_Detail.Rows.Count - 1
                DR = DT_CurrentSelect.NewRow
                DR("TAG_ID") = DT_Detail.Rows(i).Item("TAG_ID")
                DR("TAG_TYPE_ID") = DT_Detail.Rows(i).Item("TAG_TYPE_ID")
                DT_CurrentSelect.Rows.Add(DR)

            Next
        End If

        Session("DT_CurrentSelect") = DT_CurrentSelect

    End Sub

    Private Sub BindTag()

        Dim SQL As String = " SELECT * FROM (" & vbNewLine
        SQL &= "   Select AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TAG.ROUTE_ID,MS_ST_TAG.AREA_ID,MS_ST_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TAG' To_Table " & vbNewLine
        SQL &= "   FROM MS_ST_TAG" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine

        SQL &= "   UNION ALL" & vbNewLine
        SQL &= "   Select  AREA_CODE +'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TA_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TA_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TA_TAG.ROUTE_ID,MS_ST_TA_TAG.AREA_ID,MS_ST_TA_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TA_TAG' To_Table " & vbNewLine
        SQL &= "   From MS_ST_TA_TAG" & vbNewLine
        SQL &= "   INNER Join MS_ST_TAG_TYPE On  MS_ST_TA_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER Join MS_ST_Route ON MS_ST_TA_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER Join MS_PLANT On MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER Join MS_Area ON MS_ST_TA_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER Join MS_Process On MS_ST_TA_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
        SQL &= " ) TB " & vbNewLine

        Dim WHERE As String = ""

        If ddl_Edit_Plant.SelectedIndex > 0 Then
            WHERE &= " TB.PLANT_ID=" & ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value & " And "
        Else
            WHERE &= " 0=1 And "

        End If

        If ddl_Edit_Tag_Type.SelectedIndex > 0 Then
            WHERE &= " TB.TAG_TYPE_ID=" & ddl_Edit_Tag_Type.Items(ddl_Edit_Tag_Type.SelectedIndex).Value & " And "
        Else
            WHERE &= " 0=1 And "

        End If

        'If txt_Search_Tag.Text <> "" Then
        '    WHERE &= " ( TB.TAG_CODE Like '%" & txt_Search_Tag.Text & "%'  OR "
        '    WHERE &= "  TB.TAG_Name Like '%" & txt_Search_Tag.Text & "%' )  And "
        'Else
        '    'WHERE &= " 0=1 And "
        'End If


        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("MS_ST_TA_TAG") = DT

        Navigation.SesssionSourceName = "MS_ST_TA_TAG"
        Navigation.RenderLayout()

    End Sub



    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim chkItem As CheckBox = e.Item.FindControl("chkItem")


        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagNo.Attributes("TAG_NO") = e.Item.DataItem("TAG_NO")
        lblTagName.Text = e.Item.DataItem("TAG_Name")
        lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")
        lblTagType.Attributes("TAG_TYPE_ID") = e.Item.DataItem("TAG_TYPE_ID")

        lblTo_Table.Text = e.Item.DataItem("To_Table")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
        End If

        '----ทำเครื่องหมายรายการที่ เลือก
        Dim DT As DataTable = Session("DT_CurrentSelect")
        DT.DefaultView.RowFilter = " TAG_ID =" & e.Item.DataItem("TAG_ID") & ""
        If DT.DefaultView.Count > 0 Then
            chkItem.Checked = True
        Else
            chkItem.Checked = False
        End If

        chkItem.Attributes("ck_TAG_ID") = e.Item.DataItem("TAG_ID")
        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

        'chkItem.Attributes("onchange") = "document.getElementById('" & btnSelect.ClientID & "').click();"

    End Sub

    Protected Sub chkItem_OnCheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = DirectCast(sender, CheckBox)

        Dim Item As RepeaterItem = chk.Parent
        Dim lblTagNo As Label = Item.FindControl("lblTagNo")
        Dim btnEdit As ImageButton = Item.FindControl("btnEdit")
        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")
        Dim lblTo_Table As Label = Item.FindControl("lblTo_Table")
        Dim To_Table As String = lblTo_Table.Text
        Dim lblTagType As Label = Item.FindControl("lblTagType")
        Dim chkItem As CheckBox = Item.FindControl("chkItem")

        '------หา รายการที่ chk/remove

        Dim DT As DataTable = GetCurrentSelect()

        Dim DR As DataRow
        If (chkItem.Checked) Then
            DR = DT.NewRow
            DR("TAG_ID") = Convert.ToInt64(TAG_ID)
            DR("TAG_TYPE_ID") = Convert.ToInt64(lblTagType.Attributes("TAG_TYPE_ID"))
            DT.Rows.Add(DR)
        Else
            Dim index_row As Integer = 0
            For i As Integer = 0 To DT.Rows.Count - 1
                If TAG_ID = DT.Rows(i)("TAG_ID") Then
                    DT.Rows.RemoveAt(i)
                    Exit For
                End If
            Next
        End If

        Session("DT_CurrentSelect") = DT
    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim chkItem As CheckBox = e.Item.FindControl("chkItem")

        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")
        Dim TAG_TYPE_ID As Integer = lblTagType.Attributes("TAG_TYPE_ID")
        Dim To_Table As String = lblTo_Table.Text

        Select Case e.CommandName
            Case "Edit"

                ''Select Case lblTagType.Attributes("TAG_TYPE_ID")
                ''    Case EIR_BL.ST_TAG_TYPE.Drum
                ''        DialogDrum.TAG_CODE = lblTagNo.Text
                ''        DialogDrum.TAG_ID = TAG_ID
                ''        DialogDrum.TAG_TYPE_ID = TAG_TYPE_ID
                ''        DialogDrum.TAG_Name = lblTagName.Text
                ''        DialogDrum.TAG_TYPE_Name = lblTagType.Text
                ''        DialogDrum.TO_TABLE = "MS_ST_DRUM_SPEC"
                ''        DialogDrum.BindHeader()
                ''        DialogDrum.ShowDialog()

                ''    Case EIR_BL.ST_TAG_TYPE.General_Stationary

                ''        DialogAbsorber.TO_TABLE = "MS_ST_ABSORBER_SPEC"
                ''        DialogColumn.ShowDialog()

                ''    Case EIR_BL.ST_TAG_TYPE.Absorber
                ''        DialogAbsorber.TAG_CODE = lblTagNo.Text
                ''        DialogAbsorber.TAG_ID = TAG_ID
                ''        DialogAbsorber.TAG_TYPE_ID = TAG_TYPE_ID
                ''        DialogAbsorber.TAG_Name = lblTagName.Text
                ''        DialogAbsorber.TAG_TYPE_Name = lblTagType.Text
                ''        DialogAbsorber.TO_TABLE = "MS_ST_ABSORBER_SPEC"
                ''        DialogAbsorber.BindHeader()
                ''        DialogAbsorber.ShowDialog()

                ''    Case EIR_BL.ST_TAG_TYPE.Column
                ''        DialogColumn.TAG_CODE = lblTagNo.Text
                ''        DialogColumn.TAG_ID = TAG_ID
                ''        DialogColumn.TAG_TYPE_ID = TAG_TYPE_ID
                ''        DialogColumn.TAG_Name = lblTagName.Text
                ''        DialogColumn.TAG_TYPE_Name = lblTagType.Text
                ''        DialogColumn.TO_TABLE = "MS_ST_COLUMN_SPEC"
                ''        DialogColumn.BindHeader()
                ''        DialogColumn.ShowDialog()

                ''    Case EIR_BL.ST_TAG_TYPE.Filter
                ''        DialogFilter.TAG_CODE = lblTagNo.Text
                ''        DialogFilter.TAG_ID = TAG_ID
                ''        DialogFilter.TAG_TYPE_ID = TAG_TYPE_ID
                ''        DialogFilter.TAG_Name = lblTagName.Text
                ''        DialogFilter.TAG_TYPE_Name = lblTagType.Text
                ''        DialogFilter.TO_TABLE = "MS_ST_FILTER_SPEC"
                ''        DialogFilter.BindHeader()
                ''        DialogFilter.ShowDialog()

                ''    Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager
                ''        DialogHeat_Exchnager.TAG_CODE = lblTagNo.Text
                ''        DialogHeat_Exchnager.TAG_ID = TAG_ID
                ''        DialogHeat_Exchnager.TAG_TYPE_ID = TAG_TYPE_ID
                ''        DialogHeat_Exchnager.TAG_Name = lblTagName.Text
                ''        DialogHeat_Exchnager.TAG_TYPE_Name = lblTagType.Text
                ''        DialogHeat_Exchnager.TO_TABLE = "MS_ST_HEAT_EXCHNAGER_SPEC"
                ''        DialogHeat_Exchnager.BindHeader()
                ''        DialogHeat_Exchnager.ShowDialog()

                ''    Case EIR_BL.ST_TAG_TYPE.Strainer


                ''    Case Else


                ''End Select

        End Select

    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        'BindTag()
        '--Query Type ที่มีใน plant
        'BL.BindDDlTagType("ST", ddl_Edit_Tag_Type)
        If ddl_Edit_Plant.SelectedIndex > 0 Then
            ddl_Edit_Tag_Type.Enabled = True
        Else
            ddl_Edit_Tag_Type.Enabled = False
            ddl_Edit_Tag_Type.SelectedIndex = -1
        End If
    End Sub

    Private Sub ddl_Edit_Tag_Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Edit_Tag_Type.SelectedIndexChanged

        BindTag()
    End Sub


    '-----เพิ่ม-------

    '-----แก้ไข------

    '-----ลบ--------

    Private Sub btnCreatePlan_Click(sender As Object, e As EventArgs) Handles btnCreatePlan.Click

        If ddl_Edit_Plant.SelectedIndex < 1 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If Not BL.IsProgrammingDate(txtStart.Text) Then
            lblValidation.Text = "Please select Start Date"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If Not BL.IsProgrammingDate(txtEnd.Text) Then
            lblValidation.Text = "Please select End Date"
            pnlValidation.Visible = True
            Exit Sub
        End If

        'Dim Old_RPT_Year As Integer = txtStart.Attributes("RPT_Year")
        'Dim Old_RPT_No As Integer = txtStart.Attributes("RPT_No")

        'Dim StartDate As Date = C.StringToDate(txtStart.Text, "yyyy-MM-dd")
        'Dim EndDate As Date = C.StringToDate(txtEnd.Text, "yyyy-MM-dd")
        'Dim Estimate As Integer = DateDiff(DateInterval.Day, StartDate, EndDate)

        'Dim RPT_Year As Integer = Year(StartDate) '+ 543
        'Dim RPT_No As Integer = Old_RPT_No

        Dim Old_RPT_Year As Integer = txtStart.Attributes("RPT_Year")
        Dim Old_RPT_No As Integer = txtStart.Attributes("RPT_No")

        Dim c As New Converter
        Dim StartDate As Date = c.StringToDate(txtStart.Text, "yyyy-MM-dd")
        Dim EndDate As Date = c.StringToDate(txtEnd.Text, "yyyy-MM-dd")
        Dim Estimate As Integer = DateDiff(DateInterval.Day, StartDate, EndDate)

        Dim RPT_Year As Integer = StartDate.ToString("yyyy", New Globalization.CultureInfo("th-TH")) 'Year(StartDate) + 543
        If RPT_Year < 2500 Then
            RPT_Year = RPT_Year + 543
        End If
        Dim RPT_No As Integer = Old_RPT_No


        Dim SQL As String = "SELECT * FROM RPT_ST_TA_Header WHERE RPT_Year=" & Old_RPT_Year & " AND RPT_No=" & Old_RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Select Case lblUpdateMode.Text
            Case "Create"

                Dim TotalPeriod As Integer = 1

                For i As Integer = 1 To TotalPeriod
                    Dim DR As DataRow = DT.NewRow
                    RPT_No = GetNewPlanNo(RPT_Year)
                    DR("RPT_Year") = RPT_Year
                    DR("RPT_No") = RPT_No
                    DR("RPT_Type_ID") = RPT_Type_ID
                    DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
                    DR("TAG_TYPE_ID") = ddl_Edit_Tag_Type.Items(ddl_Edit_Tag_Type.SelectedIndex).Value
                    DR("RPT_Step") = EIR_BL.Report_Step.New_Step
                    Dim _start As Date = StartDate
                    DR("RPT_Period_Start") = _start
                    DR("RPT_Period_End") = DateAdd(DateInterval.Day, Estimate, _start)

                    DR("Update_Time") = Now
                    DR("Update_By") = Session("USER_ID")
                    DT.Rows.Add(DR)

                    Dim cmd As New SqlCommandBuilder(DA)
                    Try
                        DA.Update(DT)
                        DT.AcceptChanges()
                    Catch ex As Exception
                        lblValidation.Text = ex.Message
                        pnlValidation.Visible = True
                        Exit Sub
                    End Try
                Next
            Case "Update"
                If DT.Rows.Count = 0 Then
                    lblValidation.Text = "This plan period has been removed from schedule."
                    pnlValidation.Visible = True
                    Exit Sub
                End If
                Dim DR As DataRow = DT.Rows(0)
                DR("RPT_Period_Start") = StartDate
                DR("RPT_Period_End") = EndDate
                DR("Update_Time") = Now
                DR("Update_By") = Session("USER_ID")

                Dim cmd As New SqlCommandBuilder(DA)
                Try
                    DA.Update(DT)
                Catch ex As Exception
                    lblValidation.Text = ex.Message
                    pnlValidation.Visible = True
                    Exit Sub
                End Try
        End Select


        '---Save Tag ลงตารางรายละเอียด
        Dim SQL_Detail As String = "SELECT * FROM RPT_ST_TA_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA_Detail As New SqlDataAdapter(SQL_Detail, BL.ConnStr)
        Dim DT_Detail As New DataTable
        DA_Detail.Fill(DT_Detail)

        '---filter เฉพาะรายการ Tag ที่อยู่ใน Plant เท่านั้น
        If (DT_Detail.Rows.Count > 0) Then
            Dim SQL_Del As String = "DELETE  FROM RPT_ST_TA_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            Dim DA_Del As New SqlDataAdapter(SQL_Del, BL.ConnStr)
            Dim DT_Del As New DataTable
            DA_Del.Fill(DT_Del)
        End If

        Dim Last_DETAIL_ID = GetNewRPT_ST_TA_Detail()
        Dim DR_Detail As DataRow
        Dim DT_TAG As DataTable = GetCurrentSelect()
        For i As Integer = 0 To DT_TAG.Rows.Count - 1
            DR_Detail = DT_Detail.NewRow
            DR_Detail("DETAIL_ID") = Last_DETAIL_ID + i
            DR_Detail("RPT_Year") = RPT_Year
            DR_Detail("RPT_No") = RPT_No
            DR_Detail("TAG_ID") = DT_TAG.Rows(i).Item("TAG_ID")
            DR_Detail("TAG_TYPE_ID") = DT_TAG.Rows(i).Item("TAG_TYPE_ID")

            DR_Detail("Update_Time") = Now
            DR_Detail("Update_By") = Session("USER_ID")

            DT_Detail.Rows.Add(DR_Detail)

        Next
        Dim cmd_Detail As New SqlCommandBuilder(DA_Detail)
        Try
            DA_Detail.Update(DT_Detail)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try


        ResetPlan(Nothing, Nothing)
        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True
        Response.Redirect("ST_TA_Inspection_Plan.aspx?StatusPlan=Success")
    End Sub

    Private Function GetNewPlanNo(ByVal RPT_Year As Integer) As Integer

        Dim SQL As String = "SELECT IsNull(MIN(RPT_No),0)-1 FROM RPT_ST_TA_Header" & vbNewLine
        SQL &= "  WHERE RPT_No < 0 AND RPT_Year=" & RPT_Year
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

    Private Function GetNewRPT_ST_TA_Detail() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(DETAIL_ID),0)+1 FROM RPT_ST_TA_Detail " & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

    Protected Sub ResetPlan(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ClearPanelSearch()
        Response.Redirect("ST_TA_Inspection_Plan.aspx")

    End Sub

    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    '------เก็บข้อมูลเฉพาะรายการที่ เลือก chk

    Private Function GetCurrentSelect() As DataTable
        Dim DT As DataTable = Session("DT_CurrentSelect")

        Return DT
    End Function






    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click
        Dim btn As Button = sender

    End Sub


End Class

'Imports System.Data
'Imports System.Data.SqlClient

'Public Class ST_TA_Inspection_Plan_Edit
'    Inherits System.Web.UI.Page

'    Dim BL As New EIR_BL
'    Dim C As New Converter
'    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Turnaround_Inspection_Reports

'    Private Property Mode() As String
'        Get
'            If Request.QueryString("Mode") <> Nothing Then
'                Return Request.QueryString("Mode")
'            Else
'                Return ""
'            End If

'        End Get
'        Set(ByVal value As String)
'            ViewState("Mode") = value
'        End Set
'    End Property

'    Private Property RPT_Year() As Integer
'        Get
'            If IsNumeric(ViewState("RPT_Year")) Then
'                Return ViewState("RPT_Year")
'            Else
'                Return 0
'            End If
'        End Get
'        Set(ByVal value As Integer)
'            ViewState("RPT_Year") = value
'        End Set
'    End Property
'    Private Property RPT_No() As Integer
'        Get
'            If IsNumeric(ViewState("RPT_No")) Then
'                Return ViewState("RPT_No")
'            Else
'                Return 0
'            End If
'        End Get
'        Set(ByVal value As Integer)
'            ViewState("RPT_No") = value
'        End Set
'    End Property

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        '---------------Hide Dialog-------------------------


'        DialogAbsorber.CloseDialog()
'        DialogDrum.CloseDialog()
'        DialogColumn.CloseDialog()
'        DialogFilter.CloseDialog()
'        DialogHeat_Exchnager.CloseDialog()

'        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
'            Response.Redirect("Login.aspx", True)
'        End If

'        If Not IsPostBack Then
'            '--------------Check Initialize Report--------------
'            RPT_Year = Request.QueryString("RPT_Year")
'            RPT_No = Request.QueryString("RPT_No")

'            '----สร้างตารางเก็ยรายการที่เลือก
'            Dim DT As DataTable
'            DT = New DataTable
'            DT.Columns.Add("TAG_ID", GetType(Int64))
'            DT.Columns.Add("TAG_TYPE_ID", GetType(Int64))
'            Session("DT_CurrentSelect") = DT

'            If Mode = "Create" Then
'                'Clear Form
'                ClearPanelEdit()

'            ElseIf Mode = "Edit" Then
'                BindHeader()
'                BindTag_Select()
'            End If

'            BindTag()
'            ClearPanelSearch()
'        End If

'        HideValidator()

'    End Sub

'    Private Sub ClearPanelEdit()
'        pnlEdit.Visible = True
'        lblUpdateMode.Text = "Create"
'        BL.BindDDlPlant(ddl_Edit_Plant)
'        BL.BindDDlST_TA_TagType(ddl_Edit_Tag_Type)
'        ddl_Edit_Tag_Type.Enabled = False
'        txtStart.Text = ""
'        txtStart.Attributes("RPT_Year") = "0"
'        txtStart.Attributes("RPT_No") = "0"

'        txtEstimate.Text = ""
'        txtEnd.Text = ""
'        ImplementJavaIntegerText(txtEstimate, True)

'    End Sub

'    Private Sub ClearPanelSearch()

'        ckStatus_OK.Checked = False
'        ckStatus_Abnormal.Checked = False
'        ckStatus_ClassA.Checked = False
'        ckStatus_ClassB.Checked = False
'        ckStatus_ClassC.Checked = False
'        ckStatus_ClassD.Checked = False
'    End Sub

'    Protected Sub txtStart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStart.TextChanged
'        If Not BL.IsProgrammingDate(txtStart.Text) Then
'            txtStart.Text = ""
'            txtEstimate.Text = ""
'            Exit Sub
'        End If

'        Dim StartDate As Date = DateTime.Parse(txtStart.Text)

'        If Not BL.IsProgrammingDate(txtEnd.Text) Then
'            If IsNumeric(txtEstimate.Text) Then
'                Dim EndDate = DateAdd(DateInterval.Day, CInt(txtEstimate.Text) - 1, StartDate)
'                txtEnd.Text = BL.ReportProgrammingDate(EndDate)
'                Exit Sub
'            Else
'                txtEstimate.Text = ""
'                txtEnd.Text = ""
'                Exit Sub
'            End If
'        Else
'            Dim EndDate As Date = DateTime.Parse(txtEnd.Text)
'            txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, StartDate, EndDate) + 1, 0)
'            Exit Sub
'        End If

'    End Sub




'    Protected Sub txtEnd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEnd.TextChanged
'        If Not BL.IsProgrammingDate(txtEnd.Text) Then
'            txtEnd.Text = ""
'            txtEstimate.Text = ""
'            Exit Sub
'        End If

'        Dim EndDate As Date = DateTime.Parse(txtEnd.Text)

'        If Not BL.IsProgrammingDate(txtStart.Text) Then
'            If IsNumeric(txtEstimate.Text) Then
'                Dim StartDate As Date = DateAdd(DateInterval.Day, -CInt(txtEstimate.Text) - 1, EndDate)
'                txtStart.Text = BL.ReportProgrammingDate(StartDate)
'                Exit Sub
'            Else
'                txtEstimate.Text = ""
'                txtStart.Text = ""
'                Exit Sub
'            End If
'        Else
'            Dim StartDate As Date = DateTime.Parse(txtStart.Text)
'            txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, StartDate, EndDate) + 1, 0)
'            Exit Sub
'        End If

'    End Sub

'    Protected Sub txtEstimate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEstimate.TextChanged
'        If Not IsNumeric(txtEstimate.Text) Then
'            txtEstimate.Text = ""
'            If BL.IsProgrammingDate(txtStart.Text) Then
'                txtEnd.Text = ""
'                Exit Sub
'            End If
'            If BL.IsProgrammingDate(txtEnd.Text) Then
'                txtStart.Text = ""
'                Exit Sub
'            End If
'            Exit Sub
'        End If

'        If BL.IsProgrammingDate(txtStart.Text) Then
'            Dim StartDate As Date = DateTime.Parse(txtStart.Text)
'            Dim EndDate As Date = DateAdd(DateInterval.Day, CInt(txtEstimate.Text) - 1, StartDate)
'            txtEnd.Text = BL.ReportProgrammingDate(EndDate)
'            Exit Sub
'        End If

'        If BL.IsProgrammingDate(txtEnd.Text) Then
'            Dim EndDate As Date = DateTime.Parse(txtEnd.Text)
'            Dim StartDate As Date = DateAdd(DateInterval.Day, -CInt(txtEstimate.Text) - 1, EndDate)
'            txtStart.Text = BL.ReportProgrammingDate(StartDate)
'            Exit Sub
'        End If

'        txtStart.Text = ""
'        txtEnd.Text = ""

'    End Sub

'    Private Sub BindHeader()
'        pnlEdit.Visible = True
'        lblUpdateMode.Text = "Update"

'        '--------------Bind Value------------
'        Dim SQL As String = "SELECT * " & vbNewLine
'        SQL &= " FROM RPT_ST_TA_Header " & vbNewLine
'        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
'        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
'        Dim DT As New DataTable
'        DA.Fill(DT)
'        If DT.Rows.Count = 0 Then
'            Response.Redirect("ST_TA_Inspection_Plan.aspx?StatusPlan=Fail")
'            Exit Sub

'        Else
'            RPT_Year = Request.QueryString("RPT_Year")
'            RPT_No = Request.QueryString("RPT_No")

'            txtStart.Attributes("RPT_Year") = RPT_Year
'            txtStart.Attributes("RPT_No") = RPT_No


'        End If

'        BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
'        BL.BindDDlST_TA_TagType(ddl_Edit_Tag_Type, DT.Rows(0).Item("TAG_TYPE_ID"))

'        txtStart.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("RPT_Period_Start"))
'        txtEnd.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("RPT_Period_End"))
'        txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, DT.Rows(0).Item("RPT_Period_Start"), DT.Rows(0).Item("RPT_Period_End")), 0)

'        ImplementJavaIntegerText(txtEstimate, True)

'    End Sub
'    Private Sub BindTag_Select()

'        Dim DT_CurrentSelect As DataTable = Session("DT_CurrentSelect")
'        Dim SQL_Detail As String = "SELECT * FROM RPT_ST_TA_TAG WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
'        Dim DA_Detail As New SqlDataAdapter(SQL_Detail, BL.ConnStr)
'        Dim DT_Detail As New DataTable
'        DA_Detail.Fill(DT_Detail)
'        If (DT_Detail.Rows.Count > 0) Then
'            Dim DR As DataRow
'            For i As Integer = 0 To DT_Detail.Rows.Count - 1
'                DR = DT_CurrentSelect.NewRow
'                DR("TAG_ID") = DT_Detail.Rows(i).Item("TAG_ID")
'                DR("TAG_TYPE_ID") = DT_Detail.Rows(i).Item("TAG_TYPE_ID")
'                DT_CurrentSelect.Rows.Add(DR)

'            Next
'        End If

'        Session("DT_CurrentSelect") = DT_CurrentSelect

'    End Sub

'    Private Sub BindTag()

'        Dim SQL As String = " SELECT * FROM (" & vbNewLine
'        SQL &= "   Select AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TAG.ROUTE_ID,MS_ST_TAG.AREA_ID,MS_ST_TAG.Update_Time" & vbNewLine
'        SQL &= "   , 'MS_ST_TAG' To_Table " & vbNewLine
'        SQL &= "   FROM MS_ST_TAG" & vbNewLine
'        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
'        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
'        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
'        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
'        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine

'        SQL &= "   UNION ALL" & vbNewLine
'        SQL &= "   Select  AREA_CODE +'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TA_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TA_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TA_TAG.ROUTE_ID,MS_ST_TA_TAG.AREA_ID,MS_ST_TA_TAG.Update_Time" & vbNewLine
'        SQL &= "   , 'MS_ST_TA_TAG' To_Table " & vbNewLine
'        SQL &= "   From MS_ST_TA_TAG" & vbNewLine
'        SQL &= "   INNER Join MS_ST_TAG_TYPE On  MS_ST_TA_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
'        SQL &= "   INNER Join MS_ST_Route ON MS_ST_TA_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
'        SQL &= "   INNER Join MS_PLANT On MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
'        SQL &= "   INNER Join MS_Area ON MS_ST_TA_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
'        SQL &= "   INNER Join MS_Process On MS_ST_TA_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
'        SQL &= " ) TB " & vbNewLine

'        Dim WHERE As String = ""

'        If ddl_Edit_Plant.SelectedIndex > 0 Then
'            WHERE &= " TB.PLANT_ID=" & ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value & " And "
'        Else
'            WHERE &= " 0=1 And "

'        End If

'        If ddl_Edit_Tag_Type.SelectedIndex > 0 Then
'            WHERE &= " TB.TAG_TYPE_ID=" & ddl_Edit_Tag_Type.Items(ddl_Edit_Tag_Type.SelectedIndex).Value & " And "
'        Else
'            WHERE &= " 0=1 And "

'        End If

'        'If txt_Search_Tag.Text <> "" Then
'        '    WHERE &= " ( TB.TAG_CODE Like '%" & txt_Search_Tag.Text & "%'  OR "
'        '    WHERE &= "  TB.TAG_Name Like '%" & txt_Search_Tag.Text & "%' )  And "
'        'Else
'        '    'WHERE &= " 0=1 And "
'        'End If


'        If WHERE <> "" Then
'            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
'        End If

'        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

'        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
'        Dim DT As New DataTable

'        Try
'            DA.Fill(DT)
'        Catch ex As Exception
'            pnlBindingError.Visible = True
'            lblBindingError.Text = ex.Message
'            Exit Sub
'        End Try

'        Session("MS_ST_TA_TAG") = DT

'        Navigation.SesssionSourceName = "MS_ST_TA_TAG"
'        Navigation.RenderLayout()

'    End Sub



'    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
'        Navigation.TheRepeater = rptTag
'    End Sub

'    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
'        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

'        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
'        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
'        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
'        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
'        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
'        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
'        Dim chkItem As CheckBox = e.Item.FindControl("chkItem")


'        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
'        lblTagNo.Attributes("TAG_NO") = e.Item.DataItem("TAG_NO")
'        lblTagName.Text = e.Item.DataItem("TAG_Name")
'        lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")
'        lblTagType.Attributes("TAG_TYPE_ID") = e.Item.DataItem("TAG_TYPE_ID")

'        lblTo_Table.Text = e.Item.DataItem("To_Table")

'        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
'            lblStatus.Text = "Available"
'            lblStatus.ForeColor = Drawing.Color.Green
'        Else
'            lblStatus.Text = "Unavailable"
'            lblStatus.ForeColor = Drawing.Color.OrangeRed
'        End If

'        '----ทำเครื่องหมายรายการที่ เลือก
'        Dim DT As DataTable = Session("DT_CurrentSelect")
'        DT.DefaultView.RowFilter = " TAG_ID =" & e.Item.DataItem("TAG_ID") & ""
'        If DT.DefaultView.Count > 0 Then
'            chkItem.Checked = True
'        Else
'            chkItem.Checked = False
'        End If

'        chkItem.Attributes("ck_TAG_ID") = e.Item.DataItem("TAG_ID")
'        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

'        'chkItem.Attributes("onchange") = "document.getElementById('" & btnSelect.ClientID & "').click();"

'    End Sub

'    Protected Sub chkItem_OnCheckedChanged(sender As Object, e As EventArgs)
'        Dim chk As CheckBox = DirectCast(sender, CheckBox)

'        Dim Item As RepeaterItem = chk.Parent
'        Dim lblTagNo As Label = Item.FindControl("lblTagNo")
'        Dim btnEdit As ImageButton = Item.FindControl("btnEdit")
'        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")
'        Dim lblTo_Table As Label = Item.FindControl("lblTo_Table")
'        Dim To_Table As String = lblTo_Table.Text
'        Dim lblTagType As Label = Item.FindControl("lblTagType")
'        Dim chkItem As CheckBox = Item.FindControl("chkItem")

'        '------หา รายการที่ chk/remove

'        Dim DT As DataTable = GetCurrentSelect()

'        Dim DR As DataRow
'        If (chkItem.Checked) Then
'            DR = DT.NewRow
'            DR("TAG_ID") = Convert.ToInt64(TAG_ID)
'            DR("TAG_TYPE_ID") = Convert.ToInt64(lblTagType.Attributes("TAG_TYPE_ID"))
'            DT.Rows.Add(DR)
'        Else
'            Dim index_row As Integer = 0
'            For i As Integer = 0 To DT.Rows.Count - 1
'                If TAG_ID = DT.Rows(i)("TAG_ID") Then
'                    DT.Rows.RemoveAt(i)
'                    Exit For
'                End If
'            Next
'        End If

'        Session("DT_CurrentSelect") = DT
'    End Sub

'    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
'        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
'        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
'        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
'        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
'        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
'        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
'        Dim chkItem As CheckBox = e.Item.FindControl("chkItem")

'        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")
'        Dim TAG_TYPE_ID As Integer = lblTagType.Attributes("TAG_TYPE_ID")
'        Dim To_Table As String = lblTo_Table.Text

'        Select Case e.CommandName
'            Case "Edit"

'                ''Select Case lblTagType.Attributes("TAG_TYPE_ID")
'                ''    Case EIR_BL.ST_TAG_TYPE.Drum
'                ''        DialogDrum.TAG_CODE = lblTagNo.Text
'                ''        DialogDrum.TAG_ID = TAG_ID
'                ''        DialogDrum.TAG_TYPE_ID = TAG_TYPE_ID
'                ''        DialogDrum.TAG_Name = lblTagName.Text
'                ''        DialogDrum.TAG_TYPE_Name = lblTagType.Text
'                ''        DialogDrum.TO_TABLE = "MS_ST_DRUM_SPEC"
'                ''        DialogDrum.BindHeader()
'                ''        DialogDrum.ShowDialog()

'                ''    Case EIR_BL.ST_TAG_TYPE.General_Stationary

'                ''        DialogAbsorber.TO_TABLE = "MS_ST_ABSORBER_SPEC"
'                ''        DialogColumn.ShowDialog()

'                ''    Case EIR_BL.ST_TAG_TYPE.Absorber
'                ''        DialogAbsorber.TAG_CODE = lblTagNo.Text
'                ''        DialogAbsorber.TAG_ID = TAG_ID
'                ''        DialogAbsorber.TAG_TYPE_ID = TAG_TYPE_ID
'                ''        DialogAbsorber.TAG_Name = lblTagName.Text
'                ''        DialogAbsorber.TAG_TYPE_Name = lblTagType.Text
'                ''        DialogAbsorber.TO_TABLE = "MS_ST_ABSORBER_SPEC"
'                ''        DialogAbsorber.BindHeader()
'                ''        DialogAbsorber.ShowDialog()

'                ''    Case EIR_BL.ST_TAG_TYPE.Column
'                ''        DialogColumn.TAG_CODE = lblTagNo.Text
'                ''        DialogColumn.TAG_ID = TAG_ID
'                ''        DialogColumn.TAG_TYPE_ID = TAG_TYPE_ID
'                ''        DialogColumn.TAG_Name = lblTagName.Text
'                ''        DialogColumn.TAG_TYPE_Name = lblTagType.Text
'                ''        DialogColumn.TO_TABLE = "MS_ST_COLUMN_SPEC"
'                ''        DialogColumn.BindHeader()
'                ''        DialogColumn.ShowDialog()

'                ''    Case EIR_BL.ST_TAG_TYPE.Filter
'                ''        DialogFilter.TAG_CODE = lblTagNo.Text
'                ''        DialogFilter.TAG_ID = TAG_ID
'                ''        DialogFilter.TAG_TYPE_ID = TAG_TYPE_ID
'                ''        DialogFilter.TAG_Name = lblTagName.Text
'                ''        DialogFilter.TAG_TYPE_Name = lblTagType.Text
'                ''        DialogFilter.TO_TABLE = "MS_ST_FILTER_SPEC"
'                ''        DialogFilter.BindHeader()
'                ''        DialogFilter.ShowDialog()

'                ''    Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager
'                ''        DialogHeat_Exchnager.TAG_CODE = lblTagNo.Text
'                ''        DialogHeat_Exchnager.TAG_ID = TAG_ID
'                ''        DialogHeat_Exchnager.TAG_TYPE_ID = TAG_TYPE_ID
'                ''        DialogHeat_Exchnager.TAG_Name = lblTagName.Text
'                ''        DialogHeat_Exchnager.TAG_TYPE_Name = lblTagType.Text
'                ''        DialogHeat_Exchnager.TO_TABLE = "MS_ST_HEAT_EXCHNAGER_SPEC"
'                ''        DialogHeat_Exchnager.BindHeader()
'                ''        DialogHeat_Exchnager.ShowDialog()

'                ''    Case EIR_BL.ST_TAG_TYPE.Strainer


'                ''    Case Else


'                ''End Select

'        End Select

'    End Sub

'    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
'        'BindTag()
'        '--Query Type ที่มีใน plant
'        'BL.BindDDlTagType("ST", ddl_Edit_Tag_Type)
'        If ddl_Edit_Plant.SelectedIndex > 0 Then
'            ddl_Edit_Tag_Type.Enabled = True
'        Else
'            ddl_Edit_Tag_Type.Enabled = False
'            ddl_Edit_Tag_Type.SelectedIndex = -1
'        End If
'    End Sub

'    Private Sub ddl_Edit_Tag_Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Edit_Tag_Type.SelectedIndexChanged

'        BindTag()
'    End Sub


'    '-----เพิ่ม-------

'    '-----แก้ไข------

'    '-----ลบ--------

'    Private Sub btnCreatePlan_Click(sender As Object, e As EventArgs) Handles btnCreatePlan.Click

'        If ddl_Edit_Plant.SelectedIndex < 1 Then
'            lblValidation.Text = "Please select Plant"
'            pnlValidation.Visible = True
'            Exit Sub
'        End If

'        If Not BL.IsProgrammingDate(txtStart.Text) Then
'            lblValidation.Text = "Please select Start Date"
'            pnlValidation.Visible = True
'            Exit Sub
'        End If
'        If Not BL.IsProgrammingDate(txtEnd.Text) Then
'            lblValidation.Text = "Please select End Date"
'            pnlValidation.Visible = True
'            Exit Sub
'        End If

'        'Dim Old_RPT_Year As Integer = txtStart.Attributes("RPT_Year")
'        'Dim Old_RPT_No As Integer = txtStart.Attributes("RPT_No")

'        'Dim StartDate As Date = C.StringToDate(txtStart.Text, "yyyy-MM-dd")
'        'Dim EndDate As Date = C.StringToDate(txtEnd.Text, "yyyy-MM-dd")
'        'Dim Estimate As Integer = DateDiff(DateInterval.Day, StartDate, EndDate)

'        'Dim RPT_Year As Integer = Year(StartDate) '+ 543
'        'Dim RPT_No As Integer = Old_RPT_No

'        Dim Old_RPT_Year As Integer = txtStart.Attributes("RPT_Year")
'        Dim Old_RPT_No As Integer = txtStart.Attributes("RPT_No")

'        Dim c As New Converter
'        Dim StartDate As Date = c.StringToDate(txtStart.Text, "yyyy-MM-dd")
'        Dim EndDate As Date = c.StringToDate(txtEnd.Text, "yyyy-MM-dd")
'        Dim Estimate As Integer = DateDiff(DateInterval.Day, StartDate, EndDate)

'        Dim RPT_Year As Integer = StartDate.ToString("yyyy", New Globalization.CultureInfo("th-TH")) 'Year(StartDate) + 543
'        If RPT_Year < 2500 Then
'            RPT_Year = RPT_Year + 543
'        End If
'        Dim RPT_No As Integer = Old_RPT_No


'        Dim SQL As String = "SELECT * FROM RPT_ST_TA_Header WHERE RPT_Year=" & Old_RPT_Year & " AND RPT_No=" & Old_RPT_No
'        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
'        Dim DT As New DataTable
'        DA.Fill(DT)

'        Select Case lblUpdateMode.Text
'            Case "Create"

'                Dim TotalPeriod As Integer = 1

'                For i As Integer = 1 To TotalPeriod
'                    Dim DR As DataRow = DT.NewRow
'                    RPT_No = GetNewPlanNo(RPT_Year)
'                    DR("RPT_Year") = RPT_Year
'                    DR("RPT_No") = RPT_No
'                    DR("RPT_Type_ID") = RPT_Type_ID
'                    DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
'                    DR("TAG_TYPE_ID") = ddl_Edit_Tag_Type.Items(ddl_Edit_Tag_Type.SelectedIndex).Value
'                    DR("RPT_Step") = EIR_BL.Report_Step.New_Step
'                    Dim _start As Date = StartDate
'                    DR("RPT_Period_Start") = _start
'                    DR("RPT_Period_End") = DateAdd(DateInterval.Day, Estimate, _start)

'                    DR("Update_Time") = Now
'                    DR("Update_By") = Session("USER_ID")
'                    DT.Rows.Add(DR)

'                    Dim cmd As New SqlCommandBuilder(DA)
'                    Try
'                        DA.Update(DT)
'                        DT.AcceptChanges()
'                    Catch ex As Exception
'                        lblValidation.Text = ex.Message
'                        pnlValidation.Visible = True
'                        Exit Sub
'                    End Try
'                Next
'            Case "Update"
'                If DT.Rows.Count = 0 Then
'                    lblValidation.Text = "This plan period has been removed from schedule."
'                    pnlValidation.Visible = True
'                    Exit Sub
'                End If
'                Dim DR As DataRow = DT.Rows(0)
'                DR("RPT_Period_Start") = StartDate
'                DR("RPT_Period_End") = EndDate
'                DR("Update_Time") = Now
'                DR("Update_By") = Session("USER_ID")

'                Dim cmd As New SqlCommandBuilder(DA)
'                Try
'                    DA.Update(DT)
'                Catch ex As Exception
'                    lblValidation.Text = ex.Message
'                    pnlValidation.Visible = True
'                    Exit Sub
'                End Try
'        End Select


'        '---Save Tag ลงตาราง รายการ tag ที่ต้องตรวจ
'        Dim SQL_TAG As String = "SELECT * FROM RPT_ST_TA_TAG WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
'        Dim DA_TAG As New SqlDataAdapter(SQL_TAG, BL.ConnStr)
'        Dim DT_TAG As New DataTable
'        DA_TAG.Fill(DT_TAG)

'        '---filter เฉพาะรายการ Tag ที่อยู่ใน Plant เท่านั้น
'        If (DT_TAG.Rows.Count > 0) Then
'            Dim SQL_Del As String = "DELETE  FROM RPT_ST_TA_TAG WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
'            Dim DA_Del As New SqlDataAdapter(SQL_Del, BL.ConnStr)
'            Dim DT_Del As New DataTable
'            DA_Del.Fill(DT_Del)
'        End If

'        Dim Last_DETAIL_ID = GetNewRPT_ST_TA_TAG()
'        Dim DR_Detail As DataRow
'        Dim DT_TAG_Select As DataTable = GetCurrentSelect()
'        For i As Integer = 0 To DT_TAG_Select.Rows.Count - 1
'            DR_Detail = DT_TAG.NewRow
'            DR_Detail("DETAIL_TAG_ID") = Last_DETAIL_ID + i
'            DR_Detail("RPT_Year") = RPT_Year
'            DR_Detail("RPT_No") = RPT_No
'            DR_Detail("TAG_ID") = DT_TAG_Select.Rows(i).Item("TAG_ID")
'            DR_Detail("TAG_TYPE_ID") = DT_TAG_Select.Rows(i).Item("TAG_TYPE_ID")

'            DR_Detail("Update_Time") = Now
'            DR_Detail("Update_By") = Session("USER_ID")

'            DT_TAG.Rows.Add(DR_Detail)

'        Next
'        Dim cmd_Detail As New SqlCommandBuilder(DA_TAG)
'        Try
'            DA_TAG.Update(DT_TAG)
'        Catch ex As Exception
'            lblValidation.Text = ex.Message
'            pnlValidation.Visible = True
'            Exit Sub
'        End Try


'        ResetPlan(Nothing, Nothing)
'        lblBindingSuccess.Text = "Save successfully"
'        pnlBindingSuccess.Visible = True
'        Response.Redirect("ST_TA_Inspection_Plan.aspx?StatusPlan=Success")
'    End Sub

'    Private Function GetNewPlanNo(ByVal RPT_Year As Integer) As Integer

'        Dim SQL As String = "SELECT IsNull(MIN(RPT_No),0)-1 FROM RPT_ST_TA_Header" & vbNewLine
'        SQL &= "  WHERE RPT_No < 0 AND RPT_Year=" & RPT_Year
'        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
'        Dim DT As New DataTable
'        DA.Fill(DT)
'        Return DT.Rows(0).Item(0)

'    End Function

'    Private Function GetNewRPT_ST_TA_TAG() As Integer

'        Dim SQL As String = "SELECT IsNull(MAX(DETAIL_TAG_ID),0)+1 FROM RPT_ST_TA_TAG " & vbNewLine
'        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
'        Dim DT As New DataTable
'        DA.Fill(DT)
'        Return DT.Rows(0).Item(0)

'    End Function

'    Protected Sub ResetPlan(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

'        ClearPanelSearch()
'        Response.Redirect("ST_TA_Inspection_Plan.aspx")

'    End Sub

'    Private Sub HideValidator()
'        pnlValidation.Visible = False
'        pnlBindingError.Visible = False
'        pnlBindingSuccess.Visible = False
'    End Sub

'    '------เก็บข้อมูลเฉพาะรายการที่ เลือก chk

'    Private Function GetCurrentSelect() As DataTable
'        Dim DT As DataTable = Session("DT_CurrentSelect")

'        Return DT
'    End Function






'    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click
'        Dim btn As Button = sender

'    End Sub


'End Class