﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Dashboard_Top10_Problem.ascx.vb" Inherits="EIR.UC_Dashboard_Top10_Problem" %>

<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
    <tr>
        <td style="vertical-align:top; text-align:center;">
            <asp:Chart ID="ChartMain" runat="server" Width="650px" Height="500px" CssClass="ChartHighligh" >
                <Series>
                    <asp:Series ChartArea="ChartArea1" ChartType="Column" Color="DodgerBlue" Name="Series1">
                    </asp:Series>                    
                </Series>
                <titles>
                    <asp:Title Name="Title1" font="Microsoft Sans Serif, 12pt, style=Bold">
                </asp:Title>
                </titles>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                            <AxisY IntervalAutoMode="VariableCount" islabelautofit="False">
                            <majorgrid linecolor="LightGray"></majorgrid>
                            <LabelStyle Font="Microsoft Sans Serif, 6.75pt"></LabelStyle>                            
                            </AxisY>
                            <AxisX IntervalAutoMode="VariableCount" islabelautofit="False">
                            <majorgrid enabled="False"></majorgrid>
                            <LabelStyle Angle="-90" TruncatedLabels="True"></LabelStyle>                            
                            </AxisX>
                            <AxisX2 IntervalAutoMode="VariableCount">
                            <majorgrid enabled="False"></majorgrid>
                            <minorgrid linecolor="Gray"></minorgrid>
                            </AxisX2>
                            <AxisY2 IntervalAutoMode="VariableCount"></AxisY2>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </td>
        <td style="vertical-align:top; text-align:center; width:100%">
            <center>
                <table border="0" cellpadding="3" cellspacing="0" style="width:300px; border:1px solid #efefef;">
                      <tr>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:50px; border-bottom:1px solid #003366;">
                              No.</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:150px; border-bottom:1px solid #003366;">
                              Problem</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:100px; border-bottom:1px solid #003366;">
                              Found (times)</td>                          
                      </tr>
                      <asp:Repeater ID="rptData" runat="server">
                        <ItemTemplate>
                            <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                              <td style="text-align:center;">
                                  <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                              <td>
                                  <asp:Label ID="lblProblem" runat="server" ></asp:Label></td>
                              <td style="text-align:right;">
                                  <asp:Label ID="lblFound" runat="server" CssClass="TextClassA"></asp:Label></td>                              
                            </tr>
                        </ItemTemplate>
                      </asp:Repeater>
                      
                </table>         
            </center>           
        </td>
    </tr>
</table>
<div style="display: none;">
    <asp:Label ID="lblMONTH_F" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblMONTH_T" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblYEAR_F" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblYEAR_T" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblEQUIPMENT" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblPrintPage" runat="server" Text=""></asp:Label>
</div>