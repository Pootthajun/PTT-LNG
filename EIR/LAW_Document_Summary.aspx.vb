﻿Imports System.IO
Imports System.Data.SqlClient

Public Class LAW_Document_Summary
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CL As New LawClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ClearPanelSearch()
            BindDocumentPlan()
            BindNotificationList()

            Dim TempPath As String = CL.UploadTempPath() & Session("USER_Name") & "\"
            If Directory.Exists(TempPath) = True Then
                Directory.Delete(TempPath, True)
            End If
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        'pnlValidation.Visible = False
        pnlBindingError.Visible = False
        'pnlBindingSuccess.Visible = False
    End Sub

    'Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
    '    pnlValidation.Visible = False
    'End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    'Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
    '    pnlBindingSuccess.Visible = False
    'End Sub
#End Region

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "SELECT ISNULL(MIN(document_year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(document_year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM LAW_Document_Plan"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddlSearchYear.Items.Clear()
        'ddlSearchYear.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddlSearchYear.Items.Add(i)
        Next
        ddlSearchYear.Text = Now.Year + 543

        CL.BindMasterDocument(ddlSearchDocTemplete)
        BL.BindDDlPlant(ddlSearchPlant, False)
        CL.BindDDLTagID(ddlSearchTag, 0, 0, 0)
    End Sub

    Private Sub ddlSearchPlant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchPlant.SelectedIndexChanged
        If ddlSearchPlant.SelectedIndex > -1 Then
            CL.BindDDLTagID(ddlSearchTag, ddlSearchPlant.SelectedValue, 0, 0)
            BindDocumentPlan()
        End If
    End Sub

    Private Sub ddlSearchDocTemplete_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchDocTemplete.SelectedIndexChanged
        If ddlSearchDocTemplete.SelectedIndex > -1 Then
            BindDocumentPlan()
        End If
    End Sub

    Private Sub ddlSearchYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchYear.SelectedIndexChanged
        If ddlSearchYear.SelectedIndex > -1 Then
            BindDocumentPlan()
        End If
    End Sub

    Private Sub ddlSearchTag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchTag.SelectedIndexChanged
        If ddlSearchTag.SelectedIndex > -1 Then
            BindDocumentPlan()
        End If
    End Sub


#End Region

#Region "Document Plan"
    Public Sub BindDocumentPlan()
        Dim SearchYear As Integer = 0
        If ddlSearchYear.SelectedIndex > -1 Then
            SearchYear = ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value
        End If
        Dim dt As DataTable = CL.GetListDocumentAllPlan(ddlSearchDocTemplete.SelectedValue, SearchYear, ddlSearchPlant.SelectedValue, ddlSearchTag.SelectedValue)
        If dt.Rows.Count > 0 Then
            rpt.DataSource = dt
            rpt.DataBind()
        Else
            rpt.DataSource = Nothing
            rpt.DataBind()
        End If

        If ddlSearchYear.SelectedIndex > -1 Then
            Dim ChartDT As DataTable = CL.GetDocumentChartYearData(ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value)
            DisplayChart(ChartDT)
        End If
    End Sub

    Private Sub rpt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpt.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDocumentName As Label = e.Item.FindControl("lblDocumentName")
        Dim lblDocumentPlanID As Label = e.Item.FindControl("lblDocumentPlanID")
        Dim lblDocumentYear As Label = e.Item.FindControl("lblDocumentYear")
        Dim lblPlantCode As Label = e.Item.FindControl("lblPlantCode")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblPlanStatus As Label = e.Item.FindControl("lblPlanStatus")
        Dim lblPercentComplete As Label = e.Item.FindControl("lblPercentComplete")
        Dim txtPercentWeight As TextBox = e.Item.FindControl("txtPercentWeight")
        Dim lblNoticeDate As Label = e.Item.FindControl("lblNoticeDate")
        Dim lblCriticalDate As Label = e.Item.FindControl("lblCriticalDate")
        Dim lblUploadDate As Label = e.Item.FindControl("lblUploadDate")

        lblDocumentName.Text = e.Item.DataItem("document_name")
        lblDocumentPlanID.Text = e.Item.DataItem("document_plan_id")
        lblDocumentYear.Text = e.Item.DataItem("document_year")
        lblPlantCode.Text = e.Item.DataItem("plant_code")
        lblTagNo.Text = e.Item.DataItem("tag_no")

        Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPlanStatus(e.Item.DataItem("document_plan_id"))
        lblPercentComplete.Text = pStatus.PercentComplete
        If Convert.IsDBNull(e.Item.DataItem("percent_weight")) = False Then txtPercentWeight.Text = e.Item.DataItem("percent_weight")

        lblNoticeDate.Text = pStatus.NoticeDate.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
        lblCriticalDate.Text = pStatus.CriticalDate.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))

        If pStatus.UploadDate.Year <> 1 Then
            lblUploadDate.Text = pStatus.UploadDate.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
        End If

        Select Case pStatus.PlanStatus
            Case "WAITING"
                lblPlanStatus.Text = "<span style='color:blue'><b>WAITING</b></span>"
            Case "NOTICE"
                lblPlanStatus.Text = "<span style='color:orange'><b>NOTICE</b></span>"
            Case "CRITICAL"
                lblPlanStatus.Text = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
            Case "COMPLETE"
                lblPlanStatus.Text = "<span style='color:green'><b>COMPLETE</b></span>"
        End Select

        ImplementJavaNumericText(txtPercentWeight)

        'Dim pDt As DataTable = CL.GetListDocumentPlanPaper(lblDocumentPlanID.Text)
        'Dim UC_DocumentTreePlanPaper1 As UC_DocumentTreePlanPaper = e.Item.FindControl("UC_DocumentTreePlanPaper1")
        'UC_DocumentTreePlanPaper1.SetPlanPaperData(pDt)
    End Sub

    Protected Sub UC_DocumentTreePlanPaper1_SetPercentComplete(sender As UC_DocumentTreePlanPaper, PercentComplete As Integer)
        Dim UC As UC_DocumentTreePlanPaper = DirectCast(sender, UC_DocumentTreePlanPaper)
        Dim RowItem As RepeaterItem = UC.Parent.Parent.Parent

        Dim lblPercentComplete As Label = RowItem.FindControl("lblPercentComplete")
        Dim lblUploadDate As Label = RowItem.FindControl("lblUploadDate")

        lblPercentComplete.Text = PercentComplete
        lblUploadDate.Text = DateTime.Now.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
    End Sub


    'Dim urlExpand As String = "resources/images/tree_expanded.png"
    'Dim urlCollapsed As String = "resources/images/tree_collapsed.png"
    'Dim urlNoChild As String = "resources/images/tree_nochild.png"

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim img As ImageButton = DirectCast(sender, ImageButton)
        Dim RowItem As RepeaterItem = img.Parent
        Dim lblDocumentPlanID As Label = RowItem.FindControl("lblDocumentPlanID")

        Response.Redirect("LAW_Document_Summary_Detail.aspx?id=" & lblDocumentPlanID.Text)
    End Sub

    Protected Sub txtPercentWeight_TextChanged(sender As Object, e As System.EventArgs)
        Dim itm As RepeaterItem = DirectCast(sender, TextBox).Parent
        Dim lblDocumentPlanID As Label = itm.FindControl("lblDocumentPlanID")

        Dim ret As String = CL.UpdatePlanPercentWeight(lblDocumentPlanID.Text, DirectCast(sender, TextBox).Text)
        If ret.ToLower <> "true" Then
            lblValidation.Text = ret
            pnlValidation.Visible = True
            Exit Sub
        Else
            lblValidation.Text = ""
            pnlValidation.Visible = False

            Dim ChartDT As DataTable = CL.GetDocumentChartYearData(ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value)
            DisplayChart(ChartDT)
        End If
    End Sub
#End Region

#Region "Chart"
    Protected Sub DisplayChart(ByVal DT As DataTable)
        ChartYear.ChartAreas("ChartArea1").AxisX.CustomLabels.Clear()

        Dim culture As New System.Globalization.CultureInfo("en-US")
        Dim C As New Converter

        Dim PlanValue As Integer = 0
        Dim ActualValue As Integer = 0

        'ChartMain.Series("Series1").IsValueShownAsLabel = True
        'ChartMain.Series("Series2").IsValueShownAsLabel = True

        ChartYear.Titles("Title1").Text = ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value

        For i As Integer = 0 To DT.Rows.Count - 1
            PlanValue = (PlanValue + DT.Rows(i).Item("plan_percent"))
            ActualValue = (ActualValue + DT.Rows(i).Item("actual_percent"))

            Dim PlanDate As String = Convert.ToDateTime(DT.Rows(i).Item("action_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

            ChartYear.Series("Series1").Points.AddXY(i + 1, PlanValue)
            ChartYear.Series("Series1").Points(i).ToolTip = "Plan" & vbNewLine & PlanDate & vbNewLine & DT.Rows(i)("paper_name") & vbNewLine & PlanValue & "%"

            ChartYear.Series("Series2").Points.AddXY(i + 1, ActualValue)
            ChartYear.Series("Series2").Points(i).ToolTip = "Actual" & vbNewLine & PlanDate & vbNewLine & DT.Rows(i)("paper_name") & vbNewLine & ActualValue & "%"

            ChartYear.ChartAreas("ChartArea1").AxisX.LabelStyle.Angle = -45
            ChartYear.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, PlanDate)
        Next
    End Sub
#End Region

#Region "Notification List"
    Private Sub BindNotificationList()
        Dim dt As DataTable = CL.GetDocumentNotificationList(DateTime.Now.ToString("yyyy", New System.Globalization.CultureInfo("en-US")))
        'Dim dt As DataTable = CL.GetDocumentNotificationList(2018)
        If dt.Rows.Count > 0 Then
            rptNotification.DataSource = dt
            rptNotification.DataBind()
        Else
            rptNotification.DataSource = Nothing
            rptNotification.DataBind()
        End If
    End Sub

    Private Sub rptNotification_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptNotification.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNoticeDocumentName As Label = e.Item.FindControl("lblNoticeDocumentName")
        Dim lblOrgName As Label = e.Item.FindControl("lblOrgName")
        Dim lblNoticeTemplateName As Label = e.Item.FindControl("lblNoticeTemplateName")
        Dim lblNoticePlantCode As Label = e.Item.FindControl("lblNoticePlantCode")
        Dim lblNoticeTagNo As Label = e.Item.FindControl("lblNoticeTagNo")
        Dim lblNoticeDate As Label = e.Item.FindControl("lblNoticeDate")

        lblNoticeDocumentName.Text = e.Item.DataItem("paper_name")
        lblOrgName.Text = e.Item.DataItem("org_name")
        lblNoticeTemplateName.Text = e.Item.DataItem("template_name")
        lblNoticePlantCode.Text = e.Item.DataItem("PLANT_Code")

        lblNoticeTagNo.Text = e.Item.DataItem("tag_no")
        lblNoticeDate.Text = Convert.ToDateTime(e.Item.DataItem("next_notice_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
    End Sub
#End Region
End Class