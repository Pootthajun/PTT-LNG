﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Current_Status_AllTag_THM.aspx.vb" Inherits="EIR.Dashboard_Current_Status_AllTag_THM" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2>Thermography Current Tag Status for <asp:LinkButton ID="lblPlant" runat="server"></asp:LinkButton></h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td colspan="2">
				    <h3 style="color: #0066CC;">Chart Type&nbsp;
				        <asp:DropDownList ID="ddl_ChartType" runat="server" AutoPostBack="true" >
				        <asp:ListItem Value="Pie" Selected="True"></asp:ListItem>
				        <asp:ListItem Value="Doughnut"></asp:ListItem>				
				        <asp:ListItem Value="Funnel"></asp:ListItem>
				        <asp:ListItem Value="Pyramid"></asp:ListItem>
                        </asp:DropDownList>
                    </h3> 
                    <asp:LinkButton ID="lblBack" runat="server"></asp:LinkButton>              
                </td>
			  </tr>
			  <tr>
				<td width="200" style="vertical-align:top; text-align:center;">
				    <asp:Chart ID="ChartTHM" runat="server" Height="300px" Width="300px" CssClass="ChartHighligh">
                        <titles>
                            <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="All tag(s) in this plant xxx"></asp:Title>
                            <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title2" Text="xxx tag(s)" Docking="Bottom"></asp:Title>
                        </titles>
                        <series>
                            <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                <emptypointstyle isvisibleinlegend="False" />
                            </asp:Series>
                        </series>
                        <chartareas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </chartareas>
                    </asp:Chart>
                </td>
			      <td style="vertical-align:top;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Tag</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Equipement Type
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Route
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Current</td>  
                              <%--<td style="text-align:center; background-color:#003366; color:White;">
                                  Last Report</td> --%>                      
                          </tr>
                          <asp:Repeater ID="rptData" runat="server" >
                            <ItemTemplate>
                                <tr>
                                    <td style="border-left:1px solid #eeeeee; text-align:left;">
                                        <asp:Label ID="lblTag" Text="-" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td style="text-align:center; ">
                                        <asp:Label ID="lblEqm" Text="-" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; ">
                                        <asp:Label ID="lblRoute" Text="-" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; ">
                                        <asp:Label ID="lblStatus" Text="-" runat="server"></asp:Label>
                                        <%--<a ID="lblStatus" runat="server" style="color:#666666">-</a>--%>
                                    </td>
                                    <%--<td style="text-align:center; width:100px;">
                                        <a ID="lblLastReport" runat="server" style="color:#666666">-</a>
                                    </td>--%>
                                </tr>
                            </ItemTemplate>
                          </asp:Repeater>
                          
                      </table>
                  </td>
		      </tr>
			</table>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>
