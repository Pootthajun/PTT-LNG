﻿Public Class UC_FileAlbum
    Inherits System.Web.UI.UserControl

    Public Property UNIQUE_ID As String
        Get
            Return lblUNIQUE_ID.Text
        End Get
        Set(value As String)
            lblUNIQUE_ID.Text = value
        End Set
    End Property

    Public Property Attachments As List(Of FileAttachment)
        Get
            Try
                Return Session(UNIQUE_ID)
            Catch ex As Exception
                Return New List(Of FileAttachment)
            End Try
        End Get
        Set(value As List(Of FileAttachment))
            Session(UNIQUE_ID) = value
        End Set
    End Property

    Dim SelectedCss As String = "default-tab current"
    Public Property CurrentDisplayFileType As Integer
        Get
            For Each Item As RepeaterItem In rptDisplayType.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim lnk As LinkButton = Item.FindControl("lnk")
                If lnk.CssClass = SelectedCss Then
                    Return lnk.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(value As Integer)
            lnkDisplayAll.CssClass = ""
            For Each Item As RepeaterItem In rptDisplayType.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim lnk As LinkButton = Item.FindControl("lnk")
                If lnk.CommandArgument = value Then
                    lnk.CssClass = SelectedCss
                Else
                    lnk.CssClass = ""
                End If
            Next
            If value = 0 Then lnkDisplayAll.CssClass = SelectedCss
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '---------------- Do Nothing --------------
        If Not IsPostBack Then
            cfmbtnClearAll.ConfirmText = cfmlnkClearAll.ConfirmText
        End If
    End Sub

    Public Property DisplayToolbar As Boolean
        Get
            Return Toolbar.Visible
        End Get
        Set(value As Boolean)
            Toolbar.Visible = value
        End Set
    End Property


    Dim AFT As DataTable
    Public Sub BindData()

        '------------------ Get All Configuration Type----------
        Dim BL As New EIR_BL
        AFT = BL.Get_ATTACHMENT_FILE_TYPE()

        '---------------------- Bind All File------------------
        rptFile.DataSource = Attachments
        rptFile.DataBind()

        '--------------- Bind All File Type -------------------
        rptAddType.DataSource = AFT
        rptAddType.DataBind()

        For i As Integer = AFT.Rows.Count - 1 To 0 Step -1
            Dim L As List(Of FileAttachment) = GetAttachmentByType(AFT.Rows(i).Item("AFT_ID"))
            If L.Count = 0 Then
                AFT.Rows.RemoveAt(i)
            End If
        Next
        Dim LastDisplayType As Integer = CurrentDisplayFileType
        '--------------- Bind Tab And All Existing Type- -----------
        rptDisplayType.DataSource = AFT
        rptDisplayType.DataBind()

        rptClear.DataSource = AFT
        rptClear.DataBind()
        '--------------- Set Show Display File Type And Toolbar ----------------
        lnkDisplayAll.Visible = AFT.Rows.Count > 0
        groupClearAll.Visible = Attachments.Count > 0

        If AFT.Rows.Count = 0 Then
            CurrentDisplayFileType = 0 '---------- Set Highlight 'All Type' ------------
        Else
            AFT.DefaultView.RowFilter = "AFT_ID=" & LastDisplayType
            If AFT.DefaultView.Count = 0 Then '---------- Set Highlight 'All Type' ------------
                CurrentDisplayFileType = 0 '---------- Set Highlight 'All Type' ------------
            Else
                CurrentDisplayFileType = LastDisplayType
            End If
        End If
        '------------- Now Got Real Displayed Type -------------------
        If CurrentDisplayFileType = 0 Then Exit Sub
        '----------- Set Visible / Invisible File in Container -------
        For i As Integer = 0 To Attachments.Count - 1
            Dim Obj As FileAttachment = Attachments(i)
            If Obj.DocType <> CurrentDisplayFileType Then rptFile.Items(i).Visible = False
        Next

    End Sub

    Private Function GetAttachmentByID(ByVal UNIQUE_ID As String) As FileAttachment
        For Each F As FileAttachment In Attachments
            If F.UNIQUE_ID = UNIQUE_ID Then
                Return F
            End If
        Next
        Return Nothing
    End Function

    Private Function GetAttachmentByType(ByVal DocType As FileAttachment.AttachmentType) As List(Of FileAttachment)
        Dim Result As New List(Of FileAttachment)
        For Each F As FileAttachment In Attachments
            If F.DocType = DocType Then
                Result.Add(F)
            End If
        Next
        Return Result
    End Function

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim Obj As New FileAttachment
        Obj.GenerateUniqueID()
        Obj.Extension = FileAttachment.ExtensionType.SVG
        Session(Obj.UNIQUE_ID) = Obj
        ShowDialogEditSVG(Me.Page, Obj.UNIQUE_ID, "", btnRefreshAdd.ClientID, txtUpdateID.ClientID)
    End Sub

    Private Sub btnRefreshAdd_Click(sender As Object, e As EventArgs) Handles btnRefreshAdd.Click
        Dim UNIQUE_ID As String = txtUpdateID.Text
        If UNIQUE_ID = "" Then Exit Sub
        Dim Target As FileAttachment = GetAttachmentByID(UNIQUE_ID)
        If IsNothing(Target) Then
            '------------ Add --------------
            Dim Obj As FileAttachment = Session(UNIQUE_ID)
            Attachments.Add(Obj)
        End If
        BindData()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), " FocusMainWindow", "window.focus();", True)
    End Sub

    Private Sub rptFile_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptFile.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim Obj As FileAttachment = Attachments(e.Item.ItemIndex)
        Dim pnlImage As Panel = e.Item.FindControl("pnlImage")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnRefresh As Button = e.Item.FindControl("btnRefresh")
        Dim aZoomMask As HtmlAnchor = e.Item.FindControl("aZoomMask")
        Dim lblTitle As Label = e.Item.FindControl("lblTitle")
        Dim FileBlock As Panel = e.Item.FindControl("FileBlock")

        Dim cfm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfm")

        Dim Thumnail As String = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=" & Obj.UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
        pnlImage.BackImageUrl = Thumnail
        aZoomMask.HRef = Thumnail

        lblTitle.Text = Obj.Title
        If Obj.Description <> "" Then
            FileBlock.Attributes("title") = Obj.Description
        End If
        If Obj.Title <> "" Then
            cfm.ConfirmText = "Confirm to delete file : '" & Obj.Title & "'"
        Else
            cfm.ConfirmText = "Confirm to delete file # " & (e.Item.ItemIndex + 1)
        End If
        cfm.BehaviorID = btnDelete.ClientID

        '------------- Set Border By File Type-----------
        Dim FT As DataTable = AFT.Copy
        FT.DefaultView.RowFilter = "AFT_ID=" & CInt(Obj.DocType)
        If FT.DefaultView.Count > 0 AndAlso FT.DefaultView(0).Item("UIColor").ToString <> "" Then
            FileBlock.Style("border") = "5px solid " & FT.DefaultView(0).Item("UIColor")
        End If
        FT.Dispose()

        btnEdit.CommandArgument = Obj.UNIQUE_ID
        btnDelete.CommandArgument = Obj.UNIQUE_ID
        btnRefresh.CommandArgument = Obj.UNIQUE_ID
    End Sub

    Private Sub rptFile_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptFile.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim UNIQUE_ID As String = e.CommandArgument
                Dim Obj As FileAttachment = GetAttachmentByID(UNIQUE_ID)
                Dim btnRefresh As Button = e.Item.FindControl("btnRefresh")
                If Not IsNothing(Obj) Then
                    ShowDialogEditSVG(Me.Page, Obj.UNIQUE_ID, "", btnRefresh.ClientID, "")
                End If
                BindData()
            Case "Delete"
                Dim UNIQUE_ID As String = e.CommandArgument
                Dim Obj As FileAttachment = GetAttachmentByID(UNIQUE_ID)
                If Not IsNothing(Obj) Then
                    Attachments.Remove(Obj)
                    Obj = Nothing
                End If
                BindData()
            Case "Refresh"
                'Dim UNIQUE_ID As String = e.CommandArgument
                'Dim Obj As FileAttachment = GetAttachmentByID(UNIQUE_ID)
                BindData()
        End Select
    End Sub

    Private Sub rptType_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAddType.ItemDataBound, rptDisplayType.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lnk As LinkButton = e.Item.FindControl("lnk")
        lnk.Text = e.Item.DataItem("AFT_Name_EN")
        lnk.Style("color") = e.Item.DataItem("UIColor")
        lnk.CommandArgument = e.Item.DataItem("AFT_ID")
    End Sub

    Private Sub rptDisplayType_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDisplayType.ItemCommand
        Select Case e.CommandName
            Case "Select"
                Dim lnk As LinkButton = e.Item.FindControl("lnk")
                Dim AFT_ID As Integer = lnk.CommandArgument
                CurrentDisplayFileType = AFT_ID
                BindData()
        End Select
    End Sub

    Private Sub rptAddType_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptAddType.ItemCommand
        Select Case e.CommandName
            Case "Add"
                Dim AFT_ID As Integer = e.CommandArgument
                Dim Obj As New FileAttachment
                Obj.GenerateUniqueID()
                Obj.Extension = FileAttachment.ExtensionType.SVG
                Obj.DocType = AFT_ID
                Session(Obj.UNIQUE_ID) = Obj
                ShowDialogEditSVG(Me.Page, Obj.UNIQUE_ID, "", btnRefreshAdd.ClientID, txtUpdateID.ClientID)
        End Select
    End Sub

    Private Sub rptClear_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptClear.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lnk As LinkButton = e.Item.FindControl("lnk")
        Dim cfm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfm")
        lnk.Text = e.Item.DataItem("AFT_Name_EN")
        lnk.Style("color") = e.Item.DataItem("UIColor")
        lnk.CommandArgument = e.Item.DataItem("AFT_ID")

        cfm.BehaviorID = lnk.ClientID
        cfm.ConfirmText = "Are you sure to clear " & lnk.Text & "?"
    End Sub

    '-------------- Clear SomeFileType ------------
    Private Sub rptClear_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptClear.ItemCommand
        Select Case e.CommandName
            Case "Clear"
                Dim AFT_ID As FileAttachment.AttachmentType = e.CommandArgument
                Dim Lst As List(Of FileAttachment) = GetAttachmentByType(AFT_ID)
                While Lst.Count > 0
                    Dim F As FileAttachment = Lst(0)
                    Attachments.Remove(F)
                    Lst.Remove(F)
                    F = Nothing
                End While
                BindData()
        End Select
    End Sub

    '-------------- Clear All File --------------
    Private Sub btnClearAll_Click(sender As Object, e As EventArgs) Handles btnClearAll.Click, lnkClearAll.Click
        While Attachments.Count > 0
            Dim F As FileAttachment = Attachments(0)
            Attachments.Remove(F)
            F = Nothing
        End While
        BindData()
    End Sub

    Private Sub lnkDisplayAll_Click(sender As Object, e As EventArgs) Handles lnkDisplayAll.Click
        CurrentDisplayFileType = 0
        BindData()
    End Sub


End Class