﻿Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Public Class MTAP_Routine_Edit3
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.MTap_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("MTAP_Routine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_MTAP_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='MTAP_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_MTAP_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_MTAP_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_MTAP_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindTabData()

        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_MTAP_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='MTAP_Routine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='MTAP_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='MTAP_Routine_Summary.aspx'", True)
            Exit Sub
        End If

        '---------------- Set Permission Visibility -------------
        If USER_LEVEL = EIR_BL.User_Level.Administrator Then
            btn_Send_Collector.Visible = True
            btn_Send_Inspector.Visible = False
            btn_Send_Analyst.Visible = True
            btn_Posted.Visible = True
        Else
            btn_Send_Collector.Visible = USER_LEVEL = EIR_BL.User_Level.Approver
            btn_Send_Inspector.Visible = False
            btn_Send_Analyst.Visible = USER_LEVEL = EIR_BL.User_Level.Collector
            btn_Posted.Visible = USER_LEVEL = EIR_BL.User_Level.Approver
        End If
        pnl_Inspector.Visible = False

        pnl_Collector.Enabled = USER_LEVEL = EIR_BL.User_Level.Collector
        pnl_Inspector.Enabled = USER_LEVEL = EIR_BL.User_Level.Inspector
        pnl_Analyst.Enabled = USER_LEVEL = EIR_BL.User_Level.Approver

        '----------------- Get Issue -----------------
        '----------------Remain--------------
        SQL = "SELECT * FROM VW_REPORT_MTAP_DETAIL WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND IS_Complete=0" & vbNewLine
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            btn_Send_Collector.Enabled = True
            btn_Send_Inspector.Enabled = True
            btn_Send_Analyst.Enabled = True
            btn_Posted.Enabled = True
        Else
            btn_Send_Collector.Enabled = False
            btn_Send_Inspector.Enabled = False
            btn_Send_Analyst.Enabled = False
            btn_Posted.Enabled = False
        End If
    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_MTAP_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("MTAP_Routine_Summary.aspx")
            Exit Sub
        End If

        '------------------------------Header -----------------------------------
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_Code")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If
        '------------------------------Header -----------------------------------

        SQL = "SELECT RPT_Result, " & vbNewLine
        SQL &= " RPT_MTAP_Header.RPT_COL_By,RPT_MTAP_Header.RPT_COL_Date,RPT_MTAP_Header.RPT_COL_Comment,COL.USER_Name + ' ' + COL.User_Surname  COL_NAME," & vbNewLine
        SQL &= " RPT_MTAP_Header.RPT_INSP_By,RPT_MTAP_Header.RPT_INSP_Date,RPT_MTAP_Header.RPT_INSP_Comment,INP.USER_Name + ' ' + INP.User_Surname INSP_NAME," & vbNewLine
        SQL &= " RPT_MTAP_Header.RPT_ANL_By,RPT_MTAP_Header.RPT_ANL_Date,RPT_MTAP_Header.RPT_ANL_Comment,ANL.USER_Name + ' ' + ANL.User_Surname ANL_NAME" & vbNewLine
        SQL &= " FROM RPT_MTAP_Header " & vbNewLine
        SQL &= " LEFT JOIN MS_User COL ON RPT_MTAP_Header.RPT_COL_By=COL.USER_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_User INP ON RPT_MTAP_Header.RPT_INSP_By=INP.USER_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_User ANL ON RPT_MTAP_Header.RPT_ANL_By=ANL.USER_ID" & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable

        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        If Not IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            txt_RPT_Result.Text = DT.Rows(0).Item("RPT_Result")
        End If
        '--------- Collector --------
        If Not IsDBNull(DT.Rows(0).Item("COL_NAME")) Then
            txt_RPT_COL_By.Text = DT.Rows(0).Item("COL_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_COL_Date")) Then
            txt_RPT_COL_Date.Text = DT.Rows(0).Item("RPT_COL_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_COL_Comment")) Then
            txt_RPT_COL_Comment.Text = DT.Rows(0).Item("RPT_COL_Comment")
        End If
        txt_RPT_COL_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Collector

        '--------- Inspector --------
        If Not IsDBNull(DT.Rows(0).Item("INSP_NAME")) Then
            txt_RPT_INSP_By.Text = DT.Rows(0).Item("INSP_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_INSP_Date")) Then
            txt_RPT_INSP_Date.Text = DT.Rows(0).Item("RPT_INSP_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_INSP_Comment")) Then
            txt_RPT_INSP_Comment.Text = DT.Rows(0).Item("RPT_INSP_Comment")
        End If
        txt_RPT_INSP_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Inspector

        '--------- Approver --------
        If Not IsDBNull(DT.Rows(0).Item("ANL_NAME")) Then
            txt_RPT_ANL_By.Text = DT.Rows(0).Item("ANL_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_ANL_Date")) Then
            txt_RPT_ANL_Date.Text = DT.Rows(0).Item("RPT_ANL_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_ANL_Comment")) Then
            txt_RPT_ANL_Comment.Text = DT.Rows(0).Item("RPT_ANL_Comment")
        End If
        txt_RPT_ANL_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Approver

        BindRecomment()
    End Sub

    Private Sub BindRecomment()
        '----------------- Get Issue -----------------
        Dim DT As DataTable = BL.Get_MTAP_Problem_Recomment(RPT_Year, RPT_No)
        rptSummary.DataSource = DT
        rptSummary.DataBind()
    End Sub


#Region "Navigator"

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click, HTabDetail.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='MTAP_Routine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='MTAP_Routine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

#End Region

    Private Sub Save_Summary(Optional ByVal ReportSuccess As Boolean = True)

        Dim DT As New DataTable
        Dim SQL As String = "SELECT RPT_Year,RPT_No,RPT_Result,RPT_COL_By,RPT_COL_Date,RPT_COL_Comment,RPT_INSP_By,RPT_INSP_Date,RPT_INSP_Comment,"
        SQL &= " RPT_ANL_By,RPT_ANL_Date,RPT_ANL_Comment,Update_By,Update_Time"
        SQL &= " FROM RPT_MTAP_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save.\nThis report has been removed!'); window.location.href='MTAP_Routine_Summary.aspx';", True)
            Exit Sub
        End If

        Dim NeedSave As Boolean = False
        '------------------- Save Result----------
        If IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_Result") = txt_RPT_Result.Text
        Else
            If DT.Rows(0).Item("RPT_Result") <> txt_RPT_Result.Text Then
                NeedSave = True
                DT.Rows(0).Item("RPT_Result") = txt_RPT_Result.Text
            End If
        End If
        '------------------- Save Collector----------
        If IsDBNull(DT.Rows(0).Item("RPT_COL_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_COL_Comment") = txt_RPT_COL_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_COL_Comment") <> txt_RPT_COL_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Collector Then
                NeedSave = True
                DT.Rows(0).Item("RPT_COL_Comment") = txt_RPT_COL_Comment.Text
            End If
        End If
        '------------------- Save Inspector----------
        If IsDBNull(DT.Rows(0).Item("RPT_INSP_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_INSP_Comment") = txt_RPT_INSP_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_INSP_Comment") <> txt_RPT_INSP_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Inspector Then
                NeedSave = True
                DT.Rows(0).Item("RPT_INSP_Comment") = txt_RPT_INSP_Comment.Text
            End If
        End If
        '------------------- Save Approver----------
        If IsDBNull(DT.Rows(0).Item("RPT_ANL_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_ANL_Comment") = txt_RPT_ANL_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_ANL_Comment") <> txt_RPT_ANL_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Approver Then
                NeedSave = True
                DT.Rows(0).Item("RPT_ANL_Comment") = txt_RPT_ANL_Comment.Text
            End If
        End If

        If Not NeedSave Then
            If ReportSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('There is nothing to be saved!\nThe report does not changed');", True)
            End If
            Exit Sub
        End If

        DT.Rows(0).Item("Update_By") = Session("USER_ID")
        DT.Rows(0).Item("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        If ReportSuccess Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save successfully');", True)
        End If


    End Sub

#Region "WorkFlow"
    Protected Sub btn_Send_Collector_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Collector.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_MTAP_Header set RPT_LOCK_BY=NULL,RPT_STEP=1,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Collector!!'); window.location.href='MTAP_Routine_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Send_Inspector_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Inspector.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_MTAP_Header set RPT_LOCK_BY=NULL,RPT_STEP=2,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Inspector!!'); window.location.href='MTAP_Routine_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Send_Analyst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Analyst.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_MTAP_Header set RPT_LOCK_BY=NULL,RPT_STEP=3,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Approver!!'); window.location.href='MTAP_Routine_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Posted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Posted.Click

        Save_Summary(False)

        Dim DefaultValue As String = Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & (Now.Year + 543).ToString.Substring(2, 2) & "_" & "_MTAP_"
        DefaultValue &= UCase(Session("USER_Name")) & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "-" & RPT_No.ToString.PadLeft(4, "0") & ".PDF"
        DialogInput.ShowDialog("Please insert finalize report file name..", DefaultValue)

    End Sub

    Protected Sub DialogInput_AnswerDialog(ByVal Result As String) Handles DialogInput.AnswerDialog

        If Result = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please insert file name to be saved');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If
        If Not BL.IsFormatFileName(Result) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('File name must not contained following excepted charactors /\:*?""<>|;');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) RPT_Code FROM RPT_MTAP_Header WHERE Result_FileName='" & Replace(Result, "'", "''") & "' AND RPT_Year<>" & RPT_Year & " AND RPT_No<>" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This file name is already exists. And has been reserved for report " & DT.Rows(0).Item("RPT_Code") & "');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        '---------------- Generate Posted Report-----------
        If Result.Length >= 4 AndAlso Right(Result, 4).ToUpper <> ".PDF".ToUpper Then
            Result = Result & ".PDF"
        ElseIf Result.Length < 4 Then
            Result = Result & ".PDF"
        End If

        Dim DestinationPath As String = BL.PostedReport_Path & "\" & Result
        Dim GenerateResult = BL.GeneratePostedReport(RPT_Year, RPT_No, DestinationPath)
        If Not GenerateResult.Success Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Unavailable to posted report !!'); alert('" & GenerateResult.Message.Replace("'", """") & "');", True)
            DialogInput.Visible = False
            Exit Sub
        End If
        DialogInput.Visible = False
        '--------------------- Update Report Status ---------------------------------
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_MTAP_Header set RPT_LOCK_BY=NULL,RPT_STEP=4,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE(),Result_FileName='" & Replace(Result, "'", "''") & "'" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Report has been Approved and job done!!'); ShowPreviewReport(" & RPT_Year & "," & RPT_No & "); window.location.href='MTAP_Routine_Summary.aspx';", True)
    End Sub

#End Region


    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

    'Protected Sub lnkRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRefresh.Click
    '    BindTabData()
    'End Sub

    Protected Sub txt_RPT_Result_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_RPT_Result.TextChanged
        Save_Summary(False)
    End Sub

    Protected Sub rptSummary_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSummary.ItemCommand
        Select Case e.CommandName
            Case "Save"
                Dim btnEdit As Button = e.Item.FindControl("btnEdit")
                Dim DETAIL_ID As Integer = btnEdit.Attributes("DETAIL_ID")
                Dim PROB_Code As String = btnEdit.Attributes("PROB_Code")
                Dim lblProblem As Label = e.Item.FindControl("lblProblem")
                Dim txtComment As TextBox = e.Item.FindControl("txtComment")
                Dim txtResponse As TextBox = e.Item.FindControl("txtResponse")

                Dim SQL As String = "SELECT * FROM RPT_MTAP_PROB_Comment" & vbLf
                SQL &= " WHERE DETAIL_ID=" & DETAIL_ID & " AND PROB_Code='" & PROB_Code.Replace("'", "''") & "'"
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                Dim DR As DataRow
                If DT.Rows.Count = 0 Then
                    DR = DT.NewRow
                    DR("DETAIL_ID") = DETAIL_ID
                    DR("PROB_Code") = PROB_Code
                    DT.Rows.Add(DR)
                Else
                    DR = DT.Rows(0)
                End If
                DR("PROB_Detail") = lblProblem.Text
                DR("PROB_Recomment") = txtComment.Text
                DR("Responsible") = txtResponse.Text
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)

                BindRecomment()
        End Select
    End Sub

    Protected Sub rptSummary_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSummary.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblProblem As Label = e.Item.FindControl("lblProblem")
        Dim txtComment As TextBox = e.Item.FindControl("txtComment")
        Dim txtResponse As TextBox = e.Item.FindControl("txtResponse")
        Dim btnEdit As Button = e.Item.FindControl("btnEdit")

        lblTag.Text = e.Item.DataItem("TAG_CODE").ToString
        lblProblem.Text = e.Item.DataItem("PROB_Detail").ToString
        txtComment.Text = e.Item.DataItem("PROB_Recomment").ToString
        txtResponse.Text = e.Item.DataItem("Responsible").ToString
        btnEdit.Attributes("DETAIL_ID") = e.Item.DataItem("DETAIL_ID")
        btnEdit.Attributes("PROB_Code") = e.Item.DataItem("PROB_Code")

        txtComment.Attributes("onchange") = "document.getElementById('" & btnEdit.ClientID & "').click();"
        txtResponse.Attributes("onchange") = "document.getElementById('" & btnEdit.ClientID & "').click();"
    End Sub

End Class