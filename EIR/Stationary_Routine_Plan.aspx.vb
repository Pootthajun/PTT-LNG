﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Stationary_Routine_Plan
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Routine_Report

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetPlan(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindPlan()
        Dim SQL As String = "SELECT dbo.UDF_RPT_Code(RPT_ST_Header.RPT_Year,RPT_ST_Header.RPT_No) RPT_Code,RPT_ST_Header.RPT_Year,RPT_No,PLANT_Code,ROUTE_Code,RPT_Round,RPT_Period_Start,RPT_Period_End," & vbNewLine
        SQL &= " CASE RPT_ST_Header.RPT_STEP WHEN 0 THEN CASE WHEN RPT_Period_Start<GETDATE() THEN  'New' ELSE 'Waiting' END ELSE STEP_NAME END STEP_NAME," & vbNewLine
        SQL &= " RPT_ST_Header.RPT_STEP" & vbNewLine
        SQL &= " FROM RPT_ST_Header " & vbNewLine
        SQL &= " LEFT JOIN MS_Plant ON RPT_ST_Header.PLANT_ID=MS_Plant.PLANT_ID AND MS_Plant.Active_Status=1" & vbNewLine
        SQL &= " LEFT JOIN MS_ST_Route ON MS_Plant.PLANT_ID=MS_ST_Route.PLANT_ID AND RPT_ST_Header.ROUTE_ID=MS_ST_Route.ROUTE_ID AND MS_ST_Route.Active_Status=1" & vbNewLine
        SQL &= " INNER JOIN MS_Report_Step ON RPT_ST_Header.RPT_STEP=MS_Report_Step.RPT_STEP" & vbNewLine
        SQL &= " WHERE RPT_ST_Header.RPT_Type_ID=" & RPT_Type_ID & " AND " & vbNewLine

        If ddl_Search_Year.SelectedIndex > 0 Then
            SQL &= " RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            SQL &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            SQL &= " MS_ST_Route.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND " & vbNewLine
        End If
        SQL = SQL.Substring(0, SQL.Length - 6) & vbNewLine
        SQL &= " ORDER BY RPT_Period_Start,RPT_Period_End,PLANT_Code,ROUTE_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("MN_Stationary_Plan") = DT

        Navigation.SesssionSourceName = "MN_Stationary_Plan"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblYear As Label = e.Item.FindControl("lblYear")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblStart As Label = e.Item.FindControl("lblStart")
        Dim lblEnd As Label = e.Item.FindControl("lblEnd")
        Dim lblEstimate As Label = e.Item.FindControl("lblEstimate")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblCode.Text = e.Item.DataItem("RPT_Code").ToString
        lblYear.Text = e.Item.DataItem("RPT_Year").ToString
        lblPlant.Text = e.Item.DataItem("PLANT_Code").ToString
        lblRoute.Text = e.Item.DataItem("ROUTE_Code").ToString
        lblStart.Text = BL.ReportGridTime(e.Item.DataItem("RPT_Period_Start"))
        lblEnd.Text = BL.ReportGridTime(e.Item.DataItem("RPT_Period_End"))
        lblEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, e.Item.DataItem("RPT_Period_Start"), e.Item.DataItem("RPT_Period_End")) + 1, 0)
        lblStatus.Text = e.Item.DataItem("STEP_Name").ToString
        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP").ToString)

        btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year").ToString
        btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No").ToString

        btnEdit.Visible = e.Item.DataItem("RPT_STEP") < EIR_BL.Report_Step.Inspecting_Step
        btnToggle.Visible = e.Item.DataItem("RPT_STEP") < EIR_BL.Report_Step.Inspecting_Step

        '--------------- Round Editor -----------------
        Dim txtRound As TextBox = e.Item.FindControl("txtRound")
        Dim btnUpdateRound As Button = e.Item.FindControl("btnUpdateRound")
        If Not IsDBNull(e.Item.DataItem("RPT_Round")) Then
            txtRound.Text = e.Item.DataItem("RPT_Round")
        End If
        ImplementJavaIntegerText(txtRound, False)
        txtRound.Style.Item("text-align") = "center"
        txtRound.Attributes("onchange") = "document.getElementById('" & btnUpdateRound.ClientID & "').click();"
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
        Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                '------------------------------------
                pnlListPlan.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * " & vbNewLine
                SQL &= " FROM RPT_ST_Header " & vbNewLine
                SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Plan Period Not Found"
                    pnlBindingError.Visible = True
                    BindPlan()
                    Exit Sub
                End If

                txtStart.Attributes("RPT_Year") = RPT_Year
                txtStart.Attributes("RPT_No") = RPT_No

                BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
                BL.BindDDl_ST_Route(DT.Rows(0).Item("PLANT_ID"), ddl_Edit_Route, DT.Rows(0).Item("ROUTE_ID"))
                txtStart.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("RPT_Period_Start"))
                txtEnd.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("RPT_Period_End"))
                txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, DT.Rows(0).Item("RPT_Period_Start"), DT.Rows(0).Item("RPT_Period_End")), 0)


                ddl_Edit_Plant.Enabled = False
                ddl_Edit_Route.Enabled = False
                pnl_Recurring.Visible = False

            Case "Round"

                Dim txtRound As TextBox = e.Item.FindControl("txtRound")
                Dim SQL As String = "Update RPT_ST_Header set RPT_Round="
                If txtRound.Text <> "" Then
                    SQL &= txtRound.Text
                Else
                    SQL = "NULL"
                    txtRound.Text = ""
                End If
                SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                Dim Conn As New SqlConnection(BL.ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .CommandText = SQL
                    .Connection = Conn
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()

            Case "ToggleStatus"
                Try
                    BL.Drop_RPT_ST_Header(RPT_Year, RPT_No)
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindPlan()

                lblBindingSuccess.Text = "Delete plan successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetPlan(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ClearPanelSearch()
        BindPlan()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------

        pnlListPlan.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        BL.BindDDlPlant(ddl_Edit_Plant)
        BL.BindDDl_ST_Route(0, ddl_Edit_Route)

        txtStart.Text = ""
        txtStart.Attributes("RPT_Year") = "0"
        txtStart.Attributes("RPT_No") = "0"
        txtEstimate.Text = ""
        txtEnd.Text = ""

        pnl_Recurring.Visible = False
        chkRecurring.Checked = False
        txt_Interval.Text = ""
        ddl_Repeat.SelectedIndex = 0
        txt_Repeat.Text = ""

        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")

        btnCreate.Visible = USER_LEVEL = EIR_BL.User_Level.Administrator Or
                            USER_LEVEL = EIR_BL.User_Level.Inspector Or
                            USER_LEVEL = EIR_BL.User_Level.Approver

        '-------------InStall Javascript------------
        ImplementJavaIntegerText(txtEstimate, True)
        ImplementJavaIntegerText(txt_Interval, False)
        ImplementJavaIntegerText(txt_Repeat, False)

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_ST_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.Text = Now.Year + 543

        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDl_ST_Route(0, ddl_Search_Route)
    End Sub

    Protected Sub ddl_Search_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Route_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Route)
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        ddl_Edit_Plant.Enabled = True
        ddl_Edit_Route.Enabled = True
        ddl_Edit_Plant.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnl_Recurring.Visible = True
        txt_Interval.Enabled = False
        ddl_Repeat.Enabled = False
        txt_Repeat.Enabled = False
    End Sub

    Protected Sub chkRecurring_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRecurring.CheckedChanged
        txt_Interval.Enabled = chkRecurring.Checked
        ddl_Repeat.Enabled = chkRecurring.Checked
        txt_Repeat.Enabled = chkRecurring.Checked
    End Sub

    Protected Sub txtStart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStart.TextChanged
        If Not BL.IsProgrammingDate(txtStart.Text) Then
            txtStart.Text = ""
            txtEstimate.Text = ""
            Exit Sub
        End If

        Dim StartDate As Date = DateTime.Parse(txtStart.Text)

        If Not BL.IsProgrammingDate(txtEnd.Text) Then
            If IsNumeric(txtEstimate.Text) Then
                Dim EndDate = DateAdd(DateInterval.Day, CInt(txtEstimate.Text) - 1, StartDate)
                txtEnd.Text = BL.ReportProgrammingDate(EndDate)
                Exit Sub
            Else
                txtEstimate.Text = ""
                txtEnd.Text = ""
                Exit Sub
            End If
        Else
            Dim EndDate As Date = DateTime.Parse(txtEnd.Text)
            txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, StartDate, EndDate) + 1, 0)
            Exit Sub
        End If

    End Sub

    Protected Sub txtEnd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEnd.TextChanged
        If Not BL.IsProgrammingDate(txtEnd.Text) Then
            txtEnd.Text = ""
            txtEstimate.Text = ""
            Exit Sub
        End If

        Dim EndDate As Date = DateTime.Parse(txtEnd.Text)

        If Not BL.IsProgrammingDate(txtStart.Text) Then
            If IsNumeric(txtEstimate.Text) Then
                Dim StartDate As Date = DateAdd(DateInterval.Day, -CInt(txtEstimate.Text) - 1, EndDate)
                txtStart.Text = BL.ReportProgrammingDate(StartDate)
                Exit Sub
            Else
                txtEstimate.Text = ""
                txtStart.Text = ""
                Exit Sub
            End If
        Else
            Dim StartDate As Date = DateTime.Parse(txtStart.Text)
            txtEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, StartDate, EndDate) + 1, 0)
            Exit Sub
        End If

    End Sub

    Protected Sub txtEstimate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEstimate.TextChanged
        If Not IsNumeric(txtEstimate.Text) Then
            txtEstimate.Text = ""
            If BL.IsProgrammingDate(txtStart.Text) Then
                txtEnd.Text = ""
                Exit Sub
            End If
            If BL.IsProgrammingDate(txtEnd.Text) Then
                txtStart.Text = ""
                Exit Sub
            End If
            Exit Sub
        End If

        If BL.IsProgrammingDate(txtStart.Text) Then
            Dim StartDate As Date = DateTime.Parse(txtStart.Text)
            Dim EndDate As Date = DateAdd(DateInterval.Day, CInt(txtEstimate.Text) - 1, StartDate)
            txtEnd.Text = BL.ReportProgrammingDate(EndDate)
            Exit Sub
        End If

        If BL.IsProgrammingDate(txtEnd.Text) Then
            Dim EndDate As Date = DateTime.Parse(txtEnd.Text)
            Dim StartDate As Date = DateAdd(DateInterval.Day, -CInt(txtEstimate.Text) - 1, EndDate)
            txtStart.Text = BL.ReportProgrammingDate(StartDate)
            Exit Sub
        End If

        txtStart.Text = ""
        txtEnd.Text = ""

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If ddl_Edit_Plant.SelectedIndex < 1 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If Not BL.IsProgrammingDate(txtStart.Text) Then
            lblValidation.Text = "Please select Start Date"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If Not BL.IsProgrammingDate(txtEnd.Text) Then
            lblValidation.Text = "Please select End Date"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If chkRecurring.Checked Then
            If Not IsNumeric(txt_Interval.Text) Or ddl_Repeat.SelectedIndex < 1 Or Not IsNumeric(txt_Repeat.Text) Then
                lblValidation.Text = "Please completed all Recurring Setting"
                pnlValidation.Visible = True
                Exit Sub
            End If
        End If

        Dim Old_RPT_Year As Integer = txtStart.Attributes("RPT_Year")
        Dim Old_RPT_No As Integer = txtStart.Attributes("RPT_No")

        Dim StartDate As Date = C.StringToDate(txtStart.Text, "yyyy-MM-dd")
        Dim EndDate As Date = C.StringToDate(txtEnd.Text, "yyyy-MM-dd")
        Dim Estimate As Integer = DateDiff(DateInterval.Day, StartDate, EndDate)

        Dim RPT_Year As Integer = Year(StartDate) + 543
        Dim RPT_No As Integer = Old_RPT_No

        Dim SQL As String = "SELECT * FROM RPT_ST_Header WHERE RPT_Year=" & Old_RPT_Year & " AND RPT_No=" & Old_RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Select Case lblUpdateMode.Text
            Case "Create"

                Dim TotalPeriod As Integer = 1
                If chkRecurring.Checked Then TotalPeriod = CInt(txt_Repeat.Text)

                For i As Integer = 1 To TotalPeriod
                    Dim DR As DataRow = DT.NewRow
                    RPT_No = GetNewPlanNo(RPT_Year)
                    DR("RPT_Year") = RPT_Year
                    DR("RPT_No") = RPT_No
                    DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
                    DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
                    DR("RPT_Step") = EIR_BL.Report_Step.New_Step

                    Dim Interval As Integer = 0
                    If IsNumeric(txt_Interval.Text) Then
                        Interval = CInt(txt_Interval.Text)
                    End If

                    Dim _start As Date = StartDate
                    Select Case ddl_Repeat.Items(ddl_Repeat.SelectedIndex).Value
                        Case "D"
                            _start = DateAdd(DateInterval.Day, (Interval * (i - 1)), StartDate)
                        Case "W"
                            _start = DateAdd(DateInterval.Weekday, (Interval * (i - 1)), StartDate)
                        Case "M"
                            _start = DateAdd(DateInterval.Month, (Interval * (i - 1)), StartDate)
                    End Select
                    DR("RPT_Period_Start") = _start
                    DR("RPT_Period_End") = DateAdd(DateInterval.Day, Estimate, _start)

                    DR("RPT_Type_ID") = RPT_Type_ID
                    DR("Update_Time") = Now
                    DR("Update_By") = Session("USER_ID")
                    DT.Rows.Add(DR)

                    Dim cmd As New SqlCommandBuilder(DA)
                    Try
                        DA.Update(DT)
                        DT.AcceptChanges()
                    Catch ex As Exception
                        lblValidation.Text = ex.Message
                        pnlValidation.Visible = True
                        Exit Sub
                    End Try
                Next
            Case "Update"
                If DT.Rows.Count = 0 Then
                    lblValidation.Text = "This plan period has been removed from schedule."
                    pnlValidation.Visible = True
                    Exit Sub
                End If
                Dim DR As DataRow = DT.Rows(0)
                DR("RPT_Period_Start") = StartDate
                DR("RPT_Period_End") = EndDate
                DR("Update_Time") = Now
                DR("Update_By") = Session("USER_ID")

                Dim cmd As New SqlCommandBuilder(DA)
                Try
                    DA.Update(DT)
                Catch ex As Exception
                    lblValidation.Text = ex.Message
                    pnlValidation.Visible = True
                    Exit Sub
                End Try
        End Select



        ResetPlan(Nothing, Nothing)
        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewPlanNo(ByVal RPT_Year As Integer) As Integer

        Dim SQL As String = "SELECT IsNull(MIN(RPT_No),0)-1 FROM RPT_ST_Header" & vbNewLine
        SQL &= "  WHERE RPT_No < 0 AND RPT_Year=" & RPT_Year
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function


End Class