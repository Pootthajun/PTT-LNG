﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Rotating_OffRoutine_Edit4
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Rotating_Off_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public ReadOnly Property UNIQUEPAGEID() As String
        Get
            Return RPT_No
        End Get
    End Property



    Public Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Rotating_OffRoutine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_RO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_RO_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_RO_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_RO_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            BindTabData()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                '.RPT_Type = RPT_Type_ID
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_OffRoutine_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year

        '------------------ GET TAG ID------------------------------
        SQL = "SELECT TOP 1 TAG_ID FROM RPT_RO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_OffRoutine_Summary.aspx")
            Exit Sub
        End If
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        lbl_TAG.Text = BL.Get_Tag_Code_Rotating(TAG_ID)
        '------------------------------Header -----------------------------------
        GetAllTag()

        '------------------------------ Bind Photo-----------------------------------
        SQL = " SELECT PIC_Detail FROM RO_Vibration " & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("PIC_Detail")) Then
            Session("PREVIEW_IMG_" & UNIQUEPAGEID & "_1") = DT.Rows(0).Item("PIC_Detail")
            imgPreview.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUEPAGEID & "&Image=1"
        Else
            Session("PREVIEW_IMG_" & UNIQUEPAGEID & "_1") = Nothing
            imgPreview.ImageUrl = "resources/images/Sample_40.png"
        End If
    End Sub

    Private Sub GetAllTag()
        Dim DT As DataTable = BL.Get_All_RO_Tag_By_Report(RPT_Year, RPT_No)
        DT.DefaultView.RowFilter = "TAG_ID=" & TAG_ID
        Session("TagStatus_" & RPT_Year & "_" & RPT_No) = DT.DefaultView.ToTable
    End Sub

#Region "Navigator"

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabPhoto.Click, btn_Back.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDocument.Click, btn_Next.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit5.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit6.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

#End Region

#Region "Toolbar"
    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM RO_Vibration WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            .Connection = Conn
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        BindTabData()
    End Sub

    Protected Sub lnkUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUpload.Click, imgPreview.Click
        Session("PREVIEW_IMG_" & UNIQUEPAGEID & "_1") = BL.Get_Rotating_OffRoutine_Vibration_Image(RPT_Year, RPT_No)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUEPAGEID & "',1,document.getElementById('" & btnSaveImage.ClientID & "'));", True)
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

    Protected Sub lnkRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub btnSaveImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImage.Click
        If IsNothing(Session("TempImage_" & UNIQUEPAGEID & "_1")) Then Exit Sub

        '------------------------ Save ------------------------
        Dim SQL As String = "SELECT * FROM RO_Vibration WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_Year
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("DETAIL_ID") = BL.GetNew_RO_Vibration_ID
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("PIC_ID") = 1
        Else
            DR = DT.Rows(0)
        End If
        DR("PIC_Detail") = Session("TempImage_" & UNIQUEPAGEID & "_1")
        DR("Update_Time") = Now
        DR("Update_By") = Session("USER_ID")

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------------Refresh----------------
        BindTabData()
    End Sub
#End Region


End Class