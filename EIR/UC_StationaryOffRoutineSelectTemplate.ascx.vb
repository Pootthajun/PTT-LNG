﻿Imports System.IO


Public Class UC_StationaryOffRoutineSelectTemplate
    Inherits System.Web.UI.UserControl
    Dim BL As New EIR_BL
    Public Event SelectTemplate(TemplateType As OffRoutineTemplateType)

    Public Sub ShowDialog()
        Me.Visible = True
    End Sub


    Public Sub CloseDialog()
        Me.Visible = False
    End Sub

    Private Sub btn_Close_Click(sender As Object, e As EventArgs) Handles btn_Close.Click
        CloseDialog()
    End Sub

#Region "Select Template"
    Private Sub lnkTemplate1_Click(sender As Object, e As EventArgs) Handles lnkTemplate1.Click
        SeleteTemplate(OffRoutineTemplateType.Template1)
    End Sub

    Private Sub lnkTemplate2_Click(sender As Object, e As EventArgs) Handles lnkTemplate2.Click
        SeleteTemplate(OffRoutineTemplateType.Template2)
    End Sub

    Private Sub lnkTemplate3_Click(sender As Object, e As EventArgs) Handles lnkTemplate3.Click
        SeleteTemplate(OffRoutineTemplateType.Template3)
    End Sub

    Private Sub lnkTemplate4_Click(sender As Object, e As EventArgs) Handles lnkTemplate4.Click
        SeleteTemplate(OffRoutineTemplateType.Template4)
    End Sub

    Private Sub lnkTemplate5_Click(sender As Object, e As EventArgs) Handles lnkTemplate5.Click
        SeleteTemplate(OffRoutineTemplateType.Template5)
    End Sub

    Private Sub lnkTemplate6_Click(sender As Object, e As EventArgs) Handles lnkTemplate6.Click
        SeleteTemplate(OffRoutineTemplateType.Template6)
    End Sub

    Private Sub SeleteTemplate(tmp As OffRoutineTemplateType)
        RaiseEvent SelectTemplate(tmp)
        CloseDialog()
    End Sub



#End Region
    Public Enum OffRoutineTemplateType
        Template1 = 1
        Template2 = 2
        Template3 = 3
        Template4 = 4
        Template5 = 5
        Template6 = 6
    End Enum
End Class