﻿Imports System.Data.SqlClient

Public Class PIPE_CUI_Edit6
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE
    Dim C As New Converter
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Pipe_CUI_Reports
    Dim RPT_Step As EIR_PIPE.Report_Step = EIR_PIPE.Report_Step.Insulating_Inspection

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property PAGE_UNIQUE_ID() As String
        Get
            If IsNothing(ViewState("PAGE_UNIQUE_ID")) OrElse ViewState("PAGE_UNIQUE_ID") = "" Then
                ViewState("PAGE_UNIQUE_ID") = Now.ToOADate.ToString.Replace(".", "")
            End If
            Return ViewState("PAGE_UNIQUE_ID")
        End Get
        Set(ByVal value As String)
            ViewState("PAGE_UNIQUE_ID") = value
        End Set
    End Property

    Private Property TAG_CODE As String
        Get
            Return lbl_Tag.Text
        End Get
        Set(ByVal value As String)
            lbl_Tag.Text = value
        End Set
    End Property

    Private Property TAG_ID As Integer
        Get
            Try
                Return ViewState("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property POINT_ID As Integer
        Get
            Try
                Return ViewState("POINT_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("POINT_ID") = value
        End Set
    End Property

    Private Property HeaderProgress As EIR_PIPE.Report_Step
        Get
            Try
                Return ViewState("HeaderProgress")
            Catch ex As Exception
                Return EIR_PIPE.Report_Step.NA
            End Try
        End Get
        Set(ByVal value As EIR_PIPE.Report_Step)
            ViewState("HeaderProgress") = value
        End Set
    End Property

    Private Property IsFinished As Boolean
        Get
            Try
                Return ViewState("IsFinished")
            Catch ex As Exception
                Return False
            End Try
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsFinished") = value
        End Set
    End Property

    Private Property ImageFile1 As FileAttachment
        Get
            Try
                Return Session("PIPE_CUI_Edit6_1_" & PAGE_UNIQUE_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As FileAttachment)
            Session("PIPE_CUI_Edit6_1_" & PAGE_UNIQUE_ID) = value
            If Not IsNothing(value) Then
                img_File1.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PIPE_CUI_Edit6_1_" & PAGE_UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                ZoomMask1.Attributes("onclick") = "window.open('" & img_File1.BackImageUrl & "');"
                ZoomMask1.Visible = True
                btnDelete1.Visible = True
            Else
                img_File1.BackImageUrl = "Resources/Images/Sample_40.png"
                ZoomMask1.Visible = False
                btnDelete1.Visible = False
            End If
        End Set
    End Property

    Private Property ImageFile2 As FileAttachment
        Get
            Try
                Return Session("PIPE_CUI_Edit6_2_" & PAGE_UNIQUE_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As FileAttachment)
            Session("PIPE_CUI_Edit6_2_" & PAGE_UNIQUE_ID) = value
            If Not IsNothing(value) Then
                img_File2.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PIPE_CUI_Edit6_2_" & PAGE_UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                ZoomMask2.Attributes("onclick") = "window.open('" & img_File2.BackImageUrl & "');"
                ZoomMask2.Visible = True
                btnDelete2.Visible = True
            Else
                img_File2.BackImageUrl = "Resources/Images/Sample_40.png"
                ZoomMask2.Visible = False
                btnDelete2.Visible = False
            End If
        End Set
    End Property

    Private Property ImageFile3 As FileAttachment
        Get
            Try
                Return Session("PIPE_CUI_Edit6_3_" & PAGE_UNIQUE_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As FileAttachment)
            Session("PIPE_CUI_Edit6_3_" & PAGE_UNIQUE_ID) = value
            If Not IsNothing(value) Then
                img_File3.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PIPE_CUI_Edit6_3_" & PAGE_UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                ZoomMask3.Attributes("onclick") = "window.open('" & img_File3.BackImageUrl & "');"
                ZoomMask3.Visible = True
                btnDelete3.Visible = True
            Else
                img_File3.BackImageUrl = "Resources/Images/Sample_40.png"
                ZoomMask3.Visible = False
                btnDelete3.Visible = False
            End If
        End Set
    End Property

    Private Property ImageFile4 As FileAttachment
        Get
            Try
                Return Session("PIPE_CUI_Edit6_4_" & PAGE_UNIQUE_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As FileAttachment)
            Session("PIPE_CUI_Edit6_4_" & PAGE_UNIQUE_ID) = value
            If Not IsNothing(value) Then
                img_File4.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PIPE_CUI_Edit6_4_" & PAGE_UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                ZoomMask4.Attributes("onclick") = "window.open('" & img_File4.BackImageUrl & "');"
                ZoomMask4.Visible = True
                btnDelete4.Visible = True
            Else
                img_File4.BackImageUrl = "Resources/Images/Sample_40.png"
                ZoomMask4.Visible = False
                btnDelete4.Visible = False
            End If
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("PIPE_CUI_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("Select * FROM RPT_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='PIPE_CUI_Summary.aspx'", True)
                    Exit Sub
                End If
            End If
            '--------------UPDATE RESPONSIBLE PERSON (Do Nothing)------------
            Try
                SetUserAuthorization()
                BindTabData()
                BindTabNavigation()
            Catch ex As Exception
                lblBindingError.Text = ex.Message
                pnlBindingError.Visible = True
            End Try
        End If

        StoreJS()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Private Sub btnValidationClose_Click(sender As Object, e As ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

#End Region

    Private Sub StoreJS()
        Dim Script As String = "        window.onresize = resize_PIPE_Report_Window;
                                        $(document).ready(resize_PIPE_Report_Window);"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJS", Script, True)
    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='PIPE_CUI_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            Dim PM As New Report_CUI_ERO_Permission
            With PM

                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                .IsFinished = Not IsDBNull(DT.Rows(0).Item("Finished")) AndAlso DT.Rows(0).Item("Finished")

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    Msg &= "-Report is posted completely\n" & vbNewLine
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='PIPE_CUI_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

            '---------- Collect Information ------------
            HeaderProgress = DT.Rows(0).Item("RPT_STEP")
            IsFinished = DT.Rows(0).Item("Finished")

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='PIPE_CUI_Summary.aspx'", True)
            Exit Sub
        End If
    End Sub

    Public Sub BindTabData()

        Dim SQL As String = "SELECT * FROM VW_PIPE_CUI_Insulating WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("PIPE_CUI_Summary.aspx")
            Exit Sub
        End If

        lblReportCode.Text = DT.Rows(0).Item("RPT_Code")
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        POINT_ID = DT.Rows(0).Item("POINT_ID")
        TAG_CODE = DT.Rows(0).Item("TAG_CODE")

        lbl_Point.Text = DT.Rows(0).Item("POINT_Name")

        If Not IsDBNull(DT.Rows(0).Item("RPT_Date")) Then
            txt_Rpt_Date.Text = C.DateToString(DT.Rows(0).Item("RPT_Date"), txt_Rpt_Date_Extender.Format)
        Else
            txt_Rpt_Date.Text = ""
        End If

        '----------- Pipe Info----------
        With Pipe_Info
            .BindData(TAG_ID)
            .DisplayMainPointProperty = False
            .SetPropertyEditable(UC_Pipe_Tag_Info.PropertyItem.All, False)
        End With
        '----------- Point Info----------
        With Point_Info
            .BindData(TAG_ID, POINT_ID)
            .RaisePropertyChangedByUser = False
            .SetPropertyEditable(UC_Pipe_Point_Info.PropertyItem.All, True)
        End With

        '----------- Photography -----------
        ImageFile1 = PIPE.Get_FileAttachment_By_ID(DT.Rows(0).Item("IMG1").ToString)
        ImageFile2 = PIPE.Get_FileAttachment_By_ID(DT.Rows(0).Item("IMG2").ToString)
        ImageFile3 = PIPE.Get_FileAttachment_By_ID(DT.Rows(0).Item("IMG3").ToString)
        ImageFile4 = PIPE.Get_FileAttachment_By_ID(DT.Rows(0).Item("IMG4").ToString)

        txtFileDesc1.Text = Trim(DT.Rows(0).Item("IMG1_Desc").ToString)
        txtFileDesc2.Text = Trim(DT.Rows(0).Item("IMG2_Desc").ToString)
        txtFileDesc3.Text = Trim(DT.Rows(0).Item("IMG3_Desc").ToString)
        txtFileDesc4.Text = Trim(DT.Rows(0).Item("IMG4_Desc").ToString)

        '----------- Visual Inspection -----
        txt_Note.Text = Trim(DT.Rows(0).Item("Note").ToString)
        txt_Recomment.Text = Trim(DT.Rows(0).Item("Recommendation").ToString)

        '-------------- Bind Tab Navigation ---------------
        lblStep.Text = PIPE.Get_ReportStep_Name(RPT_Step)

    End Sub

#Region "Display Tab Navigation"
    Private Sub BindTabNavigation()
        Dim DT As DataTable = PIPE.Get_ReportStep(EIR_BL.Report_Type.Pipe_CUI_Reports)
        rptTab.DataSource = DT
        rptTab.DataBind()
    End Sub

    Private Sub rptTab_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTab.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim ThisStep As EIR_PIPE.Report_Step = e.Item.DataItem("RPT_STEP")
        If ThisStep > HeaderProgress Then
            e.Item.Visible = False
            Exit Sub
        End If

        Dim HTab As HtmlAnchor = e.Item.FindControl("HTab")
        HTab.InnerHtml = e.Item.DataItem("STEP_Name")
        If CInt(ThisStep) <> RPT_Step Then
            HTab.HRef = "PIPE_CUI_Edit" & CInt(ThisStep) & ".aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
            HTab.Title = "Go to see " & e.Item.DataItem("STEP_Name")
        Else
            HTab.Attributes("Class") = "default-tab current"
        End If
    End Sub

#End Region

#Region "Image Functional"
    Private Sub btnEditFile_Click(sender As Object, e As ImageClickEventArgs) Handles btnEdit1.Click, btnEdit2.Click, btnEdit3.Click, btnEdit4.Click
        Dim btnEdit As ImageButton = sender
        Dim ImageID As Integer = btnEdit.ID.Replace("btnEdit", "")
        Dim RefreshButton As Button = Nothing

        Dim Obj As FileAttachment = Nothing
        Dim txtFileDesc As TextBox = Nothing
        Select Case ImageID
            Case 1
                Obj = ImageFile1
                RefreshButton = btnRefresh1
                txtFileDesc = txtFileDesc1
            Case 2
                Obj = ImageFile2
                RefreshButton = btnRefresh2
                txtFileDesc = txtFileDesc2
            Case 3
                Obj = ImageFile3
                RefreshButton = btnRefresh3
                txtFileDesc = txtFileDesc3
            Case 4
                Obj = ImageFile4
                RefreshButton = btnRefresh4
                txtFileDesc = txtFileDesc4
        End Select

        If IsNothing(Obj) Then
            Obj = New FileAttachment
            With Obj
                .GenerateUniqueID()
                Select Case ImageID
                    Case 1
                        .DocType = FileAttachment.AttachmentType.Photograph
                        ImageFile1 = Obj
                    Case 2
                        .DocType = FileAttachment.AttachmentType.Photograph
                        ImageFile2 = Obj
                    Case 3
                        .DocType = FileAttachment.AttachmentType.Photograph
                        ImageFile3 = Obj
                    Case 4
                        .DocType = FileAttachment.AttachmentType.Photograph
                        ImageFile4 = Obj
                End Select
            End With
        End If
        Obj.Description = txtFileDesc.Text
        ShowDialogEditSVG(Me.Page, "PIPE_CUI_Edit6_" & ImageID & "_" & PAGE_UNIQUE_ID, "", RefreshButton.ClientID, "")
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh1.Click, btnRefresh2.Click, btnRefresh3.Click, btnRefresh4.Click
        Dim btnRefresh As Button = sender
        Dim ImageID As Integer = btnRefresh.ID.Replace("btnRefresh", "")

        Select Case ImageID
            Case 1
                ImageFile1 = ImageFile1
                txtFileDesc1.Text = ImageFile1.Description
            Case 2
                ImageFile2 = ImageFile2
                txtFileDesc2.Text = ImageFile2.Description
            Case 3
                ImageFile3 = ImageFile3
                txtFileDesc3.Text = ImageFile3.Description
            Case 4
                ImageFile4 = ImageFile4
                txtFileDesc4.Text = ImageFile4.Description
        End Select
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As ImageClickEventArgs) Handles btnDelete1.Click, btnDelete2.Click, btnDelete3.Click, btnDelete4.Click
        Dim btnDelete As ImageButton = sender
        Dim ImageID As Integer = btnDelete.ID.Replace("btnDelete", "")

        Select Case ImageID
            Case 1
                ImageFile1 = Nothing
            Case 2
                ImageFile2 = Nothing
            Case 3
                ImageFile3 = Nothing
            Case 4
                ImageFile4 = Nothing
        End Select
    End Sub

#End Region

#Region "Saving"

    Private Sub SaveTab()
        '---------------- Validate ---------------
        If Point_Info.ValidateIncompleteMessage <> "" Then
            Dim ER As New Exception(Point_Info.ValidateIncompleteMessage)
            Throw (ER)
        End If
        Try
            Point_Info.SaveData()
        Catch ex As Exception
            Dim ER As New Exception(ex.Message)
            Throw (ER)
        End Try

        '---------------- Save Step Detail---------
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_Insulating WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
        Else
            DR = DT.Rows(0)
        End If
        Try
            DR("RPT_Date") = C.StringToDate(txt_Rpt_Date.Text, txt_Rpt_Date_Extender.Format)
        Catch ex As Exception
            DR("RPT_Date") = DBNull.Value
        End Try

        DR("IMG1_Desc") = txtFileDesc1.Text
        DR("IMG2_Desc") = txtFileDesc2.Text
        DR("IMG3_Desc") = txtFileDesc3.Text
        DR("IMG4_Desc") = txtFileDesc4.Text
        DR("Note") = txt_Note.Text
        DR("Recommendation") = txt_Recomment.Text

        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)
        CMD.Dispose()
        DA.Dispose()
        DT.Dispose()
        '---------------- Save Image File---------------------------
        '---------------- DROP ALL File---------------------------
        PIPE.Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)
        If Not IsNothing(ImageFile1) Then
            PIPE.SAVE_RPT_FILE(RPT_Year, RPT_No, RPT_Step, 1, ImageFile1)
        End If
        If Not IsNothing(ImageFile2) Then
            PIPE.SAVE_RPT_FILE(RPT_Year, RPT_No, RPT_Step, 2, ImageFile2)
        End If
        If Not IsNothing(ImageFile3) Then
            PIPE.SAVE_RPT_FILE(RPT_Year, RPT_No, RPT_Step, 3, ImageFile3)
        End If
        If Not IsNothing(ImageFile4) Then
            PIPE.SAVE_RPT_FILE(RPT_Year, RPT_No, RPT_Step, 4, ImageFile4)
        End If

    End Sub
#End Region

#Region "Navigation Button"

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try
        Response.Redirect("PIPE_CUI_Summary.aspx", True)
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            pnlValidation.Focus()
            Exit Sub
        End Try
        Response.Redirect("PIPE_CUI_Edit5.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&", True)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            pnlValidation.Focus()
            Exit Sub
        End Try
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Saving", "alert('Save successfully');", True)
        BindTabData()
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try
        BindTabData()
        '------------ Validate For Completion For This Step--------------
        If Not IsInformationCompleted Then Exit Sub

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_Final_Report WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)

        '-----------------Create Default Info If Not Existed------------
        If DT.Rows.Count = 0 Then
            Dim DR As DataRow
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("Update_By") = Session("USER_ID")
            DR("Update_Time") = Now
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT)
            '------------------Update CUI_Header Step-----------------------
            PIPE.Update_CUI_Header_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Final, Session("USER_ID"))
        End If

        '------------ Redirect to next step-----------------------------
        Response.Redirect("PIPE_CUI_Edit7.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&r=1", True)

    End Sub

    Public ReadOnly Property IsInformationCompleted() As Boolean
        Get
            If txt_Rpt_Date.Text = "" Then
                lblValidation.Text = "Select report date"
                pnlValidation.Visible = True
                Return False
            End If
            If Pipe_Info.ValidateIncompleteMessage <> "" Then
                lblValidation.Text = Pipe_Info.ValidateIncompleteMessage
                pnlValidation.Visible = True
                Return False
            End If
            If Point_Info.ValidateIncompleteMessage <> "" Then
                lblValidation.Text = Point_Info.ValidateIncompleteMessage
                pnlValidation.Visible = True
                Return False
            End If
            If IsNothing(ImageFile1) And txtFileDesc1.Text <> "" Then
                lblValidation.Text = "Upload Visual Image#1 neither leave information to blank"
                pnlValidation.Visible = True
                Return False
            End If
            If IsNothing(ImageFile2) And txtFileDesc2.Text <> "" Then
                lblValidation.Text = "Upload Visual Image#2 neither leave information to blank"
                pnlValidation.Visible = True
                Return False
            End If
            If IsNothing(ImageFile3) And txtFileDesc3.Text <> "" Then
                lblValidation.Text = "Upload Visual Image#3 neither leave information to blank"
                pnlValidation.Visible = True
                Return False
            End If
            If IsNothing(ImageFile4) And txtFileDesc4.Text <> "" Then
                lblValidation.Text = "Upload Visual Image#4 neither leave information to blank"
                pnlValidation.Visible = True
                Return False
            End If


            Return True
        End Get
    End Property

    Private Sub btnPost_Click(sender As Object, e As EventArgs) Handles btnPost.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            pnlValidation.Focus()
            Exit Sub
        End Try
        BindTabData()
        '------------ Validate For Completion For This Step--------------
        If Not IsInformationCompleted Then Exit Sub

        '------------ Request Filename To Save Result PDF ---------------
        Dim DefaultValue As String = Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & (Now.Year + 543).ToString.Substring(2, 2) & "_" & lbl_Plant.Text.Replace("#", "").Replace("_", "") & "_PIPE_CUI_"
        DefaultValue &= lbl_Tag.Text.Replace("""", "(inch)") & "_" & UCase(Session("USER_Name")) & "_" & lblReportCode.Text & ".PDF"
        DialogInput.ShowDialog("Please insert finalize report file name..", DefaultValue)
    End Sub

    Protected Sub DialogInput_AnswerDialog(ByVal Result As String) Handles DialogInput.AnswerDialog
        If Result = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please insert file name to be saved');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If
        If Not BL.IsFormatFileName(Result) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('File name must not contained following excepted charactors /\:*?""<>|;');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) RPT_Code FROM RPT_PIPE_CUI_Header WHERE Result_FileName='" & Replace(Result, "'", "''") & "' AND RPT_Year<>" & RPT_Year & " AND RPT_No<>" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This file name is already exists. And has been reserved for report " & DT.Rows(0).Item("RPT_Code") & "');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        '---------------- Generate Posted Report-----------
        If Result.Length >= 4 AndAlso Right(Result, 4).ToUpper <> ".PDF".ToUpper Then
            Result = Result & ".PDF"
        ElseIf Result.Length < 4 Then
            Result = Result & ".PDF"
        End If

        Dim DestinationPath As String = BL.PostedReport_Path & "\" & Result
        Dim GenerateResult = BL.GeneratePostedReport(RPT_Year, RPT_No, DestinationPath)
        If Not GenerateResult.Success Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Unavailable to posted report !!'); alert('" & GenerateResult.Message.Replace("'", """") & "');", True)
            DialogInput.Visible = False
            Exit Sub
        End If
        DialogInput.Visible = False
        '--------------------- Delete Next Later Step -------------------------------
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Final)
        '--------------------- Update Report Status ---------------------------------
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_PIPE_CUI_Header set RPT_LOCK_BY=NULL,RPT_STEP=" & CInt(RPT_Step) & ",Finished_Time=GETDATE(),Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE(),Result_FileName='" & Replace(Result, "'", "''") & "'" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Report has been approved and job done!!'); ShowPreviewReport(" & RPT_Year & "," & RPT_No & "); window.location.href='PIPE_CUI_Summary.aspx';", True)
    End Sub


    Private Sub lnkClear_Click(sender As Object, e As EventArgs) Handles lnkClear.Click

        '------------- Clear Image-------------
        PIPE.Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_Insulating WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)

        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
        Else
            DR = DT.Rows(0)
        End If
        '---------- Already Drop Image-----------
        DR("RPT_Date") = DBNull.Value
        DR("IMG1_Desc") = DBNull.Value
        DR("IMG2_Desc") = DBNull.Value
        DR("IMG3_Desc") = DBNull.Value
        DR("IMG4_Desc") = DBNull.Value
        DR("Note") = DBNull.Value
        DR("Recommendation") = DBNull.Value
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTabData()
    End Sub

    Private Sub lnkPreview_Click(sender As Object, e As EventArgs) Handles lnkPreview.Click
        '------------- Save First ---------
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            pnlValidation.Focus()
            Exit Sub
        End Try
        BindTabData()

        '------------- Show Dialog-------------
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Detail_ID=" & CInt(RPT_Step) & "&"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PreviewReport", "ShowPreviewReportWithParam('" & Param & "');", True)
    End Sub

#End Region
End Class