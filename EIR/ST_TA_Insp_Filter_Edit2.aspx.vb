﻿
Imports System.Data
Imports System.Data.SqlClient
Public Class ST_TA_Insp_Filter_Edit2
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Turnaround_Inspection_Reports

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("ST_TA_Inspection_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_TA_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='ST_TA_Inspection_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------Check Permisson----------------------


            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindTabData()
            pnlValidation.Visible = False

        End If
    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_TA_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("ST_TA_Inspection_Summary.aspx")
            Exit Sub
        End If


        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Equipment.Text = DT.Rows(0).Item("TAG_TYPE_NAME")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If

        BindTag()

    End Sub


    Private Sub BindTag()



        Dim SQL As String = " " & vbNewLine
        SQL &= "    SELECT RPT_ST_TA_Detail.DETAIL_ID												 " & vbNewLine
        SQL &= "         ,RPT_ST_TA_Detail.RPT_Year													 " & vbNewLine
        SQL &= "         ,RPT_ST_TA_Detail.RPT_No													 " & vbNewLine
        SQL &= "         ,RPT_ST_TA_Detail.TAG_ID													 " & vbNewLine
        SQL &= "         ,RPT_ST_TA_Detail.TAG_TYPE_ID												 " & vbNewLine
        SQL &= "         ,TAG_CODE,TAG_No,TAG_Name,TAG_TYPE_Name,AREA_CODE,PROC_CODE,RPT_ST_TA_Detail.PLANT_ID,ROUTE_ID,To_Table " & vbNewLine
        SQL &= "         ,STATUS_ID																	 " & vbNewLine
        SQL &= "     FROM RPT_ST_TA_Detail															 " & vbNewLine
        SQL &= "     LEFT JOIN VW_ST_TA_TAG On VW_ST_TA_TAG.TAG_ID=RPT_ST_TA_Detail.TAG_ID 			 " & vbNewLine
        SQL &= "   							And VW_ST_TA_TAG.TAG_TYPE_ID=RPT_ST_TA_Detail.TAG_TYPE_ID" & vbNewLine
        SQL &= "     WHERE  RPT_ST_TA_Detail.RPT_Year = '" & RPT_Year & "' And										 " & vbNewLine
        SQL &= "           RPT_ST_TA_Detail.RPT_No = '" & RPT_No & "'											 " & vbNewLine

        'Dim WHERE As String = ""

        'If ddl_Edit_Plant.SelectedIndex > 0 Then
        '    WHERE &= " TB.PLANT_ID=" & ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value & " And "
        'Else
        '    WHERE &= " 0=1 And "

        'End If

        'If ddl_Edit_Tag_Type.SelectedIndex > 0 Then
        '    WHERE &= " TB.TAG_TYPE_ID=" & ddl_Edit_Tag_Type.Items(ddl_Edit_Tag_Type.SelectedIndex).Value & " And "
        'Else
        '    WHERE &= " 0=1 And "

        'End If

        'If WHERE <> "" Then
        '    SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        'End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception

            Exit Sub
        End Try

        Session("MS_ST_TA_TAG_Filter") = DT

        Navigation.SesssionSourceName = "MS_ST_TA_TAG_Filter"
        Navigation.RenderLayout()

    End Sub


    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")

        Dim lnkAsFound As LinkButton = e.Item.FindControl("lnkAsFound")
        Dim lnkAfterclean As LinkButton = e.Item.FindControl("lnkAfterclean")
        Dim lnkNDE As LinkButton = e.Item.FindControl("lnkNDE")
        Dim lnkRepair As LinkButton = e.Item.FindControl("lnkRepair")
        Dim lnkAfterRepair As LinkButton = e.Item.FindControl("lnkAfterRepair")
        Dim lnkFinal As LinkButton = e.Item.FindControl("lnkFinal")

        Dim TdAsFound As HtmlTableCell = e.Item.FindControl("TdAsFound")
        Dim TdAfterclean As HtmlTableCell = e.Item.FindControl("TdAfterclean")
        Dim TdNDE As HtmlTableCell = e.Item.FindControl("TdNDE")
        Dim TdRepair As HtmlTableCell = e.Item.FindControl("TdRepair")
        Dim TdAfterRepair As HtmlTableCell = e.Item.FindControl("TdAfterRepair")
        Dim TdFinal As HtmlTableCell = e.Item.FindControl("TdFinal")

        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagNo.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")
        lblTagNo.Attributes("DETAIL_ID") = e.Item.DataItem("DETAIL_ID")

        lblTagName.Text = e.Item.DataItem("TAG_Name")
        lblTagName.Attributes("TAG_TYPE_Name") = e.Item.DataItem("TAG_TYPE_Name")
        lblTagName.Attributes("TAG_TYPE_ID") = e.Item.DataItem("TAG_TYPE_ID")
        lblTagName.Attributes("To_Table") = e.Item.DataItem("To_Table")

        '------Click ในช่อง Table
        TdAsFound.Attributes.Add("onclick", "return StepClick('" & lnkAsFound.ClientID & "');")
        TdAsFound.Attributes("TdAsFound") = e.Item.DataItem("TAG_ID")

        TdAfterclean.Attributes.Add("onclick", "return StepClick('" & lnkAfterclean.ClientID & "');")
        TdAfterclean.Attributes("TdAfterclean") = e.Item.DataItem("TAG_ID")

        TdNDE.Attributes.Add("onclick", "return StepClick('" & lnkNDE.ClientID & "');")
        TdNDE.Attributes("TdNDE") = e.Item.DataItem("TAG_ID")

        TdRepair.Attributes.Add("onclick", "return StepClick('" & lnkRepair.ClientID & "');")
        TdRepair.Attributes("TdRepair") = e.Item.DataItem("TAG_ID")

        TdAfterRepair.Attributes.Add("onclick", "return StepClick('" & lnkAfterRepair.ClientID & "');")
        TdAfterRepair.Attributes("TdAfterRepair") = e.Item.DataItem("TAG_ID")

        TdFinal.Attributes.Add("onclick", "return StepClick('" & lnkFinal.ClientID & "');")
        TdFinal.Attributes("TdFinal") = e.Item.DataItem("TAG_ID")

        lnkAsFound.CommandArgument = e.Item.DataItem("TAG_ID")
        lnkAfterclean.CommandArgument = e.Item.DataItem("TAG_ID")
        lnkNDE.CommandArgument = e.Item.DataItem("TAG_ID")
        lnkRepair.CommandArgument = e.Item.DataItem("TAG_ID")
        lnkAfterRepair.CommandArgument = e.Item.DataItem("TAG_ID")
        lnkFinal.CommandArgument = e.Item.DataItem("TAG_ID")

        '------schedule Color---------

        'หาเวลาน้อยสุดที่เริ่มของ step as found

        'หาเวลามากสุดที่เริ่มของ step as found

        'ตรวจสอบสถานะ  ถ้า สถานะ 


        Dim To_Table = ""
        For i As Integer = 1 To 6

            Dim lblStatus As Label = e.Item.FindControl("lblStatus_" & i)
            Dim TD As HtmlTableCell = Nothing

            Select Case i
                Case 1
                    To_Table = "RPT_Filter_As_Found"
                    TD = e.Item.FindControl("TdAsFound")
                Case 2
                    To_Table = "RPT_Filter_After_Clean"
                    TD = e.Item.FindControl("TdAfterclean")
                Case 3
                    To_Table = "RPT_Filter_NDE"
                    TD = e.Item.FindControl("TdNDE")
                Case 4
                    To_Table = "RPT_Filter_Repair"
                    TD = e.Item.FindControl("TdRepair")
                Case 5
                    To_Table = "RPT_Filter_After_Repair"
                    TD = e.Item.FindControl("TdAfterRepair")
                Case 6
                    To_Table = "RPT_Filter_Final"
                    TD = e.Item.FindControl("TdFinal")
            End Select
            Dim SQL As String = " " & vbNewLine
            SQL &= "    Select  MIN(RPT_Period_Start) MIN_RPT_Period_Start,MAX(RPT_Period_End) MAX_RPT_Period_End,MIN(RPT_Status) RPT_Status " & vbNewLine
            SQL &= "    From " & To_Table & "  " & vbNewLine
            SQL &= "    Where DETAIL_ID =" & e.Item.DataItem("DETAIL_ID") & " AND TAG_ID=" & e.Item.DataItem("TAG_ID") & "" & vbNewLine
            SQL &= "    Group By RPT_Status  " & vbNewLine
            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If (DT.Rows.Count > 0) Then
                If Not IsDBNull(DT.Rows(0)("MIN_RPT_Period_Start")) Then
                    lblStatus.Text = BL.ReportGridTime(DT.Rows(0)("MIN_RPT_Period_Start"))
                End If


                If Not IsDBNull(DT.Rows(0)("RPT_Status")) Then
                    If DT.Rows(0)("RPT_Status") = EIR_BL.ST_TA_STATUS.Finish Then
                        TD.Style("background-color") = "green"
                        lblStatus.Style("color") = "white"
                        If Not IsDBNull(DT.Rows(0)("MAX_RPT_Period_End")) Then
                            lblStatus.Text &= " - " & BL.ReportGridTime(DT.Rows(0)("MAX_RPT_Period_End"))
                        Else
                            lblStatus.Text &= " - ???"
                        End If

                    ElseIf DT.Rows(0)("RPT_Status") = EIR_BL.ST_TA_STATUS.In_Progress Then
                        TD.Style("background-color") = "Gray"
                        lblStatus.Style("color") = "white"
                        lblStatus.Text &= " - ???"
                    End If

                End If
            End If

        Next




    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand


        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub


        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")

        Dim lnkAsFound As LinkButton = e.Item.FindControl("lnkAsFound")
        Dim lnkAfterclean As LinkButton = e.Item.FindControl("lnkAfterclean")
        Dim lnkNDE As LinkButton = e.Item.FindControl("lnkNDE")
        Dim lnkRepair As LinkButton = e.Item.FindControl("lnkRepair")
        Dim lnkAfterRepair As LinkButton = e.Item.FindControl("lnkAfterRepair")
        Dim lnkFinal As LinkButton = e.Item.FindControl("lnkFinal")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim td3 As HtmlTableCell = e.Item.FindControl("Td3")

        Dim TAG_ID As Integer = lblTagNo.Attributes("TAG_ID")


        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & lblTagName.Attributes("TAG_TYPE_ID") & "&DETAIL_ID=" & lblTagNo.Attributes("DETAIL_ID")

        Select Case e.CommandName
            Case "AddAsFound"
                Response.Redirect("ST_TA_Filter_AsFound.aspx?" & Param & "", True)
            Case "AddAfterClean"
                Response.Redirect("ST_TA_Filter_AfterClean.aspx?" & Param & "", True)
            Case "AddNDE"
                Response.Redirect("ST_TA_Filter_NDE.aspx?" & Param & "", True)
            Case "AddRepair"
                Response.Redirect("ST_TA_Filter_Repair.aspx?" & Param & "", True)
            Case "AddAfterRepair"
                Response.Redirect("ST_TA_Filter_AfterRepair.aspx?" & Param & "", True)
            Case "AddFinal"
                Response.Redirect("ST_TA_Filter_Final.aspx?" & Param & "", True)

        End Select

    End Sub

    Private Sub btn_Back_Click(sender As Object, e As EventArgs) Handles btn_Back.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        Response.Redirect("ST_TA_Inspection_Edit1.aspx?" & Param)
    End Sub

    Private Sub HTabHeader_Click(sender As Object, e As EventArgs) Handles HTabHeader.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        Response.Redirect("ST_TA_Inspection_Edit1.aspx?" & Param)
    End Sub

End Class


