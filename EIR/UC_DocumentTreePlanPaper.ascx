﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_DocumentTreePlanPaper.ascx.vb" Inherits="EIR.UC_DocumentTreePlanPaper" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Label ID="lblDocumentPlanID" runat="server" Visible="false" ></asp:Label>
<asp:Repeater ID="rptShowProcess" runat="server" >
    <ItemTemplate>
        <table cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="background-color:White;">
            <tr>
                <td>
                    <p>
                        <b><asp:Label ID="lblPaperName" runat="server"></asp:Label></b>
                        &nbsp;&nbsp;&nbsp;<asp:Label id="lblPlanStatus" runat="server"></asp:Label>
                    </p>
                    <table width="300px">
                        <tr>
                            <td style="width:100px;font-weight:bold;">Complete Date</td>
                            <td style="width:100px;font-weight:bold;">Notice Date</td>
                            <td style="width:100px;font-weight:bold;">Critical Date</td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblUploadDate" runat="server"></asp:Label></td>
                            <td><asp:Label ID="lblNoticeDate" runat="server"></asp:Label></td>
                            <td><asp:Label ID="lblCriticalDate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblFileIconList" runat="server" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:FileUpload ID="ful" runat="server"  style="display:none;"  ></asp:FileUpload>
                    <asp:TextBox ID="txtFileName" runat="server" Enabled="false" Width="300px"></asp:TextBox>
                    <asp:Button ID="btnBrowse" runat="server" Text="Browse"  />
                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" OnClick="btnUpload_Click" />
                    <br />
                    <asp:Label ID="lblDocumentPlanPaperID" runat="server" Visible="false" Text="0" ></asp:Label>
                    <asp:TextBox ID="txtTempFilePath" runat="server" style="display:none;" ></asp:TextBox>
                </td>
            </tr>
            
        </table>
    </ItemTemplate>                                                                                                                                            
</asp:Repeater>


<script type="text/javascript">
    function UploadFile(fulClientID) {
        $('#' + fulClientID).click();
    }

    function asyncUploadFile(txtFileName, txtTempFilePath, fulClientID) {
        var ful = $("#" + fulClientID);
        var filename = ful.val();
        //alert(txtFileName + "#### filename=" + filename);

        if (filename.trim() == "" || filename.trim() == "undefined")
            return;

        var filecontent = ful.prop("files")[0];
        if (filecontent.size > (12 * 1024 * 1024)) {
            //alert("ไฟล์ที่อัพโหลดต้องไม่เกิน 12 Mb");
            alert("File size must not larger than 12MB");
            return false;
        }
        //if (!inFileNameValid(filename)) return;

        var formData = new FormData();
        formData.append("filecontent", filecontent);

        $.ajax({
            url: "RenderDocumentPlanPaperUploadFile.aspx",
            type: 'POST',
            data: formData,
            success: function (data) {
                var fle = data.split("#");
                $("#" + txtFileName).val(fle[0]);
                $("#" + txtTempFilePath).val(fle[1]);
            },
               
            cache: false,
            contentType: false,
            processData: false
        });

    }
</script>