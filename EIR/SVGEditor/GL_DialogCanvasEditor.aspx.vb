﻿Imports System.IO

Public Class GL_DialogCanvasEditor
    Inherits System.Web.UI.Page

    Private ReadOnly Property UNIQUE_POPUP_ID() As String
        Get
            If Not IsNothing(Request.QueryString("UNIQUE_POPUP_ID")) Then
                Return Request.QueryString("UNIQUE_POPUP_ID")
            Else
                Return ""
            End If
        End Get
    End Property

    'Private ReadOnly Property DisplayTitle As Boolean
    '    Get
    '        Try
    '            Return CBool(Request.QueryString("DisplayTitle"))
    '        Catch ex As Exception
    '            Return True
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property DisplayType As Boolean
    '    Get
    '        Try
    '            Return CBool(Request.QueryString("DisplayType"))
    '        Catch ex As Exception
    '            Return True
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property DisplayDesc As Boolean
    '    Get
    '        Try
    '            Return CBool(Request.QueryString("DisplayDesc"))
    '        Catch ex As Exception
    '            Return True
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property DisplayTag As Boolean
    '    Get
    '        Try
    '            Return CBool(Request.QueryString("DisplayTag"))
    '        Catch ex As Exception
    '            Return True
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property _ReadOnly As Boolean
    '    Get
    '        Try
    '            Return CBool(Request.QueryString("_ReadOnly"))
    '        Catch ex As Exception
    '            Return False
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property CancelButton As String
    '    Get
    '        Try
    '            Return Request.QueryString("CancelButton")
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property OKButton As String
    '    Get
    '        Try
    '            Return Request.QueryString("OKButton")
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property InputTextbox As String
    '    Get
    '        Try
    '            Return Request.QueryString("InputTextbox")
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End Get
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If UNIQUE_POPUP_ID = "" OrElse IsNothing(Session(UNIQUE_POPUP_ID)) Then
            Alert(Me.Page, "Invalid Information")
            CloseTopWindow(Page)
            Exit Sub
        End If

        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()

        Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)

        '------------- Write Temp SVG File--------------------
        Dim BL As New EIR_BL
        Dim Path As String = BL.ServerMapPath & "\Temp\" & UNIQUE_POPUP_ID & ".svg"
        Try : If File.Exists(Path) Then DeleteFile(Path) Catch : End Try
        If Obj.Content.Count > 100 Then
            Dim ST As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            ST.Write(Obj.Content, 0, Obj.Content.Count)
            ST.Close()
            ST.Dispose()
            '------------- Send Javascript To Open SVG--------------
            LoadSVGFromPath("../Temp/" & UNIQUE_POPUP_ID & ".svg")
        End If

        '------------- Bind Form File Info ------------
        Dim URL As String = "AttachmentForm.aspx?"
        For i As Integer = 0 To Request.QueryString.Count - 1
            URL &= Request.QueryString.Keys(i) & "=" & Request.QueryString.GetValues(i)(0) & "&"
        Next
        pnlInfo.Attributes("src") = URL

    End Sub

    Private Sub LoadSVGFromPath(ByVal Path As String)
        Dim Script As String = ""
        Script &= "$(document).ready(function(){" & vbNewLine
        Script &= "                              LoadSVGFromFile('" & Path & "','" & UNIQUE_POPUP_ID & "');" & vbNewLine
        Script &= "                             }" & vbNewLine
        Script &= ");"
        ScriptManager.RegisterStartupScript(Me, GetType(String), "SVGLoad", Script, True)
    End Sub
End Class