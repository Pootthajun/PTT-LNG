﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_Improvement_Report
    Inherits System.Web.UI.UserControl

    Dim Dashboard As New DashboardClass

    Public Sub BindData(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer, Optional ByVal PrintPage As Boolean = False)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        lblMONTH_F.Text = Month_F
        lblMONTH_T.Text = Month_T
        lblYEAR_F.Text = Year_F
        lblYEAR_T.Text = Year_T
        lblEQUIPMENT.Text = Equipment
        lblPrintPage.Text = PrintPage

        DT_Dashboard = Dashboard.Improvement(Month_F, Month_T, Year_F, Year_T, Equipment)
        Session("Dashboard_Improvement_Report") = DT_Dashboard

        If DT_Dashboard.Rows.Count > 6 Then
            Dim NewWidth As Unit = Unit.Pixel(77 * DT_Dashboard.Rows.Count)
            ChartMain.Width = NewWidth
        End If

        DisplayChart(ChartMain, New EventArgs, Month_F, Month_T, Year_F, Year_T, Equipment, PrintPage)

        rptData.DataSource = DT_Dashboard
        rptData.DataBind()

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer, ByVal PrintPage As Boolean)
        Dim DisplayText As String = ""
        If Year_F < 2500 Then
            Year_F = Year_F + 543
        End If
        If Year_T < 2500 Then
            Year_T = Year_T + 543
        End If

        If Year_F = Year_T Then
            If Month_F = Month_T Then
                DisplayText = "on  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F
            Else
                DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_F
            End If
        Else
            DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_T
        End If

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)
        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain.Titles("Title1").Text = "Problem  improved  Completely  for  " & Dashboard.FindEquipmentName(Equipment) & "  Equipement" & vbNewLine & vbNewLine & DisplayText
        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter

        ChartMain.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain.ChartAreas("ChartArea1").AxisY.Title = "Problem(s)"
        ChartMain.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver

        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Column
        Dim DT As DataTable = Session("Dashboard_Improvement_Report")
        For i As Integer = 0 To DT.Rows.Count - 1

            ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("PLANT_Name"), DT.Rows(i).Item("Improvement"))
            'ถ้าเป็นหน้า Print ก็ไม่ต้องใส่ Link
            If PrintPage = False Then
                Dim Url As String = "Dashboard_Improvement_Report_Plant.aspx?PLANT_ID=" & DT.Rows(i).Item("PLANT_ID") & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Equipment
                ChartMain.Series("Series1").Points(i).Url = Url
            End If
            '--------------------------
            Dim Tooltip As String = DT.Rows(i).Item("Improvement").ToString & " issues improved"
            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip
        Next

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblImprovement As Label = e.Item.FindControl("lblImprovement")

        Dim Improvement As Integer = 0

        lblPlant.Text = e.Item.DataItem("PLANT_NAME")

        If Not IsDBNull(e.Item.DataItem("Improvement")) AndAlso e.Item.DataItem("Improvement") <> 0 Then
            Improvement = e.Item.DataItem("Improvement")
            lblImprovement.Text = FormatNumber(Improvement, 0)
        Else
            lblImprovement.Text = "-"
        End If

        If CBool(lblPrintPage.Text) = False Then
            tbTag.Attributes("onclick") = "window.location.href='Dashboard_Improvement_Report_Plant.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "&MONTH_F=" & lblMONTH_F.Text & "&MONTH_T=" & lblMONTH_T.Text & "&YEAR_F=" & lblYEAR_F.Text & "&YEAR_T=" & lblYEAR_T.Text & "&EQUIPMENT=" & lblEQUIPMENT.Text & "';"
        End If

    End Sub

End Class