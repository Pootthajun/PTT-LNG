﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_Improvement_Sheet
    Inherits System.Web.UI.UserControl

    Dim Dashboard As New DashboardClass

    Public Sub BindData(ByVal Year As Integer, ByVal Equipment As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard_Y As New DataTable
        Dim DT_Dashboard_M As New DataTable
        Dim DT As New DataTable
        Dim DR As DataRow
        Dim DA As New SqlDataAdapter

        If Year > 2500 Then
            Year = Year - 543
        End If

        '************* DT Year *************
        DT_Dashboard_Y.Columns.Add("Year")
        DT_Dashboard_Y.Columns.Add("Improvement")

        For i As Integer = 1 To 4
            DR = DT_Dashboard_Y.NewRow
            DR("Year") = CStr(Year - i)
            DR("Improvement") = 0
            DT_Dashboard_Y.Rows.Add(DR)
        Next
        DT_Dashboard_Y.DefaultView.Sort = "Year"
        DT_Dashboard_Y = DT_Dashboard_Y.DefaultView.ToTable

        DT = Dashboard.ImprovementSheetYear(Year, Equipment)

        For i As Integer = 0 To DT_Dashboard_Y.Rows.Count - 1
            DT.DefaultView.RowFilter = "YY = '" & DT_Dashboard_Y.Rows(i).Item("Year").ToString & "'"
            If DT.DefaultView.Count > 0 Then
                Dim DT_Temp As New DataTable
                DT_Temp = DT.DefaultView.ToTable
                DT_Dashboard_Y.Rows(i).Item("Improvement") = DT_Temp.Rows(0).Item("Improvement").ToString
            End If
            DT.DefaultView.RowFilter = ""

            If IsNumeric(DT_Dashboard_Y.Rows(i).Item("Year")) Then
                Dim YY As Integer = DT_Dashboard_Y.Rows(i).Item("Year")
                If YY < 2500 Then
                    YY = YY + 543
                End If
                DT_Dashboard_Y.Rows(i).Item("Year") = YY
            End If
        Next


        Session("Dashboard_Improvement_Sheet_Y") = DT_Dashboard_Y
        '***********************************
        '************* DT Month *************
        DT_Dashboard_M.Columns.Add("M")
        DT_Dashboard_M.Columns.Add("Month")
        DT_Dashboard_M.Columns.Add("Improvement")
        DT_Dashboard_M.Columns.Add("Improvement_Total")

        For i As Integer = 1 To 12
            Dim C As New Converter
            DR = DT_Dashboard_M.NewRow
            DR("M") = i
            DR("Month") = C.ToMonthNameEN(i).Substring(0, 3)
            DR("Improvement") = 0
            DR("Improvement_Total") = 0
            DT_Dashboard_M.Rows.Add(DR)
        Next

        DT = New DataTable
        DT = Dashboard.ImprovementSheetMonth(Year, Equipment)

        If DT.Rows.Count > 0 Then
            Dim Improvement_Total As Integer = 0
            For i As Integer = 0 To DT_Dashboard_M.Rows.Count - 1
                If Year = Date.Now.Year And DT_Dashboard_M.Rows(i).Item("M") > Date.Now.Month Then
                    DT_Dashboard_M.Rows(i).Item("Improvement") = DBNull.Value
                    DT_Dashboard_M.Rows(i).Item("Improvement_Total") = DBNull.Value
                Else
                    DT.DefaultView.RowFilter = "MM = '" & DT_Dashboard_M.Rows(i).Item("Month").ToString & "'"
                    If DT.DefaultView.Count > 0 Then
                        Dim DT_Temp As New DataTable
                        DT_Temp = DT.DefaultView.ToTable
                        Improvement_Total = Improvement_Total + DT_Temp.Rows(0).Item("Improvement")
                        DT_Dashboard_M.Rows(i).Item("Improvement") = DT_Temp.Rows(0).Item("Improvement")
                    End If
                    DT_Dashboard_M.Rows(i).Item("Improvement_Total") = Improvement_Total
                    DT.DefaultView.RowFilter = ""
                End If
            Next
        End If

        Session("Dashboard_Improvement_Sheet_M") = DT_Dashboard_M
        '***********************************

        DisplayChart(ChartMain_Y, New EventArgs, Year, Equipment)

        rptData_Y.DataSource = DT_Dashboard_Y
        rptData_Y.DataBind()

        rptData_M.DataSource = DT_Dashboard_M
        rptData_M.DataBind()

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Year As Integer, ByVal Equipment As Integer)
        Dim DisplayText As String = ""

        If Year < 2500 Then
            Year = Year + 543
        End If

        DisplayText = "Year " & Year & "  ( " & Dashboard.FindEquipmentName(Equipment) & " )"

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)
        ChartMain_Y.ChartAreas("ChartArea1").AxisX.TitleFont = F
        ChartMain_Y.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain_Y.ChartAreas("ChartArea1").AxisX.TitleForeColor = Drawing.Color.Navy
        ChartMain_Y.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain_Y.ChartAreas("ChartArea1").AxisX.Title = "Year " & CStr(Year - 4) & " - " & CStr(Year - 1) & vbNewLine & "( " & Dashboard.FindEquipmentName(Equipment) & " )"
        lbl_Y.Text = "Year " & CStr(Year - 4) & " - " & CStr(Year - 1)
        ChartMain_Y.ChartAreas("ChartArea1").AxisY.Title = " Problem(s)"
        ChartMain_Y.Series("Series1").Points.Clear()
        ChartMain_Y.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Column
        ChartMain_Y.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain_Y.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver
        Dim DT_Y As DataTable = Session("Dashboard_Improvement_Sheet_Y")
        For i As Integer = 0 To DT_Y.Rows.Count - 1
            ChartMain_Y.Series("Series1").Points.AddXY(DT_Y.Rows(i).Item("Year"), DT_Y.Rows(i).Item("Improvement"))
            Dim Tooltip As String = DT_Y.Rows(i).Item("Improvement").ToString & " issues improved"
            ChartMain_Y.Series("Series1").Points(i).ToolTip = Tooltip
        Next

        ChartMain_M.ChartAreas("ChartArea1").AxisX.TitleFont = F
        ChartMain_M.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain_M.ChartAreas("ChartArea1").AxisX.TitleForeColor = Drawing.Color.Navy
        ChartMain_M.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain_M.ChartAreas("ChartArea1").AxisX.Title = "Year " & Year & "  ( " & Dashboard.FindEquipmentName(Equipment) & " )" & vbNewLine & " "
        lbl_M.Text = "Year " & Year
        ChartMain_M.Series("Series1").Points.Clear()
        ChartMain_M.Series("Series2").Points.Clear()
        ChartMain_M.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Column
        ChartMain_M.Series("Series2").ChartType = DataVisualization.Charting.SeriesChartType.Line
        ChartMain_M.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain_M.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver

        ChartMain_M.Titles("Title1").Font = F
        ChartMain_M.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain_M.Titles("Title1").Text = "      Problem improved Completely"
        ChartMain_M.Titles("Title1").Alignment = Drawing.ContentAlignment.TopLeft
        ChartMain_Y.Titles("Title1").Font = F
        ChartMain_Y.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain_Y.Titles("Title1").Text = " "
        ChartMain_Y.Titles("Title1").Alignment = Drawing.ContentAlignment.TopLeft

        Dim DT_M As DataTable = Session("Dashboard_Improvement_Sheet_M")
        Dim MaxVal As Integer = 0

        For i As Integer = 0 To DT_M.Rows.Count - 1
            ChartMain_M.Series("Series1").Points.AddXY(DT_M.Rows(i).Item("Month"), DT_M.Rows(i).Item("Improvement"))
            ChartMain_M.Series("Series2").Points.AddXY(DT_M.Rows(i).Item("Month"), DT_M.Rows(i).Item("Improvement_Total"))

            If Not IsDBNull(DT_M.Rows(i).Item("Improvement_Total")) Then
                MaxVal = DT_M.Rows(i).Item("Improvement_Total")
                Dim Tooltip As String = DT_M.Rows(i).Item("Improvement").ToString & " issues improved"
                Dim lbl As New Label
                lbl.Text = DT_M.Rows(i).Item("Improvement_Total")
                ChartMain_M.Series("Series1").Points(i).ToolTip = Tooltip
                ChartMain_M.Series("Series2").Points(i).Label = DT_M.Rows(i).Item("Improvement_Total")
                ChartMain_M.Series("Series2").Points(i).LabelBackColor = Drawing.Color.Green
                ChartMain_M.Series("Series2").Points(i).LabelForeColor = Drawing.Color.White
            End If
        Next

        Dim Y_MaxVal As Integer = 0
        Y_MaxVal = Dashboard.FindMaxValue(MaxVal)

    End Sub

    Protected Sub rptData_Y_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_Y.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblTitle As Label = e.Item.FindControl("lblTitle")
        Dim lblImprovement As Label = e.Item.FindControl("lblImprovement")

        Dim Improvement As Integer = 0

        lblTitle.Text = e.Item.DataItem("Year")

        If Not IsDBNull(e.Item.DataItem("Improvement")) AndAlso e.Item.DataItem("Improvement") <> 0 Then
            Improvement = e.Item.DataItem("Improvement")
            lblImprovement.Text = FormatNumber(Improvement, 0)
        Else
            lblImprovement.Text = "-"
        End If

    End Sub

    Protected Sub rptData_M_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_M.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblTitle As Label = e.Item.FindControl("lblTitle")
        Dim lblImprovement As Label = e.Item.FindControl("lblImprovement")

        Dim Improvement As Integer = 0

        lblTitle.Text = e.Item.DataItem("Month")

        If Not IsDBNull(e.Item.DataItem("Improvement")) AndAlso e.Item.DataItem("Improvement") <> 0 Then
            Improvement = e.Item.DataItem("Improvement")
            lblImprovement.Text = FormatNumber(Improvement, 0)
        Else
            lblImprovement.Text = "-"
        End If

    End Sub

End Class