﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class Master_Utility
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If
    End Sub

    Protected Sub btnDeleteTemp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteTemp.Click

        Dim TempPath As String = Server.MapPath("Temp")
        Dim F As String() = Directory.GetFiles(TempPath)
        Dim File_Deleted As Integer = 0
        Dim File_Locked As Integer = 0
        For i As Integer = 0 To F.Length - 1
            Try
                Kill(F(i))
                File_Deleted += 1
            Catch ex As Exception
                File_Locked += 1
            End Try
        Next

        Dim D As String() = Directory.GetDirectories(TempPath)
        For i As Integer = 0 To D.Length - 1
            Try
                Directory.Delete(D(i))
                File_Deleted += 1
            Catch ex As Exception
                File_Locked += 1
            End Try
        Next

        Dim Result As String = ""

        If File_Deleted > 0 Then
            Result = FormatNumber(File_Deleted, 0) & " item(s) cleaned\n"
        End If
        If File_Locked > 0 Then
            Result = FormatNumber(File_Locked, 0) & " item(s) cannot be cleaned\n"
        End If
        If File_Locked = 0 And File_Deleted = 0 Then
            Result = "It's already emptied"
        End If
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Result & "');", True)

    End Sub

    Protected Sub btnBackupDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackupDB.Click

        Try
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            Dim FileName As String = "PTT_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".rar"
            Dim SQL As String = "BACKUP DATABASE [" & Conn.Database & "] TO  DISK = N'" & Server.MapPath("") & "\Temp\" & FileName & "' WITH NOFORMAT, INIT,  NAME = N'PTT-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10"
            Dim Comm As New SqlCommand
            With Comm
                .CommandType = CommandType.Text
                .Connection = Conn
                .CommandText = SQL
                .ExecuteNonQuery()
                .Dispose()
                Conn.Close()

                Session("FilePathToDownload") = Server.MapPath("") & "\Temp\" & FileName
                Session("FileTypeToDownload") = "application/octet-stream"

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='GL_DownloadFile.aspx';", True)
            End With
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to backup\n" & ex.Message & "');", True)
        End Try

    End Sub

    Protected Sub btnShinkDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShinkDB.Click
        Try
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()

            Dim SQL As String = "DBCC SHRINKDATABASE(N'" & Conn.Database & "')"
            Dim Comm As New SqlCommand
            With Comm
                .CommandType = CommandType.Text
                .Connection = Conn
                .CommandText = SQL
                .ExecuteNonQuery()
                .Dispose()
                Conn.Close()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Compact database successfully!!');", True)
            End With
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to compact database\n" & ex.Message & "');", True)
        End Try
    End Sub

    Protected Sub btnClearUnusdMaster_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearUnusdMaster.Click

        Dim DT As DataTable
        Dim DA As SqlDataAdapter
        Dim cmd As SqlCommandBuilder

        Dim Command() As String = {
        "SELECT * FROM MS_TAG WHERE Active_Status=0 AND dbo.UDF_IsTagUnused(TAG_ID)=1",
        "SELECT * FROM MS_TAG_TYPE WHERE Active_Status=0 AND dbo.UDF_IsTagTypeUnused(TAG_TYPE_ID)=1",
        "SELECT * FROM MS_Process WHERE Active_Status=0 AND dbo.UDF_IsProcessUnused(PROC_ID)=1",
        "SELECT * FROM MS_Area WHERE Active_Status=0 AND dbo.UDF_IsAreaUnused(AREA_ID)=1",
        "SELECT * FROM MS_ROUTE WHERE Active_Status=0 AND dbo.UDF_IsRouteUnused(ROUTE_ID)=1",
        "SELECT * FROM MS_PLANT WHERE Active_Status=0 AND dbo.UDF_IsPlantUnused(PLANT_ID)=1",
        "SELECT * FROM MS_User_Coverage WHERE ISNULL((SELECT TOP 1 Active_Status FROM MS_User WHERE MS_User.USER_ID=MS_User_Coverage.USER_ID),0)=0 AND dbo.UDF_IsUserUnused(USER_ID)=1",
        "SELECT * FROM MS_User WHERE Active_Status=0 AND dbo.UDF_IsUserUnused(USER_ID)=1"
        }
        Dim TotalRemove As Integer = 0

        '--------------- Delete Tag-------------
        DT = New DataTable
        DA = New SqlDataAdapter(Command(0), BL.ConnStr)
        DA.Fill(DT)
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        If DT.Rows.Count > 0 Then
            Dim Com As New SqlCommand
            With Com
                .Connection = Conn
                .CommandType = CommandType.Text
                For t As Integer = 0 To DT.Rows.Count - 1
                    .CommandText = "DELETE FROM RPT_Rotating_Routine_Import WHERE TAG_ID=" & DT.Rows(t).Item("TAG_ID")
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM RPT_Picture WHERE DETAIL_ID IN (SELECT DETAIL_ID FROM RPT_DETAIL WHERE TAG_ID=" & DT.Rows(t).Item("TAG_ID") & ")"
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM RPT_Detail WHERE TAG_ID=" & DT.Rows(t).Item("TAG_ID")
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM MS_TAG WHERE TAG_ID=" & DT.Rows(t).Item("TAG_ID")
                    .ExecuteNonQuery()
                    TotalRemove += 1
                Next
                .Dispose()
            End With
        End If

        '--------------- Delete Other Data------
        For i As Integer = 1 To Command.Count - 1
            DT = New DataTable
            DA = New SqlDataAdapter(Command(i), Conn)
            DA.Fill(DT)
            For j As Integer = DT.Rows.Count - 1 To 0 Step -1
                DT.Rows(j).Delete()
                TotalRemove += 1
            Next
            cmd = New SqlCommandBuilder(DA)
            DA.Update(DT)
        Next

        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Clean " & FormatNumber(TotalRemove, 0) & " item(s) unused master data successfully!!');", True)

    End Sub

End Class