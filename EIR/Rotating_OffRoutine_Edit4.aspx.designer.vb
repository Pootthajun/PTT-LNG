﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Rotating_OffRoutine_Edit4
    
    '''<summary>
    '''udp1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents udp1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''txt_Buffer_RPT_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Buffer_RPT_Year As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_Buffer_RPT_No control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Buffer_RPT_No As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btn_Buffer_Refresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_Buffer_Refresh As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''lblReportCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReportCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''HTabHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabHeader As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabDetail As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabPhoto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabPhoto As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabVibration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabVibration As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabDocument control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabDocument As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabSummary control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabSummary As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lbl_Plant control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Plant As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl_Route control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Route As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Year As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl_TAG control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_TAG As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lnkUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkUpload As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkPaste control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkPaste As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkClear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkClear As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkClear_ConfirmButtonExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkClear_ConfirmButtonExtender As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''lnkRefresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkRefresh As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkPreview control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkPreview As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''imgPreview control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgPreview As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnSaveImage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveImage As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btn_Back control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_Back As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btn_Next control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_Next As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlValidation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlValidation As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnValidationClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnValidationClose As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblValidation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblValidation As Global.System.Web.UI.WebControls.Label
End Class
