﻿
Imports System.Data.SqlClient
Public Class Stationary_Routine_Edit1
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Stationary_Routine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Stationary_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_ST_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_ST_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_ST_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = BL.GetReportCode(RPT_Year, RPT_No)
            BindTabData()

        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_Routine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Stationary_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_Routine_Summary.aspx'", True)
            Exit Sub
        End If
    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Stationary_Routine_Summary.aspx")
            Exit Sub
        End If


        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If
        '------------------------------Header -----------------------------------

        If Not IsDBNull(DT.Rows(0).Item("RPT_Subject")) Then
            txt_RPT_Subject.Text = DT.Rows(0).Item("RPT_Subject")
        Else
            txt_RPT_Subject.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_To")) Then
            txt_RPT_To.Text = DT.Rows(0).Item("RPT_To")
        Else
            txt_RPT_To.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_Cause")) Then
            txt_RPT_Cause.Text = DT.Rows(0).Item("RPT_Cause")
        Else
            txt_RPT_Cause.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("Created_Time")) Then
            txt_Created_Time.Text = BL.ReportGridTime(DT.Rows(0).Item("Created_Time"))
        Else
            txt_Created_Time.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("Creator_Name")) Then
            txt_Created_By.Text = DT.Rows(0).Item("Creator_Name")
        Else
            txt_Created_By.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
            txt_RPT_STEP.Text = BL.Get_User_Level_Name(DT.Rows(0).Item("RPT_STEP"))
            txt_RPT_STEP.ForeColor = BL.Get_Report_Step_Color(DT.Rows(0).Item("RPT_STEP"))
        Else
            txt_RPT_STEP.ForeColor = Drawing.Color.Black
        End If

        '------------------ Officer ------------------
        If Not IsDBNull(DT.Rows(0).Item("Officer_Collector")) Then
            BL.BindCmbReportOfficer(cmbCollector, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Collector, DT.Rows(0).Item("Officer_Collector"))
        Else
            BL.BindCmbReportOfficer(cmbCollector, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Collector)
        End If

        If Not IsDBNull(DT.Rows(0).Item("Officer_Inspector")) Then
            BL.BindCmbReportOfficer(cmbInspector, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Inspector"))
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
            BL.BindCmbReportOfficer(cmbInspector, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Engineer"))
        Else
            BL.BindCmbReportOfficer(cmbInspector, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Inspector)
        End If

        If Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
            BL.BindCmbReportOfficer(cmbEngineer, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Engineer, DT.Rows(0).Item("Officer_Engineer"))
        Else
            BL.BindCmbReportOfficer(cmbEngineer, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Engineer)
        End If

        If Not IsDBNull(DT.Rows(0).Item("Officer_Analyst")) Then
            BL.BindCmbReportOfficer(cmbAnalyst, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Approver, DT.Rows(0).Item("Officer_Analyst"))
        Else
            BL.BindCmbReportOfficer(cmbAnalyst, RPT_Year, RPT_No, DT.Rows(0).Item("PLANT_ID"), RPT_Type_ID, EIR_BL.User_Level.Approver)
        End If

    End Sub

#Region "Navigator"

    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Next.Click
        Save_Header(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_Routine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        Save_Header(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_Routine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        Save_Header(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_Routine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

#End Region

#Region "Saving"
    Private Sub Save_Header(Optional ByVal ReportSuccess As Boolean = True)

        Dim DT As New DataTable
        Dim SQL As String = "SELECT RPT_Year,RPT_No,RPT_Subject,RPT_To,RPT_Cause,Update_By,Update_Time,Officer_Collector,Officer_Inspector,Officer_Engineer,Officer_Analyst FROM RPT_ST_Header "
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save." & vbNewLine & "This report has been removed!'); window.location.href='Stationary_Routine_Summary.aspx';", True)
            Exit Sub
        End If

        Dim NeedSave As Boolean = False
        If Not IsDBNull(DT.Rows(0).Item("RPT_Subject")) AndAlso DT.Rows(0).Item("RPT_Subject") <> txt_RPT_Subject.Text Then
            NeedSave = True
            DT.Rows(0).Item("RPT_Subject") = txt_RPT_Subject.Text
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_To")) AndAlso DT.Rows(0).Item("RPT_To") <> txt_RPT_To.Text Then
            NeedSave = True
            DT.Rows(0).Item("RPT_To") = txt_RPT_To.Text
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_Cause")) AndAlso DT.Rows(0).Item("RPT_Cause") <> txt_RPT_Cause.Text Then
            NeedSave = True
            DT.Rows(0).Item("RPT_Cause") = txt_RPT_Cause.Text
        End If

        '------------------ Officer ------------------
        If cmbCollector.SelectedIndex > 0 Then
            If IsDBNull(DT.Rows(0).Item("Officer_Collector")) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Collector") = cmbCollector.Text
            ElseIf DT.Rows(0).Item("Officer_Collector") <> cmbCollector.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Collector") = cmbCollector.Text
            End If
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Collector")) AndAlso DT.Rows(0).Item("Officer_Collector") <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Collector") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Collector") = ""
        End If
        '----------------------- Inspector -----------------------
        Dim Inspector As Object = DBNull.Value
        Select Case True
            Case Not IsDBNull(DT.Rows(0).Item("Officer_Engineer"))
                Inspector = DT.Rows(0).Item("Officer_Engineer")
            Case Not IsDBNull(DT.Rows(0).Item("Officer_Inspector"))
                Inspector = DT.Rows(0).Item("Officer_Inspector")
        End Select
        If cmbInspector.SelectedIndex > 0 Then
            If IsDBNull(Inspector) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Inspector") = cmbInspector.Text
            ElseIf Inspector <> cmbInspector.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Inspector") = cmbInspector.Text
            End If
        ElseIf Not IsDBNull(Inspector) AndAlso Inspector <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Inspector") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Inspector") = ""
        End If

        '--------------------- Engineer -----------
        If cmbEngineer.SelectedIndex > 0 Then
            If IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Engineer") = cmbEngineer.Text
            ElseIf DT.Rows(0).Item("Officer_Engineer") <> cmbEngineer.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Engineer") = cmbEngineer.Text
            End If
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) AndAlso DT.Rows(0).Item("Officer_Engineer") <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Engineer") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Engineer") = ""
        End If

        '----------------- Approver -------------
        If cmbAnalyst.SelectedIndex > 0 Then
            If IsDBNull(DT.Rows(0).Item("Officer_Analyst")) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Analyst") = cmbAnalyst.Text
            ElseIf DT.Rows(0).Item("Officer_Analyst") <> cmbAnalyst.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Analyst") = cmbAnalyst.Text
            End If
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Analyst")) AndAlso DT.Rows(0).Item("Officer_Analyst") <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Analyst") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Analyst") = ""
        End If

        If Not NeedSave Then
            If ReportSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('There is nothing to be saved!\nThe report does not changed');", True)
            End If
            Exit Sub
        End If

        DT.Rows(0).Item("Update_By") = Session("USER_ID")
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        If ReportSuccess Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save successfully');", True)
        End If

    End Sub
#End Region

End Class