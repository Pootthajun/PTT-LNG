﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_DialogLawJob
    Inherits System.Web.UI.UserControl

    Dim CL As New LawClass
    Public Event SaveCompleted(ByRef sender As GL_DialogLawJob)

#Region "Dynamic Property"
    Public ReadOnly Property FOLDER_ID As Integer
        Get
            Return lblFolderID.Text
        End Get
    End Property
    Public ReadOnly Property PARENT_ID As Integer
        Get
            Return lblParentID.Text
        End Get
    End Property

    Public ReadOnly Property JOB_NAME As String
        Get
            Return txt_JobName.Text
        End Get
    End Property

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txt_JobName.Text.Trim = "" Then
                lblValidation.Text = "Please insert Job Name"
                pnlValidation.Visible = True
                dialogLawFolder.Show()
                Exit Sub
            End If

            Dim ret As String = CL.SaveFolder(lblFolderID.Text, txt_JobName.Text, txt_Description.Text, lblParentID.Text, Session("USER_ID"))

            Dim tmp() As String = Split(ret, "|")
            If tmp(0).ToLower <> "true" Then

                lblValidation.Text = tmp(1)
                pnlValidation.Visible = True
                dialogLawFolder.Show()
                Exit Sub
            Else
                lblFolderID.Text = tmp(1)
                RaiseEvent SaveCompleted(Me)
                CloseDialog()
            End If

        Catch ex As Exception
            'lblValidation.Text = "Allow only PDF,XLS,TXT,TIF,DOC,JPG,PNG to be uploaded!!"
            'ImgPreview.ImageUrl = Nothing
            'pnlValidation.Visible = True
            'txt_Name.Text = ""
        End Try

    End Sub

    Public Property Disabled() As Boolean
        Get
            Return Not btnSave.Visible
        End Get
        Set(ByVal value As Boolean)
            btnSave.Visible = Not value
            txt_JobName.ReadOnly = value
            txt_Description.ReadOnly = value
            btnSave.Visible = Not value
        End Set
    End Property


#Region "Static Property"
    'Private Property Init_DOC_ID() As Integer
    '    Get
    '        If Not IsNumeric(Me.Attributes("Init_DOC_ID")) Then
    '            Return 0
    '        Else
    '            Return Me.Attributes("Init_DOC_ID")
    '        End If
    '    End Get
    '    Set(ByVal value As Integer)
    '        Me.Attributes("Init_DOC_ID") = value
    '    End Set
    'End Property

    'Public Property UNIQUE_POPUP_ID() As String
    '    Get
    '        Return Me.Attributes("UNIQUE_POPUP_ID")
    '    End Get
    '    Set(ByVal value As String)
    '        Me.Attributes("UNIQUE_POPUP_ID") = value
    '    End Set
    'End Property
#End Region



    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Public Sub CloseDialog()
        Disabled = False
        lblFolderPath.Text = ""
        lblFolderID.Text = 0
        lblParentID.Text = 0
        txt_JobName.Text = ""
        txt_Description.Text = ""
        dialogLawFolder.Hide()
    End Sub

    Public Sub ShowDialog(FolderID As Integer, dialogAction As String)
        lblFolderPath.Text = CL.GetFolderPath(FolderID)
        If dialogAction = LawDialogAction.newFolder.ToString Then
            lblAction.Text = "New "

            lblParentID.Text = FolderID
            lblFolderID.Text = 0
        Else
            lblAction.Text = "Edit "

            Dim cl As New LawClass
            Dim dt As DataTable = cl.GetFolderDetail(FolderID)
            If dt.Rows.Count > 0 Then
                lblFolderID.Text = FolderID
                lblParentID.Text = dt.Rows(0)("parent_id")
                txt_JobName.Text = dt.Rows(0)("folder_name")

                If Convert.IsDBNull(dt.Rows(0)("folder_description")) = False Then txt_Description.Text = dt.Rows(0)("folder_description")
            End If
            dt.Dispose()
        End If
        Disabled = False

        dialogLawFolder.Show()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
    End Sub


    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        lblValidation.Text = ""
        pnlValidation.Visible = False
        dialogLawFolder.Show()
    End Sub

End Class