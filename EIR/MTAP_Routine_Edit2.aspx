﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="MTAP_Routine_Edit2.aspx.vb" Inherits="EIR.MTAP_Routine_Edit2" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
    .ValueMapingTable
    {
    	width:100%;
    	font-size:10px;    	
    	}
    .ValueMapingTable tr td
    {
    	padding:3px;
    	text-align:center;
    	border:1px solid #ccc;
    	}
    
</style>

<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>Edit MTAP Report</h2>
			
            <div class="clear"></div> <!-- End .clear -->		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" CssClass="default-tab current">Report Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold; font-size:16px;">
								<label class="column-left" style="width:120px; font-size:16px;" >Report for: </label>
								     <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ></asp:Label>
							    </p>	
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button"> <span><img src="resources/images/icons/cross_48.png" alt="icon" /><br />Clear </span></asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete current information permanently?">
                                    </cc1:ConfirmButtonExtender>
								 						  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button"> <span><img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />Preview report </span></asp:LinkButton>
								  </li>
					        </ul>								
                            <br><br>
                            </fieldset>
                            <asp:Panel ID="pnlGrid" runat="server">
                                 <table>
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="text-align:center; border:1px solid #CCCCCC; width:60px;">
                                                Item No
                                            </th>
                                            <th colspan="2" style="text-align:center; border:1px solid #CCCCCC;">
                                                Equipment
                                            </th>
                                            <th colspan="6" style="text-align:center; border:1px solid #CCCCCC;">
                                                Inspection Items
                                            </th>
                                            <th rowspan="2" style="text-align:center; border:1px solid #CCCCCC; width:60px;">Action
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Tag No
                                            </th>
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Testing<br>Mode
                                            </th>
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Power Quality
                                            </th> 
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Insulation
                                            </th> 
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Power Circuit
                                            </th>
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Stator
                                            </th>
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Rotor
                                            </th>
                                            <th style="text-align:center; border:1px solid #CCCCCC;">
                                                Air Gap
                                            </th>                               
                                        </tr>
                                    </thead>
                                         <tbody>
                                            <asp:Repeater ID="rptTAG" runat="server" >
                                                <ItemTemplate>
                                                      <tr>
                                                            <td style="text-align:center; border:1px solid #CCCCCC;">
                                                                <asp:Label ID="lblNo" Text="" runat="server"></asp:Label>
                                                            </td>
                                                            <td style="border:1px solid #CCCCCC;">
                                                                <asp:LinkButton ID="lblTagCode" Text="" runat="server" Font-Bold="True"  CommandName="Edit"></asp:LinkButton>
                                                            </td>
                                                            <td style="border:1px solid #CCCCCC;">
                                                                <asp:DropDownList ID="ddlMode" runat="server" style="border:none; width:100%;">
                                                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                                </asp:DropDownList>                                                                
                                                            </td>
                                                            <td id="tdPowerQuality" runat="server" style="border:1px solid #CCCCCC; font-size:14; text-align:center;">
                                                                <asp:LinkButton ID="lblPowerQuality" Text="" runat="server" ForeColor="#666666"  CommandName="Edit"></asp:LinkButton>
                                                                <asp:DropDownList ID="ddlPowerQuality" runat="server" style="border:none; width:100%; text-align:center;">
                                                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="OK"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="AB"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td id="tdInsulation" runat="server" style="border:1px solid #CCCCCC; font-size:14; text-align:center;">
                                                                <asp:LinkButton ID="lblInsulation" Text="" runat="server" ForeColor="#666666"  CommandName="Edit"></asp:LinkButton>
                                                            </td>
                                                            <td id="tdPowerCircuit" runat="server" style="border:1px solid #CCCCCC; font-size:14; text-align:center;">
                                                                <asp:LinkButton ID="lblPowerCircuit" Text="" runat="server" ForeColor="#666666"  CommandName="Edit"></asp:LinkButton>
                                                                <asp:DropDownList ID="ddlPowerCircuit" runat="server" style="border:none; width:100%; text-align:center;">
                                                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="OK"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="AB"></asp:ListItem>
                                                                </asp:DropDownList>                                                                
                                                            </td>
                                                            <td id="tdStator" runat="server" style="border:1px solid #CCCCCC; font-size:14; text-align:center;">
                                                                <asp:LinkButton ID="lblStator" Text="" runat="server" ForeColor="#666666"  CommandName="Edit"></asp:LinkButton>
                                                                <asp:DropDownList ID="ddlStator" runat="server" style="border:none; width:100%; text-align:center;">
                                                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="OK"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="AB"></asp:ListItem>
                                                                </asp:DropDownList>                                                               
                                                            </td>
                                                            <td id="tdRotor" runat="server" style="border:1px solid #CCCCCC; font-size:14; text-align:center;">
                                                                <asp:LinkButton ID="lblRotor" Text="" runat="server" ForeColor="#666666"  CommandName="Edit"></asp:LinkButton>                                                               
                                                            </td>
                                                            <td id="tdAirGap" runat="server" style="border:1px solid #CCCCCC; font-size:14; text-align:center;">
                                                                <asp:LinkButton ID="lblAirGap" Text="" runat="server" ForeColor="#666666"  CommandName="Edit"></asp:LinkButton>                                                              
                                                            </td>
                                                            <td style="border:1px solid #CCCCCC; text-align:right;">
                                                                <asp:TextBox ID="txtPowerFrom" runat="server" style="display:none;"></asp:TextBox>
                                                                <asp:Button ID="btnSave" runat="server" style="display:none;" CommandName="Save"></asp:Button>
                                                                <asp:ImageButton ID="btnInComplete" runat="server" ImageUrl="resources/images/icons/alert.gif" ToolTip="Incomplete" CommandName="Edit"></asp:ImageButton>
                                                                <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="resources/images/icons/pencil.png" BorderStyle="None" CommandName="Edit" />
                                                                <a ID="btnReport" runat="server" href="javascript:;"><img src="resources/images/icons/printer.png" border="0"></a>
                                                            </td>
                                                        </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            
                                     </tbody>
                                 </table>
                             </asp:Panel>
                             <asp:Panel ID="pnlData" runat="server" Visible="false">
                                
                                    <label style="font-weight:normal; height:10px;"></label>
				                    <label style="font-size:16px;" >Name Plate Motor : 
                                    <asp:Label ID="lblTagNo" runat="server" Text="-" CssClass="EditReportHeader"></asp:Label>
                                    </label>
				                  <table cellpadding="0" cellspacing="0" style="width:100%;"> 
					                <tr style="height:35px;">
					                    <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Brand
					                    </td>
					                    <td style="border:1px solid #CCCCCC; width:320px; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagBrand" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Voltage (V)
					                    </td>
					                    <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagVoltage" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="width:120px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Frequency (Hz)
					                    <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagFrequency" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td></td>
					                </tr>
					                <tr style="height:35px;">
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Type
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagType" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Current (A)
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagCurrent" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Power Factor
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagPowerFactor" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td></td>
					                </tr>
					                <tr style="height:35px;">
					                    <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        KW
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagKW" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        RPM
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagRPM" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Insulation Class
					                    </td>
					                    <td style="border:1px solid #CCCCCC; vertical-align:middle;">
					                        <asp:Label runat="server" ID="lblTagInsulationClass" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:Label>
					                    </td>
					                    <td></td>
					                </tr>
                                    <tr style="height:35px;">
                                       <td style="width:100px; border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Mode
					                    </td>
                                       <td>
					                        <asp:DropDownList ID="ddlSelectMode" runat="server" style="border:none; background-color:Transparent; width:100%; text-align:center; font-weight:bold;" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Online Testing Mcc To Motor"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Offline Testing Mcc To Motor"></asp:ListItem>
                                            </asp:DropDownList>
					                    </td>
                                       <td style="border:1px solid #CCCCCC; vertical-align:middle; font-weight:700;">
					                        Inspected Date
					                    </td>
                                       <td style="border:1px solid #CCCCCC;" colspan="2">
                                            <asp:TextBox ID="txtInspDate" runat="server" Width="100%" Font-bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                AutoPostBack="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txt_Calendar" runat="server" 
                                                Format="yyyy-MM-dd" TargetControlID="txtInspDate" PopupPosition="Right"></cc1:CalendarExtender>
                                        </td>
                                       <td style="border:1px solid #CCCCCC;"></td>
                                   </tr>
					             </table>
					            
					            
                                <asp:Button ID="btnHiden" runat="server" Text="-" style="display:none;"/>
					            <asp:Panel ID="pnlOnline" runat="server" Width="1000px" DefaultButton="btnHiden">
                                <p>
                                    <label style="font-weight:normal; height:10px;"></label>
				                    <label style="height:10px; font-size:16px;">Current Signature Analysis</label>
				                    <label style="font-weight:normal; height:10px;" >Evaluating the condition of an induction motor's squirrel cage rotor</label>
				                </p>
				                <table cellpadding="0" cellspacing="0"> 
                                    <tr style="height:35px;">
                                        <td colspan="2"></td>
                                        <td style="text-align:center; font-weight:700; border:1px solid #CCCCCC; width:200px; vertical-align:middle;">Condition</td>
                                        <td style="width:420px; border:1px solid #CCCCCC;" rowspan="11">
                                            <div style="position:absolute;">
                                                
                                                <asp:ImageButton ID="imgCurrent1" runat="server" Width="400px" Height="200px" ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" ToolTip="Click to edit picture" />
                                                <asp:button ID="btnSaveImageCurrent1" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;"/>
                                                
                                                <br>
                                                <br>
                                                
                                                <asp:ImageButton ID="imgCurrent2" runat="server" Width="400px" Height="200px" ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" ToolTip="Click to edit picture" />
                                                <asp:button ID="btnSaveImageCurrent2" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;"/>
                                                
                                           </div>
                                        </td>
                                        <td rowspan="4">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">Fline-Fpole pass<br />
                                            (dB)</td>
                                        <td style="border:1px solid #CCCCCC; width:120px; vertical-align:middle;">
                                            <asp:TextBox ID="txtFline" runat="server" Width="100%" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center; height: 18px;" 
                                                AutoPostBack="true"></asp:TextBox>
                                        </td>
                                        <td id="tdFline" runat="server" style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                            <asp:label ID="lblFline" runat="server" Font-Bold="True"></asp:label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">Swirl Effect</td>
                                        <td style="border:1px solid #CCCCCC; width:117px;">
                                            <asp:DropDownList ID="ddlSwirlEffect" runat="server" Font-Bold="true" style="border:none; background-color:Transparent; width:100%; text-align:center;" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Occured"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Not Occured"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td id="tdSwirlEffect" runat="server" style="border:1px solid #CCCCCC; width:100px; text-align:center;">
                                            <asp:label ID="lblSwirlEffect" runat="server" Font-Bold="True"></asp:label>
                                        </td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td rowspan="8" style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">Ref. IEEE Std<br />43-2000</td>
                                        <td colspan="2" style="font-weight:700; text-align:center; border:1px solid #CCCCCC; vertical-align:middle; font-size:10px;">Criteria for analyzing CS</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            > 60
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#00b0f0">
                                            Excellent
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            60 - 54
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#00b050">
                                            Good
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            54 - 48
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#92d050">
                                            Moderate
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            48 - 42
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#ffff00">
                                            Rotor bar crack may be developing or problem with high resistance joints
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            42 - 36
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#ffc000">
                                            Rotor bars likely cracked or broken or problems with high resistance joints
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            36 - 30
                                        </td>
                                        <td style="color:White; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#ff0000">
                                            Multiple cracked or broken bars at end rings indicated. Also slip ring and joint problems.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC;" bgcolor="#DBDBDB">
                                            < 30
                                        </td>
                                        <td style="color:White; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#c00000">
                                            Multiple cracked or broken bars at end rings indicated. Severe rotor problems.
                                        </td>
                                    </tr>
                                </table> 
                                <p>
                                    <label style="font-weight:normal; height:10px;"></label>
				                    <label style="height:10px; font-size:16px;">Eccentricity</label>
				                </p>
				                <table cellpadding="0" cellspacing="0"> 
                                    <tr style="height:35px;">
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; vertical-align:middle; font-weight:700;">Peak 1 (&#8710;dB)</td>
                                        <td id="tdPeak1" runat="server" style="border:1px solid #CCCCCC; width:120px; vertical-align:middle;">
                                            <asp:TextBox ID="txtPeak1" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:none; border-top:1px solid #ccc;">&nbsp;</td>
                                        <td style="width:500px; border-right:1px solid #ccc; border-top:1px solid #ccc; border-bottom:1px solid #ccc;" rowspan="10">
                                            <div style="position:absolute;">
                                                <p>
                                                    <asp:ImageButton ID="imgEccentricity" runat="server" Height="200px" 
                                                        ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" 
                                                        ToolTip="Click to edit picture" Width="400px" />
                                                    <asp:Button ID="btnSaveImageEccentricity" runat="server" Height="0px" 
                                                        style="visibility:hidden; position:absolute;" Width="0px" />
                                                </p>
                                            </div>
                                            
                                            
                                        </td>
                                        <td rowspan="10" style="border-left:none;"></td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; vertical-align:middle; font-weight:700;">Peak 2 (&#8710;dB)</td>
                                        <td id="tdPeak2" runat="server" style="border:1px solid #CCCCCC; vertical-align:middle;">
                                            <asp:TextBox ID="txtPeak2" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:none;"></td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; vertical-align:middle; font-weight:700;">Peak 3 (&#8710;dB)</td>
                                        <td id="tdPeak3" runat="server" style="border:1px solid #CCCCCC; vertical-align:middle;">
                                            <asp:TextBox ID="txtPeak3" runat="server" AutoPostBack="true" Font-Bold="true" style="border:none; background-color:Transparent; text-align:center;" Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:none;"></td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; vertical-align:middle; font-weight:700;">Peak 4 (&#8710;dB)</td>
                                        <td id="tdPeak4" runat="server" style="border:1px solid #CCCCCC; vertical-align:middle;">
                                            <asp:TextBox ID="txtPeak4" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border-style: none; border-color: inherit; border-width: medium; background-color:Transparent; text-align:center; height: 18px;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="text-align:center; font-weight:bold; border:1px solid #CCCCCC; border-right:none;  border-top:none; width:200px;"></td>
                                    </tr>
                                   
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">#Peaks above<br />
                                            noise level</td>
                                        <td style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;">
                                            <asp:label ID="lblPeakCount" runat="server" style="text-align:center;" Font-Bold="true" ForeColor="Black"></asp:label>
                                        </td>
                                        <td id="tdAbove" runat="server" style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;">
                                            <asp:DropDownList ID="ddlPeak" runat="server" AutoPostBack="true" 
                                                style="border:none; text-align:center; font-weight:bold">
                                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Good" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Modelate" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Severe" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                            
                                        </td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td colspan="3" style="font-weight:700; text-align:center; border:1px solid #CCCCCC; vertical-align:middle;">Criteria for analyzing Eccentricity</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">Ref. IEEE Std<br />43-2000</td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            > 20
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#ff0000">
                                            Severe
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            10 - 20
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#ffff00">
                                            Moderate
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#DBDBDB">
                                            0 - 10
                                        </td>
                                        <td style="color:Black; text-align:center; border:1px solid #CCCCCC; font-size:10px;" bgcolor="#92d050">
                                            Good
                                        </td>
                                    </tr>
                                </table> 
                                <p>
                                    <label style="font-weight:normal; height:10px;"></label>
				                    <label style="height:10px; font-size:16px;">Power Quality&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:DropDownList ID="ddlPowerQualityStatus" runat="server" AutoPostBack="true" style="width:100px; text-align:center;">
                                        <asp:ListItem Text="" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="OK" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="AB" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    </label>
				                </p>
				                
                                <asp:ImageButton ID="imgPowerQuality" runat="server" Height="800px" 
                                    ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" 
                                    ToolTip="Click to edit picture" Width="800px"/>
                                <asp:Button ID="btnSaveImagePowerQuality" runat="server" Height="0px" 
                                    style="visibility:hidden; position:absolute;" Width="0px" />
                                
				                </asp:Panel> 
				                <asp:Panel ID="pnlOffline" runat="server" Width="1000px" DefaultButton="btnHiden">
				                <p>
				                    <label style="font-weight:normal; height:10px;"></label>
				                    <label style="height:10px; font-size:16px;">AC STANDARD TEST</label>
				                </p>
				                <table cellpadding="0" cellspacing="0" align="center"> 
				                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:250px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            Temperature (°C)
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:150px; vertical-align:middle;">
                                            <asp:TextBox ID="txtTemperature" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                    </tr>
                                    <tr style="height:35px;">
                                      <td colspan="2" style="border:1px solid #CCCCCC; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            <asp:Label ID="lblNote" runat="server" Text="Note" Font-Bold="True" Font-Underline="True"></asp:Label>
                                            &nbsp;: Stator
                                        <td style="font-weight:bold; text-align:center; border:1px solid #CCCCCC;">Condition</td>
                                        <td style="font-weight:bold; text-align:center; border:1px solid #CCCCCC;" colspan="6" align="center">Limit Value</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            Ohm Ph 1 to 2
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtOhm12" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        <td >&nbsp;
                                            </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            Ohm Ph 1 to 3
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtOhm13" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            Ohm Ph 2 to 3
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtOhm23" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                        <td>&nbsp;
                                      </td>
                                       <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            %Resistive Imbalance
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtResistiveImbalance" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td id="tdResistiveImbalance" 
                                            style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;" runat="server">
                                            <asp:Label ID="lblResistiveImbalance" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                                        </td>
                                        <td colspan="6" rowspan="3" style="border-right:1px solid #CCCCCC; padding:0;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="ValueMapingTable">
                                          <tr>
                                            <td colspan="6" style="text-align:center;"><b>% Resistive Imbalance</b></td>
                                          </tr>
                                          <tr>
                                            <td>Voltage &lt;= 600</td>
                                            <td>>= 4</td>
                                            <td class="MTAP_Eccentricity_Severe">Severe</td>
                                            <td>Voltage > 600</td>
                                            <td>>= 3</td>
                                            <td class="MTAP_Eccentricity_Severe">Severe</td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>>= 3</td>
                                            <td class="MTAP_PI_Caution">Coution</td>
                                            <td>&nbsp;</td>
                                            <td>>= 2</td>
                                            <td class="MTAP_PI_Caution">Coution</td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>< 3</td>
                                            <td class="MTAP_PI_Good">Good</td>
                                            <td>&nbsp;</td>
                                            <td>< 2</td>
                                            <td class="MTAP_PI_Good">Good</td>
                                          </tr>
                                        </table>
                                            </td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            mH Ph 1 to 2
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtmH12" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            mH Ph 1 to 3
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtmH13" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            mH Ph 2 to 3
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtmH23" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                        <td colspan="6" rowspan="5" style="vertical-align:bottom; border-right:1px solid #CCCCCC; padding:0;">
                                        
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="ValueMapingTable">
                                          <tr>
                                            <td colspan="6" align="center"><b>%Inductive Imbalance</b></td>
                                          </tr>
                                          <tr>
                                            <td>Voltage &lt;= 600</td>
                                            <td>&gt;= 12</td>
                                            <td class="MTAP_Eccentricity_Severe">Severe</td>
                                            <td>Voltage &gt; 600</td>
                                            <td>&gt;= 7</td>
                                            <td class="MTAP_Eccentricity_Severe">Severe</td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>&gt;= 8</td>
                                            <td class="MTAP_PI_Caution">Coution</td>
                                            <td>&nbsp;</td>
                                            <td>&gt;= 5</td>
                                            <td class="MTAP_PI_Caution">Coution</td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>&lt; 8</td>
                                            <td class="MTAP_PI_Good">Good</td>
                                            <td>&nbsp;</td>
                                            <td>&lt; 5</td>
                                            <td class="MTAP_PI_Good">Good</td>
                                          </tr>
                                          <tr>
                                            <td colspan="6"><b>Resistance to Ground (M&#937;)</b></td>
                                          </tr>
                                          <tr>
                                            <td>Voltage &lt;= 600</td>
                                            <td>&lt;= 5</td>
                                            <td class="MTAP_Eccentricity_Severe">Severe</td>
                                            <td>Voltage &gt; 600</td>
                                            <td>&lt;= 100</td>
                                            <td class="MTAP_Eccentricity_Severe">Severe</td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>&gt; 5</td>
                                            <td class="MTAP_PI_Good">Good</td>
                                            <td>&nbsp;</td>
                                            <td>&gt; 100</td>
                                            <td class="MTAP_PI_Good">Good</td>
                                          </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; height: 25px; vertical-align:middle; font-weight:700;">
                                            Average Inductance
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; text-align:center; vertical-align:middle;">
                                            <asp:label ID="lblAverageInductance" runat="server" style="text-align:center;" ForeColor="Black" Font-Bold="true"></asp:label>
                                        </td>
                                        <td style="border-right:1px solid #CCCCCC;">&nbsp;</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            %Inductive Imbalance
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtInductiveImbalance" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center; height: 18px;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td id="tdInductiveImbalance" style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;" runat="server">
                                            <asp:DropDownList ID="ddlInductiveImbalance" runat="server" AutoPostBack="true" Font-Bold="true"  
                                                style="border:none; font-weight:bold; color:Black; ">
                                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Good"  Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Coution" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Severe" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            Capacitance to Ground (pF)
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtCapacitance" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td id="tdCapacitance" style="border:1px solid #CCCCCC; text-align:center; vertical-align:middle;" runat="server">
                                            <asp:DropDownList ID="ddlCapacitance" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; font-weight:bold; color:Black; ">
                                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Good" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Severe" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                            
                                        </td>
                                        <td colspan="6" rowspan="2">
                                            &nbsp;</td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="border:1px solid #CCCCCC; width:200px; padding-left:10px; vertical-align:middle; font-weight:700;">
                                            Resistance to Ground (M&#937;)
                                        </td>
                                      <td style="border:1px solid #CCCCCC; width:100px; vertical-align:middle;">
                                            <asp:TextBox ID="txtResistance" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center; height: 18px;" 
                                                Width="100%"></asp:TextBox>
                                        </td>
                                        <td id="tdResistance" style="border:1px solid #CCCCCC; width:130px; text-align:center; vertical-align:middle;" runat="server">
                                            <asp:Label ID="lblResistance" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                                        </td>
                                    </tr>   
                                    
                                </table> 
                                <p>
                                    <label style="font-weight:normal; height:10px;"></label>
				                    <label style="height:10px; font-size:16px;">POLARIZATION INDEX</label>
				                    <label style="font-weight:normal; height:10px;">Addressing the insulation fault zone</label>
				                </p>
				                <table cellpadding="0" cellspacing="0"> 
                                    <tr>
                                        <td style="width:100px;">
                                        
                                        </td>
                                        <td style="width:100px;">
                                        
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; font-weight:700;">
                                            Condition
                                        </td>
                                        <td rowspan="9">
                                            <div style="position:absolute; padding-left:40px;">
                                                <asp:ImageButton ID="imgPolarization" runat="server" Height="200px" 
                                                    ImageUrl="resources/images/Sample_40.png" style="cursor:pointer;" 
                                                    ToolTip="Click to edit picture" Width="400px" />
                                                <asp:Button ID="btnSaveImagePolarization" runat="server" Height="0px" 
                                                    style="visibility:hidden; position:absolute;" Width="0px" />
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr style="height:35px;">
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-weight:bold; vertical-align:middle; font-weight:700;">
                                            PI Value
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; vertical-align:middle;">                                            
                                                <asp:TextBox ID="txtPI_Value" runat="server" AutoPostBack="true" Font-Bold="true"
                                                style="border:none; background-color:Transparent; text-align:center;" 
                                                Width="100%"></asp:TextBox>                                            
                                        </td>
                                        <td id="tdPI_Value" runat="server" style="text-align:center; border:1px solid #CCCCCC; width:120px; vertical-align:middle;">
                                           <asp:Label ID="lblPI_Value" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                                        </td>                                       
                                    </tr>
                                    <tr>
                                        <td rowspan="7" style="text-align:center; border:1px solid #CCCCCC; width:100px; font-weight:700; font-size:10px;">
                                            Ref. IEEE Std 43-2000
                                        </td>
                                        <td colspan="2" style="text-align:center; border:1px solid #CCCCCC; font-weight:700; font-size:10px;">
                                            Criteria for analyzing PI
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-size:10px;" bgcolor="#DBDBDB">
                                            > 7
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black; font-size:10px;" bgcolor="#ff0000">
                                            *Severe
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; color:Black; font-size:10px;" bgcolor="#DBDBDB">
                                            > 5
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black; font-size:10px;" bgcolor="#ffc000">
                                            *Coution
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-size:10px;" bgcolor="#DBDBDB">
                                            > 2 - 5
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black; font-size:10px;" bgcolor="#92d050">
                                        
                                            Good
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-size:10px;" bgcolor="#DBDBDB">
                                            1.5 - 2.0
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black; font-size:10px;" bgcolor="#ffff00">
                                        
                                            Observe
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-size:10px;" bgcolor="#DBDBDB">
                                            1.0 - 1.5
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black; font-size:10px;" bgcolor="#ffc000">
                                            Coution
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:100px; font-size:10px;" bgcolor="#DBDBDB">
                                            < 1
                                        </td>
                                        <td style="text-align:center; border:1px solid #CCCCCC; width:120px; color:Black; font-size:10px;" bgcolor="#ff0000">
                                            Severe
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align:center; border:1px solid #CCCCCC; font-size:10px;">
                                            <asp:Label ID="Label1" runat="server" Text="Note" Font-Bold="True" Font-Underline="True"></asp:Label>
                                            &nbsp;: If the 1 min RTG is > 5 Gohms, the
                                            <br />
                                            calculated PI ratio may not be meaningful.<br />
                                            <asp:Label ID="Label2" runat="server" Text="* Interpreting value with a graph shape." ForeColor="Red"></asp:Label>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                                
                                
                                
				                </asp:Panel>

							 <p align="right">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							 </p>
                             </asp:Panel>
                             
				    </div>
		       	 <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>	         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
</ContentTemplate>
</asp:UpdatePanel>  
	 
</asp:Content>