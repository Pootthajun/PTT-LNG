﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Total_Problem_Report.aspx.vb" Inherits="EIR.Dashboard_Total_Problem_Report" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register src="UC_Dashboard_Total_Problem_Report.ascx" tagname="UC_Dashboard_Total_Problem_Report" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


<!-- Page Head -->
<h2>Total Problem by Month</h2>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
  <tr>
	<td>
	    <h3>
        <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="From"></asp:Label>
        &nbsp;<asp:DropDownList ID="ddl_Month_F" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:DropDownList ID="ddl_Year_F" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="#009933" Text="To"></asp:Label>
&nbsp;<asp:DropDownList ID="ddl_Month_T" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:DropDownList ID="ddl_Year_T" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:Label ID="Label2" runat="server" ForeColor="#009933" 
            Text="Equipment Category"></asp:Label>
        &nbsp;<asp:DropDownList ID="ddl_Equipment" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        </h3>         
     </td>
  </tr>
  <tr>
	<td>
	    <asp:Panel ID="pnlDashboard" runat="server">
            <uc1:UC_Dashboard_Total_Problem_Report ID="UC_Dashboard_Total_Problem_Report" runat="server" />
        </asp:Panel>
    </td>
  </tr>
</table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
