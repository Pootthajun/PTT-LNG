﻿Imports System.Data
Imports System.Data.SqlClient
Public Class PDMA_Plan
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CV As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Lube_Oil_Report

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetPlan(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindPlan()
        Dim SQL As String = "SELECT * FROM VW_REPORT_PDMA_HEADER " & vbLf

        Dim WHERE As String = ""
        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If

        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= "  PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= "  ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND " & vbNewLine
        End If

        If WHERE <> "" Then
            SQL &= "WHERE " & WHERE.Substring(0, WHERE.Length - 6) & vbNewLine
        End If

        SQL &= " ORDER BY RPT_Year,PLANT_Code,ROUTE_Code"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("PDMA_Routine_Plan") = DT

        Navigation.SesssionSourceName = "PDMA_Routine_Plan"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        Select Case e.CommandName
            Case "ToggleStatus"
                Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

                Dim RPT_Year As Integer = btnToggle.Attributes("RPT_Year")
                Dim RPT_No As Integer = btnToggle.Attributes("RPT_No")
                Try
                    BL.Drop_RPT_PDMA_Header(RPT_Year, RPT_No)
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindPlan()

                lblBindingSuccess.Text = "Delete plan successfully"
                pnlBindingSuccess.Visible = True
        End Select
    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblYear As Label = e.Item.FindControl("lblYear")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblTags As Label = e.Item.FindControl("lblTags")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblCode.Text = e.Item.DataItem("RPT_Code")
        lblYear.Text = e.Item.DataItem("RPT_Year")
        lblPlant.Text = e.Item.DataItem("PLANT_Code")
        lblRoute.Text = e.Item.DataItem("ROUTE_Code").ToString
        lblTags.Text = e.Item.DataItem("TOTAL_TAG")
        '-------------------- Route----------------------
        lblStatus.Text = e.Item.DataItem("STEP_Name")
        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        btnToggle.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnToggle.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
    End Sub

    Protected Sub ResetPlan(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearPanelSearch()
        BindPlan()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListPlan.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False

        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")

        btnConstruct.Visible = USER_LEVEL = EIR_BL.User_Level.Administrator Or
                            USER_LEVEL = EIR_BL.User_Level.Inspector Or
                            USER_LEVEL = EIR_BL.User_Level.Approver


    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_PDMA_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
            If i = Now.Year + 543 Then
                ddl_Search_Year.SelectedIndex = ddl_Search_Year.Items.Count - 1
            End If
        Next

        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDl_PDMA_Route(0, ddl_Search_Route)
    End Sub

    Protected Sub ddl_Search_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged, ddl_Search_Route.SelectedIndexChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BindPlan()
        BL.BindDDl_PDMA_Route(ddl_Search_Plant.SelectedValue, ddl_Search_Route)
    End Sub
#End Region

    Protected Sub btnConstruct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConstruct.Click

        ClearPanelEdit()
        '--------------- Bind Year ---------------
        ddl_Edit_Year.Items.Clear()
        Dim DefaultItem As New ListItem("---Select Year---", 0)
        ddl_Edit_Year.Items.Add(DefaultItem)
        For i As Integer = Now.Year - 1 To Now.Year + 1
            Dim Item As New ListItem(i + 543, i + 543)
            ddl_Edit_Year.Items.Add(Item)
        Next
        ddl_Edit_Year.SelectedIndex = 0

        pnlEdit.Visible = True
        btnConstruct.Visible = False

        '-----------------------------------
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddl_Edit_Year.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Year"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL &= "SELECT DISTINCT MS_PDMA_Route.PLANT_ID,MS_PDMA_Route.ROUTE_ID" & vbCrLf
        SQL &= "FROM MS_PDMA_Route" & vbCrLf

        SQL &= "LEFT JOIN RPT_PDMA_Header ON RPT_PDMA_Header.ROUTE_ID = MS_PDMA_Route.ROUTE_ID " & vbCrLf
        SQL &= "AND RPT_Year=" & ddl_Edit_Year.Items(ddl_Edit_Year.SelectedIndex).Value & vbCrLf
        SQL &= "WHERE RPT_PDMA_Header.ROUTE_ID Is NULL" & vbCrLf
        SQL &= "ORDER BY PLANT_ID,MS_PDMA_Route.ROUTE_ID"
        Dim DT As New DataTable
        Dim DT_PDMA As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        SQL = "SELECT * FROM RPT_PDMA_Header WHERE 1=0"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_PDMA)

        Dim NeedToUpdate As Boolean = False
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim DR As DataRow = DT_PDMA.NewRow
            DR("RPT_Year") = ddl_Edit_Year.Items(ddl_Edit_Year.SelectedIndex).Value
            DR("RPT_No") = GetNewPlanNo(DR("RPT_Year"))
            DR("PLANT_ID") = DT.Rows(i).Item("PLANT_ID")
            DR("ROUTE_ID") = DT.Rows(i).Item("ROUTE_ID")
            Dim TheYear As Integer = ddl_Edit_Year.Items(ddl_Edit_Year.SelectedIndex).Value
            Dim StartDate As String = (TheYear - 543) & "-01-01"
            Dim EndDate As String = (TheYear - 543) & "-12-31"
            DR("RPT_Period_Start") = CV.StringToDate(StartDate, "yyyy-MM-dd")
            DR("RPT_Period_End") = CV.StringToDate(EndDate, "yyyy-MM-dd")
            DR("RPT_STEP") = EIR_BL.Report_Step.New_Step
            DR("Update_Time") = Now
            DR("Update_By") = Session("USER_ID")
            DT_PDMA.Rows.Add(DR)

            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT_PDMA)
            DT.AcceptChanges()
            NeedToUpdate = True
        Next

        If NeedToUpdate Then
            ResetPlan(Nothing, Nothing)
            lblBindingSuccess.Text = "Construct successfully"
            pnlBindingSuccess.Visible = True
        Else
            lblValidation.Text = "Plan for this year has already constructed"
            pnlValidation.Visible = True
        End If


    End Sub

    Private Function GetNewPlanNo(ByVal RPT_Year As Integer) As Integer
        Dim SQL As String = "SELECT IsNull(MIN(RPT_No),0)-1 FROM RPT_PDMA_Header" & vbNewLine
        SQL &= "  WHERE RPT_Year=" & RPT_Year
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNewDetailID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(DETAIL_ID),0)+1 FROM RPT_PDMA_Detail" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function


End Class