﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LAW_Document_Template_Setting.aspx.vb" Inherits="EIR.LAW_Document_Template_Setting" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>

    <h2>Document Template Setting</h2>
			
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->             
                <div class="content-box-content">
                    
                     <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  
                   <asp:Panel ID="pnlListDocument" runat="server">
                  
                      <!-- This is the target div. id must match the href of this div's tab -->
                      <table>
                        <thead>
                          <tr>
                           <th><a href="#">Document Name</a></th>
                            <th><a href="#">Status</a></th>
                            <th><a href="#">Action</a></th>
                          </tr>
                        </thead>
                        <asp:Repeater ID="rptDocument" runat="server">
                               <HeaderTemplate>
                                    <tbody>
                                 </HeaderTemplate>
                                  <ItemTemplate>
                                      <tr>
                                        <td><asp:Label ID="lblDocumentName" runat="server"></asp:Label></td>
                                        <td><asp:Label ID="lblActiveStatus" runat="server"></asp:Label></td>
                                    
                                        <td><!-- Icons -->
                                            <asp:Label ID="lblDocumentSettingID" runat="server" Visible="false" ></asp:Label>
                                           <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                           <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                           <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnToggle" ConfirmText="Are you sure to delete this document permanently?" />
                                        </td>
                                      </tr>
                                   </ItemTemplate>
                                  
                                   <FooterTemplate>
                                 </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        <tfoot>
                          <tr>
                            <td colspan="7">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                            </div>
                                <uc1:PageNavigation ID="Navigation" runat="server" />
                              <!-- End .pagination -->
                              <div class="clear"></div>  
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                   </asp:Panel>
                   
                   <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                   
                <asp:Panel ID="pnlEdit" runat="server">
                    <div class="content-box-header">
                        <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Document Template</h3>
                        <div class="clear"></div>
                        <asp:Label ID="lblDocumentProcessSettingID" runat="server" Text="0" Visible="false" ></asp:Label>
                    </div>
                   
                    <fieldset>
                        <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                        <p>&nbsp;</p>
					    <p>
                            <label class="column-left" style="width:200px;" >Document Name : </label>
                            <asp:TextBox runat="server" ID="txt_Document_Name" CssClass="text-input small-input " Width="600px" MaxLength="255"></asp:TextBox>
                        </p>
                        <p>
                            <label class="column-left" style="width:200px;" >Interval : </label>
                            <asp:TextBox runat="server" ID="txtIntervalYear" CssClass="text-input small-input " Width="50px" MaxLength="2"></asp:TextBox> Year(s)
                        </p>
                        <p>
                          <label class="column-left" style="width:200px;" >Process Description : </label>
                        </p>
                        <table cellpadding="0" cellspacing="0" class="propertyTable">
                            <asp:Repeater ID="rptPaperList" runat="server">
                                <HeaderTemplate>
                                    <thead>
                                        <th style="width:5%;height:24px;text-align:center;" class="propertyCaption" >No.</th>
                                        <th style="text-align:center;" class="propertyCaption">Paper Name</th>
                                        <th style="width:20%;text-align:center;" class="propertyCaption">Organize</th>
                                        <th style="width:5%;text-align:center;" class="propertyCaption">%</th>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align:center;" >
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            <asp:TextBox runat="server" ID="txt_Paper_Name" CssClass="text-input small-input " Width="98%"   MaxLength="255"></asp:TextBox>
                                            <asp:Label ID="lblDocumentSettingPaperID" runat="server" Visible="false" Text="0" ></asp:Label>
                                        </td>
                                        <td >
                                            <asp:DropDownList CssClass="text-input small-input" ID="cmbOrganize" style="width:100%"
                                            runat="server" Font-Bold="True"></asp:DropDownList>	
                                        </td>
                                        <td style="text-align:center;">
                                            <asp:TextBox runat="server" ID="txt_Percent_Complete" CssClass="text-input small-input" Width="30px"
                                                AutoPostBack="true"  OnTextChanged="txt_Percent_Complete_TextChanged" MaxLength="255"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnDelete" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                            <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this paper permanently?" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td style="text-align:right;" colspan="2" >&nbsp;Total %</td>
                                        <td style="text-align:center;">
                                            <asp:Label runat="server" ID="lbl_Total_Percent"  Width="30px" Text="0" MaxLength="255"></asp:Label>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                        <p>
                            <asp:Button ID="btnAddPaper" runat="server" CssClass="button" Text="Add Paper" />
                        </p>
                        <p>
                          <label class="column-left" style="width:120px;" >Available : </label>
                            &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
				        </p>
				        <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				            <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                            <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                        </asp:Panel>
                        <p align="right">
                          <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                          <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                        </p>
                    </fieldset>

                </asp:Panel>
                    
                </div>
                <!-- End #tab1 -->
                
    </div>
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>