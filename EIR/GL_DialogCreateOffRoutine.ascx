﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogCreateOffRoutine.ascx.vb" Inherits="EIR.GL_DialogCreateOffRoutine" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<div class="MaskDialog">
</div>
<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog" style="top:250px; left:20%; width:auto;">
    <h2>
        Create Report for
        <asp:DropDownList CssClass="select" ID="ddl_Plant" runat="server" AutoPostBack="True">
        </asp:DropDownList>
		
		<asp:DropDownList CssClass="select" ID="ddl_Route" runat="server" AutoPostBack="True">
        </asp:DropDownList>    
                        
        <asp:DropDownList ID="ddl_Tag" runat="server" AutoPostBack="True" 
            CssClass="select">
        </asp:DropDownList>
        
        <asp:Label ID="lbl_TagType" runat="server" CssClass="EditReportHeader" Text=""></asp:Label>
    </h2>
   <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
    </asp:Panel>
    <asp:Button ID="btnClose" runat="server" Class="button" Text="Cancel" />
    &nbsp;
    <asp:Button ID="btnCreate" runat="server" Class="button" Text="Create report" />

</asp:Panel>
<cc1:DragPanelExtender ID="pnlDialog_DragPanelExtender" runat="server" 
    DragHandleID="pnlDialog" Enabled="True" TargetControlID="pnlDialog">
</cc1:DragPanelExtender>