﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Dashboard_Improvement_Report.ascx.vb" Inherits="EIR.UC_Dashboard_Improvement_Report" %>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
    <tr>
        <td style="vertical-align:top; text-align:center; width:500px;">
            <asp:Chart ID="ChartMain" runat="server" Width="500px" Height="400px" CssClass="ChartHighligh"
                    RightToLeft="Inherit"  >
                <Series>
                    <asp:Series ChartArea="ChartArea1" Color="DodgerBlue" LegendText="Improvement" 
                        Name="Series1">
                    </asp:Series>
                </Series>
                <titles>
                    <asp:Title Name="Title1">
                </asp:Title>
                </titles>
                <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <axisy intervalautomode="VariableCount">
                    </axisy>
                    <axisx intervalautomode="VariableCount">
                    </axisx>
                    <axisx2 intervalautomode="VariableCount">
                    </axisx2>
                    <axisy2 intervalautomode="VariableCount">
                    </axisy2>
                </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </td>
        <td style="vertical-align:top; text-align:center; width:100%">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" style="width:200px; border:1px solid #efefef;">
                  <tr>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:100px; border-bottom:1px solid #003366;">
                          Plant</td>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:100px; border-bottom:1px solid #003366;">
                          Problem(s)</td>
                  </tr>
                  <asp:Repeater ID="rptData" runat="server">
                    <ItemTemplate>
                        <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid;padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblImprovement" runat="server" ForeColor = "DodgerBlue"></asp:Label></td>
                        </tr>
                    </ItemTemplate>
                  </asp:Repeater>
              </table>
            </center>
        </td>
    </tr>
</table>
<div style="visibility: hidden">
    <asp:Label ID="lblMONTH_F" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblMONTH_T" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblYEAR_F" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblYEAR_T" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblEQUIPMENT" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblPrintPage" runat="server" Text=""></asp:Label>
</div>
