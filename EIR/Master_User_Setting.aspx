﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Master_User_Setting.aspx.vb" Inherits="EIR.Master_User_Setting" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
<!-- Page Head -->
			<h2>User Authorization</h2>
			
						
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header" >
                <h3>Display condition </h3>
					<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Level" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
				
    				<asp:DropDownList CssClass="select" style="position:relative; top:5px; left: 0px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px; left: 0px;" 
                      ID="ddl_Search_Position" runat="server" AutoPostBack="True">
                    </asp:DropDownList>                    
				            
                    <asp:CheckBox ID="chkSeeAll" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" Checked="FAlse" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="B2" runat="server">Included deactive user(s)</b>
				  
                  <asp:Panel ID="pnlSearchSkill" runat="server" Visible="false">
                      <br>
                  &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_Stationary" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" Checked="True" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="lblEditable" runat="server">Stationary</b>
                  
                  &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_Rotating" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" Checked="False" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="B1" runat="server">Rotating</b>
                  
                  &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_LubeOil" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" Checked="False" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="B3" runat="server">Lube Oil</b>
                  
                  &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_PdMA" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" Checked="False" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="B4" runat="server">PdMA</b>
                  
                  &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_Thermography" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" Checked="False" /> 
                  &nbsp; <b style="position:relative; top:8px;" id="B5" runat="server">Thermography</b>
                  
                  &nbsp;&nbsp;&nbsp;
                  &nbsp; &nbsp;  
                 </asp:Panel>
                  </div>
                  
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListUser" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">User</a></th>
                        <th><a href="#">Fullname</a></th>
                        <th><a href="#">Position</a></th>
                        <th><a href="#">Level</a></th>
                        <th><a href="#">Coverage</a></th>
                        <th><a href="#">Skill</a></th>
                        <th><a href="#">Last Visited</a></th>
                        <th><a href="#">Updated</a></th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptUser" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblUser" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblFullname" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPosition" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblLevel" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblCover" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblSkill" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblVisit" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                          <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                     </td>
                                  </tr>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="8">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                            
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				    </asp:Panel>
				    
				 <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
			    
			    <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;User </h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
                    <p>
                      <label class="column-left" style="width:150px;" >Full Name : </label>
                      <asp:TextBox runat="server" ID="txtPrefix" CssClass="text-input small-input " MaxLength="50" Width="50px"></asp:TextBox>
                      <asp:TextBox runat="server" ID="txtName" CssClass="text-input small-input " MaxLength="200" Width="150px"></asp:TextBox>
                      <asp:TextBox runat="server" ID="txtSurname" CssClass="text-input small-input " MaxLength="200" Width="150px"></asp:TextBox>
				      <!-- End .clear -->
                    </p>
					 <p>
                        <label class="column-left" style="width:150px;" >Gender : </label>
                        <asp:DropDownList CssClass="select" ID="ddl_Edit_Gender" runat="server">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    
                     <p>
                        <label class="column-left" style="width:150px;" >Position : </label>
                        <asp:DropDownList CssClass="select" ID="ddl_Edit_Position" runat="server">
                            <asp:ListItem></asp:ListItem>                            
                        </asp:DropDownList>
                    </p>
                    
					 <p>
                      <label class="column-left" style="width:150px;" >LEVEL : </label>
                      <asp:DropDownList CssClass="select" ID="ddl_Edit_Level" runat="server">
                      </asp:DropDownList>
				      <!-- End .clear -->
                    </p>
					
                    
                     <p>                      
                        <table width="650">
                            <tr>
                                <td Width="150" valign="top">
                                <label class="column-left" style="width:150px;" >Coverage Plant : </label>
                                </td>
                                <td Width="200" align="left" valign="top" style="text-align:left !important;">
                                    <asp:Repeater ID="rptPlant1" runat="server">
                                        <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" style="position:relative; top:-15px;" /> &nbsp; <asp:Label ID="lbl" runat="Server" Width="150px" runat="server" Font-Bold="true" style="position:relative; top:-15px;"></asp:Label><br><br>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td align="left" valign="top" style="text-align:left !important;">
                                    <asp:Repeater ID="rptPlant2" runat="server">
                                        <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" style="position:relative; top:-15px;" /> &nbsp; <asp:Label ID="lbl" runat="Server" Width="150px" runat="server" Font-Bold="true" style="position:relative; top:-15px;"></asp:Label><br><br>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p>                      
                        <table width="650">
                            <tr>
                                <td Width="150" valign="top">
                                <label class="column-left" style="width:150px;">Equipment Skill : </label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkStationary" runat="Server" style="position:relative; top:-15px; left:10px;" /> &nbsp; <asp:Label ID="lblStationary" runat="server" Font-Bold="true" Text="Stationary" style="position:relative; top:-15px;"></asp:Label>
                                    <asp:CheckBox ID="chkRotating" runat="Server" style="position:relative; top:-15px; left:10px;" Visible="false" /> &nbsp; <asp:Label ID="lblRotating" runat="server" Font-Bold="true"  Text="Rotating" style="position:relative; top:-15px;" Visible="false"></asp:Label>
                                    <asp:CheckBox ID="chkLubeOil" runat="Server" style="position:relative; top:-15px; left:10px;" Visible="false" /> &nbsp; <asp:Label ID="lblLubeOil" runat="server" Font-Bold="true"  Text="Lube Oil" style="position:relative; top:-15px;" Visible="false"></asp:Label>
                                    <asp:CheckBox ID="chkPdMA" runat="Server" style="position:relative; top:-15px; left:10px;" Visible="false" /> &nbsp; <asp:Label ID="lblPdMA" runat="server" Font-Bold="true"  Text="PdMA & MTap" style="position:relative; top:-15px;" Visible="false"></asp:Label>
                                    <asp:CheckBox ID="chkThermography" runat="Server" style="position:relative; top:-15px; left:10px;" Visible="false" /> &nbsp; <asp:Label ID="lblThermography" runat="Server"  Font-Bold="true"  Text="Thermography" style="position:relative; top:-15px;" Visible="false"></asp:Label>
                                 </td>
                            </tr>
                        </table>
                    </p>
                    
                   
                    <p>
                      <label class="column-left" style="width:150px;" >Username : </label>
                        <asp:TextBox runat="server" ID="txt_Login" CssClass="text-input small-input " MaxLength="50"></asp:TextBox>
                    </p>
                    <p>
                      <label class="column-left" style="width:150px;" >Password : </label>
                        <asp:TextBox runat="server" ID="txt_Password" CssClass="text-input small-input " TextMode="Password" MaxLength="50"></asp:TextBox>
                    </p>
                    <p>
                      <label class="column-left" style="width:150px;" >Confirm : </label>
                        <asp:TextBox runat="server" ID="txt_Confirm" CssClass="text-input small-input " TextMode="Password" MaxLength="50"></asp:TextBox>
                    </p>
                    		
					<p>
                      <label class="column-left" style="width:150px;" >Available : </label>
                        &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
				      <!-- End .clear -->
                    </p>
				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                        
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset></asp:Panel>
                </div>
                <!-- End #tab1 -->
					
				
			  </div> <!-- End .content-box-content -->
				
		  </div>
		  
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>