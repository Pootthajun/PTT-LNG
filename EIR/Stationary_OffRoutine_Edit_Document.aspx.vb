﻿Imports System.Data
Imports System.Data.SqlClient
Imports EIR

Public Class Stationary_OffRoutine_Edit_Document
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Off_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Stationary_OffRoutine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_ST_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_ST_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_ST_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            BindTabData()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")

            '--------------Unvisible Select Template------------
            UC_SelectTemplate.CloseDialog()
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                '.RPT_Type = RPT_Type_ID
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub



    Private Sub BindTabData()
        ''------------------------------Header -----------------------------------
        'Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        'Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        'Dim DT As New DataTable
        'DA.Fill(DT)
        'If DT.Rows.Count = 0 Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
        '    Response.Redirect("Stationary_OffRoutine_Summary.aspx")
        '    Exit Sub
        'End If

        'lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        'lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        'lbl_Year.Text = RPT_Year
        ''------------------ GET TAG ID------------------------------
        'SQL = "SELECT TOP 1 TAG_ID FROM RPT_ST_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        'DA = New SqlDataAdapter(SQL, BL.ConnStr)
        'DT = New DataTable
        'DA.Fill(DT)
        'DA.Fill(DT)
        'If DT.Rows.Count = 0 Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
        '    Response.Redirect("Stationary_OffRoutine_Summary.aspx")
        '    Exit Sub
        'End If
        'TAG_ID = DT.Rows(0).Item("TAG_ID")
        'lbl_TAG.Text = BL.Get_Tag_Code_Stationary(TAG_ID)

        ''------------------ Bind Photo -----------------------------
        'SQL = " SELECT RPT_ST_Detail.DETAIL_ID,RPT_ST_Detail.INSP_ID,INSP_Name,STATUS_Name,RPT_ST_Detail.ICLS_ID,PROB_Detail,PROB_Recomment,PIC_Detail1 as P1,PIC_Detail2 as P2," & vbLf
        'SQL &= " CONVERT(image, '') as PIC_Detail1,CONVERT(image, '') as PIC_Detail2 FROM RPT_ST_Detail " & vbLf
        'SQL &= " INNER JOIN MS_ST_Default_Inspection INSP ON RPT_ST_Detail.INSP_ID=INSP.INSP_ID" & vbLf
        'SQL &= " INNER JOIN MS_ST_Default_Inspection_Status STAT ON RPT_ST_Detail.STATUS_ID=STAT.STATUS_ID" & vbLf
        'SQL &= " LEFT JOIN VW_Not_Require_Inspection_Picture EXC ON RPT_ST_Detail.INSP_ID=EXC.INSP_ID" & vbLf
        'SQL &= " WHERE RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No & " AND EXC.INSP_ID IS NULL " & vbLf
        'SQL &= " AND RPT_ST_Detail.ICLS_ID IS NOT NULL AND RPT_ST_Detail.STATUS_ID IS NOT NULL AND RPT_ST_Detail.ICLS_ID IS NOT NULL" & vbLf
        'DA = New SqlDataAdapter(SQL, BL.ConnStr)
        'DT = New DataTable
        'DA.Fill(DT)

        'For i As Integer = 0 To DT.Rows.Count - 1
        '    If DT.Rows(i).Item("P1") = True Then
        '        Dim Image As Byte() = BL.Get_ST_Image(DT.Rows(i).Item("DETAIL_ID"), 1)
        '        DT.Rows(i).Item("PIC_Detail1") = Image
        '    End If
        '    If DT.Rows(i).Item("P2") = True Then
        '        Dim Image As Byte() = BL.Get_ST_Image(DT.Rows(i).Item("DETAIL_ID"), 2)
        '        DT.Rows(i).Item("PIC_Detail2") = Image
        '    End If
        'Next

        'rptINSP.DataSource = DT
        'rptINSP.DataBind()

    End Sub

#Region "Navigator"

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click, btn_Back.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDocumentTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDocumentTemplate.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit_Document.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    'Protected Sub HTabDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDocument.Click, btn_Next.Click
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    'End Sub

    'Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit5.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    'End Sub
#End Region

    Protected Sub rptINSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptINSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Select Case e.CommandName
            Case "Delete"
                Dim dt As DataTable = BuiltDataGridview()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                rptINSP.DataSource = dt
                rptINSP.DataBind()

        End Select


        'Dim btnUpdate As Button = e.Item.FindControl("btnUpdate")
        'Dim Detail_ID As Integer = btnUpdate.CommandArgument

        'Select Case e.CommandName
        '    Case "Save" '----------------- Auto Save -----------------
        '        Dim txtDetail As TextBox = e.Item.FindControl("txtDetail")
        '        Dim txtRecomment As TextBox = e.Item.FindControl("txtRecomment")
        '        Dim SQL As String = "SELECT * FROM RPT_ST_Detail WHERE DETAIL_ID=" & e.CommandArgument
        '        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        '        Dim DT As New DataTable
        '        DA.Fill(DT)
        '        If DT.Rows.Count > 0 Then
        '            DT.Rows(0).Item("PROB_Detail") = txtDetail.Text
        '            DT.Rows(0).Item("PROB_Recomment") = txtRecomment.Text
        '            Dim cmb As New SqlCommandBuilder(DA)
        '            DA.Update(DT)
        '        End If
        '        ''--------------------- Update Picture --------------
        '        Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
        '        Dim imgRight As ImageButton = e.Item.FindControl("imgRight")
        '        imgLeft.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & Detail_ID & "&Image=1"
        '        imgRight.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & Detail_ID & "&Image=2"

        '    Case "Upload"
        '        '--------------- Open Image Dialog ----------------
        '        Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
        '        Dim imgRight As ImageButton = e.Item.FindControl("imgRight")
        '        Dim btnSaveImage1 As Button = e.Item.FindControl("btnSaveImage1")
        '        Dim btnSaveImage2 As Button = e.Item.FindControl("btnSaveImage2")

        '        Select Case e.CommandArgument
        '            Case "1"
        '                Session("PREVIEW_IMG_" & Detail_ID & "_1") = BL.Get_ST_Image(RPT_Year, RPT_No, Detail_ID, 1)
        '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & Detail_ID & "',1,document.getElementById('" & btnSaveImage1.ClientID & "'));", True)
        '            Case "2"
        '                Session("PREVIEW_IMG_" & Detail_ID & "_2") = BL.Get_ST_Image(RPT_Year, RPT_No, Detail_ID, 2)
        '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & Detail_ID & "',2,document.getElementById('" & btnSaveImage2.ClientID & "'));", True)
        '        End Select

        '    Case "SaveImage" '------------Occur when image has updated only --------------

        '        Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
        '        Dim imgRight As ImageButton = e.Item.FindControl("imgRight")

        '        If IsNothing(Session("TempImage_" & Detail_ID & "_" & e.CommandArgument)) Then Exit Sub
        '        '------------------------ Save ------------------------
        '        Dim SQL As String = "SELECT DETAIL_ID,PIC_ID,PIC_Detail" & e.CommandArgument & " FROM RPT_Picture WHERE DETAIL_ID=" & Detail_ID
        '        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        '        Dim DT As New DataTable
        '        DA.Fill(DT)
        '        If DT.Rows.Count > 0 Then
        '            DT.Rows(0).Item("PIC_Detail" & e.CommandArgument) = Session("TempImage_" & Detail_ID & "_" & e.CommandArgument)
        '            Dim cmb As New SqlCommandBuilder(DA)
        '            DA.Update(DT)
        '        End If
        '        If e.CommandArgument = 1 Then
        '            Session("PREVIEW_IMG_" & Detail_ID & "_1") = Session("TempImage_" & Detail_ID & "_1")
        '            imgLeft.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & Detail_ID & "&Image=1"
        '        Else
        '            Session("PREVIEW_IMG_" & Detail_ID & "_2") = Session("TempImage_" & Detail_ID & "_2")
        '            imgRight.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & Detail_ID & "&Image=2"
        '        End If

        'End Select
    End Sub



#Region "Toolbar"
    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM RPT_Picture WHERE DETAIL_ID IN (SELECT DETAIL_ID FROM RPT_ST_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & ")"
            .Connection = Conn
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        BindTabData()
    End Sub

    Protected Sub lnkRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub


#End Region

#Region "Section"
    Private Sub lnkAddSection_Click(sender As Object, e As EventArgs) Handles lnkAddSection.Click
        UC_SelectTemplate.ShowDialog()
    End Sub

    Private Sub UC_SelectTemplate_SelectTemplate(TemplateType As UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType) Handles UC_SelectTemplate.SelectTemplate
        Dim dt As DataTable = BuiltDataGridview()

        Dim dr As DataRow = dt.NewRow
        dr("OffRoutineTemplateType") = TemplateType.ToString
        dr("INSP_Name") = ""
        dr("STATUS_Name") = ""
        'dr("ICLS_ID") = ""
        'dr("PIC_Detail1") = ""
        dr("DETAIL_ID") = ""
        dr("PROB_Detail") = ""

        dt.Rows.Add(dr)

        rptINSP.DataSource = dt
        rptINSP.DataBind()
    End Sub

    Private Function BuiltDataGridview() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("INSP_Name")
        dt.Columns.Add("STATUS_Name")
        dt.Columns.Add("ICLS_ID")
        dt.Columns.Add("DETAIL_ID")
        dt.Columns.Add("OffRoutineTemplateType")
        dt.Columns.Add("PROB_Detail")
        dt.Columns.Add("PIC_Detail1", GetType(Byte()))

        For i As Integer = 0 To rptINSP.Items.Count - 1
            'Dim lblNo As Label = rptINSP.Items(i).FindControl("lblNo")
            Dim lblINSP As Label = rptINSP.Items(i).FindControl("lblINSP")
            Dim lblStatus As Label = rptINSP.Items(i).FindControl("lblStatus")
            Dim lblIclsID As Label = rptINSP.Items(i).FindControl("lblIclsID")
            Dim lblDetailID As Label = rptINSP.Items(i).FindControl("lblDetailID")
            Dim lblOffRoutineTemplateType As Label = rptINSP.Items(i).FindControl("lblOffRoutineTemplateType")
            Dim lblPROB_Detail As Label = rptINSP.Items(i).FindControl("lblPROB_Detail")

            Dim dr As DataRow = dt.NewRow
            dr("INSP_Name") = lblINSP.Text
            dr("STATUS_Name") = lblStatus.Text
            dr("ICLS_ID") = IIf(lblIclsID.Text.Trim = "", DBNull.Value, lblIclsID.Text)
            dr("DETAIL_ID") = lblDetailID.Text
            dr("OffRoutineTemplateType") = lblOffRoutineTemplateType.Text
            dr("PROB_Detail") = lblPROB_Detail.Text

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function
#End Region


    Protected Sub rptINSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptINSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub


        If Convert.IsDBNull(e.Item.DataItem("OffRoutineTemplateType")) = False Then
            Dim lblNo As Label = e.Item.FindControl("lblNo")
            Dim lblINSP As Label = e.Item.FindControl("lblINSP")
            Dim lblStatus As Label = e.Item.FindControl("lblStatus")
            Dim lblClass As Label = e.Item.FindControl("lblClass")
            Dim lblOffRoutineTemplateType As Label = e.Item.FindControl("lblOffRoutineTemplateType")
            Dim lblIclsID As Label = e.Item.FindControl("lblIclsID")
            Dim lblDetailID As Label = e.Item.FindControl("lblDetailID")
            Dim lblPROB_Detail As Label = e.Item.FindControl("lblPROB_Detail")

            lblNo.Text = e.Item.ItemIndex + 1
            lblINSP.Text = IIf(Convert.IsDBNull(e.Item.DataItem("INSP_Name")) = False, e.Item.DataItem("INSP_Name"), "")
            lblStatus.Text = IIf(Convert.IsDBNull(e.Item.DataItem("STATUS_Name")) = False, e.Item.DataItem("STATUS_Name"), "")
            lblOffRoutineTemplateType.Text = e.Item.DataItem("OffRoutineTemplateType")

            '------------------ Set Class ------------------
            If Convert.IsDBNull(e.Item.DataItem("ICLS_ID")) = False Then
                lblClass.Text = BL.Get_Problem_Level_Name(e.Item.DataItem("ICLS_ID"))
                Dim CssClass As String = BL.Get_Inspection_Css_Text_By_Level(e.Item.DataItem("ICLS_ID"))
                lblINSP.CssClass = CssClass
                lblStatus.CssClass = CssClass
                lblClass.CssClass = CssClass
                lblIclsID.Text = e.Item.DataItem("ICLS_ID")
            End If

            Select Case e.Item.DataItem("OffRoutineTemplateType")
                '-----------------------Template 1----------------------
                Case UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType.Template1.ToString
                    Dim UC As UC_StationaryOffRoutineTemplate1 = e.Item.FindControl("UC_StationaryOffRoutineTemplate1")
                    UC.Visible = True

                    If Not IsDBNull(e.Item.DataItem("PROB_Detail")) Then
                        UC.TextDetail.Text = e.Item.DataItem("PROB_Detail")
                    End If

                    '------------------ Set Image ------------------
                    If Not IsDBNull(e.Item.DataItem("PIC_Detail1")) Then
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = e.Item.DataItem("PIC_Detail1")
                        UC.ImageButtonImage.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DETAIL_ID") & "&Image=1"

                    Else
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = Nothing
                        UC.ImageButtonImage.ImageUrl = "resources/images/Sample_40.png"
                    End If
                    lblDetailID.Text = e.Item.DataItem("DETAIL_ID")
                '---------------------End Template 1----------------------


                '-----------------------Template 2----------------------
                Case UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType.Template2.ToString
                    Dim UC As UC_StationaryOffRoutineTemplate2 = e.Item.FindControl("UC_StationaryOffRoutineTemplate2")
                    UC.Visible = True

                    If Not IsDBNull(e.Item.DataItem("PROB_Detail")) Then
                        UC.TextDetail.Text = e.Item.DataItem("PROB_Detail")
                    End If

                    '------------------ Set Image ------------------
                    If Not IsDBNull(e.Item.DataItem("PIC_Detail1")) Then
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = e.Item.DataItem("PIC_Detail1")
                        UC.ImageButtonImage.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DETAIL_ID") & "&Image=1"

                    Else
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = Nothing
                        UC.ImageButtonImage.ImageUrl = "resources/images/Sample_40.png"
                    End If
                    lblDetailID.Text = e.Item.DataItem("DETAIL_ID")
                    '---------------------End Template 2----------------------

                '-----------------------Template 3----------------------
                Case UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType.Template3.ToString
                    Dim UC As UC_StationaryOffRoutineTemplate3 = e.Item.FindControl("UC_StationaryOffRoutineTemplate3")
                    UC.Visible = True

                    If Not IsDBNull(e.Item.DataItem("PROB_Detail")) Then
                        UC.TextDetail.Text = e.Item.DataItem("PROB_Detail")
                    End If

                    '------------------ Set Image ------------------
                    If Not IsDBNull(e.Item.DataItem("PIC_Detail1")) Then
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = e.Item.DataItem("PIC_Detail1")
                        UC.ImageButtonImage.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DETAIL_ID") & "&Image=1"

                    Else
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = Nothing
                        UC.ImageButtonImage.ImageUrl = "resources/images/Sample_40.png"
                    End If
                    lblDetailID.Text = e.Item.DataItem("DETAIL_ID")
                    '---------------------End Template 3----------------------

                '-----------------------Template 4----------------------
                Case UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType.Template4.ToString
                    Dim UC As UC_StationaryOffRoutineTemplate4 = e.Item.FindControl("UC_StationaryOffRoutineTemplate4")
                    UC.Visible = True

                    '------------------ Set Image ------------------
                    If Not IsDBNull(e.Item.DataItem("PIC_Detail1")) Then
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = e.Item.DataItem("PIC_Detail1")
                        UC.ImageButtonImage.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DETAIL_ID") & "&Image=1"

                    Else
                        Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = Nothing
                        UC.ImageButtonImage.ImageUrl = "resources/images/Sample_40.png"
                    End If
                    lblDetailID.Text = e.Item.DataItem("DETAIL_ID")
                    '---------------------End Template 3----------------------

                '-----------------------Template 5----------------------
                Case UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType.Template5.ToString
                    Dim UC As UC_StationaryOffRoutineTemplate5 = e.Item.FindControl("UC_StationaryOffRoutineTemplate5")
                    UC.Visible = True

                    If Not IsDBNull(e.Item.DataItem("PROB_Detail")) Then
                        UC.TextDetail.Text = e.Item.DataItem("PROB_Detail")
                    End If

                    '---------------------End Template 5----------------------

                '-----------------------Template 6----------------------
                Case UC_StationaryOffRoutineSelectTemplate.OffRoutineTemplateType.Template6.ToString
                    Dim UC As UC_StationaryOffRoutineTemplate6 = e.Item.FindControl("UC_StationaryOffRoutineTemplate6")
                    UC.Visible = True

                    '---------------------End Template 6----------------------
            End Select
        End If

        'Dim lblNo As Label = e.Item.FindControl("lblNo")
        'Dim lblINSP As Label = e.Item.FindControl("lblINSP")
        'Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        'Dim lblClass As Label = e.Item.FindControl("lblClass")

        'Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
        'Dim imgRight As ImageButton = e.Item.FindControl("imgRight")
        'Dim txtDetail As TextBox = e.Item.FindControl("txtDetail")
        'Dim txtRecomment As TextBox = e.Item.FindControl("txtRecomment")
        'Dim btnUpdate As Button = e.Item.FindControl("btnUpdate")


        'lblNo.Text = e.Item.ItemIndex + 1
        'lblINSP.Text = e.Item.DataItem("INSP_Name")
        'lblStatus.Text = e.Item.DataItem("STATUS_Name")
        'lblClass.Text = BL.Get_Problem_Level_Name(e.Item.DataItem("ICLS_ID"))

        'Dim CssClass As String = BL.Get_Inspection_Css_Text_By_Level(e.Item.DataItem("ICLS_ID"))
        'lblINSP.CssClass = CssClass
        'lblStatus.CssClass = CssClass
        'lblClass.CssClass = CssClass

        'If Not IsDBNull(e.Item.DataItem("PROB_Detail")) Then
        '    txtDetail.Text = e.Item.DataItem("PROB_Detail")
        'End If
        'If Not IsDBNull(e.Item.DataItem("PROB_Recomment")) Then
        '    txtRecomment.Text = e.Item.DataItem("PROB_Recomment")
        'End If

        ''------------------ Set Image ------------------
        'If Not IsDBNull(e.Item.DataItem("PIC_Detail1")) Then
        '    Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = e.Item.DataItem("PIC_Detail1")
        '    imgLeft.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DETAIL_ID") & "&Image=1"
        'Else
        '    Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_1") = Nothing
        '    imgLeft.ImageUrl = "resources/images/Sample_40.png"
        'End If
        'If Not IsDBNull(e.Item.DataItem("PIC_Detail2")) Then
        '    Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_2") = e.Item.DataItem("PIC_Detail2")
        '    imgRight.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DETAIL_ID") & "&Image=2"
        'Else
        '    Session("PREVIEW_IMG_" & e.Item.DataItem("DETAIL_ID") & "_2") = Nothing
        '    imgRight.ImageUrl = "resources/images/Sample_40.png"
        'End If

        ''--------------- Set Auto Update ---------------
        'txtDetail.Attributes("onchange") = "document.getElementById('" & btnUpdate.ClientID & "').click();"
        'txtRecomment.Attributes("onchange") = "document.getElementById('" & btnUpdate.ClientID & "').click();"

        'btnUpdate.CommandArgument = e.Item.DataItem("DETAIL_ID")
    End Sub

End Class