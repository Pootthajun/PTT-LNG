﻿Imports System.Data.SqlClient

Public Class MS_ST_Corrosion1
    Inherits System.Web.UI.UserControl
    Dim C As New Converter
    Dim BL As New EIR_BL

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property DETAIL_STEP_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_STEP_ID")) Then
                Return ViewState("DETAIL_STEP_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_STEP_ID") = value
        End Set
    End Property

    Private Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_TYPE_ID")) Then
                Return ViewState("TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_TYPE_ID") = value
        End Set
    End Property

    Public Property RPT_Date As String
        Get
            Return ViewState("RPT_Date")
        End Get
        Set(value As String)
            ViewState("RPT_Date") = Convert.ToDateTime(value).ToString("yyyy", New System.Globalization.CultureInfo("en-US"))
        End Set
    End Property

    Private Property TAG_UTM_ID_rpt() As Integer
        Get
            If IsNumeric(ViewState("TAG_UTM_ID_rpt")) Then
                Return ViewState("TAG_UTM_ID_rpt")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_UTM_ID_rpt") = value
        End Set
    End Property

    Private Property Last_Required_Thickness() As Object
        Get
            If Not IsNumeric(txt_S_MinThick.Text) Then
                Return Nothing
            Else
                Return CInt(txt_S_MinThick.Text)
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_S_MinThick.Text = ""
            Else
                txt_S_MinThick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Initial_Required_Thickness() As Object
        Get
            If Not IsNumeric(txt_l_MinThick.Text) Then
                Return Nothing
            Else
                Return CInt(txt_l_MinThick.Text)
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_l_MinThick.Text = ""
            Else
                txt_l_MinThick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Last_Measure_Year() As Object
        Get
            If Not IsNumeric(txt_s_LastYear.Text) Then
                Return Nothing
            Else
                Return CInt(txt_s_LastYear.Text)
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_s_LastYear.Text = ""
            Else
                txt_s_LastYear.Text = CInt(value)
            End If
        End Set
    End Property

    Private Property Last_Thickness() As Object
        Get
            If IsNumeric(txt_s_LastThick.Text) Then
                Return CDbl(txt_s_LastThick.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_s_LastThick.Text = ""
            Else
                txt_s_LastYear.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Initial_Thickness As Object
        Get
            If IsNumeric(txt_l_LastThick.Text) Then
                Return CDbl(txt_l_LastThick.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsNumeric(value) Then
                txt_l_LastThick.Text = ""
            Else
                txt_l_LastThick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Initial_Year As Object
        Get
            If IsNumeric(txt_l_LastYear.Text) Then
                Return CDbl(txt_l_LastYear.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsNumeric(value) Then
                txt_l_LastYear.Text = ""
            Else
                txt_l_LastYear.Text = CInt(value)
            End If
        End Set
    End Property

    Private Property Minimum_Actual_Thickness As Object
        Get
            If IsNumeric(txt_s_Thick.Text) Then
                Return CDbl(txt_s_Thick.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_s_Thick.Text = ""
                txt_l_Thick.Text = ""
            Else
                txt_s_Thick.Text = FormatNumericTextLimitPlace(value, True, 2)
                txt_l_Thick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property


    Private Property Diff_Year_Short() As Double
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Double)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property Diff_Year_Long() As Double
        Get
            If IsNumeric(ViewState("Diff_Year_Long")) Then
                Return ViewState("Diff_Year_Long")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Double)
            ViewState("Diff_Year_Long") = value
        End Set
    End Property

    Private Property Between_Tlast As Object
        Get
            If IsNumeric(txt_s_Year.Text) Then
                Return CInt(txt_s_Year.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_s_Year.Text = ""
            Else
                txt_s_Year.Text = CInt(value)
            End If
        End Set
    End Property

    Private Property Between_Tinitail As Object
        Get
            If IsNumeric(txt_l_Year.Text) Then
                Return CInt(txt_l_Year.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                txt_l_Year.Text = ""
            Else
                txt_l_Year.Text = CInt(value)
            End If
        End Set
    End Property


    Private Property Corrosion_rate_Short As Object
        Get
            If IsNumeric(lbl_s_Rate.Text) Then
                Return CDbl(lbl_s_Rate.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_Rate.Text = ""
            Else
                lbl_s_Rate.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Corrosion_rate_Long As Object
        Get
            If IsNumeric(lbl_l_Rate.Text) Then
                Return CDbl(lbl_l_Rate.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_l_Rate.Text = ""
            Else
                lbl_l_Rate.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property



    Private Property Remain_Life_Short As Object
        Get
            If IsNumeric(lbl_s_Remain.Text) Then
                Return CDbl(lbl_s_Remain.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_Remain.Text = ""
            Else
                lbl_s_Remain.Text = FormatNumber(value, 0)
            End If
        End Set
    End Property

    Private Property Remain_Life_Long As Object
        Get
            If IsNumeric(lbl_l_Remain.Text) Then
                Return CDbl(lbl_l_Remain.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_l_Remain.Text = ""
            Else
                lbl_l_Remain.Text = FormatNumber(value, 0)
            End If
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        RPT_Year = Request.QueryString("RPT_Year")
        RPT_No = Request.QueryString("RPT_No")
        DETAIL_STEP_ID = Request.QueryString("DETAIL_STEP_ID")

        'Dim SQL As String = ""
        'SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        'SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        'Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        'Dim DT As New DataTable
        'DA.Fill(DT)
        'If (DT.Rows.Count > 0) Then
        '    TAG_ID = DT.Rows(0).Item("TAG_ID")
        '    TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")

        'End If



    End Sub

    Public Sub ClearTextbox()
        ImplementJavaNumericText(txt_S_MinThick, "center")
        ImplementJavaNumericText(txt_l_MinThick, "center")

        ImplementJavaNumericText(txt_s_LastThick, "center")
        ImplementJavaNumericText(txt_l_LastThick, "center")
        ImplementJavaNumericText(txt_s_Thick, "center")
        ImplementJavaNumericText(txt_l_Thick, "center")

        ImplementJavaIntegerText(txt_s_LastYear, False,, "center")
        ImplementJavaIntegerText(txt_l_LastYear, False,, "center")

        ImplementJavaIntegerText(txt_s_Year, False,, "center")
        ImplementJavaIntegerText(txt_l_Year, False,, "center")




    End Sub


    Private Sub CalculateAll()

        '---วันที่ของใบตรวจ-----

        '------------ Calculate Diff Year--------------
        If Not IsNothing(Last_Measure_Year) And Not IsNothing(Diff_Year_Short) Then
            Diff_Year_Short = Last_Measure_Year - Between_Tlast
        Else
            Diff_Year_Short = Nothing
        End If

        If Not IsNothing(Initial_Year) And Not IsNothing(Between_Tinitail) Then
            Diff_Year_Long = Initial_Year - Between_Tinitail
        Else
            Diff_Year_Long = Nothing
        End If

        '-------------- Calculate For Short Term--------------
        If Not IsShortTermCalculationCompleted OrElse Diff_Year_Short = 0 Then
            Corrosion_rate_Short = Nothing
            Remain_Life_Short = Nothing
        Else
            Corrosion_rate_Short = (Last_Thickness - Minimum_Actual_Thickness) / Diff_Year_Short
            If Corrosion_rate_Short <= 0 Then Corrosion_rate_Short = 0
            Remain_Life_Short = (Minimum_Actual_Thickness - Last_Required_Thickness) / Corrosion_rate_Short
            If Remain_Life_Short <= 0 Then Remain_Life_Short = 0
        End If


        '-------------- Calculate For Long Term--------------
        If Not IsLongTermCalculationCompleted OrElse Diff_Year_Long = 0 Then
            Corrosion_rate_Long = Nothing
            Remain_Life_Long = Nothing
        Else
            Corrosion_rate_Long = (Initial_Thickness - Minimum_Actual_Thickness) / Diff_Year_Long
            If Corrosion_rate_Long <= 0 Then Corrosion_rate_Long = 0
            Remain_Life_Long = (Minimum_Actual_Thickness - Initial_Required_Thickness) / Corrosion_rate_Long
            If Remain_Life_Long <= 0 Then Remain_Life_Long = 0
        End If

    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        CalculateAll()


    End Sub

    Public ReadOnly Property IsShortTermCalculationCompleted As Boolean
        Get
            Return Not IsNothing(Last_Required_Thickness) And Not IsNothing(Last_Thickness) And Not IsNothing(Minimum_Actual_Thickness) And
                Not IsNothing(Last_Measure_Year) And Not IsNothing(Diff_Year_Short)
        End Get
    End Property

    Public ReadOnly Property IsLongTermCalculationCompleted As Boolean
        Get
            Return Not IsNothing(Initial_Required_Thickness) And Not IsNothing(Initial_Thickness) And Not IsNothing(Minimum_Actual_Thickness) And
                Not IsNothing(Initial_Year) And Not IsNothing(Diff_Year_Long)
        End Get
    End Property

    Public Sub SavrCorrasion_Rate()
        DETAIL_STEP_ID = Request.QueryString("DETAIL_STEP_ID")
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Corrosion_Remaining " & vbNewLine
        SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If (DT.Rows.Count = 0) Then
            DR = DT.NewRow
        Else
            DR = DT.Rows(0)
        End If
        '--Shot--
        DR("DETAIL_STEP_ID") = DETAIL_STEP_ID
        DR("Shot_Requied_thickness") = Last_Required_Thickness
        DR("Shot_Last_measurement_thickness_Tlast") = Last_Thickness
        DR("Shot_Minimum_actual_thickness_Tactual") = FormatNumericTextLimitPlace(txt_s_Thick.Text, True, 2)
        DR("Shot_Last_measurement_year") = Last_Measure_Year
        DR("Shot_T_Between_Tlast_Tactual") = Between_Tlast
        'DR("Shot_Corrosion_rate") = Corrosion_rate_Short
        'DR("Shot_Remaining_Life") = Remain_Life_Short

        '--Long--
        DR("Long_Requied_thickness") = Initial_Required_Thickness
        DR("Long_Initial_thickness_Tinitail") = Initial_Thickness
        DR("Long_Minimum_actual_thickness_Tactual") = FormatNumericTextLimitPlace(txt_l_Thick.Text, True, 2)
        DR("Long_Initial_year") = Initial_Year
        DR("Long_T_Between_Tinitail_Tactual") = Between_Tinitail
        'DR("Long_Corrosion_rate") = Corrosion_rate_Long
        'DR("Long_Remaining_Life") = Remain_Life_Long

        DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception

            Exit Sub
        End Try

    End Sub



    Public Sub BindCorrasion_Rate()

        DETAIL_STEP_ID = Request.QueryString("DETAIL_STEP_ID")
        '------1. ตรวจสอบการการ Insp
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Corrosion_Remaining " & vbNewLine
        SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If (DT.Rows.Count > 0) Then
            '--Shot-0-
            If Not IsDBNull(DT.Rows(0).Item("Shot_Requied_thickness")) Then
                txt_S_MinThick.Text = DT.Rows(0).Item("Shot_Requied_thickness")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Shot_Last_measurement_thickness_Tlast")) Then
                txt_s_LastThick.Text = DT.Rows(0).Item("Shot_Last_measurement_thickness_Tlast")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Shot_Minimum_actual_thickness_Tactual")) Then
                txt_s_Thick.Text = DT.Rows(0).Item("Shot_Minimum_actual_thickness_Tactual")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Shot_Last_measurement_year")) Then
                txt_s_LastYear.Text = DT.Rows(0).Item("Shot_Last_measurement_year")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Shot_T_Between_Tlast_Tactual")) Then
                txt_s_Year.Text = DT.Rows(0).Item("Shot_T_Between_Tlast_Tactual")
            End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Corrosion_rate")) Then
            '    lbl_s_Rate.Text = DT.Rows(0).Item("Shot_Corrosion_rate")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Remaining_Life")) Then
            '    lbl_s_Remain.Text = DT.Rows(0).Item("Shot_Remaining_Life")
            'End If

            '--Long--
            If Not IsDBNull(DT.Rows(0).Item("Long_Requied_thickness")) Then
                txt_l_MinThick.Text = DT.Rows(0).Item("Long_Requied_thickness")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Long_Initial_thickness_Tinitail")) Then
                txt_l_LastThick.Text = DT.Rows(0).Item("Long_Initial_thickness_Tinitail")
            End If

            If Not IsDBNull(DT.Rows(0).Item("Long_Minimum_actual_thickness_Tactual")) Then
                txt_l_Thick.Text = DT.Rows(0).Item("Long_Minimum_actual_thickness_Tactual")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Long_Initial_year")) Then
                txt_l_LastYear.Text = DT.Rows(0).Item("Long_Initial_year")
            End If
            If Not IsDBNull(DT.Rows(0).Item("Long_T_Between_Tinitail_Tactual")) Then
                txt_l_Year.Text = DT.Rows(0).Item("Long_T_Between_Tinitail_Tactual")
            End If
            '                            If Not IsDBNull(DT.Rows(0).Item("Long_Corrosion_rate")) Then
            '                                Corrosion_rate_Long = DT.Rows(0).Item("Long_Corrosion_rate")
            '                            End If
            'If Not IsDBNull(DT.Rows(0).Item("Long_Remaining_Life")) Then
            '    Remain_Life_Long = DT.Rows(0).Item("Long_Remaining_Life")
            'End If




            ''--Shot-0-
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Requied_thickness")) Then
            '    Last_Required_Thickness = DT.Rows(0).Item("Shot_Requied_thickness")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Last_measurement_thickness_Tlast")) Then
            '    Last_Thickness = DT.Rows(0).Item("Shot_Last_measurement_thickness_Tlast")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Minimum_actual_thickness_Tactual")) Then
            '    txt_s_Thick.Text = DT.Rows(0).Item("Shot_Minimum_actual_thickness_Tactual")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Last_measurement_year")) Then
            '    Last_Measure_Year = DT.Rows(0).Item("Shot_Last_measurement_year")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_T_Between_Tlast_Tactual")) Then
            '    Between_Tlast = DT.Rows(0).Item("Shot_T_Between_Tlast_Tactual")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Corrosion_rate")) Then
            '    Corrosion_rate_Short = DT.Rows(0).Item("Shot_Corrosion_rate")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Shot_Remaining_Life")) Then
            '    Remain_Life_Short = DT.Rows(0).Item("Shot_Remaining_Life")
            'End If

            ''--Long--
            'If Not IsDBNull(DT.Rows(0).Item("Long_Requied_thickness")) Then
            '    Initial_Required_Thickness = DT.Rows(0).Item("Long_Requied_thickness")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Long_Initial_thickness_Tinitail")) Then
            '    Initial_Thickness = DT.Rows(0).Item("Long_Initial_thickness_Tinitail")
            'End If

            'If Not IsDBNull(DT.Rows(0).Item("Long_Minimum_actual_thickness_Tactual")) Then
            '    txt_l_Thick.Text = DT.Rows(0).Item("Long_Minimum_actual_thickness_Tactual")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Long_Initial_year")) Then
            '    Initial_Year = DT.Rows(0).Item("Long_Initial_year")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Long_T_Between_Tinitail_Tactual")) Then
            '    Between_Tinitail = DT.Rows(0).Item("Long_T_Between_Tinitail_Tactual")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Long_Corrosion_rate")) Then
            '    Corrosion_rate_Long = DT.Rows(0).Item("Long_Corrosion_rate")
            'End If
            'If Not IsDBNull(DT.Rows(0).Item("Long_Remaining_Life")) Then
            '    Remain_Life_Long = DT.Rows(0).Item("Long_Remaining_Life")
            'End If


        Else
            ClearTextbox()

        End If


        CalculateAll()

    End Sub



    '--------------------
    Dim Inspection_Status As New DataTable


    Private Function GetItem_HeaderY(ByVal TAG_UTM_ID As Integer) As DataTable

        '--1 A-D
        '--2 A-H

        Dim SQL As String = ""
        SQL &= " SELECT Y_Template FROM MS_ST_TAG_UTM WHERE TAG_UTM_ID=" & TAG_UTM_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow

        '-- if ถ้าตารางเก็บข้อมูล ว่าง Gen ตาม Template--
        Dim _DT As New DataTable
        _DT.Columns.Add("Y_Template")
        If (DT.Rows.Count > 0) Then

            Select Case DT.Rows(0).Item("Y_Template")
            Case 1

                For Each letter As Char In Enumerable.Range(Convert.ToInt16("A"c), 4).Select(Function(i) Convert.ToChar(i))
                    DR = _DT.NewRow
                    DR("Y_Template") = letter.ToString.ToUpper()
                    _DT.Rows.Add(DR)
                Next
            Case 2
                For Each letter As Char In Enumerable.Range(Convert.ToInt16("A"c), 8).Select(Function(i) Convert.ToChar(i))
                    DR = _DT.NewRow
                    DR("Y_Template") = letter.ToString.ToUpper()
                    _DT.Rows.Add(DR)
                Next
        End Select

        End If
        Return _DT
    End Function

    Private Function GetItem_List(ByVal TAG_UTM_ID As Integer, ByVal Point_number As Integer) As DataTable

        '--1 A-D
        '--2 A-H 
        Dim DR As DataRow
        Dim SQL As String = ""
        SQL = ""
        SQL &= " SELECT * FROM RPT_ST_TA_CUI_POINT WHERE TAG_UTM_ID=" & TAG_UTM_ID & " AND DETAIL_STEP_ID=" & DETAIL_STEP_ID & " AND Point_number=" & Point_number
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim _DT As New DataTable
        _DT.Columns.Add("Y_Template")

        If (DT.Rows.Count > 0) Then
            '_DT = DT.Copy

            Dim DT_Header As DataTable = GetItem_HeaderY(TAG_UTM_ID_rpt)
            If (DT_Header.Rows.Count > 0) Then
                For h As Integer = 0 To DT_Header.Rows.Count - 1
                    DR = _DT.NewRow
                    DR("Y_Template") = DT.Rows(0).Item(DT_Header.Rows(h).Item("Y_Template").ToString())
                    _DT.Rows.Add(DR)
                Next
            End If




        Else

            SQL = ""
            SQL &= " SELECT Y_Template, Point_Code,Point_number  FROM MS_ST_TAG_UTM WHERE TAG_UTM_ID=" & TAG_UTM_ID
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)


            '-- if ถ้าตารางเก็บข้อมูล ว่าง Gen ตาม Template--

            If (DT.Rows.Count > 0) Then

                Select Case DT.Rows(0).Item("Y_Template")
                    Case 1

                        For Each letter As Char In Enumerable.Range(Convert.ToInt16("A"c), 4).Select(Function(i) Convert.ToChar(i))
                            DR = _DT.NewRow
                            'DR("Y_Template") = letter.ToString.ToUpper()
                            DR("Y_Template") = Nothing
                            _DT.Rows.Add(DR)
                        Next
                    Case 2
                        For Each letter As Char In Enumerable.Range(Convert.ToInt16("A"c), 8).Select(Function(i) Convert.ToChar(i))
                            DR = _DT.NewRow
                            DR("Y_Template") = Nothing
                            _DT.Rows.Add(DR)
                        Next
                End Select
            End If
        End If



        Return _DT
    End Function


    Private Function GetPositionList(ByVal TAG_UTM_ID As Integer) As DataTable
        Dim SQL As String = ""


        SQL = ""
        SQL &= " SELECT * FROM RPT_ST_TA_CUI_POINT WHERE TAG_UTM_ID=" & TAG_UTM_ID & " AND DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim _DT As New DataTable

        If (DT.Rows.Count > 0) Then
            _DT = DT.Copy

        Else

            SQL = ""
            SQL &= " SELECT Point_Code,Point_number FROM MS_ST_TAG_UTM WHERE TAG_UTM_ID=" & TAG_UTM_ID
            'Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            'Dim DT As New DataTable

            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable

            DA.Fill(DT)
            Dim DR As DataRow

            '-- if ถ้าตารางเก็บข้อมูล ว่าง Gen ตาม Template--
            _DT.Columns.Add("number")
            _DT.Columns.Add("Point_Code")
            _DT.Columns.Add("Position_Name")
            _DT.Columns.Add("Point_number")

            For i As Integer = 1 To DT.Rows(0).Item("Point_number")
                DR = _DT.NewRow
                DR("number") = i
                DR("Point_Code") = DT.Rows(0).Item("Point_Code").ToString()
                DR("Position_Name") = DT.Rows(0).Item("Point_Code").ToString() & "-" & i
                DR("Point_number") = i

                _DT.Rows.Add(DR)
            Next
        End If

        Return _DT
    End Function

    Public Sub Bind_rptLocationTag(ByVal _TAG_ID As Integer, ByVal _TAG_TYPE_ID As Integer)

        TAG_ID = _TAG_ID
        TAG_TYPE_ID = _TAG_TYPE_ID
        RPT_Year = Request.QueryString("RPT_Year")
        RPT_No = Request.QueryString("RPT_No")
        DETAIL_STEP_ID = Request.QueryString("DETAIL_STEP_ID")

        Dim SQL As String = ""
        SQL &= " SELECT * FROM MS_ST_TAG_UTM WHERE TAG_ID=" & _TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If (DT.Rows.Count > 0) Then
            rptLocationTag.DataSource = DT
            rptLocationTag.DataBind()
        End If



    End Sub

    Private Sub rptLocationTag_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptLocationTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblLocation_Name As Label = e.Item.FindControl("lblLocation_Name")

        lblLocation_Name.Text = e.Item.DataItem("Location_Name").ToString()
        lblLocation_Name.Attributes("TAG_UTM_ID") = e.Item.DataItem("TAG_UTM_ID")

        Dim rptItem_HeaderY As Repeater = e.Item.FindControl("rptItem_HeaderY")
        AddHandler rptItem_HeaderY.ItemDataBound, AddressOf rptItem_HeaderY_ItemDataBound
        Dim _DT As DataTable = GetItem_HeaderY(e.Item.DataItem("TAG_UTM_ID"))
        If (_DT.Rows.Count > 0) Then
            rptItem_HeaderY.DataSource = _DT
            rptItem_HeaderY.DataBind()
        End If

        Dim rptPositionList As Repeater = e.Item.FindControl("rptPositionList")
        AddHandler rptPositionList.ItemDataBound, AddressOf rptPositionList_ItemDataBound
        Dim DT As DataTable = GetPositionList(e.Item.DataItem("TAG_UTM_ID"))
        TAG_UTM_ID_rpt = e.Item.DataItem("TAG_UTM_ID")
        If (DT.Rows.Count > 0) Then
            rptPositionList.DataSource = DT
            rptPositionList.DataBind()
        End If




        '-------------Calculation Corrosion---------------------------
        Dim ddl_Treq As DropDownList = e.Item.FindControl("ddl_Treq")

        Dim rdoShort As RadioButton = e.Item.FindControl("rdoShort")
        Dim rdoLong As RadioButton = e.Item.FindControl("rdoLong")


        '--GetValue

        Get_Short_Term_Parameter(TAG_TYPE_ID, TAG_ID)

        Get_Long_Term_Parameter(TAG_TYPE_ID, TAG_ID)

        '--Shot--
        Dim lbl_s_LastThick As Label = e.Item.FindControl("lbl_s_LastThick")
        Dim lbl_s_LastYear As Label = e.Item.FindControl("lbl_s_LastYear")
        Dim lbl_s_Year As Label = e.Item.FindControl("lbl_s_Year")
        Dim lbl_s_Rate As Label = e.Item.FindControl("lbl_s_Rate")
        Dim lbl_s_Thick As Label = e.Item.FindControl("lbl_s_Thick")
        Dim lbl_S_ReqThick As Label = e.Item.FindControl("lbl_S_ReqThick")
        Dim lbl_s_Remain As Label = e.Item.FindControl("lbl_s_Remain")
        Dim lbl_s_LastNo As Label = e.Item.FindControl("lbl_s_LastNo")

        lbl_s_LastThick.Attributes("TAG_TYPE_ID") = TAG_TYPE_ID
        lbl_s_LastThick.Attributes("TAG_ID") = TAG_ID

        '--GetValue
        If Not IsNothing(Short_Tlast) Then
            lbl_s_LastThick.Text = FormatNumericTextLimitPlace(Short_Tlast, True, 2)
        End If

        If Not IsNothing(Long_INITIAL_YEAR) Then
            lbl_s_LastYear.Text = CInt(Long_INITIAL_YEAR)
        End If

        '--กรณีวัดครั้งแรก--
        If Not IsNothing(Long_Diff_Year) Then
            lbl_s_Year.Text = CInt(Long_Diff_Year)
        End If

        '--ใช้สูตร
        lbl_s_LastNo.Text = ""
        lbl_s_Rate.Text = ""
        '--ดึงจาก minimum ของตาราง UTM
        lbl_s_Thick.Text = ""


        '--Long--
        Dim lbl_l_LastThick As Label = e.Item.FindControl("lbl_l_LastThick")
        Dim lbl_l_LastYear As Label = e.Item.FindControl("lbl_l_LastYear")
        Dim lbl_l_Year As Label = e.Item.FindControl("lbl_l_Year")
        Dim lbl_l_Rate As Label = e.Item.FindControl("lbl_l_Rate")
        Dim lbl_l_Thick As Label = e.Item.FindControl("lbl_l_Thick")
        Dim lbl_l_ReqThick As Label = e.Item.FindControl("lbl_l_ReqThick")
        Dim lbl_l_Remain As Label = e.Item.FindControl("lbl_l_Remain")

        If Not IsNothing(Long_Initial_Thickness) Then
            lbl_l_LastThick.Text = FormatNumericTextLimitPlace(Long_Initial_Thickness, True, 2)
        End If

        If Not IsNothing(Long_INITIAL_YEAR) Then
            lbl_l_LastYear.Text = CInt(Long_INITIAL_YEAR)
        End If

        If Not IsNothing(Long_Diff_Year) Then
            lbl_l_Year.Text = CInt(Long_Diff_Year)
        End If

        '--ใช้สูตร
        lbl_l_Rate.Text = ""
        '--ดึงจาก minimum ของตาราง UTM
        lbl_l_Thick.Text = ""


        lbl_l_ReqThick.Attributes("Long_Norminal_Thickness") = Nothing
        lbl_S_ReqThick.Attributes("Short_Norminal_Thickness") = Nothing

        lbl_l_ReqThick.Attributes("Long_CORROSION_ALLOWANCE") = Nothing
        lbl_S_ReqThick.Attributes("Short_CORROSION_ALLOWANCE") = Nothing

        lbl_l_ReqThick.Attributes("Long_Calculated_Thickness") = Nothing
        lbl_S_ReqThick.Attributes("Short_Calculated_Thickness") = Nothing

        If Not IsNothing(Long_Norminal_Thickness) Then
            lbl_l_ReqThick.Attributes("Long_Norminal_Thickness") = (FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2))
        End If
        If Not IsNothing(Short_Norminal_Thickness) Then
            lbl_S_ReqThick.Attributes("Short_Norminal_Thickness") = (FormatNumericTextLimitPlace(Short_Norminal_Thickness, True, 2))
        End If

        If Not IsNothing(Long_CORROSION_ALLOWANCE) Then
            lbl_l_ReqThick.Attributes("Long_CORROSION_ALLOWANCE") = (FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
        End If
        If Not IsNothing(Short_CORROSION_ALLOWANCE) Then
            lbl_l_ReqThick.Attributes("Short_CORROSION_ALLOWANCE") = (FormatNumericTextLimitPlace(Short_CORROSION_ALLOWANCE, True, 2))
        End If

        If Not IsNothing(Long_Calculated_Thickness) Then
            lbl_l_ReqThick.Attributes("Long_Calculated_Thickness") = (FormatNumericTextLimitPlace(Long_Calculated_Thickness, True, 2))
        End If
        If Not IsNothing(Short_Calculated_Thickness) Then
            lbl_l_ReqThick.Attributes("Short_Calculated_Thickness") = (FormatNumericTextLimitPlace(Short_Calculated_Thickness, True, 2))
        End If

        Select Case ddl_Treq.SelectedValue
            Case 1
                '--(Norminal Thickness) - (Corrosion Allowance)
                If Not IsNothing(Long_Norminal_Thickness) And Not IsNothing(Long_CORROSION_ALLOWANCE) Then
                    lbl_l_ReqThick.Text = ((FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2)) - FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
                End If
                If Not IsNothing(Short_Norminal_Thickness) And Not IsNothing(Short_CORROSION_ALLOWANCE) Then
                    lbl_S_ReqThick.Text = ((FormatNumericTextLimitPlace(Short_Norminal_Thickness, True, 2)) - FormatNumericTextLimitPlace(Short_CORROSION_ALLOWANCE, True, 2))
                End If


            Case 2
                '--(Calculated Thickness) + (Corrosion Allowance)
                If Not IsNothing(Long_Norminal_Thickness) And Not IsNothing(Long_CORROSION_ALLOWANCE) Then
                    lbl_l_ReqThick.Text = ((FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2)) + FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
                End If
                If Not IsNothing(Short_Norminal_Thickness) And Not IsNothing(Short_CORROSION_ALLOWANCE) Then
                    lbl_S_ReqThick.Text = ((FormatNumericTextLimitPlace(Short_Norminal_Thickness, True, 2)) + FormatNumericTextLimitPlace(Short_CORROSION_ALLOWANCE, True, 2))
                End If

            Case 3
                '--(Calculated Thickness)
                If Not IsNothing(Long_Calculated_Thickness) Then
                    lbl_l_ReqThick.Text = FormatNumericTextLimitPlace(Long_Calculated_Thickness, True, 2)
                End If
                If Not IsNothing(Short_Calculated_Thickness) Then
                    lbl_S_ReqThick.Text = FormatNumericTextLimitPlace(Short_Calculated_Thickness, True, 2)
                End If
            Case Else


        End Select


    End Sub

    Private Sub rptLocationTag_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptLocationTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim Calculate As Button = e.Item.FindControl("Calculate")
        Dim lblLocation_Name As Label = e.Item.FindControl("lblLocation_Name")
        '--Shot--
        Dim lbl_s_LastThick As Label = e.Item.FindControl("lbl_s_LastThick")
        Dim lbl_s_LastYear As Label = e.Item.FindControl("lbl_s_LastYear")
        Dim lbl_s_Year As Label = e.Item.FindControl("lbl_s_Year")
        Dim lbl_s_Rate As Label = e.Item.FindControl("lbl_s_Rate")
        Dim lbl_s_Thick As Label = e.Item.FindControl("lbl_s_Thick")
        Dim lbl_S_ReqThick As Label = e.Item.FindControl("lbl_S_ReqThick")
        Dim lbl_s_Remain As Label = e.Item.FindControl("lbl_s_Remain")

        '--Long--
        Dim lbl_l_LastThick As Label = e.Item.FindControl("lbl_l_LastThick")
        Dim lbl_l_LastYear As Label = e.Item.FindControl("lbl_l_LastYear")
        Dim lbl_l_Year As Label = e.Item.FindControl("lbl_l_Year")
        Dim lbl_l_Rate As Label = e.Item.FindControl("lbl_l_Rate")
        Dim lbl_l_Thick As Label = e.Item.FindControl("lbl_l_Thick")
        Dim lbl_l_ReqThick As Label = e.Item.FindControl("lbl_l_ReqThick")
        Dim lbl_l_Remain As Label = e.Item.FindControl("lbl_l_Remain")

        Select Case e.CommandName
            Case "Calculate"
                'Dim DT As DataTable = GetCurrentData_rpt()

                '--------- Get Minimum Actual Thickness And Summary------------
                Dim DT As DataTable = GetCurrentData_rpt()
                DT.DefaultView.RowFilter = " TAG_UTM_ID =" & lblLocation_Name.Attributes("TAG_UTM_ID")
                DT = DT.DefaultView.ToTable()
                DT.Columns.RemoveAt(0)
                DT.Columns.RemoveAt(0)
                DT.Columns.RemoveAt(0)
                DT.Columns.RemoveAt(0)
                DT.Columns.RemoveAt(0)
                Dim MinTM As Double = Double.MaxValue

                For i As Integer = 0 To DT.Columns.Count - 1
                    Dim MIN As Object = DT.Compute("MIN(" & DT.Columns(i).ToString() & ")", "")
                    Dim lblMIN As Object
                    If Not IsDBNull(MIN) Then
                        lblMIN = FormatNumericTextLimitPlace(MIN, True, 2)
                        If MIN < MinTM Then MinTM = MIN
                    Else
                        lblMIN = Nothing
                    End If

                Next

                If MinTM < Double.MaxValue Then
                    Long_Minimum_Thickness = MinTM
                    lbl_l_Thick.Text = MinTM
                    lbl_s_Thick.Text = MinTM
                Else
                    lbl_l_Thick.Text = ""
                    lbl_s_Thick.Text = ""
                    Long_Minimum_Thickness = Nothing
                End If

                Get_Short_Term_Parameter(lbl_s_LastThick.Attributes("TAG_TYPE_ID"), lbl_s_LastThick.Attributes("TAG_ID"))
                Get_Long_Term_Parameter(lbl_s_LastThick.Attributes("TAG_TYPE_ID"), lbl_s_LastThick.Attributes("TAG_ID"))

                Dim ddl_Treq As DropDownList = e.Item.FindControl("ddl_Treq")
                Select Case ddl_Treq.SelectedValue
                    Case 1
                        '--(Norminal Thickness) - (Corrosion Allowance)
                        If Not IsNothing(Long_Norminal_Thickness) And Not IsNothing(Long_CORROSION_ALLOWANCE) Then
                            lbl_l_ReqThick.Text = ((FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2)) - FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
                            lbl_S_ReqThick.Text = ((FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2)) - FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
                        End If
                    Case 2
                        '--(Calculated Thickness) + (Corrosion Allowance)
                        If Not IsNothing(Long_Norminal_Thickness) And Not IsNothing(Long_CORROSION_ALLOWANCE) Then
                            lbl_l_ReqThick.Text = ((FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2)) + FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
                            lbl_S_ReqThick.Text = ((FormatNumericTextLimitPlace(Long_Norminal_Thickness, True, 2)) + FormatNumericTextLimitPlace(Long_CORROSION_ALLOWANCE, True, 2))
                        End If
                    Case 3
                        '--(Calculated Thickness)
                        If Not IsNothing(Long_Calculated_Thickness) Then
                            lbl_l_ReqThick.Text = FormatNumericTextLimitPlace(Long_Calculated_Thickness, True, 2)
                            lbl_S_ReqThick.Text = FormatNumericTextLimitPlace(Long_Calculated_Thickness, True, 2)
                        End If
                    Case Else
                End Select

                '---Short-----------------------------------------------------------------------------------------------
                '-------------- Calculate For Short Term--------------
                If Not (lbl_S_ReqThick.Text <> "" And Not IsNothing(Short_Norminal_Thickness) And lbl_s_Thick.Text <> "" And
                Not IsNothing(Short_LastYear) And Not IsNothing(Short_Diff_Year)) OrElse Short_Diff_Year = 0 Then

                    Corrosion_rate_Short = Nothing
                    Remain_Life_Short = Nothing
                Else
                    Corrosion_rate_Short = (Short_Tlast - Short_Minimum_Thickness) / Short_Diff_Year
                    If Corrosion_rate_Short <= 0 Then Corrosion_rate_Short = 0
                    Remain_Life_Short = (Short_Minimum_Thickness - (FormatNumericTextLimitPlace(lbl_S_ReqThick.Text, True, 2))) / Corrosion_rate_Short
                    If Remain_Life_Short <= 0 Then Remain_Life_Short = 0


                    lbl_s_Rate.Text = FormatNumericTextLimitPlace(Corrosion_rate_Short, True, 2)
                    lbl_s_Remain.Text = FormatNumericTextLimitPlace(Remain_Life_Short, True, 2)

                End If

                '---Long-----------------------------------------------------------------------------------------------
                '-------------- Calculate For Long Term--------------
                If Not (lbl_l_ReqThick.Text <> "" And Not IsNothing(Long_Norminal_Thickness) And lbl_l_Thick.Text <> "" And
                Not IsNothing(Long_INITIAL_YEAR) And Not IsNothing(Long_Diff_Year)) OrElse Long_Diff_Year = 0 Then

                    Corrosion_rate_Long = Nothing
                    Remain_Life_Long = Nothing
                Else
                    Corrosion_rate_Long = (Long_Initial_Thickness - Long_Minimum_Thickness) / Long_Diff_Year
                    If Corrosion_rate_Long <= 0 Then Corrosion_rate_Long = 0
                    Remain_Life_Long = (Long_Minimum_Thickness - (FormatNumericTextLimitPlace(lbl_l_ReqThick.Text, True, 2))) / Corrosion_rate_Long
                    If Remain_Life_Long <= 0 Then Remain_Life_Long = 0

                    lbl_l_Rate.Text = FormatNumericTextLimitPlace(Corrosion_rate_Long, True, 2)
                    lbl_l_Remain.Text = FormatNumericTextLimitPlace(Remain_Life_Long, True, 2)

                End If

        End Select

    End Sub

    Private Sub rptItem_HeaderY_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblHeader_Y As Label = e.Item.FindControl("lblHeader_Y")
        lblHeader_Y.Text = e.Item.DataItem("Y_Template")
    End Sub


    Private Sub rptPositionList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblPosition_Name As Label = e.Item.FindControl("lblPosition_Name")
        lblPosition_Name.Text = e.Item.DataItem("Position_Name")
        lblPosition_Name.Attributes("Point_Code") = e.Item.DataItem("Point_Code")
        lblPosition_Name.Attributes("Point_number") = e.Item.DataItem("Point_number")

        Dim rptItemY As Repeater = e.Item.FindControl("rptItemY")
        AddHandler rptItemY.ItemDataBound, AddressOf rptItemY_ItemDataBound
        Dim DT As DataTable = GetItem_List(TAG_UTM_ID_rpt, lblPosition_Name.Attributes("Point_number"))
        If (DT.Rows.Count > 0) Then
            rptItemY.DataSource = DT
            rptItemY.DataBind()
        End If

    End Sub

    Private Sub rptItemY_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        'Dim txtItem_Header_Y As TextBox = e.Item.FindControl("txtItem_Header_Y")

        Dim txtItem_Header_Y As TextBox = e.Item.FindControl("txtItem_Header_Y")


        If Not IsDBNull(e.Item.DataItem("Y_Template")) Then
                    txtItem_Header_Y.Text = FormatNumericTextLimitPlace(e.Item.DataItem("Y_Template"), True, 2)

                Else
                    txtItem_Header_Y.Text =Nothing 
                End If



    End Sub


    Function GetCurrentData_rpt() As DataTable


        Dim dt As New DataTable
        dt.Columns.Add("TAG_UTM_ID")
        dt.Columns.Add("Location_Name")
        dt.Columns.Add("Position_Code")
        dt.Columns.Add("Position_Name")
        dt.Columns.Add("Position_Number")

        Dim DT_Header As DataTable = GetItem_HeaderY(TAG_UTM_ID_rpt)
        If (DT_Header.Rows.Count > 0) Then
            For h As Integer = 0 To DT_Header.Rows.Count - 1
                dt.Columns.Add(DT_Header.Rows(h).Item("Y_Template"))
            Next
        End If


        Dim dr As DataRow
        For i As Integer = 0 To rptLocationTag.Items.Count - 1
            Dim lblLocation_Name As Label = rptLocationTag.Items(i).FindControl("lblLocation_Name")


            Dim rptPositionList As Repeater = rptLocationTag.Items(i).FindControl("rptPositionList")

            For j As Integer = 0 To rptPositionList.Items.Count - 1
                Dim lblPosition_Name As Label = rptPositionList.Items(j).FindControl("lblPosition_Name")


                dr = dt.NewRow

                dr("TAG_UTM_ID") = lblLocation_Name.Attributes("TAG_UTM_ID")
                dr("Location_Name") = lblLocation_Name.Text
                dr("Position_Code") = lblPosition_Name.Attributes("Point_Code")
                dr("Position_Name") = lblPosition_Name.Text
                dr("Position_Number") = lblPosition_Name.Attributes("Point_number")

                Dim rptItemY As Repeater = rptPositionList.Items(j).FindControl("rptItemY")
                For k As Integer = 0 To rptItemY.Items.Count - 1

                    If (DT_Header.Rows.Count > 0) Then
                        For h As Integer = 0 To DT_Header.Rows.Count - 1
                            Try
                                Dim txtItem_Header_Y As TextBox = rptItemY.Items(h).FindControl("txtItem_Header_Y")
                                dr(DT_Header.Rows(h).Item("Y_Template")) = FormatNumericTextLimitPlace(txtItem_Header_Y.Text, True, 2)

                            Catch ex As Exception
                                dr(DT_Header.Rows(h).Item("Y_Template")) = DBNull.Value
                            End Try

                        Next
                    End If
                Next

                dt.Rows.Add(dr)

            Next


        Next

        Return dt
    End Function


    Function GetCurrentData_Corosion_rpt() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("TAG_UTM_ID")

        dt.Columns.Add("Last_RPT_Year")
        dt.Columns.Add("Last_RPT_No")
        dt.Columns.Add("Last_Measure_Year")
        dt.Columns.Add("Last_Thickness")

        dt.Columns.Add("Initial_Year")
        dt.Columns.Add("Initial_Thickness")
        dt.Columns.Add("Min_Thickness")
        dt.Columns.Add("Required_Thickness_Mode")
        dt.Columns.Add("Use_Term")
        dt.Columns.Add("Norminal_Thickness")
        dt.Columns.Add("Corrosion_Allowance")
        dt.Columns.Add("Calculated_Thickness")
        dt.Columns.Add("Required_Thickness")
        dt.Columns.Add("Corrosion_Rate")
        dt.Columns.Add("Remain_Life")

        Dim dr As DataRow
        For i As Integer = 0 To rptLocationTag.Items.Count - 1
            Dim lblLocation_Name As Label = rptLocationTag.Items(i).FindControl("lblLocation_Name")
            dr = dt.NewRow
            dr("TAG_UTM_ID") = lblLocation_Name.Attributes("TAG_UTM_ID")
            'dr("Location_Name") = lblLocation_Name.Text

            '--Shot--
            Dim lbl_s_LastThick As Label = rptLocationTag.Items(i).FindControl("lbl_s_LastThick")
            Dim lbl_s_LastYear As Label = rptLocationTag.Items(i).FindControl("lbl_s_LastYear")
            Dim lbl_s_Year As Label = rptLocationTag.Items(i).FindControl("lbl_s_Year")
            Dim lbl_s_Rate As Label = rptLocationTag.Items(i).FindControl("lbl_s_Rate")
            Dim lbl_s_Thick As Label = rptLocationTag.Items(i).FindControl("lbl_s_Thick")
            Dim lbl_S_ReqThick As Label = rptLocationTag.Items(i).FindControl("lbl_S_ReqThick")
            Dim lbl_s_Remain As Label = rptLocationTag.Items(i).FindControl("lbl_s_Remain")
            Dim lbl_s_LastNo As Label = rptLocationTag.Items(i).FindControl("lbl_s_LastNo")

            '--Long--
            Dim lbl_l_LastThick As Label = rptLocationTag.Items(i).FindControl("lbl_l_LastThick")
            Dim lbl_l_LastYear As Label = rptLocationTag.Items(i).FindControl("lbl_l_LastYear")
            Dim lbl_l_Year As Label = rptLocationTag.Items(i).FindControl("lbl_l_Year")
            Dim lbl_l_Rate As Label = rptLocationTag.Items(i).FindControl("lbl_l_Rate")
            Dim lbl_l_Thick As Label = rptLocationTag.Items(i).FindControl("lbl_l_Thick")
            Dim lbl_l_ReqThick As Label = rptLocationTag.Items(i).FindControl("lbl_l_ReqThick")
            Dim lbl_l_Remain As Label = rptLocationTag.Items(i).FindControl("lbl_l_Remain")

            Dim ddl_Treq As DropDownList = rptLocationTag.Items(i).FindControl("ddl_Treq")

            Dim rdoShort As RadioButton = rptLocationTag.Items(i).FindControl("rdoShort")
            Dim rdoLong As RadioButton = rptLocationTag.Items(i).FindControl("rdoLong")

            If (lbl_s_LastYear.Text <> "") Then
                dr("Last_RPT_Year") = CInt(lbl_s_LastYear.Text)
            Else
                dr("Last_RPT_Year") = DBNull.Value
            End If
            If (lbl_s_LastNo.Text <> "") Then
                dr("Last_RPT_No") = CInt(lbl_s_LastNo.Text)
            Else
                dr("Last_RPT_No") = DBNull.Value
            End If
            If (lbl_s_LastYear.Text <> "") Then
                dr("Last_Measure_Year") = CInt(lbl_s_LastYear.Text)
            Else
                dr("Last_Measure_Year") = DBNull.Value
            End If
            If (lbl_s_LastThick.Text <> "") Then
                dr("Last_Thickness") = FormatNumericTextLimitPlace(lbl_s_LastThick.Text, True, 2)
            Else
                dr("Last_Thickness") = DBNull.Value
            End If

            '----------------

            If (lbl_l_LastYear.Text <> "") Then
                dr("Initial_Year") = FormatNumericTextLimitPlace(lbl_l_LastYear.Text, True, 2)
            Else
                dr("Initial_Year") = DBNull.Value
            End If
            If (lbl_l_LastThick.Text <> "") Then
                dr("Initial_Thickness") = FormatNumericTextLimitPlace(lbl_l_LastThick.Text, True, 2)
            Else
                dr("Initial_Thickness") = DBNull.Value
            End If
            If (lbl_s_Thick.Text <> "") Then
                dr("Min_Thickness") = FormatNumericTextLimitPlace(lbl_s_Thick.Text, True, 2)
            Else
                dr("Min_Thickness") = DBNull.Value
            End If

            If (ddl_Treq.SelectedValue <> "") Then
                dr("Required_Thickness_Mode") = ddl_Treq.SelectedValue
            Else
                dr("Required_Thickness_Mode") = DBNull.Value
            End If

            If (lbl_S_ReqThick.Text <> "") Then
                dr("Required_Thickness") = FormatNumericTextLimitPlace(lbl_S_ReqThick.Text, True, 2)
            Else
                dr("Required_Thickness") = DBNull.Value
            End If

            '-- ถ้าทำเครื่องหมายเลือก Short or Long ให้เก็บค่า
            If (rdoShort.Checked Or rdoLong.Checked) Then
                If (rdoShort.Checked) Then

                    dr("Use_Term") = 1
                    '---------------- Corrosion Rate,Remaining Life  ------------
                    If Not IsNothing(Corrosion_rate_Short) Then
                        dr("Corrosion_Rate") = Corrosion_rate_Short
                    Else
                        dr("Corrosion_Rate") = DBNull.Value
                    End If
                    If Not IsNothing(Remain_Life_Short) Then
                        dr("Remain_Life") = Remain_Life_Short
                    Else
                        dr("Remain_Life") = DBNull.Value
                    End If
                    '---------------------------------------------------------------

                    If Not IsNothing(lbl_l_ReqThick.Attributes("Long_Norminal_Thickness")) Then
                        dr("Norminal_Thickness") = FormatNumericTextLimitPlace(lbl_l_ReqThick.Attributes("Long_Norminal_Thickness"), True, 2)
                    Else
                        dr("Norminal_Thickness") = DBNull.Value
                    End If
                    If Not IsNothing(lbl_l_ReqThick.Attributes("Long_CORROSION_ALLOWANCE")) Then
                        dr("Corrosion_Allowance") = FormatNumericTextLimitPlace(lbl_l_ReqThick.Attributes("Long_CORROSION_ALLOWANCE"), True, 2)
                    Else
                        dr("Corrosion_Allowance") = DBNull.Value
                    End If
                    If Not IsNothing(lbl_l_ReqThick.Attributes("Long_Calculated_Thickness")) Then
                        dr("Calculated_Thickness") = FormatNumericTextLimitPlace(lbl_l_ReqThick.Attributes("Long_Calculated_Thickness"), True, 2)
                    Else
                        dr("Calculated_Thickness") = DBNull.Value
                    End If



                Else
                    dr("Use_Term") = 2
                    '---------------- Corrosion Rate,Remaining Life  ------------
                    If Not IsNothing(Corrosion_rate_Long) Then
                        dr("Corrosion_Rate") = Corrosion_rate_Long
                    Else
                        dr("Corrosion_Rate") = DBNull.Value
                    End If
                    If Not IsNothing(Remain_Life_Long) Then
                        dr("Remain_Life") = Remain_Life_Long
                    Else
                        dr("Remain_Life") = DBNull.Value
                    End If
                    '---------------------------------------------------------------
                    If Not IsNothing(lbl_l_ReqThick.Attributes("Short_Norminal_Thickness")) Then
                        dr("Norminal_Thickness") = FormatNumericTextLimitPlace(lbl_l_ReqThick.Attributes("Short_Norminal_Thickness"), True, 2)
                    Else
                        dr("Norminal_Thickness") = DBNull.Value
                    End If
                    If Not IsNothing(lbl_l_ReqThick.Attributes("Short_CORROSION_ALLOWANCE")) Then
                        dr("Corrosion_Allowance") = FormatNumericTextLimitPlace(lbl_l_ReqThick.Attributes("Short_CORROSION_ALLOWANCE"), True, 2)
                    Else
                        dr("Corrosion_Allowance") = DBNull.Value
                    End If
                    If Not IsNothing(lbl_l_ReqThick.Attributes("Short_Calculated_Thickness")) Then
                        dr("Calculated_Thickness") = FormatNumericTextLimitPlace(lbl_l_ReqThick.Attributes("Short_Calculated_Thickness"), True, 2)
                    Else
                        dr("Calculated_Thickness") = DBNull.Value
                    End If

                End If
            End If

            dt.Rows.Add(dr)


        Next

        Return dt
    End Function


#Region "Short Term"


    '--Last measurement thickness(Tlast):   การวัดครั้งแรกสุด
    Dim Short_Tlast As Object
    '--Last measurement year                   ปีครั้งที่แล้ว 
    Dim Short_LastYear As Integer
    'Time Between Tinitail And Tactual =    ปีที่ตรวจ - Initial Year
    'Minimum Actual Thickness(Tactual)      ค่า น้อยที่สุด จากการตรวจ
    Dim Short_Minimum_Thickness As Object
    '--Requied thickness =                    มีสูตร ต้องหา 2 ค่า  คือ Norminal_Thickness , CORROSION_ALLOWANCE
    Dim Field_Short_Norminal_Thickness As String = ""
    Dim Field_Short_CORROSION_ALLOWANCE As String = ""
    Dim Short_Norminal_Thickness As Object
    Dim Short_CORROSION_ALLOWANCE As Object
    Dim Short_Calculated_Thickness As Object
    Dim Short_Diff_Year As Object

    Private Sub Get_Short_Term_Parameter(ByVal _TAG_TYPE As Integer, ByVal _TAG_ID As Integer)

        Dim SQL As String = "SELECT INITIAL_YEAR,Norminal_Thickness,CORROSION_ALLOWANCE,Norminal_Thickness,Calculated_Thickness  "

        Select Case _TAG_TYPE
            Case EIR_BL.ST_TAG_TYPE.Absorber
                SQL &= " FROM MS_ST_ABSORBER_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Column
                SQL &= " FROM MS_ST_COLUMN_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Filter
                SQL &= " FROM MS_ST_FILTER_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Drum
                SQL &= " FROM MS_ST_DRUM_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager
                'SQL &= " FROM MS_ST_HEAT_EXCHNAGER_SPEC "

        End Select

        SQL &= " WHERE TAG_ID=" & _TAG_ID

        Dim DT As DataTable = New DataTable
        Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then
                Short_Tlast = DT.Rows(0).Item("Norminal_Thickness")
            Else
                Short_Tlast = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("INITIAL_YEAR")) Then
                Short_LastYear = DT.Rows(0).Item("INITIAL_YEAR")

                Short_Diff_Year = (RPT_Year - 543) - DT.Rows(0).Item("INITIAL_YEAR")
            Else
                Short_LastYear = Nothing
                Short_Diff_Year = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then
                Short_Norminal_Thickness = DT.Rows(0).Item("Norminal_Thickness")
            Else
                Short_Norminal_Thickness = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("CORROSION_ALLOWANCE")) Then
                Short_CORROSION_ALLOWANCE = DT.Rows(0).Item("CORROSION_ALLOWANCE")
            Else
                Short_CORROSION_ALLOWANCE = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then
                Short_Calculated_Thickness = DT.Rows(0).Item("Calculated_Thickness")
            Else
                Short_Calculated_Thickness = Nothing
            End If

            '--Required_Thickness -จาก ddl ที่เลือก

        End If

    End Sub




#End Region


#Region "Long Term"


    '--Initial thickness(Tinitail)    การวัดครั้งแรกสุด
    Dim Long_Initial_Thickness As Object
    '--Initial Year                   ปีแรกของอุปกรณ์
    Dim Field_Long_Initial_Year As String = ""
    Dim Long_INITIAL_YEAR As Integer
    'Time Between Tinitail And Tactual =    ปีที่ตรวจ - Initial Year
    'Dim Long_Time_Between_Tinitail_Tactual As Integer
    'Corrosion Rate() =
    'Minimum Actual Thickness(Tactual)      ค่า น้อยที่สุด จากการตรวจ
    Dim Long_Minimum_Thickness As Object
    '--Requied thickness =                    มีสูตร ต้องหา 2 ค่า  คือ Norminal_Thickness , CORROSION_ALLOWANCE
    Dim Field_Long_Norminal_Thickness As String = ""
    Dim Field_Long_CORROSION_ALLOWANCE As String = ""
    Dim Long_Norminal_Thickness As Object
    Dim Long_CORROSION_ALLOWANCE As Object
    Dim Long_Calculated_Thickness As Object
    Dim Required_Thickness As Object
    Dim Long_Diff_Year As Object
    'Remaining Life =
    Private Sub Get_Long_Term_Parameter(ByVal _TAG_TYPE As Integer, ByVal _TAG_ID As Integer)

        Dim SQL As String = "SELECT INITIAL_YEAR,Norminal_Thickness,CORROSION_ALLOWANCE,Norminal_Thickness,Calculated_Thickness  "

        Select Case _TAG_TYPE
            Case EIR_BL.ST_TAG_TYPE.Absorber
                SQL &= " FROM MS_ST_ABSORBER_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Column
                SQL &= " FROM MS_ST_COLUMN_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Filter
                SQL &= " FROM MS_ST_FILTER_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Drum
                SQL &= " FROM MS_ST_DRUM_SPEC "

            Case EIR_BL.ST_TAG_TYPE.Heat_Exchnager
                'SQL &= " FROM MS_ST_HEAT_EXCHNAGER_SPEC "

        End Select

        SQL &= " WHERE TAG_ID=" & _TAG_ID

        Dim DT As DataTable = New DataTable
        Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then
                Long_Initial_Thickness = DT.Rows(0).Item("Norminal_Thickness")
            Else
                Long_Initial_Thickness = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("INITIAL_YEAR")) Then
                Long_INITIAL_YEAR = DT.Rows(0).Item("INITIAL_YEAR")

                Long_Diff_Year = (RPT_Year - 543) - DT.Rows(0).Item("INITIAL_YEAR")
            Else
                Long_INITIAL_YEAR = Nothing
                Long_Diff_Year = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then
                Long_Norminal_Thickness = DT.Rows(0).Item("Norminal_Thickness")
            Else
                Long_Norminal_Thickness = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("CORROSION_ALLOWANCE")) Then
                Long_CORROSION_ALLOWANCE = DT.Rows(0).Item("CORROSION_ALLOWANCE")
            Else
                Long_CORROSION_ALLOWANCE = Nothing
            End If

            If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then
                Long_Calculated_Thickness = DT.Rows(0).Item("Calculated_Thickness")
            Else
                Long_Calculated_Thickness = Nothing
            End If

            '--Required_Thickness -จาก ddl ที่เลือก

        End If

    End Sub




#End Region






End Class