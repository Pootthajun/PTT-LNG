﻿Imports System.Drawing
Imports System.IO
Public Class GL_ImageEditor

    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Request.QueryString("UNIQUE_POPUP_ID")
            IMAGE_ID = Request.QueryString("IMAGE_ID")
            ViewState("ButtonRefreshControl") = Request.QueryString("btn")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID) = Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID)

            Try
                Dim B As Byte() = Session("TempImage_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID)
                Dim ST As New MemoryStream(B)
                Try
                    Dim IMG As Image = Image.FromStream(ST)
                    Dim W As Integer = IMG.Width
                    Dim H As Integer = IMG.Height
                    UpdateImage(W, H, B.Length)
                Catch ex As Exception
                End Try
                '-------------- Test Image------------                
            Catch ex As Exception
            End Try

            FUL_Image.Attributes("onchange") = "document.getElementById('" & btnUpload.ClientID & "').click();"
        End If

        lblValidation.Text = ""
    End Sub


#Region "Static Property"
    Public Property UNIQUE_POPUP_ID() As String
        Get

            Return ImgPreview.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ImgPreview.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property IMAGE_ID() As String
        Get
            Return ImgPreview.Attributes("IMAGE_ID")
        End Get
        Set(ByVal value As String)
            ImgPreview.Attributes("IMAGE_ID") = value
        End Set
    End Property
#End Region

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If Not FUL_Image.HasFile Then
            lblValidation.Text = "Please select file to update"
            Exit Sub
        End If

        If FUL_Image.PostedFile.ContentType.ToUpper.IndexOf("IMAGE") = -1 Then
            lblValidation.Text = "Allow only image to be uploaded!!"
            Exit Sub
        End If
        '----------------------- For Testing -------------------
        If UNIQUE_POPUP_ID = "" Then UNIQUE_POPUP_ID = Now.ToOADate
        '-----------------------Check Size-----------------------
        Dim ST As IO.Stream = FUL_Image.PostedFile.InputStream
        Dim C As New Converter
        Try
            Dim B As Byte() = C.StreamToByte(ST)

            '-------------- Test Image------------
            Dim IMG As Image = Image.FromStream(ST)
            Dim W As Integer = IMG.Width
            Dim H As Integer = IMG.Height
            Session("TempImage_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID) = B

            UpdateImage(W, H, B.Length)

        Catch ex As Exception
            lblValidation.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Private Sub UpdateImage(ByVal Width As Integer, ByVal Height As Integer, ByVal Size As Long)
        Dim ContainerRatio As Single = 550 / 500
        Dim ImageRatio = Width / Height

        txtImageWidth.Text = Width
        txtImageHeight.Text = Height

        If ContainerRatio > ImageRatio Then '------- Vertical
            If Height > 500 Then ' Over Size
                ImgPreview.Height = Unit.Pixel(500)
                ImgPreview.Width = Unit.Pixel(ImageRatio * 500)
            Else
                ImgPreview.Width = Unit.Pixel(Width)
                ImgPreview.Height = Unit.Pixel(Height)
            End If
        Else ' --------------------------------------Horizontal
            If Width > 550 Then ' Over Size
                ImgPreview.Width = Unit.Pixel(550)
                ImgPreview.Height = Unit.Pixel(550 / ImageRatio)
            Else
                ImgPreview.Width = Unit.Pixel(Width)
                ImgPreview.Height = Unit.Pixel(Height)
            End If
        End If
        ImgPreview.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&TEMP_ID=" & UNIQUE_POPUP_ID & "&Image=" & IMAGE_ID
        ImgPreview.Visible = True
    End Sub

    Protected Sub btnDraw_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDraw.Click
        If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID)) Then Exit Sub

        Dim RX As Double = CInt(txtImageWidth.Text) / ImgPreview.Width.Value
        Dim RY As Double = CInt(txtImageHeight.Text) / ImgPreview.Height.Value

        Dim PosX As Integer = e.X * RX
        Dim PosY As Integer = e.Y * RY

        lblTest.Text = PosX & " xxxx " & PosY

        Dim IMG As Image
        If e.X = 0 Or e.Y = 0 Then
            Exit Sub
        End If
        Dim B As Byte() = Session("TempImage_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID)
        Dim ST As New MemoryStream(B)
        Try
            IMG = Image.FromStream(ST)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            Exit Sub
        End Try

        Dim G As Graphics = Graphics.FromImage(IMG)
        Select Case txtMode.Text
            Case "Circle"
                Dim Radius As Integer = txt_CircleRadius_Value.Text
                Dim Rect As New Rectangle(PosX - Radius, PosY - Radius, Radius * 2, Radius * 2)
                Dim BS As New SolidBrush(CircleColor.ResultColor)
                Dim P As New Pen(BS)
                P.Width = 3 * (RX + RY) / 2

                Select Case ddl_CircleStyle.SelectedItem.Text
                    Case "Solid"
                        P.DashStyle = Drawing2D.DashStyle.Solid
                    Case "Dashed"
                        P.DashStyle = Drawing2D.DashStyle.Dash
                    Case "Dotted"
                        P.DashStyle = Drawing2D.DashStyle.Dot
                End Select
                G.DrawEllipse(P, Rect)

            Case "Box"

                Dim Radius As Integer = txt_BoxRadius_Value.Text
                Dim Rect As New Rectangle(PosX - Radius, PosY - Radius, Radius * 2, Radius * 2)
                Dim BS As New SolidBrush(BoxColor.ResultColor)
                Dim P As New Pen(BS)
                P.Width = 3 * (RX + RY) / 2

                Select Case ddl_BoxStyle.SelectedItem.Text
                    Case "Solid"
                        P.DashStyle = Drawing2D.DashStyle.Solid
                    Case "Dashed"
                        P.DashStyle = Drawing2D.DashStyle.Dash
                    Case "Dotted"
                        P.DashStyle = Drawing2D.DashStyle.Dot
                End Select
                G.DrawRectangle(P, Rect)

            Case "Text"

                Dim BS As SolidBrush
                If ddl_TextBackground.SelectedIndex = 1 Then ' ------- Fill BackGround ----------
                    Dim BoxWidth As Integer = CInt(txt_TextWidth_Value.Text)
                    Dim BoxHeight As Integer = CInt(txt_TextHeight_Value.Text)
                    Dim Rect As New Rectangle(PosX + 2, PosY + 2, BoxWidth / RX, BoxHeight / RY)
                    BS = New SolidBrush(TextBackColor.ResultColor)
                    G.FillRectangle(BS, Rect)
                End If
                Dim F As New Font(ddl_TextFont.Items(ddl_TextFont.SelectedIndex).Value, CInt(txt_FontSize_Value.Text), FontStyle.Regular, GraphicsUnit.Pixel)
                BS = New SolidBrush(TextForeColor.ResultColor)
                Dim Position As New Point(PosX + 2, PosY + 2)
                G.DrawString(txt_Message.Text, F, BS, Position)

        End Select

        Dim TheFile As String = Server.MapPath("Temp") & "\" & UNIQUE_POPUP_ID & "_" & Now.ToOADate.ToString.Replace(".", "")

        Dim myBitmap As Bitmap = New Bitmap(IMG)
        myBitmap.SetResolution(IMG.HorizontalResolution, IMG.VerticalResolution)
        Dim myImageCodecInfo As System.Drawing.Imaging.ImageCodecInfo = GetEncoderInfo("image/jpeg")

        Dim myEncoder As System.Drawing.Imaging.Encoder = System.Drawing.Imaging.Encoder.Compression

        Dim myEncoderParameters As System.Drawing.Imaging.EncoderParameters = New System.Drawing.Imaging.EncoderParameters(1)
        Dim myEncoderParameter As System.Drawing.Imaging.EncoderParameter = New System.Drawing.Imaging.EncoderParameter(myEncoder, CLng(System.Drawing.Imaging.EncoderValue.CompressionCCITT4))
        myEncoderParameters.Param(0) = myEncoderParameter
        'myBitmap.Save(TheFile, myImageCodecInfo, myEncoderParameters)
        myBitmap.Save(TheFile)

        Dim FT As FileStream = File.OpenRead(TheFile)
        Dim C As New Converter
        B = C.StreamToByte(FT)
        '-------------- Test Image------------
        Session("TempImage_" & UNIQUE_POPUP_ID & "_" & IMAGE_ID) = B

        UpdateImage(IMG.Width, IMG.Height, B.Length)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Close", "window.close();", True)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Close", "if(opener){opener.document.getElementById('" & ViewState("ButtonRefreshControl") & "').click(); opener.focus();} window.close();", True)
    End Sub

    Private Function GetEncoderInfo(ByVal mimeType As String) As System.Drawing.Imaging.ImageCodecInfo
        Dim encoders() As System.Drawing.Imaging.ImageCodecInfo = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders
        For j As Integer = 0 To encoders.Length - 1
            If encoders(j).MimeType.ToUpper = mimeType.ToUpper Then
                Return encoders(j)
            End If
        Next
        Return Nothing
    End Function

End Class