﻿Imports System.Data
Imports System.Data.SqlClient
Public Class THM_TYPE
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetRoute(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindType()

        Dim SQL As String = "SELECT " & vbNewLine
        SQL &= " THM_TYPE_ID," & vbNewLine
        SQL &= " THM_TYPE_Name," & vbNewLine
        SQL &= " Active_Status," & vbNewLine
        SQL &= " Update_By," & vbNewLine
        SQL &= " Update_Time" & vbNewLine
        SQL &= " FROM MS_THM_TYPE" & vbNewLine
        SQL &= " ORDER BY THM_TYPE_Name," & vbNewLine
        SQL &= " Active_Status" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("MS_THM_TYPE") = DT

        Navigation.SesssionSourceName = "MS_THM_TYPE"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptType
    End Sub

    Protected Sub rptRoute_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptType.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTypeName As Label = e.Item.FindControl("lblTypeName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTypeName.Text = e.Item.DataItem("THM_TYPE_Name")
        'btnEdit.CommandArgument = e.Item.DataItem("TM_TYPE_ID")
        'btnToggle.CommandArgument = e.Item.DataItem("TM_TYPE_ID")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))
        btnEdit.Attributes("THM_TYPE_ID") = e.Item.DataItem("THM_TYPE_ID")

    End Sub

    Protected Sub rptRoute_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptType.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Type_ID As Integer = btnEdit.Attributes("THM_TYPE_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                txtTypeName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListType.Enabled = False
                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_THM_TYPE WHERE THM_TYPE_ID=" & Type_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Equipement Type Not Found"
                    pnlBindingError.Visible = True
                    BindType()
                    Exit Sub
                End If

                txtTypeName.Text = DT.Rows(0).Item("THM_TYPE_Name")
                txtTypeName.Attributes("THM_TYPE_ID") = DT.Rows(0).Item("THM_TYPE_ID")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_THM_TYPE Set Active_Status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  THM_TYPE_ID=" & Type_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindType()
                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select


    End Sub

    Protected Sub ResetRoute(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindType()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListType.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtTypeName.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True
        pnlListType.Enabled = True
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        txtTypeName.Focus()
        lblUpdateMode.Text = "Create"
        '-----------------------------------
        pnlListType.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtTypeName.Text = "" Then
            lblValidation.Text = "Please insert Equipement Type Name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim TypeID As Integer = txtTypeName.Attributes("THM_TYPE_ID")

        Dim SQL As String = "SELECT * FROM MS_THM_TYPE WHERE THM_TYPE_Name='" & txtTypeName.Text & "' AND THM_TYPE_ID <> " & TypeID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Equipement Type Name is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_THM_TYPE WHERE THM_TYPE_ID=" & TypeID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TypeID = GetNewTypeID()
        Else
            DR = DT.Rows(0)
        End If

        DR("THM_TYPE_ID") = TypeID
        DR("THM_TYPE_Name") = txtTypeName.Text
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID") ' Remain
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        BindType()

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True
        ClearPanelEdit()
    End Sub

    Private Function GetNewTypeID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(THM_TYPE_ID),0)+1 FROM MS_THM_TYPE "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class