﻿Imports System.IO
Imports System.Drawing
Imports System.Data
Imports System.Xml

Public Class SVGAPI
    Inherits System.Web.UI.Page

    Dim SVG As New SVG_API
    Dim BL As New EIR_BL
    Dim C As New Converter

    Private ReadOnly Property MODE As String
        Get
            Try
                Return Request.QueryString("MODE").ToString
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public ReadOnly Property UNIQUE_POPUP_ID() As String
        Get
            Try
                Return Me.Request.QueryString("UNIQUE_POPUP_ID")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Select Case MODE
            Case "RENDER_TO_IMAGE"
                Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)
                Select Case Obj.Extension
                    Case FileAttachment.ExtensionType.TIFF, FileAttachment.ExtensionType.PNG,
                         FileAttachment.ExtensionType.JPEG, FileAttachment.ExtensionType.GIF
                        Dim DT As DataTable = BL.Get_FILE_EXTENSION_INFO()
                        DT.DefaultView.RowFilter = "EXT_ID=" & CInt(Obj.Extension)
                        If DT.DefaultView.Count > 0 Then
                            Response.AddHeader("Content-Type", DT.DefaultView(0).Item("Content_Type"))
                        End If
                        Response.BinaryWrite(Obj.Content)
                    Case FileAttachment.ExtensionType.SVG
                        Dim SVGContent As String = C.ByteToString(Obj.Content, Converter.EncodeType._UTF8)
                        Dim IMG As Image = SVG.SVGToImage(SVGContent)
                        Dim B As Byte() = C.ImageToByte(IMG)
                        Response.AddHeader("Content-Type", "image/png")
                        Response.BinaryWrite(B)
                    Case FileAttachment.ExtensionType.Unknow
                        Response.End()
                End Select

            Case "RENDER_FILE_FROM_SESSION"
                'Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)
                'Dim DT As DataTable = BL.Get_FILE_EXTENSION_INFO()
                'DT.DefaultView.RowFilter = "EXT_ID=" & CInt(Obj.Extension)
                'If DT.DefaultView.Count > 0 Then
                '    Response.AddHeader("Content-Type", DT.DefaultView(0).Item("Content_Type"))
                'End If
                'Response.Write(Str)


            Case "DELETE_TEMP_SVG"

                Dim Path As String = BL.ServerMapPath & "\Temp\" & UNIQUE_POPUP_ID & ".svg"
                If File.Exists(Path) Then
                    Try
                        DeleteFile(Path)
                        Response.Write(GenJSONCallback(MODE, UNIQUE_POPUP_ID, "Succcess", ""))
                    Catch ex As Exception
                        Response.Write(GenJSONCallback(MODE, UNIQUE_POPUP_ID, "Fail", ex.Message))
                    End Try
                Else
                    Response.Write(GenJSONCallback(MODE, UNIQUE_POPUP_ID, "Succcess", "File does not existed"))
                End If

            Case "STORE_SVG_TO_SESSION"

                Dim SVGContent As String = C.ByteToString(C.StreamToByte(Request.InputStream))
                'If Not IsNothing(Request.Form("Content")) Then
                '    SVGContent = Request.Form("Content")
                'End If
                Try
                    Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)
                    Obj.Extension = FileAttachment.ExtensionType.SVG
                    Obj.Content = C.StringToByte(SVGContent)
                    Response.Write(GenJSONCallback(MODE, UNIQUE_POPUP_ID, "Succcess", ""))
                Catch ex As Exception
                    Response.Write(GenJSONCallback(MODE, UNIQUE_POPUP_ID, "Fail", ex.Message))
                End Try

        End Select
    End Sub

    Private Function GenJSONCallback(ByVal Fn As String, ByVal UNIQUE_POPUP_ID As String, ByVal Status As String, ByVal reason As String) As String
        Dim json As String = "{ " & vbLf
        json &= "fn:'" & Fn.Replace("'", "\'") & "'" & vbLf
        json &= ",UNIQUE_POPUP_ID:'" & UNIQUE_POPUP_ID.Replace("'", "\'") & "'" & vbLf
        json &= ",status:'" & Status.Replace("'", "\'") & "'" & vbLf
        json &= ",reason:'" & reason.Replace("'", "\'") & "'" & vbLf
        json &= "}"
        Return json
    End Function
End Class


