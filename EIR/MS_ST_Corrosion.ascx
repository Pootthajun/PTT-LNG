<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_Corrosion.ascx.vb" Inherits="EIR.MS_ST_Corrosion1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>

<%@ Register src="GL_DialogInputValue.ascx" tagname="GL_DialogInputValue" tagprefix="uc1" %>
<link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    <title>PTT-EIR :: Electronic-Inspection-Report</title>
    <link rel="icon" type="image/ico" href="resources/images/icons/Logo.ico"></link>
    <link rel="shortcut icon" href="resources/images/icons/Logo.ico"></link>

    <!-- Reset Stylesheet -->
    <link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="resources/css/style.css" type="text/css" media="all" />

    <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
    <link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />

    <!-- Custom Script -->
    <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/PTTScript.js"></script>
    <script type="text/javascript" src="js/txtClientControl.js"></script>
    <script type="text/javascript" src="js/jquery.maphilight.js"></script>

    <!--CSS-->
    <link rel="stylesheet" href="resources/css/jquery-ui.css" type="text/css" media="screen" />
    <style type="text/css">
        .ChartHighligh {
            display: inline-block;
            float: left !important;
        }
    </style>

    <!-- Sidebar -->
    <link rel="stylesheet" href="resources/css/sidebar.css" type="text/css" media="screen" />

                


<asp:Repeater ID="rptLocationTag" runat="server">
    <ItemTemplate>
        <br />
        <h3><asp:Label ID="lblLocation_Name" runat="server"></asp:Label></h3>


                              
                       
                        <table  cellpadding="0" cellspacing="0"  class="propertyTable measurementGrid" >
                        <thead>
                        
                        <tr>                          
                            <th   style=" border :1px solid #CCCCCC; text-align:center;" >Position</th>
                            <asp:Repeater ID="rptItem_HeaderY" runat="server">
                                <ItemTemplate>
                                    <th   style="border :1px solid #CCCCCC; text-align:center;" ><asp:Label ID="lblHeader_Y" runat="server" Font-Size="12px" Font-Bold="false"></asp:Label></th>
                                </ItemTemplate>                            
                            </asp:Repeater>
                        </tr>
                        
                      </thead>
                    <asp:Repeater ID="rptPositionList" runat="server">
                       <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="lblPosition_Name" runat="server"></asp:Label></td>
                                
                                <asp:Repeater ID="rptItemY" runat="server">
                                    <ItemTemplate>
                                        <td><asp:TextBox ID="txtItem_Header_Y" runat="server"></asp:TextBox></td>
                                    </ItemTemplate>                                    
                                </asp:Repeater>
                                
                            </tr>
                       </ItemTemplate>
                       <FooterTemplate>
                           </table>
                       </FooterTemplate>
                      </asp:Repeater>                   

<br />

                <table cellpadding="0" cellspacing="0" class="propertyTable">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="6">
                           Corrosion Rate & Remaining Life Calculation by Require Thickness = 
                            <asp:DropDownList ID="ddl_Treq" runat="server" AutoPostBack="true" style="width:auto;">
                                <asp:ListItem Text="(Norminal Thickness) - (Corrosion Allowance)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness) + (Corrosion Allowance)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness)" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="propertyGroup" colspan="3">
                          <asp:RadioButton ID="rdoShort"  runat="server" GroupName="Term" AutoPostBack="true" Checked ="true"  /> Short Term
                        </td>
                        <td class="propertyGroup" colspan="3">
                           <asp:RadioButton ID="rdoLong"  runat="server" GroupName="Term" AutoPostBack="true" /> Long Term
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement thickness(Tlast)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;"><b>Initial thickness(Tinitail)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_LastYear" runat="server"  Font-Bold="true"></asp:Label><asp:Label ID="lbl_s_LastNo" runat="server"  Font-Bold="true" Visible ="false"  ></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="background-color:#f5f5f5;"><b>Initial year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastYear" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tlast and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tinitail and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_S_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center; width:7%;">mm.</td>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_l_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="width:7%; text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                </table>
<p align="right">
                <asp:Button ID="btnCalculate" runat="server" CommandName ="Calculate"  CssClass="button" Text="Calculate"  />


</p>


    </ItemTemplate>
</asp:Repeater>


<%--                <table cellpadding="0" cellspacing="0" class="propertyTable">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="6">
                           Corrosion Rate & Remaining Life Calculation  =
                            <asp:DropDownList ID="ddl_Treq" runat="server" AutoPostBack="true" style="width:auto;">
                                <asp:ListItem Text="(Norminal Thickness) - (Corrosion Allowance)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness) + (Corrosion Allowance)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness)" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="propertyGroup" colspan="3">
                          <asp:RadioButton ID="rdoShort"  runat="server" GroupName="Term" AutoPostBack="true" /> Short Term
                        </td>
                        <td class="propertyGroup" colspan="3">
                           <asp:RadioButton ID="rdoLong"  runat="server" GroupName="Term" AutoPostBack="true" /> Long Term
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement thickness(Tlast)</b>:</td>
                        <td style="text-align:center;"><asp:LinkButton ID="lbl_s_LastThick" runat="server"  Font-Bold="true"></asp:LinkButton></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;"><b>Initial thickness(Tinitail)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement year</b></td>
                        <td style="text-align:center;"><asp:LinkButton ID="lbl_s_LastYear" runat="server"  Font-Bold="true"></asp:LinkButton></td>
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="background-color:#f5f5f5;"><b>Initial year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastYear" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tlast and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tinitail and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_S_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center; width:7%;">mm.</td>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_l_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="width:7%; text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                </table>--%>


<table cellpadding="0" cellspacing="0" class="propertyTable" id="tbOld" runat ="server" visible ="false" >
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="6">
                           Corrosion Rate & Remaining Life Calculation
                        </td>                       
                    </tr>
                    <tr>
                        <td class="propertyGroup" colspan="3">
                           Short Term
                        </td>
                        <td class="propertyGroup" colspan="3">
                            Long Term
                        </td>
                    </tr>
                    <tr>
                        <td style="width:28%;">Requied thickness :</td>
                        <td style="text-align:center; width:15%;"><asp:TextBox ID="txt_S_MinThick" runat="server" MaxLength="5"></asp:TextBox></td>
                        <td style="text-align:center; width:7%;">mm.</td>
                        <td style="width:28%;">Requied thickness :</td>
                        <td style="text-align:center; width:15%;"><asp:TextBox ID="txt_l_MinThick" runat="server" MaxLength="5"></asp:TextBox></td>
                        <td style="width:7%; text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td><b>Last measurement thickness(Tlast)</b>:</td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_s_LastThick" runat="server" MaxLength="5" ></asp:TextBox> </td>
                        <td style="text-align:center;">mm.</td>
                        <td><b>Initial thickness(Tinitail)</b>:</td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_l_LastThick" runat="server" MaxLength="5" ></asp:TextBox></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td>Minimum actual thickness(Tactual):</td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_s_Thick" runat="server" MaxLength="5"></asp:TextBox></td>
                        <td style="text-align:center;">mm.</td>
                        <td>Minimum actual thickness(Tactual):</td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_l_Thick" runat="server" MaxLength="5"></asp:TextBox></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td><b>Last measurement year</b>:</td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_s_LastYear" runat="server" MaxLength="5"></asp:TextBox></td>
                        <td style="text-align:center;">&nbsp;</td>
                        <td><b>Initial year</b>:</td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_l_LastYear" runat="server" MaxLength="5"></asp:TextBox></td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Time Between <b>Tlast and Tactual</b>: </td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_s_Year" runat="server" MaxLength="5" ></asp:TextBox></td>
                        <td style="text-align:center;">year</td>
                        <td>Time Between <b>Tinitail and Tactual</b>: </td>
                        <td style="text-align:center;"><asp:TextBox ID="txt_l_Year" runat="server" MaxLength="5" ></asp:TextBox></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                    <tr>
                        <td>Corrosion rate =</td>
                        <td style="text-align:center;background-color:lemonchiffon;"><asp:Label ID="lbl_s_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                        <td>Corrosion rate =</td>
                        <td style="text-align:center;background-color:lemonchiffon;"><asp:Label ID="lbl_l_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                    </tr>
                    <tr>
                        <td>Remaining Life =</td>
                        <td style="text-align:center;background-color:lemonchiffon;"><asp:Label ID="lbl_s_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td>Remaining Life =</td>
                        <td style="text-align:center;background-color:lemonchiffon;"><asp:Label ID="lbl_l_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
   


     

                </table>
<asp:Panel ID="pnlbtn" runat ="server" Visible ="false" >
<p align="right">
                <asp:Button ID="btnCalculate" runat="server"  CssClass="button" Text="Calculation Corrosion Rate & Remaining"  />


</p>


</asp:Panel>