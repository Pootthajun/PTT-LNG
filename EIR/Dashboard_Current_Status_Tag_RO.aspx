﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Current_Status_Tag_RO.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Tag_RO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_Dashboard_DialogUploadImage.ascx" tagname="UC_Dashboard_DialogUploadImage" tagprefix="uc2" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="resources/css/DialogDashboard.css" type="text/css" media="screen"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<cc1:toolkitscriptmanager ID="ToolkitScriptManager1" runat="server" />
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

<div class="content-box">
				
				<div>
					<h3 style="margin-top:0"><asp:Label ID="lblTagCode" runat="server" style="padding-left:10px;"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTagName" runat="server"></asp:Label></h3>
				</div>
				
				<div class="content-box-content">
					<p style="font-weight:bold;">
					    <label class="column-left" style="width:120px;" >Report for: </label>
					    <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
					    <asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
					    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
					    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
					    | Period <asp:Label ID="lbl_Period" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
					</p>
					
					<table cellspacing="0" cellpadding="0" style="font-size:11px; border-bottom:None; border-top:None; background-color:White;">                                        
                        <tr style="border:None; font-size:12px; font-weight:bold;" height="30px">
                            <td bgcolor="#F3F3F3" style="text-align:center; width:100px;">Inspection</td>
                            <td bgcolor="#F3F3F3" style="text-align:center; width:100px;">Part</td>
                            <td bgcolor="#F3F3F3" style="text-align:center; width:80px;">Last status </td>
                            <td bgcolor="#F3F3F3" style="text-align:center; width:70px;">Fixed</td>
                            <td bgcolor="#F3F3F3" style="text-align:center; width:160px;" colspan="2">Current Status</td>
                            <td bgcolor="#F3F3F3" style="text-align:center; width:100px;">Trace</td>
                            <td bgcolor="#F3F3F3" style="text-align:center;">Detail</td>
                            <td bgcolor="#F3F3F3" style="text-align:center; width:70px;">Action</td>
                        </tr>
                        <asp:Repeater ID="rptInspection" runat="server">
                            <ItemTemplate>                                 
                                <tr style="border:None; font-size:11px;" height="25px">
                                    <td style="width:100px;" id="TDLevel" runat="server" >
                                        <asp:Label ID="lblInspection" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; width:100px;">
                                        <asp:Label ID="lblPart" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; width:80px;">
                                        <asp:Label ID="lblLastStatus" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; width:70px;">
                                        <asp:Label ID="lblFixed" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; width:80px;">
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align:center; width:80px;">
                                        <asp:Label ID="lblLevel" runat="server"></asp:Label>
                                    </td>
                                    <td style="width:100px;">
                                        <asp:Label ID="lblTrace" runat="server" Font-Italic="true" ></asp:Label>
                                    </td>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblDetail" runat="server" ></asp:Label>
                                    </td>
                                    <td style="width:70px; text-align:center;">
                                        <asp:ImageButton ID="btnView" CommandName="View" runat="server" ImageUrl="resources/images/icons/pencil.png"/>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater> 
                   </table>
				</div>
				 
		          
		          <uc2:UC_Dashboard_DialogUploadImage ID="UC_Dashboard_DialogUploadImage" runat="server" />
		          
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
			  </div>

</ContentTemplate>
</asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
