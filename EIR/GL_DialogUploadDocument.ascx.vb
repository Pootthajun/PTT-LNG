﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_DialogUploadDocument
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Public Event UpdateCompleted(ByRef sender As GL_DialogUploadDocument)


#Region "Dynamic Property"


    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_CODE() As String
        Get
            Return lblTAGCode.Text
        End Get
        Set(ByVal value As String)
            lblTAGCode.Text = value
        End Set
    End Property

    Public Property DOC_Name() As String
        Get
            Return txt_Name.Text
        End Get
        Set(ByVal value As String)
            txt_Name.Text = value
        End Set
    End Property

    Public Property DOC_Recomment() As String
        Get
            Return txt_Recomment.Text
        End Get
        Set(ByVal value As String)
            txt_Recomment.Text = value
        End Set
    End Property

    Public Property DOC_ID() As Integer
        Get
            Return Me.Attributes("DOC_ID")
        End Get
        Set(ByVal value As Integer)

            Session("TMP_Doc_" & UNIQUE_POPUP_ID) = Nothing
            Session("FileName_Doc_" & UNIQUE_POPUP_ID) = ""

            Dim DOC As EIR_BL.Document_Detail = BL.GetDocumentDetail(RPT_Year, RPT_No, value)
            If IsNothing(DOC) Then
                ImgPreview.ImageUrl = "resources/images/space.png"
                Me.Attributes("DOC_ID") = 0
                Session("Upload_Doc_" & UNIQUE_POPUP_ID) = Nothing
            Else
                Select Case DOC.DOC_Type
                    Case "application/pdf"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_pdf.png"
                    Case "application/vnd.ms-excel"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_xls.png"
                    Case "text/plain"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_txt.png"
                    Case "image/tiff"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_tif.png"
                    Case "image/jpeg"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_jpg.png"
                    Case "image/png", "image/x-png"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_png.png"
                    Case "application/msword"
                        ImgPreview.ImageUrl = "resources/images/icons/file_extension_doc.png"
                End Select
                Session("Upload_Doc_" & UNIQUE_POPUP_ID) = DOC
                Dim DT As DataTable = BL.GetAllDocumentByReport(RPT_Year, RPT_No)
                DT.DefaultView.RowFilter = "DOC_ID=" & value

                DOC_Name = DT.DefaultView(0).Item("DOC_Detail")
                DOC_Recomment = DT.DefaultView(0).Item("DOC_Recomment")

                Me.Attributes("DOC_ID") = value
            End If

        End Set
    End Property

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If IsNothing(Session("TMP_Doc_" & UNIQUE_POPUP_ID)) Then
                lblValidation.Text = "Allow only PDF,XLS,TXT,TIF,DOC,JPG,PNG to be uploaded!!"
                ImgPreview.ImageUrl = "resources/images/space.png"
                pnlValidation.Visible = True
                txt_Name.Text = ""
                Exit Sub
            End If
            Dim DOC As EIR_BL.Document_Detail = Session("TMP_Doc_" & UNIQUE_POPUP_ID)
            txt_Name.Text = Session("FileName_Doc_" & UNIQUE_POPUP_ID)
            Select Case DOC.DOC_Type
                Case "application/pdf"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_pdf.png"
                Case "application/vnd.ms-excel"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_xls.png"
                Case "text/plain"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_txt.png"
                Case "image/tiff"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_tif.png"
                Case "image/jpeg", "image/pjpeg"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_jpg.png"
                Case "image/png", "image/x-png"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_png.png"
                Case "application/msword" ',"application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    ImgPreview.ImageUrl = "resources/images/icons/file_extension_doc.png"
                Case Else
                    lblValidation.Text = "Allow only PDF,XLS,TXT,TIF,DOC,JPG,PNG to be uploaded!!"
                    ImgPreview.ImageUrl = "resources/images/space.png"
                    pnlValidation.Visible = True
                    txt_Name.Text = ""
                    Exit Sub
            End Select

            Session("Upload_Doc_" & UNIQUE_POPUP_ID) = DOC
            Session("TMP_Doc_" & UNIQUE_POPUP_ID) = ""
            Session("FileName_Doc_" & UNIQUE_POPUP_ID) = Nothing
        Catch ex As Exception
            lblValidation.Text = "Allow only PDF,XLS,TXT,TIF,DOC,JPG,PNG to be uploaded!!"
            ImgPreview.ImageUrl = Nothing
            pnlValidation.Visible = True
            txt_Name.Text = ""
        End Try

    End Sub

    Public Property Disabled() As Boolean
        Get
            Return Not btnUpdate.Visible
        End Get
        Set(ByVal value As Boolean)
            btnUpload.Visible = Not value
            ful1.Visible = Not value
            txt_Name.ReadOnly = value
            txt_Recomment.ReadOnly = value
            btnUpdate.Visible = Not value
        End Set
    End Property

#End Region

#Region "Static Property"
    Private Property Init_DOC_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("Init_DOC_ID")) Then
                Return 0
            Else
                Return Me.Attributes("Init_DOC_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("Init_DOC_ID") = value
        End Set
    End Property

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return Me.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            Me.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property
#End Region

    Protected Sub AsyncFileUpload1_UploadedComplete(ByVal sender As Object, ByVal e As AjaxControlToolkit.AsyncFileUploadEventArgs) Handles ful1.UploadedComplete
        Session("TMP_Doc_" & UNIQUE_POPUP_ID) = Nothing
        Session("FileName_Doc_" & UNIQUE_POPUP_ID) = Nothing

        Dim Result As New EIR_BL.Document_Detail
        Dim C As New Converter
        Try
            With Result
                .DOC_Type = ful1.PostedFile.ContentType
                .DOC_BIN = C.StreamToByte(ful1.PostedFile.InputStream)
            End With
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        Session("FileName_Doc_" & UNIQUE_POPUP_ID) = ful1.FileName
        Session("TMP_Doc_" & UNIQUE_POPUP_ID) = Result

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Public Sub CloseDialog()
        Disabled = False
        Me.Visible = False
    End Sub

    Public Sub ShowDialog(ByVal Init_RPT_Year As Integer, ByVal Init_RPT_No As Integer, Optional ByVal Init_TAG_CODE As String = "", Optional ByVal Init_DOC_ID As Integer = 0)
        DOC_Name = ""
        DOC_Recomment = ""
        txt_Name.Text = ""

        RPT_Year = Init_RPT_Year
        RPT_No = Init_RPT_No
        TAG_CODE = Init_TAG_CODE
        Me.Init_DOC_ID = Init_DOC_ID
        DOC_ID = Init_DOC_ID

        Session("Upload_Doc_" & UNIQUE_POPUP_ID) = Nothing

        Disabled = False
        Me.Visible = True

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If RPT_Year < 2000 Or RPT_No < 1 Then
            lblValidation.Text = "Lost required information!"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If IsNothing(Session("Upload_Doc_" & UNIQUE_POPUP_ID)) Then
            lblValidation.Text = "Select file to upload!"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If IsNothing(Session("TMP_Doc_" & UNIQUE_POPUP_ID)) Then
            lblValidation.Text = "Allow only PDF,XLS,TXT,TIF,DOC,JPG,PNG to be uploaded!!"
            ImgPreview.ImageUrl = "resources/images/space.png"
            pnlValidation.Visible = True
            txt_Name.Text = ""
            Exit Sub
        End If

        lblValidation.Text = "******** OK ********"
        pnlValidation.Visible = True
        Exit Sub

    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        lblValidation.Text = ""
        pnlValidation.Visible = False
    End Sub

End Class