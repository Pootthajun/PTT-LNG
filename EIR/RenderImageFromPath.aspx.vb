﻿Imports System.IO
Public Class RenderImageFromPath
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PathFile As String = Request.QueryString("PathFile")
        If PathFile = "" Then
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End If
        Try
            Dim C As New Converter
            Dim F As FileStream = File.Open(PathFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim IMG As Byte() = C.StreamToByte(F)
            F.Close()
            F.Dispose()
            Response.Clear()
            Response.BinaryWrite(IMG)
        Catch ex As Exception
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End Try
    End Sub

End Class