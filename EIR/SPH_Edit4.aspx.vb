﻿Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class SPH_Edit4
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property RPT_ID() As Integer
        Get
            If IsNumeric(ViewState("RPT_ID")) Then
                Return ViewState("RPT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_ID = Request.QueryString("RPT_ID")

            If RPT_ID = 0 Then
                Response.Redirect("SPH_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM SPH_RPT_Header WHERE RPT_ID=" & RPT_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='SPH_Summary.aspx'", True)
                    Exit Sub
                End If
                '---------------- Get RPT_Year + RPT_No
                RPT_Year = DT.Rows(0)("RPT_Year")
                RPT_No = DT.Rows(0)("RPT_No")
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE SPH_RPT_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE SPH_RPT_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE SPH_RPT_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindTabData()

        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_SPH_REPORT_HEADER WHERE RPT_ID=" & RPT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='SPH_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reason\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='SPH_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='SPH_Summary.aspx'", True)
            Exit Sub
        End If

        '---------------- Set Permission Visibility -------------
        If USER_LEVEL = EIR_BL.User_Level.Administrator Then
            btn_Send_Collector.Visible = True
            btn_Send_Inspector.Visible = False
            btn_Send_Analyst.Visible = True
            btn_Posted.Visible = True
        Else
            btn_Send_Collector.Visible = USER_LEVEL = EIR_BL.User_Level.Approver
            btn_Send_Inspector.Visible = False
            btn_Send_Analyst.Visible = USER_LEVEL = EIR_BL.User_Level.Collector
            btn_Posted.Visible = USER_LEVEL = EIR_BL.User_Level.Approver
        End If
        pnl_Inspector.Visible = False

        pnl_Collector.Enabled = USER_LEVEL = EIR_BL.User_Level.Collector
        pnl_Inspector.Enabled = USER_LEVEL = EIR_BL.User_Level.Inspector
        pnl_Analyst.Enabled = USER_LEVEL = EIR_BL.User_Level.Approver

        '----------------- Get Issue (Incomplete Form) -----------------
        SQL = "SELECT SUM(ISNULL(CASE WHEN (PIC_Detail1 IS NULL OR PIC_Detail2 IS NULL)  THEN 1 WHEN ISNULL(INSP_Level,'')='' THEN 1 ELSE 0 END,0)) Issue" & vbLf
        SQL &= " FROM SPH_RPT_Detail WHERE RPT_ID=" & RPT_ID
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("Issue")) AndAlso DT.Rows(0).Item("Issue") > 0 Then
            btn_Send_Collector.Enabled = False
            btn_Send_Inspector.Enabled = False
            btn_Send_Analyst.Enabled = False
            btn_Posted.Enabled = False
        End If

    End Sub

    Private Sub BindTabData()

        Dim SQL As String = "SELECT COUNT(1) FROM SPH_RPT_Detail WHERE RPT_ID=" & RPT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        lbl_Spring.Text = FormatNumber(DT.Rows(0)(0), 0)

        '------------------------------Header -----------------------------------
        SQL = "SELECT VW.* " & vbLf
        SQL &= " FROM VW_SPH_REPORT_HEADER VW " & vbLf
        SQL &= " WHERE RPT_ID=" & RPT_ID & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("SPH_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year

        '------------------------------Header -----------------------------------
        SQL = "  SELECT RPT_Result,  Header.RPT_COL_By,Header.RPT_COL_Date,Header.RPT_COL_Comment," & vbLf
        SQL &= " COL.User_Prefix+COL.User_Name+' '+COL.User_Surname COL_NAME, Header.RPT_INSP_By,Header.RPT_INSP_Date,Header.RPT_INSP_Comment," & vbLf
        SQL &= " INP.User_Prefix+INP.User_Name+' '+INP.User_Surname INSP_NAME, Header.RPT_ANL_By,Header.RPT_ANL_Date,Header.RPT_ANL_Comment," & vbLf
        SQL &= " ANL.User_Prefix+ANL.User_Name+' '+ANL.User_Surname ANL_NAME" & vbLf
        SQL &= " FROM SPH_RPT_Header Header  " & vbLf
        SQL &= " LEFT JOIN MS_User COL ON Header.RPT_COL_By=COL.USER_ID " & vbLf
        SQL &= " LEFT JOIN MS_User INP ON Header.RPT_INSP_By=INP.USER_ID " & vbLf
        SQL &= " LEFT JOIN MS_User ANL ON Header.RPT_ANL_By=ANL.USER_ID " & vbLf
        SQL &= " WHERE RPT_ID =" & RPT_ID & vbLf

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Exit Sub
        If Not IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            txt_RPT_Result.Text = DT.Rows(0).Item("RPT_Result")
        End If
        '--------- Collector --------
        If Not IsDBNull(DT.Rows(0).Item("COL_NAME")) Then
            txt_RPT_COL_By.Text = DT.Rows(0).Item("COL_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_COL_Date")) Then
            txt_RPT_COL_Date.Text = DT.Rows(0).Item("RPT_COL_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_COL_Comment")) Then
            txt_RPT_COL_Comment.Text = DT.Rows(0).Item("RPT_COL_Comment")
        End If
        txt_RPT_COL_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Collector

        '--------- Inspector --------
        If Not IsDBNull(DT.Rows(0).Item("INSP_NAME")) Then
            txt_RPT_INSP_By.Text = DT.Rows(0).Item("INSP_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_INSP_Date")) Then
            txt_RPT_INSP_Date.Text = DT.Rows(0).Item("RPT_INSP_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_INSP_Comment")) Then
            txt_RPT_INSP_Comment.Text = DT.Rows(0).Item("RPT_INSP_Comment")
        End If
        txt_RPT_INSP_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Inspector
        '--------- Approver --------
        If Not IsDBNull(DT.Rows(0).Item("ANL_NAME")) Then
            txt_RPT_ANL_By.Text = DT.Rows(0).Item("ANL_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_ANL_Date")) Then
            txt_RPT_ANL_Date.Text = DT.Rows(0).Item("RPT_ANL_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_ANL_Comment")) Then
            txt_RPT_ANL_Comment.Text = DT.Rows(0).Item("RPT_ANL_Comment")
        End If
        txt_RPT_ANL_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Approver

        '----------------- Update Summary TAG -------------------
        SQL = "SELECT " & vbLf
        SQL &= " COUNT(1) TotalTag," & vbLf
        SQL &= " SUM(CASE dbo.UDF_SPH_CalculateTravelStatus(Length_Cold,Length_Hot,INSP_Level) WHEN 'Accepted' THEN 1 WHEN 'Adjusted' THEN 0 ELSE 0 END) Accepted," & vbLf
        SQL &= " SUM(CASE dbo.UDF_SPH_CalculateTravelStatus(Length_Cold,Length_Hot,INSP_Level) WHEN 'Accepted' THEN 0 WHEN 'Adjusted' THEN 1 ELSE 0 END) Adjusted," & vbLf
        SQL &= " SUM(CASE dbo.UDF_SPH_CalculateTravelStatus(Length_Cold,Length_Hot,INSP_Level) WHEN 'Accepted' THEN 0 WHEN 'Adjusted' THEN 0 ELSE 1 END) Unknown" & vbLf
        SQL &= " FROM SPH_RPT_Detail Detail" & vbLf
        SQL &= " INNER JOIN SPH_MS_Spring Spring ON Detail.SPH_ID=Spring.SPH_ID" & vbLf
        SQL &= " WHERE Detail.RPT_ID = " & RPT_ID

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Exit Sub

        lblTotal.Text = FormatNumber(DT.Rows(0).Item("TotalTag"), 0)
        lblAccept.Text = FormatNumber(DT.Rows(0).Item("Accepted"), 0)
        lblAdjusted.Text = FormatNumber(DT.Rows(0).Item("Adjusted"), 0)
        lblUnknow.Text = FormatNumber(DT.Rows(0).Item("Unknown"), 0)

    End Sub

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click, lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Save_Summary(True)
    End Sub

#Region "Navigator"

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit1.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit2.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click, HTabPicture.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit3.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit4.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

#End Region

#Region "Saving"
    Private Sub Save_Summary(Optional ByVal ReportSuccess As Boolean = True)
        'If USER_LEVEL = EIR_BL.User_Level.Administrator Then Exit Sub

        Dim DT As New DataTable
        Dim SQL As String = "SELECT RPT_ID,RPT_Year,RPT_No,RPT_Result,RPT_COL_By,RPT_COL_Date,RPT_COL_Comment,RPT_INSP_By,RPT_INSP_Date,RPT_INSP_Comment,"
        SQL &= " RPT_ANL_By,RPT_ANL_Date,RPT_ANL_Comment,Update_By,Update_Time"
        SQL &= " FROM SPH_RPT_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save.\nThis report has been removed!'); window.location.href='SPH_Summary.aspx';", True)
            Exit Sub
        End If

        Dim NeedSave As Boolean = False
        '------------------- Save Result----------
        If IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_Result") = txt_RPT_Result.Text
        Else
            If DT.Rows(0).Item("RPT_Result") <> txt_RPT_Result.Text Then
                NeedSave = True
                DT.Rows(0).Item("RPT_Result") = txt_RPT_Result.Text
            End If
        End If
        '------------------- Save Collector----------
        If IsDBNull(DT.Rows(0).Item("RPT_COL_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_COL_Comment") = txt_RPT_COL_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_COL_Comment") <> txt_RPT_COL_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Collector Then
                NeedSave = True
                DT.Rows(0).Item("RPT_COL_Comment") = txt_RPT_COL_Comment.Text
            End If
        End If
        '------------------- Save Inspector----------
        If IsDBNull(DT.Rows(0).Item("RPT_INSP_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_INSP_Comment") = txt_RPT_INSP_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_INSP_Comment") <> txt_RPT_INSP_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Inspector Then
                NeedSave = True
                DT.Rows(0).Item("RPT_INSP_Comment") = txt_RPT_INSP_Comment.Text
            End If
        End If
        '------------------- Save Approver----------
        If IsDBNull(DT.Rows(0).Item("RPT_ANL_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_ANL_Comment") = txt_RPT_ANL_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_ANL_Comment") <> txt_RPT_ANL_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Approver Then
                NeedSave = True
                DT.Rows(0).Item("RPT_ANL_Comment") = txt_RPT_ANL_Comment.Text
            End If
        End If

        If Not NeedSave Then
            If ReportSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('There is nothing to be saved!\nThe report does not changed');", True)
            End If
            Exit Sub
        End If

        DT.Rows(0).Item("Update_By") = Session("USER_ID")
        DT.Rows(0).Item("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        If ReportSuccess Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save successfully');", True)
        End If

    End Sub
#End Region

#Region "WorkFlow"

    Protected Sub btn_Send_Collector_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Collector.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE SPH_RPT_Header set RPT_LOCK_BY=NULL,RPT_STEP=1,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_ID=" & RPT_ID
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Collector!!'); window.location.href='SPH_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Send_Inspector_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Inspector.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE SPH_RPT_Header set RPT_LOCK_BY=NULL,RPT_STEP=2,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_ID=" & RPT_ID
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Inspector!!'); window.location.href='SPH_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Send_Analyst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Analyst.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE SPH_RPT_Header set RPT_LOCK_BY=NULL,RPT_STEP=3,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_ID=" & RPT_ID
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Approver!!'); window.location.href='SPH_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Posted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Posted.Click

        Save_Summary(False)

        Dim DefaultValue As String = Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & (Now.Year + 543).ToString.Substring(2, 2) & "_" & lbl_Plant.Text.Replace("#", "").Replace("_", "") & "_SPRING_" & lbl_Route.Text.Replace("#", "").Replace("_", "") & "_"
        DefaultValue &= UCase(Session("USER_Name")) & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "-" & RPT_No.ToString.PadLeft(4, "0") & ".PDF"
        DialogInput.ShowDialog("Please insert finalize report file name..", DefaultValue)

    End Sub

    Protected Sub DialogInput_AnswerDialog(ByVal Result As String) Handles DialogInput.AnswerDialog

        If Result = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please insert file name to be saved');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If
        If Not BL.IsFormatFileName(Result) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('File name must not contained following excepted charactors /\:*?""<>|;');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) RPT_Code FROM SPH_RPT_Header WHERE Result_FileName='" & Replace(Result, "'", "''") & "' AND RPT_Year<>" & RPT_Year & " AND RPT_No<>" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This file name is already exists. And has been reserved for report " & DT.Rows(0).Item("RPT_Code") & "');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        '---------------- Generate Posted Report-----------
        If Result.Length >= 4 AndAlso Right(Result, 4).ToUpper <> ".PDF".ToUpper Then
            Result = Result & ".PDF"
        ElseIf Result.Length < 4 Then
            Result = Result & ".PDF"
        End If

        Dim DestinationPath As String = BL.PostedReport_Path & "\" & Result
        Dim GenerateResult As CreateReportResult = GenerateReport(DestinationPath)
        If Not GenerateResult.Success Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Unavailable to posted report !!'); alert('" & GenerateResult.Message.Replace("'", """") & "');", True)
            DialogInput.Visible = False
            Exit Sub
        End If
        DialogInput.Visible = False
        '--------------------- Update Report Status ---------------------------------
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE SPH_RPT_Header set RPT_LOCK_BY=NULL,RPT_STEP=4,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE(),Result_FileName='" & Replace(Result, "'", "''") & "'" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Report has been approved and job done!!'); ShowPreviewReport(" & RPT_Year & "," & RPT_No & "); window.location.href='SPH_Summary.aspx';", True)
    End Sub

#End Region

#Region "GenerateReport Copy From Hardcode in GL_Report"
    Structure CreateReportResult
        Public Success As Boolean
        Public Message As String
    End Structure

    Public Function GenerateReport(ByVal DestinationPath As String) As CreateReportResult
        Dim Result As New CreateReportResult
        Try
            Dim logonInfo As New TableLogOnInfo
            logonInfo.ConnectionInfo.ServerName = ConfigurationManager.AppSettings.Item("Crystal_Login_ServerName").ToString
            logonInfo.ConnectionInfo.DatabaseName = ConfigurationManager.AppSettings.Item("Crystal_Login_Database").ToString
            logonInfo.ConnectionInfo.UserID = ConfigurationManager.AppSettings.Item("Crystal_Login_User").ToString
            logonInfo.ConnectionInfo.Password = ConfigurationManager.AppSettings.Item("Crystal_Login_Password").ToString
            logonInfo.ConnectionInfo.IntegratedSecurity = False
            Dim cc As New ReportDocument()
            Dim PDFList As String() = {}
            Dim ReportName As String() = {"1", "2", "3"}
            For i As Integer = 0 To ReportName.Length - 1
                Dim FileName = Server.MapPath("Temp\" & Session.SessionID & "_" & ReportName(i) & ".pdf")
                cc.Load(Server.MapPath("Report\SpringHanger_Routine_Report\" & ReportName(i) & ".rpt"))
                cc.SetParameterValue("@ReportId", RPT_ID)
                cc.Database.Tables(0).ApplyLogOnInfo(logonInfo)
                cc.ExportToDisk(ExportFormatType.PortableDocFormat, FileName)
                BL.PushString(PDFList, FileName)
            Next

            Dim OutputFile As String = Server.MapPath("Temp1") & "\" & Session.SessionID & "_INSP_" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
            BL.MergePDF(PDFList, OutputFile)
            If IO.File.Exists(DestinationPath) Then IO.File.Delete(DestinationPath)
            IO.File.Copy(OutputFile, DestinationPath)
            IO.File.Delete(OutputFile)

            Result.Success = True
            Return Result

        Catch ex As Exception
            Result.Success = False
            Result.Message = ex.Message
            Return Result
        End Try

    End Function
#End Region

End Class