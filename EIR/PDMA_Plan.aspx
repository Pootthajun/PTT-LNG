﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PDMA_Plan.aspx.vb" Inherits="EIR.PDMA_Plan" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>

    <h2>PDMA Master Plan</h2>
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
					<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Year" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;"
                      ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
        		    <div class="clear"></div>
                  </div>
                                
                <div class="content-box-content">
                    
                     <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  
                   <asp:Panel ID="pnlListPlan" runat="server">
                  
                  <!-- This is the target div. id must match the href of this div's tab -->
                          <table>
                            <thead>
                              <tr>
                                <th><a href="#">Code</a></th>
                                <th><a href="#">Year</a></th>
                                <th><a href="#">Plant</a></th>
                                <th><a href="#">Route</a></th>
                                <th><a href="#">Tags</a></th>
                                <th><a href="#">Status</a></th>
                                <th><a href="#">Action</a></th>
                              </tr>
                            </thead>
                            <asp:Repeater ID="rptPlan" runat="server">
                                   <HeaderTemplate>
                                     <tbody>
                                     </HeaderTemplate>
                                      <ItemTemplate>
                                          <tr>
                                            <td><asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblYear" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblRoute" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblTags" runat="server"></asp:Label></td>
                                            <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                            <td>
                                                <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                                <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnToggle" ConfirmText="Are you sure to delete this report permanently?" />
                                            </td>
                                          </tr>
                                       </ItemTemplate>
                                          
                                       <FooterTemplate>
                                     </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                            <tfoot>
                              <tr>
                                <td colspan="5">
                                <div class="bulk-actions align-left">                             
                                    <asp:LinkButton ID="btnConstruct" runat="server" CssClass="button" Text="Construct plan"></asp:LinkButton>
                                </div>
                                    
                                </td>
                              </tr>
                            </tfoot>
                          </table>
                                 <uc1:PageNavigation ID="Navigation" runat="server" />
                                  <!-- End .pagination -->
                                  <div class="clear"></div>  
                   </asp:Panel>
                   
                   <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                   
                <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3>Construct PDMA Inspection Plan</h3>
                    <div class="clear"></div>
                  </div>
                    
                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>
                          </table>
                       <label class="column-left" style="width:50%;" > &nbsp; &nbsp;
                       Whole of year : 
                        <asp:DropDownList CssClass="select" ID="ddl_Edit_Year" runat="server" style="position:relative; top:3px;">
                      </asp:DropDownList> &nbsp; &nbsp;
                       </label>                        
				             <div class="clear"></div>
				      <!-- End .clear -->
                        <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <p align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Construct" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                        </p>
                         
                                               
                         
                         
                    </p>
                    </fieldset></asp:Panel>
                    
                </div>
                <!-- End #tab1 -->
                
    </div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
