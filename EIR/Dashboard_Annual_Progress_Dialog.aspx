﻿<%@ Page  Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Annual_Progress_Dialog.aspx.vb" Inherits="EIR.Dashboard_Annual_Progress_Dialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <table border="0" cellpadding="0" cellspacing="0" style="width:270px; border:1px solid #efefef;">
                 <tr>
                      <td colspan="2" style="text-align:center;">
                          <asp:Label ID="lblHeader" runat="server" Text="-"></asp:Label>
                      </td>
                 </tr>
                 <tr>
                      <td colspan="2" style="text-align:center;">
                          
                      </td>
                 </tr>
                  <tr>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:150px; border-bottom:1px solid #003366;">
                          Period End Date</td>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:150px; border-bottom:1px solid #003366;">
                          Actual Date</td>
                  </tr>
                  <asp:Repeater ID="rptData" runat="server">
                    <ItemTemplate>
                        <tr id="tbTag" runat="server" style="border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblPeriodEndDate" runat="server"></asp:Label></td>
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblActualDate" runat="server" ForeColor="Green"></asp:Label></td>
                        </tr>
                    </ItemTemplate>
                  </asp:Repeater>
                </table> 
    </div>
    </form>
</body>
</html>

