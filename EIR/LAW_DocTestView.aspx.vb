﻿Public Class LAW_DocTestView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("USER_Name") = "PID"
        UC_ImageEditor1.CloseDialog()
    End Sub

    Private Sub btnShowImageEditor_Click(sender As Object, e As EventArgs) Handles btnShowImageEditor.Click
        UC_ImageEditor1.ShowDialog()
    End Sub
End Class