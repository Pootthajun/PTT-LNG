﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Current_Status_Tag_THM.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Tag_THM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="resources/css/DialogDashboard.css" type="text/css" media="screen"/>

                <style type="text/css">
                    .tag_info {
                        /*border:none;
                        border:1px solid #efefef; */
                        /*padding-top:10px; 
                        padding-bottom:10px; 
                        
                        padding:10px 20px 10px 20px;*/
                    }

                        .tag_info tr td {
                        height:25px;
                        vertical-align:middle;                         
                        border:1px solid #efefef; 
                        padding:0 10px 0 10px;
                        }

                    .table_tag {
                        border:none;
                        padding: 3px 5px 3px 5px; 
                    }

                       .table_tag tr:hover td
                            {
                            background-color:Lavender;
                            border:none;
                            }
                            
                        .header2 {
                            text-align:center; 
                            background-color:darkblue; 
                            color:white;
                            font-weight:bold;
                            
                        }
                        .tagItem td {
                            color:#666666;
                            text-align:center;
                            border:none;
                        }
                        .tagItem .tag {
                            text-align:left;
                        }

                        .tagItem:hover td {
                            background-color:#ace4fa; 
                        }

       .btnDeselect {
            border-radius:2px;
            border:1px solid #eee;
            background-color:#eee;
            color:white;
            width:40px;
            cursor:pointer;
        }

        .btnNA {
            border-radius:2px;
            border:1px solid #ccc;
            background-color:darkgray;
            color:white;
            width:40px;
            cursor:pointer;
 
        }
        .btnOK {
            border-radius:2px;
            border:1px solid green;
            background-color:green;
            color:white;
            width:40px;
            cursor:pointer;
        }
        .btnAB{
            border-radius:2px;
            border:1px solid red;
            background-color:red;
            color:white;
            width:40px;
            cursor:pointer;
        }
                    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="tag_info" cellpadding="0" cellspacing="1">
            <tr>
                <td style="">
                    Report No
                </td>
                <td>
                    <asp:Label ID="lblReportNo" runat="server"></asp:Label>
                </td>              
            </tr>
            <tr>
                <td>
                    Equipement Type
                </td>
                <td>
                    <asp:Label ID="lblEquipement" runat="server"></asp:Label>
                </td>            
            </tr>
            <tr>
                <td>
                    Plant
                </td>
                <td>
                    <asp:Label ID="lblPlant" runat="server"></asp:Label>
                </td>            
            </tr>
            <tr id="TrRoute" runat="server" style="display:none;">
                <td>
                    Route
                </td>
                <td>
                    <asp:Label ID="lblRoute" runat="server"></asp:Label>
                </td>               
            </tr>
            <tr>
                <td>
                    Final Report
                </td>
                <td>
                    <asp:Label ID="lblResultFileName" runat="server" style="color:Navy; font-weight:bold; cursor:pointer;"></asp:Label>
                </td>            
            </tr>
            
            
            
        </table>

                      
        <table cellpadding="5" cellspacing="0" class="table_tag">
            <thead>
                <tr>
                    <th class="header2">Tag No</th>
                    <th class="header2">Tag Name</th>
                    <th class="header2">Status</th>
                </tr></thead><asp:Repeater ID="rptTag" runat="server">
                <ItemTemplate>
                    <tr class="tagItem" id="tr" runat="server">
                        <td class="tag"><asp:Label ID="lblTagCode" runat="server"></asp:Label></td>
                        <td class="tag"><asp:Label ID="lblTagName" runat="server"></asp:Label></td>
                        <td>
                            <input type="button" ID="btnNA" runat="server" value="NA" class="btnDeselect" />                           
                            <input type="button" ID="btnOK" runat="server" value="OK" class="btnDeselect"/>
                            <input type="button" ID="btnAB" runat="server" value="AB" class="btnDeselect"/>  
                        </td>                                  
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
           
    </div>
    </form>
</body>
</html>