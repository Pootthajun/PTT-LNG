﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Improvement_Report_Tag_PdMA
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            Dim PLANT_ID As Integer = Request.QueryString("PLANT_ID")
            Dim Month As Integer = Request.QueryString("MM")
            Dim Year As Integer = Request.QueryString("YY")
            Dim Improve As Integer = Request.QueryString("Improve")

            Dim Month_F As Integer = Request.QueryString("Month_F")
            Dim Month_T As Integer = Request.QueryString("Month_T")
            Dim Year_F As Integer = Request.QueryString("Year_F")
            Dim Year_T As Integer = Request.QueryString("Year_T")

            If Year > 2000 Then
                Year = Year - 543
            End If
            Dim Eqm As Integer = Request.QueryString("Eqm")

            lblBack.PostBackUrl = "Dashboard_Improvement_Report_Plant.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Eqm

            Dim Sql As String = ""
            Dim PlantName As String = ""
            Sql = "SELECT PLANT_NAME FROM MS_Plant WHERE PLANT_ID =" & PLANT_ID
            Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            PlantName = DT.Rows(0).Item("PLANT_NAME").ToString

            Sql = ""
            Sql &= "SELECT RPT_Type,RPT_Year,RPT_No,TAG_ID,TAG_CODE,Inspec_Date,PLANT_ID," & vbNewLine
            Sql &= "PWQ_ISSUE,INS_ISSUE,PWC_ISSUE,STA_ISSUE,ROT_ISSUE,AIR_ISSUE" & vbNewLine
            Sql &= "FROM VW_DASHBOARD_PDMA" & vbNewLine
            Sql &= "WHERE (PWQ_ISSUE = 3 OR INS_ISSUE = 3 OR PWC_ISSUE = 3 OR STA_ISSUE = 3 OR ROT_ISSUE = 3 OR AIR_ISSUE = 3) AND" & vbNewLine
            Sql &= "MONTH(Inspec_Date) = " & Month & " AND YEAR(Inspec_Date) = " & Year & " AND" & vbNewLine
            Sql &= "PLANT_ID = " & PLANT_ID & vbNewLine
            Sql &= "ORDER BY TAG_CODE" & vbNewLine

            DA = New SqlDataAdapter(Sql, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)

            lblHead.Text = "Found " & Improve & " PdMA & MTap Equipement Problem(S) Improved Completely <br> At " & PlantName & " on " & Dashboard.FindMonthNameEng(Month) & " " & Year

            DT.DefaultView.Sort = "RPT_Type DESC,RPT_Year,RPT_No,TAG_CODE"
            rptData.DataSource = DT
            rptData.DataBind()

        End If

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblPwq As Label = e.Item.FindControl("lblPwq")
        Dim lblIns As Label = e.Item.FindControl("lblIns")
        Dim lblPwc As Label = e.Item.FindControl("lblPwc")
        Dim lblSta As Label = e.Item.FindControl("lblSta")
        Dim lblRot As Label = e.Item.FindControl("lblRot")
        Dim lblAir As Label = e.Item.FindControl("lblAir")

        lblTag.Text = e.Item.DataItem("TAG_CODE")
        lblType.Text = e.Item.DataItem("RPT_Type")
        If e.Item.DataItem("PWQ_ISSUE").ToString = "3" Then
            lblPwq.Text = "Fix"
        End If
        If e.Item.DataItem("INS_ISSUE").ToString = "3" Then
            lblIns.Text = "Fix"
        End If
        If e.Item.DataItem("PWC_ISSUE").ToString = "3" Then
            lblPwc.Text = "Fix"
        End If
        If e.Item.DataItem("STA_ISSUE").ToString = "3" Then
            lblSta.Text = "Fix"
        End If
        If e.Item.DataItem("ROT_ISSUE").ToString = "3" Then
            lblRot.Text = "Fix"
        End If
        If e.Item.DataItem("AIR_ISSUE").ToString = "3" Then
            lblAir.Text = "Fix"
        End If

        If lblType.Text.ToUpper.Trim = "PDMA" Then
            tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PDMA.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        Else
            tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PDMA.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If

    End Sub

End Class