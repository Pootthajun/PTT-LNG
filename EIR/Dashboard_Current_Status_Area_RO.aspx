﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Current_Status_Area_RO.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Area_RO" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2>Rotating Current Tag Status for <asp:LinkButton ID="lblPlant" runat="server"></asp:LinkButton>
				Area <asp:LinkButton ID="lblArea" runat="server"></asp:LinkButton></h2>
			
			


        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td colspan="3"><h3 style="color: #0066CC;">Chart Type 
				    &nbsp;<asp:DropDownList ID="ddl_ChartType" runat="server" AutoPostBack="true" >
				<asp:ListItem Value="Pie" Selected="True"></asp:ListItem>
				<asp:ListItem Value="Doughnut"></asp:ListItem>				
				<asp:ListItem Value="Funnel"></asp:ListItem>
				<asp:ListItem Value="Pyramid"></asp:ListItem>
                </asp:DropDownList>
                                </h3> 
                <asp:LinkButton ID="lblBack" runat="server" Tooltip="Back to see all areas in this plant" Text="Back to see all areas in this plant"></asp:LinkButton>              
                                </td>
			  </tr>
			  <tr>
				<td width="500" style="vertical-align:top; width: 0%;">
				<asp:Chart ID="ChartMain" runat="server" Width="500px" Height="400px" CssClass="ChartHighligh" >
                    <legends>
                        <asp:Legend Name="Legend1" DockedToChartArea="ChartArea1">
                        </asp:Legend>
                    </legends>
                    <titles>
                        <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" 
                            Text="All tag(s) in this area">
                        </asp:Title>
                        <asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" 
                            Font="Microsoft Sans Serif, 9.75pt">
                        </asp:Title>
                    </titles>
                    <Series>
                        <asp:Series Name="Series1" ChartType="Pie" Palette="Bright" ShadowColor="" 
                            Legend="Legend1" Font="Microsoft Sans Serif, 8.25pt, style=Bold">
                            <points>
                                <asp:DataPoint AxisLabel="" BackGradientStyle="None" BackSecondaryColor="" 
                                    Color="Green" Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Normal" 
                                    MapAreaAttributes="" ToolTip="" Url="" YValues="10" />
                                <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" Color="Yellow" 
                                    Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Class C" MapAreaAttributes="" 
                                    ToolTip="" Url="" YValues="20" />
                                <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" 
                                    Color="255, 128, 0" Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Class B" 
                                    MapAreaAttributes="" ToolTip="" Url="" YValues="30" />
                                <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" Color="Red" 
                                    Font="Tahoma, 8.25pt" Label="#VAL" LabelForeColor="Maroon" LegendText="Class A" 
                                    MapAreaAttributes="" ToolTip="" Url="" YValues="40" />
                            </points>
                            <emptypointstyle isvisibleinlegend="False" />
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                    </asp:Chart>
                    &nbsp;</td>
                <td style="width:200px;"></td>
			      <td style="vertical-align:top;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Tag</td>                               
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Current</td>
                              
                          </tr>
                          <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblTag" runat="server"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblLevel" runat="server" CssClass="TextNormal"></asp:Label></td>                              
                                </tr>
                            </ItemTemplate>
                          </asp:Repeater>
                          
                      </table>
                  </td>
		      </tr>
			</table>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>