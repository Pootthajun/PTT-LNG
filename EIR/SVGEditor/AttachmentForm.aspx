﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AttachmentForm.aspx.vb" Inherits="EIR.AttachmentForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UpdateProgress.ascx" TagName="UpdateProgress" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>PTT-EIR :: File Canvas Editor</title>

<link rel="icon" type="image/png" href="images/favicon.ico"/>
<link rel="stylesheet" href="../resources/css/jquery-ui.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/TITCustom.css" type="text/css" media="all" />

<!-- Reset Stylesheet -->
<link rel="stylesheet" href="../resources/css/reset.css" type="text/css" media="screen" />

<!-- Main Stylesheet -->
<link rel="stylesheet" href="../resources/css/style.css" type="text/css" media="all" />

<style type="text/css">
    body {
    overflow:hidden;
    }

    input[type=text],textarea {
        width:90% !important;
    }

    select {
        width:98% !important;
    }
</style>


<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js/PTTScript.js"></script>
<script type="text/javascript" src="src/TITCustom.js"></script>
</head>
<body onblur="window.focus();">
<form id="form1" runat="server" target="_self">
    <ajax:ToolkitScriptManager ID="TK" runat="server" />
    <asp:UpdatePanel ID="UDPMain" runat="Server">
	<ContentTemplate>

			<asp:Panel ID="pnlInfo" runat="server" CssClass="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">					
					<h3><asp:Label ID="lblReportCode" runat="server">File Info</asp:Label></h3>					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
				 	<!--tabHeader -->
				 	<div class="tab-content current">
				 
				         <fieldset> 
							<div class="clear"></div><!-- End .clear -->
								<p id="pTitle" runat="server">
									<label class="column-left">Title: </label>
									<asp:TextBox CssClass="text-input" ID="txtTitle" MaxLength="200" runat="server"></asp:TextBox>									
								</p>
                                <p id="pType" runat="server">
									<label class="column-left">Type: </label>
									<asp:DropDownList CssClass="text-input small-input" ID="ddlType" runat="server"></asp:DropDownList>									
								</p>
								<p id="pDesc" runat="server">
									<label class="column-left">Description: </label>
									<asp:TextBox CssClass="text-input" ID="txtDesc" MaxLength="4000" Height="50px" TextMode="MultiLine" runat="server"></asp:TextBox>
								</p>
                             	<p id="pTag" runat="server">
									<label class="column-left">Tag (Search Keyword): </label>
									<asp:TextBox CssClass="text-input" ID="txtTag" MaxLength="500" Height="50px" TextMode="MultiLine" runat="server"></asp:TextBox>
								</p>
                                 <p>
									<label class="column-left">Last Edit: </label>
									<asp:TextBox CssClass="text-input" ID="txtLastEdit" ReadOnly="True" runat="server" Rows="2" TextMode="MultiLine"></asp:TextBox>								
								</p>							
							</fieldset>
						    
				   </div>
			   
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="../resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>                
           
			</asp:Panel>

               <div id="divCommand" >
                    <asp:Button ID="btnClose" runat="server" class="button" Style="margin-left:20px" text="Close" />
                    <input type="button" id="btnPresave" runat="server" class="button" value="OK" />
					<asp:Button ID="btnSave" runat="server" style="display:none;" />
				</div>

</ContentTemplate>
</asp:UpdatePanel>
</form>
</body>

</html>