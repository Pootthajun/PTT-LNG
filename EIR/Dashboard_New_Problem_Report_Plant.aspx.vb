﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class Dashboard_New_Problem_Report_Plant
    Inherits System.Web.UI.Page

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim Plant_ID As Integer = Request.QueryString("PLANT_ID")
            Dim Month_F As Integer = Request.QueryString("MONTH_F")
            Dim Month_T As Integer = Request.QueryString("MONTH_T")
            Dim Year_F As Integer = Request.QueryString("YEAR_F")
            Dim Year_T As Integer = Request.QueryString("YEAR_T")
            Dim Equipment As Integer = Request.QueryString("EQUIPMENT")

            Dashboard.BindDDlMonthEng(ddl_Month_F, Month_F)
            Dashboard.BindDDlMonthEng(ddl_Month_T, Month_T)
            Dashboard.BindDDlYear(ddl_Year_F, Year_F)
            Dashboard.BindDDlYear(ddl_Year_T, Year_T)
            Dashboard.BindDDlEquipment(ddl_Equipment, Equipment)

            lblBack.PostBackUrl = "Dashboard_New_Problem_Report.aspx?MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Equipment

            Dim SQL As String = ""
            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter
            SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & Plant_ID
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                lblPlant.Text = DT.Rows(0).Item("PLANT_Name").ToString
            End If

            lblPlantID.Text = Plant_ID
            BindData()
        End If
    End Sub

    Private Sub BindData()
        UC_Dashboard_New_Problem_Report_Plant.BindData(lblPlantID.Text, ddl_Month_F.SelectedValue, ddl_Month_T.SelectedValue, ddl_Year_F.SelectedValue, ddl_Year_T.SelectedValue, ddl_Equipment.SelectedValue)
    End Sub

    Protected Sub ALL_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Month_F.SelectedIndexChanged, ddl_Month_T.SelectedIndexChanged, ddl_Year_F.SelectedIndexChanged, ddl_Year_T.SelectedIndexChanged, ddl_Equipment.SelectedIndexChanged
        BindData()
    End Sub


End Class