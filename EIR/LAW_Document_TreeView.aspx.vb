﻿Imports EIR
Imports System.IO
Imports ICSharpCode.SharpZipLib.Core
Imports ICSharpCode.SharpZipLib.Zip


Public Class LAW_Document_TreeView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then
            'ProcessTreeview()
            BindFolderChildNode(0, False)
            GL_DialogLawUploadDocument1.CloseDialog()

            'Delete TempFile
            Dim cl As New LawClass
            Dim TempPath As String = cl.UploadTempPath() & Session("USER_Name") & "\"
            If Directory.Exists(TempPath) = True Then
                Directory.Delete(TempPath, True)
            End If
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "InitPage", "InitContextMenu();", False)
        End If

        pnlBindingError.Visible = False
    End Sub

    Private Sub btnContextMenuAction_Click(sender As Object, e As EventArgs) Handles btnContextMenuAction.Click
        Dim NodeID As Integer = 0
        If txtNodeID.Text.StartsWith("NodeID_") = True Then
            Dim tmp() As String = Split(txtNodeID.Text, "_")
            If tmp.Length = 2 Then
                NodeID = Convert.ToInt32(tmp(1))
            End If
        End If

        Select Case txtAction.Text.Trim
            Case LawDialogAction.newFolder.ToString
                GL_DialogLawFolder1.ShowDialog(NodeID, txtAction.Text.Trim)
            Case LawDialogAction.editFolder.ToString
                GL_DialogLawFolder1.ShowDialog(NodeID, txtAction.Text.Trim)
            Case LawDialogAction.deleteFolder.ToString
                Dim cl As New LawClass

                Dim dt As DataTable = cl.GetFolderDetail(NodeID)
                If dt.Rows.Count > 0 Then
                    Dim ParentID As Integer = Convert.ToInt32(dt.Rows(0)("parent_id"))
                    If cl.DeleteFolder(NodeID).ToLower = "true" Then
                        tvLawDocument.RemoveNode(NodeID)

                        'Dim CurrNode As TreeNode = FindChildNode(ParentID, tvLawDocument.Nodes(0))
                        'BindFolderChildNode(CurrNode)
                        'CurrNode.Expand()
                    End If
                End If
                dt.Dispose()

            Case LawDialogAction.newDocument.ToString
                GL_DialogLawUploadDocument1.ShowDialogAddDocument(NodeID)
            Case LawDialogAction.editDocument.ToString
                GL_DialogLawUploadDocument1.ShowDialogEditDocument(NodeID)
            Case LawDialogAction.duplicateFolder.ToString

                DuplicateFolder(NodeID)
            Case LawDialogAction.deleteDocument.ToString
                Dim cl As New LawClass
                Dim dt As DataTable = cl.GetDocumentDetail(NodeID)
                If dt.Rows.Count > 0 Then
                    Dim ParentID As Integer = dt.Rows(0)("folder_id")
                    If cl.DeleteDocument(NodeID).ToLower = "true" Then
                        tvLawDocument.RemoveNode(NodeID)

                        'Dim CurrNode As TreeNode = FindChildNode(ParentID, tvLawDocument.Nodes(0))
                        'BindFolderChildNode(CurrNode)
                        'CurrNode.Expand()
                    End If
                End If
                dt.Dispose()
            Case LawDialogAction.DownloadFolder.ToString
                DownloadFolder(NodeID)
        End Select
    End Sub

    Private Sub DownloadFolder(FolderID As Long)
        Dim cl As New LawClass
        Dim fDt As DataTable = cl.GetFolderDetail(FolderID)
        If fDt.Rows.Count > 0 Then
            'Delete TempFile
            Dim TempPath As String = cl.UploadTempPath() & Session("USER_Name") & "\"
            If Directory.Exists(TempPath) = True Then
                Directory.Delete(TempPath, True)
            End If

            Dim sourceDir As String = cl.BuiltDownloadFolder(FolderID, fDt.Rows(0)("folder_name"), Session("USER_Name"))
            'Dim sourceDir As String = "D:\Project\PTT-EIR5\PTT-EIR5\Sourcecode\EIR\Temp\PID\ถังเก็บผลิตภัณฑ์ Propan LPG\"
            If Directory.Exists(sourceDir) = True Then
#Region "Zip To File"
                '######### Zip To File ##################
                'Dim strmZipOutputStream As New ZipOutputStream(File.Create("D:\ZipZip.zip"))
                'strmZipOutputStream.SetLevel(9)
                'ZipFolder(sourceDir, sourceDir, strmZipOutputStream)
                'strmZipOutputStream.Finish()
                'strmZipOutputStream.Close()
                '#########################################
#End Region

#Region "Zip To Web Response"
                '###### Zip To Web Response #########################
                Dim targetName As String = [String].Format(fDt.Rows(0)("folder_name") & "_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss", New Globalization.CultureInfo("en-US")))
                targetName = targetName.Replace(",", "").Replace("(", "").Replace(")", "")

                Dim aStrFileName() As String = Directory.GetFiles(sourceDir, "*.*", SearchOption.AllDirectories)
                Response.Clear()
                Response.BufferOutput = False
                Response.ContentType = "application/zip"
                Response.AddHeader("content-disposition", "attachment; filename=" + targetName)

                Dim strmZipOutputStream As New ZipOutputStream(Response.OutputStream)
                'REM Compression Level: 0-9
                'REM 0: no(Compression)
                'REM 9: maximum compression
                strmZipOutputStream.SetLevel(9)

                ZipFolder(sourceDir, sourceDir, strmZipOutputStream)

                strmZipOutputStream.Finish()
                strmZipOutputStream.Close()

                Response.Flush()
                Response.End()
#End Region
            End If
        End If
        fDt.Dispose()

    End Sub


    Private Sub ZipFolder(RootFolder As String, CurrentFolder As String, zipStream As Object)
        Try
            Dim SubFolders() As String = Directory.GetDirectories(CurrentFolder)
            For Each Folder As String In SubFolders
                ZipFolder(RootFolder, Folder, zipStream)
            Next

            Dim relativePath = CurrentFolder.Substring(RootFolder.Length).Replace("\", "/").Replace("(", "").Replace(")", "").Replace(",", "") & "/"
            If relativePath.Length > 1 Then
                Dim dirEntry As ZipEntry
                dirEntry = New ZipEntry(relativePath)
                dirEntry.DateTime = DateTime.Now
                zipStream.PutNextEntry(dirEntry)
            End If

            For Each file As String In Directory.GetFiles(CurrentFolder)
                AddFileToZip(zipStream, relativePath, file)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AddFileToZip(zipStream As Object, relativePath As String, zfile As String)
        Dim buffer(4096) As Byte
        Dim fileRelativePath As String = IIf(relativePath.Length > 1, relativePath, "") & Path.GetFileName(zfile)
        Dim entry As New ZipEntry(fileRelativePath)
        entry.DateTime = DateTime.Now
        zipStream.PutNextEntry(entry)

        Using fs As FileStream = File.OpenRead(zfile)
            Dim sourceBytes As Integer = fs.Read(buffer, 0, buffer.Length)
            Do While sourceBytes > 0
                zipStream.Write(buffer, 0, sourceBytes)
                sourceBytes = fs.Read(buffer, 0, buffer.Length)

                If Response.IsClientConnected = False Then
                    Exit Do
                End If

                Response.Flush()
            Loop
        End Using
    End Sub


    Private Function GetDocumentStatus(CriticalDate As DateTime, NoticeDate As DateTime, BlankFile As String) As String
        Dim ret As String = ""
        If DateTime.Now.Date >= CriticalDate.Date And CriticalDate.Year <> 1 And BlankFile = "Y" Then
            ret = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
        ElseIf DateTime.Now.Date >= NoticeDate.Date And NoticeDate.Year <> 1 And BlankFile = "Y" Then
            ret = "<span style='color:orange'><b>NOTICE</b></span>"
        ElseIf DateTime.Now.Date < NoticeDate.Date And NoticeDate.Year <> 1 And BlankFile = "Y" Then
            ret = "<span style='color:blue'><b>WAITING</b></span>"
        ElseIf NoticeDate.Year = 1 And CriticalDate.Year = 1 And BlankFile = "Y" Then
            ret = "<span style='color:blue'><b>WAITING</b></span>"
        ElseIf BlankFile = "N" Then
            ret = "<span style='color:green'><b>COMPLETE</b></span>"
        End If
        Return ret
    End Function

    Private Function GetFolderStatus(CriticalDate As DateTime, NoticeDate As DateTime, IsComplete As String) As String
        Dim ret As String = ""
        If DateTime.Now.Date >= CriticalDate.Date And CriticalDate.Year <> 1 Then
            ret = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
        ElseIf DateTime.Now.Date >= NoticeDate.Date And NoticeDate.Year <> 1 Then
            ret = "<span style='color:orange'><b>NOTICE</b></span>"
        ElseIf DateTime.Now.Date < NoticeDate.Date And NoticeDate.Year <> 1 Then
            ret = "<span style='color:blue'><b>WAITING</b></span>"
        ElseIf NoticeDate.Year = 1 And CriticalDate.Year = 1 And IsComplete = "N" Then
            ret = "<span style='color:blue'><b>WAITING</b></span>"
        Else
            ret = "<span style='color:green'><b>COMPLETE</b></span>"
        End If
        Return ret
    End Function


    Private Sub BindFolderChildNode(ParentID As Integer, IsExpand As Boolean)
        Dim cl As New LawClass
        Dim dt As DataTable = cl.GetChildNode(ParentID)
        If dt.Rows.Count > 0 Then

            For Each dr As DataRow In dt.Rows
                Dim _NodeText As String = "<span class='" & dr("node_type") & "' id='NodeID_" & dr("folder_id") & "' >" & dr("folder_name") & "</span> " & Environment.NewLine

                If dr("node_type") = TreeviewNodeType.FolderMenu.ToString Then
                    Dim NoticeDate As Date = Nothing
                    Dim CriticalDate As Date = Nothing
                    Dim ActualDate As Date = Nothing
                    Dim NodeStatus As String = ""

                    Dim dDt As DataTable = cl.GetFolderNodeDateData(dr("folder_id"))
                    If dDt.Rows.Count > 0 Then
                        NoticeDate = Convert.ToDateTime(dDt.Rows(0)("notice_date"))
                        CriticalDate = Convert.ToDateTime(dDt.Rows(0)("critical_date"))
                        'ActualDate = Convert.ToDateTime(dDt.Rows(0)("actual_date"))

                        NodeStatus = GetFolderStatus(CriticalDate, NoticeDate, dDt.Rows(0)("is_complete"))
                    End If
                    dDt.Dispose()

                    Dim ImageUrl As String = ""
                    If dr("child_node") > 0 Then
                        ImageUrl = "resources/images/icons/icon-Folder_open.png"
                        tvLawDocument.AddNode(dr("folder_id"), _NodeText, NodeStatus, ParentID, NoticeDate, CriticalDate, ActualDate, ImageUrl, IsExpand)

                        BindFolderChildNode(dr("folder_id"), False)
                    Else
                        ImageUrl = "resources/images/icons/icon-Folder_close.png"
                        tvLawDocument.AddNode(dr("folder_id"), _NodeText, NodeStatus, ParentID, NoticeDate, CriticalDate, ActualDate, ImageUrl, IsExpand)
                    End If
                Else
                    Dim NoticeDate As New DateTime
                    Dim CriticalDate As New DateTime
                    Dim ActualDate As New DateTime
                    Dim NodeStatus As String = ""

                    If Convert.IsDBNull(dr("notice_date")) = False Then NoticeDate = Convert.ToDateTime(dr("notice_date"))
                    If Convert.IsDBNull(dr("critical_date")) = False Then CriticalDate = Convert.ToDateTime(dr("critical_date"))

                    If Convert.IsDBNull(dr("file_ext")) = False AndAlso dr("file_ext") <> "" Then
                        If Convert.IsDBNull(dr("actual_date")) = False Then ActualDate = Convert.ToDateTime(dr("actual_date"))
                    End If

                    Dim BlankFile As String = "N"
                    If Convert.IsDBNull(dr("full_name")) OrElse IO.File.Exists(dr("full_name")) = False Then
                        BlankFile = "Y"
                    End If
                    NodeStatus = GetDocumentStatus(CriticalDate, NoticeDate, BlankFile)

                    Dim IconURL As String = ""
                    If Convert.IsDBNull(dr("file_ext")) = False Then
                        IconURL = cl.GetIconImageUrl(dr("file_ext"))

                        _NodeText = "<a href='" & cl.LawDocumentDownloadURL() & dr("folder_id") & dr("file_ext") & "' target='_Blank' title='Download'  >" & _NodeText & "</a>"
                    Else
                        IconURL = "resources/images/icons/icon_blank_file.png"
                    End If

                    tvLawDocument.AddNode(dr("folder_id"), _NodeText, NodeStatus, ParentID, NoticeDate, CriticalDate, ActualDate, IconURL, IsExpand)
                End If
            Next
        End If
        dt.Dispose()
    End Sub


    Private Sub GL_DialogLawFolder1_SaveCompleted(ByRef sender As GL_DialogLawJob) Handles GL_DialogLawFolder1.SaveCompleted
        tvLawDocument.RemoveAllNodes()
        BindFolderChildNode(0, False)

        tvLawDocument.ExpandToNode(sender.FOLDER_ID)
    End Sub


    Private Function FindChildNode(FolderID As Integer, tNode As TreeNode) As TreeNode
        Dim ret As TreeNode = Nothing
        If FolderID = 0 Then 'Root Node
            Return tNode
        End If
        If tNode.ChildNodes.Count > 0 Then
            For Each t As TreeNode In tNode.ChildNodes
                If t.Value.Trim = "" Then Continue For

                If t.Value = FolderID Then
                    Return t
                Else
                    ret = FindChildNode(FolderID, t)
                    If ret IsNot Nothing Then
                        Return ret
                    End If
                End If
            Next
        End If

        Return ret
    End Function

    Private Sub GL_DialogLawUploadDocument1_SaveCompleted(ByRef sender As GL_DialogLawUploadDocument) Handles GL_DialogLawUploadDocument1.SaveCompleted
        tvLawDocument.RemoveAllNodes()
        'ProcessTreeview()
        BindFolderChildNode(0, False)
        tvLawDocument.ExpandToNode(sender.DOCUMENT_ID)
    End Sub


    Private Sub DuplicateFolder(FolderID As Integer)
        Dim cl As New LawClass
        Dim dt As DataTable = cl.GetFolderDetail(FolderID)
        If dt.Rows.Count > 0 Then
            Dim ret() As String = Split(cl.CopyFolder(FolderID, dt.Rows(0)("parent_id"), Session("USER_ID"), False), "|")

            If ret(0).ToLower = "true" Then
                tvLawDocument.RemoveAllNodes()
                'ProcessTreeview()
                BindFolderChildNode(0, False)

                tvLawDocument.ExpandToNode(FolderID)

            End If
        End If
        dt.Dispose()
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Dim cl As New LawClass
        Dim dt = cl.SearchDocumentFile(txtSearch.Text.Trim)

        If dt.Rows.Count = 0 Then
            pnlBindingError.Visible = True
            lblBindingError.Text = "No data found"
        Else
            pnlSearchTableView.Visible = True
            pnlTreeView.Visible = False

            rptSearchResult.DataSource = dt
            rptSearchResult.DataBind()
        End If
    End Sub

    Private Sub rptSearchResult_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptSearchResult.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblUpdateDate As Label = e.Item.FindControl("lblUpdateDate")
        Dim lblPath As Label = e.Item.FindControl("lblPath")

        lblName.Text = e.Item.DataItem("document_name")

        If Convert.IsDBNull(e.Item.DataItem("actual_date")) = False Then
            lblUpdateDate.Text = Convert.ToDateTime(e.Item.DataItem("actual_date")).ToString("dd/MM/yyyy", New Globalization.CultureInfo("th-TH"))
        End If

        lblPath.Text = e.Item.DataItem("folder_path")

    End Sub

    Private Sub btnBindingErrorClose_Click(sender As Object, e As ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Private Sub btnAddRootJob_Click(sender As Object, e As EventArgs) Handles btnAddRootJob.Click
        GL_DialogLawFolder1.ShowDialog(0, LawDialogAction.newFolder.ToString)
    End Sub
End Class