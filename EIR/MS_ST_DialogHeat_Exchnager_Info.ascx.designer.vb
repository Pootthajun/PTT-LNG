﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class MS_ST_DialogHeat_Exchnager_Info
    
    '''<summary>
    '''txt_Initial_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Initial_Year As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_DESIGN_TEMP_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_DESIGN_TEMP_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_DESIGN_TEMP_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_DESIGN_TEMP_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_DESIGN_TEMP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_DESIGN_TEMP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_DESIGN_PRESSURE_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_DESIGN_PRESSURE_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_DESIGN_PRESSURE_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_DESIGN_PRESSURE_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_DESIGN_PRESSURE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_DESIGN_PRESSURE As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_OPERATING_TEMP_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_OPERATING_TEMP_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_OPERATING_TEMP_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_OPERATING_TEMP_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_OPERATING_TEMP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_OPERATING_TEMP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_OPERATING_PRESSURE_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_OPERATING_PRESSURE_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_OPERATING_PRESSURE_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_OPERATING_PRESSURE_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_OPERATING_PRESSURE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_OPERATING_PRESSURE As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_HYDRO_TEST_PRESSURE_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_HYDRO_TEST_PRESSURE_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_HYDRO_TEST_PRESSURE_MIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_HYDRO_TEST_PRESSURE_MIN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_HYDRO_TEST_PRESSURE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_HYDRO_TEST_PRESSURE As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_MAWP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_MAWP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_MAWP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_MAWP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_MAWP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_MAWP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_MATERIAL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_MATERIAL As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_MATERIAL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_MATERIAL As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_MATERIAL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_MATERIAL As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_NOM_THK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_NOM_THK As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_NOM_THK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_NOM_THK As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_NOM_THK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_NOM_THK As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_CORROSION_ALLOW control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_CORROSION_ALLOW As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_CORROSION_ALLOW control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_CORROSION_ALLOW As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_CORROSION_ALLOW control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_CORROSION_ALLOW As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_MIN_ALL_THK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_MIN_ALL_THK As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_MIN_ALL_THK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_MIN_ALL_THK As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_MIN_ALL_THK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_MIN_ALL_THK As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_SIZE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_SIZE As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_SIZE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_SIZE As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_SIZE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_SIZE As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_QUANTITY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_QUANTITY As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_QUANTITY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_QUANTITY As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_QUANTITY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_QUANTITY As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_FLUID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_FLUID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_FLUID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_FLUID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_FLUID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_FLUID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_SHELL_NO_PASSES control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_SHELL_NO_PASSES As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_TUBE_NO_PASSES control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_TUBE_NO_PASSES As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_REMARK_NO_PASSES control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_REMARK_NO_PASSES As Global.System.Web.UI.WebControls.TextBox
End Class
