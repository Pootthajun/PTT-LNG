﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LAW_Document_Summary.aspx.vb" Inherits="EIR.LAW_Document_Summary" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
    .processBGImage {
        background-size:100px;
        background-repeat:no-repeat;
        background-position:center center;
        width:100px;
        height:100px;
    }
</style>

    <h2>Document Summary</h2>
    <div class="content-box"><!-- Start Content Box -->
        <!-- End .content-box-header -->
    <div class="content-box-header">
        <h3>Display condition</h3>


        <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
            ID="ddlSearchDocTemplete" runat="server" AutoPostBack="True">
        </asp:DropDownList>

        <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
            ID="ddlSearchYear" runat="server" AutoPostBack="True">
        </asp:DropDownList>

        <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
            ID="ddlSearchPlant" runat="server" AutoPostBack="True">
        </asp:DropDownList>

        <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
            ID="ddlSearchTag" runat="server" AutoPostBack="True">
        </asp:DropDownList>

        <div class="clear"></div>
    </div>
    <div class="content-box-content" style="padding:10px;">
        <asp:Panel ID="pnlTreeView" runat="server" CssClass="tab-content default-tab">
            <!-- This is the target div. id must match the href of this div's tab -->
            <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
            </asp:Panel>
                  
            <asp:Panel ID="pnlListDocument" runat="server">
                <table  >
                    <thead>
                        <tr>
                            <th ><a href="#">Document Name</a> </th>
                            <th ><a href="#">Year</a></th>
                            <th ><a href="#">Plant</a></th>
                            <th ><a href="#">Tag No</a></th>
                            <th ><a href="#">Status</a></th>
                            <th ><a href="#">% Complete</a></th>
                            <th ><a href="#">% Weight</a></th>
                            <th ><a href="#">Notice Date</a></th>
                            <th ><a href="#">Critical Date</a></th>
                            <th ><a href="#">Upload Date</a></th>
                            <th ><a href="#">Action</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rpt" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td >
                                        <asp:Label ID="lblDocumentName" runat="server"></asp:Label>
                                        <asp:Label ID="lblDocumentPlanID" runat="server" Visible="false" Text="0" ParentID="0"></asp:Label>    
                                    </td>
                                    <td><asp:Label ID="lblDocumentYear" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPlantCode" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPlanStatus" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblPercentComplete" runat="server"></asp:Label></td>
                                    <td><asp:TextBox ID="txtPercentWeight" runat="server" CssClass="text-input small-input" Width="40" AutoPostBack="true" OnTextChanged="txtPercentWeight_TextChanged" ></asp:TextBox></td>
                                    <td><asp:Label ID="lblNoticeDate" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblCriticalDate" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUploadDate" runat="server"></asp:Label></td>
                                    <td style="text-align:center">
                                        <asp:ImageButton ID="btnEdit" runat="server" OnClick="btnEdit_Click" ImageUrl="resources/images/icons/pencil.png" BorderStyle="None" ToolTip="Detail" CommandName="Edit" />
                                    </td>                  
                                </tr> 
                            </ItemTemplate>
                            <FooterTemplate></FooterTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>

                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg" Visible="false" > 
				    <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                    <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                </asp:Panel>
                <div class="clear"></div>
            </asp:Panel>
        </asp:Panel>
    </div>
    </div>

    <asp:Panel ID="pnlDashboard" runat="server" class="content-box">
    <!-- Start Content Box -->
        <div class="content-box-header">
            <h3>S-Curve</h3>
        </div>
        <div class="content-box-content" style="padding: 10px;height:510px;align-content:center">
                <asp:Chart ID="ChartYear" runat="server" Height="500px" BackColor="" CssClass="ChartHighligh" Width="1000">
                    <Legends>
                        <asp:Legend Name="Legend1" LegendStyle="Row" Docking="Bottom" Alignment="Center">
                        </asp:Legend>
                    </Legends>
                    <Titles>
                        <asp:Title Name="Title1" Font="Microsoft Sans Serif, 12pt, style=Bold" ForeColor="SteelBlue">
                        </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Series1" ChartType="Line" ChartArea="ChartArea1"
                            Color="Green" BorderWidth="3" Legend="Legend1" LegendText="Plan" ShadowColor="">
                        </asp:Series>
                        <asp:Series Name="Series2" ChartType="Line" ChartArea="ChartArea1"
                            Color="MediumSlateBlue" BorderWidth="3" Legend="Legend1" LegendText="Actual">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <AxisY IntervalAutoMode="VariableCount" IsLabelAutoFit="False" LineColor="Gray"
                                Maximum="100" TitleForeColor="Gray" Title="Progress %">
                                <MajorGrid LineColor="Gainsboro" />
                                <MinorGrid Enabled="True" Interval="5" LineColor="WhiteSmoke" />
                                <MajorTickMark Enabled="False" />
                                <LabelStyle Font="Microsoft Sans Serif, 8.25pt" ForeColor="Gray"
                                    Interval="10" />
                            </AxisY>
                            <AxisX IsLabelAutoFit="False"
                                IsMarginVisible="False" LineColor="Gray" IntervalType="Months"
                                LabelAutoFitStyle="IncreaseFont, DecreaseFont, StaggeredLabels, LabelsAngleStep30, LabelsAngleStep45"
                                TitleAlignment="Far" TitleForeColor="DimGray">
                                <MajorGrid Enabled="False" LineColor="DimGray" />
                                <MajorTickMark Enabled="False" />
                                <LabelStyle Font="Microsoft Sans Serif, 6.75pt" ForeColor="Gray"
                                    Interval="Auto" />
                            </AxisX>
                            <AxisX2 IntervalAutoMode="VariableCount">
                            </AxisX2>
                            <AxisY2 IntervalAutoMode="VariableCount">
                            </AxisY2>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
        </div>
    </asp:Panel>
    <div class="clear"></div>
    <div class="content-box"><!-- Start Content Box -->
        <div class="content-box-header">
            <h3>Notification</h3>
        </div>
        <div class="content-box-content" style="padding:10px;">
            <table >
                <thead>
                    <tr>
                        <th ><a href="#">Document Name</a> </th>
                        <th ><a href="#">Organization</a> </th>
                        <th ><a href="#">Template Name</a> </th>
                        <th ><a href="#">Plant</a></th>
                        <th ><a href="#">Tag No</a></th>
                        <th ><a href="#">Notice Date</a></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptNotification" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="lblNoticeDocumentName" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblOrgName" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblNoticeTemplateName" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblNoticePlantCode" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblNoticeTagNo" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblNoticeDate" runat="server"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>

