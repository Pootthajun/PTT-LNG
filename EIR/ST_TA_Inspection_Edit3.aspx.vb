﻿Imports System.Data
Imports System.Data.SqlClient
Public Class ST_TA_Inspection_Edit3
    Inherits System.Web.UI.Page
    Dim C As New Converter
    Dim BL As New EIR_BL
    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property Step_ID() As Integer    '-- Step การตรวจ 1as found ...
        Get
            If IsNumeric(ViewState("Step_ID")) Then
                Return ViewState("Step_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Step_ID") = value
        End Set
    End Property

    Private Property PLANT_ID() As Integer
        Get
            If IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
        End Set
    End Property

    Private Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Public Property INSP_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.Font.Underline Then
                    Return btnINSP.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            'FROM TAG
            BindInspection()
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.CommandArgument = value Then
                    btnINSP.Font.Underline = True
                Else
                    btnINSP.Font.Underline = False
                End If
            Next
            STATUS_ID = 0
            'PARTNO = ""

            ''-------------------Check Require Picture --------------
            'Dim RequiredPicture As Boolean = BL.IsInspectionRequirePicture(value)
            'TDImage.Visible = RequiredPicture
        End Set
    End Property

    Private Property TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_TYPE_ID")) Then
                Return ViewState("TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_TYPE_ID") = value
        End Set
    End Property


    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Public Property STATUS_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.Font.Underline Then
                    Return btnStatus.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            BindStatus()
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.CommandArgument = value Then
                    btnStatus.Font.Underline = True
                Else
                    btnStatus.Font.Underline = False
                End If
            Next
            LEVEL_ID = LEVEL_ID
        End Set
    End Property

    Public Property LEVEL_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.Font.Underline Then
                    Return btnLevel.CommandArgument
                End If
            Next
            Return -1
        End Get
        Set(ByVal value As Integer)
            BindLevel()
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.CommandArgument = value Then
                    btnLevel.Font.Underline = True
                Else
                    btnLevel.Font.Underline = False
                End If
            Next
            '--------Update Color---------
            UpdateSelectedColor()
        End Set
    End Property

    Private Sub UpdateSelectedColor()

        Dim CSS As String = "LevelDeselect"
        Select Case INSP_ID
            Case 12
                CSS = BL.Get_Inspection_Css_Box_By_Zone(LEVEL_ID)
            Case Else
                CSS = BL.Get_Inspection_Css_Box_By_Level(LEVEL_ID)
        End Select

        '--------------Inspection---------------
        For Each item As RepeaterItem In rpt_INSP.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnINSP As Button = item.FindControl("btnINSP")
            If btnINSP.Font.Underline Then
                btnINSP.CssClass = CSS
            Else
                btnINSP.CssClass = "LevelDeselect"
            End If
        Next
        '--------------Status---------------
        For Each item As RepeaterItem In rpt_STATUS.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnStatus As Button = item.FindControl("btnStatus")
            If btnStatus.Font.Underline Then
                btnStatus.CssClass = CSS
            Else
                btnStatus.CssClass = "LevelDeselect"
            End If
        Next
        '---------------Level---------------
        For Each item As RepeaterItem In rpt_LEVEL.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnLevel As Button = item.FindControl("btnLevel")
            If btnLevel.Font.Underline Then
                btnLevel.CssClass = CSS
            Else
                btnLevel.CssClass = "LevelDeselect"
            End If
        Next
    End Sub

    Private Property DETAIL_STEP_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_STEP_ID")) Then
                Return ViewState("DETAIL_STEP_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_STEP_ID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '---------------Hide Dialog------------------
        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")

            BindHeader()
            BindLevel()

            BindComponents()
            pnlValidation.Visible = False
            dialogCreateReport.Visible = False
            dialogTag.Visible = False


            'ddl_Fixed.Attributes("onchange") = "document.getElementById('" & btn_Autosave.ClientID & "').click();"

        End If
    End Sub



    Private Sub BindHeader()

        '--Page--
        Dim SQL As String = ""
        SQL &= "    Select TAG_ID,TAG_CODE,TAG_No,TAG_TYPE_ID,TAG_Name,TAG_TYPE_Name FROM VW_ST_TA_TAG " & vbNewLine
        SQL &= "        WHERE   TAG_ID = " & TAG_ID & " And " & vbNewLine
        SQL &= "        TAG_TYPE_ID = " & TAG_TYPE_ID & "  " & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        '------------------------------Report For -----------------------------------
        SQL = " Select * FROM VW_REPORT_ST_TA_HEADER WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("ST_TA_Inspection_Summary.aspx")
            Exit Sub
        End If
        PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")
        lbl_Plant_Edit.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Equipment_Edit.Text = DT.Rows(0).Item("TAG_TYPE_NAME")
        lbl_Year_Edit.Text = RPT_Year


        '--dialog Create--
        lbl_Plant_dialog.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Year_dialog.Text = RPT_Year
        lbl_Equipment_dialog.Text = DT.Rows(0).Item("TAG_TYPE_NAME")



        '--dialog Tag--'
        lbl_Plant_dialog_Tag.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Equipment_dialog_Tag.Text = DT.Rows(0).Item("TAG_TYPE_NAME")


        HTabAsFound.Attributes("class") = ""
        HTabAfterClean.Attributes("class") = ""
        HTabNDE.Attributes("class") = ""
        HTabRepair.Attributes("class") = ""
        HTabAfterRepair.Attributes("class") = ""
        HTabFinal.Attributes("class") = ""
        Step_ID = Request.QueryString("Step_ID")
        Select Case Step_ID
            Case 1
                HTabAsFound.Attributes("class") = "default-tab current"

            Case 2
                HTabAfterClean.Attributes("class") = "default-tab current"

            Case 3
                HTabNDE.Attributes("class") = "default-tab current"
            Case 4
                HTabRepair.Attributes("class") = "default-tab current"

            Case 5
                HTabAfterRepair.Attributes("class") = "default-tab current"
            Case 6
                HTabFinal.Attributes("class") = "default-tab current"
        End Select


    End Sub




    Private Sub BindInspection()
        Dim DT As New DataTable
        DT = BL.Get_Type_INSP_Name(TAG_TYPE_ID)
        rpt_INSP.DataSource = DT
        rpt_INSP.DataBind()
    End Sub

    Private Sub BindStatus()

        Dim SQL As String = " DECLARE @TAG_TYPE_ID As INT =" & TAG_TYPE_ID & vbNewLine
        SQL &= "  DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine
        SQL &= "         Select  TB.STATUS_ID ,VW_Default_Inspection_Status.STATUS_Name FROM (              " & vbNewLine
        SQL &= "  Select *                                                                                          " & vbNewLine
        SQL &= "   From [MS_ST_TA_TAG_Inspection]                                                  " & vbNewLine
        SQL &= "   UNION ALL                                                                                            " & vbNewLine
        SQL &= " Select *                                                                                           " & vbNewLine
        SQL &= "   From [MS_ST_TAG_Inspection]                                                     " & vbNewLine
        SQL &= "   ) AS TB                                                                                              " & vbNewLine
        SQL &= "   Left Join VW_Default_Inspection_Status On VW_Default_Inspection_Status.status_ID = TB.status_ID      " & vbNewLine
        SQL &= "   WHERE TAG_TYPE_ID =  @TAG_TYPE_ID And INSP_ID = @INSP_ID                                                                " & vbNewLine
        SQL &= "   ORDER BY VW_Default_Inspection_Status.STATUS_Order                                                   " & vbNewLine


        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        'DT = BL.Get_TA_Default_Inspection_Status
        DA.Fill(DT)
        rpt_STATUS.DataSource = DT
        rpt_STATUS.DataBind()
    End Sub

    Protected Sub rpt_INSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_INSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        btnINSP.Text = e.Item.DataItem("INSP_NAME")
        btnINSP.CssClass = "LevelDeselect"
        btnINSP.CommandArgument = e.Item.DataItem("INSP_ID")
    End Sub

    Protected Sub rpt_INSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_INSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        If INSP_ID <> btnINSP.CommandArgument Then INSP_ID = btnINSP.CommandArgument

        INSP_ID = btnINSP.CommandArgument
        BindStatus()
    End Sub

    Protected Sub rpt_STATUS_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_STATUS.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        btnStatus.Text = e.Item.DataItem("STATUS_Name")
        btnStatus.CssClass = "LevelDeselect"
        btnStatus.CommandArgument = e.Item.DataItem("STATUS_ID")

    End Sub

    Protected Sub rpt_STATUS_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_STATUS.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        If STATUS_ID <> btnStatus.CommandArgument Then STATUS_ID = btnStatus.CommandArgument
    End Sub

    Private Sub BindLevel()

        Dim SQL As String = "SELECT * FROM ISPT_Class"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_LEVEL.DataSource = DT
        rpt_LEVEL.DataBind()

    End Sub

    Protected Sub rpt_LEVEL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_LEVEL.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        btnLevel.CommandArgument = e.Item.DataItem("ICLS_LEVEL")
        Select Case INSP_ID
            Case 12
                Select Case CInt(e.Item.DataItem("ICLS_LEVEL"))
                    Case 0
                        btnLevel.Text = "ZoneA"
                    Case 1
                        btnLevel.Text = "ZoneB"
                    Case 2
                        btnLevel.Text = "ZoneC"
                    Case 3
                        btnLevel.Text = "ZoneD"
                End Select
            Case Else
                btnLevel.Text = e.Item.DataItem("ICLS_Description")
        End Select

    End Sub

    Protected Sub rpt_LEVEL_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_LEVEL.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        If LEVEL_ID <> btnLevel.CommandArgument Then LEVEL_ID = btnLevel.CommandArgument
    End Sub




    '=============dialog Tag==========================================


    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged

        If ddl_Edit_Plant.SelectedIndex > 0 Then
            ddl_Edit_Tag_Type.Enabled = True
        Else
            ddl_Edit_Tag_Type.Enabled = False
            ddl_Edit_Tag_Type.SelectedIndex = -1
        End If
    End Sub

    Private Sub ddl_Edit_Tag_Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Edit_Tag_Type.SelectedIndexChanged
        BindTag()
    End Sub


    Private Sub BindTag()

        Dim SQL As String = " SELECT * FROM (" & vbNewLine
        SQL &= "   Select AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TAG.ROUTE_ID,MS_ST_TAG.AREA_ID,MS_ST_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TAG' To_Table " & vbNewLine
        SQL &= "   FROM MS_ST_TAG" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine

        SQL &= "   UNION ALL" & vbNewLine
        SQL &= "   Select  AREA_CODE +'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TA_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TA_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TA_TAG.ROUTE_ID,MS_ST_TA_TAG.AREA_ID,MS_ST_TA_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TA_TAG' To_Table " & vbNewLine
        SQL &= "   From MS_ST_TA_TAG" & vbNewLine
        SQL &= "   INNER Join MS_ST_TAG_TYPE On  MS_ST_TA_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER Join MS_ST_Route ON MS_ST_TA_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER Join MS_PLANT On MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER Join MS_Area ON MS_ST_TA_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER Join MS_Process On MS_ST_TA_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
        SQL &= " ) TB " & vbNewLine
        SQL &= " left JOIN RPT_ST_TA_Detail ON RPT_ST_TA_Detail.TAG_ID = TB.TAG_ID AND RPT_ST_TA_Detail.TAG_TYPE_ID=TB.TAG_TYPE_ID  " & vbNewLine
        SQL &= " Where RPT_ST_TA_Detail.RPT_Year =" & RPT_Year & " AND RPT_ST_TA_Detail.RPT_No=" & RPT_No & "  " & vbNewLine

        Dim WHERE As String = ""

        'If ddl_Edit_Plant.SelectedIndex > 0 Then
        '    WHERE &= " TB.PLANT_ID=" & ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value & " And "
        'Else
        '    WHERE &= " 0=1 And "
        'End If
        If PLANT_ID > 0 Then
            WHERE &= " TB.PLANT_ID=" & PLANT_ID & " And "
        Else
            WHERE &= " 0=1 And "

        End If
        If TAG_TYPE_ID > 0 Then
            WHERE &= " TB.TAG_TYPE_ID=" & TAG_TYPE_ID & " And "
        Else
            WHERE &= " 0=1 And "

        End If


        If WHERE <> "" Then
            SQL &= " AND " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            'pnlBindingError.Visible = True
            'lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("MS_ST_TA_TAG") = DT

        Navigation.SesssionSourceName = "MS_ST_TA_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        'Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnSelect As ImageButton = e.Item.FindControl("btnSelect")

        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagNo.Attributes("TAG_NO") = e.Item.DataItem("TAG_NO")
        lblTagName.Text = e.Item.DataItem("TAG_Name")
        'lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")
        lblTagName.Attributes("TAG_TYPE_ID") = e.Item.DataItem("TAG_TYPE_ID")

        lblTo_Table.Text = e.Item.DataItem("To_Table")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
        End If


        btnSelect.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")
        btnSelect.Attributes("DETAIL_ID") = e.Item.DataItem("DETAIL_ID")


    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim btnSelect As ImageButton = e.Item.FindControl("btnSelect")
        'Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")

        Select Case e.CommandName
            Case "Select"
                TAG_ID = btnSelect.Attributes("TAG_ID")
                TAG_TYPE_ID = lblTagName.Attributes("TAG_TYPE_ID")

                lbl_Tag_Code_dialog.Text = lblTagNo.Text
                lbl_Tag_Name_dialog.Text = lblTagName.Text
                DETAIL_ID = btnSelect.Attributes("DETAIL_ID")
                dialogTag.Visible = False
                'BindInspection()
        End Select

    End Sub

    Private Sub btnAddTag_Click(sender As Object, e As EventArgs) Handles btnAddTag.Click
        dialogTag.Visible = True
        BindTag()
        BL.BindDDlPlant(ddl_Edit_Plant)
        BL.BindDDlST_TA_TagType(ddl_Edit_Tag_Type)

    End Sub

    Private Sub lnkAddInspection_Click(sender As Object, e As EventArgs) Handles lnkAddInspection.Click
        'dialogCreateReport_Clear()
        DETAIL_STEP_ID = 0
        dialogCreateReport.Visible = True
        pnlValidation_dialog.Visible = False
        BL.BindDDlST_TA_Step(ddl_ST_TA_Step)
        BindInspection()
        'LEVEL_ID = -1
        STATUS_ID = 0
        INSP_ID = 0

        If (Step_ID <> 0) Then
            ddl_ST_TA_Step.SelectedValue = Step_ID
            ddl_ST_TA_Step.Enabled = False
        End If
    End Sub

    Private Sub btnOK_dialogCreateReport_Click(sender As Object, e As EventArgs) Handles btnOK_dialogCreateReport.Click

        '----Validate -----

        'Tag
        If TAG_ID < 1 Then
            lblValidation_dialog.Text = "Select Tag "
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If
        'Report Date
        If Not BL.IsProgrammingDate(txt_dialog_Date.Text) Then
            lblValidation_dialog.Text = "Please select Date"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If
        'Step        
        If (ddl_ST_TA_Step.SelectedIndex < 0) Then
            lblValidation_dialog.Text = "Please select Step"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If
        'Inspection
        If INSP_ID < 1 Then
            lblValidation_dialog.Text = "Select Inspection Point"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If
        'Condition
        If STATUS_ID < 1 Then
            lblValidation_dialog.Text = "Select Condition"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If
        'Level
        If LEVEL_ID < 0 Then
            lblValidation_dialog.Text = "Select level"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If

        '----Add Table-----

        Dim _Year As Integer = C.DateToString(C.StringToDate(txt_dialog_Date.Text, dialog_Date_Extender.Format), "yyyy")
        If _Year > 2500 Then
            _Year = _Year - 543
        End If

        '-----ตรวจสอบรอบว่ามีรอบและจุดตรวจหรือไม่----
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE DETAIL_ID=" & DETAIL_ID & " AND RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND INSP_STATUS_ID=" & STATUS_ID & " AND Step_ID=" & ddl_ST_TA_Step.SelectedValue & " AND RPT_DATE='" & _Year & C.DateToString(C.StringToDate(txt_dialog_Date.Text, dialog_Date_Extender.Format), "-MM-dd") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            lblValidation_dialog.Text = "This report cannot be created due to follow these reasons"
            lblValidation_dialog.Text &= "<li>Report duplicates<li>Try to check report <b>" & vbNewLine
            pnlValidation_dialog.Visible = True
            Exit Sub

        Else
            '---สร้างรายการ

            'pnlList.Visible = True
            'pnlEdit.Visible = False
            DETAIL_STEP_ID = BL.GetNew_Table_ID("RPT_ST_TA_Detail_Step", "DETAIL_STEP_ID")
            '------2. สร้างรายการของ Insp
            Dim DR As DataRow
            DR = DT.NewRow
            DR("DETAIL_STEP_ID") = DETAIL_STEP_ID
            DR("DETAIL_ID") = DETAIL_ID
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("INSP_ID") = INSP_ID
            DR("INSP_STATUS_ID") = STATUS_ID
            DR("Step_ID") = ddl_ST_TA_Step.SelectedValue

            DR("STATUS_ID") = LEVEL_ID     '--Level Class A-C


            Try
                Dim StartDate As Date = C.StringToDate(txt_dialog_Date.Text, "yyyy-MM-dd")
                DR("RPT_Date") = StartDate
            Catch ex As Exception
                DR("RPT_Date") = DBNull.Value
            End Try

            DR("Created_Time") = Now
            DR("Created_By") = Session("USER_ID")
            DT.Rows.Add(DR)

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                '----สร้างรายงาน หลัก
                DA.Update(DT)
                DT.AcceptChanges()

                If (ddl_ST_TA_Step.SelectedValue = EIR_BL.ST_TA_STEP.As_Found) Then

                    '----สร้างรายงาน รอใน Step ถัดไป
                    '-----ตรวจสอบรอบว่ามีรอบและจุดตรวจหรือไม่----
                    Dim SQL_InsertNextStep As String = ""
                    SQL_InsertNextStep &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
                    SQL_InsertNextStep &= "    WHERE DETAIL_ID=" & DETAIL_ID & " AND RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND INSP_STATUS_ID=" & STATUS_ID & " AND Step_ID=" & EIR_BL.ST_TA_STEP.After_Clean
                    Dim DA_InsertNextStep As New SqlDataAdapter(SQL_InsertNextStep, BL.ConnStr)
                    Dim DT_InsertNextStep As New DataTable
                    DA_InsertNextStep.Fill(DT_InsertNextStep)
                    '---ตรวจสอบว่ามีการเพิ่มรายการตรวจรอแล้วหรือยัง  ?
                    If (DT_InsertNextStep.Rows.Count = 0) Then

                        '---สร้างรายการ

                        'pnlList.Visible = True
                        'pnlEdit.Visible = False
                        Dim DETAIL_STEP_ID_NextStep = BL.GetNew_Table_ID("RPT_ST_TA_Detail_Step", "DETAIL_STEP_ID")
                        '------2. สร้างรายการของ Insp

                        DR = DT_InsertNextStep.NewRow
                        DR("DETAIL_STEP_ID") = DETAIL_STEP_ID_NextStep
                        DR("DETAIL_ID") = DETAIL_ID
                        DR("RPT_Year") = RPT_Year
                        DR("RPT_No") = RPT_No
                        DR("TAG_ID") = TAG_ID
                        DR("INSP_ID") = INSP_ID
                        DR("INSP_STATUS_ID") = STATUS_ID
                        DR("STATUS_ID") = DBNull.Value      '--Level Class A-C
                        '----รายการรอให้วันที่ null
                        DR("RPT_Date") = DBNull.Value
                        DR("Created_Time") = Now
                        DR("Created_By") = Session("USER_ID")



                        Select Case ddl_ST_TA_Step.SelectedValue
                            Case EIR_BL.ST_TA_STEP.As_Found
                                '---เพิ่มนอใน Step After_Clean , Date Is Null
                                DR("Step_ID") = EIR_BL.ST_TA_STEP.After_Clean


                            Case EIR_BL.ST_TA_STEP.After_Clean


                            Case EIR_BL.ST_TA_STEP.NDE


                            Case EIR_BL.ST_TA_STEP.Repair


                            Case EIR_BL.ST_TA_STEP.After_Repair


                            Case EIR_BL.ST_TA_STEP.Final



                        End Select
                        DT_InsertNextStep.Rows.Add(DR)
                        cmd = New SqlCommandBuilder(DA_InsertNextStep)

                        Try


                            '----สร้างรายงาน หลัก
                            DA_InsertNextStep.Update(DT_InsertNextStep)
                            DT_InsertNextStep.AcceptChanges()
                        Catch ex As Exception
                            lblValidation_dialog.Text = ex.Message
                            pnlValidation_dialog.Visible = True
                            Exit Sub
                        End Try

                    End If

                End If

            Catch ex As Exception
                lblValidation_dialog.Text = ex.Message
                pnlValidation_dialog.Visible = True
                Exit Sub
            End Try



        End If




        '--สร้างรายงานเสร็จ เข้าหน้ากรอกรายละเอียด
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Response.Redirect("ST_TA_Daily_Detail.aspx?" & Param & "", True)
    End Sub



    Private Sub BindComponents()
        dialogCreateReport.Visible = False
        '------------------------------Header -----------------------------------
        Dim DT As DataTable = BL.Get_ST_TA_Inspection_For_Equipment(RPT_Year, RPT_No)
        DT.DefaultView.RowFilter = "Step_ID=" & Step_ID

        rptComponents.DataSource = DT.DefaultView.ToTable()
        rptComponents.DataBind()

        If (Step_ID = 4) Then
            th_Condition.Visible = False
            th_Fixed.Visible = True
        Else
            th_Condition.Visible = True
            th_Fixed.Visible = False
        End If
    End Sub

    Dim LastStep As String = ""
    Dim LastComponents As String = ""
    Dim LastStatus As String = ""


    Private Sub rptComponents_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptComponents.ItemDataBound
        'Dim lblStep As Label = e.Item.FindControl("lblStep")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")

        Dim lblRPT_Date As Label = e.Item.FindControl("lblRPT_Date")
        Dim lblComponents As Label = e.Item.FindControl("lblComponents")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblConditions As Label = e.Item.FindControl("lblConditions")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim lblActualStart As Label = e.Item.FindControl("lblActualStart")
        Dim lblActualEnd As Label = e.Item.FindControl("lblActualEnd")
        Dim lblFinished As Label = e.Item.FindControl("lblFinished")
        Dim ddl_Fixed As DropDownList = e.Item.FindControl("ddl_Fixed")

        'Dim trStep As HtmlTableRow = e.Item.FindControl("trStep")
        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim td1 As HtmlTableCell = e.Item.FindControl("td1")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")

        Dim td_Condition As HtmlTableCell = e.Item.FindControl("td_Condition")
        Dim td_Fixed As HtmlTableCell = e.Item.FindControl("td_Fixed")

        'Time
        Dim lblTimeStart As Label = e.Item.FindControl("lblTimeStart")
        Dim lblTimeEnd As Label = e.Item.FindControl("lblTimeEnd")

        If (Step_ID = 4) Then
            td_Condition.Visible = False
            td_Fixed.Visible = True
            If Not IsDBNull(e.Item.DataItem("Fixed")) Then
                ddl_Fixed.SelectedValue = e.Item.DataItem("Fixed")

            End If
        Else
            td_Condition.Visible = True
            td_Fixed.Visible = False
        End If


        ''--------LastStep------------
        'If LastStep <> e.Item.DataItem("Step_Name").ToString Then
        '    LastStep = e.Item.DataItem("Step_Name").ToString
        '    lblStep.Text = LastStep
        '    trStep.Visible = True
        '    LastComponents = ""
        'Else
        '    trStep.Visible = False
        'End If

        '--------LastComponents------------
        If LastComponents <> e.Item.DataItem("INSP_Name").ToString() & e.Item.DataItem("Step_Name").ToString() Then
            LastComponents = e.Item.DataItem("INSP_Name").ToString() & e.Item.DataItem("Step_Name").ToString()
            lblComponents.Text = e.Item.DataItem("INSP_Name").ToString()
            LastStatus = ""
        Else
            'LastStatus = ""
            td1.Attributes("style") &= DuplicatedStyleTop
        End If

        '--------LastStatus------------
        If LastStatus <> e.Item.DataItem("STATUS_Name").ToString & e.Item.DataItem("INSP_Name").ToString() & e.Item.DataItem("Step_Name").ToString() Then

            LastStatus = e.Item.DataItem("STATUS_Name").ToString & e.Item.DataItem("INSP_Name").ToString() & e.Item.DataItem("Step_Name").ToString()

            lblStatus.Text = e.Item.DataItem("STATUS_Name").ToString
        Else
            'LastStatus = ""
            lblStatus.Text = ""
            td2.Attributes("style") &= DuplicatedStyleTop
        End If

        'lblStep.Attributes("STEP_ID") = e.Item.DataItem("STEP_ID")

        lblFinished.Text = e.Item.DataItem("RPT_Status").ToString()
        If (e.Item.DataItem("RPT_Status").ToString() = "Inspecting") Then
            lblFinished.ForeColor = Drawing.Color.SteelBlue
        Else
            lblFinished.ForeColor = Drawing.Color.Green
        End If

        lblTagNo.Text = e.Item.DataItem("TAG_CODE").ToString()
        lblTagName.Text = e.Item.DataItem("TAG_Name").ToString()



        If Not IsDBNull(e.Item.DataItem("RPT_DATE")) Then
            lblRPT_Date.Text = BL.ReportProgrammingDate(e.Item.DataItem("RPT_DATE"))
            lblRPT_Date.Attributes("RPT_Date") = e.Item.DataItem("RPT_DATE")
        Else
            lblRPT_Date.Text = "???"
            lblRPT_Date.Attributes("RPT_Date") = ""
        End If

        'lblComponents.Text = e.Item.DataItem("INSP_Name").ToString()
        lblComponents.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")
        lblComponents.Attributes("INSP_ID") = e.Item.DataItem("INSP_ID").ToString()
        lblComponents.Attributes("DETAIL_STEP_ID") = e.Item.DataItem("DETAIL_STEP_ID").ToString()
        'lblStatus.Text = e.Item.DataItem("STATUS_Name").ToString()  '--สถานะที่เป็นไปได้
        lblComponents.Attributes("INSP_STATUS_ID") = e.Item.DataItem("INSP_STATUS_ID")
        lblComponents.Attributes("DETAIL_ID") = e.Item.DataItem("DETAIL_ID")

        lblConditions.Text = e.Item.DataItem("Condition_Problem").ToString()

        btnEdit.CommandArgument = e.Item.DataItem("INSP_ID")
        btnEdit.Attributes("DETAIL_STEP_ID") = e.Item.DataItem("DETAIL_STEP_ID")



        'Time
        lblTimeStart.Text = e.Item.DataItem("TIME_Start").ToString()
        lblTimeEnd.Text = e.Item.DataItem("TIME_End").ToString()

        If Not IsDBNull(e.Item.DataItem("DETAIL_ID")) Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If

        Dim TD As HtmlTableCell = e.Item.FindControl("TD")
        If Not IsDBNull(e.Item.DataItem("STATUS_ID")) Then
            If e.Item.DataItem("STATUS_ID") = 0 Then
                TD.Style("background-color") = "white"
                lblRPT_Date.Style("color") = "gray"

            ElseIf (e.Item.DataItem("STATUS_ID") = 1) Then
                TD.Style("background-color") = "yellow"
                lblRPT_Date.Style("color") = "gray"

            ElseIf (e.Item.DataItem("STATUS_ID") = 2) Then
                TD.Style("background-color") = "Orange"
                lblRPT_Date.Style("color") = "gray"
            ElseIf (e.Item.DataItem("STATUS_ID") = 3) Then
                TD.Style("background-color") = "red"
                lblRPT_Date.Style("color") = "gray"


            End If
        Else
            TD.Style("background-color") = "white"
            lblRPT_Date.Style("color") = "gray"


        End If

    End Sub



    Private Sub rptComponents_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptComponents.ItemCommand

        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        'Dim lblStep As Label = e.Item.FindControl("lblStep")
        Dim lblRPT_Date As Label = e.Item.FindControl("lblRPT_Date")
        Dim lblComponents As Label = e.Item.FindControl("lblComponents")
        Dim txtConditions As TextBox = e.Item.FindControl("txtConditions")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim lblTimeStart As Label = e.Item.FindControl("lblTimeStart")
        Dim lblTimeEnd As Label = e.Item.FindControl("lblTimeEnd")



        Select Case e.CommandName
            Case "Action"

                Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&DETAIL_STEP_ID=" & btnEdit.Attributes("DETAIL_STEP_ID")
                Response.Redirect("ST_TA_Daily_Detail.aspx?" & Param & "", True)

            Case "Delete"



                'Case "Add"
                '    Dim Add_STEP_ID As Integer = lblStep.Attributes("STEP_ID")

                '    DETAIL_STEP_ID = 0
                '    dialogCreateReport.Visible = True
                '    pnlValidation_dialog.Visible = False
                '    BL.BindDDlST_TA_Step(ddl_ST_TA_Step)
                '    BindInspection()
                '    STATUS_ID = 0
                '    INSP_ID = 0
                '    ddl_ST_TA_Step.SelectedValue = Add_STEP_ID


        End Select


    End Sub

    Private Sub btnClose_dialogCreateReport_Click(sender As Object, e As EventArgs) Handles btnClose_dialogCreateReport.Click
        dialogCreateReport.Visible = False
    End Sub

    Private Sub btnClose_dialogTag_Click(sender As Object, e As EventArgs) Handles btnClose_dialogTag.Click
        dialogTag.Visible = False
    End Sub

#Region "Navigator"

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click, HTabDetail.Click

        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit2.aspx?" & Param & "';", True)

    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click

        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit1.aspx?" & Param & "';", True)

    End Sub

    'Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='TURN_ITP_Edit4.aspx';", True)
    'End Sub


    Private Sub HTabAsFound_Click(sender As Object, e As EventArgs) Handles HTabAsFound.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.As_Found
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabAfterClean_Click(sender As Object, e As EventArgs) Handles HTabAfterClean.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.After_Clean
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabNDE_Click(sender As Object, e As EventArgs) Handles HTabNDE.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.NDE
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabRepair_Click(sender As Object, e As EventArgs) Handles HTabRepair.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.Repair
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabAfterRepair_Click(sender As Object, e As EventArgs) Handles HTabAfterRepair.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.After_Repair
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabFinal_Click(sender As Object, e As EventArgs) Handles HTabFinal.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.Final
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub



#End Region


    Protected Sub ddl_Fixed_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)

        Dim Item As RepeaterItem = ddl.Parent.Parent
        Dim btnEdit As ImageButton = Item.FindControl("btnEdit")
        Dim lblComponents As Label = Item.FindControl("lblComponents")
        Dim _DETAIL_STEP_ID As Integer = lblComponents.Attributes("DETAIL_STEP_ID")
        Dim ddl_Fixed As DropDownList = Item.FindControl("ddl_Fixed")

        Dim _TAG_ID As Integer = lblComponents.Attributes("TAG_ID")
        Dim _INSP_ID As Integer = lblComponents.Attributes("INSP_ID")
        Dim _INSP_STATUS_ID As Integer = lblComponents.Attributes("INSP_STATUS_ID")
        Dim _DETAIL_ID As Integer = lblComponents.Attributes("DETAIL_ID")

        '---update รายการที่การการซ่อม--

        '------1. ตรวจสอบการการ Insp
        Dim SQL As String = ""
        SQL &= "    Select * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE DETAIL_STEP_ID=" & _DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save." & vbNewLine & "This report has been removed!'); window.location.href='ST_TA_AsFound.aspx';", True)
            Exit Sub
        End If

        DR = DT.Rows(0)
        DR("FIXED") = ddl_Fixed.SelectedValue

        DR("Update_Time") = Now
        DR("Update_By") = Session("USER_ID")

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
            If (ddl_Fixed.SelectedValue = 1) Then
                Add_InStep_AfterRepair(_TAG_ID, _INSP_ID, _INSP_STATUS_ID, _DETAIL_ID)
            End If
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try


    End Sub



    Private Sub Add_InStep_AfterRepair(ByVal _TAG_ID As Integer, ByVal _INSP_ID As Integer, ByVal _INSP_STATUS_ID As Integer, ByVal _DETAIL_ID As Integer)

        '--Insert ใน Step Repair--
        Dim DR As DataRow
        '----สร้างรายงาน รอใน Step ถัดไป
        '-----ตรวจสอบรอบว่ามีรอบและจุดตรวจหรือไม่----
        Dim SQL_InsertNextStep As String = ""
        SQL_InsertNextStep &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL_InsertNextStep &= "    WHERE DETAIL_ID=" & _DETAIL_ID & " AND RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & _TAG_ID & " AND INSP_ID=" & _INSP_ID & " AND INSP_STATUS_ID=" & _INSP_STATUS_ID & " AND Step_ID=" & EIR_BL.ST_TA_STEP.After_Repair
        Dim DA_InsertNextStep As New SqlDataAdapter(SQL_InsertNextStep, BL.ConnStr)
        Dim DT_InsertNextStep As New DataTable
        DA_InsertNextStep.Fill(DT_InsertNextStep)
        '---ตรวจสอบว่ามีการเพิ่มรายการตรวจรอแล้วหรือยัง  ?
        If (DT_InsertNextStep.Rows.Count = 0) Then

            '---สร้างรายการ
            Dim DETAIL_STEP_ID_NextStep = BL.GetNew_Table_ID("RPT_ST_TA_Detail_Step", "DETAIL_STEP_ID")
            '------2. สร้างรายการของ Insp

            DR = DT_InsertNextStep.NewRow
            DR("DETAIL_STEP_ID") = DETAIL_STEP_ID_NextStep
            DR("DETAIL_ID") = _DETAIL_ID
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = _TAG_ID
            DR("INSP_ID") = _INSP_ID
            DR("INSP_STATUS_ID") = _INSP_STATUS_ID

            Dim DT_Date_Step As DataTable = DT_Daily_Date(RPT_Year, RPT_No, _TAG_ID, _INSP_ID, _INSP_STATUS_ID)
            DT_Date_Step.DefaultView.RowFilter = "STEP_ID=" & EIR_BL.ST_TA_STEP.Repair & " AND STATUS_ID IS NOT NULL "
            Dim DT_RowFilter = DT_Date_Step.DefaultView.ToTable()
            Dim Max_CLASS As Integer = 0
            If (DT_Date_Step.DefaultView.Count > 0) Then

                Max_CLASS = DT_Date_Step.DefaultView(0).Item("STATUS_ID") '-- สถานะล่าสุด
            End If

            DR("STATUS_ID") = Max_CLASS      '--Level Class A-C หา status ล่าสุด
            '----รายการรอให้วันที่ null
            DR("RPT_Date") = DBNull.Value
            DR("Created_Time") = Now
            DR("Created_By") = Session("USER_ID")


            DR("Step_ID") = EIR_BL.ST_TA_STEP.After_Repair

            DT_InsertNextStep.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA_InsertNextStep)
            Try


                '----สร้างรายงาน หลัก
                DA_InsertNextStep.Update(DT_InsertNextStep)
                DT_InsertNextStep.AcceptChanges()
            Catch ex As Exception
                lblValidation_dialog.Text = ex.Message
                pnlValidation_dialog.Visible = True
                Exit Sub
            End Try

        End If




    End Sub


    Function DT_Daily_Date(ByRef _RPT_Year As Integer, ByRef _RPT_No As Integer, ByRef _TAG_ID As Integer, ByRef _INSP_ID As Integer, ByRef _INSP_STATUS_ID As Integer) As DataTable
        Dim Sql As String = ""
        Sql &= "   DECLARE @RPT_Year As integer = " & _RPT_Year & "									" & vbNewLine
        Sql &= "   DECLARE @RPT_No As integer = " & _RPT_No & "										" & vbNewLine
        Sql &= "   DECLARE @TAG_ID As integer = " & _TAG_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_ID As integer = " & _INSP_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_STATUS_ID As integer = " & _INSP_STATUS_ID & "								" & vbNewLine
        Sql &= "   SELECT DISTINCT  													" & vbNewLine
        Sql &= "          STEP_ID														" & vbNewLine
        Sql &= "         ,RPT_DATE														" & vbNewLine
        Sql &= "         ,STATUS_ID														" & vbNewLine
        Sql &= "         ,MAX(STATUS_ID) MAX_CLASS										" & vbNewLine
        Sql &= "     FROM RPT_ST_TA_Detail_Step											" & vbNewLine
        Sql &= "     WHERE RPT_Year = @RPT_Year AND RPT_No = @RPT_No					" & vbNewLine
        Sql &= "     AND TAG_ID=@TAG_ID													" & vbNewLine
        Sql &= "     AND INSP_ID =@INSP_ID												" & vbNewLine
        Sql &= "     AND INSP_STATUS_ID= @INSP_STATUS_ID								" & vbNewLine
        Sql &= "     GROUP BY STEP_ID  ,RPT_DATE ,STATUS_ID								" & vbNewLine
        Sql &= "     ORDER BY RPT_DATE 	DESC						" & vbNewLine

        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)


        Return DT
    End Function


End Class