﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Stationary_OffRoutine_Summary
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Off_Routine_Report

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ClearPanelSearch()
            SetUserAuthorization()
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub
#End Region

    Private Sub SetUserAuthorization()
        btnCreate.Visible = USER_LEVEL = EIR_BL.User_Level.Collector Or USER_LEVEL = EIR_BL.User_Level.Inspector Or USER_LEVEL = EIR_BL.User_Level.Approver Or USER_LEVEL = EIR_BL.User_Level.Administrator
    End Sub

    Private Sub BindPlan()
        Dim SQL As String = "SELECT DISTINCT HD.RPT_Code,HD.RPT_Year," & vbNewLine
        SQL &= " HD.RPT_No,HD.PLANT_Code,HD.ROUTE_Code,RPT_ST_Detail.TAG_ID, AREA_Code + '-' + PROC_Code + '-' + TAG.TAG_No TAG_CODE," & vbNewLine
        SQL &= " HD.Created_Time,HD.RPT_STEP,HD.STEP_NAME,RPT_LOCK_BY" & vbNewLine

        '--------- Add User Locking Detial --------------
        SQL &= " ,User_Prefix+User_Name+' '+User_Surname Lock_By_Name" & vbNewLine

        SQL &= " FROM VW_REPORT_ST_HEADER HD " & vbNewLine
        SQL &= " INNER JOIN RPT_ST_Detail ON HD.RPT_Year=RPT_ST_Detail.RPT_Year AND HD.RPT_No=RPT_ST_Detail.RPT_No" & vbNewLine
        SQL &= " INNER JOIN MS_ST_TAG TAG ON RPT_ST_Detail.TAG_ID=TAG.TAG_ID AND TAG.Active_Status=1" & vbNewLine
        SQL &= " INNER JOIN MS_AREA AREA ON TAG.AREA_ID=AREA.AREA_ID AND AREA.Active_Status=1" & vbNewLine
        SQL &= " INNER JOIN MS_Process PRO ON TAG.PROC_ID=PRO.PROC_ID AND PRO.Active_Status=1" & vbNewLine

        '--------- Add User Locking Detial --------------
        SQL &= " LEFT JOIN MS_User ON HD.RPT_LOCK_BY=MS_User.USER_ID " & vbNewLine

        Dim WHERE As String = "WHERE HD.RPT_Type_ID=" & RPT_Type_ID & " AND " & vbNewLine
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " HD.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " HD.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " HD.RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If txt_Search_Start.Text <> "" Then
            WHERE &= " HD.Created_Time>='" & txt_Search_Start.Text & "' AND " & vbNewLine
        End If
        If txt_Search_End.Text <> "" Then
            WHERE &= " HD.Created_Time<='" & txt_Search_End.Text & "' AND " & vbNewLine
        End If
        If ddl_Search_Tag.SelectedIndex > 0 Then
            WHERE &= " RPT_ST_Detail.TAG_ID=" & ddl_Search_Tag.Items(ddl_Search_Tag.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If chk_Search_Edit.Checked Then
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    WHERE &= " HD.RPT_STEP IN (0,1) AND (RPT_LOCK_BY IS NULL OR RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
                Case EIR_BL.User_Level.Inspector
                    WHERE &= " HD.RPT_STEP = 2 AND (RPT_LOCK_BY IS NULL OR RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
                Case EIR_BL.User_Level.Approver
                    WHERE &= " HD.RPT_STEP = 3 AND (RPT_LOCK_BY IS NULL OR RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
            End Select
        End If

        SQL &= WHERE.Substring(0, WHERE.Length - 6) & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("MS_Stationary_Off_Routine_Summary") = DT

        Navigation.SesssionSourceName = "MS_Stationary_Off_Routine_Summary"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
        Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")
        '------- Check First --------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            lblBindingError.Text = "This report has been removed."
            pnlBindingError.Visible = True
            BindPlan()
            Exit Sub
        End If

        '---------------------- Set Available -----------------------
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim RPT_STEP As EIR_BL.Report_Step = lblStatus.Attributes("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY
            '.RPT_Type = RPT_Type_ID
            If Not .CanEdit Then
                lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
                lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
                lblBindingError.Text &= "<li>This report has been locked by others"
                pnlBindingError.Visible = True
                BindPlan()
                Exit Sub
            End If
        End With

        Select Case e.CommandName
            Case "Edit"
                '-------------Update Activated Report-----------
                SQL = "SELECT * FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                DA = New SqlDataAdapter(SQL, BL.ConnStr)
                DT = New DataTable
                DA.Fill(DT)

                If Session("USER_ID") <> 0 Then ' Lock if current user <> administrator
                    DT.Rows(0).Item("RPT_LOCK_BY") = Session("USER_ID")
                    Dim CMD As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If

                Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit1.aspx?" & Param & "';", True)

            Case "Delete"
                'Try
                '    BL.Drop_RPT_ST_Header(RPT_Year, RPT_No)
                'Catch ex As Exception
                '    lblBindingError.Text = ex.Message
                '    pnlBindingError.Visible = True
                '    Exit Sub
                'End Try
                'BindPlan()

                lblBindingError.Text = "Delete report successfully"
                pnlBindingError.Visible = True

        End Select

    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblCreated As Label = e.Item.FindControl("lblCreated")

        Dim imgLock As Image = e.Item.FindControl("imgLock")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete") '----- Add Delete Feature 

        lblRptNo.Text = e.Item.DataItem("RPT_Code")

        lblRptNo.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        lblRptNo.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        If Not IsDBNull(e.Item.DataItem("PLANT_Code")) Then
            lblPlant.Text = e.Item.DataItem("PLANT_Code")
        End If
        If Not IsDBNull(e.Item.DataItem("ROUTE_Code")) Then
            lblRoute.Text = e.Item.DataItem("ROUTE_Code")
        End If

        lblTag.Text = e.Item.DataItem("TAG_Code")

        lblStatus.Text = e.Item.DataItem("STEP_NAME")
        lblStatus.Attributes("RPT_STEP") = e.Item.DataItem("RPT_STEP")
        If Not IsDBNull(e.Item.DataItem("RPT_LOCK_BY")) Then
            lblStatus.Attributes("RPT_LOCK_BY") = e.Item.DataItem("RPT_LOCK_BY")
        Else
            lblStatus.Attributes("RPT_LOCK_BY") = -1
        End If

        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        lblCreated.Text = BL.ReportGridTime(e.Item.DataItem("Created_Time"))

        '------------------------- Set for Action Premission -------------------------
        btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnCreate.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnCreate.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnReport.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"

        Dim RPT_STEP As EIR_BL.Report_Step = e.Item.DataItem("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")


        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY

            If .CanEdit And RPT_LOCK_BY = -1 Then
                btnEdit.Visible = True
                btnEdit.ImageUrl = "resources/images/icons/pencil.png"
                btnDelete.Visible = True
                '------------ Set Lock Status ------------
                imgLock.Visible = False

            ElseIf .CanEdit And RPT_LOCK_BY = Session("USER_ID") Then
                btnEdit.Visible = True
                btnDelete.Visible = True
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And Session("USER_ID") = 0 Then
                btnEdit.ImageUrl = "resources/images/icons/pencil.png"
                btnEdit.Visible = True
                btnDelete.Visible = True
                '------------ Set Lock Status ------------
                imgLock.Visible = RPT_LOCK_BY <> -1
                '---------------- Add Locked By Detail- ------------------
                If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                End If
            ElseIf RPT_LOCK_BY > 0 And RPT_LOCK_BY <> Session("USER_ID") Then
                btnEdit.Visible = False
                btnCreate.Visible = False
                imgLock.Visible = True
                If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                End If
            Else
                btnEdit.Visible = False
                btnDelete.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = True
                '---------------- Add Locked Detail-------------------
                imgLock.ToolTip = "Lock by workflow"
            End If

            btnReport.Visible = .CanPreview
        End With

    End Sub

    Private Sub ClearPanelSearch()

        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDl_ST_Route(0, ddl_Search_Route)
        BL.BindDDl_ST_Tag(ddl_Search_Tag, -1)

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_ST_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.Text = Now.Year + 543

        txt_Search_Start.Text = ""
        txt_Search_End.Text = ""

        If USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Viewer Then
            If Not IsNothing(Request.QueryString("Editable")) AndAlso Request.QueryString("Editable") = "True" Then
                chk_Search_Edit.Checked = True
            End If
        Else
            chk_Search_Edit.Visible = False
            lblEditable.Visible = False
        End If

        BindPlan()
    End Sub

    Protected Sub Search_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged, ddl_Search_Route.SelectedIndexChanged, ddl_Search_Tag.SelectedIndexChanged, txt_Search_Start.TextChanged, txt_Search_End.TextChanged, chk_Search_Edit.CheckedChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        ddl_Search_Route_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub ddl_Search_Route_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged
        BL.BindDDl_ST_Tag(ddl_Search_Tag, ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value)
        BindPlan()
    End Sub

End Class