﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_Inspection_Plan.aspx.vb" Inherits="EIR.ST_TA_Inspection_Plan" %>

<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">

        <ContentTemplate>

            <h2>Turnaround Inspection Plan</h2>


            <div class="clear"></div>
            <!-- End .clear -->

            <div class="content-box">
                <!-- Start Content Box -->
                <!-- End .content-box-header -->
                <div class="content-box-header">
                    <h3>Display condition </h3>


                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddl_Search_Year" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

         

                    <div class="clear"></div>
                </div>



                <div class="content-box-content">

                    <!-- This is the target div. id must match the href of this div's tab -->
                    <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                        <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div>
                            <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                    </asp:Panel>

                    <asp:Panel ID="pnlListPlan" runat="server">

                        <!-- This is the target div. id must match the href of this div's tab -->
                        <table>
                            <thead>
                                <tr>
                                    <th><a href="#">Code</a></th>
                                    <th><a href="#">Year</a></th>
                                    <th><a href="#">Plant</a></th>
                                    <th><a href="#">Equipement-Type</a></th>
                                    <%--<th><a href="#">Round</a></th>--%>
                                    <th><a href="#">Start</a></th>
                                    <th><a href="#">End</a></th>
                                    <th><a href="#">Estimate(days)</a></th>
                                    <th><a href="#">TAG(QTY)</a></th>
                                    <th><a href="#">Status</a></th>
                                    <th><a href="#">Action</a></th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptPlan" runat="server">
                                <HeaderTemplate>


                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblYear" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblType" runat="server"></asp:Label></td>
                                       <%-- <td>
                                            <asp:TextBox ID="txtRound" runat="server" Style="border: none; background-color: Yellow;" Width="50px"></asp:TextBox></td>--%>
                                        <td>
                                            <asp:Label ID="lblStart" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblEnd" runat="server"></asp:Label></td>
                                        <td  style ="text-align :center ;" >
                                            <asp:Label ID="lblEstimate" runat="server"></asp:Label></td>
                                        <td  style ="text-align :center ;"  >
                                            <asp:Label ID="lblCountTag" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                        <td>
                                            <!-- Icons -->
                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                            <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                            <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnToggle" ConfirmText="Are you sure to delete this report permanently?" />

                                            <asp:Button ID="btnUpdateRound" CommandName="Round" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" />

                                        </td>
                                    </tr>
                                </ItemTemplate>

                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="bulk-actions align-left">
                                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                                        </div>
                                        <uc1:PageNavigation ID="Navigation" runat="server" />
                                        <!-- End .pagination -->
                                        <div class="clear"></div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    </asp:Panel>
                    <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>

                </div>
                <!-- End #tab1 -->

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
