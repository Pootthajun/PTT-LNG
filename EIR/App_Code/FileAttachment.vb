﻿Public Class FileAttachment

    Public UNIQUE_ID As String = ""
    Public Title As String = ""
    Public Description As String = ""
    Public Tag As String = ""
    Public Extension As ExtensionType = ExtensionType.Unknow
    Public DocType As AttachmentType = AttachmentType.Other
    Public Content As Byte() = {}
    Public LastEditTime As DateTime = DateTime.FromOADate(0)
    Public LastEditBy As Integer = -1

    Public Enum AttachmentType
        Cad_Drawing = 1
        Specification = 2
        Photograph = 3
        ReferenceDocument = 4
        LegalDocument = 5
        EquipementPreview = 10
        Other = 99
    End Enum

    Public Enum ExtensionType
        JPEG = 1
        GIF = 2
        PNG = 3
        TIFF = 4
        SVG = 5
        Unknow = 99
    End Enum

    Public Sub GenerateUniqueID()
        UNIQUE_ID = GenerateNewUniqueID()
    End Sub

End Class
