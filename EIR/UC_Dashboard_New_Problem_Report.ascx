﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Dashboard_New_Problem_Report.ascx.vb" Inherits="EIR.UC_Dashboard_New_Problem_Report" %>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
    <tr>
        <td style="vertical-align:top; text-align:center;">
            <asp:Chart ID="ChartMain" runat="server" Width="750px" Height="400px" CssClass="ChartHighligh"
                RightToLeft="Inherit"  >
            <legends>
                <asp:Legend Docking="Bottom" LegendStyle="Row" Name="Legend1">
                </asp:Legend>
            </legends>
            <titles>
                <asp:Title Name="Title1">
            </asp:Title>
            </titles>
            <Series>
                <asp:Series ChartArea="ChartArea1" Color="Yellow" 
                    Legend="Legend1" LegendText="ClassC" Name="Series1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" 
                    Color="255, 128, 0" Legend="Legend1" LegendText="ClassB" Name="Series2">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Color="Red" 
                    Legend="Legend1" LegendText="ClassA" Name="Series3">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <axisy intervalautomode="VariableCount">
                    </axisy>
                    <axisx intervalautomode="VariableCount">
                    </axisx>
                    <axisx2 intervalautomode="VariableCount">
                    </axisx2>
                    <axisy2 intervalautomode="VariableCount">
                    </axisy2>
                </asp:ChartArea>
            </ChartAreas>
            </asp:Chart>
        </td>
        <td style="vertical-align:top; text-align:center; width:100%">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" style="width:260px; border:1px solid #efefef;">
                      <tr>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:80px; border-bottom:1px solid #003366;">
                              Plant</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:60px; border-bottom:1px solid #003366;">
                              ClassC</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:60px; border-bottom:1px solid #003366;">
                              ClassB</td>
                          <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:60px; border-bottom:1px solid #003366;">
                              ClassA</td>
                      </tr>
                      <asp:Repeater ID="rptData" runat="server">
                        <ItemTemplate>
                            <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                              <td style="text-align:center;">
                                  <asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblClassC" runat="server" CssClass="TextClassC"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblClassB" runat="server" CssClass="TextClassB"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblClassA" runat="server" CssClass="TextClassA"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                      </asp:Repeater>
                </table>         
            </center>           
        </td>
    </tr>
</table>
<div style="visibility: hidden">
    <asp:Label ID="lblMONTH_F" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblMONTH_T" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblYEAR_F" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblYEAR_T" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblEQUIPMENT" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblPrintPage" runat="server" Text=""></asp:Label>
</div>