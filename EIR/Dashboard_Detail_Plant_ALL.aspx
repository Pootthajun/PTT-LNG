﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Detail_Plant_ALL.aspx.vb" Inherits="EIR.Dashboard_Detail_Plant_ALL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDP1" runat="server">
        <ContentTemplate>
			<h2>
                <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
			</h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
        	    <tr>
        	        <td>
        	            <asp:LinkButton ID="lblBack" runat="server" Tooltip="Back to see on this plant" Text="Back to see on this plant"></asp:LinkButton>
        	        </td>
        	    </tr>
                <tr>
                    <td>
                        <table cellpadding="3" cellspacing="0" style="border:2px solid #FFAA00;">
                    <tr>
                        <td style="background-color:#00cc33; text-align:center; color:White; font-weight:bold;" colspan="5">          
                            <asp:Label ID="lblHead" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <asp:Chart ID="Chart1" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center;">
                            <asp:Chart ID="Chart2" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center;">
                            <asp:Chart ID="Chart3" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center;">
                            <asp:Chart ID="Chart4" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center;">
                            <asp:Chart ID="Chart5" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px;">
                                <tr>
                                    <td class="LevelClassC" style="text-align:center;">
                                        ClassC
                                    </td>
                                    <td class="LevelClassB" style="text-align:center;">
                                        ClassB
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        ClassA
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_C" runat="server" CssClass="TextClassC" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_B" runat="server" CssClass="TextClassB" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px;">
                                <tr>
                                    <td class="LevelClassC" style="text-align:center;">
                                        ClassC
                                    </td>
                                    <td class="LevelClassB" style="text-align:center;">
                                        ClassB
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        ClassA
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_C" runat="server" CssClass="TextClassC" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_B" runat="server" CssClass="TextClassB" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px;">
                                <tr>
                                    <td class="LevelClassA" style="text-align:center;">
                                        Abnormal
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl3_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px;">
                                <tr>
                                    <td class="LevelClassA" style="text-align:center;">
                                        Abnormal
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl4_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px;">
                                <tr>
                                    <td class="LevelClassA" style="text-align:center;">
                                        Abnormal
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl5_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                    </td>
                </tr>
			</table>
			<div style="visibility: hidden">
                <asp:Label ID="lblMonth" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblYear" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblPlantName" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblReportName" runat="server" Text="Label"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

