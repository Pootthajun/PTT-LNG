﻿Imports System.Data.SqlClient
Public Class PIPE_Insulation
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ResetInsulation(Nothing, Nothing)
            ClearPanelSearch()
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindInsulation()

        Dim SQL As String = " Select INS.IN_ID,IN_Code,IN_Name,INS.Active_Status,INS.Update_By,INS.Update_Time,COUNT(TAG.TAG_ID) TotalTag " & vbLf
        SQL &= " FROM MS_PIPE_Insulation INS " & vbLf
        SQL &= " LEFT JOIN  " & vbLf
        SQL &= " (SELECT DISTINCT MS_PIPE_POINT.TAG_ID,MS_PIPE_POINT.IN_ID,1 Active_Status" & vbLf
        SQL &= " 	FROM MS_PIPE_TAG " & vbLf
        SQL &= " 	INNER JOIN MS_PIPE_POINT ON  MS_PIPE_TAG.TAG_ID=MS_PIPE_POINT.TAG_ID" & vbLf
        SQL &= " 	WHERE MS_PIPE_TAG.Active_Status=1" & vbLf
        SQL &= " ) TAG On TAG.IN_ID=INS.IN_ID AND TAG.Active_Status=1" & vbLf

        Dim WHERE As String = ""
        If txt_Search.Text <> "" Then
            WHERE &= " (IN_Code Like '%" & txt_Search.Text.Replace("'", "''") & "%' OR IN_Name Like '%" & txt_Search.Text.Replace("'", "''") & "%') AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " GROUP BY INS.IN_ID,IN_Code,IN_Name,INS.Active_Status,INS.Update_By,INS.Update_Time" & vbLf
        SQL &= " ORDER BY IN_Code" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try

        Session("PIPE_Insulation") = DT

        Navigation.SesssionSourceName = "PIPE_Insulation"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptInsulation
    End Sub

    Protected Sub rptrptInsulation_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInsulation.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfbDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblCode.Text = e.Item.DataItem("IN_Code").ToString
        lblName.Text = e.Item.DataItem("IN_Name").ToString

        If Not IsDBNull(e.Item.DataItem("TotalTag")) AndAlso e.Item.DataItem("TotalTag") > 0 Then
            lblPipe.Text = FormatNumber(e.Item.DataItem("TotalTag"), 0)
            cfbDelete.Enabled = False
            btnDelete.Visible = False
        Else
            lblPipe.Text = "-"
            cfbDelete.Enabled = True
            btnDelete.Visible = True
        End If

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        If Not IsDBNull(e.Item.DataItem("Update_Time")) Then
            lblUpdateTime.Text = C.DateToString(e.Item.DataItem("Update_Time"), "dd MMM yyyy")
        Else
            lblUpdateTime.Text = "-"
        End If
        btnEdit.Attributes("IN_ID") = e.Item.DataItem("IN_ID")

    End Sub

    Private Sub rptInsulation_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptInsulation.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim IN_ID As Integer = btnEdit.Attributes("IN_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("IN_ID") = IN_ID
                '------------------------------------
                pnlListInsulation.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT IN_ID,IN_Code,IN_Name,Active_Status,Update_By,Update_Time" & vbLf
                SQL &= " FROM MS_PIPE_Insulation" & vbLf
                SQL &= " WHERE IN_ID = " & IN_ID & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Material Not Found"
                    pnlBindingError.Visible = True
                    BindInsulation()
                    Exit Sub
                End If

                txtCode.Text = DT.Rows(0).Item("IN_Code").ToString
                txtName.Text = DT.Rows(0).Item("IN_Name").ToString
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_PIPE_Insulation Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE IN_ID=" & IN_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindInsulation()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try

            Case "Delete"
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM MS_PIPE_Insulation WHERE IN_ID=" & IN_ID
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindInsulation()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try
        End Select

    End Sub

    Protected Sub ResetInsulation(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindInsulation()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListInsulation.Enabled = True
    End Sub
    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtCode.Text = ""
        txtName.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True
    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        txt_Search.Text = ""
        BindInsulation()
    End Sub

    Private Sub txt_Search_TextChanged(sender As Object, e As EventArgs) Handles txt_Search.TextChanged
        BindInsulation()
    End Sub
#End Region

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("IN_ID") = 0

        '-----------------------------------
        pnlListInsulation.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtCode.Text = "" Then
            lblValidation.Text = "Please insert insulation code "
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim IN_ID As Integer = lblUpdateMode.Attributes("IN_ID")

        Dim SQL As String = "SELECT * FROM MS_PIPE_Insulation WHERE IN_Code='" & txtCode.Text.Replace("'", "''") & "' AND IN_ID<>" & IN_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Insulation Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        'If txtName.Text <> "" Then
        '    SQL = "SELECT * FROM MS_PIPE_Insulation WHERE IN_Name='" & txtName.Text.Replace("'", "''") & "' AND IN_ID<>" & IN_ID
        '    DA = New SqlDataAdapter(SQL, BL.ConnStr)
        '    DT = New DataTable
        '    DA.Fill(DT)
        '    If DT.Rows.Count > 0 Then
        '        lblValidation.Text = "This Insulation Name is already exists"
        '        pnlValidation.Visible = True
        '        Exit Sub
        '    End If
        'End If

        SQL = "SELECT * FROM MS_PIPE_Insulation WHERE IN_ID=" & IN_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            IN_ID = GetNewInsulationlID()
            DR("IN_ID") = IN_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("IN_Code") = txtCode.Text
        DR("IN_Name") = txtName.Text

        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = ex.Message
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetInsulation(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        'DT = Session("PIPE_Insulation")
        'DT.DefaultView.RowFilter = "IN_ID=" & IN_ID
    End Sub

    Private Function GetNewInsulationlID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(IN_ID),0)+1 FROM MS_PIPE_Insulation "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function
End Class