﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Detail_Plant_ST
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim SQL As String = ""
            Dim DT_PLANT As New DataTable
            Dim DA As New SqlDataAdapter
            SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & Request.QueryString("PLANT_ID")
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT_PLANT)
            If DT_PLANT.Rows.Count > 0 Then
                lblPlantName.Text = DT_PLANT.Rows(0).Item("PLANT_Name").ToString
            End If
            lblBack.Text = "Back to see on this plant " & lblPlantName.Text
            lblMonth.Text = Request.QueryString("MONTH")
            lblYear.Text = Request.QueryString("YEAR")
            Select Case Request.QueryString("REPORT")
                Case EIR_BL.ReportName_Problem.NEW_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "New Problem Occurred"
                    lblReport.Text = EIR_BL.Dashboard.New_Problem_Occurred
                    BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
                Case EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.TOTAL_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_Total_Problem_Report_Plant.aspx?PLANT_ID=" & Request.QueryString("PLANT_ID") & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "Total Problem by Month"
                    lblReport.Text = EIR_BL.Dashboard.Total_Problem_by_month
                    BindData(Request.QueryString("PLANT_ID"), Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
            End Select
        End If

    End Sub

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month As Integer, ByVal Year As Integer, ByVal Equipment As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        If Year > 2500 Then
            Year = Year - 543
        End If

        Dim DateFrom As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)
        Select Case lblReport.Text
            Case EIR_BL.Dashboard.New_Problem_Occurred
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)
            Case EIR_BL.Dashboard.Total_Problem_by_month
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate)
                DT_Dashboard.DefaultView.RowFilter = "ICLS_ID > 0"
                DT_Dashboard = DT_Dashboard.DefaultView.ToTable
        End Select

        DT_Dashboard.DefaultView.Sort = "TAG_CODE"
        DT_Dashboard = DT_Dashboard.DefaultView.ToTable

        Session("Dashboard_Detail_Plant_ST") = DT_Dashboard
        DisplayChart(ChartMain, New EventArgs)
        rptData.DataSource = DT_Dashboard
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        lblTag.Text = e.Item.DataItem("TAG_CODE")
        lblLevel.Text = BL.Get_Problem_Level_Name(e.Item.DataItem("ICLS_ID"))
        lblLevel.CssClass = BL.Get_Inspection_Css_Text_By_Level(e.Item.DataItem("ICLS_ID"))
        tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_ST.aspx?TAG_ID=" & e.Item.DataItem("TAG_ID") & "&RPT_YEAR=" & e.Item.DataItem("RPT_YEAR") & "&RPT_NO=" & e.Item.DataItem("RPT_NO") & "','Dialog_Tag_ST_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_ChartType.SelectedIndexChanged

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartMain.Series(0).ChartType
        Select Case ddl_ChartType.Items(ddl_ChartType.SelectedIndex).Value
            Case "Pie"
                ChartType = DataVisualization.Charting.SeriesChartType.Pie
            Case "Doughnut"
                ChartType = DataVisualization.Charting.SeriesChartType.Doughnut
            Case "Funnel"
                ChartType = DataVisualization.Charting.SeriesChartType.Funnel
            Case "Pyramid"
                ChartType = DataVisualization.Charting.SeriesChartType.Pyramid
        End Select
        ChartMain.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Detail_Plant_ST")

        Dim ClassC As Double = DT.Compute("COUNT(TAG_ID)", "ICLS_ID=1")
        Dim ClassB As Double = DT.Compute("COUNT(TAG_ID)", "ICLS_ID=2")
        Dim ClassA As Double = DT.Compute("COUNT(TAG_ID)", "ICLS_ID=3")

        Dim YValue As Double() = {ClassC, ClassB, ClassA}
        ChartMain.Series("Series1").Points.DataBindY(YValue)
        ChartMain.Series("Series1").Points(0).LegendText = "ClassC"
        ChartMain.Series("Series1").Points(1).LegendText = "ClassB"
        ChartMain.Series("Series1").Points(2).LegendText = "ClassA"

        ChartMain.Series("Series1").Points(0).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(1).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(2).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

        ChartMain.Series("Series1").Points(0).Color = Drawing.Color.Yellow
        ChartMain.Series("Series1").Points(1).Color = Drawing.Color.Orange
        ChartMain.Series("Series1").Points(2).Color = Drawing.Color.Red

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)

        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter
        ChartMain.Titles("Title2").Text = "Total " & FormatNumber(ClassC + ClassB + ClassA, 0) & " tag(s)"

        Select Case lblReport.Text
            Case EIR_BL.Dashboard.New_Problem_Occurred
                ChartMain.Titles("Title1").Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Stationary_Routine_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text & vbNewLine & " "
            Case EIR_BL.Dashboard.Total_Problem_by_month
                ChartMain.Titles("Title1").Text = "Total Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Stationary_Routine_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text & vbNewLine & " "
        End Select

    End Sub

End Class