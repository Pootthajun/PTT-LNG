﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_Inspection_Plan_Edit.aspx.vb" Inherits="EIR.ST_TA_Inspection_Plan_Edit" %>

<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register Src="MS_ST_DialogAbsorber_Spec.ascx" TagName="MS_ST_DialogAbsorber_Spec" TagPrefix="uc4" %>
<%@ Register Src="MS_ST_DialogColumn_Spec.ascx" TagName="MS_ST_DialogColumn_Spec" TagPrefix="uc5" %>
<%@ Register Src="MS_ST_DialogFilter_Spec.ascx" TagName="MS_ST_DialogFilter_Spec" TagPrefix="uc6" %>
<%@ Register Src="MS_ST_DialogDrum_Spec.ascx" TagName="MS_ST_DialogDrum_Spec" TagPrefix="uc7" %>
<%@ Register Src="MS_ST_DialogHeat_Exchnager_Spec.ascx" TagName="MS_ST_DialogHeat_Exchnager_Spec" TagPrefix="uc8" %>


<asp:Content ID="ContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <link rel="stylesheet" href="resources/css/StyleTurnaround.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />

    

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UDPMain" runat="Server">

        <ContentTemplate>

            <h2>Turnaround Inspection Plan</h2>

            <div class="clear"></div>
            <!-- End .clear -->

            <div class="content-box">
                <!-- Start Content Box -->
                <!-- End .content-box-header -->
                <div class="content-box-header">
                    <h3>
                        <asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Plan </h3>


                    <%--<asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddl_Search_Year" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>--%>

                    <div class="clear"></div>
                </div>
                <div class="content-box-content">

                    <%--                    <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">
                        <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div>
                            <asp:Label ID="lblBindingSuccess" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>--%>
                    <asp:Panel ID="pnlEdit" runat="server">

                        <fieldset>
                            <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                            <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                                <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                                <div>
                                    <asp:Label ID="lblBindingError" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>
                            <p>
                                <label class="column-left" style="width: 120px;">Define to Plant : </label>
                                <asp:DropDownList CssClass="select"
                                    ID="ddl_Edit_Plant" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </p>

                            <p>
                                <label class="column-left" style="width: 120px;">Define to Type : </label>
                                <asp:DropDownList CssClass="select"
                                    ID="ddl_Edit_Tag_Type" runat="server" AutoPostBack="True">
                                </asp:DropDownList>

                            </p>
                            <p>
                                <label class="column-left" style="width: 120px;">Start date :</label>
                                
                                <asp:TextBox
                                    runat="server" ID="txtStart" CssClass="text-input small-input " MaxLength="20"
                                    AutoPostBack="True"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtStart_CalendarExtender" runat="server"
                                    Format="yyyy-MM-dd" TargetControlID="txtStart" PopupPosition="Right">
                                </cc1:CalendarExtender>
                                <!-- End .clear -->
                            </p>
                            <%-- <p>
                                <label class="column-left" style="width: 120px;">Start date xxx :</label><asp:TextBox
                                    runat="server" ID="TextBox1" CssClass="text-input small-input " MaxLength="20"
                                    AutoPostBack="True"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                    Format="yyyy-MM-dd" TargetControlID="TextBox1" PopupPosition="Right">
                                </cc1:CalendarExtender>
                                <!-- End .clear -->
                            </p>--%>
                            <p>
                                <label class="column-left" style="width: 120px;">Estimate :</label><asp:TextBox
                                    runat="server" Style="width: 100px" ID="txtEstimate"
                                    CssClass="text-input small-input " MaxLength="5" AutoPostBack="True"></asp:TextBox>
                                &nbsp;<span style="font-weight: bold">days</span>
                                <!-- End .clear -->
                            </p>

                            <p>
                                <label class="column-left" style="width: 120px;">End date :</label><asp:TextBox
                                    runat="server" ID="txtEnd" CssClass="text-input small-input " MaxLength="20"
                                    AutoPostBack="True"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtEnd_CalendarExtender" runat="server"
                                    Format="yyyy-MM-dd" TargetControlID="txtEnd" PopupPosition="Right">
                                </cc1:CalendarExtender>
                                <!-- End .clear -->
                            </p>
                            <p>
                                <table style="border: none;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 80px; vertical-align: top;">
                                                <label class="column-left" style="width: 100px;">Tag :</label></td>
                                            <td style="vertical-align: top;">
                                                <%--SELECT TAG--%>
                                                <div class="content-box">
                                                    <!-- Start Content Box -->
                                                    <!-- End .content-box-header -->
                                                    <div class="content-box-header" style="height: 50px;">
                                                        <table>
                                                            <tr>
                                                                <td valign="middle">
                                                                    <h5 style="position: relative; top: 5px;">Display condition </h5>
                                                                </td>
                                                                <td valign="middle">
                                                                    <%--<asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                                                                        ID="ddl_Search_Tag_Type" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>--%>
                                                                   <%-- <asp:TextBox runat="server" placeholder="Code , Tag Name" ID="txt_Search_Tag" Style="position: relative; top: 5px;" CssClass="text-input small-input " MaxLength="20" AutoPostBack="True"></asp:TextBox>--%>

                                                                </td>
                                                                <td valign="middle">&nbsp;<asp:Label ID="lbl1" runat="server" Style="position: relative; top: 5px;" Text=" Status"></asp:Label>

                                                                </td>
                                                                <td valign="middle">
                                                                    <table style="position: relative; top: -10px;">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckStatus_OK" runat="server" AutoPostBack="True" />
                                                                                <span>Ok</span>&nbsp; 
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckStatus_Abnormal" runat="server" AutoPostBack="True" />
                                                                                <span>Abnormal</span>&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckStatus_ClassA" runat="server" AutoPostBack="True" />
                                                                                <span>Class A</span>&nbsp;
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckStatus_ClassB" runat="server" AutoPostBack="True" />
                                                                                <span>Class B</span>&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckStatus_ClassC" runat="server" AutoPostBack="True" />
                                                                                <span>Class C</span>&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckStatus_ClassD" runat="server" AutoPostBack="True" />
                                                                                <span>Class D</span>&nbsp; 
                                                                            </td>
                                                                        </tr>


                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </table>




                                                        <%--<h3>Display condition </h3>--%>


                                                        <%--<asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                                                            ID="ddl_Search_Tag_Type" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>--%>
                                                        <%--<asp:TextBox runat="server" placeholder="Code , Tag Name" ID="txt_Search_Tag" style="position:relative; top:5px;" CssClass="text-input small-input " MaxLength="20" AutoPostBack="True"></asp:TextBox>
                                                        <asp:Label ID="lbl1" runat="server" style="position:relative; top:5px;" Text=" Status"></asp:Label>--%>
                                                        <%--<asp:CheckBox ID="ckStatus_OK" runat="server" /><span>OK</span>&nbsp;
                                                        <asp:CheckBox ID="ckStatus_Abnormal" runat="server" /><span>Abnormal</span>&nbsp;
                                                        <asp:CheckBox ID="ckStatus_ClassA" runat="server" /><span>Class A</span>&nbsp;--%>
                                                        <%--<asp:CheckBox ID="ckStatus_ClassB" runat="server" /><span>Class B</span>&nbsp;
                                                        <asp:CheckBox ID="ckStatus_ClassC" runat="server" /><span>Class C</span>&nbsp;
                                                        <asp:CheckBox ID="ckStatus_ClassD" runat="server" /><span>Class D</span>&nbsp;--%>


                                                        <div class="clear"></div>
                                                    </div>



                                                    <div class="content-box-content">


                                                        <asp:Panel ID="Panel2" runat="server">

                                                            <!-- This is the target div. id must match the href of this div's tab -->


                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <%--<asp:CheckBox ID="chkAll" runat="server" /><a href="#"> </a>--%><a href="#">Select</a></th>
                                                                        <th><a href="#">Code </a></th>
                                                                        <th><a href="#">Tag Name </a></th>
                                                                        <th><a href="#">Equipement-Type</a></th>
                                                                        <th><a href="#">Status</a></th>
                                                                        <th><a href="#">Spec</a></th>
                                                                    </tr>
                                                                </thead>

                                                                <asp:Repeater ID="rptTag" runat="server">
                                                                    <HeaderTemplate>

                                                                        <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>

                                                                                <asp:CheckBox ID="chkItem" runat="server" CommandName="Edit" OnCheckedChanged="chkItem_OnCheckedChanged" AutoPostBack="true" /></td>
                                                                            <%--<td>
                                                                                <asp:Label ID="lblTagNo" runat="server"></asp:Label></td>--%>
                                                                            <td style="width: 100px;"><a href="javascript:;" id="ContentPlaceHolder1_rptTag_lblTagNo_0" style="font-weight: bold; color: gray;" onclick="document.getElementById(&#39;ContentPlaceHolder1_rptInspection_GLT_1_btnExpand_1&#39;).click();">
                                                                                <asp:Label ID="lblTagNo" runat="server"></asp:Label></a></td>
                                                                            <td>
                                                                                <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                                                                <asp:Label ID="lblTo_Table" runat="server" Visible="false"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblTagType" runat="server"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                                                            <td>
                                                                                <!-- Icons -->
                                                                                <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />

                                                                            </td>
                                                                        </tr>


                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td colspan="6">
                                                                            <div class="bulk-actions align-left">
                                                                                <%--<asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>--%>
                                                                            </div>
                                                                            <uc1:PageNavigation ID="Navigation" runat="server" />
                                                                            <!-- End .pagination -->
                                                                            <div class="clear"></div>
                                                                        </td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>

                                                            <div class="clear"></div>
                                                            
                                                             

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </p>
                            <p></p>

                            <uc4:MS_ST_DialogAbsorber_Spec ID="DialogAbsorber" runat="server" />
                                                            <uc5:MS_ST_DialogColumn_Spec ID="DialogColumn" runat="server" />
                                                            <uc6:MS_ST_DialogFilter_Spec ID="DialogFilter" runat="server" />
                                                            <uc7:MS_ST_DialogDrum_Spec ID="DialogDrum" runat="server" />
                                                            <uc8:MS_ST_DialogHeat_Exchnager_Spec ID="DialogHeat_Exchnager" runat="server" />
                          </fieldset>  
                    </asp:Panel>

                </div>
                <!-- End #tab1 -->

            </div>


            <%-- </td>
                                        </tr>
                                    </tbody>


                                </table>
                            </p>--%>

            <%--SELECT TAG--%>
            <%--                                <div class="content-box">
                                    <!-- Start Content Box -->
                                    <!-- End .content-box-header -->
                                    <div class="content-box-header">
                                        <h3>TAG </h3>

                                        <div class="clear"></div>
                                    </div>



                                    <div class="content-box-content">

                                        <!-- This is the target div. id must match the href of this div's tab -->
                                        <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                                            <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                                            <div>
                                                <asp:Label ID="lblBindingError" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlListPlan" runat="server">

                                            <!-- This is the target div. id must match the href of this div's tab -->
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <asp:CheckBox ID="ckTag_Header" runat="server" /><a href="#">SELECT</a></th>
                                                        <th><a href="#">Code</a></th>
                                                        <th><a href="#">Name</a></th>
                                                        <th><a href="#">Type</a></th>
                                                        <th><a href="#">Status</a></th>
                                                        <th><a href="#">Action</a></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ckTag_List" runat="server" /></td>
                                                        <td>
                                                            <asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblYear" runat="server"></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblRoute" runat="server"></asp:Label></td>

                                                        <td>
                                                            <!-- Icons -->
                                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                                            <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                                            <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnToggle" ConfirmText="Are you sure to delete this report permanently?" />

                                                            <asp:Button ID="btnUpdateRound" CommandName="Round" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" />

                                                        </td>
                                                    </tr>


                                                </tbody>



                                            </table>

                                        </asp:Panel>

                                    </div>
                                    <!-- End #tab1 -->

                                </div>--%>

            <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">
                <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                <div>
                    <asp:Label ID="lblBindingSuccess" runat="server"></asp:Label>
                </div>
            </asp:Panel>
            <asp:Button ID="btnSelect" runat="Server" style="display:none;" />
            <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                <div>
                    <asp:Label ID="lblValidation" runat="server"></asp:Label>
                </div>
            </asp:Panel>
















           <%-- <uc4:MS_ST_DialogAbsorber_Spec ID="DialogAbsorber" runat="server" />
            <uc5:MS_ST_DialogColumn_Spec ID="DialogColumn" runat="server" />
            <uc6:MS_ST_DialogFilter_Spec ID="DialogFilter" runat="server" />
            <uc7:MS_ST_DialogDrum_Spec ID="DialogDrum" runat="server" />
            <uc8:MS_ST_DialogHeat_Exchnager_Spec ID="DialogHeat_Exchnager" runat="server" />--%>


            <p align="right">
                <asp:LinkButton ID="btnCreatePlan" runat="server" CssClass="button"
                    Text="Create Plan"></asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="button" Text="Cancel"></asp:LinkButton>

            </p>
            <%-- </fieldset>

                    </asp:Panel>

                </div>
                <!-- End #tab1 -->

            </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

