﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_CUI_Summary.aspx.vb" Inherits="EIR.PIPE_CUI_Summary" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="~/UC_Pipe_Tag_Info.ascx" TagPrefix="uc1" TagName="UC_Pipe_Tag_Info" %>
<%@ Register Src="~/UC_Pipe_Point_Info.ascx" TagPrefix="uc1" TagName="UC_Pipe_Point_Info" %>
<%@ Register Src="~/UC_FileAlbum.ascx" TagPrefix="uc1" TagName="UC_FileAlbum" %>


<asp:Content ID="ContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
<!-- Page Head -->
	<h2>Corrosion Under Insulation Report</h2>
	
	<div class="clear"></div> <!-- End .clear -->
    <div class="content-box"><!-- Start Content Box -->
        <div class="content-box-header" style="height:auto; padding-top:5px; padding-bottom:5px;">
                <h3>Display Condition</h3>
                
				        <asp:DropDownList CssClass="select"
                          ID="ddl_Search_Year" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
    						
						<asp:DropDownList CssClass="select"
                          ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                        
                        <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Area" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" 
                      ID="ddl_Search_Process" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                      <asp:DropDownList CssClass="select"
                      ID="ddl_Search_Service" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
  					
                        <asp:DropDownList CssClass="select"
                          ID="ddl_Search_Step" runat="server" AutoPostBack="True">
                        </asp:DropDownList>

    					<asp:DropDownList CssClass="select"
                          ID="ddl_Search_Status" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="-1" Text="All status"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Inspecting"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Finished"></asp:ListItem>
                        </asp:DropDownList>
                        
                        <asp:TextBox runat="server" ID="txt_Search_Code" AutoPostBack="True" CssClass="text-input small-input " 
                            Width="200px" MaxLength="50" PlaceHolder="Search for Tag code..."></asp:TextBox>                        
            		    
                       &nbsp; &nbsp;  <asp:CheckBox ID="chk_Search_Edit" runat="server" AutoPostBack="True" style="position:relative; top:10px;" Text="" /> 
                     &nbsp; <b style="position:relative; top:8px;" id="lblEditable" runat="server">Editable</b>
                  <div class="clear"></div>
                </div>
                <div class="content-box-content">
                    <div class="tab-content default-tab" id="tab1">
                        <!-- This is the target div. id must match the href of this div's tab -->
                        <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                          <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                          <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                      </asp:Panel>
                      <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                        <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                    <table>
                        <thead>
                          <tr>
                            <th><a href="javascript:;">Report No.</a> </th>
                            <th><a href="javascript:;">Plant</a></th>
                            <th><a href="javascript:;">Area</a></th>
                            <th><a href="javascript:;">Tag Code</a></th>
                            <th><a href="javascript:;">Point</a></th>
                            <th style="text-align:center;"><a href="javascript:;">Remaining Life<br />(Years)</a></th>
                            <th><a href="javascript:;">Started</a></th>
                            <th colspan="2" style="text-align:center;"><a href="javascript:;">Step</a></th>
                            <th><a href="javascript:;">Action</a></th>
                          </tr>
                        </thead>
                   
                        <asp:Repeater ID="rptReport" runat="server">
                               <HeaderTemplate>                           
                                <tbody>
                               </HeaderTemplate>
                               <ItemTemplate>
                                  <tr>
                                    <td style="text-align:center;"><asp:Label ID="lblRptNo" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblArea" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPoint" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblLife" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblStart" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblStep" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblFinish" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                      <asp:Image ID="imgLock" runat="server" ImageUrl="resources/images/icons/lock.png" BorderStyle="None" ToolTip="Lock" />
                                      <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="resources/images/icons/pencil.png" BorderStyle="None" ToolTip="Edit Report" CommandName="Edit" />
                                      <a ID="btnReport" runat="server" href="javascript:;"><img src="resources/images/icons/printer.png" border="0" alt="print" /></a>
                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" />
                                        <cc1:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete ?" TargetControlID="btnDelete"></cc1:ConfirmButtonExtender>
                                    </td>
                                  </tr>                              
                        </ItemTemplate>
                        <FooterTemplate>
                         </tbody>
                        </FooterTemplate>
                       </asp:Repeater>
                       <tfoot>
                          <tr>
                            <td colspan="9">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create Report"></asp:LinkButton>
                            </div>
                                <uc1:PageNavigation ID="Navigation" runat="server" />
                              <!-- End .pagination -->
                              <div class="clear"></div>  
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                </div>
         </div>
<asp:Panel ID="dialogCreateReport" runat="server" Visible="false">


    <div class="MaskDialog"></div>
	<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture" style="width:90%; position:absolute; left:5%; height:auto; ">
            <h3 style="width:100%; text-align:left;">Create CUI Report for 
                <asp:DropDownList runat="server" ID="ddl_Select_Year" CssClass="select"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddl_Select_Plant" CssClass="select" AutoPostBack="true"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddl_Select_Area" CssClass="select" AutoPostBack="true"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddl_Select_Tag" CssClass="select " AutoPostBack="true"></asp:DropDownList>                
                <asp:DropDownList runat="server" ID="ddl_Select_Point" CssClass="select " AutoPostBack="true"></asp:DropDownList>                
            </h3>
           
            <table cellpadding="0" cellspacing="0">
                <tbody style="border-bottom:none;">                         
                        <tr>					            
					        <td colspan="2">
                                <uc1:UC_Pipe_Tag_Info runat="server" ID="Pipe_Info" />
					        </td>					        
					    </tr>
                        <tr>					            
					        <td colspan="2">
                                <div style="width:70%; display:inline-block; float:left;">
                                     <uc1:UC_Pipe_Point_Info runat="server" ID="Point_Info" />
                                </div>
                                <div style="width:30%; display:inline-block; float:right">
                                    <uc1:UC_FileAlbum runat="server" ID="FileAlbum" />
                                </div>                               
					        </td>					        
					    </tr>
                        <tr >
                            <td style="border:none;">
                                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg" >
                                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                    <div>
                                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td style="text-align:right; border:none;">
                                <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" />
                                &nbsp;
                                <asp:Button ID="btnOK" runat="server" Class="button" Text="Create Report" />

                            </td>
                        </tr>
                    </tbody>
             </table>
                    
			
	</asp:Panel>

</asp:Panel>

</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>
