﻿Imports System.IO
Public Class RenderDocumentPlanPaperUploadFile
    Inherits System.Web.UI.Page

    Dim CL As New LawClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Files.Count = 0 Then Exit Sub

        Dim fileContent As HttpPostedFile = Request.Files(0)
        Dim OriginalFileName As String = fileContent.FileName

        Dim TempPath As String = CL.UploadTempPath() & Session("USER_Name") & "\"
        If Directory.Exists(TempPath) = False Then
            Directory.CreateDirectory(TempPath)
        End If

        Dim FilePath As String = TempPath & OriginalFileName
        If File.Exists(FilePath) = True Then
            'ถ้ามีไฟล์เดิมอยู่แล้ว ก็ลบออกก่อน
            File.Delete(FilePath)
        End If

        'บันทึกไฟล์ที่ทำการ Upload
        fileContent.SaveAs(FilePath)
        If File.Exists(FilePath) = True Then
            Response.Write(OriginalFileName & "#" & FilePath)
        End If
    End Sub

End Class