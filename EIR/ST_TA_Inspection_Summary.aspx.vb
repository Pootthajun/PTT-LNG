﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ST_TA_Inspection_Summary
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Turnaround_Inspection_Reports        'Turnaround Inspection Reports  11

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        'HideValidator()

        If Not IsPostBack Then
            ClearPanelSearch()
            SetUserAuthorization()
        End If


    End Sub

    Private Sub SetUserAuthorization()
        'btnCreate.Visible = USER_LEVEL = EIR_BL.User_Level.Inspector Or USER_LEVEL = EIR_BL.User_Level.Administrator ' Only Inspector Can Create Plan 
    End Sub


    Private Sub BindPlan()

        'Dim SQL As String = " SELECT * FROM VW_REPORT_ST_TA_HEADER "


        Dim SQL As String = " " & vbNewLine
        SQL &= " SELECT 							   " & vbNewLine
        SQL &= " RPT_CODE,						   " & vbNewLine
        SQL &= " HEADER.RPT_Year,				   " & vbNewLine
        SQL &= " HEADER.RPT_No,					   " & vbNewLine
        SQL &= " HEADER.PLANT_Code,				   " & vbNewLine
        SQL &= " HEADER.PLANT_ID,				   " & vbNewLine
        SQL &= " HEADER.TAG_TYPE_Name,			   " & vbNewLine
        SQL &= " HEADER.STEP_NAME	 ,			   " & vbNewLine
        SQL &= " HEADER.RPT_STEP	 ,				   " & vbNewLine
        SQL &= " HEADER.RPT_LOCK_BY	 ,			   " & vbNewLine
        SQL &= " HEADER.Created_Time ,			   " & vbNewLine
        SQL &= " HEADER.RPT_Period_Start ,			   " & vbNewLine
        SQL &= " HEADER.Lock_Name ,				   " & vbNewLine
        SQL &= " COUNT (Detail.DETAIL_ID) CountTag  " & vbNewLine
        SQL &= " FROM VW_REPORT_ST_TA_HEADER HEADER " & vbNewLine
        SQL &= " LEFT JOIN RPT_ST_TA_Detail Detail ON HEADER.RPT_Year= Detail.RPT_Year AND HEADER.RPT_No = Detail.RPT_No  " & vbNewLine


        'SQL &= " WHERE HEADER.RPT_Type_ID=11 AND  " & vbNewLine
        'SQL &= " HEADER.RPT_Year=2559 			 " & vbNewLine










        Dim WHERE As String = "WHERE HEADER.RPT_Type_ID=" & RPT_Type_ID & " AND " & vbNewLine

        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " HEADER.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If

        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " HEADER.RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If txt_Search_Start.Text <> "" Then
            WHERE &= " HEADER.RPT_Period_Start>='" & txt_Search_Start.Text & "' AND " & vbNewLine
        End If
        If txt_Search_End.Text <> "" Then
            WHERE &= " HEADER.RPT_Period_Start<='" & txt_Search_End.Text & "' AND " & vbNewLine
        End If
        If ddl_Search_Status.SelectedIndex > 0 Then
            WHERE &= " HEADER.RPT_STEP=" & ddl_Search_Status.Items(ddl_Search_Status.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If chk_Search_Edit.Checked Then
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    WHERE &= " HEADER.RPT_STEP IN (0,1) AND (HEADER.RPT_LOCK_BY IS NULL OR HEADER.RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
                Case EIR_BL.User_Level.Inspector
                    WHERE &= " HEADER.RPT_STEP = 2 AND (HEADER.RPT_LOCK_BY IS NULL OR HEADER.RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
                Case EIR_BL.User_Level.Approver
                    WHERE &= " HEADER.RPT_STEP = 3 AND (HEADER.RPT_LOCK_BY IS NULL OR HEADER.RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
            End Select
        End If

        SQL &= WHERE.Substring(0, WHERE.Length - 6) & vbNewLine
        'SQL &= " ORDER BY CASE RPT_CODE WHEN '????' THEN 'Z' ELSE RPT_CODE END,RPT_Round" & vbNewLine


        SQL &= " GROUP BY 						 " & vbNewLine
        SQL &= " RPT_CODE,						 " & vbNewLine
        SQL &= " HEADER.RPT_Year,				 " & vbNewLine
        SQL &= " HEADER.RPT_No,					 " & vbNewLine
        SQL &= " HEADER.PLANT_Code,				 " & vbNewLine
        SQL &= " HEADER.PLANT_ID,				 " & vbNewLine
        SQL &= " HEADER.TAG_TYPE_Name,			 " & vbNewLine
        SQL &= " HEADER.STEP_NAME	 ,			 " & vbNewLine
        SQL &= " HEADER.RPT_STEP	 ,				 " & vbNewLine
        SQL &= " HEADER.RPT_LOCK_BY	 ,			 " & vbNewLine
        SQL &= " HEADER.RPT_Period_Start ,		 " & vbNewLine
        SQL &= " HEADER.Created_Time ,			 " & vbNewLine
        SQL &= " HEADER.Lock_Name 				 " & vbNewLine
        SQL &= " ORDER BY CASE HEADER.RPT_CODE WHEN '????' THEN 'Z' ELSE HEADER.RPT_CODE END  " & vbNewLine



        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("MS_Stationary_Routine_Summary") = DT

        Navigation.SesssionSourceName = "MS_Stationary_Routine_Summary"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblType As Label = e.Item.FindControl("lblType")
        'Dim lblRound As Label = e.Item.FindControl("lblRound")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")
        Dim lblNormal As Label = e.Item.FindControl("lblNormal")
        Dim lblAbnormal As Label = e.Item.FindControl("lblAbnormal")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblPlanPeriod As Label = e.Item.FindControl("lblPlanPeriod")
        Dim lblActualStart As Label = e.Item.FindControl("lblActualStart")

        Dim imgLock As Image = e.Item.FindControl("imgLock")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnCreate As ImageButton = e.Item.FindControl("btnCreate")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")
        Dim cfbCreate As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbCreate")

        lblRptNo.Text = e.Item.DataItem("RPT_Code")

        lblRptNo.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        lblRptNo.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        If Not IsDBNull(e.Item.DataItem("PLANT_Code")) Then
            lblPlant.Text = e.Item.DataItem("PLANT_Code")
        End If
        If Not IsDBNull(e.Item.DataItem("TAG_TYPE_Name")) Then
            lblType.Text = e.Item.DataItem("TAG_TYPE_Name")
        End If



        'If e.Item.DataItem("RPT_No") > 0 Then
        '    lblNormal.Text = FormatNumber(e.Item.DataItem("Normal"), 0)
        '    lblAbnormal.Text = FormatNumber(e.Item.DataItem("Problem"), 0)
        'Else
        '    lblNormal.Text = "-"
        '    lblAbnormal.Text = "-"
        'End If

        lblTotal.Text = FormatNumber(e.Item.DataItem("CountTag"), 0)

        lblStatus.Text = e.Item.DataItem("STEP_NAME")
        lblStatus.Attributes("RPT_STEP") = e.Item.DataItem("RPT_STEP")
        If Not IsDBNull(e.Item.DataItem("RPT_LOCK_BY")) Then
            lblStatus.Attributes("RPT_LOCK_BY") = e.Item.DataItem("RPT_LOCK_BY")
        Else
            lblStatus.Attributes("RPT_LOCK_BY") = -1
        End If

        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        lblPlanPeriod.Text = BL.ReportProgrammingDate(e.Item.DataItem("RPT_Period_Start"))
        If Not IsDBNull(e.Item.DataItem("Created_Time")) Then
            lblActualStart.Text = BL.ReportProgrammingDate(e.Item.DataItem("Created_Time"))
            If Int(CDate(e.Item.DataItem("Created_Time")).ToOADate) > Int(CDate(e.Item.DataItem("RPT_Period_Start")).ToOADate) Then
                lblActualStart.CssClass = BL.Get_Inspection_Css_Text_By_Level(EIR_BL.InspectionLevel.ClassB)
                lblActualStart.ToolTip = "Late"
            Else
                lblActualStart.CssClass = BL.Get_Inspection_Css_Text_By_Level(EIR_BL.InspectionLevel.Normal)
                lblActualStart.ToolTip = "On-Time"
            End If
        End If

        '------------------------- Set for Action Premission -------------------------
        btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnCreate.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnCreate.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnReport.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"

        Dim RPT_STEP As EIR_BL.Report_Step = e.Item.DataItem("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")



        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY

            If .CanEdit And e.Item.DataItem("RPT_No") < 0 And RPT_STEP = EIR_BL.Report_Step.New_Step Then
                btnCreate.Visible = True
                cfbCreate.TargetControlID = btnCreate.ID
                cfbCreate.ConfirmText = "Do you want to start create this report!?"
                btnEdit.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And RPT_LOCK_BY = -1 Then
                btnEdit.Visible = True
                btnEdit.ImageUrl = btnCreate.ImageUrl
                btnCreate.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And RPT_LOCK_BY = Session("USER_ID") Then
                btnEdit.Visible = True
                btnCreate.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And Session("USER_ID") = 0 Then
                btnEdit.ImageUrl = btnCreate.ImageUrl
                btnEdit.Visible = True
                btnCreate.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = RPT_LOCK_BY <> -1
                '---------------- Add Locked By Detail- ------------------
                If Not IsDBNull(e.Item.DataItem("Lock_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_Name")
                End If
            ElseIf RPT_LOCK_BY > 0 And RPT_LOCK_BY <> Session("USER_ID") Then
                btnEdit.Visible = False
                btnCreate.Visible = False
                imgLock.Visible = True
                If Not IsDBNull(e.Item.DataItem("Lock_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_Name")
                End If
            Else
                btnCreate.Visible = False
                btnEdit.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = True
                '---------------- Add Locked Detail-------------------
                imgLock.ToolTip = "Lock by workflow"
            End If
            btnReport.Visible = .CanPreview
        End With

    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
        Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")
        '------- Check First --------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_TA_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            lblBindingError.Text = "This report has been removed."
            pnlBindingError.Visible = True
            Exit Sub
        End If

        '---------------------- Set Available -----------------------
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim RPT_STEP As EIR_BL.Report_Step = lblStatus.Attributes("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY

            If Not .CanEdit Then
                lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
                lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
                lblBindingError.Text &= "<li>This report has been locked by others"
                pnlBindingError.Visible = True
                Exit Sub
            End If
        End With

        '--------------Check Previous Incomplete Report ---------
        Dim PT As DataTable = BL.Check_Previous_Incomplete_ST_TA_Report(RPT_Year, RPT_No)
        If PT.Rows.Count > 0 Then
            Dim Col() As String = {"RPT_CODE", "RPT_Type_ID"}
            PT.DefaultView.Sort = "RPT_CODE"
            PT = PT.DefaultView.ToTable(True, Col).Copy
            Dim ReportNo As String = ""
            For r As Integer = 0 To PT.Rows.Count - 1
                Dim Y As String = "25" & PT.Rows(r).Item("RPT_CODE").ToString.Substring(7, 2)
                Dim N As Integer = PT.Rows(r).Item("RPT_CODE").ToString.Substring(10)
                If PT.Rows(r).Item("RPT_Type_ID") = EIR_BL.Report_Type.Turnaround_Inspection_Reports Then
                    ReportNo &= ", <a href='ST_TA_Inspection_Edit2.aspx?RPT_Year=" & Y & "&RPT_No=" & N & "' target='_blank' title='Click to see more detail'>" & PT.Rows(r).Item("RPT_CODE") & "</a>"
                Else
                    ReportNo &= ", <a href='ST_TA_Inspection_Edit2.aspx?RPT_Year=" & Y & "&RPT_No=" & N & "' target='_blank' title='Click to see more detail'>" & PT.Rows(r).Item("RPT_CODE") & "</a>"
                End If
            Next
            lblBindingError.Text = "This report cannot be edited due to follow these reasons<br>" & vbNewLine
            lblBindingError.Text &= "<li>Some of previous report has not completed to updating information<li>Try to check report <b>" & ReportNo.Substring(1) & "</b>" & vbNewLine
            pnlBindingError.Visible = True
            Exit Sub
        End If


        '-------------Update Activated Report-----------

        '---------------------------- Add Report Header Info----------------------
        SQL = "SELECT * FROM RPT_ST_TA_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        '------------Update Detail---ก่อน Gen รหัส--------------------
        Dim Old_RPT_No As Integer = RPT_No



        If DT.Rows.Count = 0 Then
            lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
            lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
            lblBindingError.Text &= "<li>This report has been locked by others"
            pnlBindingError.Visible = True
            Exit Sub
        End If
        If RPT_No <= 0 Then
            RPT_No = BL.GetNewReportNumber(RPT_Year)
            DT.Rows(0).Item("RPT_No") = RPT_No
        End If

        Dim Period As Date = DT.Rows(0).Item("RPT_Period_Start")
        If IsDBNull(DT.Rows(0).Item("RPT_Subject")) Then
            DT.Rows(0).Item("RPT_Subject") = "รายงานการตรวจสอบ Turnaround Inspection for Stationary Equipments "
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_To")) Then
            DT.Rows(0).Item("RPT_To") = "ผจ.ตร."
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_Cause")) Then
            DT.Rows(0).Item("RPT_Cause") = "ตามแผนงานการตรวจสอบสภาพอุปกรณ์ของ Turnaround Equipments ประจำปี " & RPT_Year & " เดือน " & MonthThai(Period.Month)
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            DT.Rows(0).Item("RPT_Result") = ""
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_STEP")) OrElse DT.Rows(0).Item("RPT_STEP") <= 0 Then
            DT.Rows(0).Item("RPT_STEP") = 1
        End If
        If IsDBNull(DT.Rows(0).Item("Created_By")) Then
            DT.Rows(0).Item("Created_By") = Session("USER_ID")
        End If
        If IsDBNull(DT.Rows(0).Item("Created_Time")) Then
            DT.Rows(0).Item("Created_Time") = Now
        End If

        If Session("USER_ID") <> 0 Then
            If IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                DT.Rows(0).Item("RPT_LOCK_BY") = Session("USER_ID")
            End If
        End If

        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)


        '------------Update Detail-----------------------

        Dim COM As New SqlCommand
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        COM.Connection = Conn
        COM.CommandType = CommandType.Text
        SQL = "UPDATE RPT_ST_TA_Detail SET RPT_No=" & RPT_No
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & Old_RPT_No
        COM.CommandText = SQL
        COM.ExecuteNonQuery()



        'If RPT_STEP <> 4 Then
        '    '---------------------------- Add Report Detail Info----------------------
        '    BL.Construct_ST_Report_Detail(Session("USER_ID"), RPT_Year, RPT_No)
        'End If




        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit1.aspx?" & Param & "';", True)
    End Sub




    'Private Sub BindPlan()

    '    'Dim SQL As String = " SELECT * FROM VW_REPORT_ST_TA_HEADER "


    '    Dim SQL As String = " " & vbNewLine
    '    SQL &= " SELECT 							   " & vbNewLine
    '    SQL &= " RPT_CODE,						   " & vbNewLine
    '    SQL &= " HEADER.RPT_Year,				   " & vbNewLine
    '    SQL &= " HEADER.RPT_No,					   " & vbNewLine
    '    SQL &= " HEADER.PLANT_Code,				   " & vbNewLine
    '    SQL &= " HEADER.PLANT_ID,				   " & vbNewLine
    '    SQL &= " HEADER.TAG_TYPE_Name,			   " & vbNewLine
    '    SQL &= " HEADER.STEP_NAME	 ,			   " & vbNewLine
    '    SQL &= " HEADER.RPT_STEP	 ,				   " & vbNewLine
    '    SQL &= " HEADER.RPT_LOCK_BY	 ,			   " & vbNewLine
    '    SQL &= " HEADER.Created_Time ,			   " & vbNewLine
    '    SQL &= " HEADER.RPT_Period_Start ,			   " & vbNewLine
    '    SQL &= " HEADER.Lock_Name ,				   " & vbNewLine
    '    SQL &= " COUNT (Detail.DETAIL_TAG_ID) CountTag  " & vbNewLine
    '    SQL &= " FROM VW_REPORT_ST_TA_HEADER HEADER " & vbNewLine
    '    SQL &= " LEFT JOIN RPT_ST_TA_TAG Detail ON HEADER.RPT_Year= Detail.RPT_Year AND HEADER.RPT_No = Detail.RPT_No  " & vbNewLine


    '    'SQL &= " WHERE HEADER.RPT_Type_ID=11 AND  " & vbNewLine
    '    'SQL &= " HEADER.RPT_Year=2559 			 " & vbNewLine










    '    Dim WHERE As String = "WHERE HEADER.RPT_Type_ID=" & RPT_Type_ID & " AND " & vbNewLine

    '    If ddl_Search_Plant.SelectedIndex > 0 Then
    '        WHERE &= " HEADER.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
    '    End If

    '    If ddl_Search_Year.SelectedIndex > 0 Then
    '        WHERE &= " HEADER.RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
    '    End If
    '    If txt_Search_Start.Text <> "" Then
    '        WHERE &= " HEADER.RPT_Period_Start>='" & txt_Search_Start.Text & "' AND " & vbNewLine
    '    End If
    '    If txt_Search_End.Text <> "" Then
    '        WHERE &= " HEADER.RPT_Period_Start<='" & txt_Search_End.Text & "' AND " & vbNewLine
    '    End If
    '    If ddl_Search_Status.SelectedIndex > 0 Then
    '        WHERE &= " HEADER.RPT_STEP=" & ddl_Search_Status.Items(ddl_Search_Status.SelectedIndex).Value & " AND " & vbNewLine
    '    End If
    '    If chk_Search_Edit.Checked Then
    '        Select Case USER_LEVEL
    '            Case EIR_BL.User_Level.Collector
    '                WHERE &= " HEADER.RPT_STEP IN (0,1) AND (HEADER.RPT_LOCK_BY IS NULL OR HEADER.RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
    '            Case EIR_BL.User_Level.Inspector
    '                WHERE &= " HEADER.RPT_STEP = 2 AND (HEADER.RPT_LOCK_BY IS NULL OR HEADER.RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
    '            Case EIR_BL.User_Level.Approver
    '                WHERE &= " HEADER.RPT_STEP = 3 AND (HEADER.RPT_LOCK_BY IS NULL OR HEADER.RPT_LOCK_BY=" & Session("USER_ID") & ") AND " & vbNewLine
    '        End Select
    '    End If

    '    SQL &= WHERE.Substring(0, WHERE.Length - 6) & vbNewLine
    '    'SQL &= " ORDER BY CASE RPT_CODE WHEN '????' THEN 'Z' ELSE RPT_CODE END,RPT_Round" & vbNewLine


    '    SQL &= " GROUP BY 						 " & vbNewLine
    '    SQL &= " RPT_CODE,						 " & vbNewLine
    '    SQL &= " HEADER.RPT_Year,				 " & vbNewLine
    '    SQL &= " HEADER.RPT_No,					 " & vbNewLine
    '    SQL &= " HEADER.PLANT_Code,				 " & vbNewLine
    '    SQL &= " HEADER.PLANT_ID,				 " & vbNewLine
    '    SQL &= " HEADER.TAG_TYPE_Name,			 " & vbNewLine
    '    SQL &= " HEADER.STEP_NAME	 ,			 " & vbNewLine
    '    SQL &= " HEADER.RPT_STEP	 ,				 " & vbNewLine
    '    SQL &= " HEADER.RPT_LOCK_BY	 ,			 " & vbNewLine
    '    SQL &= " HEADER.RPT_Period_Start ,		 " & vbNewLine
    '    SQL &= " HEADER.Created_Time ,			 " & vbNewLine
    '    SQL &= " HEADER.Lock_Name 				 " & vbNewLine
    '    SQL &= " ORDER BY CASE HEADER.RPT_CODE WHEN '????' THEN 'Z' ELSE HEADER.RPT_CODE END  " & vbNewLine



    '    Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
    '    Dim DT As New DataTable

    '    Try
    '        DA.Fill(DT)
    '    Catch ex As Exception
    '        pnlBindingError.Visible = True
    '        lblBindingError.Text = ex.Message
    '        Exit Sub
    '    End Try
    '    pnlBindingError.Visible = False

    '    Session("MS_Stationary_Routine_Summary") = DT

    '    Navigation.SesssionSourceName = "MS_Stationary_Routine_Summary"
    '    Navigation.RenderLayout()

    'End Sub

    'Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
    '    Navigation.TheRepeater = rptPlan
    'End Sub

    'Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
    '    If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

    '    Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
    '    Dim lblPlant As Label = e.Item.FindControl("lblPlant")
    '    Dim lblType As Label = e.Item.FindControl("lblType")
    '    'Dim lblRound As Label = e.Item.FindControl("lblRound")
    '    Dim lblTotal As Label = e.Item.FindControl("lblTotal")
    '    Dim lblNormal As Label = e.Item.FindControl("lblNormal")
    '    Dim lblAbnormal As Label = e.Item.FindControl("lblAbnormal")
    '    Dim lblStatus As Label = e.Item.FindControl("lblStatus")
    '    Dim lblPlanPeriod As Label = e.Item.FindControl("lblPlanPeriod")
    '    Dim lblActualStart As Label = e.Item.FindControl("lblActualStart")

    '    Dim imgLock As Image = e.Item.FindControl("imgLock")
    '    Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
    '    Dim btnCreate As ImageButton = e.Item.FindControl("btnCreate")
    '    Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")
    '    Dim cfbCreate As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbCreate")

    '    lblRptNo.Text = e.Item.DataItem("RPT_Code")

    '    lblRptNo.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
    '    lblRptNo.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
    '    If Not IsDBNull(e.Item.DataItem("PLANT_Code")) Then
    '        lblPlant.Text = e.Item.DataItem("PLANT_Code")
    '    End If
    '    If Not IsDBNull(e.Item.DataItem("TAG_TYPE_Name")) Then
    '        lblType.Text = e.Item.DataItem("TAG_TYPE_Name")
    '    End If



    '    'If e.Item.DataItem("RPT_No") > 0 Then
    '    '    lblNormal.Text = FormatNumber(e.Item.DataItem("Normal"), 0)
    '    '    lblAbnormal.Text = FormatNumber(e.Item.DataItem("Problem"), 0)
    '    'Else
    '    '    lblNormal.Text = "-"
    '    '    lblAbnormal.Text = "-"
    '    'End If

    '    lblTotal.Text = FormatNumber(e.Item.DataItem("CountTag"), 0)

    '    lblStatus.Text = e.Item.DataItem("STEP_NAME")
    '    lblStatus.Attributes("RPT_STEP") = e.Item.DataItem("RPT_STEP")
    '    If Not IsDBNull(e.Item.DataItem("RPT_LOCK_BY")) Then
    '        lblStatus.Attributes("RPT_LOCK_BY") = e.Item.DataItem("RPT_LOCK_BY")
    '    Else
    '        lblStatus.Attributes("RPT_LOCK_BY") = -1
    '    End If

    '    lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
    '    lblPlanPeriod.Text = BL.ReportProgrammingDate(e.Item.DataItem("RPT_Period_Start"))
    '    If Not IsDBNull(e.Item.DataItem("Created_Time")) Then
    '        lblActualStart.Text = BL.ReportProgrammingDate(e.Item.DataItem("Created_Time"))
    '        If Int(CDate(e.Item.DataItem("Created_Time")).ToOADate) > Int(CDate(e.Item.DataItem("RPT_Period_Start")).ToOADate) Then
    '            lblActualStart.CssClass = BL.Get_Inspection_Css_Text_By_Level(EIR_BL.InspectionLevel.ClassB)
    '            lblActualStart.ToolTip = "Late"
    '        Else
    '            lblActualStart.CssClass = BL.Get_Inspection_Css_Text_By_Level(EIR_BL.InspectionLevel.Normal)
    '            lblActualStart.ToolTip = "On-Time"
    '        End If
    '    End If

    '    '------------------------- Set for Action Premission -------------------------
    '    btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
    '    btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
    '    btnCreate.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
    '    btnCreate.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
    '    btnReport.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"

    '    Dim RPT_STEP As EIR_BL.Report_Step = e.Item.DataItem("RPT_STEP")
    '    Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
    '    Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")



    '    With BL.ReportPermissionManager
    '        .RPT_STEP = RPT_STEP
    '        .USER_LEVEL = USER_LEVEL
    '        .USER_ID = Session("USER_ID")
    '        .RPT_LOCK_BY = RPT_LOCK_BY

    '        If .CanEdit And e.Item.DataItem("RPT_No") < 0 And RPT_STEP = EIR_BL.Report_Step.New_Step Then
    '            btnCreate.Visible = True
    '            cfbCreate.TargetControlID = btnCreate.ID
    '            cfbCreate.ConfirmText = "Do you want to start create this report!?"
    '            btnEdit.Visible = False
    '            '------------ Set Lock Status ------------
    '            imgLock.Visible = False
    '        ElseIf .CanEdit And RPT_LOCK_BY = -1 Then
    '            btnEdit.Visible = True
    '            btnEdit.ImageUrl = btnCreate.ImageUrl
    '            btnCreate.Visible = False
    '            '------------ Set Lock Status ------------
    '            imgLock.Visible = False
    '        ElseIf .CanEdit And RPT_LOCK_BY = Session("USER_ID") Then
    '            btnEdit.Visible = True
    '            btnCreate.Visible = False
    '            '------------ Set Lock Status ------------
    '            imgLock.Visible = False
    '        ElseIf .CanEdit And Session("USER_ID") = 0 Then
    '            btnEdit.ImageUrl = btnCreate.ImageUrl
    '            btnEdit.Visible = True
    '            btnCreate.Visible = False
    '            '------------ Set Lock Status ------------
    '            imgLock.Visible = RPT_LOCK_BY <> -1
    '            '---------------- Add Locked By Detail- ------------------
    '            If Not IsDBNull(e.Item.DataItem("Lock_Name")) Then
    '                imgLock.ToolTip = e.Item.DataItem("Lock_Name")
    '            End If
    '        ElseIf RPT_LOCK_BY > 0 And RPT_LOCK_BY <> Session("USER_ID") Then
    '            btnEdit.Visible = False
    '            btnCreate.Visible = False
    '            imgLock.Visible = True
    '            If Not IsDBNull(e.Item.DataItem("Lock_Name")) Then
    '                imgLock.ToolTip = e.Item.DataItem("Lock_Name")
    '            End If
    '        Else
    '            btnCreate.Visible = False
    '            btnEdit.Visible = False
    '            '------------ Set Lock Status ------------
    '            imgLock.Visible = True
    '            '---------------- Add Locked Detail-------------------
    '            imgLock.ToolTip = "Lock by workflow"
    '        End If
    '        btnReport.Visible = .CanPreview
    '    End With

    'End Sub

    'Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
    '    If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

    '    Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
    '    Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
    '    Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")
    '    '------- Check First --------
    '    Dim SQL As String = "SELECT * FROM VW_REPORT_ST_TA_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
    '    Dim DT As New DataTable
    '    Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
    '    DA.Fill(DT)
    '    If DT.Rows.Count = 0 Then
    '        lblBindingError.Text = "This report has been removed."
    '        pnlBindingError.Visible = True
    '        Exit Sub
    '    End If

    '    '---------------------- Set Available -----------------------
    '    Dim lblStatus As Label = e.Item.FindControl("lblStatus")
    '    Dim RPT_STEP As EIR_BL.Report_Step = lblStatus.Attributes("RPT_STEP")
    '    Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
    '    Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

    '    With BL.ReportPermissionManager
    '        .RPT_STEP = RPT_STEP
    '        .USER_LEVEL = USER_LEVEL
    '        .USER_ID = Session("USER_ID")
    '        .RPT_LOCK_BY = RPT_LOCK_BY

    '        If Not .CanEdit Then
    '            lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
    '            lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
    '            lblBindingError.Text &= "<li>This report has been locked by others"
    '            pnlBindingError.Visible = True
    '            Exit Sub
    '        End If
    '    End With

    '    '--------------Check Previous Incomplete Report ---------
    '    Dim PT As DataTable = BL.Check_Previous_Incomplete_ST_TA_Report(RPT_Year, RPT_No)
    '    If PT.Rows.Count > 0 Then
    '        Dim Col() As String = {"RPT_CODE", "RPT_Type_ID"}
    '        PT.DefaultView.Sort = "RPT_CODE"
    '        PT = PT.DefaultView.ToTable(True, Col).Copy
    '        Dim ReportNo As String = ""
    '        For r As Integer = 0 To PT.Rows.Count - 1
    '            Dim Y As String = "25" & PT.Rows(r).Item("RPT_CODE").ToString.Substring(7, 2)
    '            Dim N As Integer = PT.Rows(r).Item("RPT_CODE").ToString.Substring(10)
    '            If PT.Rows(r).Item("RPT_Type_ID") = EIR_BL.Report_Type.Turnaround_Inspection_Reports Then
    '                ReportNo &= ", <a href='ST_TA_Inspection_Edit2.aspx?RPT_Year=" & Y & "&RPT_No=" & N & "' target='_blank' title='Click to see more detail'>" & PT.Rows(r).Item("RPT_CODE") & "</a>"
    '            Else
    '                ReportNo &= ", <a href='ST_TA_Inspection_Edit2.aspx?RPT_Year=" & Y & "&RPT_No=" & N & "' target='_blank' title='Click to see more detail'>" & PT.Rows(r).Item("RPT_CODE") & "</a>"
    '            End If
    '        Next
    '        lblBindingError.Text = "This report cannot be edited due to follow these reasons<br>" & vbNewLine
    '        lblBindingError.Text &= "<li>Some of previous report has not completed to updating information<li>Try to check report <b>" & ReportNo.Substring(1) & "</b>" & vbNewLine
    '        pnlBindingError.Visible = True
    '        Exit Sub
    '    End If


    '    '-------------Update Activated Report-----------

    '    '---------------------------- Add Report Header Info----------------------
    '    SQL = "SELECT * FROM RPT_ST_TA_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
    '    DA = New SqlDataAdapter(SQL, BL.ConnStr)
    '    DT = New DataTable
    '    DA.Fill(DT)

    '    '------------Update Detail---ก่อน Gen รหัส--------------------
    '    Dim Old_RPT_No As Integer = RPT_No



    '    If DT.Rows.Count = 0 Then
    '        lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
    '        lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
    '        lblBindingError.Text &= "<li>This report has been locked by others"
    '        pnlBindingError.Visible = True
    '        Exit Sub
    '    End If
    '    If RPT_No <= 0 Then
    '        RPT_No = BL.GetNewReportNumber(RPT_Year)
    '        DT.Rows(0).Item("RPT_No") = RPT_No
    '    End If

    '    Dim Period As Date = DT.Rows(0).Item("RPT_Period_Start")
    '    If IsDBNull(DT.Rows(0).Item("RPT_Subject")) Then
    '        DT.Rows(0).Item("RPT_Subject") = "รายงานการตรวจสอบ Turnaround Inspection for Stationary Equipments "
    '    End If
    '    If IsDBNull(DT.Rows(0).Item("RPT_To")) Then
    '        DT.Rows(0).Item("RPT_To") = "ผจ.ตร."
    '    End If
    '    If IsDBNull(DT.Rows(0).Item("RPT_Cause")) Then
    '        DT.Rows(0).Item("RPT_Cause") = "ตามแผนงานการตรวจสอบสภาพอุปกรณ์ของ Turnaround Equipments ประจำปี " & RPT_Year & " เดือน " & BL.ReportMonthThai(Period.Month)
    '    End If
    '    If IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
    '        DT.Rows(0).Item("RPT_Result") = ""
    '    End If
    '    If IsDBNull(DT.Rows(0).Item("RPT_STEP")) OrElse DT.Rows(0).Item("RPT_STEP") <= 0 Then
    '        DT.Rows(0).Item("RPT_STEP") = 1
    '    End If
    '    If IsDBNull(DT.Rows(0).Item("Created_By")) Then
    '        DT.Rows(0).Item("Created_By") = Session("USER_ID")
    '    End If
    '    If IsDBNull(DT.Rows(0).Item("Created_Time")) Then
    '        DT.Rows(0).Item("Created_Time") = Now
    '    End If

    '    If Session("USER_ID") <> 0 Then
    '        If IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
    '            DT.Rows(0).Item("RPT_LOCK_BY") = Session("USER_ID")
    '        End If
    '    End If

    '    Dim CMD As New SqlCommandBuilder(DA)
    '    DA.Update(DT)


    '    '------------Update Detail-----------------------

    '    Dim COM As New SqlCommand
    '    Dim Conn As New SqlConnection(BL.ConnStr)
    '    Conn.Open()
    '    COM.Connection = Conn
    '    COM.CommandType = CommandType.Text
    '    SQL = "UPDATE RPT_ST_TA_TAG SET RPT_No=" & RPT_No
    '    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & Old_RPT_No
    '    COM.CommandText = SQL
    '    COM.ExecuteNonQuery()



    '    'If RPT_STEP <> 4 Then
    '    '    '---------------------------- Add Report Detail Info----------------------
    '    '    BL.Construct_ST_Report_Detail(Session("USER_ID"), RPT_Year, RPT_No)
    '    'End If




    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit1.aspx?" & Param & "';", True)
    'End Sub



    Private Sub ClearPanelSearch()
        BL.BindDDlReportStep(ddl_Search_Status)
        BL.BindDDlPlant(ddl_Search_Plant)

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_ST_TA_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")

        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.SelectedIndex = ddl_Search_Year.Items.Count - 1

        txt_Search_Start.Text = ""
        txt_Search_End.Text = ""

        If USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Viewer Then
            If Not IsNothing(Request.QueryString("Editable")) AndAlso Request.QueryString("Editable") = "True" Then
                chk_Search_Edit.Checked = True
            End If
        Else
            chk_Search_Edit.Visible = False
            lblEditable.Visible = False
        End If

        BindPlan()
    End Sub

    Private Sub ddl_Search_Year_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Year.SelectedIndexChanged
        BindPlan()
    End Sub

    Private Sub ddl_Search_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BindPlan()
    End Sub

    Private Sub ddl_Search_Status_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Status.SelectedIndexChanged
        BindPlan()
    End Sub

    Private Sub txt_Search_Start_TextChanged(sender As Object, e As EventArgs) Handles txt_Search_Start.TextChanged
        BindPlan()
    End Sub

    Private Sub txt_Search_End_TextChanged(sender As Object, e As EventArgs) Handles txt_Search_End.TextChanged
        BindPlan()
    End Sub
End Class