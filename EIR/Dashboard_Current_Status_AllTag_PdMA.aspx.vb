﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Dashboard_Current_Status_AllTag_PdMA
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("PLANT_ID")) Then
                PLANT_ID = Request.QueryString("PLANT_ID")
            End If
            lblBack.Text = "Back to see all current status for " & lblPlant.Text
            lblBack.PostBackUrl = "Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & PLANT_ID
            BindDataPdMA()
            BindDataMTAP()
            DisplayChart()
        End If

    End Sub

    Private Sub BindDataPdMA()
        Dim SQL As String = " DECLARE @PLANT_ID INT=" & PLANT_ID & vbNewLine
        SQL &= "SELECT TAG.TAG_ID,TAG_Code," & vbNewLine
        SQL &= "Pwq_RPT_Year,Pwq_RPT_No,CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END Pwq," & vbNewLine
        SQL &= "Ins_RPT_Year,Ins_RPT_No,CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Ins," & vbNewLine
        SQL &= "Pwc_RPT_Year,Pwc_RPT_No,CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END Pwc," & vbNewLine
        SQL &= "Sta_RPT_Year,Sta_RPT_No,CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Sta," & vbNewLine
        SQL &= "Rot_RPT_Year,Rot_RPT_No,CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rot," & vbNewLine
        SQL &= "Air_RPT_Year,Air_RPT_No,CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END Air," & vbNewLine
        SQL &= "RPT_CODE,LAST_RPT_Year,LAST_RPT_No" & vbNewLine
        SQL &= "FROM MS_PDMA_TAG TAG " & vbNewLine
        SQL &= "LEFT JOIN MS_PDMA_Route ON TAG.ROUTE_ID = MS_PDMA_Route.ROUTE_ID" & vbNewLine
        SQL &= "LEFT JOIN MS_Plant ON MS_PDMA_Route.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Pwq_RPT_Year,RPT_No Pwq_RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= ") PWQ" & vbNewLine
        SQL &= "ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Ins_RPT_Year,RPT_No Ins_RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
        SQL &= ") INS" & vbNewLine
        SQL &= "ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Pwc_RPT_Year,RPT_No Pwc_RPT_No,PowerCircuit" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
        SQL &= ") PWC" & vbNewLine
        SQL &= "ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Sta_RPT_Year,RPT_No Sta_RPT_No,Stator" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
        SQL &= ") STA" & vbNewLine
        SQL &= "ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Rot_RPT_Year,RPT_No Rot_RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= ") ROT" & vbNewLine
        SQL &= "ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Air_RPT_Year,RPT_No Air_RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= ") AIR" & vbNewLine
        SQL &= "ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,LAST_RPT_Year,LAST_RPT_No,dbo.UDF_RPT_Code(LAST_RPT_Year, LAST_RPT_No) RPT_CODE" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY LAST_RPT_YEAR DESC,LAST_RPT_No DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year LAST_RPT_Year,RPT_No LAST_RPT_No" & vbNewLine
        SQL &= "		FROM RPT_PDMA_Detail" & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1" & vbNewLine
        SQL &= ") LAST_RPT" & vbNewLine
        SQL &= "ON TAG.TAG_ID = LAST_RPT.TAG_ID" & vbNewLine
        SQL &= "WHERE TAG.Active_Status = 1 AND MS_PDMA_Route.PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "ORDER BY TAG_Code" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        rptPdMA.DataSource = DT
        rptPdMA.DataBind()

        Session("Dashboard_Current_Status_AllTag_PdMA") = DT

    End Sub

    Protected Sub rptPdMA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPdMA.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblPwq As HtmlAnchor = e.Item.FindControl("lblPwq")
        Dim lblIns As HtmlAnchor = e.Item.FindControl("lblIns")
        Dim lblPwc As HtmlAnchor = e.Item.FindControl("lblPwc")
        Dim lblSta As HtmlAnchor = e.Item.FindControl("lblSta")
        Dim lblRot As HtmlAnchor = e.Item.FindControl("lblRot")
        Dim lblAir As HtmlAnchor = e.Item.FindControl("lblAir")
        Dim lblLastReport As HtmlAnchor = e.Item.FindControl("lblLastReport")

        lblTag.Text = e.Item.DataItem("TAG_Code").ToString

        If e.Item.DataItem("Pwq").ToString <> "" Then
            If e.Item.DataItem("Pwq") = 0 Then
                lblPwq.InnerHtml = "Normal"
                lblPwq.Style("color") = "Green"
                lblPwq.Style("cursor") = "pointer"
            Else
                lblPwq.InnerHtml = "Abnormal"
                lblPwq.Style("color") = "Red"
                lblPwq.Style("cursor") = "pointer"
            End If
            lblPwq.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Pwq_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Pwq_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Ins").ToString <> "" Then
            If e.Item.DataItem("Ins") = 0 Then
                lblIns.InnerHtml = "Normal"
                lblIns.Style("color") = "Green"
                lblIns.Style("cursor") = "pointer"
            Else
                lblIns.InnerHtml = "Abnormal"
                lblIns.Style("color") = "Red"
                lblIns.Style("cursor") = "pointer"
            End If
            lblIns.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Ins_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Ins_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Pwc").ToString <> "" Then
            If e.Item.DataItem("Pwc") = 0 Then
                lblPwc.InnerHtml = "Normal"
                lblPwc.Style("color") = "Green"
                lblPwc.Style("cursor") = "pointer"
            Else
                lblPwc.InnerHtml = "Abnormal"
                lblPwc.Style("color") = "Red"
                lblPwc.Style("cursor") = "pointer"
            End If
            lblPwc.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Pwc_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Pwc_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Sta").ToString <> "" Then
            If e.Item.DataItem("Sta") = 0 Then
                lblSta.InnerHtml = "Normal"
                lblSta.Style("color") = "Green"
                lblSta.Style("cursor") = "pointer"
            Else
                lblSta.InnerHtml = "Abnormal"
                lblSta.Style("color") = "Red"
                lblSta.Style("cursor") = "pointer"
            End If
            lblSta.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Sta_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Sta_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Rot").ToString <> "" Then
            If e.Item.DataItem("Rot") = 0 Then
                lblRot.InnerHtml = "Normal"
                lblRot.Style("color") = "Green"
                lblRot.Style("cursor") = "pointer"
            Else
                lblRot.InnerHtml = "Abnormal"
                lblRot.Style("color") = "Red"
                lblRot.Style("cursor") = "pointer"
            End If
            lblRot.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Rot_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Rot_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Air").ToString <> "" Then
            If e.Item.DataItem("Air") = 0 Then
                lblAir.InnerHtml = "Normal"
                lblAir.Style("color") = "Green"
                lblAir.Style("cursor") = "pointer"
            Else
                lblAir.InnerHtml = "Abnormal"
                lblAir.Style("color") = "Red"
                lblAir.Style("cursor") = "pointer"
            End If
            lblAir.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Air_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Air_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If

        If e.Item.DataItem("RPT_CODE").ToString <> "" Then
            lblLastReport.InnerHtml = e.Item.DataItem("RPT_CODE")
            lblLastReport.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Last_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Last_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.PdMA_Report & "','Dialog_Tag_PdMA_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            lblLastReport.Style("cursor") = "pointer"
        End If

    End Sub

    Private Sub BindDataMTAP()
        Dim SQL As String = " DECLARE @PLANT_ID INT=" & PLANT_ID & vbNewLine
        SQL &= "SELECT TAG.TAG_ID,TAG_Code," & vbNewLine
        SQL &= "Pwq_RPT_Year,Pwq_RPT_No,CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END Pwq," & vbNewLine
        SQL &= "Ins_RPT_Year,Ins_RPT_No,CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Ins," & vbNewLine
        SQL &= "Pwc_RPT_Year,Pwc_RPT_No,CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END Pwc," & vbNewLine
        SQL &= "Sta_RPT_Year,Sta_RPT_No,CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Sta," & vbNewLine
        SQL &= "Rot_RPT_Year,Rot_RPT_No,CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rot," & vbNewLine
        SQL &= "Air_RPT_Year,Air_RPT_No,CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END Air," & vbNewLine
        SQL &= "RPT_CODE,LAST_RPT_Year,LAST_RPT_No" & vbNewLine
        SQL &= "FROM MS_MTAP_TAG TAG " & vbNewLine
        SQL &= "LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Pwq_RPT_Year,RPT_No Pwq_RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= ") PWQ" & vbNewLine
        SQL &= "ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Ins_RPT_Year,RPT_No Ins_RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
        SQL &= ") INS" & vbNewLine
        SQL &= "ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Pwc_RPT_Year,RPT_No Pwc_RPT_No,PowerCircuit" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
        SQL &= ") PWC" & vbNewLine
        SQL &= "ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Sta_RPT_Year,RPT_No Sta_RPT_No,Stator" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
        SQL &= ") STA" & vbNewLine
        SQL &= "ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Rot_RPT_Year,RPT_No Rot_RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= ") ROT" & vbNewLine
        SQL &= "ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,RPT_Year Air_RPT_Year,RPT_No Air_RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
        SQL &= ") AIR" & vbNewLine
        SQL &= "ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,LAST_RPT_Year,LAST_RPT_No,dbo.UDF_RPT_Code(LAST_RPT_Year, LAST_RPT_No) RPT_CODE" & vbNewLine
        SQL &= "	FROM" & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY LAST_RPT_YEAR DESC,LAST_RPT_No DESC) ROW_NUM FROM " & vbNewLine
        SQL &= "		(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year LAST_RPT_Year,RPT_No LAST_RPT_No" & vbNewLine
        SQL &= "		FROM RPT_MTAP_Detail" & vbNewLine
        SQL &= "		) TB1" & vbNewLine
        SQL &= "	) TB2" & vbNewLine
        SQL &= "	WHERE ROW_NUM = 1" & vbNewLine
        SQL &= ") LAST_RPT" & vbNewLine
        SQL &= "ON TAG.TAG_ID = LAST_RPT.TAG_ID" & vbNewLine
        SQL &= "WHERE TAG.Active_Status = 1 AND TAG.PLANT_ID = @PLANT_ID" & vbNewLine
        SQL &= "ORDER BY TAG_Code" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        rptMTap.DataSource = DT
        rptMTap.DataBind()

        Session("Dashboard_Current_Status_AllTag_MTAP") = DT
    End Sub

    Protected Sub rptMTAP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMTap.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTag As Label = e.Item.FindControl("lblMTapTag")
        Dim lblPwq As HtmlAnchor = e.Item.FindControl("lblMTapPwq")
        Dim lblIns As HtmlAnchor = e.Item.FindControl("lblMTapIns")
        Dim lblPwc As HtmlAnchor = e.Item.FindControl("lblMTapPwc")
        Dim lblSta As HtmlAnchor = e.Item.FindControl("lblMTapSta")
        Dim lblRot As HtmlAnchor = e.Item.FindControl("lblMTapRot")
        Dim lblAir As HtmlAnchor = e.Item.FindControl("lblMTapAir")
        Dim lblLastReport As HtmlAnchor = e.Item.FindControl("lblMTapLastReport")

        lblTag.Text = e.Item.DataItem("TAG_Code").ToString

        If e.Item.DataItem("Pwq").ToString <> "" Then
            If e.Item.DataItem("Pwq") = 0 Then
                lblPwq.InnerHtml = "Normal"
                lblPwq.Style("color") = "Green"
                lblPwq.Style("cursor") = "pointer"
            Else
                lblPwq.InnerHtml = "Abnormal"
                lblPwq.Style("color") = "Red"
                lblPwq.Style("cursor") = "pointer"
            End If
            lblPwq.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Pwq_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Pwq_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Ins").ToString <> "" Then
            If e.Item.DataItem("Ins") = 0 Then
                lblIns.InnerHtml = "Normal"
                lblIns.Style("color") = "Green"
                lblIns.Style("cursor") = "pointer"
            Else
                lblIns.InnerHtml = "Abnormal"
                lblIns.Style("color") = "Red"
                lblIns.Style("cursor") = "pointer"
            End If
            lblIns.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Ins_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Ins_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Pwc").ToString <> "" Then
            If e.Item.DataItem("Pwc") = 0 Then
                lblPwc.InnerHtml = "Normal"
                lblPwc.Style("color") = "Green"
                lblPwc.Style("cursor") = "pointer"
            Else
                lblPwc.InnerHtml = "Abnormal"
                lblPwc.Style("color") = "Red"
                lblPwc.Style("cursor") = "pointer"
            End If
            lblPwc.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Pwc_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Pwc_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Sta").ToString <> "" Then
            If e.Item.DataItem("Sta") = 0 Then
                lblSta.InnerHtml = "Normal"
                lblSta.Style("color") = "Green"
                lblSta.Style("cursor") = "pointer"
            Else
                lblSta.InnerHtml = "Abnormal"
                lblSta.Style("color") = "Red"
                lblSta.Style("cursor") = "pointer"
            End If
            lblSta.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Sta_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Sta_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Rot").ToString <> "" Then
            If e.Item.DataItem("Rot") = 0 Then
                lblRot.InnerHtml = "Normal"
                lblRot.Style("color") = "Green"
                lblRot.Style("cursor") = "pointer"
            Else
                lblRot.InnerHtml = "Abnormal"
                lblRot.Style("color") = "Red"
                lblRot.Style("cursor") = "pointer"
            End If
            lblRot.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Rot_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Rot_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If
        If e.Item.DataItem("Air").ToString <> "" Then
            If e.Item.DataItem("Air") = 0 Then
                lblAir.InnerHtml = "Normal"
                lblAir.Style("color") = "Green"
                lblAir.Style("cursor") = "pointer"
            Else
                lblAir.InnerHtml = "Abnormal"
                lblAir.Style("color") = "Red"
                lblAir.Style("cursor") = "pointer"
            End If
            lblAir.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Air_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Air_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
        End If

        If e.Item.DataItem("RPT_CODE").ToString <> "" Then
            lblLastReport.InnerHtml = e.Item.DataItem("RPT_CODE")
            lblLastReport.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_PdMA.aspx?RPT_Year=" & e.Item.DataItem("Last_RPT_Year") & "&RPT_No=" & e.Item.DataItem("Last_RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "&RPT_Type_ID=" & EIR_BL.Report_Type.MTap_Report & "','Dialog_Tag_MTAP_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            lblLastReport.Style("cursor") = "pointer"
        End If

    End Sub

    Protected Sub DisplayChart() Handles ddl_ChartType.SelectedIndexChanged

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartPdMA.Series(0).ChartType
        Select Case ddl_ChartType.Items(ddl_ChartType.SelectedIndex).Value
            Case "Pie"
                ChartType = DataVisualization.Charting.SeriesChartType.Pie
            Case "Doughnut"
                ChartType = DataVisualization.Charting.SeriesChartType.Doughnut
            Case "Funnel"
                ChartType = DataVisualization.Charting.SeriesChartType.Funnel
            Case "Pyramid"
                ChartType = DataVisualization.Charting.SeriesChartType.Pyramid
        End Select
        ChartPdMA.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Current_Status_AllTag_PdMA")
        Dim DT_MTap As DataTable = Session("Dashboard_Current_Status_AllTag_MTap")
        DT.Merge(DT_MTap)

        Dim Normal As Double
        Dim Abnormal As Double
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("Pwq").ToString = "1" Or DT.Rows(i).Item("Ins").ToString = "1" Or DT.Rows(i).Item("Pwc").ToString = "1" Or DT.Rows(i).Item("Sta").ToString = "1" Or DT.Rows(i).Item("Rot").ToString = "1" Or DT.Rows(i).Item("Air").ToString = "1" Then
                Abnormal += 1
            Else
                Normal += 1
            End If
        Next

        Dim YValue() As Double = {Normal, Abnormal}
        Dim XValue() As String = {"Normal", "Abnormal"}

        ChartPdMA.Series("Series1").Points.DataBindY(YValue)
        ChartPdMA.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
        ChartPdMA.Series("Series1").Points(1).ToolTip = "Abnormal : " & FormatNumber(Abnormal, 0) & " tag(s)"
        ChartPdMA.Titles("Title1").Text = ChartPdMA.Titles("Title1").Text.Replace("xxx", lblPlant.Text)
        ChartPdMA.Titles("Title2").Text = ChartPdMA.Titles("Title2").Text.Replace("xxx", DT.Rows.Count.ToString)

        If Normal > 0 Then
            ChartPdMA.Series("Series1").Points(0).Label = Normal
            ChartPdMA.Series("Series1").Points(0).LabelForeColor = Drawing.Color.White
        End If
        If Abnormal > 0 Then
            ChartPdMA.Series("Series1").Points(1).Label = Abnormal
            ChartPdMA.Series("Series1").Points(0).LabelForeColor = Drawing.Color.White
        End If

        ChartPdMA.Series("Series1").Points(0).Color = Drawing.Color.Green
        ChartPdMA.Series("Series1").Points(1).Color = Drawing.Color.Red
    End Sub

End Class