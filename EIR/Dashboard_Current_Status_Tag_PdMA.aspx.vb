﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status_Tag_PdMA
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim CV As New Converter

    'Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.PdMA_Report
    Public Picture_Path As String = ConfigurationManager.AppSettings("Picture_Path").ToString

    Private Property RPT_Type_ID() As Integer
        Get
            If IsNumeric(ViewState("RPT_Type_ID")) Then
                Return ViewState("RPT_Type_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Type_ID") = value
        End Set
    End Property

    Private Property Tag_ID() As Integer
        Get
            If IsNumeric(ViewState("Tag_ID")) Then
                Return ViewState("Tag_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Tag_ID") = value
        End Set
    End Property

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Tag_ID = Request.QueryString("Tag_ID")
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            DETAIL_ID = Request.QueryString("DETAIL_ID")
            RPT_Type_ID = Request.QueryString("RPT_Type_ID")

            Dim SQL As String = ""
            If RPT_Type_ID = EIR_BL.Report_Type.PdMA_Report Then
                SQL = "SELECT * FROM VW_REPORT_PDMA_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            Else
                SQL = "SELECT * FROM VW_REPORT_MTap_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            End If

            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            lblRptCode.Text = DT.Rows(0).Item("Rpt_Code").ToString
            lbl_Plant.Text = DT.Rows(0).Item("PLANT_Code")
            lbl_Route.Text = DT.Rows(0).Item("ROUTE_Code")
            lbl_Year.Text = RPT_Year

            If RPT_Type_ID = EIR_BL.Report_Type.PdMA_Report Then
                SQL = "SELECT * FROM RPT_PDMA_Detail" & vbNewLine
                SQL &= " LEFT JOIN MS_PDMA_TAG ON RPT_PDMA_Detail.TAG_ID = MS_PDMA_TAG.TAG_ID" & vbNewLine
                SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_PDMA_Detail.TAG_ID=" & Tag_ID
            Else
                SQL = "SELECT * FROM RPT_MTAP_Detail" & vbNewLine
                SQL &= " LEFT JOIN MS_MTAP_TAG ON RPT_MTAP_Detail.TAG_ID = MS_MTAP_TAG.TAG_ID" & vbNewLine
                SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_MTAP_Detail.TAG_ID=" & Tag_ID
            End If

            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)

            lblTagNo.Text = DT.Rows(0).Item("TAG_Code").ToString
            lblTagBrand.Text = DT.Rows(0).Item("TAG_Brand").ToString
            lblTagVoltage.Text = DT.Rows(0).Item("TAG_Voltage").ToString
            lblTagFrequency.Text = DT.Rows(0).Item("TAG_Frequency").ToString
            lblTagType.Text = DT.Rows(0).Item("TAG_Type").ToString
            lblTagInsulationClass.Text = DT.Rows(0).Item("TAG_Insulation_Class").ToString
            If IsNumeric(DT.Rows(0).Item("TAG_Current")) Then
                lblTagCurrent.Text = FormatNumber(DT.Rows(0).Item("TAG_Current"))
            End If
            If IsNumeric(DT.Rows(0).Item("TAG_Power_Factor")) Then
                lblTagPowerFactor.Text = FormatNumber(DT.Rows(0).Item("TAG_Power_Factor"))
            End If
            If IsNumeric(DT.Rows(0).Item("TAG_KW")) Then
                lblTagKW.Text = FormatNumber(DT.Rows(0).Item("TAG_KW"))
            End If
            lblTagRPM.Text = DT.Rows(0).Item("TAG_RPM").ToString
            If Not IsDBNull(DT.Rows(0).Item("Inspec_Date")) Then
                lblInspDate.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("Inspec_Date"))
            Else
                lblInspDate.Text = ""
            End If
            If DT.Rows(0).Item("TAG_Mode") = 1 Then
                lblMode.Text = "Online Testing Mcc To Motor"
                pnlOnline.Visible = True
                pnlOffline.Visible = False

                If DT.Rows(0).Item("N_Fline").ToString <> "" Then
                    lblFlineValue.Text = FormatNumber(DT.Rows(0).Item("N_Fline"))
                    lblFline.Text = BL.Get_PDMA_AnalyzingCS_Text(CDbl(lblFlineValue.Text))
                    tdFline.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(CDbl(lblFlineValue.Text))
                    If tdFline.Attributes("class") = "PDMA_CS_7" Then
                        lblFline.ForeColor = Drawing.Color.White
                    Else
                        lblFline.ForeColor = Drawing.Color.Black
                    End If
                Else
                    lblFlineValue.Text = ""
                    lblFline.Text = ""
                    tdFline.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(-1)
                End If

                If DT.Rows(0).Item("N_Swirl_Effect").ToString <> "" Then
                    If DT.Rows(0).Item("N_Swirl_Effect") = 1 Then
                        lblSwirlEffectValue.Text = "Occured"
                        lblSwirlEffect.Text = BL.Get_PDMA_AnalyzingCS_Text(1)
                        tdSwirlEffect.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(1)
                        lblSwirlEffect.ForeColor = Drawing.Color.White
                    Else
                        lblSwirlEffectValue.Text = "Not Occured"
                        lblSwirlEffect.Text = BL.Get_PDMA_AnalyzingCS_Text(100)
                        tdSwirlEffect.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(100)
                        lblSwirlEffect.ForeColor = Drawing.Color.Black
                    End If
                Else
                    lblSwirlEffectValue.Text = ""
                    lblSwirlEffect.Text = BL.Get_PDMA_AnalyzingCS_Text(-1)
                    tdSwirlEffect.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(-1)
                    lblSwirlEffect.ForeColor = Drawing.Color.Black
                End If

                If DT.Rows(0).Item("N_Peak1").ToString <> "" Then
                    lblPeak1Value.Text = FormatNumber(DT.Rows(0).Item("N_Peak1"))
                    tdPeak1.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(lblPeak1Value.Text))
                Else
                    lblPeak1Value.Text = ""
                    tdPeak1.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
                End If

                If DT.Rows(0).Item("N_Peak2").ToString <> "" Then
                    lblPeak2Value.Text = FormatNumber(DT.Rows(0).Item("N_Peak2"))
                    tdPeak2.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(lblPeak2Value.Text))
                Else
                    lblPeak2Value.Text = ""
                    tdPeak2.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
                End If

                If DT.Rows(0).Item("N_Peak3").ToString <> "" Then
                    lblPeak3Value.Text = FormatNumber(DT.Rows(0).Item("N_Peak3"))
                    tdPeak3.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(lblPeak3Value.Text))
                Else
                    lblPeak3Value.Text = ""
                    tdPeak3.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
                End If

                If DT.Rows(0).Item("N_Peak4").ToString <> "" Then
                    lblPeak4Value.Text = FormatNumber(DT.Rows(0).Item("N_Peak4"))
                    tdPeak4.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(lblPeak4Value.Text))
                Else
                    lblPeak4Value.Text = ""
                    tdPeak4.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
                End If

                Dim Count As Integer = 0
                If tdPeak1.Attributes("class") = "PDMA_Eccentricity_Severe" Then
                    Count = Count + 1
                End If
                If tdPeak2.Attributes("class") = "PDMA_Eccentricity_Severe" Then
                    Count = Count + 1
                End If
                If tdPeak3.Attributes("class") = "PDMA_Eccentricity_Severe" Then
                    Count = Count + 1
                End If
                If tdPeak4.Attributes("class") = "PDMA_Eccentricity_Severe" Then
                    Count = Count + 1
                End If
                lblPeakCount.Text = Count

                If DT.Rows(0).Item("N_Peak_Above").ToString <> "" Then
                    Select Case DT.Rows(0).Item("N_Peak_Above")
                        Case 0
                            lblPeaksAbove.Text = ""
                            tdAbove.Attributes("class") = "PDMA_Eccentricity"
                        Case 1
                            lblPeaksAbove.Text = "Good"
                            tdAbove.Attributes("class") = "PDMA_Eccentricity_Good"
                        Case 2
                            lblPeaksAbove.Text = "Modelate"
                            tdAbove.Attributes("class") = "PDMA_Eccentricity_Moderate"
                        Case 3
                            lblPeaksAbove.Text = "Severe"
                            tdAbove.Attributes("class") = "PDMA_Eccentricity_Severe"
                    End Select
                Else
                    lblPeaksAbove.Text = ""
                    tdAbove.Attributes("class") = "PDMA_Eccentricity"
                End If

                If DT.Rows(0).Item("N_Status_PowerQuality").ToString <> "" Then
                    If DT.Rows(0).Item("N_Status_PowerQuality") = True Then
                        'OK
                        txtPowerQualityStatus.Text = "OK"
                        txtPowerQualityStatus.BackColor = Drawing.Color.White
                    Else
                        'AB
                        txtPowerQualityStatus.Text = "AB"
                        txtPowerQualityStatus.BackColor = Drawing.Color.Red
                        txtPowerQualityStatus.ForeColor = Drawing.Color.White
                    End If
                Else
                    txtPowerQualityStatus.Text = ""
                End If

                '----------------- Store Picture ------------------
                imgCurrent1.ImageUrl = "RenderImageFromPath.aspx?PathFile=" & DT.Rows(0).Item("N_Img1").ToString
                imgCurrent2.ImageUrl = "RenderImageFromPath.aspx?PathFile=" & DT.Rows(0).Item("N_Img2").ToString
                imgEccentricity.ImageUrl = "RenderImageFromPath.aspx?PathFile=" & DT.Rows(0).Item("N_Img3").ToString
                imgPowerQuality.ImageUrl = "RenderImageFromPath.aspx?PathFile=" & DT.Rows(0).Item("N_Img4").ToString

            Else
                lblMode.Text = "Offline Testing Mcc To Motor"
                pnlOffline.Visible = True
                pnlOnline.Visible = False

                If DT.Rows(0).Item("F_Temperature").ToString <> "" Then
                    lblTemperatureValue.Text = FormatNumber(DT.Rows(0).Item("F_Temperature"))
                Else
                    lblTemperatureValue.Text = ""
                End If

                If DT.Rows(0).Item("F_Ohm12").ToString <> "" Then
                    lblOhm12Value.Text = FormatNumber(DT.Rows(0).Item("F_Ohm12"), 5)
                Else
                    lblOhm12Value.Text = ""
                End If

                If DT.Rows(0).Item("F_Ohm13").ToString <> "" Then
                    lblOhm13Value.Text = FormatNumber(DT.Rows(0).Item("F_Ohm13"), 5)
                Else
                    lblOhm13Value.Text = ""
                End If

                If DT.Rows(0).Item("F_Ohm23").ToString <> "" Then
                    lblOhm23Value.Text = FormatNumber(DT.Rows(0).Item("F_Ohm23"), 5)
                Else
                    lblOhm23Value.Text = ""
                End If

                If DT.Rows(0).Item("F_Resistive_Imbalance").ToString <> "" Then
                    lblResistiveImbalancevalue.Text = FormatNumber(DT.Rows(0).Item("F_Resistive_Imbalance"))
                Else
                    lblResistiveImbalancevalue.Text = ""
                End If
                If DT.Rows(0).Item("F_Status_Resistive_Imbalance").ToString <> "" Then
                    If DT.Rows(0).Item("F_Status_Resistive_Imbalance") = True Then
                        'OK
                        lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("Green")
                        tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                    Else
                        'AB
                        If CInt(lblTagVoltage.Text) <= 600 Then
                            Select Case CDbl(lblResistiveImbalancevalue.Text)
                                Case Is >= 4
                                    lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("Red")
                                    tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                                Case Is >= 3
                                    lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("ORANGE")
                                    tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                            End Select
                        Else
                            Select Case CDbl(lblResistiveImbalancevalue.Text)
                                Case Is >= 3
                                    lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("Red")
                                    tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                                Case Is >= 2
                                    lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("ORANGE")
                                    tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                            End Select
                        End If

                    End If
                Else
                    lblResistiveImbalance.Text = ""
                    tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
                End If


                If DT.Rows(0).Item("F_mH12").ToString <> "" Then
                    lblmH12Value.Text = FormatNumber(DT.Rows(0).Item("F_mH12"), 3)
                Else
                    lblmH12Value.Text = ""
                End If

                If DT.Rows(0).Item("F_mH13").ToString <> "" Then
                    lblmH13Value.Text = FormatNumber(DT.Rows(0).Item("F_mH13"), 3)
                Else
                    lblmH13Value.Text = ""
                End If

                If DT.Rows(0).Item("F_mH23").ToString <> "" Then
                    lblmH23Value.Text = FormatNumber(DT.Rows(0).Item("F_mH23"), 3)
                Else
                    lblmH23Value.Text = ""
                End If
                Dim Value As Double = 0
                If lblmH12Value.Text <> "" Then
                    Value = Value + lblmH12Value.Text
                End If
                If lblmH13Value.Text <> "" Then
                    Value = Value + lblmH13Value.Text
                End If
                If lblmH23Value.Text <> "" Then
                    Value = Value + lblmH23Value.Text
                End If

                If Value > 0 Then
                    lblAverageInductance.Text = FormatNumber(Value / 3, 3)
                Else
                    lblAverageInductance.Text = ""
                End If

                If DT.Rows(0).Item("F_Inductive_Imbalance").ToString <> "" Then
                    lblInductiveImbalanceValue.Text = FormatNumber(DT.Rows(0).Item("F_Inductive_Imbalance"))
                Else
                    lblInductiveImbalanceValue.Text = ""
                End If
                If DT.Rows(0).Item("F_Status_Inductive_Imbalance").ToString <> "" Then
                    Select Case DT.Rows(0).Item("F_Status_Inductive_Imbalance")
                        Case 1 'OK
                            lblInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                            lblInductiveImbalance.Text = "Good"
                            tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                        Case 2 'Coution
                            lblInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                            lblInductiveImbalance.Text = "Coution"
                            tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                        Case 3 'AB
                            lblInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                            lblInductiveImbalance.Text = "Severe"
                            tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                    End Select
                Else
                    lblInductiveImbalance.Text = ""
                    tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
                End If

                If DT.Rows(0).Item("F_Capacitance").ToString <> "" Then
                    lblCapacitancevalue.Text = FormatNumber(DT.Rows(0).Item("F_Capacitance"), 0)
                Else
                    lblCapacitancevalue.Text = ""
                End If

                If DT.Rows(0).Item("F_Status_Capacitance").ToString <> "" Then
                    If DT.Rows(0).Item("F_Status_Capacitance") = True Then
                        'OK
                        lblCapacitance.Text = "Good"
                        tdCapacitance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                    Else
                        'AB
                        lblCapacitance.Text = "Severe"
                        tdCapacitance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                    End If
                Else
                    lblCapacitancevalue.Text = ""
                    tdCapacitance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
                End If

                If DT.Rows(0).Item("F_Status_Resistance").ToString <> "" Then
                    If DT.Rows(0).Item("F_Status_Resistance") = True Then
                        'OK
                        lblResistance.Text = BL.Get_PDMA_Color_Text("Green")
                        tdResistance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                    Else
                        'AB
                        lblResistance.Text = BL.Get_PDMA_Color_Text("Red")
                        tdResistance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                    End If
                Else
                    lblResistance.Text = ""
                    tdResistance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
                End If

                If DT.Rows(0).Item("F_Resistance").ToString <> "" Then
                    lblResistanceValue.Text = FormatNumber(DT.Rows(0).Item("F_Resistance"))
                Else
                    lblResistanceValue.Text = ""
                End If

                If DT.Rows(0).Item("F_PI_Value").ToString <> "" Then
                    lblPI_ValueValue.Text = FormatNumber(DT.Rows(0).Item("F_PI_Value"))
                    lblPI_Value.Text = BL.Get_PDMA_AnalyzingPI_Text(CDbl(lblPI_ValueValue.Text))
                    tdPI_Value.Attributes("class") = BL.Get_PDMA_AnalyzingPI_CSS(CDbl(lblPI_ValueValue.Text))
                Else
                    lblPI_ValueValue.Text = ""
                    tdPI_Value.Attributes("class") = BL.Get_PDMA_AnalyzingPI_CSS(-1)
                End If

                imgPolarization.ImageUrl = "RenderImageFromPath.aspx?PathFile=" & DT.Rows(0).Item("F_img5").ToString
            End If
        End If

    End Sub

End Class