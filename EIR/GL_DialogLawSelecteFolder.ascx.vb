﻿Public Class GL_DialogLawSelecteFolder
    Inherits System.Web.UI.UserControl

    Public Event SelectedFolder(ToFolderID As Integer, ToFolderName As String, ParentID As Integer)

    Dim _FromFolderID As Integer = 0
    Public ReadOnly Property FromFolderID As Integer
        Get
            Return _FromFolderID
        End Get
    End Property

    Public Sub ShowDialog(FolderID As Integer)
        treeLawDocument.RootNode.Clear()

        _FromFolderID = FolderID

        Dim cl As New LawClass
        Dim dt As DataTable = cl.GetChildNode(0)
        dt.DefaultView.RowFilter = "node_type='" & TreeviewNodeType.FolderMenu.ToString & "'"
        If dt.DefaultView.Count > 0 Then
            Dim rootNode As Goldtect.ASTreeViewNode = treeLawDocument.RootNode
            rootNode.NodeIcon = "resources/images/icons/icon-Folder_open.png"
            GenerateTreeview(dt.DefaultView.ToTable.Copy, rootNode)
        End If

        dt.Dispose()
        pnlValidation.Visible = False

        dialogLawFolder.Show()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then

        End If
    End Sub

    Private Sub btnbtnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click
        Dim selectedNode As Goldtect.ASTreeViewNode = treeLawDocument.GetSelectedNode()
        If selectedNode IsNot Nothing Then
            Dim cl As New LawClass
            Dim dt As DataTable = cl.GetFolderDetail(selectedNode.NodeValue)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                RaiseEvent SelectedFolder(dr("folder_id"), dr("folder_name"), dr("parent_id"))
            Else
                lblValidation.Text = "Folder not found"
                pnlValidation.Visible = True
                dialogLawFolder.Show()
            End If
        Else
            lblValidation.Text = "Please select folder"
            pnlValidation.Visible = True
            dialogLawFolder.Show()
        End If
    End Sub

    Private Sub btnValidationClose_Click(sender As Object, e As ImageClickEventArgs) Handles btnValidationClose.Click
        lblValidation.Text = ""
        pnlValidation.Visible = False
        dialogLawFolder.Show()
    End Sub

    Private Sub GenerateTreeview(dt As DataTable, treeNode As Goldtect.ASTreeViewNode)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim ChNode As New Goldtect.ASTreeViewLinkNode(dr("folder_name"), dr("folder_id"), "#", "resources/images/icons/icon-Folder_close.png")

                Dim cl As New LawClass
                Dim cDt As DataTable = cl.GetChildNode(dr("folder_id"))
                cDt.DefaultView.RowFilter = "node_type='" & TreeviewNodeType.FolderMenu.ToString & "'"
                If cDt.DefaultView.Count > 0 Then
                    ChNode.NodeIcon = "resources/images/icons/icon-Folder_open.png"

                End If
                treeNode.AppendChild(ChNode)

                GenerateTreeview(cDt.DefaultView.ToTable, ChNode)

                cDt.Dispose()
            Next
        End If
    End Sub

End Class