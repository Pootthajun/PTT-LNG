﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_DialogHeat_Exchnager_Spec.ascx.vb" Inherits="EIR.MS_ST_DialogHeat_Exchnager_Spec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>


<%--<div class="MaskDialog"></div>--%>
<asp:Panel ID="pnl" runat="server">
    <asp:Label ID="lblTag" runat="server" Text="" Visible ="false" ></asp:Label>
</asp:Panel>

    
    <table cellpadding="0" cellspacing="0" class="propertyTable">
        <tr>
            <td class="propertyGroup" style="border: none;" colspan="4">Description</td>
            <td class="propertyGroup" style="border: none;">Cad / Drawing&nbsp;
        <asp:Button ID="btnUploadFile" runat="server" Text="" Style="display: none;" />
            </td>
            <td class="propertyGroup" style="border: none; text-align: right;">
                <input type="button" id="btnAddFile" tag_id="0" active_status="True" tag_code="" tag_name=""
                    runat="server" class="button" value="+ Add" style="cursor: pointer;" />
            </td>
        </tr>
        <tr>
            <td style="width: 15%;" class="propertyCaption">Plant <font color="red">**</font></td>
            <td style="width: 15%;">
                <asp:DropDownList ID="ddl_Edit_Plant" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>
            </td>
            <td style="width: 15%;" class="propertyCaption">Route <font color="red">**</font></td>
            <td style="width: 15%;">
                <asp:DropDownList ID="ddl_Edit_Route" runat="server" CssClass="select">
                </asp:DropDownList>
            </td>
            <td style="width: 40%;" rowspan="19" colspan="2">
                <asp:Panel ID="DrawingAlbum" runat="Server" CssClass="FileAlbum">
                    <asp:Repeater ID="rptDrawing" runat="server">
                        <ItemTemplate>
                            <div class="item" id="item" runat="server">
                                <div class="thumbnail">
                                    <a id="lnk_File_Dialog" runat="server" target="_blank" title="Drawing">
                                        <asp:Image ID="img_File" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
                                    </a>
                                </div>
                                <div id="pnlEditFile" runat="server" class="toolbar">
                                    <asp:Button ID="btnUpload" runat="server" Text="" Style="display: none;" CommandName="Edit" />
                                    <input type="image" id="btnEdit" runat="server" src="resources/images/icons/edit_white_16.png" style="margin-right: 10px;" />
                                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
                                    <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </td>
        </tr>



        <tr>
            <td class="propertyCaption">Tag No <font color="red">**</font></td>
            <td>
                <asp:DropDownList ID="ddl_Edit_Area" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddl_Edit_Process" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtTagNo" runat="server" MaxLength="10" Font-Size="10" Style="margin-left: 7px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Tag Name <font color="red">**</font></td>
            <td colspan="3">
                <asp:TextBox ID="txtTagName" runat="server" MaxLength="50" Font-Size="10" Style="text-align: left; margin-left: 7px;"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Equipement <font color="red">**</font></td>
            <td style="border-right-style: none;">
                <asp:DropDownList ID="ddl_Edit_Type" runat="server" CssClass="select">
                </asp:DropDownList>
            </td>

            <td colspan="2" style="border-left-style: none;"></td>

        </tr>
        <tr>
            <td class="propertyGroup" style="border: none;" colspan="4">Specification</td>
        </tr>


        <tr>
            <td class="propertyCaption">Initial Year </td>
            <td  style="border-right-style: none;">
                <asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" Font-Size="10" Style="text-align: left; margin-left: 7px;"></asp:TextBox>

            </td>
            <td colspan="2" style="border-left-style: none;"></td>
        </tr>
    <%-- <tr>					            
					        
		<td class="propertyCaption">Initial Year</td>					        
		<td colspan="2" style="border-right-style: none;"><asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td colspan="3" style="border-left-style: none;"></td>
	</tr>--%>

        <tr>
            <td class="propertyGroup" colspan="4" style="border: none; padding :0px;">

                        <table cellpadding="0" cellspacing="0" >
            <tbody>
                <%--Head Table--%>
                <tr>
                    <td class="propertyCaption" style="text-align:center;width: 140px;"><b>SPEC</b></td>
                    <td class="propertyCaption" style="text-align:center;width: 100px;"><b>SHELL SIDE</b></td>
                    <td class="propertyCaption" colspan="2" style="text-align:center;"><b style="text-align:center;">TUBE SIDE</b></td>
                    <td class="propertyCaption" style="text-align:center;width: 180px;"><b>REMARK</b></td>
                </tr>
                <%--Table Content--%>
              
        <%--Table Content--%>
        <tr>              
	        <td class="propertyCaption">Design Temp</td>	
	        <td><asp:TextBox ID="txt_SHELL_DESIGN_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_DESIGN_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">° F</td>
	        <td><asp:TextBox ID="txt_REMARK_DESIGN_TEMP" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Design Pressure</td>	
	        <td><asp:TextBox ID="txt_SHELL_DESIGN_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_DESIGN_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_DESIGN_PRESSURE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Operate Temp</td>	
	        <td><asp:TextBox ID="txt_SHELL_OPERATING_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_OPERATING_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">° C</td>
	        <td><asp:TextBox ID="txt_REMARK_OPERATING_TEMP" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Operate Pressure</td>	
	        <td><asp:TextBox ID="txt_SHELL_OPERATING_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_OPERATING_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_OPERATING_PRESSURE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Hydro. Test Pressure</td>	
	        <td><asp:TextBox ID="txt_SHELL_HYDRO_TEST_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_HYDRO_TEST_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_HYDRO_TEST_PRESSURE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">M.A.W.P</td>	
	        <td><asp:TextBox ID="txt_SHELL_MAWP" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_MAWP" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_MAWP" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Material</td>	
	        <td><asp:TextBox ID="txt_SHELL_MATERIAL" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_MATERIAL" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_MATERIAL" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Nom.Thk</td>	
	        <td><asp:TextBox ID="txt_SHELL_NOM_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_NOM_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">mm</td>
	        <td><asp:TextBox ID="txt_REMARK_NOM_THK" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Corrosion Allow</td>	
	        <td><asp:TextBox ID="txt_SHELL_CORROSION_ALLOW" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_CORROSION_ALLOW" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">mm</td>
	        <td><asp:TextBox ID="txt_REMARK_CORROSION_ALLOW" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Min. All. Thk.</td>	
	        <td><asp:TextBox ID="txt_SHELL_MIN_ALL_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_MIN_ALL_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">mm</td>
	        <td><asp:TextBox ID="txt_REMARK_MIN_ALL_THK" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Size</td>	
	        <td><asp:TextBox ID="txt_SHELL_SIZE" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_SIZE" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_SIZE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Quantity</td>	
	        <td><asp:TextBox ID="txt_SHELL_QUANTITY" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_QUANTITY" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_QUANTITY" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>
        <tr>              
	        <td class="propertyCaption">Fluid</td>	
	        <td><asp:TextBox ID="txt_SHELL_FLUID" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_FLUID" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_FLUID" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>
        <tr>              
	        <td class="propertyCaption">NO. Passes</td>	
	        <td><asp:TextBox ID="txt_SHELL_NO_PASSES" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_NO_PASSES" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_NO_PASSES" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>


            </tbody>
        </table>


            </td>
        </tr>
        <tr>
            <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" Visible="false" />
            <caption>
                &nbsp;
                <asp:Button ID="btnUpdate" runat="server" Class="button" Text="Update to Tag" Visible="false" />
            </caption>
        </tr>
    </table>

<asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
<asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>


          










