﻿Imports System.Data
Imports System.Data.SqlClient

Public Class SPH_Summary
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ClearPanelSearch()
            SetUserAuthorization()
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub
#End Region

    Private Sub SetUserAuthorization()
        btnCreate.Visible = USER_LEVEL = EIR_BL.User_Level.Collector Or
                            USER_LEVEL = EIR_BL.User_Level.Inspector Or
                            USER_LEVEL = EIR_BL.User_Level.Approver Or
                            USER_LEVEL = EIR_BL.User_Level.Administrator
    End Sub

    Private Sub BindPlan()

        Dim SQL As String = "SELECT HD.RPT_ID,HD.RPT_Code,HD.RPT_Year," & vbLf
        SQL &= " HD.RPT_No,HD.RPT_Type_Name,HD.PLANT_Code,ROUTE_Code," & vbLf
        SQL &= " HD.RPT_STEP, HD.STEP_NAME, RPT_LOCK_BY," & vbLf
        SQL &= " COUNT(Detail.SPH_ID) TotalSpring," & vbLf
        SQL &= " SUM(CASE dbo.UDF_SPH_CalculateTravelStatus( Spring.Length_Cold,Spring.Length_Hot,INSP_Level )" & vbLf
        SQL &= " WHEN 'Adjusted' THEN 1 ELSE 0 END) TotalAdjusted" & vbLf
        SQL &= " ,HD.Created_Time" & vbLf
        '--------- Add User Locking Detial --------------
        SQL &= " ,User_Prefix+User_Name+' '+User_Surname Lock_By_Name" & vbLf

        SQL &= " FROM VW_SPH_REPORT_HEADER HD" & vbLf
        SQL &= " LEFT JOIN SPH_RPT_Detail Detail ON HD.RPT_ID=Detail.RPT_ID" & vbLf
        SQL &= " LEFT JOIN SPH_MS_Spring Spring ON Detail.SPH_ID=Spring.SPH_ID " & vbLf

        '--------- Add User Locking Detial --------------
        SQL &= " LEFT JOIN MS_User ON HD.RPT_LOCK_BY=MS_User.USER_ID " & vbNewLine

        Dim WHERE As String = ""
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " HD.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " HD.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " HD.RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If WHERE <> "" Then SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 6) & vbNewLine

        SQL &= " Group BY" & vbLf
        SQL &= " HD.RPT_ID,HD.RPT_Code,HD.RPT_Year,HD.RPT_No,HD.RPT_Type_Name,HD.PLANT_Code,ROUTE_Code," & vbLf
        SQL &= " HD.RPT_STEP, HD.STEP_NAME, RPT_LOCK_BY,HD.Created_Time" & vbLf
        '--------- Add User Locking Detial --------------
        SQL &= " ,User_Prefix,User_Name,User_Surname" & vbLf
        SQL &= " ORDER BY HD.RPT_Code DESC" & vbLf


        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = ex.Message
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("MS_SPH_Summary") = DT

        Navigation.SesssionSourceName = "MS_SPH_Summary"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        '------- Check First --------
        Dim SQL As String = "SELECT * FROM VW_SPH_REPORT_HEADER WHERE RPT_ID=" & e.CommandArgument
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            lblBindingError.Text = "This report has been removed."
            pnlBindingError.Visible = True
            BindPlan()
            Exit Sub
        End If

        '---------------------- Set Available -----------------------
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim RPT_STEP As EIR_BL.Report_Step = lblStatus.Attributes("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY
            If Not .CanEdit Then
                lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
                lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
                lblBindingError.Text &= "<li>This report has been locked by others"
                pnlBindingError.Visible = True
                BindPlan()
                Exit Sub
            End If
        End With

        Select Case e.CommandName
            Case "Edit"
                '-------------Update Activated Report-----------
                SQL = "SELECT * FROM SPH_RPT_Header WHERE RPT_ID=" & e.CommandArgument
                DA = New SqlDataAdapter(SQL, BL.ConnStr)
                DT = New DataTable
                DA.Fill(DT)

                If Session("USER_ID") <> 0 Then ' Lock if current user <> administrator
                    DT.Rows(0).Item("RPT_LOCK_BY") = Session("USER_ID")
                    Dim CMD As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If

                Dim Param As String = "RPT_ID=" & e.CommandArgument
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit1.aspx?" & Param & "';", True)
            Case "Delete"
                Try
                    BL.Drop_SPH_RPT_Header(e.CommandArgument)
                Catch ex As Exception
                    lblBindingError.Text = ex.Message
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindPlan()

                lblBindingError.Text = "Delete report successfully"
                pnlBindingError.Visible = True
        End Select

    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblSpring As Label = e.Item.FindControl("lblSpring")
        Dim lblAdjuested As Label = e.Item.FindControl("lblAdjuested")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblCreated As Label = e.Item.FindControl("lblCreated")

        Dim imgLock As Image = e.Item.FindControl("imgLock")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete") '----- Add Delete Feature 

        lblRptNo.Text = e.Item.DataItem("RPT_Code")

        lblRptNo.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        lblRptNo.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        If Not IsDBNull(e.Item.DataItem("PLANT_Code")) Then
            lblPlant.Text = e.Item.DataItem("PLANT_Code")
        End If
        If Not IsDBNull(e.Item.DataItem("ROUTE_Code")) Then
            lblRoute.Text = e.Item.DataItem("ROUTE_Code")
        End If

        'Dim DT As DataTable = CType(rptPlan.DataSource, DataTable).Copy
        lblSpring.Text = e.Item.DataItem("TotalSpring")
        lblAdjuested.Text = e.Item.DataItem("TotalAdjusted")

        lblStatus.Text = e.Item.DataItem("STEP_NAME")
        lblStatus.Attributes("RPT_STEP") = e.Item.DataItem("RPT_STEP")
        If Not IsDBNull(e.Item.DataItem("RPT_LOCK_BY")) Then
            lblStatus.Attributes("RPT_LOCK_BY") = e.Item.DataItem("RPT_LOCK_BY")
        Else
            lblStatus.Attributes("RPT_LOCK_BY") = -1
        End If

        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        lblCreated.Text = BL.ReportGridTime(e.Item.DataItem("Created_Time"))

        '------------------------- Set for Action Premission -------------------------
        btnEdit.CommandArgument = e.Item.DataItem("RPT_ID")
        btnCreate.CommandArgument = e.Item.DataItem("RPT_ID")
        btnDelete.CommandArgument = e.Item.DataItem("RPT_ID")

        btnReport.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"

        Dim RPT_STEP As EIR_BL.Report_Step = e.Item.DataItem("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY

            If .CanEdit And RPT_LOCK_BY = -1 Then
                btnEdit.Visible = True
                btnEdit.ImageUrl = "resources/images/icons/pencil.png"
                btnDelete.Visible = True
                '------------ Set Lock Status ------------
                imgLock.Visible = False

            ElseIf .CanEdit And RPT_LOCK_BY = Session("USER_ID") Then
                btnEdit.Visible = True
                btnDelete.Visible = True
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And Session("USER_ID") = 0 Then
                btnEdit.ImageUrl = "resources/images/icons/pencil.png"
                btnEdit.Visible = True
                btnDelete.Visible = True
                '------------ Set Lock Status ------------
                imgLock.Visible = RPT_LOCK_BY <> -1
                '---------------- Add Locked By Detail- ------------------
                If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                End If
            ElseIf RPT_LOCK_BY > 0 And RPT_LOCK_BY <> Session("USER_ID") Then
                btnEdit.Visible = False
                btnCreate.Visible = False
                imgLock.Visible = True
                If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                End If
            Else
                btnEdit.Visible = False
                btnDelete.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = True
                '---------------- Add Locked Detail-------------------
                imgLock.ToolTip = "Lock by workflow"
            End If

            btnReport.Visible = .CanPreview
        End With

    End Sub

    Private Sub ClearPanelSearch()

        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDlSpringRoute(ddl_Search_Route)

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM SPH_RPT_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.Text = Now.Year + 543

        txt_Search_Start.Text = ""
        txt_Search_End.Text = ""

        If USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Viewer Then
            If Not IsNothing(Request.QueryString("Editable")) AndAlso Request.QueryString("Editable") = "True" Then
                chk_Search_Edit.Checked = True
            End If
        Else
            chk_Search_Edit.Visible = False
            lblEditable.Visible = False
        End If

        BindPlan()
    End Sub

    Protected Sub Search_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged, ddl_Search_Route.SelectedIndexChanged, txt_Search_Start.TextChanged, txt_Search_End.TextChanged, chk_Search_Edit.CheckedChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDlSpringRoute(ddl_Search_Route, ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value)
        BindPlan()
    End Sub


End Class