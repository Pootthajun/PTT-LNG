﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogUploadDocument.ascx.vb" Inherits="EIR.GL_DialogUploadDocument" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<div class="MaskDialog"></div>

<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog" Width= "730px">
       
    <h2>Update Reference Document for <asp:Label ID="lblTAGCode" runat="server"></asp:Label></h2>
	<label class="dialog-col-left" style="width:270px;"><div align="center" style="width: 260px"><asp:Image ImageUrl="~/resources/images/space.png" width="150px" height="150px" ID="ImgPreview" runat="server" ImageAlign="AbsMiddle" />
    </div>
    <br />
    <cc1:AsyncFileUpload ID="ful1" runat="server" 
        CompleteBackColor="DarkGreen" CssClass="button" ErrorBackColor="Red" 
        ForeColor="white" Width="240px" /><br /><br ><asp:Button 
        ID="btnUpload" runat="server" CssClass="button" Text="Upload" 
        Width="257px" /></label>
    <div class="dialog-col-right" style="width:400px;">
        <table align="left" width="400" style="border-bottom: 1px none #fff !important;" cellpadding="0" cellspacing="0">
            <tbody style="border-bottom: 1px none #fff !important;">
            <tr>
                <td>
                    <b>Document Name</b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Name" runat="server" CssClass="text-input" 
                        MaxLength="1000" Width="430px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Recomment</b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Recomment" runat="server" CssClass="text-input" 
                        Height="80px" MaxLength="1000" TextMode="MultiLine" Width="430px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align:right;">
                    <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" />
&nbsp;
                    <asp:Button ID="btnUpdate" runat="server" Class="button" 
                        Text="Update to report" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg" 
                        Visible="False">
                        <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                            ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                        <div>
                            <asp:Label ID="lblValidation" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    				

</asp:Panel>

<cc1:DragPanelExtender ID="pnlDialog_DragPanelExtender" runat="server"  
    DragHandleID="pnlDialog" Enabled="True" TargetControlID="pnlDialog">
</cc1:DragPanelExtender>