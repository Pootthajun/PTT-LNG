﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dashboard.BindDDlEquipment(ddl_Equipment, EIR_BL.Report_Type.All)
            BindData(ddl_Equipment.SelectedValue)
        End If
        lblUserFullname.Text = Session("USER_Full_Name")
    End Sub

    Private Sub BindData(ByVal Equipment As EIR_BL.Report_Type)

        Dim SQL As String = ""
        SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= "	 SELECT DISTINCT MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_ST_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
                SQL &= "	 FROM MS_Plant" & vbLf
                SQL &= "	 LEFT JOIN MS_ST_Route ON MS_Plant.PLANT_ID=MS_ST_Route.PLANT_ID AND MS_ST_Route.Active_Status=1" & vbLf
                SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
                SQL &= "	 INNER JOIN MS_ST_TAG ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID AND MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID AND MS_ST_TAG.Active_Status=1" & vbLf
                SQL &= "     LEFT JOIN (" & vbNewLine
                SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
                SQL &= "        FROM" & vbNewLine
                SQL &= "        (" & vbNewLine
                SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
                SQL &= "            FROM" & vbNewLine
                SQL &= "            (" & vbNewLine
                SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
                SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
                SQL &= "                FROM" & vbNewLine
                SQL &= "     		    (" & vbNewLine
                SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_ST_Detail.TAG_TYPE_ID,RPT_ST_Detail.INSP_ID," & vbNewLine
                SQL &= "     				ICLS_ID CUR_LEVEL" & vbNewLine
                SQL &= "                    FROM RPT_ST_Detail" & vbNewLine
                SQL &= "     		    ) TB1" & vbNewLine
                SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
                SQL &= "     	    ) TB2" & vbNewLine
                SQL &= "        ) TB3" & vbNewLine
                SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "     ) VW ON MS_ST_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
                SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= "	 SELECT DISTINCT MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_RO_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
                SQL &= "	 FROM MS_Plant" & vbLf
                SQL &= "	 LEFT JOIN MS_RO_Route ON MS_Plant.PLANT_ID=MS_RO_Route.PLANT_ID AND MS_RO_Route.Active_Status=1" & vbLf
                SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
                SQL &= "	 INNER JOIN MS_RO_TAG ON MS_RO_TAG.AREA_ID=MS_Area.AREA_ID AND MS_RO_TAG.ROUTE_ID=MS_RO_Route.ROUTE_ID AND MS_RO_TAG.Active_Status=1" & vbLf
                SQL &= "     LEFT JOIN (" & vbNewLine
                SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
                SQL &= "        FROM" & vbNewLine
                SQL &= "        (" & vbNewLine
                SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
                SQL &= "            FROM" & vbNewLine
                SQL &= "            (" & vbNewLine
                SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
                SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
                SQL &= "                FROM" & vbNewLine
                SQL &= "     		    (" & vbNewLine
                SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_RO_Detail.TAG_TYPE_ID,RPT_RO_Detail.INSP_ID," & vbNewLine
                SQL &= "     				CASE WHEN RPT_RO_Detail.INSP_ID = 12 THEN" & vbNewLine
                SQL &= "     					CASE ICLS_ID WHEN 3 THEN 3 WHEN 2 THEN 1 WHEN 1 THEN 0 WHEN 0 THEN 0 ELSE ICLS_ID END" & vbNewLine
                SQL &= "     				ELSE" & vbNewLine
                SQL &= "                        ICLS_ID" & vbNewLine
                SQL &= "     				END CUR_LEVEL" & vbNewLine
                SQL &= "                    FROM RPT_RO_Detail" & vbNewLine
                SQL &= "     		    ) TB1" & vbNewLine
                SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
                SQL &= "     	    ) TB2" & vbNewLine
                SQL &= "        ) TB3" & vbNewLine
                SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "     ) VW ON MS_RO_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
                SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "	SELECT *," & vbNewLine
                SQL &= "	CASE WHEN	TAN_Value>=1.5 OR " & vbNewLine
                SQL &= "				(Oil_Cat=1 AND Ox_Value>=30) OR  ---------- Oxidation Abnormal----------" & vbNewLine
                SQL &= "				(Oil_Cat=2 AND Ox_Value<=30) OR " & vbNewLine
                SQL &= "				VANISH_Value>30 OR ---------- Varnish Abnormal----------" & vbNewLine
                SQL &= "				(Oil_Cat=1 AND WATER_Value>=750) OR" & vbNewLine
                SQL &= "				(Oil_Cat=2 AND WATER_Value>=1500)" & vbNewLine
                SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
                SQL &= "" & vbNewLine
                SQL &= "	 FROM" & vbNewLine
                SQL &= "	(" & vbNewLine
                SQL &= "		SELECT TAG.LO_TAG_ID TAG_ID,TAG.PLANT_ID,PLANT_Code,PLANT_Name,Oil_Cat,TAN_Value,OX_Value,VANISH_Value,WATER_Value" & vbNewLine
                SQL &= "		FROM MS_LO_TAG TAG" & vbNewLine
                SQL &= "		LEFT JOIN MS_LO_Oil_Type ON TAG.Oil_TYPE_ID = MS_LO_Oil_Type.Oil_TYPE_ID" & vbNewLine
                SQL &= "		LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,TAN_Value " & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,TAN_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE TAN_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) TAN_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = TAN_Value.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,OX_Value " & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,OX_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE OX_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) OX_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = OX_Value.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,VANISH_Value " & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,VANISH_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE VANISH_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) VANISH_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = VANISH_Value.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,WATER_Value " & vbNewLine
                SQL &= "		FROM" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,WATER_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE WATER_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) WATER_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = WATER_Value.TAG_ID" & vbNewLine
                SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
                SQL &= "	) ALL_TAG" & vbNewLine
                SQL &= ") TAG_FILTER" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "	SELECT *," & vbNewLine
                SQL &= "	CASE WHEN	PowerQuality = 1 OR Insulation = 1 OR PowerCircuit = 1 OR" & vbNewLine
                SQL &= "			Stator = 1 OR Rotor = 1 OR AirGap = 1" & vbNewLine
                SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
                SQL &= "" & vbNewLine
                SQL &= "	 FROM" & vbNewLine
                SQL &= "	(" & vbNewLine
                SQL &= "		SELECT TAG.TAG_ID,MS_PDMA_Route.PLANT_ID,PLANT_Code,PLANT_Name," & vbNewLine
                SQL &= "		CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END PowerQuality," & vbNewLine
                SQL &= "		CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Insulation," & vbNewLine
                SQL &= "		CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END PowerCircuit," & vbNewLine
                SQL &= "		CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Stator," & vbNewLine
                SQL &= "		CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rotor," & vbNewLine
                SQL &= "		CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END AirGap" & vbNewLine
                SQL &= "		FROM MS_PDMA_TAG TAG " & vbNewLine
                SQL &= "		LEFT JOIN MS_PDMA_Route ON TAG.ROUTE_ID = MS_PDMA_Route.ROUTE_ID" & vbNewLine
                SQL &= "		LEFT JOIN MS_Plant ON MS_PDMA_Route.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) PWQ" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
                SQL &= "		) INS" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,PowerCircuit" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
                SQL &= "		) PWC" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,Stator" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
                SQL &= "		) STA" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) ROT" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) AIR" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
                SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
                SQL &= "	) ALL_TAG" & vbNewLine
                SQL &= ") TAG_FILTER" & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "	SELECT *," & vbNewLine
                SQL &= "	CASE WHEN	PowerQuality = 1 OR Insulation = 1 OR PowerCircuit = 1 OR" & vbNewLine
                SQL &= "			Stator = 1 OR Rotor = 1 OR AirGap = 1" & vbNewLine
                SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
                SQL &= "" & vbNewLine
                SQL &= "	 FROM" & vbNewLine
                SQL &= "	(" & vbNewLine
                SQL &= "		SELECT TAG.TAG_ID,TAG.PLANT_ID,PLANT_Code,PLANT_Name," & vbNewLine
                SQL &= "		CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END PowerQuality," & vbNewLine
                SQL &= "		CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Insulation," & vbNewLine
                SQL &= "		CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END PowerCircuit," & vbNewLine
                SQL &= "		CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Stator," & vbNewLine
                SQL &= "		CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rotor," & vbNewLine
                SQL &= "		CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END AirGap" & vbNewLine
                SQL &= "		FROM MS_MTAP_TAG TAG " & vbNewLine
                SQL &= "		LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) PWQ" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
                SQL &= "		) INS" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,PowerCircuit" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
                SQL &= "		) PWC" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,Stator" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
                SQL &= "		) STA" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) ROT" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) AIR" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
                SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
                SQL &= "	) ALL_TAG" & vbNewLine
                SQL &= ") TAG_FILTER" & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= "SELECT TAG.PLANT_ID,PLANT_Code,PLANT_Name,TAG.TAG_ID," & vbNewLine
                SQL &= "CASE TAG_STATUS WHEN 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM MS_THM_Tag TAG" & vbNewLine
                SQL &= "LEFT JOIN" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "    SELECT RPT_Year,RPT_No,TAG_ID,TAG_STATUS" & vbNewLine
                SQL &= "    FROM" & vbNewLine
                SQL &= "    (" & vbNewLine
                SQL &= " 	SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
                SQL &= "        FROM" & vbNewLine
                SQL &= "        (" & vbNewLine
                SQL &= "            SELECT TAG_ID,RPT_Year,RPT_No,TAG_STATUS," & vbNewLine
                SQL &= " 	    CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
                SQL &= "            FROM" & vbNewLine
                SQL &= "	    (" & vbNewLine
                SQL &= " 		SELECT RPT_Year,RPT_No,RPT_THM_Detail.TAG_ID,TAG_STATUS" & vbNewLine
                SQL &= "                FROM RPT_THM_Detail INNER JOIN MS_THM_Tag ON RPT_THM_Detail.TAG_ID = MS_THM_Tag.TAG_ID AND MS_THM_Tag.Active_Status = 1" & vbNewLine
                SQL &= " 	    ) TB1" & vbNewLine
                SQL &= " 	    GROUP BY TAG_ID,RPT_Year,RPT_No,TAG_STATUS" & vbNewLine
                SQL &= " 	 ) TB2" & vbNewLine
                SQL &= "    ) TB3" & vbNewLine
                SQL &= "    WHERE ROW_NUM = 1" & vbNewLine
                SQL &= " ) TAG_STATUS" & vbNewLine
                SQL &= " ON TAG.TAG_ID = TAG_STATUS.TAG_ID" & vbNewLine
                SQL &= "LEFT JOIN MS_Plant " & vbNewLine
                SQL &= "ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
            Case EIR_BL.Report_Type.All
                '--------------------- ST -----------------------
                SQL &= "	 SELECT DISTINCT MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_ST_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
                SQL &= "	 FROM MS_Plant" & vbLf
                SQL &= "	 LEFT JOIN MS_ST_Route ON MS_Plant.PLANT_ID=MS_ST_Route.PLANT_ID AND MS_ST_Route.Active_Status=1" & vbLf
                SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
                SQL &= "	 INNER JOIN MS_ST_TAG ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID AND MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID AND MS_ST_TAG.Active_Status=1" & vbLf
                SQL &= "     LEFT JOIN (" & vbNewLine
                SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
                SQL &= "        FROM" & vbNewLine
                SQL &= "        (" & vbNewLine
                SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
                SQL &= "            FROM" & vbNewLine
                SQL &= "            (" & vbNewLine
                SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
                SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
                SQL &= "                FROM" & vbNewLine
                SQL &= "     		    (" & vbNewLine
                SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_ST_Detail.TAG_TYPE_ID,RPT_ST_Detail.INSP_ID," & vbNewLine
                SQL &= "     				ICLS_ID CUR_LEVEL" & vbNewLine
                SQL &= "                    FROM RPT_ST_Detail" & vbNewLine
                SQL &= "     		    ) TB1" & vbNewLine
                SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
                SQL &= "     	    ) TB2" & vbNewLine
                SQL &= "        ) TB3" & vbNewLine
                SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "     ) VW ON MS_ST_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
                SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
                '------------------------------------------------
                SQL &= "UNION ALL" & vbNewLine
                '--------------------- RO -----------------------
                SQL &= "	 SELECT DISTINCT MS_Plant.PLANT_ID,MS_Plant.PLANT_Code,MS_Plant.PLANT_Name,MS_RO_TAG.TAG_ID,ISNULL(CUR_LEVEL,0) CUR_LEVEL" & vbLf
                SQL &= "	 FROM MS_Plant" & vbLf
                SQL &= "	 LEFT JOIN MS_RO_Route ON MS_Plant.PLANT_ID=MS_RO_Route.PLANT_ID AND MS_RO_Route.Active_Status=1" & vbLf
                SQL &= "	 LEFT JOIN MS_Area ON MS_Plant.PLANT_ID=MS_Area.PLANT_ID AND MS_Area.Active_Status=1" & vbLf
                SQL &= "	 INNER JOIN MS_RO_TAG ON MS_RO_TAG.AREA_ID=MS_Area.AREA_ID AND MS_RO_TAG.ROUTE_ID=MS_RO_Route.ROUTE_ID AND MS_RO_TAG.Active_Status=1" & vbLf
                SQL &= "     LEFT JOIN (" & vbNewLine
                SQL &= "        SELECT TAG_ID,CUR_LEVEL" & vbNewLine
                SQL &= "        FROM" & vbNewLine
                SQL &= "        (" & vbNewLine
                SQL &= "     	    SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
                SQL &= "            FROM" & vbNewLine
                SQL &= "            (" & vbNewLine
                SQL &= "                SELECT TAG_ID,RPT_Year,RPT_No,MAX(CUR_LEVEL) CUR_LEVEL," & vbNewLine
                SQL &= "     	        CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
                SQL &= "                FROM" & vbNewLine
                SQL &= "     		    (" & vbNewLine
                SQL &= "     				SELECT RPT_Year,RPT_No,TAG_ID,RPT_RO_Detail.TAG_TYPE_ID,RPT_RO_Detail.INSP_ID," & vbNewLine
                SQL &= "     				CASE WHEN RPT_RO_Detail.INSP_ID = 12 THEN" & vbNewLine
                SQL &= "     					CASE ICLS_ID WHEN 3 THEN 3 WHEN 2 THEN 1 WHEN 1 THEN 0 WHEN 0 THEN 0 ELSE ICLS_ID END" & vbNewLine
                SQL &= "     				ELSE" & vbNewLine
                SQL &= "                        ICLS_ID" & vbNewLine
                SQL &= "     				END CUR_LEVEL" & vbNewLine
                SQL &= "                    FROM RPT_RO_Detail" & vbNewLine
                SQL &= "     		    ) TB1" & vbNewLine
                SQL &= "     		    GROUP BY TAG_ID,RPT_Year,RPT_No" & vbNewLine
                SQL &= "     	    ) TB2" & vbNewLine
                SQL &= "        ) TB3" & vbNewLine
                SQL &= "        WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "     ) VW ON MS_RO_TAG.TAG_ID=VW.TAG_ID" & vbNewLine
                SQL &= "	 WHERE MS_Plant.Active_Status = 1" & vbLf
                '------------------------------------------------
                SQL &= "UNION ALL" & vbNewLine
                '--------------------- LO -----------------------
                SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "	SELECT *," & vbNewLine
                SQL &= "	CASE WHEN	TAN_Value>=1.5 OR " & vbNewLine
                SQL &= "				(Oil_Cat=1 AND Ox_Value>=30) OR  ---------- Oxidation Abnormal----------" & vbNewLine
                SQL &= "				(Oil_Cat=2 AND Ox_Value<=30) OR " & vbNewLine
                SQL &= "				VANISH_Value>30 OR ---------- Varnish Abnormal----------" & vbNewLine
                SQL &= "				(Oil_Cat=1 AND WATER_Value>=750) OR" & vbNewLine
                SQL &= "				(Oil_Cat=2 AND WATER_Value>=1500)" & vbNewLine
                SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
                SQL &= "" & vbNewLine
                SQL &= "	 FROM" & vbNewLine
                SQL &= "	(" & vbNewLine
                SQL &= "		SELECT TAG.LO_TAG_ID TAG_ID,TAG.PLANT_ID,PLANT_Code,PLANT_Name,Oil_Cat,TAN_Value,OX_Value,VANISH_Value,WATER_Value" & vbNewLine
                SQL &= "		FROM MS_LO_TAG TAG" & vbNewLine
                SQL &= "		LEFT JOIN MS_LO_Oil_Type ON TAG.Oil_TYPE_ID = MS_LO_Oil_Type.Oil_TYPE_ID" & vbNewLine
                SQL &= "		LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,TAN_Value " & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,TAN_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE TAN_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) TAN_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = TAN_Value.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,OX_Value " & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,OX_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE OX_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) OX_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = OX_Value.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,VANISH_Value " & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,VANISH_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE VANISH_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) VANISH_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = VANISH_Value.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "		SELECT TAG_ID,RPT_Year,RPT_No,WATER_Value " & vbNewLine
                SQL &= "		FROM" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "			SELECT LO_TAG_ID TAG_ID,RPT_Year,RPT_No,WATER_Value " & vbNewLine
                SQL &= "			FROM RPT_LO_Detail " & vbNewLine
                SQL &= "			) TB1" & vbNewLine
                SQL &= "			WHERE WATER_Value IS NOT NULL" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1" & vbNewLine
                SQL &= "		) WATER_Value" & vbNewLine
                SQL &= "		ON TAG.LO_TAG_ID = WATER_Value.TAG_ID" & vbNewLine
                SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
                SQL &= "	) ALL_TAG" & vbNewLine
                SQL &= ") TAG_FILTER" & vbNewLine
                '------------------------------------------------
                SQL &= "UNION ALL" & vbNewLine
                '-------------------- PdMA ----------------------
                SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "	SELECT *," & vbNewLine
                SQL &= "	CASE WHEN	PowerQuality = 1 OR Insulation = 1 OR PowerCircuit = 1 OR" & vbNewLine
                SQL &= "			Stator = 1 OR Rotor = 1 OR AirGap = 1" & vbNewLine
                SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
                SQL &= "" & vbNewLine
                SQL &= "	 FROM" & vbNewLine
                SQL &= "	(" & vbNewLine
                SQL &= "		SELECT TAG.TAG_ID,MS_PDMA_Route.PLANT_ID,PLANT_Code,PLANT_Name," & vbNewLine
                SQL &= "		CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END PowerQuality," & vbNewLine
                SQL &= "		CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Insulation," & vbNewLine
                SQL &= "		CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END PowerCircuit," & vbNewLine
                SQL &= "		CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Stator," & vbNewLine
                SQL &= "		CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rotor," & vbNewLine
                SQL &= "		CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END AirGap" & vbNewLine
                SQL &= "		FROM MS_PDMA_TAG TAG " & vbNewLine
                SQL &= "		LEFT JOIN MS_PDMA_Route ON TAG.ROUTE_ID = MS_PDMA_Route.ROUTE_ID" & vbNewLine
                SQL &= "		LEFT JOIN MS_Plant ON MS_PDMA_Route.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) PWQ" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
                SQL &= "		) INS" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,PowerCircuit" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
                SQL &= "		) PWC" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,Stator" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
                SQL &= "		) STA" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) ROT" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
                SQL &= "				FROM RPT_PDMA_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) AIR" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
                SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
                SQL &= "	) ALL_TAG" & vbNewLine
                SQL &= ") TAG_FILTER" & vbNewLine
                '-------------------- MTAP ----------------------
                SQL &= "UNION ALL" & vbNewLine
                '-------------------- MTAP ----------------------
                SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name,TAG_ID,CASE WHEN Is_Abnormal = 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "	SELECT *," & vbNewLine
                SQL &= "	CASE WHEN	PowerQuality = 1 OR Insulation = 1 OR PowerCircuit = 1 OR" & vbNewLine
                SQL &= "			Stator = 1 OR Rotor = 1 OR AirGap = 1" & vbNewLine
                SQL &= "	THEN 1 ELSE 0 END Is_Abnormal" & vbNewLine
                SQL &= "" & vbNewLine
                SQL &= "	 FROM" & vbNewLine
                SQL &= "	(" & vbNewLine
                SQL &= "		SELECT TAG.TAG_ID,TAG.PLANT_ID,PLANT_Code,PLANT_Name," & vbNewLine
                SQL &= "		CASE PowerQuality WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerQuality END PowerQuality," & vbNewLine
                SQL &= "		CASE Insulation WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Insulation END Insulation," & vbNewLine
                SQL &= "		CASE PowerCircuit WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE PowerCircuit END PowerCircuit," & vbNewLine
                SQL &= "		CASE Stator WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Stator END Stator," & vbNewLine
                SQL &= "		CASE Rotor WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE Rotor END Rotor," & vbNewLine
                SQL &= "		CASE AirGap WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE AirGap END AirGap" & vbNewLine
                SQL &= "		FROM MS_MTAP_TAG TAG " & vbNewLine
                SQL &= "		LEFT JOIN MS_Plant ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END PowerQuality" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerQuality ELSE NULL END PowerQuality" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN PowerQuality ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) PWQ" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWQ.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END Insulation" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN NULL ELSE F_Status_Insulation END Insulation" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN 1 ELSE Insulation END IS NOT NULL" & vbNewLine
                SQL &= "		) INS" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = INS.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,PowerCircuit" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_PowerCircuit ELSE F_Status_PowerCircuit END PowerCircuit" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND PowerCircuit IS NOT NULL" & vbNewLine
                SQL &= "		) PWC" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = PWC.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,Stator" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Stator ELSE F_Status_Stator END Stator" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND Stator IS NOT NULL" & vbNewLine
                SQL &= "		) STA" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = STA.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END Rotor" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_Rotor ELSE NULL END Rotor" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN Rotor ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) ROT" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = ROT.TAG_ID" & vbNewLine
                SQL &= "		LEFT JOIN" & vbNewLine
                SQL &= "		(" & vbNewLine
                SQL &= "			SELECT TAG_ID,RPT_Year,RPT_No,CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END AirGap" & vbNewLine
                SQL &= "			FROM" & vbNewLine
                SQL &= "			(" & vbNewLine
                SQL &= "				SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_YEAR DESC,RPT_NO DESC) ROW_NUM FROM " & vbNewLine
                SQL &= "				(" & vbNewLine
                SQL &= "				SELECT TAG_ID,RPT_Year,RPT_No,TAG_Mode,CASE WHEN TAG_Mode = 1 THEN N_Status_AirGap ELSE NULL END AirGap" & vbNewLine
                SQL &= "				FROM RPT_MTAP_Detail WHERE TAG_Mode > 0 " & vbNewLine
                SQL &= "				) TB1" & vbNewLine
                SQL &= "			) TB2" & vbNewLine
                SQL &= "			WHERE ROW_NUM = 1 AND CASE WHEN TAG_Mode = 1 THEN AirGap ELSE 1 END IS NOT NULL" & vbNewLine
                SQL &= "		) AIR" & vbNewLine
                SQL &= "		ON TAG.TAG_ID = AIR.TAG_ID" & vbNewLine
                SQL &= "		WHERE TAG.Active_Status = 1" & vbNewLine
                SQL &= "	) ALL_TAG" & vbNewLine
                SQL &= ") TAG_FILTER" & vbNewLine
                '------------------------------------------------
                SQL &= "UNION ALL" & vbNewLine
                '---------------------- THM ----------------------
                SQL &= "SELECT TAG.PLANT_ID,PLANT_Code,PLANT_Name,TAG.TAG_ID," & vbNewLine
                SQL &= "CASE TAG_STATUS WHEN 1 THEN 3 ELSE 0 END CUR_LEVEL" & vbNewLine
                SQL &= "FROM MS_THM_Tag TAG" & vbNewLine
                SQL &= "LEFT JOIN" & vbNewLine
                SQL &= "(" & vbNewLine
                SQL &= "    SELECT RPT_Year,RPT_No,TAG_ID,TAG_STATUS" & vbNewLine
                SQL &= "    FROM" & vbNewLine
                SQL &= "    (" & vbNewLine
                SQL &= " 	SELECT *,ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Code DESC) ROW_NUM" & vbNewLine
                SQL &= "        FROM" & vbNewLine
                SQL &= "        (" & vbNewLine
                SQL &= "            SELECT TAG_ID,RPT_Year,RPT_No,TAG_STATUS," & vbNewLine
                SQL &= " 	    CONVERT(VARCHAR(4),RPT_Year) + RIGHT('0000' + CONVERT(VARCHAR(10),RPT_No),4) RPT_Code" & vbNewLine
                SQL &= "            FROM" & vbNewLine
                SQL &= "	    (" & vbNewLine
                SQL &= " 		SELECT RPT_Year,RPT_No,RPT_THM_Detail.TAG_ID,TAG_STATUS" & vbNewLine
                SQL &= "                FROM RPT_THM_Detail INNER JOIN MS_THM_Tag ON RPT_THM_Detail.TAG_ID = MS_THM_Tag.TAG_ID AND MS_THM_Tag.Active_Status = 1" & vbNewLine
                SQL &= " 	    ) TB1" & vbNewLine
                SQL &= " 	    GROUP BY TAG_ID,RPT_Year,RPT_No,TAG_STATUS" & vbNewLine
                SQL &= " 	 ) TB2" & vbNewLine
                SQL &= "    ) TB3" & vbNewLine
                SQL &= "    WHERE ROW_NUM = 1" & vbNewLine
                SQL &= " ) TAG_STATUS" & vbNewLine
                SQL &= " ON TAG.TAG_ID = TAG_STATUS.TAG_ID" & vbNewLine
                SQL &= "LEFT JOIN MS_Plant " & vbNewLine
                SQL &= "ON TAG.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                '------------------------------------------------
        End Select
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= " ORDER BY PLANT_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("Dashboard_Current_Status") = DT


        rptChart.DataSource = DT
        rptChart.DataBind()

        rptData.DataSource = DT
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblNormal As Label = e.Item.FindControl("lblNormal")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")

        lblPlant.Text = e.Item.DataItem("PLANT_CODE")

        Dim Normal As Integer = 0
        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0
        If Not IsDBNull(e.Item.DataItem("Normal")) AndAlso e.Item.DataItem("Normal") <> 0 Then
            Normal = e.Item.DataItem("Normal")
            lblNormal.Text = FormatNumber(Normal, 0)
        Else
            lblNormal.Text = "-"
        End If
        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If

        If Normal + ClassC + ClassB + ClassA <> 0 Then
            lblTotal.Text = FormatNumber(Normal + ClassC + ClassB + ClassA, 0)
        Else
            lblTotal.Text = "-"
        End If

        Select Case ddl_Equipment.SelectedValue
            Case EIR_BL.Report_Type.All
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Plant_ST.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Lube_Oil_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_AllTag_LO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_AllTag_PdMA.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Thermography_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_AllTag_THM.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
        End Select

    End Sub

    Protected Sub rptChart_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptChart.ItemDataBound

        '---------------- Legend Control ------------------
        If e.Item.ItemType = ListItemType.Footer Then
            Dim S As DataTable = rptChart.DataSource
            e.Item.Visible = Not IsNothing(S) AndAlso S.Rows.Count > 0
        End If

        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim ChartPlant As System.Web.UI.DataVisualization.Charting.Chart = e.Item.FindControl("ChartPlant")

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartPlant.Series(0).ChartType
        ChartType = DataVisualization.Charting.SeriesChartType.Pie
        ChartPlant.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Current_Status")
        Dim Normal As Double = e.Item.DataItem("Normal")
        Dim ClassC As Double = e.Item.DataItem("ClassC")
        Dim ClassB As Double = e.Item.DataItem("ClassB")
        Dim ClassA As Double = e.Item.DataItem("ClassA")

        Dim Url As String = ""

        Select Case ddl_Equipment.SelectedValue
            Case EIR_BL.Report_Type.All
                Url = "Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                Url = "Dashboard_Current_Status_Plant_ST.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                Url = "Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Lube_Oil_Report
                Url = "Dashboard_Current_Status_AllTag_LO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                Url = "Dashboard_Current_Status_AllTag_PdMA.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Thermography_Report
                Url = "Dashboard_Current_Status_AllTag_THM.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
        End Select


        ChartPlant.Series("Series1").Url = Url

        Dim YValue As Double() = {Normal, ClassC, ClassB, ClassA}
        'Dim XValue As String() = {"Normal", "ClassC", "ClassB", "ClassA"}
        ChartPlant.Series("Series1").Points.DataBindY(YValue)
        ChartPlant.Series("Series1").Points(0).LegendText = "Normal"
        ChartPlant.Series("Series1").Points(1).LegendText = "ClassC"
        ChartPlant.Series("Series1").Points(2).LegendText = "ClassB"
        ChartPlant.Series("Series1").Points(3).LegendText = "ClassA"

        ChartPlant.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
        ChartPlant.Series("Series1").Points(1).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
        ChartPlant.Series("Series1").Points(2).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
        ChartPlant.Series("Series1").Points(3).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

        ChartPlant.Series("Series1").Points(0).Color = Drawing.Color.Green
        ChartPlant.Series("Series1").Points(1).Color = Drawing.Color.Yellow
        ChartPlant.Series("Series1").Points(2).Color = Drawing.Color.Orange
        ChartPlant.Series("Series1").Points(3).Color = Drawing.Color.Red

        ChartPlant.Titles(0).Text = e.Item.DataItem("PLANT_Code")
        ChartPlant.Titles(1).Text = "Total " & FormatNumber(Normal + ClassC + ClassB + ClassA, 0) & " tag(s)"
    End Sub

    Protected Sub ddl_Equipment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Equipment.SelectedIndexChanged
        BindData(ddl_Equipment.SelectedValue)
    End Sub
End Class