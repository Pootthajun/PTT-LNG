﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_SelectTemplate.ascx.vb" Inherits="EIR.UC_ST_TA_SelectTemplate" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<div class="MaskDialog"></div>
<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture"  >
    <h2 id="plugin-name">Select Template</h2>
    <center>
    <table style="width:600px;"  >
        <tr>
            <td style="border:1px solid;" align="center" >
                <asp:LinkButton ID="lnkTemplate1" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template1.JPG" alt="Template 1" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate6" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template6.JPG" alt="Template 6" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate4" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template4.JPG" alt="Template 4" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate2" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template2.JPG" alt="Template 2" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate5" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template5.JPG" alt="Template 5" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate3" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template3.JPG" alt="Template 3" height="150" />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate7" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template7.JPG" alt="Template 7" height="100"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td style="border:1px solid;" >
                <asp:LinkButton ID="lnkTemplate8" runat="server" >
					<span>
                        <center>
						<img src="resources/images/OffRoutineTemplate/Template8.JPG" alt="Template 8" height="70"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
        </tr>
    </table>
    </center>
    <p align="right">
	    <asp:Button ID="btn_Close" runat="server" CssClass="button" Text="Close" />
    </p>
</asp:Panel>