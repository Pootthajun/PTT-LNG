<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GL_ImageEditor.ascx.vb" Inherits="EIR.GL_ImageEditor" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="ColorPicker.ascx" tagname="ColorPicker" tagprefix="uc1" %>
<%@ Register src="UpdateProgress.ascx" tagname="UpdateProgress" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="HeadMain" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
<title>PTT-EIR :: Image Editor</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
body,td,th {
	font-family: Tahoma;
	font-size: 12px;
}
.button {
				font-family: Verdana, Arial, sans-serif;
                display: inline-block;
                background: #459300 url('resources/images/bg-button-green.gif') top left repeat-x !important;
                border: 1px solid #459300 !important;
                padding: 4px 7px 4px 7px !important;
                color: #fff !important;
                font-size: 11px !important;
                cursor: pointer;
                }
                
-->
</style>

<script language="javascript">

    function colorChanged(sender) {
      document.getElementById(sender.get_element().id + '_Value').value=sender.get_selectedColor();
    }
    
    function mousemove(e)
    {
        var mode=document.getElementById('txtMode').value;
        var crop=document.getElementById('crop');
        
        if(document.getElementById("txtImageWidth").value=="")return;
        if(document.getElementById("txtImageHeight").value=="")return;
        var W=parseInt(document.getElementById("txtImageWidth").value);
        var H=parseInt(document.getElementById("txtImageHeight").value);
        var img=document.getElementById('ImgPreview');
        var RX=parseFloat(W/parseInt(img.style.width.replace('px','')));
        var RY=parseFloat(H/parseInt(img.style.height.replace('px','')));
        
        switch(mode)
        {
            case 'Circle':
                var ddl = document.getElementById('Pane_Circle_content_ddl_CircleStyle');
                //Get Size
                var Radius = parseInt(document.getElementById('Pane_Circle_content_txt_CircleRadius_Value').value);
                crop.style.width = parseInt(Radius * 2 / RX) + 'px';
                crop.style.height = parseInt(Radius * 2 / RY) + 'px';
                crop.style.borderStyle = ddl.value;
                crop.style.visibility = 'visible';
                crop.innerHTML = "";
                // �ش��ҧ
                crop.style.borderRadius = Radius + 'px';

                // Get Position
                crop.style.left = (e.clientX - (Radius / RX)) + 'px';
                crop.style.top = (e.clientY - (Radius / RY)) + 'px';
                crop.style.borderColor = document.getElementById("Pane_Circle_content_CircleColor_btnResult").style.backgroundColor;
                crop.style.backgroundColor = "";
                break;
            case 'Box':
                var ddl=document.getElementById('Pane_Box_content_ddl_BoxStyle');
                           
                //Get Size
                var Radius=parseInt(document.getElementById('Pane_Box_content_txt_BoxRadius_Value').value);
                                            
                crop.style.width=parseInt(Radius*2/RX)+'px';
                crop.style.height=parseInt(Radius*2/RY)+'px';
                crop.style.borderStyle=ddl.value;   
                crop.style.visibility='visible';
                crop.innerHTML="";
                // �ش��ҧ
                crop.style.borderRadius = '';

                // Get Position
                crop.style.left= (e.clientX-(Radius/RX))+'px';
                crop.style.top= (e.clientY-(Radius/RY))+'px';
                crop.style.borderColor=document.getElementById("Pane_Box_content_BoxColor_btnResult").style.backgroundColor;
                crop.style.backgroundColor="";
                break;
            case 'Text':
                var BoxStyle=document.getElementById('Pane_Text_content_ddl_TextBackground').value;
                var Font=document.getElementById('Pane_Text_content_ddl_TextFont').value;
                var FontSize=document.getElementById('Pane_Text_content_txt_FontSize_Value').value;
                var ForeColor=document.getElementById("Pane_Text_content_TextForeColor_btnResult").style.backgroundColor;
                var BackColor=document.getElementById("Pane_Text_content_TextBackColor_btnResult").style.backgroundColor;
                var BoxWidth=parseInt(document.getElementById('Pane_Text_content_txt_TextWidth_Value').value);
                var BoxHeight=parseInt(document.getElementById('Pane_Text_content_txt_TextHeight_Value').value);
                var M=document.getElementById('Pane_Text_content_txt_Message').value.toString().split('\n');
                var Message="";
                for(i=0;i<M.length;i++)
                {
                    Message +="<nobr>" + M[i] + "</nobr><br>"; 
                }
                          
                           
                BoxWidth = BoxWidth/RX;
                BoxHeight = BoxHeight/RY;
                crop.style.width=parseInt(BoxWidth/RX)+'px';
                crop.style.height=parseInt(BoxHeight/RY)+'px';
                crop.style.borderStyle="none";   
                crop.style.visibility='visible';
                if(BoxStyle=='Transparent')
                {
                crop.style.backgroundColor="";
                }else
                crop.style.backgroundColor=BackColor;
                crop.style.fontFamily=Font;
                crop.style.color=ForeColor;
                crop.innerHTML=Message;
                crop.style.fontSize=parseInt(FontSize/RX);
                    // Get Position
                crop.style.left= (e.clientX+2)+'px';
                crop.style.top= (e.clientY+2)+'px';
                            
                break;                            
             default: 
                crop.style.visibility='hidden';
                break;
        }
       
        
    }
</script>

</head>

<body >
<form id="form1" runat="server" target="_self">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />  
           

<table width="850"  height="580" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="300" rowspan="3" valign="top" background="resources/Images/ImageEditor/bgLeft.png" >
<%--    <asp:UpdatePanel ID="UDPLeftMenu" runat="server">
    <ContentTemplate>--%>

        
    <cc1:Accordion ID="LeftMenu" runat="server" FadeTransitions="true" RequireOpenedPane="false"  AutoSize="None">
    
        <Panes>
            <cc1:AccordionPane runat="server" ID="Pane_Circle">
                 <Header>
	                <table width="300" height="30" border="0" cellpadding="0" cellspacing="0" bgcolor="white" style="border-bottom:1px solid #cfcfcf" onclick="document.getElementById('txtMode').value='Circle';">
                      <tr>
                        <td width="15">&nbsp;</td>
                        <td style="text-decoration:none; cursor:pointer; font-weight:bold;">Draw Circle</td>
                      </tr>
                    </table>
                </Header>
                <Content>
	                <table width="300" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FAFAFA" style="border-bottom:1px solid #cfcfcf; font-size:10px;">
                      <tr>
                        <td width="15">&nbsp;</td>
                        <td width="85" height="30">Radius</td>
                        <td> 
                            <table width="200" height="30">
                                <tr>
                                    <td width="100">
                                    <asp:TextBox ID="txt_CircleRadius" runat="server" Style="text-align:center;" Text="50"></asp:TextBox>
                                    <cc1:SliderExtender ID="txt_CircleRadius_SliderExtender" runat="server" Minimum="10" Maximum="400" Steps="290"
                                        TargetControlID="txt_CircleRadius" BoundControlID="txt_CircleRadius_Value" Length="100">
                                    </cc1:SliderExtender>
                                    </td>
                                    <td Width="100">
                                    <asp:TextBox ID="txt_CircleRadius_Value" runat="server" Width="50px" Text="50"></asp:TextBox> PX
                                    </td>
                                </tr>
                            </table>
                            
                            
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td width="85" height="30">Style</td>
                        <td height="30">
                        <asp:DropDownList ID="ddl_CircleStyle" Width="185px" runat="server">
                            <asp:ListItem Text="Solid"></asp:ListItem>
                            <asp:ListItem Text="Dotted"></asp:ListItem>
                            <asp:ListItem Text="Dashed" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td width="85" height="30">Color</td>
                        <td height="30">
                         
                         <uc1:ColorPicker ID="CircleColor" runat="server" Width="180" ResultColor="Yellow" />
                            
                        </td>
                      </tr>
                         <tr>
                            <td>&nbsp;</td>
                            <td width="85" height="30">&nbsp;</td>
                            <td height="30">
                             &nbsp;
                            </td>
                          </tr>
                    </table>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane runat="server" ID="Pane_Box">
                <Header>
	                <table width="300" height="30" border="0" cellpadding="0" cellspacing="0" bgcolor="white" style="border-bottom:1px solid #cfcfcf" onclick="document.getElementById('txtMode').value='Box';">
                      <tr>
                        <td width="15">&nbsp;</td>
                        <td style="text-decoration:none; cursor:pointer; font-weight:bold;">Draw Box</td>
                      </tr>
                    </table>
                </Header>
                <Content>
	                <table width="300" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FAFAFA" style="border-bottom:1px solid #cfcfcf; font-size:10px;">
                      <tr>
                        <td width="15">&nbsp;</td>
                        <td width="85" height="30">Radius</td>
                        <td> 
                            <table width="200" height="30">
                                <tr>
                                    <td width="100">
                                    <asp:TextBox ID="txt_BoxRadius" runat="server" Style="text-align:center;" Text="50"></asp:TextBox>
                                    <cc1:SliderExtender ID="txt_BoxRadius_SliderExtender" runat="server" Minimum="10" Maximum="400" Steps="290"
                                        TargetControlID="txt_BoxRadius" BoundControlID="txt_BoxRadius_Value" Length="100">
                                    </cc1:SliderExtender>
                                    </td>
                                    <td Width="100">
                                    <asp:TextBox ID="txt_BoxRadius_Value" runat="server" Width="50px" Text="50"></asp:TextBox> PX
                                    </td>
                                </tr>
                            </table>                            
                            
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td width="85" height="30">Style</td>
                        <td height="30">
                        <asp:DropDownList ID="ddl_BoxStyle" Width="185px" runat="server">
                            <asp:ListItem Text="Solid"></asp:ListItem>
                            <asp:ListItem Text="Dotted"></asp:ListItem>
                            <asp:ListItem Text="Dashed"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td width="85" height="30">Color</td>
                        <td height="30">
                         
                         <uc1:ColorPicker ID="BoxColor" runat="server" Width="180" />
                            
                        </td>
                      </tr>
                         <tr>
                            <td>&nbsp;</td>
                            <td width="85" height="30">&nbsp;</td>
                            <td height="30">
                             &nbsp;
                            </td>
                          </tr>
                    </table>
                </Content>
             </cc1:AccordionPane>
             <cc1:AccordionPane runat="server" ID="Pane_Text">
             
                <Header>
                        <table width="300" height="30" border="0" cellpadding="0" cellspacing="0" bgcolor="white"  style="border-bottom:1px solid #cfcfcf" onclick="document.getElementById('txtMode').value='Text';" >
                          <tr>
                            <td width="15">&nbsp;</td>
                            <td style="cursor:pointer; font-weight:bold;">Text Notice</td>
                          </tr>
                        </table>
                </Header>
                <Content>
	                    <table width="300" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FAFAFA" style="border-bottom:1px solid #cfcfcf; font-size:10px;">
                          <tr>
                            <td width="15">&nbsp;</td>
                            <td width="120" height="30">Font</td>
                            <td width="165">
                                <asp:DropDownList ID="ddl_TextFont" Width="152px" runat="server">
                                <asp:ListItem Text="Angsana New"></asp:ListItem>
                                <asp:ListItem Text="Browallia UPC"></asp:ListItem>
                                <asp:ListItem Text="Cordia New"></asp:ListItem>
                                <asp:ListItem Text="Microsoft Sans Serif"></asp:ListItem>
                                <asp:ListItem Text="Tahoma" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td width="120" height="30">Font Size</td>
                            <td width="165" height="30">
                             <table width="165" height="30">
                                <tr>
                                    <td width="100">
                                    <asp:TextBox ID="txt_FontSize" runat="server" Style="text-align:center;"></asp:TextBox>
                                    <cc1:SliderExtender ID="txt_FontSize_Extender" runat="server" Minimum="8" Maximum="50" Steps="42"
                                        TargetControlID="txt_FontSize" BoundControlID="txt_FontSize_Value" Length="100">
                                    </cc1:SliderExtender>
                                    </td>
                                    <td Width="65">
                                    <asp:TextBox ID="txt_FontSize_Value" runat="server" Width="20px"></asp:TextBox> PX
                                    </td>
                                </tr>
                            </table>
                            
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td width="120" height="30">Forecolor</td>
                            <td width="165" height="30">
                            
                            <uc1:ColorPicker ID="TextForeColor" runat="server" Width="150" ResultColor="Black" />
                           
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td width="120" height="30">Background Style </td>
                            <td width="165" height="30">
                                     <asp:DropDownList ID="ddl_TextBackground" Width="152px" runat="server" >
                                    <asp:ListItem Text="Transparent" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Fill Color"></asp:ListItem>
                                    </asp:DropDownList>
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td width="120" height="30">Background Color </td>
                            <td width="165" height="30">                            
                            
                             <uc1:ColorPicker ID="TextBackColor" runat="server" Width="150" />
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td width="120" height="30">Width</td>
                            <td width="165" height="30">
                             <table width="165" height="30">
                                <tr>
                                    <td width="90">
                                    <asp:TextBox ID="txt_TextWidth" runat="server" Style="text-align:center;"></asp:TextBox>
                                    <cc1:SliderExtender ID="txt_BackgroundWidth_SliderExtender" runat="server" Minimum="50" Maximum="1000" Steps="20"
                                        TargetControlID="txt_TextWidth" BoundControlID="txt_TextWidth_Value" Length="87">
                                    </cc1:SliderExtender>
                                    </td>
                                    <td Width="75">
                                    <asp:TextBox ID="txt_TextWidth_Value" runat="server" Width="40px"></asp:TextBox> PX
                                    </td>
                                </tr>
                            </table>
                            
                            </td>
                          </tr>
                           <tr>
                            <td>&nbsp;</td>
                            <td width="120" height="30">Height</td>
                            <td width="165" height="30">
                             <table width="165" height="30">
                                <tr>
                                    <td width="90">
                                    <asp:TextBox ID="txt_TextHeight" runat="server" Style="text-align:center;"></asp:TextBox>
                                    <cc1:SliderExtender ID="txt_TextHeight_SliderExtender" runat="server" Minimum="50" Maximum="500" Steps="10"
                                        TargetControlID="txt_TextHeight" BoundControlID="txt_TextHeight_Value" Length="87">
                                    </cc1:SliderExtender>
                                    </td> 
                                    <td Width="75">
                                    <asp:TextBox ID="txt_TextHeight_Value" runat="server" Width="40px"></asp:TextBox> PX
                                    </td>
                                </tr>
                            </table>
                            
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td height="30" colspan="2">Text</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td height="30" colspan="2">
                                <asp:TextBox ID="txt_Message" runat="server" TextMode="MultiLine" Width="93%" Height="100px"></asp:TextBox>
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td height="30" colspan="2">&nbsp;</td>
                          </tr>
                        </table>
                </Content>
              </cc1:AccordionPane>
          </Panes>
    </cc1:Accordion>
    
        <asp:TextBox ID="txtMode" runat="server" Text="Circle"  style="display:none;" ></asp:TextBox>
        <asp:TextBox ID="txtImageWidth" runat="server" style="display:none;"></asp:TextBox>
        <asp:TextBox ID="txtImageHeight" runat="server" style="display:none;"></asp:TextBox>
         <asp:Label ID="lblTest" runat="server"></asp:Label>
         
<%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </td>
    <td height="40" align="center" background="resources/Images/ImageEditor/bgTop.png" style="font-weight:bold;">
        Upload <asp:FileUpload ID="FUL_Image" runat="server" style=" width:90%; height:22px; border:none; background-color:White;" />
        <asp:Button ID="btnUpload" runat="server" Text="Upload" style="display:none;" />
      </td>
  </tr>
   
  <tr>
    <td width="550" height="500">       
	            <div style="width:550px; height:500px; background-color:#000000; overflow:hidden;">    
	                    <asp:ImageButton ID="btnDraw" runat="server" style="position:absolute; z-index:110; width:550px; height:500px; cursor:crosshair; " onmousemove="mousemove(event);" onmouseout="document.getElementById('crop').style.visibility='hidden';"  ImageUrl="resources/images/transparent.png" />        	
	                    <asp:Image ID="ImgPreview" Visible="false" style="cursor:crosshair;" runat="server" ImageUrl="~/RenderImage.aspx"  />
	                    <div id="crop" style="visibility:hidden;  left:100px; top:100px; position:absolute; z-index:100; width:10px; height:10px; border-style:solid; border-width:2px;">
	                    
                        </div>
                        
	            </div>	    
	</td>
  </tr>
  <tr>
    <td width="550" height="40" align="right" background="resources/Images/ImageEditor/bgTop.png">
                <asp:Label ID="lblValidation" runat="server" Text=""  Font-Bold="true" ForeColor="Red"></asp:Label>                
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cencel" />
                <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" />   
        
    </td>
  </tr>
  
</table>

<uc2:UpdateProgress ID="UpdateProgress1" runat="server" />

</form>
</body>
</html>