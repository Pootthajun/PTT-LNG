﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="MTAP_Tag.aspx.vb" Inherits="EIR.MTAP_Tag" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
			
			<!-- Page Head -->
			<h2>MTAP Tag Setting </h2>
			
						
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
				
				    <asp:DropDownList CssClass="select" style="position:relative; top:5px; left: 0px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    
                    <div class="clear"></div>
              </div>

              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListTag" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Plant</a></th>
                        <th><a href="#">Tag-No </a></th>
                        <th><a href="#">Tag Name </a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Updated</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptTag" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTagName" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                          <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                          <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" />
                                          <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete">
                                          </Ajax:ConfirmButtonExtender>
                                    </td>
                                  </tr>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="7">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				    </asp:Panel>
				    
				 <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
			    
			    <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Tag </h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
                    
                     <p>
                      <label class="column-left" style="width:120px;" >Define to Plant : </label>
                        <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                             ID="ddl_Edit_Plant" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </p>
                   
                    <p>
                      <label class="column-left" style="width:120px;" >Tag Code : </label>
						<asp:TextBox runat="server" ID="txtTagCode" CssClass="text-input" MaxLength="20"
                            style="Width:200px;"></asp:TextBox>
                    </p>
				    <p>
                      <label class="column-left" style="width:120px;" >Tag Name : </label>
                      <asp:TextBox runat="server" ID="txtTagName" CssClass="text-input small-input " Width="400px" MaxLength="100"></asp:TextBox>
                    </p>
                    
					<p>
				      <label style="width:300px;" >Description: </label>
				        <asp:TextBox runat="server" ID="txtDesc" TextMode="MultiLine" CssClass="text-input" Width="100%" Height="80px" MaxLength="500"></asp:TextBox>
					</p>
					
					<p>
				      <label style="width:300px; height:10px;" >Name Plate</label>
				      <table cellpadding="0" cellspacing="0" style="border:1px solid #CCCCCC; width:100%;"> 
					    <tr>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Brand</label>
					        </td>
					        <td style="border:1px solid #CCCCCC; width:30%">
					            <asp:TextBox runat="server" ID="txtTagBrand" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Voltage (V) <font color="red">**</font></label>
					        </td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagVoltage" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Frequency (Hz)</label></td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagFrequency" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					    </tr>
					    <tr>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Type</label>
					        </td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagType" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Current (A)</label>
					        </td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagCurrent" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Power Factor</label></td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagPowerFactor" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					    </tr>
					    <tr>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>KW</label>
					        </td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagKW" MaxLength="100" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>RPM</label>
					        </td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagRPM" MaxLength="10" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					        <td style="width:100px; border:1px solid #CCCCCC;">
					            <label>Insulation Class</label></td>
					        <td style="border:1px solid #CCCCCC;">
					            <asp:TextBox runat="server" ID="txtTagInsulationClass" MaxLength="5" Width="95%" style="border-width:0px; text-align:left; background-color:Transparent;"></asp:TextBox>
					        </td>
					    </tr>
					 </table>
					</p>

					<p>
                      <label class="column-left" style="width:120px;" >Available : </label>
                        &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
				    </p>
				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset></asp:Panel>
                </div>
                <!-- End #tab1 -->
					
				
			  </div> <!-- End .content-box-content -->
				
		  </div>
		  
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>
