﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Current_Status_Plant.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Plant" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<%@ Register assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>
    <!-- Page Head -->
    <h2>Current Status for <asp:LinkButton ID="lblPlant" runat="server" Tooltip="Click back to see all plants" PostBackUrl="~/Dashboard_Current_Status.aspx"></asp:LinkButton></h2>

    <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
        <tr>
            <td>
                <asp:LinkButton ID="lblBack" runat="server" Text="Back to see all plants" PostBackUrl="Dashboard_Current_Status.aspx"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td width="100%" style="vertical-align:top;">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            
                            
                            <table width="100%"  style="border:2px solid #3366ff;" cellpadding="3">
                                <tr>
                                    <td style="background-color:#3366ff; text-align:center; color:White;">
                                        Total Equipement
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                         <asp:Chart ID="Chart6" runat="server" Width="400px" Height="250px" CssClass="ChartHighligh">
                                                <series>
                                                    <asp:Series ChartType="Bar" Name="Series1">
                                                    </asp:Series>
                                                </series>
                                                <chartareas>
                                                    <asp:ChartArea Name="ChartArea1">
                                                        <axisy linecolor="Gray" title="Total tag(s) in this plant" 
                                                            titleforecolor="Gray">
                                                            <majorgrid linecolor="Thistle" />
                                                        </axisy>
                                                        <axisx linecolor="Gray" ismarginvisible="False" isreversed="True">
                                                            <majorgrid enabled="False" />
                                                            <majortickmark enabled="False" linecolor="LightSeaGreen" />
                                                        </axisx>
                                                        <axisx2>
                                                            <majorgrid enabled="False" />
                                                        </axisx2>
                                                        <axisy2>
                                                            <majorgrid enabled="False" />
                                                        </axisy2>
                                                    </asp:ChartArea>
                                                </chartareas>
                                            </asp:Chart>
                                    </td>
                                </tr>
                            </table>                  
                        </td>
                        
                        <td>
                        
                            <table width="100%"  style="border:2px solid #00cc33;" cellpadding="3">
                                <tr>
                                    <td style="background-color:#00cc33; text-align:center; color:White;">
                                        Problems Management for Routine Inspection on Year <asp:Label ID="lblCurrentYear" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Chart ID="ChartAnnual" runat="server" Height="250px" Width="600px" CssClass="ChartHighligh"
                                            Palette="SeaGreen">
                                            <Series>
                                                <asp:Series ChartArea="ChartArea1" ChartType="StackedColumn" 
                                                    Color="255, 255, 153" Name="Series2" IsVisibleInLegend="False" 
                                                    Legend="Legend1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" ChartType="StackedColumn" 
                                                    Color="255, 170, 0" Name="Series3" IsVisibleInLegend="False" 
                                                    Legend="Legend1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" ChartType="StackedColumn" Color="Red" 
                                                    Name="Series4" IsVisibleInLegend="False" Legend="Legend1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" ChartType="Spline" Color="Red" 
                                                    Name="Series5" BorderWidth="2" MarkerColor="Red" MarkerSize="8" 
                                                    MarkerStyle="Circle" Font="Microsoft Sans Serif, 7pt" LabelForeColor="Red" 
                                                    Legend="Legend1" LegendText="New problems">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" ChartType="Spline" Color="ForestGreen" 
                                                    Name="Series1" BorderWidth="2" MarkerColor="0, 192, 0" MarkerSize="8" 
                                                    MarkerStyle="Circle" Font="Microsoft Sans Serif, 7pt" 
                                                    LabelForeColor="Green" Legend="Legend1" LegendText="Fixed completely">
                                                </asp:Series>
                                            </Series>
                                            <legends>
                                                <asp:Legend DockedToChartArea="ChartArea1" Name="Legend1">
                                                </asp:Legend>
                                            </legends>
                                            <titles>
                                                <asp:Title Name="Title1">
                                                </asp:Title>
                                            </titles>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" BorderWidth="0">
                                                    <axisy intervalautomode="VariableCount" islabelautofit="False" 
                                                        title="Problems" titleforecolor="DimGray">
                                                        <majorgrid linecolor="Gainsboro" />
                                                        <majortickmark enabled="False" />
                                                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" ForeColor="DimGray" />
                                                    </axisy>
                                                    <axisx intervalautomode="VariableCount" islabelautofit="False" 
                                                        ismarginvisible="False" title="xxxxx" titleforecolor="Gray">
                                                        <majorgrid linecolor="" linewidth="0" />
                                                        <majortickmark enabled="False" linecolor="" />
                                                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" ForeColor="DimGray" />
                                                    </axisx>
                                                    <axisx2 intervalautomode="VariableCount">
                                                    </axisx2>
                                                    <axisy2 intervalautomode="VariableCount">
                                                    </axisy2>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>
                                    </td>
                                </tr>
                            </table>
                            
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="3" cellspacing="0" style="border:2px solid #FFAA00;">
                    <tr>
                        <td colspan="5" bgColor="#FFAA00" style="color:White; text-align:center; ">
                             Current Equipement Status
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <asp:Chart ID="Chart1" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center; visibility:hidden;">
                            <asp:Chart ID="Chart2" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center; visibility:hidden;">
                            <asp:Chart ID="Chart3" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center; visibility:hidden;">
                            <asp:Chart ID="Chart4" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                        <td style="text-align:center; visibility:hidden;">
                            <asp:Chart ID="Chart5" runat="server" Height="180px" Width="150px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="">
                                    </asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px;">
                                <tr>
                                    <td class="LevelNormal" style="text-align:center;">
                                        Normal
                                    </td>
                                    <td class="LevelClassC" style="text-align:center;">
                                        C
                                    </td>
                                    <td class="LevelClassB" style="text-align:center;">
                                        B
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        A
                                    </td>
                                    <td style="text-align:center; background-color:Gray; color:White;">
                                        Total
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_N" runat="server" CssClass="TextNormal" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_C" runat="server" CssClass="TextClassC" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_B" runat="server" CssClass="TextClassB" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl1_T" runat="server" Font-Bold="true" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px; visibility:hidden;">
                                <tr>
                                    <td class="LevelNormal" style="text-align:center;">
                                        Normal
                                    </td>
                                    <td class="LevelClassC" style="text-align:center;">
                                        C
                                    </td>
                                    <td class="LevelClassB" style="text-align:center;">
                                        B
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        A
                                    </td>
                                    <td style="text-align:center; background-color:Gray; color:White;">
                                        Total
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_N" runat="server" CssClass="TextNormal" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_C" runat="server" CssClass="TextClassC" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_B" runat="server" CssClass="TextClassB" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl2_T" runat="server" Font-Bold="true" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px; visibility:hidden;">
                                <tr>
                                    <td class="LevelNormal" style="text-align:center;">
                                        Normal
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        Abnormal
                                    </td>
                                    <td style="text-align:center; background-color:Gray; color:White">
                                        Total
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl3_N" runat="server" CssClass="TextNormal" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl3_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl3_T" runat="server" Font-Bold="true" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px; visibility:hidden;">
                                <tr>
                                    <td class="LevelNormal" style="text-align:center;">
                                        Normal
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        Abnormal
                                    </td>
                                    <td style="text-align:center; background-color:Gray; color:White">
                                        Total
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl4_N" runat="server" CssClass="TextNormal" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl4_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl4_T" runat="server" Font-Bold="true" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align:center;">
                            <table cellpadding="0" cellspacing="0" style="font-size:7px; visibility:hidden;">
                                <tr>
                                    <td class="LevelNormal" style="text-align:center;">
                                        Normal
                                    </td>
                                    <td class="LevelClassA" style="text-align:center;">
                                        Abnormal
                                    </td>
                                    <td style="text-align:center; background-color:Gray; color:White">
                                        Total
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl5_N" runat="server" CssClass="TextNormal" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl5_A" runat="server" CssClass="TextClassA" Text="-"></asp:Label>
                                    </td>
                                    <td style="text-align:center;">
                                        <asp:Label ID="lbl5_T" runat="server" Font-Bold="true" Text="-"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>
